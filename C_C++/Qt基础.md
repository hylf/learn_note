### 一、下载和安装

#### 1、下载地址

- `https://account.qt.io/s/downloads`
- 使用镜像安装：`qt-online-installer-windows-x64-4.8.1.exe --mirror https://mirrors.aliyun.com/qt`

#### 2、开发套件

##### `WebAssembly(TP)`

- `TP`表示这是一个预览基数版本。是一种二进制字节码格式，他可以在`Web`浏览器的虚拟机上运行。可以将用`Qt`编写的程序编译为`WebAssembly`格式，发布到浏览器上运行，这样就不需要像常规发布应用那样附带`Qt`运行库。而且`WebAssembly`格式与原生机器码格式的程序的运行速度几乎是一样的。要使用`WebAssembly`套件，还需要单独安装将程序编译为`WebAssembly`格式的工具链。

##### `MSVC 2019 ARM(TP)`

- 这是使用`MSVC 2019 ARM64`编译器编译的`Qt`开发套件。一般的`Windows`计算机采用`ARM64`架构。

##### `MSVC 2019 64-bit`

- 要使用这个套件，还必须安装`Visual Studio 2019`

##### `MinGW 11.2.0 64-bit`

- `MinGW`是`Windows`平台上使用的`GNU`工具集，包含`GNU C++`编译器，可以在安装`Qt`时选择安装`MinGW`。

##### `Android`

- 这是为`Android`手机开发应用提供的`Qt`开发套件。要适用这个套件，还必须安装`Android`开发的一些工具链才能构建交叉编译开发环境。

##### `Sources`

- 这是`Qt`框架的源代码。

##### `Qt Quick 3D`

- 这是`Qt`的一个模块，它为`Qt Quick`提供一些实现`3D`图形功能的`API`。`Qt Quick`是`QML`的控件库。

##### `Qt 5 Compatibility Module`

- 这是为兼容`Qt 5`而专门设计的一个模块，它包含在`Qt 6`中移除的一些功能。

##### `Qt Shader Tools`

- `Qt`着色器工具，这是用于`3D`图形着色的模块。

##### `Additional Libraries`

- 这是`Qt`框架的一些附加模块。

##### `Qt Debug Information Files`

- 这是`Qt 6`的调试信息文件。

##### `Qt Quick Timeline`

- 这是`Qt Quick`的一个模块，在`Qt Design Studio`和`Qt Quick Designer`软件中会用到。

##### `Qt Creator 6.0.2`

- 开发`Qt`程序的`IDE`软件。

##### `Qt Creator 6.0.2 CDB Debugger Support`

- 为`Qt Creator`安装`CDB`调试相关的文件，`CDB`是`Windows`平台的调试器。

##### `Debugging Tools for Windows`

- 为调试`MSVC`编译的程序提供的调试器和工具。

##### `Qt Creator 6.0.2 Debug Symbols`

- 为在`Qt Creator`中进行程序调试提供的符号文件。

##### `Qt Creator 6.0.2 Plugin Development`

- 为`Qt Creator`开发插件所需的一些头文件和库文件。

##### `Qt Design Studio 2.3.1-community`

- `QML`编程设计界面的工具软件。

##### `Qt Installer Framework 4.2`

- 为发布应用软件制作安装包的工具软件。

##### `CMake 3.21.1 64-bit`

- `CMake`是一个构建工具。

##### `Ninja 1.10.2`

- `Ninja`是一个小型的构建系统，专注于构建速度。

##### `OpenSSL 1.1.1j Toolkit`

- 安全套接字层是一种网络安全通信协议，`OpenSSL`是实现了`SSL`协议的一个开源工具包。

#### 3、开发套件下的工具软件

##### `Qt Assistant`

- 一个独立的查看`Qt`帮助文档的软件。

##### `Qt Designer`

- 一个独立的进行窗口界面可视化设计的软件。

##### `Qt Linguist`

- 一个编辑语言资源文件的软件，在开发多语言界面的应用时会用到这个软件。

#### 4、创建项目

##### 项目类型

- `Qt Widgets Application`：基于界面组件的应用，也就是具有窗口的`GUI`程序
- `Qt Console Application`：控制台应用。
- `Qt Quick Application`：基于`Qt Quick`的应用，需要使用`QML`编程

##### 三种窗口基类

- `QMainWindow`：主窗口类，具有菜单栏、工具栏和状态栏
- `QWidget`：所有界面组件类的基类，可以作为独立的窗口。
- `QDialog`：对话框类，窗口具有对话框的显示效果，例如没有最大化按钮。

#### 5、项目结构

- `samp1_1.pro`文件。使用`qmake`构建系统时的项目配置文件，包括关于项目的格子设置内容
- `Headers`分组。该节点下是项目内的`C++`头文件。
- `Sources`分组。该节点下是项目内的`C++`源程序文件。
- 如果一个文件的程序中用到`Qt`框架中的某个类，需要编写`include`语句，可以使用`#include <类名称>`的形式

```c++
#include "widget.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv); // 定义并创建应用程序
    Widget w; // 定义并创建窗口对象
    w.show(); // 显示窗口
    return a.exec(); // 运行应用程序, 开始应用程序的消息循环和事件处理
}
```

#### 6、各种输出版本

- `Debug`：调试版本，二进制文件带有调试信息，编译时不进行优化
- `Release`：发行版本，不带调试信息，针对运行速度对文件大小进行了优化
- `Profile`：介于`Debug`和`Release`之间的性能平衡版本，可用于调试

#### 7、`Qt Creator`使用技巧

##### 文本编辑器的快捷操作

| 操作                                  | 快捷键                  | 说明                                                         |
| ------------------------------------- | ----------------------- | ------------------------------------------------------------ |
| `Switch Header/Source`                | <kbd>F4</kbd>           | 在同名的头文件和源程序文件之间切换                           |
| `Follow Symbol Under Cursor`          | <kbd>F2</kbd>           | 跟踪光标处的符号，若是变量, 可跟踪到变量声明的地方；若是函数体或函数声明，可在这两者之间切换 |
| `Refactor/Rename Symbol Under Cursor` | <kbd>Ctrl+Shift+R</kbd> | 更改光标处的符号名称，这将在所有用到这个符号的地方进行替换   |
| `Refactor/Add Definition in.cpp`      |                         | 在源程序文件里为函数原型生成函数体                           |
| `Auto-indent Selection`               | <kbd>Ctrl+I</kbd>       | 为选择的代码段进行自动缩进                                   |
| `Toggle Comment Selection`            | <kbd>Ctrl+/</kbd>       | 注释或取消注释所选代码段                                     |
| `Content Help`                        | <kbd>F1</kbd>           | 为光标处的符号显示帮助文档的内容                             |
| `Save All`                            | <kbd>Ctrl+Shift+S</kbd> | 保存所有文件                                                 |
| `Find/Replace`                        | <kbd>Ctrl+F</kbd>       | 调出查找/替换对话框                                          |
| `Find Next`                           | <kbd>F3</kbd>           | 查找下一个                                                   |
| `Build`                               | <kbd>Ctrl+B</kbd>       | 构建当前项目                                                 |
| `Start Debugging`                     | <kbd>F5</kbd>           | 开始调试                                                     |
| `Step Over`                           | <kbd>F10</kbd>          | 调试状态下单步略过，即运行当前行程序语句                     |
| `Step Into`                           | <kbd>F11</kbd>          | 调试状态下跟踪进入，即如果当前行里有函数，就跟踪进入函数体   |
| `Toggle Breakpoint`                   | <kbd>F9</kbd>           | 设置或取消当前行的断点设置                                   |

##### 项目管理

- `Build`：增量构建
- `Rebuild`：完全重新构建
- `Clean`：清除项目构建过程中的所有中间文件
- `Run qmake`：使用`qmake`构建项目。有些时候`UI`文件被修改了，构建项目时却没有出现相应变化，这时执行`Run qmake`重新构建项目，就会重新执行`UIC`、`MOC`、`RCC`等预编译器。

### 二、`GUI`程序设计基础

#### 1、`qmake`

- `$$`为替换函数的前缀，用于在配置过程中处理变量或内置函数的值
- `#`用于标识注释语句

| 变量          | 含义                                                  |
| ------------- | ----------------------------------------------------- |
| `QT`          | `Qt`模块列表，在用到某些模块时需要手动添加            |
| `CONFIG`      | 通用配置选项                                          |
| `DEFINES`     | 预处理定义列表                                        |
| `TEMPLATE`    | 模板，是`app`（应用程序）还是`lib`（库），默认为`app` |
| `HEADERS`     | 头文件列表                                            |
| `SOURCES`     | 源程序文件列表                                        |
| `FORMS`       | `UI`文件列表                                          |
| `RESOURCES`   | 资源文件(`.qrc`)列表                                  |
| `TARGET`      | 生成的可执行文件名称，默认与项目同名                  |
| `DESTDIR`     | 可执行文件的存放路径                                  |
| `INCLUDEPATH` | 文件搜索路径列表                                      |
| `DEPENDPATH`  | 其他依赖文件的搜索路径列表                            |
| `INSTALLS`    | 创建指向目录的符号链接                                |

#### 2、`CMake`

```cmake
#需要的CMake最低版本
cmake_minimum_required(VERSION 3.16)
# 项目版本0.1,编程语言是C++
project(samp2_3 VERSION 0.1 LANGUAGES CXX)
# UIC能被自动执行
set(CMAKE_AUTOUIC ON)
# MOC能被自动执行
set(CMAKE_AUTOMOC ON)
# RCC能被自动执行
set(CMAKE_AUTORCC ON)
# 设置编译器需要满足的C++语言标准为C++17
set(CMAKE_CXX_STANDARD 17)
# 要求编译器满足C++标准
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Widgets)
# 导入Qt6::Widgets模块
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Widgets)
# 设置变量PROJECT_SOURCES等于下面的列表, 也就是项目的源文件列表
set(PROJECT_SOURCES
        main.cpp
        dialog.cpp
        dialog.h
        dialog.ui
)
# 如果是Qt6以上的版本, 创建可执行文件samp2_3
if(${QT_VERSION_MAJOR} GREATER_EQUAL 6)
    qt_add_executable(samp2_3
        MANUAL_FINALIZATION # 可选参数, 手动结束创建目标的过程
        ${PROJECT_SOURCES} # 文件列表来源于变量PROJECT_SOURCES
    )
# Define target properties for Android with Qt 6 as:
#    set_property(TARGET samp2_3 APPEND PROPERTY QT_ANDROID_PACKAGE_SOURCE_DIR
#                 ${CMAKE_CURRENT_SOURCE_DIR}/android)
# For more information, see https://doc.qt.io/qt-6/qt-add-executable.html#target-creation
else()
    if(ANDROID)
        add_library(samp2_3 SHARED
            ${PROJECT_SOURCES}
        )
# Define properties for Android with Qt 5 after find_package() calls as:
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
    else()
        add_executable(samp2_3
            ${PROJECT_SOURCES}
        )
    endif()
endif()
# 在连接生成目标samp2_3时, 需要利用前面用find_package()导入的Qt6::Widgets模块
target_link_libraries(samp2_3 PRIVATE Qt${QT_VERSION_MAJOR}::Widgets)

# Qt for iOS sets MACOSX_BUNDLE_GUI_IDENTIFIER automatically since Qt 6.1.
# If you are developing for iOS or macOS you should consider setting an
# explicit, fixed bundle identifier manually though.
if(${QT_VERSION} VERSION_LESS 6.1.0)
  set(BUNDLE_ID_OPTION MACOSX_BUNDLE_GUI_IDENTIFIER com.example.samp2_3)
endif()
set_target_properties(samp2_3 PROPERTIES
    ${BUNDLE_ID_OPTION}
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE
)

include(GNUInstallDirs)
install(TARGETS samp2_3
    BUNDLE DESTINATION .
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
# 最后生成可执行文件samp2_3
if(QT_VERSION_MAJOR EQUAL 6)
    qt_finalize_executable(samp2_3)
endif()
```

- `CMakeLists.txt`是`CMake`项目的配置文件
- `CMake Modules`是项目用到的其它一些`CMake`模块，具体的模块就是一些后缀为`.cmake`的文件。
- 函数`find_package()`用于查找和导入`Qt`中的某个模块，函数`target_link_libraries()`用于设置连接时用到的`Qt`模块。要在`CMake`项目中用到`Qt`的某个模块，必须使用这两个函数导入和连接模块。
- 函数`qt_add_executable()`用于创建可执行文件，并且设置依赖的源文件。
- `CMake`项目文件`CMakeLists.txt`的内容一般不需要手动修改，项目中新增文件时会自动更新源文件列表。只有用到`Qt`的某个附加模块时，才需要编写`find_package()`和`target_link_libraries()`函数语句。

#### 3、设计窗口

- 对象名称是窗口上的组件的实例名称，界面上的每个组件需要有一个唯一的对象名称，程序里访问界面组件时都是通过其对象名称进行的，自动生成的槽函数名称里也有对象名称。所以，组件的对象名称设置好后一般不要改动。若需要修改对象名称，涉及的代码需要相应改动。
- 窗体的对象名称会影响窗口`UI`类的名称。一般不在`Qt Designer`里修改窗体的对象名称，除非是要重命名一个窗口，那么需要对窗口相关的4个文件都重命名。
- 设置窗体的`font`属性后，界面上其它组件的默认字体就是窗体的字体，无需再单独设置，除非要为某个组件设置单独的字体。
- 组件的属性都有默认值，一个组件的某个属性被修改后，属性编辑器里的属性名称会以粗体显示，如果要恢复属性的默认值，点击属性值右端的还原按钮。

##### 布局

| 布局组件            | 说明                                                         |
| ------------------- | ------------------------------------------------------------ |
| `Vertical Layout`   | 垂直方向布局，组件自动在垂直方向上分布                       |
| `Horizontal Layout` | 水平方向布局，组件自动在水平方向上分布                       |
| `Grid Layout`       | 网格布局，网格布局大小改变时，每个网格的大小都会改变         |
| `Form Layout`       | 表单布局，与网格布局类似，但是只有最右侧的一列网格会改变大小，适合于只有两列组件时的布局 |
| `Horizontal Spacer` | 用于水平间隔的非可视组件                                     |
| `Vertical Spacer`   | 用于垂直间隔的非可视组件                                     |

#### 4、信号与槽简介

- 信号是在特定情况下被发射的通知。
- 槽是对信号进行响应的函数。槽就是函数，所以也称为槽函数。槽函数与一般的`C++`函数一样，可以具有任何参数，也可以被直接调用。槽函数与一般的函数不同的是，槽函数可以与信号关联，当信号被发射时，关联的槽函数被自动执行。
- 信号与槽关联是用函数`QObject::connect()`实现的，基本格式如下

`QObject::connect(sender, SIGNAL(signal()), receiver, SLOT(slot()));`

- `connect()`是`QObject`类的一个静态函数，而`QObject`是大部分`Qt`类的基类，在实际调用时可以忽略限定部分。
- 其中`sender`是发射信号的对象的名称；`signal()`是信号，信号可以看做是特殊的函数，需要带有括号，有参数时还需要指明各参数类型；`reciver`是接收信号的对象的名称；`slot()`是槽函数，需要带有括号，有参数时还需要指明各参数类型。
- `SIGNAL`和`SLOT`是`Qt`的宏，分别用于指明信号和槽函数，并将他们的参数转换为相应的字符串。需要注意的规则：
  - 一个信号可以连接多个槽函数。当一个信号与多个槽函数关联时，槽函数按照建立连接时的顺序依次运行。当信号和槽函数带有参数时，在函数`connect()`里要指明各参数的类型，但不用指明参数名称。
  - 多个信号可以连接同一个槽函数。
  - 一个信号可以连接另一个信号。这样，当发射一个信号时，也会发射另一个信号，以实现某些特殊的功能。
  - 严格情况下，信号与槽的参数个数和类型需要一致，至少信号的参数不能少于槽的参数。如果参数不匹配，会出现编译错误或运行错误。
  - 在使用信号与槽的类中，必须在类的定义中插入宏`Q_OBJECT`
  - 当一个信号被发射时，与其关联的槽函数通常被立即执行，就像正常调用函数一样。只有当信号关联的所有槽函数运行完毕后，才运行发射信号处后面的代码。
- 函数`connect()`有多种参数形式，有一种常用的形式是不使用`SIGNAL`和`SLOT`宏，而是使用函数指针。

#### 5、信号与槽的使用

- `QMetaObject::connectSlotsByName(Dialog);`搜索`Dialog`界面上的所有组件，将名称匹配的信号和槽关联起来。
- 设置程序图标：将`.ico`图标文件复制到项目根目录下，假设为`my.ico`，在项目配置文件`.pro`里用`RC_ICONS`设置图标文件名，即：`RC_ICONS = my.ico`

#### 6、`Qt`项目构建过程基本原理

##### 元对象系统和`MOC`

- 所有从`QObject`继承的类都可以利用元对象系统提供的功能。元对象系统支持属性、信号与槽、动态类型转换等特性。
- `private slots`部分用于定义私有槽。
- `Qt`提供了元对象编译器`MOC`。在构建项目时，项目中的头文件会先被`MOC`预编译。

##### `UI`文件和`UIC`

##### 资源文件和`RCC`

- 资源文件`.qrc`会被资源编译器`RCC`转换为`C++`程序文件。

#### 7、代码化`UI`设计

- 在`Qt`中容器组件被删除时，其内部组件也会自动被删除。

```c++
#ifndef DIALOG_H
#define DIALOG_H

#include <QCheckBox>
#include <QDialog>
#include <QPlainTextEdit>
#include <QRadioButton>

class Dialog : public QDialog {
  Q_OBJECT
 private:
  QCheckBox *chkBoxUnder;
  QCheckBox *chkBoxItalic;
  QCheckBox *chkBoxBold;

  QRadioButton *radioBlack;
  QRadioButton *radioBlue;
  QRadioButton *radioRed;

  QPushButton *btnOk;
  QPushButton *btnCancel;
  QPushButton *btnClose;

  QPlainTextEdit *txtEdit;
  /**
   * @brief 创建所有界面组件并完成布局和属性设置
   */
  void initUi();

  /**
   * @brief 完成所有的信号和槽的关联
   */
  void initSignalSlots();
 private slots:
  void do_checkBoxUnder(bool checked);
  void do_checkBoxItalic(bool checked);
  void do_checkBoxBold(bool checked);
  void do_setFontColor();

 public:
  Dialog(QWidget *parent = nullptr);
  ~Dialog();
};
#endif  // DIALOG_H
```

```c++
#include "dialog.h"

#include <QHBoxLayout>
#include <QPushButton>
void Dialog::initUi() {
  chkBoxUnder = new QCheckBox("Underline");
  chkBoxItalic = new QCheckBox("Italic");
  chkBoxBold = new QCheckBox("Bold");
  QHBoxLayout *hLay1 = new QHBoxLayout();
  hLay1->addWidget(chkBoxUnder);
  hLay1->addWidget(chkBoxItalic);
  hLay1->addWidget(chkBoxBold);

  radioBlack = new QRadioButton("Black");
  radioBlack->setChecked(true);
  radioRed = new QRadioButton("Red");
  radioBlue = new QRadioButton("Blue");
  QHBoxLayout *hLay2 = new QHBoxLayout();
  hLay2->addWidget(radioBlack);
  hLay2->addWidget(radioRed);
  hLay2->addWidget(radioBlue);

  btnOk = new QPushButton("确定");
  btnCancel = new QPushButton("取消");
  btnClose = new QPushButton("退出");
  QHBoxLayout *hLay3 = new QHBoxLayout();
  hLay3->addStretch();
  hLay3->addWidget(btnOk);
  hLay3->addWidget(btnCancel);
  hLay3->addStretch();
  hLay3->addWidget(btnClose);

  txtEdit = new QPlainTextEdit();
  txtEdit->setPlainText("Hello World\n手工创建");
  QFont font = txtEdit->font();
  font.setPointSize(20);
  txtEdit->setFont(font);

  QVBoxLayout *vLay = new QVBoxLayout(this);
  vLay->addLayout(hLay1);
  vLay->addLayout(hLay2);
  vLay->addWidget(txtEdit);
  vLay->addLayout(hLay3);
  this->setLayout(vLay);
}

void Dialog::initSignalSlots() {
  connect(radioBlue, SIGNAL(clicked()), this, SLOT(do_setFontColor()));
  connect(radioBlack, SIGNAL(clicked()), this, SLOT(do_setFontColor()));
  connect(radioRed, SIGNAL(clicked()), this, SLOT(do_setFontColor()));

  connect(chkBoxUnder, SIGNAL(clicked(bool)), this,
          SLOT(do_checkBoxUnder(bool)));
  connect(chkBoxItalic, SIGNAL(clicked(bool)), this,
          SLOT(do_checkBoxItalic(bool)));
  connect(chkBoxBold, SIGNAL(clicked(bool)), this, SLOT(do_checkBoxBold(bool)));

  connect(btnOk, SIGNAL(clicked(bool)), this, SLOT(accept()));
  connect(btnCancel, SIGNAL(clicked(bool)), this, SLOT(reject()));
  connect(btnClose, SIGNAL(clicked(bool)), this, SLOT(close()));
}

void Dialog::do_checkBoxUnder(bool checked) {
  // 给文字添加下划线
  QFont font = txtEdit->font();
  font.setUnderline(checked);
  txtEdit->setFont(font);
}

void Dialog::do_checkBoxItalic(bool checked) {
  // 设置文字为斜体
  QFont font = txtEdit->font();
  font.setItalic(checked);
  txtEdit->setFont(font);
}

void Dialog::do_checkBoxBold(bool checked) {
  // 设置文字加粗
  QFont font = txtEdit->font();
  font.setBold(checked);
  txtEdit->setFont(font);
}

void Dialog::do_setFontColor() {
  // 自定义槽函数, 设置文字颜色
  QPalette plet = txtEdit->palette();
  if (radioBlue->isChecked()) {
    plet.setColor(QPalette::Text, Qt::blue);
  } else if (radioRed->isChecked()) {
    plet.setColor(QPalette::Text, Qt::red);
  } else {
    plet.setColor(QPalette::Text, Qt::black);
  }
  txtEdit->setPalette(plet);
}

Dialog::Dialog(QWidget *parent) : QDialog(parent) {
  this->initUi();
  this->initSignalSlots();
  this->setWindowTitle("手工创建UI");
}

Dialog::~Dialog() {}
```

### 三、`Qt`框架功能概述

#### 1、`Qt`基础模块

| 模块                | 功能                                                         |
| ------------------- | ------------------------------------------------------------ |
| `Qt Core`           | 这个模块是`Qt`框架的核心，定义了元对象系统对标准`C++`进行扩展 |
| `Qt GUI`            | 提供用于`GUI`设计的一些基础类，可用于窗口系统集成，事件处理、字体和文字处理等 |
| `Qt Network`        | 提供实现`TCP/IP`网络通信的一些类                             |
| `Qt Widgets`        | 提供用于创建`GUI`的各种界面组件类                            |
| `Qt D-Bus`          | `D-Bus`是实现进程间通信（`IPC`）和远程过程调用`RPC`的一种通信协议，这个模块提供实现`D-Bus`通信协议的一些类 |
| `Qt Test`           | 提供一些对应用程序和库进行单元测试的类                       |
| `Qt QML`            | 提供用`QML`编程的框架，它定义了`QML`和基础引擎               |
| `Qt Quick`          | 这个模块是用于开发`QML`应用程序的标准库，提供创建`UI`的一些基本类型 |
| `Qt Quick Controls` | 提供一套基于`Qt Quick`的控件，可用于创建复杂的`UI`           |
| `Qt Quick Dialogs`  | 提供通过`QML`使用系统对话框的功能                            |
| `Qt Quick Layouts`  | 提供用于管理界面布局的`QML`类型                              |
| `Qt Quick Test`     | 提供`QML`应用程序的单元测试框架                              |

- `Qt Core`模块是`Qt`框架的核心，其它模块都依赖此模块。

#### 2、`Qt`附加模块

| 模块                       | 功能                                                         |
| -------------------------- | ------------------------------------------------------------ |
| `Active Qt`                | 用于开发使用`ActiveX`和`COM`控件的`Windows`应用程序          |
| `Qt 3D`                    | 支持二维和三维图形渲染，用于开发近实时的仿真系统             |
| `Qt 5 Core Commpatibility` | 提供一些`Qt5`中有而`Qt6`中没有的`API`，这是为了兼容`Qt5`     |
| `Qt Bluetooth`             | 提供访问蓝牙硬件的功能                                       |
| `Qt Charts`                | 提供用于数据显示的一些二维图表组件                           |
| `Qt Concurrent`            | 提供一些类，使我们无需使用底层的线程控制就可以编写多线程应用程序 |
| `Qt Data Visualization`    | 提供一些用于三维数据可视化显示的一些类                       |
| `Qt Help`                  | 提供一些在应用程序中集成帮助文档的类                         |
| `Qt Image Formats`         | 支持附加图片格式的插件，格式包括`TIFF`、`MNG`和`TGA`等       |
| `Qt Multimedia`            | 提供处理多媒体内容的一些类，处理方式包括播放音频和视频，通过麦克风和摄像头录制音频和视频 |
| `Qt Network Authorization` | 使`Qt`应用程序能访问在线账号或`HTTP`服务，而又不暴露用户密码 |
| `Qt NFC`                   | 提供访问近场通信（`NFC`）硬件的功能                          |
| `Qt OpenGL`                | 提供一些便于在应用程序中使用`OpenGL`的类                     |
| `Qt Positioning`           | 通过`GPS`或`Wifi`定位，为应用程序提供定位信息                |
| `Qt Print Support`         | 提供一些用于打印控制的类                                     |
| `Qt Remote Objects`        | 提供一种进程间通信技术，可以在进程间或计算机之间方便地交换信息 |
| `Qt SCXML`                 | 用于通过`SCXML`（有限状态机规范）文件创建状态机              |
| `Qt Sensors`               | 提供访问传感器硬件的功能，传感器包括加速度计、陀螺仪等       |
| `Qt Serial Bus`            | 提供访问串行工业总线（如`CAN`和`Modbus`总线）的功能          |
| `Qt Serial Port`           | 提供访问兼容`RS232`引脚的串行接口的功能                      |
| `Qt Shader Tools`          | 提供用于三维图形着色器的工具                                 |
| `Qt SQL`                   | 提供一些使用`SQL`操作数据库的类                              |
| `Qt SVG`                   | 提供显示`SVG`图片文件的类                                    |
| `Qt UI Tools`              | 提供一些类，可以在程序运行时加载用`Qt Designer`设计的`UI`文件以动态创建`UI` |
| `Qt Virtual Keybord`       | 实现不同输入法的虚拟键盘                                     |
| `Qt Wayland Compositor`    | 实现了`Wayland`协议，能创建用户定制的显示服务                |
| `Qt WebChannel`            | 用于实现服务器端（`QML`或`C++`应用程序）与客户端`(HTML/JavaScript)`或`QML`应用程序进行`P2P`通信 |
| `Qt WebEngine`             | 提供一些类和函数，通过`Chromium`浏览器项目实现在应用程序中嵌入显示动态网页 |
| `Qt WebSockets`            | 提供`WebSocket`通信功能。                                    |

#### 3、`Qt`全局定义

- 头文件`<QtGlobal>`包含`Qt`框架中的一些全局定义，包括基本数据类型、函数和宏。一般的`Qt`类的头文件都会包含这个头文件。

##### `<QtGlobal>`中定义的数据类型

| `Qt`数据类型 | `POSIX`标准等效定义      | 字节数 |
| ------------ | ------------------------ | ------ |
| `qint8`      | `signed char`            | 1      |
| `qint16`     | `signed short`           | 2      |
| `qint32`     | `signed int`             | 4      |
| `qint64`     | `long long int`          | 8      |
| `qlonglong`  | `long long int`          | 8      |
| `quint8`     | `unsigned char`          | 1      |
| `quint16`    | `unsigned short`         | 2      |
| `quint32`    | `unsigned int`           | 4      |
| `quint64`    | `unsigned long long int` | 8      |
| `qulonglong` | `unsigned long long int` | 8      |
| `uchar`      | `unsigned char`          | 1      |
| `ushort`     | `unsigned short`         | 2      |
| `uint`       | `unsigned int`           | 4      |
| `ulong`      | `unsigned long`          | 8      |
| `qreal`      | `double`                 | 8      |
| `qsizetype`  | `ssize_t `               | 8      |
| `qfloat16`   | `—— `                    | 2      |

- `qreal`默认表示8字节`double`类型的浮点数，如果`Qt`使用`-qreal float`选项进行配置，就表示4字节`float`类型的浮点数。
- `qfloat16`是`Qt5.9`中新增的一种类型，用于表示16位的浮点数。要适用`qfloat16`，需要包含头文件`<QFloat16>`。

##### `<QtGlobal>`中常用的全局函数定义

| 函数原型                                                     | 功能                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `T qAbs(const T &value)`                                     | 返回变量`value`的绝对值                                      |
| `const T& qBound(const T &min, const T &value, const T &max)` | 返回`value`限定在`min~max`的值                               |
| `T qExchange(T &obj, U &&newValue)`                          | 将`obj`的值用`newValue`替换，返回`obj`的旧值                 |
| `int qFpClassify(double val)`                                | 返回`val`的分类， 包括`FP_NAN(非数)`、`FP_INFINIE(正或负的无穷大)`、`FP_ZERO(零)`等几种类型 |
| `bool qFuzzyCompare(double p1, double p2)`                   | 若`p1`和`p2`近似相等，返回`true`                             |
| `bool qFuzzyIsNull(double d)`                                | 若参数`d`约等于0，返回`true`                                 |
| `double qInf()`                                              | 返回无穷大的数                                               |
| `bool qIsFinite(double d)`                                   | 若`d`是一个有限的数，返回`true`                              |
| `bool qIsInf(double d)`                                      | 若`d`是一个无穷大的数，返回`true`                            |
| `bool qIsNaN(double d)`                                      | 若`d`为非数，返回`true`                                      |
| `const T & qMax(const T & value1, const T & value2)`         | 返回`value1`和`value2`中的较大值                             |
| `const T & qMin(const T & value1, const T & value2)`         | 返回`value1`和`value2`中的较小值                             |
| `qint64 qRound64(double value)`                              | 将`value`近似为最接近的`qint64`类型整数                      |
| `int qRound(double value)`                                   | 将`value`近似为最接近的`int`类型整数                         |

##### `<QtGlobal>`中的常用宏定义

- `QT_VERSION`：表示`Qt`版本，这个宏展开为数值形式`0xMMNNPP`
- `Q_BYTE_ORDER`表示系统内存中数据的字节序。`Q_BIG_ENDIAN`表示大端字节序，`Q_LITTLE_ENDIAN`表示小端字节序。
- `Q_DECL_IMPORT`和`Q_DECL_EXPORT`分别用于在使用或设计共享库时导入和导出库的内容。
- `Q_UNUSED(name)`用于声明函数中未被使用的参数。当函数的输入参数在函数对应的代码里未被使用时，需要使用这个宏声明，否则会出现编译警告
- `foreach(variable, container)`用于遍历容器的内容。
- `qDebug(const char *message, ...)`用于在`debugger`窗口显示信息。

#### 4、`Qt`的元对象系统

- `Qt`的元对象系统的功能建立在以下三个方面
  - `QObject`类是所有使用元对象系统的类的基类
  - 必须在一个类的开头部分插入宏`Q_OBJECT`，这样这个类才可以使用元对象系统的特性
  - `MOC`为每个`QObject`的子类提供必要的代码来实现元对象系统的特性

##### `QObject`类

- `QObject`类与元对象系统特性相关的函数（省略了函数前后的`const`关键字和入参）

<table>
  <tr>
    <th>特性</th>
    <th>函数</th>
    <th>功能</th>
  </tr>
  <tr>
    <td rowspan="2">元对象</td>
    <td>
<code>QMetaObject *metaObject()</code>
    </td>
 <td>
    返回这个对象的元对象
    </td>
  </tr>
  <tr>
    <td><code>QMetaObject staticMetaObject</code></td>
    <td>这是类的静态变量，不是函数，存储了类的元对象</td>
  </tr>
  <tr>
    <td>类型信息</td>
    <td><code>bool inherits()</code></td>
    <td>判断这个对象是不是某个类的子类的实例</td>
  </tr>
  <tr>
    <td>动态翻译</td>
    <td><code>QString tr()</code></td>
    <td>类的静态函数，返回一个字符串的翻译版本</td>
  </tr>
    <tr>
    <td rowspan="5">对象树</td>
    <td><code>QObjectList &amp;children()</code></td>
    <td>返回子对象列表</td>
  </tr>
    <tr>
    <td><code>QObject *parent()</code></td>
    <td>返回父对象指针</td>
  </tr>
    <tr>
    <td><code>void setParent()</code></td>
    <td>设置父对象</td>
  </tr>
    <tr>
    <td><code>T findChild()</code></td>
    <td>按照对象名称，查找可被转换为类型<code>T</code>的子对象</td>
  </tr>
    <tr>
    <td><code>QList&lt;T&gt; findChildren()</code></td>
    <td>返回符号名称和类型条件的子对象列表</td>
  </tr>
      <tr>
    <td rowspan="4">信号与槽</td>
    <td><code>QMetaObject::Connection connect()</code></td>
    <td>设置信号与槽关联</td>
  </tr>
      <tr>
    <td><code>bool disconnect()</code></td>
    <td>解除信号与槽的关联</td>
  </tr>
      <tr>
    <td><code>bool blockSignals()</code></td>
    <td>设置是否阻止对象发射任何信号</td>
  </tr>
      <tr>
    <td><code>bool signalsBlocked()</code></td>
    <td>若返回值为<code>true</code>表示对象被阻止发射信号</td>
  </tr>
        <tr>
    <td rowspan="3">属性系统</td>
    <td><code>QList&lt;QByteArray&gt; dynamicPropertyNames()</code></td>
    <td>返回所有动态属性名称</td>
  </tr>
        <tr>
    <td><code>bool setProperty()</code></td>
    <td>设置属性值, 或添加动态属性</td>
  </tr>
        <tr>
    <td><code>QVariant property()</code></td>
    <td>返回属性值</td>
  </tr>
</table>


- 元对象系统的特性是通过`QObject`的一些函数来实现的
  - 元对象。每个`QObject`及其子类的实例都有一个元对象，这个对象是自动创建的。静态变量`staticMetaObject`就是这个元对象，函数`metaObject()`返回这个元对象指针。
  - 类型信息。`QObject`的`inherits()`函数可以判断对象是不是从某个类继承的类的实例
  - 动态翻译。
  - 对象树。指表示对象间从属关系的树状结构。
  - 信号与槽。
  - 属性系统。在类的代码中可以用宏`Q_PROPERTY`定义属性。

##### `QMetaObject`类

- 元对象存储了类的实例所属类的各种元数据，包括类信息元数据、方法元数据、属性元数据等。所以，元对象实质上是对类的描述。
- 主要的接口函数（未全部列出入参，并且省略入参以及函数前后的`const`关键字）

<table>
    <tr>
    	<th>分组</th>
        <th>函数原型</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="5">类的信息</td>
        <td><code>char *className()</code></td>
        <td>返回这个类的类名称</td>
    </tr>
    <tr>
        <td><code>QMetaType metaType()</code></td>
        <td>返回这个元对象的元类型</td>
    </tr>
    <tr>
        <td><code>QMetaObject *superClass()</code></td>
        <td>返回这个类的上层父类的元对象</td>
    </tr>
    <tr>
        <td><code>bool inherits(QMetaObject *metaObject)</code></td>
        <td>返回<code>true</code>表示这个类继承自<code>metaObject</code>描述的类，否则返回<code>false</code></td>
    </tr>
    <tr>
        <td><code>QObject *newInstance(****)</code></td>
        <td>创建这个类的一个实例，可以给构造函数传递最多10个参数</td>
    </tr>
    <tr>
    	<td rowspan="4">类信息元数据</td>
        <td><code>QMetaClassInfo classInfo(int index)</code></td>
        <td>返回序号为<code>index</code>的一条类信息的元数据，类信息是在类中用宏<code>Q_CLASSINFO</code>定义的一条信息</td>
    </tr>
    <tr>
        <td><code>int indexOfClassInfo(char *name)</code></td>
        <td>返回名称为<code>name</code>的类信息序号，序号可用于<code>classInfo</code>函数</td>
    </tr>
    <tr>
        <td><code>int classInfoCount()</code></td>
        <td>返回这个类的类信息条数</td>
    </tr>
    <tr>
        <td><code>int classInfoOffset()</code></td>
        <td>返回这个类的第一条类信息的序号</td>
    </tr>
    <tr>
    	<td rowspan="2">构造函数元数据</td>
        <td><code>int constructorCount()</code></td>
        <td>返回这个类的构造函数的个数</td>
    </tr>
    <tr>
        <td><code>QMetaMethod constructor(int index)</code></td>
        <td>返回这个类的序号为<code>index</code>的构造函数的元数据</td>
    </tr>
    <tr>
    	<td rowspan="5">方法元数据</td>
        <td><code>int indexOfConstructor(char *constructor)</code></td>
        <td>返回一个构造函数的序号，<code>constructor</code>包括正则化之后的函数名和参数名</td>
    </tr>
    <tr>
        <td><code>QMetaMethod method(int index)</code></td>
        <td>返回序号为<code>index</code>的方法的元数据</td>
    </tr>
    <tr>
        <td><code>int methodCount()</code></td>
        <td>返回这个类的方法的个数，包括基类中定义的方法，方法包括一般的成员函数，还包括信号和槽</td>
    </tr>
    <tr>
        <td><code>int methodOffset()</code></td>
        <td>返回这个类的第一个方法的序号</td>
    </tr>
    <tr>
        <td><code>int indexOfMethod(char *method)</code></td>
        <td>返回名称为<code>method</code>的方法的序号</td>
    </tr>
    <tr>
    	<td rowspan="4">枚举类型元数据</td>
        <td><code>QMetaEnum enumerator(int index)</code></td>
        <td>返回序号为<code>index</code>的枚举类型的元数据</td>
    </tr>
    <tr>
        <td><code>int enumeratorCount()</code></td>
        <td>返回这个类的枚举类型个数</td>
    </tr>
    <tr>
        <td><code>int enumeratorOffset()</code></td>
        <td>返回这个类的第一个枚举类型的序号</td>
    </tr>
    <tr>
        <td><code>int indexOfEnumerator(char *name)</code></td>
        <td>返回名称为<code>name</code>的枚举类型的序号</td>
    </tr>
    <tr>
    	<td rowspan="4">属性元数据</td>
        <td><code>QMetaProperty property(int index)</code></td>
        <td>返回序号为<code>index</code>的属性的元数据</td>
    </tr>
    <tr>
        <td><code>int propertyCount()</code></td>
        <td>返回这个类的属性的个数</td>
    </tr>
    <tr>
        <td><code>int propertyOffset()</code></td>
        <td>返回这个类的第一个属性的序号</td>
    </tr>
    <tr>
        <td><code>int indexOfProperty(char *name)</code></td>
        <td>返回名称为<code>name</code>的属性的序号</td>
    </tr>
    <tr>
    	<td rowspan="2">信号与槽</td>
        <td><code>int indexOfSignal(char * signal)</code></td>
        <td>返回名称为为<code>signal</code>的信号的序号</td>
    </tr>
    <tr>
        <td><code>int indexOfSlot(char * slot)</code></td>
        <td>返回名称为<code>slot</code>的槽函数的序号</td>
    </tr>
    <tr>
    	<td rowspan="4">静态函数</td>
        <td><code>bool checkConnectArgs(****)</code></td>
        <td>检查信号与槽函数的参数是否兼容</td>
    </tr>
    <tr>
        <td><code>void connectSlotsByName(QObject *object)</code></td>
        <td>迭代搜索<code>object</code>的所有子对象，将匹配的信号和槽连接起来</td>
    </tr>
    <tr>
        <td><code>bool invokeMethod(****)</code></td>
        <td>运行<code>QObject</code>对象的某个方法，包括信号、槽或成员函数</td>
    </tr>
    <tr>
        <td><code>QByteArray normalizedSignature(char * method)</code></td>
        <td>将方法<code>method</code>的名称和参数字符串正则化，去除多余空格。函数返回的结果可用于<code>checkConnectArgs()</code>、<code>indexOfConstructor()</code>等函数</td>
    </tr>
</table>
- 函数`qobject_cast()`是头文件`<QObject>`中定义的一个非成员函数，对于`QObject`及其子类对象，可以使用函数`qobject_cast()`进行动态类型转换。如果自定义类要支持函数`qobject_cast()`，那么自定义类需要直接或间接继承`QObject`，且在类中插入宏`Q_OBJECT`。

#### 5、属性系统

##### 属性定义

- 属性是`Qt C++`的一个扩展的特性，是基于元对象系统实现的。在`QObject`的子类中，可以使用宏`Q_PROPERTY`定义属性，其格式如下：

```c++
Q_PROPERTY(type name
			(READ getFunction [WRITE setFunction] |
            	MEMBER memberName [(READ getFunction | WRITE setFunction)])
           	 [RESET resetFunction]
           	 [NOTIFY notifySignal]
             [REVISION int | REVISION(int [, int])]
           	 [DESIGNABLE bool]
           	 [SCRIPTABLE bool]
           	 [STORED bool]
           	 [USER bool]
           	 [BINDABLE bindableProperty]
           	 [CONSTANT]
           	 [FINAL]
           	 [REQUIRED])
```

- 宏`Q_PROPERTY`定义一个值类型为`type`，名称为`name`的属性，用`READ`、`WRITE`关键字分别定义属性的读取、写入函数，还有一些其他关键字用于定义属性的一些操作特性。属性值的类型可以是`QVariant`支持的任何类型，也可以是自定义类型。
- 主要关键字含义如下：
  - `READ`：指定一个读取属性值的函数，没有`MEMBER`关键字时必须设置`READ`
  - `WRITE`：指定一个设置属性值的函数，只读属性没有`WRITE`配置
  - `MEMBER`：指定一个成员变量与属性关联，使之成为可读可写的属性，指定后无需再设置`READ`和`WRITE`
  - `RESET`：是可选的，用于指定一个设置属性默认值的函数
  - `NOTIFY`：是可选的，用于设置一个信号，当属性值变化时发射此信号
  - `DESIGNABLE`：表示属性是否在`Qt Designer`的属性编辑器里可见，默认值为`true`
  - `USER`：表示这个属性是不是用户可编辑属性，默认为`false`，通常一个类只有一个`USER`设置为`true`的属性，例如`QAbstractButton`的`checked`属性
  - `CONSTANT`：表示属性值是一个常数，对于一个对象实例，`READ`指定的函数返回值是常数，但是每个实例的返回值可以不一样。具有`CONSTAT`关键字的属性不能有`WRITE`和`NOTIFY`关键字。
  - `FINAL`：表示所定义的属性不能被子类重载
- 示例：`Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled)`

##### 属性使用

- 读取属性值的函数名一般与属性名相同，设置属性值的函数一般在属性名前加`set`。
- 还可以通过`QObject::property()`函数通过属性名读取属性值，通过`QObject::setProperty()`函数设置属性值。
- 属性元数据用`QMetaProperty`来描述。

##### 动态属性

- 函数`QObject::setProperty()`设置属性值时，如果属性名称不存在，就会为对象定义一个新的属性并设置属性值，这时定义的属性称为动态属性。动态属性是针对类的实例定义的，所以只能用函数`QOject::property()`读取动态属性的属性值。

##### 附加的类信息

- 元对象系统还支持使用宏`Q_CLASSINFO()`在类中定义一些类信息。

#### 6、信号与槽

##### `connect()`函数的不同参数形式

- 函数`connect()`有一种成员函数形式，还有多种静态函数形式。一般使用静态函数形式。

```c++
    static QMetaObject::Connection connect(const QObject *sender, const char *signal,
                        const QObject *receiver, const char *member, Qt::ConnectionType = Qt::AutoConnection);
```

- 使用这种参数形式的`connect()`函数进行信号与槽的连接时，一般用法如下：`connect(sender, SIGNAL(signal()), receiver, SLOT(slot()));`

```c++
static QMetaObject::Connection connect(const QObject *sender, const QMetaMethod &signal,
                    const QObject *receiver, const QMetaMethod &method,
                    Qt::ConnectionType type = Qt::AutoConnection);
```

- 对于具有默认参数的信号，即信号名称是唯一的，不存在参数不同的其它同名信号，可以使用这种函数指针形式进行关联，例如：`connect(lineEdit, &QLineEdit::textChanged, this, &Widget::do_textChanged);`
- 对于`overload`型信号和`overload`型槽函数，可以使用模板函数`qOverload()`来明确参数类型。
- 还有一个作为`QObject`成员函数的`connect`：

```c++
inline QMetaObject::Connection connect(const QObject *sender, const char *signal,
                        const char *member, Qt::ConnectionType type = Qt::AutoConnection) const;
```

- 没有表示接受者的参数，接收者就是对象自身。

##### `Qt::ConnectionType`

- 不管是哪种参数形式的`connect()`函数，最后都有一个参数`type`，它是枚举类型`Qt::ConnectionType`，默认值为`Qt::AutoConnection`。枚举类型`Qt::ConnectionType`表示信号与槽的关联方式，有以下几种取值：
  - `Qt::AutoConnect`(默认值)：如果信号的接受者和发送者在同一个线程中，就使用`Qt::DirectConnect`方式，否则使用`Qt::QueuedConnect`方式，在信号发射时自动确定关联方式
  - `Qt::DirectConnect`：信号被发射时槽函数立即运行，槽函数与信号在同一个线程中。
  - `Qt::QueuedConnect`：在事件循环回到接收者线程后运行槽函数，槽函数与信号在不同的线程中
  - `Qt::BlockingQueuedConnection`：与`Qt::QueuedConnect`相似，区别是信号线程会阻塞，直到槽函数运行完毕。当信号与槽函数在同一个线程中时绝对不能使用这种方式，否则会造成死锁。

##### `disconnect()`函数的使用

- 用于解除信号与槽的连接，它有2种成员函数形式和4种静态函数形式。以下示例中`myObject`是发射信号的对象，`myReceiver`是接收信号的对象
- 解除与一个发射者所有信号的连接
  - `disconnect(myObject, nullptr, nullptr, nullptr); // 静态函数形式`
  - `myObject->disconnect(); // 成员函数形式`
- 解除与一个特定信号的所有连接
  - `disconnect(myObject, SIGNAL(mySignal()), nullptr, nullptr);`
  - `myObject->disconnect(SIGNAL(mySignal()));`
- 解除与一个特定接收者的所有连接
  - `disconnect(myObject, nullptr, myReceiver, nullptr);`
  - `myObject->disconnect(myReceiver);`
- 解除特定的一个信号与槽的连接，示例
  - `disconnect(lineEdit, &LineEdit::textChanged, label, &QLabel::setText);`

##### 使用函数`sender()`获取信号发射者

- `sender()`是`QObject`类的一个`protected`函数，在一个槽函数里调用函数`sender()`可以获取信号发射者的`QObject`对象指针。

##### 自定义信号及其使用

- 信号就是在类定义里声明的一个函数。（`signals`部分）
- 信号函数必须是无返回值的函数，但是可以有输入参数。信号函数无需实现，而只需要再某些条件下被发射。
- 使用`Qt C++`中的关键字`emit`发射信号。

#### 7、对象树

- 创建一个`QObject`对象时若设置一个父对象，他就会被添加到父对象的子对象列表里。一个父对象被删除时，其全部子对象就会被自动删除。
- 函数`findChild()`用于在对象的子对象中查找可以转换为类型`T`的子对象。其中`options`表示查找方式，默认值是`Qt::FindChildrenRecursively`表示在子对象中递归查找。若设置为`Qt::FinfDirectChildOnly`表示只查找直接子对象。
- 函数`findChildren()`用于在对象的子对象中查找可以转换为类型`T`的子对象，可以指定对象名称，还可以使用正则表达式来匹配对象名称。如果不设置要查找的名称，就返回所有能转换为类型`T`的对象。

#### 8、容器类

- `Qt`的容器类比标准模板库中的容器类更轻巧、使用更安全且更易于使用。这些容器类是隐式共享和可重入的，而且他们进行了速度和存储上的优化，因而可以减小可执行文件的大小。此外，他们是线程安全的，即它们作为只读容器时可被多个线程访问。
- `Qt`的容器类分为顺序容器类和关联容器类。

##### 顺序容器类

- `QList`
- `QVector`，在`Qt6`中是`QList`的别名
- `QStack`，是`QList`的子类
- `QQueue`, 是`QList`的子类

##### 关联容器类

- `QSet`
- `QMap`，按照键的顺序存储数据。键必须提供`<`运算符
- `QMultiMap`，支持一个键关联多个值，使用`value`获取最新插入的值，使用`values`获取一个键的所有值
- `QHash`，无序的`Map`，键必须提供`==`运算符和一个名为`qHash()`的全局哈希函数。
- `QMultiHash`，支持一个键关联多个值

#### 9、遍历容器的数据

- `Qt`提供两类迭代器：`STL`类型的迭代器和`Java`类型的迭代器。`STL`类型的迭代器效率更高，`Java`类型的迭代器是为了向后兼容。推荐使用`STL`类型的迭代器。
- 每一个容器类有两个`STL`类型的迭代器：一个用于只读访问`(const_iterator)`，另一个用于读写访问`(iterator)`。
- `STL`类型的迭代器是数组的指针，所以，`++`运算符表示迭代器指向下一个数据线，`*`运算符返回数据项内容。
- 要遍历返回的容器类，必须先复制，`Qt`使用了隐式共享，这样的复制不会产生太大开销。隐式共享是对象的管理方法。一个对象被隐式共享，意味着只传递该对象的一个指针给使用者，而不是实际复制对象数据，只有在使用者修改数据时，才会实际复制共享对象给使用者。隐式共享还涉及另外一个问题：即当有一个迭代器在操作一个容器变量时，不要复制这个容器变量。
- 如果只是想遍历容器中的所有项，可以使用宏`foreach`。`foreach`遍历容器时创建了容器的副本，所以不能修改原来容器中的数据项。（`C++11`及以上版本推荐使用`C++`的基于范围的循环）

#### 10、其它常用的基础类

##### `QVariant`类

- 是`Qt`中的一种万能数据类型，可以存储任何类型的数据。
- 一个`QVariant`变量在任何时候只能存储一个值，可以使用它的`toT`函数将数据转换为具体类型的数据，还可以使用`value()`返回某种类型的数据。例如：`QFont font = var.value<QFont>()`。

##### `QFlags`类

- `QFlags<Enum>`是一个模板类，用于定义枚举值或运算组合。

##### `QRandomGenerator`类

- `Qt6`中已经舍弃了在`Qt5`中产生随机数的函数`qrand()`和`qsrand()`，取而代之的是`QRandomGenerator`类，他可以产生高质量的随机数。可以给它的构造函数提供一个数作为随机数种子。`QRandomGenerator *rand = new QRandomGenerator(QDateTime::currentMSecsSinceEpoch());`
- `QRandomGenerator`有一个静态函数`securelySeeded()`可以创建一个随机数发生器。如果只是短期内使用随机数发生器，且生成的随机数的数据量比较小，可以使用静态函数`QRandomGenerator.global()`表示的全局的随机数发生器。
- `quint32 generate()`生成32位随机数
- `quint64 generate64()`生成64位随机数
- `double generateDouble()`生成`[0,1)`区间内的浮点数。
- `fillRange()`可以生成一组随机数，可将其填充到列表或数组里。
- `bounded()`可以生成指定范围内的随机数（左闭右开）。

### 四、常用界面组件的使用

- 在`Qt`中，所有界面组件类的直接或间接父类都是`QWidget`，`QWidget`的父类是`QObject`和`QPaintDevice`。其中`QPaintDevice`是能使用`QPainter`类在绘图设备上绘图的类。
- 所有从`QWidget`继承而来的界面组件被称为`widget`组件。界面组件可以接收鼠标事件、键盘事件或其他事件，然后在屏幕上绘制自己。

#### 1、常用的界面组件

##### 按钮类组件

- `QWidget`
  - `QAbstractButton`
    - `QPushButton`普通按钮
      - `QCommandLinkButton`
    - `QToolButton`工具按钮
    - `QRadioButton`单选按钮
    - `QCheckBox`复选框
  - `QDialogButtonBox`
- `QCommandLinkButton`的功能类似于`QRadioButton`，用于多个互斥项的选择。
- `QDialogButtonBox`是一个复合组件类，可以设置为多个按钮的组合。这个组件可以放在对话框上。

##### 输入类组件

- `QWidget`
  - `QComboBox`
    - `QFontComboBox`
  - `QLineEdit`
  - `QFrame`
    - `QAbstractScrollArea`
      - `QTextEdit`
      - `QPlainTextEdit`
  - `QAbstractSpinBox`
    - `QSpinBox`
    - `QDoubleSpinBox`
    - `QDateTimeEdit`
      - `QDateEdit`
      - `QTimeEdit`
  - `QAbstractSlider`
    - `QDial`
    - `QScrollBar`
    - `QSlider`
  - `QKeySequenceEdit`

| 组件类名称         | 组件名称       | 功能                                                         |
| ------------------ | -------------- | ------------------------------------------------------------ |
| `QComboBox`        | 下拉框列表     | 也称为组合框，用于从下拉列表中选择一项，也可直接输入文字     |
| `QFontComboBox`    | 字体下拉框列表 | 自动从系统获取字体名称列表，用于选择字体                     |
| `QLineEdit`        | 编辑框         | 用于输入单行文字                                             |
| `QTextEdit`        | 文本编辑器     | 是一个所见即所得的文本编辑器，支持富文本格式。使用类似`HTML`的标记，或`Markdown`格式。一般用于处理较大的富文本文档 |
| `QPlainTextEdit`   | 纯文本编辑器   | 是一个纯文本编辑器，每个字符都可以有自己的属性，例如字体和颜色 |
| `QSpinBox`         | 整数输入框     | 用于输入整数或离散型数据的输入框                             |
| `QDoubleSpinBox`   | 浮点数输入框   | 用于输入浮点数的输入框                                       |
| `QDateEdit`        | 日期编辑框     | 用于编辑日期数据的编辑框                                     |
| `QTimeEdit`        | 时间编辑框     | 用于编辑时间数据的编辑框                                     |
| `QDateTimeEdit`    | 日期时间编辑框 | 用于编辑日期时间数据的编辑框                                 |
| `QDial`            | 表盘           | 一种模仿表盘的输入组件，用于在设定范围内输入和显示数值       |
| `QScrollBar`       | 滚卷条         | 通常用于实现在大的显示区域内滑动，以显示部分区域的内容。     |
| `QSlider`          | 滑动条         | 具有设定的数值范围，拖动滑块就可以设置输入的值               |
| `QKeySequenceEdit` | 按键序列编辑器 | 当这个编辑器获得输入焦点后，可记录用户设置的按键序列，一般用这个编辑器获取用户设置的快捷键序列 |

##### 显示类组件

- `QWidget`
  - `QFrame`
    - `QLabel`
    - `QAbstractScrollArea`
      - `QTextEdit`
        - `QTextBrowser`
      - `QGraphicsView`
    - `QLCDNumber`
  - `QCalenderWidget`
  - `QProgressBar`
  - `QOpenGLWidget`
  - `QQuickWidget`

| 组件类名称        | 组件名称          | 功能                                                         |
| ----------------- | ----------------- | ------------------------------------------------------------ |
| `QLabel`          | 标签              | 用于显示文字、图片等内容                                     |
| `QTextBrowser`    | 文本浏览器        | 用于显示富文本格式的内容，具有只读属性，可以根据文本内的超链接进行跳转 |
| `QGraphicsView`   | 图形视图组件      | `Graphics View`结构中的视图组件                              |
| `QCalenderWidget` | 日历组件          | 用于显示日历，并显示所设置的日期，可显示整数和浮点数         |
| `QLCDNumber`      | `LCD`数值显示组件 | 模仿`LCD`显示效果的数值显示组件，可显示整数和浮点数          |
| `QProgressBar`    | 进度条            | 用于表示某个操作的进度，进度一般用百分数表示，有水平和垂直两种方向 |
| `QOpenGLWidget`   | `OpenGL`显示组件  | 用于在`Qt`应用程序中显示`OpenGL`图形                         |
| `QQuickWidget`    | `QML`显示组件     | 用于自动加载`QML`文件，并显示`QML`文件的场景                 |

##### 容器类组件

- 在容器类组件上可以放置其他组件，并可以使用布局管理容器内的子组件。
- `QWidget`
  - `QGroupBox`
  - `QFrame`
    - `QAbstractScrollArea`
      - `QScrollArea`
      - `QMidiArea`
    - `QToolBox`
    - `QStackedWidget`
  - `QTabWidget`
  - `QDockWidget`

| 组件类名称       | 组件名称           | 功能                                                         |
| ---------------- | ------------------ | ------------------------------------------------------------ |
| `QGroupBox`      | 分组框             | 具有标题和边框的容器组件                                     |
| `QScrollArea`    | 滚卷区域           | 具有水平和垂直滚卷条的容器组件，可以容纳大面积的显示内容，通过卷滚条可实现在现实范围内移动 |
| `QToolBox`       | 工具箱             | 垂直方向的多页容器组件，每个页面有标签栏，每个页面就是一个`QWidget`组件，在其上可以放置任何界面组件 |
| `QTabWidget`     | 带标签栏的多页组件 | 具有一个标签栏，每个页签对应一个页面，每个页面就是一个`QWidget`组件，在页面上可以放置任何界面组件 |
| `QStackedWidget` | 堆叠多页组件       | 类似于`QTabWidget`的多页组件，但是没有标签栏，只有两个按钮用于在页面之间切换 |
| `QFrame`         | 框架组件           | 所有具有边框的界面组件的父类，它定义了边框形状，边框阴影，边框线宽等属性。可以直接作为容器组件 |
| `QWidget`        | 界面组件           | 可以作为容器组件，没有父组件时就是独立的窗口                 |
| `QMidiArea`      | `MDI`工作区组件    | 是`MDI`显示区域，在`MDI`应用程序中，用于管理多文档窗口       |
| `QDockWidget`    | 停靠组件           | 是可以在`QMainWindow`窗口的上、下、左、右区域停靠的组件，也可以浮动在窗口上方。 |
| `QAxWidget`      | `ActiveX`显示组件  | 用于显示`ActiveX`控件，只有`Windows`平台上才有这个组件       |

##### `Item`组件

- `Item Views`分组的组件用于模型/视图结构，每一种视图组件需要相应的一种模型用于存储数据。
- `Item Widgets`组件类是相应`Item Views`组件类的子类，他们直接使用项`item`存储数据，称为相应视图的便利类。例如`QListWidget`是`QListView`的便利类。
- `QAbstractItemView`
  - `QListView`
    - `QUndoView`
    - `QListWidget`
  - `QTreeView`
    - `QTreeWidget`
  - `QTableView`
    - `QTableWidget`
  - `QColumnView`

##### 其它界面组件

- 还有一些组件没有出现在`Qt Designer`的组件面板里。例如常用的菜单栏`QMenuBar`，菜单`QMenu`，工具栏`QToolBat`，状态栏`QStatusBar`等组件，对应的几个类都是直接从`QWidget`继承而来的。

#### 2、`QWidget`类的主要属性和接口函数

##### `QWidget`作为界面组件时的属性

- 一个属性一般有一个读取函数和一个设置函数。

| 属性名称             | 属性值类型              | 功能                                                         |
| -------------------- | ----------------------- | ------------------------------------------------------------ |
| `enable`             | `bool`                  | 组件的使能状态，为`true`时才可以操作组件                     |
| `geometry`           | `QRect`                 | 组件的几何形状，表示组件在界面上所占的矩形区域               |
| `sizePolicy`         | `QSizePolicy`           | 组件的默认布局特性，这个特性与组件的水平、垂直方向尺寸变化有关系 |
| `minimumSize`        | `QSize`                 | 组件的最小尺寸，`QSize`包含`width`和`height`两个属性         |
| `maimumSize`         | `QSize`                 | 组件的最大尺寸                                               |
| `palette`            | `QPalette`              | 组件的调色板，调色板定义了组件一些特定部分的颜色，如背景色、文字颜色等 |
| `font`               | `QFont`                 | 组件使用的字体。`QFont`定义了字体名称、大小、粗体、斜体等特性 |
| `cursor`             | `QCursor`               | 鼠标光标移动到组件上时的形状                                 |
| `mouseTracking`      | `bool`                  | 若设置为`true`，只要鼠标在组件上移动，组件就接收鼠标移动事件；否则，只有在某个鼠标键被按下时，组件才接收鼠标移动事件 |
| `tableTracking`      | `bool`                  | 是否开启平板追踪，默认是`false`，表示只有当触笔与平板计算机接触时，组件才接收平板事件 |
| `focusPolicy`        | `Qt::FocusPolicy`       | 组件的焦点策略，表示组件获取焦点的方式                       |
| `contextMenuPolicy`  | `Qt::ContextMenuPolicy` | 组件的上下文菜单策略，上下文菜单是指在组件上点击鼠标右键时弹出的快捷菜单 |
| `acceptDrops`        | `bool`                  | 组件是否接收拖动来的其它对象                                 |
| `toolTip`            | `QString`               | 鼠标移动到组件上时，在光标处显示的简短提示文字               |
| `statusTip`          | `QString`               | 鼠标移动到组件上时，在主窗口状态栏上临时显示的提示文字，显示2秒后自动消失 |
| `autoFillBackground` | `bool`                  | 组件的背景是否自动填充，如果组件使用样式表设定了背景色，这个属性会被自动设置为`false` |
| `styleSheet`         | `QString`               | 组件的样式表。样式表用于定义界面显示效果。                   |

- `Horizontal Policy`表示组件在水平方向的尺寸变化策略，`Vertical Policy`表示组件在垂直方向的尺寸变化策略，其值都是枚举类型`QSizePolicy::Policy`：
  - `QSizePolicy::Fixed`：固定尺寸，`QWidget`的`sizeHint()`函数返回组件的建议尺寸作为组件的固定尺寸，即便使用了布局管理，组件也不会放大或缩小
  - `QSizePolicy::Minimum`：最小尺寸，组件缩小到最小尺寸后就不能再缩小。
  - `QSizePolicy::Maximum`：最大尺寸，组件放大到最大尺寸后就不能再放大。
  - `QSizePolicy::Preferred`：首选尺寸，使用`sizeHint()`函数的返回值作为最优尺寸，组件仍然可以缩放，但是放大时不会超过`sizeHint()`函数返回的尺寸。
  - `QSizePolicy::Expanding`：可扩展尺寸，`sizeHint()`函数的返回值是可变大小的尺寸，组件可扩展。
  - `QSizePolicy::MinimumExpanding`：最小可扩展尺寸，`sizeHint()`函数返回的是最小尺寸，组件可扩展。
  - `QSizePolicy::Ignored`：忽略尺寸，`sizeHint()`函数的返回值被忽略，组件占据尽可能大的空间
- 一般不需要修改组件的`sizePolicy`属性，使用其默认值即可。
- `Horizontal Stretch`和`Vertical Stretch`分别表示水平延展因子和垂直延展因子，都是整数值，取值范围`0~255`，默认是0，表示组件保持默认的宽度或高度。可以理解为所占区域占比。

##### `QWidget`作为窗口时的属性

| 属性             | 属性值类型           | 功能                                                         |
| ---------------- | -------------------- | ------------------------------------------------------------ |
| `windowTitle`    | `QString`            | 窗口标题栏上的文字，若要利用`windowModified`属性，需要在标题文字中设置占位符`"[*]"` |
| `windowIcon`     | `QIcon`              | 窗口标题栏上的图标                                           |
| `windowOpacity`  | `qreal`              | 窗口的不透明度，取值范围是`0.0~1.0`                          |
| `windowFilePath` | `QString`            | 窗口相关的含路径的文件名，这个属性只在`Windows`平台上有意义，如果没有设置`windowTitle`属性，程序将自动获取不含路径的文件名作为窗口标题 |
| `windowModified` | `bool`               | 表示窗口里的文档是否被修改，若该属性值为`true`，窗口标题中的占位符`"[*]"`会显示未`"*"` |
| `windowModality` | `Qt::WindowModality` | 窗口的模态，这个属性只在`Windows`平台上有意义，表示窗口是否处于上层 |
| `windowFlags`    | `Qt::WindowFlags`    | 窗口的标志，是枚举类型`Qt::WindowFlags`的一些值的组合        |

##### `QWidget`的其它接口函数

- 当它作为独立的窗口时，有如下一些与窗口现实有关的公有槽函数：
  - `bool close()`关闭窗口
  - `void hide()`隐藏窗口
  - `void show()`显示窗口
  - `void showFullScreen()`以全屏方式显示窗口
  - `void showMaximized()`窗口最大化
  - `void showMinimized()`窗口最小化
  - `void showNormal()`全屏、最大化或最小化操作之后，恢复正常大小显示
- `QWidget`中定义的信号只有三个：
  - `void customContextMenuRequested(const QPoint &pos)`信号在组件上点击鼠标右键被发射
  - `void windowIconChanged(const QIcon &icon)`
  - `void windowTitleChanged(const QString &title)`

#### 3、布局管理

##### 布局管理相关的类

- `QVBoxLayout`垂直布局
- `QHBoxLayout`水平布局
- `QGridLayout`网格布局
- `QFormLayout`表单布局，只适用于两列组件的布局
- `QStackedLayout`堆叠布局，用于管理多个`QWidget`类对象，也就是多个页面，但任何时候只有一个页面可见，和`QStackedWidget`相似，但没有切换页面的按钮，需要另外编程处理页面切换
- `QSplitter`分隔条
- `QObject`和`QLayoutItem`
  - `QLayout`
    - `QBoxLayout`
      - `QVBoxLayout`
      - `QHBoxLayout`
    - `QGridLayout`
    - `QFormLayout`
    - `QStackedLayout`
- `QLayoutItem`
  - `QSpacerItem`，用于在布局中占位，或填充剩余空间
- `QWidget`
  - `QFrame`
    - `QSplitter`

##### 布局可视化设计及其代码管理

- 使用容器组件
- 水平布局
  - 任何布局类对象在可视化设计时都有`layoutLeftMargin`、`layoutTopMargin`、`layoutRightMargin`、`layoutBottomMargin`这4个边距属性，用于设置布局组件与父容器的4个边距的最小值，单位是像素，水平布局和垂直布局还有一个属性`layoutSpacing`，表示组件的最小间距。
- 网格布局
  - 除了4个边距属性，还有几个特有的属性：
  - `layoutHorizontalSpacing`：水平方向上组件的最小间距
  - `layoutVerticalSpacing`：垂直方向上组件的最小间距
  - `layoutRowStretch`：各行的延展因子
  - `layoutColumnStretch`：各列的延展因子
  - `layoutColumnMinimumWidth`：各列的最小宽度，单位是像素，若值是0则表示自动设置
  - `layoutRowMinimumHeight`：各行的最小高度，单位是像素，若值是0则表示自动设置
  - `layoutSizeConstraint`：布局的尺寸限制方式，其值是枚举类型`QLayout::SizeConstraint`，默认设置是`QLayout::SetDefaultConstraint`，也就是将父组件的最小尺寸作为网格布局的最小尺寸
  - `QGridLayout`类添加组件的函数是`addWidget()`，其函数原型如下：
    - `void QGridLayout::addWidget(QWidget *widget, int formRow, int formColumn, int rowSpan, int columnSpan, Qt::Alignment alignment = Qt::Alignment())`
    - `rowSpan`和`columnSpan`表示组件占用的行数和列数，`alignment `表示默认对齐方式。
  - 设计网格布局的时候，网格可以空着，也可以使用水平或垂直间隔组件占位。
- 分隔条
  - 一般是在两个可以自由改变大小的组件之间进行分割。可以使用`QMainWindow::setCentralWidget()`函数将其设置为主窗口的中心组件，也就是填满主窗口的工作区。`QSplitter`布局组件有以下几个属性：
    - `orientation`：方向，即水平分割或垂直分割
    - `opaqueResize`：如果值是`true`，表示拖动分隔条时，组件是动态改变大小的
    - `handleWidth`：进行分割操作的拖动条的宽度，单位是像素
    - `childrenCollapsible`：表示进行分割操作时，子组件的大小是否可以变为0

#### 4、`QString`字符串操作

- `QString`存储的是一串字符，每个字符是一个`QChar`类型的数据。`QChar`使用的是`UTF-16`编码，一个字符包含2字节数据。对于超过`65535`的`Unicode`编码，`QString`使用两个连续的`QChar`字符表示。在`QString`中一个汉字是一个字符。
- `QString`使用隐式共享减少内存占用，也就是只有在修改一个字符串的时候，这个字符串才会被复制。
- 可以使用`[]`或`at()`访问每个字符

##### `QChar`的功能

| 函数原型                  | 功能                                                         |
| ------------------------- | ------------------------------------------------------------ |
| `bool isDigit()`          | 判断字符是否为`0~9`的数字                                    |
| `bool isLetter()`         | 判断字符是否为字母                                           |
| `bool isLetterOrNumber()` | 判断字符是否为字母或数字                                     |
| `bool isLower()`          | 判断字符是否为小写字母                                       |
| `bool isUpper()`          | 判断字符是否为大写字母                                       |
| `bool isMark()`           | 判断字符是否为记号                                           |
| `bool isNonCharacter()`   | 判断字符是否为非文字字符                                     |
| `bool isNull()`           | 判断字符编码是否为`0x0000`，也就是`"\0"`                     |
| `bool isNumber()`         | 判断字符是否为一个数，表示数的字符不仅包括数字`0~9`，还包括数字符号①、②等 |
| `bool isPrint()`          | 判断字符是否为可打印字符                                     |
| `bool isPunct()`          | 判断字符是否为标点符号                                       |
| `bool isSpace()`          | 判断字符是否为分隔符号，包括空格、制表符                     |
| `bool isSymbol()`         | 判断字符是否为符号，如特殊符号★、▲                           |
| `char toLatin1()`         | 返回与`QChar`字符等效的`Latin1`字符，如果无等效字符则返回0   |
| `QChar toLower()`         | 返回字符的小写形式字符，如果字符不是字母，返回其本身         |
| `QChar toUpper()`         | 返回字符的大写形式字符，如果字符不是字母，返回其本身         |
| `char16_t unicode()`      | 返回字符的16位`Unicode`编码数值                              |

- `QChar`字符和`Latin1`字符的转换
  - 只有当`QChar`的字符的编码为`0~255`时，函数`toLatin1`的转换才有意义。
  - `QChar`还有一个静态函数`QChar::fromLatin1()`将`Latin1`字符转换为`QChar`字符。
- `QChar`字符的`Unicode`编码
  - 可以通过静态函数`QChar::fromUcs2()`实现将`char16_t`类型的编码构造`QChar`字符。
  - 如果字符的`UTF-16`编码超过了`Latin1`编码的范围，也就是超过了255，就不能直接传递字符用于构造`QChar`对象。这是因为`Qt`的源程序文件采用的是`UTF-8`编码，源代码中的汉字是2字节的`UTF-8`编码，而`QChar`没有这种类型参数的构造函数。
- `QlineEdit`和`QPlainTextEdit`的接口函数的参数都是`QString`字符串，所以，即使是要在`QPlainTextEdit`组件中显示一个字符，也需要将这个字符转换为`QString`字符串。

```c++
#include "widget.h"

#include "ui_widget.h"

Widget::Widget(QWidget *parent) : QWidget(parent), ui(new Ui::Widget) {
  ui->setupUi(this);
}

Widget::~Widget() { delete ui; }

void Widget::on_pushButton_1_clicked() {
  QString str = ui->lineEdit_1->text();
  // 读取输入的字符串
  if (str.isEmpty()) {
    return;
  }
  ui->plainTextEdit->clear();
  for (qint16 i = 0; i < str.size(); i++) {
    QChar ch = str[i];
    char16_t unicode = ch.unicode();
    QString chStr(ch);
    QString info = chStr + QString::asprintf("\t, Unicode编码 = 0x%X", unicode);
    ui->plainTextEdit->appendPlainText(info);
  }
}

void Widget::on_pushButton_2_clicked() {
  QString str = ui->lineEdit_2->text();
  if (str.isEmpty()) {
    return;
  }
  QChar ch = str[0];
  char16_t unicode = ch.unicode();
  QString chStr(ch);
  QString info = chStr + QString::asprintf("\t, Unicode编码 = 0x%X\n", unicode);
  ui->plainTextEdit->appendPlainText(info);
  // 判断特性
  ui->checkBox_1->setChecked(ch.isDigit());
  ui->checkBox_2->setChecked(ch.isLetter());
  ui->checkBox_5->setChecked(ch.isLetterOrNumber());
  ui->checkBox_6->setChecked(ch.isUpper());
  ui->checkBox_11->setChecked(ch.isLower());
  ui->checkBox_13->setChecked(ch.isMark());
  ui->checkBox_14->setChecked(ch.isSymbol());
  ui->checkBox_15->setChecked(ch.isPunct());
}

void Widget::on_pushButton_3_clicked() {
  QString str = "Dimple";
  ui->plainTextEdit->clear();
  ui->plainTextEdit->appendPlainText(str);
  QChar chp = QChar::fromLatin1('P');
  str[0] = chp;
  ui->plainTextEdit->appendPlainText("\n" + str);
}

void Widget::on_pushButton_5_clicked() {
  QString str = "Hello, 武汉";
  ui->plainTextEdit->clear();
  ui->plainTextEdit->appendPlainText(str);
  str[7] = QChar(0x5317);
  str[8] = QChar(0x4EAC);
  ui->plainTextEdit->appendPlainText("\n" + str);
}

void Widget::on_pushButton_4_clicked() {
  QString str = "他们来自于湖南或湖北";
  ui->plainTextEdit->clear();
  ui->plainTextEdit->appendPlainText(str);
  QString huStr = "河湖";
  QChar he = QChar::fromUcs2(huStr[0].unicode());
  QChar hu = QChar::fromUcs2(huStr[1].unicode());
  for (qint16 i = 0; i < str.size(); i++) {
    if (str[i] == hu) {
      str[i] = he;
    }
  }
  ui->plainTextEdit->appendPlainText("\n" + str);
}
```

##### `QString`字符串常用操作

- 字符串拼接
  - 使用加号运算符
  - 使用`append()`
  - 使用`prepend`
- 字符串截取
  - `front()`返回第一个字符，`back()`返回最后一个字符
  - `left()`从字符串中提取左边`n`个字符。`right()`从字符串中提取右边`n`个字符
  - `first()`从字符串中提取最前面的`n`个字符。`last()`从字符串中提取右边`n`个字符。他们和`left()`和`right()`功能相同，但速度更快
  - `mid()`返回字符串中的部分字符串。原型：`QString QString::mid(qsizetype pos, qsizetype n = -1)`。如果`pos + n`超过了字符串的边界，返回`null`
  - `sliced`与`mid()`功能相同。如果在边界内，`sliced()`执行速度更快，超出边界行为不确定。
  - `section()`，原型：`QString QString::section(const QString &sep, qsizetype start, qsizetype end = -1, QString::SectionFlags flags = SectionDefault)`，其功能是从字符串中以`sep`作为分隔符，从`start`段到`end`段的字符串。
- 存储相关的函数
  - `isNull()`和`isEmpty()`都会判断字符串是否为空。如果只有一个空字符串(`\0`)，`isNull()`返回`false`，`isEmpty()`返回`true`，只有未被赋值时，`isNull()`才返回`true`
  - `count()`、`size()`和`length()`。函数`size()`和`length()`都返回字符串中的字符个数，功能相同。不带任何参数的`count()`与这两个函数功能相同，带参数可统计某个字符串在当前字符串中出现的次数。
  - `clear()`，清空当前字符串，使之为`null`
  - `resize()`改变字符串长度。如果`size`等于当前字符串长度，就扩充字符串，但新增的字符是不确定的。如果`size`小于字符串当前长度，字符串会缩短为`size`个字符，多余的字符丢失。可用于预分配字符串的长度，也可以在字符串内容初始化时用给定的字符进行填充。
  - `fill()`将字符串中的每个字符都用一个新字符替换，且可以改变字符串长度
- 搜索和判断
  - `indexOf()`和`lastIndexOf()`。在当前字符串中查找某个字符串首次出现的位置。可指定是否区分大小写。（`Qt::CaseInsensitive`表示不区分大小写，`Qt::CaseSensitive`表示区分大小写）
  - `contains()`判断当前字符串是否包含某个字符串，可指定是否区分大小写。
  - `endsWith()`和`startsWith()`判断是否以某个字符串开头或结尾，可指定是否区分大小写。
  - `count()`统计当前字符串中某个字符串出现的次数，可指定是否区分大小写。
- 字符串转换和修改
  - `toUpper()`和`toLower()`
  - `trimmed()`去掉字符串首尾的空格，`simplified()`不仅会去掉字符串首尾的空格，还会将中间连续的空格用单个空格替换。
  - `chop()`去掉字符串末尾的`n`个字符，如果`n`大于或等于字符串实际长度，字符串内容就变为空。
  - `insert()`在字符串中的某个位置插入一个字符串，它修改当前字符串的内容，并返回字符串对象的引用。如果插入位置大于字符串长度，字符串会自动补充空格扩充长度。
  - `replace()`从字符串的`pos`未知开始替换`n`哥字符，替换后的字符串是`after`。
  - `remove()`从字符串的`pos`未知开始移除`n`个字符。如果超出了字符串长度，就把`pos`后面的字符都移除。

##### 字符串和数值的转换

- 字符串转换为整数，如果`ok`不为`null`，就作为转换结果是否成功的返回变量。`base`的范围是`2_36`，还可以设置为0，表示使用`C`语言的表示法。如果转换失败，返回值是0。
  - `int toInt(bool *ok = nullptr, int base = 10)`
  - `uint toUInt(bool *ok = nullptr, int base = 10)`
  - `long toLong(bool *ok = nullptr, int base = 10)`
  - `ulong toULong(bool *ok = nullptr, int base = 10)`
  - `short toShort(bool *ok = nullptr, int base = 10)`
  - `ushort toUShort(bool *ok = nullptr, int base = 10)`
  - `qlonglong toLongLong(bool *ok = nullptr, int base = 10)`
  - `qulonglong toULongLong(bool *ok = nullptr, int base = 10)`
- 字符串转换为浮点数
  - `float toFloat(bool *ok = nullptr)`
  - `double toDouble(bool *ok = nullptr)`
- `setNum()`用于将整数或浮点数转换为字符串。
  - 对于整数类型，可指定转换后的进制。
  - 对于浮点数，`format`是格式字符，`precision`表示精度位数。

| 格式字符 | 格式字符的含义               | 精度位数的含义           |
| -------- | ---------------------------- | ------------------------ |
| `e`      | 科学计数法，用小写字母`e`    | 基数的小数点后的有效位数 |
| `E`      | 科学计数法，用大写字母`E`    | 基数的小数点后的有效位数 |
| `f`      | 自然计数法                   | 小数点后的有效位数       |
| `g`      | 使用`e`或`f`，哪种简洁用哪种 | 小数点前后的数字位数之和 |
| `G`      | 使用`E`或`f`，哪种简洁用哪种 | 小数点前后的数字位数之和 |

- 静态函数`number()`，参数形式和功能与成员函数`setNum()`相似。
- 静态函数`asprintf()`，用于构造格式化输出各种数据的字符串，类似于标准`C`语言中的函数`printf()`。注意格式化字符串中支持汉字，但是替换格式化字符串中的`%s`只能用`UTF-8`编码的字符串，也就是变量的字符串中不能有汉字。
- 函数`arg()`，是成员函数，用于格式化输出各种数据的字符串，其功能与静态函数`asprintf()`类似，使用起来更加灵活。其中占位符使用类似`%1`、`%2`这种形式。
  - `QString arg(int a, int fieldWidth = 0, int base = 10, QChar fillChar = QLatinChar(' '))`，其中`fieldWidth `是转换成的字符串占用的最少空格数，`base`是转换成的字符串显示进制，`fillChar`是当`fieldWidth`大于实际数位宽度时使用的填充字符，默认为空格。
  - 格式字符串中的占位符出现的顺序可以打乱，甚至可以重复出现。

```c++
#include "widget.h"

#include "ui_widget.h"

Widget::Widget(QWidget *parent) : QWidget(parent), ui(new Ui::Widget) {
  ui->setupUi(this);
}

Widget::~Widget() { delete ui; }

void Widget::on_pushButton_2_clicked() {
  QString str1 = ui->lineEdit->text();
  int quantity = str1.toInt();
  QString str2 = ui->lineEdit_2->text();
  double price = str2.toDouble();
  QString str3;
  str3.setNum(quantity * price, 'f', 2);
  ui->lineEdit_3->setText(str3);
}

void Widget::on_pushButton_3_clicked() {
  int value = ui->lineEdit_4->text().toInt();
  QString str = QString::number(value, 16);
  str = str.toUpper();
  ui->lineEdit_6->setText(str);
  str = QString::number(value, 2);
  ui->lineEdit_5->setText(str);
}

void Widget::on_pushButton_4_clicked() {
  int value = ui->lineEdit_5->text().toInt(nullptr, 2);
  QString str = QString::number(value, 16);
  str = str.toUpper();
  ui->lineEdit_6->setText(str);
  str = QString::number(value, 10);
  ui->lineEdit_4->setText(str);
}

void Widget::on_pushButton_5_clicked() {
  int value = ui->lineEdit_6->text().toInt(nullptr, 16);
  QString str = QString::number(value, 10);
  ui->lineEdit_4->setText(str);
  str = QString::number(value, 2);
  ui->lineEdit_5->setText(str);
}

void Widget::on_pushButton_clicked() {
  QString str;
  double num = 1245.2783;
  qDebug("num = %f", num);
  str.setNum(num, 'f', 5);
  qDebug("str = %s", str.toLocal8Bit().data());
  str.setNum(num, 'g', 5);
  qDebug("str = %s", str.toLocal8Bit().data());
  str.setNum(num, 'g', 3);
  qDebug("str = %s", str.toLocal8Bit().data());
}
```

- 如果要生成项目的发布版本，只需要在项目的配置文件`(.pro)`中增加：`DEFINES += QT_NO_DEBUG_OUTPUT`

#### 5、`QSpinBox`和`QDoubleSpinBox`

##### 属性和接口函数

- 他们都是`QAbstractSpinBox`的子类，具有很多相同的属性，只是参数类型不同。

| 属性名称             | 功能                                                |
| -------------------- | --------------------------------------------------- |
| `prefix`             | 数字显示的前缀                                      |
| `suffix`             | 数字显示的后缀                                      |
| `buttonSymbols`      | 编辑框右侧调节按钮的符号，可以设置不显示调节按钮    |
| `text`               | 只读属性，`SpinBox`里显示的全部文字，包括前缀和后缀 |
| `cleanText`          | 只读属性，不带前缀和后缀且去除了前后空格的文字      |
| `minimum`            | 数值范围的最小值                                    |
| `maximum`            | 数值范围的最大值                                    |
| `singleStep`         | 点击编辑框右侧上下调节按钮时的单步改变值            |
| `stepType`           | 步长类型，单一步长或自适应步长                      |
| `value`              | 当前显示的值                                        |
| `displayIntegerBase` | `QSpinBox`特有属性，显示整数使用的进制              |
| `decimals`           | `QDoubleSpinBox`特有属性，显示数值的小数位数        |

- 从`Qt 5.12`开始，`QSpinBox`和`QDoubleSpinBox`新增了一个`stepType`属性，表示步长变化的方式，属性值是枚举类型`QAbstractSpinBox::StepType`：
  - `QAbstractSpinBox::DefaultStep`默认步长，也就是使用属性`singleStep`设置的固定步长
  - `QAbstractSpinBox::AdaptiveDecimalStepType`自适应十进制步长，表示将自动连续调整步长值为`10`<sup>n</sup>，其中`n`为大于或等于0的整数。
- `QSpinBox`有两个特有的信号
  - `void QSpinBox::valueChanged(int i)`，在`value`属性值变化时被发射，传递的参数`i`是变化后的数值
  - `void QSpinBox::textChanged(const QString &text)`，在显示的文字发生变化时被发射。

#### 6、常用的按钮组件

##### 按钮的属性

- `QAbstractButton`是抽象类，但它是几个按钮类的父类。主要属性如下：

| 属性            | 属性值类型     | 功能                                                         |
| --------------- | -------------- | ------------------------------------------------------------ |
| `text`          | `QString`      | 按钮的显示文字                                               |
| `icon`          | `QIcon`        | 按钮的图标                                                   |
| `shortcut`      | `QKeySequence` | 按钮的快捷键                                                 |
| `checkable`     | `bool`         | 按钮是否可复选                                               |
| `checked`       | `bool`         | 按钮是否复选的状态                                           |
| `autoExclusive` | `bool`         | 在一个布局或一个容器组件内的同类按钮是否是互斥的。如果是互斥的，当其中一个按钮的`checked`属性被设置为`true`时，其它按钮的`checked`属性被自动设置为`false` |
| `autoRepeat`    | `bool`         | 是否自动重复。如果值为`true`，那么在按钮处于按下状态时，将自动重复发射`clicked()`、`pressed()`、`released()`信号。初次重复的延迟时间由属性`autoRepeatDelay`决定，重复的周期由属性`autoRepeatInterval`决定，时间单位都是毫秒 |

- `QPushButton`还新增了以下属性：

| 属性          | 属性值类型 | 功能                                                         |
| ------------- | ---------- | ------------------------------------------------------------ |
| `autoDefault` | `bool`     | 按钮是否为自动默认按钮                                       |
| `default`     | `bool`     | 按钮是否为默认按钮                                           |
| `flat`        | `bool`     | 当`flat`属性为`true`时，按钮没有边框，只有被点击或复选时才显示按钮边框。 |

- 只有当按钮所在的窗口基类是`QDialog`时，`autoDefault`和`default`属性才有意义。在对话框上，如果一个按钮的`default`属性为`true`，他就是默认按钮，按下<kbd>Enter</kbd>键就相当于点击了默认按钮。如果一个按钮的`autoDefault`属性为`true`，当他获得焦点时，他就会变成默认按钮。
- `QCheckBox`增加了一个`tristate`属性，表示是否允许有3种复选状态，即除了`Checked`和`Unchecked`，还有`PartiallyChecked`。

##### 按钮的信号

- `void clicked(bool checked = false)`点击按钮时
- `void pressed()`按下<kbd>Space</kbd>键或鼠标左键时
- `void released()`释放<kbd>Space</kbd>键或鼠标左键时
- `void toggled(bool checked)`按钮的`checked`属性值变化时
- `QCheckBox`定义了一个新信号：`void QCheckBox::stateChanged(int state)`当复选框状态变化时。

#### 7、`QSlider`和`QProgressBar`

##### `QAbstractSlider`类的属性

- `QAbstractSlider`是`QSlider`、`QScrollBar`和`QDial`的父类，主要属性如下：

| 属性                 | 属性值类型        | 功能                                                         |
| -------------------- | ----------------- | ------------------------------------------------------------ |
| `minimum`            | `int`             | 数据范围的最小值，默认是0                                    |
| `maximum`            | `int`             | 数据范围的最大值，默认是99                                   |
| `singleStep`         | `int`             | 拖动滑块条上的滑块，或按下卷滚条两端的按钮时变化的最小数值，默认是1 |
| `pageStep`           | `int`             | 输入焦点在组件上时，按下<kbd>PgUp</kbd>或<kbd>PgDn</kbd>键时变化的数值，默认是10 |
| `value`              | `int`             | 组件的当前值，拖动滑块时自动改变此值                         |
| `sliderPosition`     | `int`             | 滑块的位置，若`tracking`属性值为`true`，`sliderPosition`值就等于`value`值 |
| `tracking`           | `bool`            | 如果设置为`true`，改变`value`值同时会改变`sliderPosition`值  |
| `orientation`        | `Qt::Orientation` | 滑动条或卷滚条的方向，水平或垂直                             |
| `invertedAppearance` | `bool`            | 显示方式是否反向，默认值是`false`，水平的滑动条或卷滚条由左向右数值增大 |
| `invertedControls`   | `bool`            | 反向按键控制，若设置为`true`，按下<kbd>PgUp</kbd>或<kbd>PgDn</kbd>时调整数值的方向相反 |

##### `QAbstractSlider`类的信号

- `void actionTriggered(int action)`滑动条触发一些动作时，`action`表示动作的类型，用枚举值`QAbstractSlider::SliderAction`的值表示。如`SliderToMinimum`表示滑动到最小值
- `void rangeChanged(int min, int max)`，`minimum`或`maximum`属性值发生变化时
- `void sliderMoved(int value)`用户按住鼠标拖动滑块时
- `void sliderPressed()`在滑块上按下鼠标时
- `void sliderReleased()`在滑块上释放鼠标时
- `void valueChanged(int value)`，`value`属性值变化时

##### `QSlider`类

- 一般用作滑动输入数值数据的组件，`QSlider`类新定义的属性有两个
  - `tickPosition`：标尺刻度的显示位置，属性值是枚举类型`QSlider::TickPosition`
  - `tickInterval`：标尺刻度的间隔值，如果设置为0，会在`singleStep`和`pageStep`之间自动选择。

##### `QScrollBar`类

- 一般与文本编辑器或容器组件组合使用，以便在一个大的显示范围内移动。

##### `QDial`类

- 表示表盘式的组件，通过旋转表盘获得输入值（`QAbstractSlider`定义的一些属性对`QDial`没有影响，例如`orientation`属性）。定义了三个新的属性：
  - `notchesVisible`：表盘外围的小刻度是否可见
  - `notchTarget`：表盘刻度间的间隔像素值
  - `wrapping`：表盘上首尾刻度是否连贯。如果设置为`false`，表盘的最小值和最大刻度之间有一定空间，否则，表盘刻度是一整圈连续的。默认是`false`

##### `QProgressBar`类

- 表示进度条组件，一般以百分比数据来显示进度。父类是`QWidget`，和`QAbstractSlider`类没有继承关系。
  - `textDirection`：文字的方向，这表示垂直进度条的文字的阅读方向，包括从上往下和从下往上两种选项。这个属性对于水平进度条无意义。
  - `format`：显示文字的格式，`%p%`显示百分比，`%v`显示当前值，`%m`显示总步数。默认`%p%`
- 还有两个与属性读写无关的函数
  - `void QProgressBar::setRange(int minimum, int maximum)`设置范围
  - `void QProgressBar::reset()`将进度条复位，也就是使进度条为0

#### 8、日期时间数据

- `QTime`表示时间数据的类
- `QDate`表示日期数据的类
- `QDateTime`表示日期时间的类
- 这三个类都没有父类，他们只用于存储日期时间数据，并定义接口函数用于数据处理。
- `QTimeEdit`：编辑和显示时间的组件类
- `QDateEdit`：编辑和显示日期的组件类
- `QDateTimeEdit`：编辑和显示日期时间的组件类
- `QCalendarWidget`：一个用日历形式显示和选择日期的组件类

##### `QTime`类

- 时间数据包括小时、分钟、秒、毫秒。总是使用24小时制，不区分`AM/PM`。其中一种构造函数可以初始化时间数据：`QTime::QTime(int h, int m, int s = 0, int ms = 0)`
- 还可以使用静态函数`QTime::currentTime()`创建一个`QTime`对象，并且将其初始化为系统当前时间。
- 主要接口函数如下（“当前时间”指的是`QTime`对象所表示的时间）：

| 函数原型                                       | 功能                                                         |
| ---------------------------------------------- | ------------------------------------------------------------ |
| `int hour()`                                   | 返回当前时间的小时数据                                       |
| `int minute()`                                 | 返回当前时间的分钟数据                                       |
| `int second()`                                 | 返回当前时间的秒数据                                         |
| `int msec()`                                   | 返回当前时间的毫秒数据                                       |
| `bool setHMS(int h, int m, int s, int ms = 0)` | 设置时间的小时、分钟、秒、毫秒数据                           |
| `int msecsSinceStartOfDay()`                   | 返回从时间`00:00:00`开始的毫秒数                             |
| `QTime addSecs(int s)`                         | 当前时间延后`s`秒之后的时间。`s`为正表示延后，`s`为负表示提前 |
| `int secsTo(QTime t)`                          | 返回当前时间与一个`QTime`对象`t`相差的秒数                   |
| `QString toString(const QString &format)`      | 将当前时间按`format`设置的格式转换为字符串                   |

```c++
// 初始化一个时间
QTime tm1(15, 2, 14);
QString str = tm1.toString("HH:mm:ss");
qDebug("Original time = %s", str.toLocal8Bit().data());
// 延后150秒
QTime tm2 = tm1.addSecs(150);
str = tm2.toString("HH:mm:ss");
qDebug("150s later, time = %s", str.toLocal8Bit().data());
// 获取当前时间
tm2 = QTime::currentTime();
str = tm2.toString("HH:mm:ss");
qDebug("Current time = %s", str.toLocal8Bit().data());
qDebug("Hour = %d", tm2.hour());
qDebug("Minute = %d", tm2.minute());
qDebug("Second = %d", tm2.second());
qDebug("MSecond = %d", tm2.msec());
```

##### `QDate`类

- 日期数据包含年、月、日。可以使用静态函数`QDate::currentDate()`获取系统当前日期。
- 主要接口函数（"当前日期"指的是`QDate`变量所表示的日期）：

| 函数原型                                        | 功能                                                         |
| ----------------------------------------------- | ------------------------------------------------------------ |
| `int year()`                                    | 返回当前日期的年数据                                         |
| `int month()`                                   | 返回当前日期的月数据，数值为1~12                             |
| `int day()`                                     | 返回当前日期的日数据，数值为1~31                             |
| `int dayOfWeek()`                               | 返回当前日期是星期几，数字1表示星期一，数字7表示星期天       |
| `int dayOfYear()`                               | 返回当前日期在一年中是第几天，数字1表示第一天                |
| `bool setDate(int year, int month, int day)`    | 设置容器的年、月、日数据                                     |
| `void getDate(int *year, int *month, int *day)` | 通过指针变量，返回当前日期的年、月、日数据                   |
| `QDate addYears(int nyears)`                    | 返回一个`QDate`变量，其日期是在当前日期基础上加`nyears`年    |
| `QDate addMonths(int nmonths)`                  | 返回一个`QDate`变量，其日期是在当前日期基础上加`nmonths`月   |
| `QDate addDays(qint64 ndays)`                   | 返回一个`QDate`变量，其日期是在当前日期基础上加`ndays`日     |
| `qint64 daysTo(QDate d)`                        | 返回当前日期与一个`QDate`变量`d`相差的天数。如果`d`值早于当前日期，返回值为负 |
| `QString toString(const QString &format)`       | 将当前日期按照`format`设置的格式转换为字符串                 |

- `QDate`还有一个静态函数`isLeapYear()`可以判断某年是否为闰年：`bool QDate::isLeapYear(int year)`

```c++
// 初始化一个日期
QDate dt1(2024, 8, 22);
QString str = dt1.toString("yyyy-MM-dd");
qDebug("dt1 = %s", str.toLocal8Bit().data());
QDate dt2;
// 设置日期
dt2.setDate(2024, 9, 10);
str = dt2.toString("yyyy-MM-dd");
qDebug("dt2 = %s", str.toLocal8Bit().data());
// dt2和dt1相差几天
qDebug("Days between dt2 and dt1 = %lld", dt2.daysTo(dt1));
// 获取当前日期
dt2 = QDate::currentDate();
str = dt2.toString("yyyy-MM-dd");
qDebug("Current date = %s", str.toLocal8Bit().data());
qDebug("Year = %d", dt2.year());
qDebug("Month = %d", dt2.month());
qDebug("Day = %d", dt2.day());
qDebug("Day of week = %d", dt2.dayOfWeek());
qDebug("Day of year = %d", dt2.dayOfYear());
qDebug("2024 is leapYear = %s", QDate::isLeapYear(2024) ? "是" : "否");
```

##### `QDateTime`类

- 包含日期数据和时间数据，综合了日期和时间的操作。特有的函数如下：

| 函数原型                                  | 功能                                                         |
| ----------------------------------------- | ------------------------------------------------------------ |
| `QDate date()`                            | 返回当前日期时间数据的日期数据                               |
| `QTime time()`                            | 返回当前日期时间数据的时间数据                               |
| `qint64 toMSecsSinceEpoch()`              | 返回与`UTC`时间`1970-01-01T00:00:00.000`相差的毫秒数         |
| `void setMSecsSinceEpoch(qint64 msecs)`   | 设置与`UTC`时间`1970-01-01T00:00:00.000`相差的毫秒数`msecs`作为当前日期时间数据 |
| `qint64 toSecsSinceEpoch()`               | 返回与`UTC`时间`1970-01-01T00:00:00.000`相差的秒数           |
| `void setSecsSinceEpoch(qint64 secs)`     | 设置与`UTC`时间`1970-01-01T00:00:00.000`相差的秒数`secs`作为当前的日期时间数据 |
| `QString toString(const QString &format)` | 将当前日期时间按`format`设置的格式转换为字符串               |
| `QDateTime toUTC()`                       | 将当前时间转换为`UTC`时间                                    |

- `QDateTime`有两个静态函数可以返回系统当前时间：
  - `QDateTime QDateTime::currentDateTime()`返回系统的当前日期时间，本地时间
  - `QDateTime QDateTime::currentDateTimeUtc()`返回系统的当前日期时间，`UTC`时间

```c++
// 系统当前时间
QDateTime dt1 = QDateTime::currentDateTime();
QString str = dt1.toString("yyyy-MM-dd HH:mm:ss");
qDebug("dt1 = %s", str.toLocal8Bit().data());
// 日期部分
QDate dt = dt1.date();
str = dt.toString("yyyy-MM-dd");
qDebug("dt1.date() = %s", str.toLocal8Bit().data());
// 时间部分
QTime tm = dt1.time();
str = tm.toString("HH:mm:ss zzz");
qDebug("dt1.time() = %s", str.toLocal8Bit().data());
// 转换为秒数
qint64 ms = dt1.toSecsSinceEpoch();
qDebug("dt1.toSecsSinceEpoch() = %lld", ms);
// 加120秒
ms += 120;
dt1.setSecsSinceEpoch(ms);
str = dt1.toString("yyyy-MM-dd HH:mm:ss");
qDebug("dt1 + 120s = %s", str.toLocal8Bit().data());
```

##### 日期时间数据与字符串的转换

- `QTime`、`QDate`和`QDateTime`都有静态函数`fromString`，用于将字符串转换为相应类的对象。例如常用的一种函数原型：`QDateTime QDateTime::fromString(const QString &string, const QString &format, QCalendar cal = QCalendar())`。其中`cal`是日历类型。

| 格式字符  | 含义                                                |
| --------- | --------------------------------------------------- |
| `d`       | 天，不补零显示，1~31                                |
| `dd`      | 天，补零显示，1~31                                  |
| `M`       | 月，不补零显示，1~12                                |
| `MM`      | 月，补零显示，1~12                                  |
| `yy`      | 年，两位显示，00~99                                 |
| `yyyy`    | 年，4位显示                                         |
| `h`       | 小时，不补零显示，0~23或1~12（如果显示`AM/PM`）     |
| `hh`      | 小时，补零两位显示，00~23或01~12（如果显示`AM/PM`） |
| `H`       | 小时，不补零显示，0~23（即使显示`AM/PM`）           |
| `HH`      | 小时，补零显示，00~23（即使显示`AM/PM`）            |
| `m`       | 分钟，不补零显示，0~59                              |
| `mm`      | 分钟，补零显示，00~59                               |
| `s`       | 秒，不补零显示，0~59                                |
| `ss`      | 秒，补零显示，00~59                                 |
| `z`       | 毫秒，不补零显示，0~999                             |
| `zzz`     | 毫秒，补零3位显示，000~999                          |
| `AP`或`A` | 使用`AM/PM`显示                                     |
| `ap`或`a` | 使用`am/pm`显示                                     |

#### 9、日期时间数据的界面组件

##### `QDateTimeEdit`及其子类

- `currentSection`：光标所在的日期时间输入段，是枚举类型`QDateTimeEdit::Section`
- `currentSectionIndex`：用序号表示的光标所在的段
- `calendarPopup`：是否允许弹出一个日历选择框。当设置为`true`时，编辑框右端的上下调节按钮变为一个下拉按钮，点击按钮时会出现一个日历选择框，用于在日历上选择日期。
- `displayFormat`：日期时间数据的显示格式。
- 常用接口函数如下：
  - `QDateTime datetime()`返回编辑框的日期时间数据
  - `void setDateTime(const QDateTime &dateTime)`设置日期时间数据
  - `QDate date()`返回编辑框的日期数据
  - `void setDate(QDate date)`设置日期数据
  - `QTime time()`返回编辑框的时间数据
  - `void setTime(QTime time)`设置时间数据
- `QDateTimeEdit`有如下3个信号
  - `void dateChanged(QDate date)`日期发生变化时
  - `void timeChanged(QTime time)`时间发生变化时
  - `void dateTimeChanged(const QDateTime &datetime)`日期或时间发生变化时

##### `QCalendarWidget`类

- 用于选择日期的日历组件。常用接口函数：
  - `void showToday()`显示系统当前日期的月历
  - `void showSelectedDate()`显示所选日期的月历
  - `QDate selectedDate()`返回选择的日期
  - `void setSelectedDate(QDate date)`设置选择的日期
- `QCalendarWidget`定义了4个信号：
  - `void activated(QDate date)`在日历组件上按<kbd>Enter</kbd>键，或双击一个日期时
  - `void clicked(QDate date)`在日历组件上点击一个有效日期时
  - `void currentPageChanged(int year, int month)`当前显示的月历变化时
  - `void selectionChanged()`当前选择的日期变化时。

#### 10、`QTimer`和`QElapsedTimer`

- `QTimer`是软件定时器，其父类是`QObject`。`QTimer`的主要功能是设置以毫秒为单位的定时周期，然后进行连续定时或单次定时。启动定时器后，定时溢出时`QTimer`会发射`timeout()`信号，为`timeout()`信号关联槽函数就可以进行定时处理。
- `QElapsedTimer`用于快速计算两个事件的间隔时间，是软件计时器。`QElapsedTimer`没有父类，其计时精度可以达到纳秒级。主要用途是比较精确地确定一段程序运行的时长。

##### `QTimer`类

- 主要属性如下：

| 属性            | 属性值类型      | 功能                                                         |
| --------------- | --------------- | ------------------------------------------------------------ |
| `interval`      | `int`           | 定时周期，单位是毫秒                                         |
| `singleShot`    | `bool`          | 定时器是否为单次定时，`true`表示单次定时                     |
| `timerType`     | `Qt::TimerType` | 定时器精度类型                                               |
| `active`        | `bool`          | 只读属性，返回`true`表示定时器正在运行，也就是运行`start()`函数启动了定时器 |
| `remainingTime` | `int`           | 只读属性，到发生定时溢出的剩余时间，单位是毫秒。若定时器未启动，属性值为-1，若已经发生定时溢出，属性值为0 |

- `Qt::TimerType`有以下几种枚举值，默认值是`Qt::CoarseTimer`
  - `Qt::PreciseTimer`精确定时器，精度尽量保持在毫秒级
  - `Qt::CoarseTimer`粗糙定时器，定时误差尽量在定时周期的5%以内
  - `Qt::VeryCoarseTimer`非常粗糙的定时器，精度保持在秒级
- 用于启动和停止定时器的函数原型：
  - `void QTimer::start()`启动定时器
  - `void QTimer::start(int msec)`启动定时器，并设置定时周期为`msec`，单位是毫秒
  - `void QTimer::stop()`停止定时器
- `QTimer`只有一个`timeout()`信号，用函数`start()`启动定时器后，定时溢出时就会发射`timeout()`信号。
- `QTimer`有一个静态函数`singleShot()`，用于创建和启动单次定时器，并且将定时器的`timeout()`信号与指定的槽函数关联。其中一种函数原型如下：
  - `void QTimer::singleShot(int msec, Qt::TimerType timerType, const QObject *receiver, const char *member)`
  - 其中，`msec`是定时周期，`timerType`是定时器精度类型，`receiver`是接收定时器的`timeout()`信号的对象，`member`是与`timeout()`信号关联的槽函数的指针。

##### `QElapsedTimer`类

- 主要的接口函数如下：
  - `void start()`复位并启动定时器
  - `qint64 elapsed()`返回流逝的时间，单位毫秒
  - `qint64 nsecsElapsed()`返回流逝的时间，单位纳秒
  - `qint64 restart()`重新启动计时器，返回从上次启动到现在的时间，单位是毫秒。
- `QLCDNumber`的关键属性
  - `digitCount`显示的数字位数
  - `smallDecimalPoint`是否显示小数点

#### 12、`QComboBox`

- 可以提供下拉列表供用户选择输入，也可以提供编辑框用于输入文字，左移`QComboBox`也被称为组合框。下拉列表框的下拉列表的每个项可以存储一个或多个`QVariant`类型的用户数据，用户数据不显示在界面上。

##### `QComboBox`类的属性和接口函数

| 属性                | 属性值类型     | 功能                                                         |
| ------------------- | -------------- | ------------------------------------------------------------ |
| `editable`          | `bool`         | 是否可编辑。如果值为`false`，就只能从下拉列表里选择；如果值为`true`，会显示一个编辑框允许输入文字 |
| `currentText`       | `QString`      | 当前显示的文字                                               |
| `currentIndex`      | `int`          | 当前选中项的序号，序号从0开始。-1表示没有项被选中            |
| `maxVisibleItems`   | `int`          | 下拉列表中显示的项的最大条数，默认值是10.如果项数超过这个值，会自动出现滚卷条 |
| `maxCount`          | `int`          | 下拉列表里项的最大条数                                       |
| `insertPolicy`      | `InsertPolicy` | 用户编辑的新文字插入列表的方式，是枚举类型`QComboBox::InsertPolicy`，默认值是`InsertAtBottom`，也就是插入列表末尾。如果值是`NoInsert`，就表示不允许插入 |
| `placeholderText`   | `QString`      | 占位文字。当`currentIndex`属性值为-1时下拉列表框显示的文字。这个文字不会出现在下拉列表里 |
| `duplicatesEnabled` | `bool`         | 是否允许列表中出现重复的项                                   |
| `modelColumn`       | `int`          | 下拉列表中的数据在数据模型中的列编号，默认值为0              |

- `QComboBox`使用模型/视图结构存储和显示下拉列表的数据，下拉列表的数据实际上存储在`QStandardItemModel`模型里，下拉列表是用`QListView`的子类组件显示的。`modelColumn`属性表示下拉列表显示的数据在模型中的列编号，默认值是0。

##### `QComboBox`的信号

- `index`是项的序号，`text`是项的文字
- `void activated(int index)`，选择下拉列表的一个项时，即使选择的项没有发生变化，组件也会发射`activated()`信号
- `void currentIndexChanged(int index)`，不管是用户在界面操作还是程序导致`currentIndex`变化，都会发射该信号
- `void currentTextChanged(const QString &text)`，不管是用户在界面操作还是程序导致`currentText`变化，都会发射该信号
- `void editTextChanged(const QString &text)`，在编辑框修改文字时，会发射该信号
- `void highlighted(int index)`，移动鼠标使下拉列表中的某一项被高亮显示但还没有完成选择时，会发射该信号
- `void textActivated(const QString &text)`，在下拉列表中选择某一项时，即使选择的项没有变化，也会发射该信号
- `void textHighlighted(const QString &text)`，当下拉列表中的某一项被高亮显示但还没有完成选择时，会发射该信号。

##### 对列表的操作

- 添加项：`addItem()`
  - `void addItem(const QString &text, const QVariant &userData = QVariant())`
  - `void addItem(const QIcon &icon, const QString &text, const QVariant &userData = QVariant())`
- 添加多个项：`addItems()`，只有文字，没有图标和用户数据
  - `void addItems(const QStringList &texts)`
- 插入一项：`insertItem()`；插入多项：`insertItems()`
  - `void insertItem(int index, const QString &text, const QVariant &userData = QVariant())`
  - `void insertItem(int index, const QIcon &icon, const QString &text, const QVariant &userData = QVariant())`
  - `void insertItems(int index, const QStringList &list)`
- 移除某项：`removeItem()`；清除整个列表内容：`clear()`
  - `void removeItem(int index)`
  - `void clear()`
- 访问列表项，`Qt::UserRole`表示用户数据，一个项不止设置一个用户数据，例如设置第二个用户数据时，传递`role`的值为`1+Qt::UserRole`
  - `int count()`返回列表中项的总数
  - `int currentIndex()`返回当前项的序号
  - `QString currentText()`返回当前项的文字
  - `QVariant currentData(int role = Qt::UserRole)`返回当前项的用户数据
  - `QString itemText(int index)`返回序号为`index`项的文字
  - `QIcon itemIcon(int index)`返回序号为`index`项的图标
  - `QVariant itemData(int index, int role = Qt::UserRole)`返回序号为`index`项的用户数据
  - `void setItemText(int index, const QString &text)`设置序号为`index`项的文字
  - `void setItemIcon(int index, const QIcon &icon)`设置序号为`index`项的图标
  - `void setItemData(int index, const QVariant &value, int role  = Qt::UserRole)`

#### 13、`QMainWindow`和`QAction`

- `QMainWindow`是主窗口类，具有菜单栏、工具栏、状态栏等主窗口常见的界面元素。
- `QAction`对象就是实现某个功能的“动作”。其父类是`QObject`，所以支持`Qt`的元对象系统。

##### 设计`Action`

- 在`Qt Designer`界面下方有一个`Action`编辑器，用于可视化设计`Action`。下面是一些设置内容：
  - `Text`：`Action`的显示文字，该文字会作为菜单项标题或工具按钮标题显示。若该标题后面有`...`（用于表示有打开对话框的操作），则工具按钮标题会自动去除。
  - `Object name`：`Action`的对象名称。应该遵循自身的命名规则。
  - `ToolTip`：当鼠标移动到菜单项或工具按钮上短暂停留时，在光标处显示的提示文字。
  - `Icon`：`Action`的图标，点击其右边的按钮可以从资源文件里选择图标
  - `Checkable`：`Action`是否可以被复选，如果勾选此复选框，那么该`Action`就有复选状态。
  - `Shortcut`：`Action`的快捷键，将光标定位到其旁边的编辑框里，按下需要设置的快捷键即可。

##### `Action`的属性

- `text`：用`Action`创建菜单项的显示文字。
- `iconText`：用`Action`创建工具按钮时按钮上显示的文字。如果`Text`设置的文字有`...`，`iconText`的文字会自动去除
- `toolTip`
- `statusTip`：这是鼠标移动到菜单项或工具按钮上时，在主窗口下方状态栏的临时消息区显示的文字，显示两秒后自动消失。默认为空
- `shortcutContext`：这是`Action`的快捷键的有效响应范围，默认值是`WindowShortcut`，表示`Action`关联的组件是当前窗口的子组件时快捷键有效；如果值为`ApplicationShortcut`，表示只要应用程序有窗口显示，快捷键就有效。
- `autoRepeat`：表示当快捷键被一直按下时，`Action`是否自动重复执行
- `menuRole`：在`macOS`上才有意义，表示`Action`创建的菜单项的作用
- `iconVisibleInMenu`：表示在菜单项上是否显示`Action`的图标
- `shortcutVisibleInContextMenu`：表示在使用`Action`创建右键快捷菜单时，是否显示快捷键
- `priority`：表示`Action`在`UI`上的优先级。默认值为`NormalPriority`。如果设置为`LowPriority`，当工具栏的`toolButtonStyle`属性设置为`Qt::ToolButtonTextBesideIcon`时，按钮上将不显示`Action`的文字。

##### 设计菜单栏和工具栏

- `QMainWindow`类窗口上有菜单栏、工具栏和状态栏，这三种界面组件对应的类分别是`QMenuBar`、`QToolBar`和`QStatusBar`，他们都是直接从`QWidget`继承来的。一个主窗口上最多有一个菜单栏和一个状态栏，可以有多个工具栏。
- `QMainWindow::setCentralWidget`函数将组件设置为中心组件，也就是使其填满主窗口的工作区。
- 工具栏`QToolBar`类的主要属性：

| 属性名称          | 属性值类型            | 含义和作用                                                   |
| ----------------- | --------------------- | ------------------------------------------------------------ |
| `movable`         | `bool`                | 工具栏是否可移动                                             |
| `allowedAreas`    | `Qt::ToolBarAreas`    | 工具栏可以放置的窗口区域                                     |
| `orientation`     | `Qt::Orientation`     | 工具栏的方向，有水平和垂直两种                               |
| `iconSize`        | `QSize`               | 图标大小，一般是16×16、24×24、32×32像素等大小                |
| `toolButtonStyle` | `Qt::ToolButtonStyle` | 工具按钮样式                                                 |
| `floatable`       | `bool`                | 工具栏是否可浮动，如果值为`true`，工具栏可以被拖出来作为一个浮动的窗口 |

- `toolButtonStyle`的几种取值
  - `Qt::ToolButtonIconOnly`只显示图标
  - `Qt::ToolButtonTextOnly`只显示文字
  - `Qt::ToolButtonTextBesideIcon`文字显示在图标旁边
  - `Qt::ToolButtonTextUnderIcon`文字显示在图标下面
  - `Qt::ToolButtonFollowStyle`由`QStyle`样式定义

##### `QAction`类

- `QAction`的信号
  - `void changed()`：`Action`的`text`、`toolTip`、`font`等属性值变化时
  - `void checkableChanged(bool checkable)`：`checkable`属性值变化时
  - `void enabledChanged(bool enabled)`：`enabled`属性值变化时
  - `void hovered()`：鼠标移动到用此`Action`创建的菜单项或工具按钮上时
  - `void toggled(bool checked)`：`checked`属性值变化时
  - `void triggered(bool checked = false)`：点击用此`Action`创建的菜单项或工具按钮时
  - `void visibleChanged()`：`visible`属性值变化时
- `QAction`的公有槽
  - `void hover()`触发`hovered()`信号
  - `void trigger()`触发`triggered()`信号
  - `void resetEnabled()`复位`enabled`属性为默认值
  - `void setChecked(bool)`设置`checked`属性的值
  - `void setDisabled(bool b)`设置`enabled`属性的值，若`b = true`，设置`enabled = false`
  - `void setEnabled(bool)`设置`enabled`属性的值
  - `void setVisible(bool)`设置`visible`属性的值
  - `void toggle()`反转`checked`属性的值

##### `QToolBar`类

- 可以用`Action`可视化的创建工具栏上的按钮，但是不能可视化的在工具栏上放置其它组件。`QToolBar`提供了接口函数，可以通过代码在工具栏上添加组件，常用的几个函数定义如下：
  - `void addAction(QAction *action)`添加一个`Action`，并根据`Action`的设置自动创建工具按钮
  - `QAction *addWidget(QWidget *widget)`添加一个界面组件
  - `QAction *insertWidget(QAction *before, QWidget *widget)`插入一个界面组件
  - `QAction *addSeparator()`添加一个分隔条
  - `QAction *insertSeparator(QAction *before)`插入一个分隔条

##### `QStatusBar`类

- 主窗口上的状态栏对应的是`QStatusBar`类，在`UI`可视化设计时，不能在状态栏上放置任何组件，而只能通过其接口函数向状态栏添加组件。`QStatusBar`有两个函数用于添加组件：
  - `void addWidget(QWidget *widget, int stretch = 0)`添加正常组件。参数`stretch`是延展因子，用于确定组件所占用的空间。按添加的先后顺序，从状态栏左端开始从左往右排列。
  - `void addPermanentWidget(QWidget *widget, int stretch = 0)`添加永久组件。按添加的先后顺序排列，但是为右对齐。也就是最后添加的组件在状态栏右端
- `QStatusBar`有两个公有槽，可以显示和清除临时消息：
  - `void showMessage(const QString &message, int timeout = 0)`显示临时消息。用于在状态栏上左端首位置显示字符串信息，显示持续时间是`timeout`，单位是毫秒。如果`timeout`设置为0，就是一直显示，直到`clearMessage()`清除，或显示下一条临时消息。
    - 使用该方法显示临时消息时，状态栏上用`addWidget()`添加的组件会被临时隐藏，而用`addPermanentWidget()`函数添加的组件会保持不变。
  - `void clearMessage()`清除临时消息
- 自动显示`Action`的`statusTip`
  - 如果一个`Action`的`statusTip`属性不为空，当鼠标移动到由这个`Action`创建的菜单项或工具按钮上时，状态栏上就会自动显示这个`Action`的`statusTip`属性的内容。

##### `QPlainTextEdit`的使用

- 信号和槽
  - `void blockCountChanged(int newBlockCount)`段落数变化时
  - `void copyAvailable(bool yes)`有文字被选择或取消选择时
  - `void cursorPositionChanged()`光标位置变化时
  - `void modificationChanged(bool changed)`文档的修改状态变化时
  - `void redoAvailable(bool available)`：`redo`操作状态变化时
  - `void selectionChanged()`选择的内容变化时
  - `void textChanged()`文档内容变化时
  - `void undoAvailable(bool available)`：`undo`操作状态变化时
  - `void updateRequest(const QRect &rect, int dy)`需要更新显示时
- 用于设置字体大小和选择字体的两个组件关联的是自定义槽函数

#### 14、`QToolButton`和`QListWidget`

- `Qt`中用于处理项数据的组件有两类：一类是`Item Views`组件，包括`QListView`、`QTreeView`、`QTableView`等。另外一类是`Item Widgets`组件，包括`QListWidget`、`QTreeWidget`、`QTableWidget`等。
- 一个项存储了文字、文字的格式定义、图标、用户数据等内容。

##### `QToolBox`组件

- 该组件是工具箱组件类，工具箱是一种垂直分页的多页容器组件。
- 工具箱的每个页面就是一个`QWidget`组件，在页面的工作区可以放置任何其他界面组件。
- 该组件有一个信号`currentChanged()`，在切换当前页面时组件发射此信号，其函数原型定义如下（参数`index`是当前页面序号）：`void QToolBox::currentChanged(int index)`

##### `QListWidget`组件

- 该组件是列表组件，每一行是一个`QListWidgetItem`类型的对象，他有一个标志变量`flags`，用于设置列表项的特性，`flags`是枚举类型`Qt::ItemFlag`的枚举值的组合。
  - `Selectable`：列表项可被选择，对应枚举值`Qt::ItemIsSelectable`
  - `Editable`：列表项可被编辑，对应枚举值`Qt::ItemIsEditable`
  - `DragEnabled`：列表项可以被拖动，对应枚举值`Qt::ItemIsDragEnabled`
  - `DropEnabled`：列表项可以接收拖放的项，对应枚举值`Qt::ItemIsDropEnabled`
  - `UserCheckable`：列表项可以被复选，若为`true`，列表项前面会出现一个复选框，对应枚举值`Qt::ItemIsUserCheckable`
  - `Enabled`：列表项可用，对应枚举值`Qt::ItemIsEnabled`
  - `Tristate`：自动改变列表项的复选状态，对应枚举值`Qt::ItemIsAutoTristate`。该特性对`QTreeWidget`的节点有效，对`QListWidget`的列表项无效。

##### `QToolButton`与界面补充创建

- `QToolButton`的属性
  - `popupMode`：属性值是枚举类型：`QToolButton::ToolButtonPopupMode`。当按钮有下拉菜单时，这个属性决定了弹出菜单的模式，几个值的含义如下：
    - `QToolButton::DelayedPopup`：按钮上没有任何附加的显示内容。如果按钮有下拉菜单，按下按钮并延时一会儿后，才显示下拉菜单。
    - `QToolButton::MenuButtonPopup`：会在按钮右侧显示一个带箭头图标的下拉按钮。点击下拉按钮才显示下拉菜单，点击工具按钮会执行按钮关联的`Action`而不会显示下拉菜单。
    - `QToolButton::InstantPopup`：会在按钮的右下角显示一个很小的下拉箭头图标，点击按钮就会立刻显示下拉菜单，即使工具按钮关联了一个`Action`也不会执行这个`Action`
  - `toolButtonStyle`：属性值是枚举类型`Qt::ToolButtonStyle`，表示工具按钮上文字与图标的显示方式，与`QToolBar`类的同名属性的含义是一样的。
  - `autoRaise`：如果设置为`true`，按钮就没有边框，鼠标移动到按钮上时才显示按钮边框。
  - `arrowType`：属性值是枚举类型`Qt::ArrowType`。默认值是`Qt::NoArrow`，不会在按钮上显示内容。另外还有4个枚举值，可以在按钮上显示表示方向的箭头图标。
- 其它接口函数
  - `setDefaultAction()`用于为工具按钮设置关联的`Action`。
  - `void QToolButton::setMenu(QMenu *menu)`：用于为工具按钮设置下拉菜单。不能可视化设计下拉菜单或某个组件的快捷菜单。
  - `QMenu`是管理菜单的类，他的父类是`QWidget`，菜单实际上是一种窗口。创建菜单主要会用到以下几个函数：
    - `void QWidget::addAction(QAction *action)`添加`Action`，创建菜单项
    - `QAtcion *QMenu::addMenu(QMenu *menu)`添加菜单，创建子菜单
    - `QAction *Menu::addSeparator()`添加一个分隔条。
  - 显示菜单项可以使用函数`exec()`：
    - `QAction *QMenu::exec(const QPoint &p, QAction *action = nullptr)`参数`p`表示菜单的左上角坐标。在点击鼠标右键显示快捷菜单时，一般使用鼠标光标的当前位置`QCursor::pos()`作为参数`p`的值。

##### `QListWidget`的操作

- `QListWidgetItem`类没有父类，所以没有属性。

- `QListWidgetItem`类的主要接口函数

| 读取函数       | 设置函数          | 数据类型         | 设置函数的功能                                       |
| -------------- | ----------------- | ---------------- | ---------------------------------------------------- |
| `text()`       | `setText()`       | `QString`        | 设置项的文字                                         |
| `icon()`       | `setIcon()`       | `QIcon`          | 设置项的图标                                         |
| `data()`       | `setData()`       | `QVariant`       | 为项的不同角色设置数据，可设置用户数据               |
| `flags()`      | `setFlags()`      | `Qt::ItemFlags`  | 设置项的特性，是枚举类型`Qt::ItemFlag`的枚举值的组合 |
| `checkState()` | `setCheckState()` | `Qt::CheckState` | 设置项的复选状态                                     |
| `isSelected()` | `setSelected()`   | `bool`           | 设置为当前项，相当于点击了这个项                     |

- `QListWidget`的主要接口函数（只列出了函数的返回值类型，省略了函数的输入参数）

<table>
    <tr>
        <th>分组</th>
        <th>函数名</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="6">添加或删除项</td>
        <td><code>void addItem()</code></td>
        <td>添加一个项</td>
    </tr>
    <tr>
    	<td><code>void addItems()</code></td>
        <td>根据一个字符串列表的内容，一次添加多个项</td>
    </tr>
    <tr>
    	<td><code>void insertItem()</code></td>
        <td>在某一行前面插入一个项</td>
    </tr>
    <tr>
    	<td><code>void insertItems()</code></td>
        <td>根据一个字符串列表的内容，一次插入多个项</td>
    </tr>
    <tr>
    	<td><code>QListWidgetItem *takeItem()</code></td>
        <td>从列表组件中移除一个项，并返回这个项的对象指针，但是并不从内存中删除这个项</td>
    </tr>
    <tr>
    	<td><code>void clear()</code></td>
        <td>移除列表中的所有项，并且从内存中删除对象</td>
    </tr>
    <tr>
    	<td rowspan="8">项的访问</td>
        <td><code>QListWidgetItem *currentItem()</code></td>
        <td>返回当前项，返回<code>nullptr</code>表示没有当前项</td>
    </tr>
    <tr>
    	<td><code>void setCurrentItem()</code></td>
        <td>设置当前项</td>
    </tr>
    <tr>
    	<td><code>QListWidgetItem *item()</code></td>
        <td>根据行号返回一个项</td>
    </tr>
    <tr>
    	<td><code>QListWidgetItem *itemAt()</code></td>
        <td>根据屏幕坐标返回项，例如鼠标在列表组件上移动时，可以用这个函数返回光标所在的项</td>
    </tr>
    <tr>
    	<td><code>int currentRow()</code></td>
        <td>返回当前行的行号，返回-1表示没有当前行</td>
    </tr>
    <tr>
    	<td><code>void setCurrentRow()</code></td>
        <td>设置当前行</td>
    </tr>
    <tr>
    	<td><code>int row()</code></td>
        <td>返回一个项所在行的行号</td>
    </tr>
    <tr>
    	<td><code>int count()</code></td>
        <td>返回列表组件中项的个数</td>
    </tr>
    <tr>
    	<td rowspan="3">排序</td>
    	<td><code>void setSortingEnabled()</code></td>
        <td>设置列表是否可排序</td>
    </tr>
    <tr>
    	<td><code>bool isSortingEnabled()</code></td>
        <td>列表是否可排序</td>
    </tr>
    <tr>
    	<td><code>void sortItems()</code></td>
        <td>对列表进行排序，可指定排序方式为升序或降序</td>
    </tr>
</table>


- 列表的排序
  - `QListWidget`有一个属性`sortingEnabled`表示列表是否可排序，默认值为`false`。但即使这个值是`false`，也可以使用函数`sortItems()`进行列表排序。如果为`true`，通过运行`addItem()`和`insertItem()`新增的项将添加到列表的最前面，所以如果要确保添加和插入项的操作正常，属性`sortingEnabled`应该设置为`false`
  - 排序函数：`void QListWidget::sortItems(Qt::SortOrder order = Qt::AscendingOrder)`
    - 参数`order`表示排序方式，是枚举类型`Qt::SortOrder`，有两种取值，其中`Qt::AscendingOrder`表示升序，`Qt::DescendingOrder`表示降序。

##### `QListWidget`的信号

- `void currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)`
- `void currentRowChanged(int currentRow)`当前项发生了切换
- `void currentTextChanged(const QString &currentText)`当前项发生了切换
- `void itemSelectionChanged()`表示选择的项发生了变化
- `void itemChanged(QListWidgetItem *item)`项的属性发生了变化，如文字、复选状态等
- `void itemActivated(QListWidgetItem *item)`光标停留在某个项上，按<kbd>Enter</kbd>键时发射此信号
- `void itemEntered(QListWidgetItem *item)`鼠标跟踪时
- `void itemPressed(QListWidgetItem *item)`鼠标左键或右键按下
- `void itemClicked(QListWidgetItem *item)`点击
- `void itemDoubleClicked(QListWidgetItem *item)`双击

##### 创建右键快捷菜单

- 每个继承自`QWidget`的类都有`customContextMenuRequested()`信号，在一个组件上点击鼠标右键时，组件发射这个信号，用于请求创建快捷菜单。如果为此信号编写槽函数，就可以创建和运行快捷菜单。
- 要使`QWidget`组件在点击鼠标右键时发射`customContextMenuRequested()`信号，还需要设置其`contextMenuPolicy`属性：`void QWidget::setContextMenuPolicy(Qt::ContextMenuPolicy policy)`，有以下几种枚举值：
  - `Qt::NoContextMenu`组件没有快捷菜单，由其父容器组件处理快捷菜单
  - `Qt::PreventContextMenu`阻止快捷菜单，并且点击鼠标右键事件也不会交给父容器组件处理。
  - `Qt::DefaultContextMenu`默认的快捷菜单，组件的`QWidget::contextMenuEvent()`事件被自动处理。某些组件有自己的默认快捷菜单，例如`QPlainTextEdit`的`contextMenuPolicy`属性默认设置为这个值，在无需任何编程的情况下，运行时点击鼠标右键就会出现一个标准操作快捷菜单。
  - `Qt::ActionsContextMenu`自动根据`QWidget::actions()`返回的`Action`列表创建并显示快捷菜单
  - `Qt::CustomContextMenu`组件发射`customContextMenuRequested()`信号，由用户编程实现创建并显示快捷菜单。

```c++
#include "mainwindow.h"

#include <QMenu>

#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  // 设置中心组件
  setCentralWidget(ui->splitter);
  // 定制快捷菜单
  ui->listWidget->setContextMenuPolicy(Qt::CustomContextMenu);
  setActionsForButton();
  createSelectionMenu();
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::setActionsForButton() {
  ui->tBtnListIni->setDefaultAction(ui->actListIni);
  ui->tBtnListClear->setDefaultAction(ui->actListClear);
  ui->tBtnListInsert->setDefaultAction(ui->actListInsert);
  ui->tBtnListAppend->setDefaultAction(ui->actListAppend);
  ui->tBtnListDelete->setDefaultAction(ui->actListDelete);
  ui->tBtnSelALL->setDefaultAction(ui->actSelALL);
  ui->tBtnSelNone->setDefaultAction(ui->actSelNone);
  ui->tBtnSelInvs->setDefaultAction(ui->actSelInvs);
}

void MainWindow::createSelectionMenu() {
  // 创建下拉菜单
  QMenu *menuSelection = new QMenu(this);
  menuSelection->addAction(ui->actSelALL);
  menuSelection->addAction(ui->actSelNone);
  menuSelection->addAction(ui->actSelInvs);

  // listWidget上方的“项选择”按钮
  ui->tBtnSelectItem->setPopupMode(QToolButton::MenuButtonPopup);
  ui->tBtnSelectItem->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
  ui->tBtnSelectItem->setDefaultAction(ui->actSelPopMenu);
  ui->tBtnSelectItem->setMenu(menuSelection);

  // 工具栏上方的“项选择”按钮，具有下拉菜单
  QToolButton *aBtn = new QToolButton(this);
  aBtn->setPopupMode(QToolButton::InstantPopup);
  aBtn->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
  aBtn->setDefaultAction(ui->actSelPopMenu);
  aBtn->setMenu(menuSelection);
  ui->mainToolBar->addWidget(aBtn);

  // 在工具栏上添加分隔条和“退出”按钮
  ui->mainToolBar->addSeparator();
  ui->mainToolBar->addAction(ui->actQuit);
}

void MainWindow::on_actListIni_triggered() {
  QListWidgetItem *aItem;
  QIcon aIcon;
  aIcon.addFile(":/icons/check2.ico");
  bool chk = ui->chkBoxListEditable->isChecked();

  ui->listWidget->clear();
  for (int i = 0; i < 10; i++) {
    QString str = QString("Item %1").arg(i);
    aItem = new QListWidgetItem();
    aItem->setText(str);
    aItem->setIcon(aIcon);
    aItem->setCheckState(Qt::Checked);
    aItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |
                    Qt::ItemIsEnabled |
                    (chk ? Qt::ItemIsEditable : Qt::NoItemFlags));
    ui->listWidget->addItem(aItem);
  }
}

void MainWindow::on_actListInsert_triggered() {
  QIcon aIcon(":/icons/check2.ico");
  bool chk = ui->chkBoxListEditable->isChecked();
  QListWidgetItem *aItem = new QListWidgetItem("Inserted Item");
  aItem->setIcon(aIcon);
  aItem->setCheckState(Qt::Checked);
  aItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |
                  Qt::ItemIsEnabled |
                  (chk ? Qt::ItemIsEditable : Qt::NoItemFlags));
  ui->listWidget->insertItem(ui->listWidget->currentRow(), aItem);
}

void MainWindow::on_actListAppend_triggered() {
  QIcon aIcon(":/icons/check2.ico");
  bool chk = ui->chkBoxListEditable->isChecked();
  QListWidgetItem *aItem = new QListWidgetItem("Added Item");
  aItem->setIcon(aIcon);
  aItem->setCheckState(Qt::Checked);
  aItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable |
                  Qt::ItemIsEnabled |
                  (chk ? Qt::ItemIsEditable : Qt::NoItemFlags));
  ui->listWidget->addItem(aItem);
}

void MainWindow::on_actListDelete_triggered() {
  int row = ui->listWidget->currentRow();
  QListWidgetItem *aItem = ui->listWidget->takeItem(row);
  delete aItem;
}

void MainWindow::on_actSelInvs_triggered() {
  QListWidgetItem *aItem;
  int n = ui->listWidget->count();
  for (int i = 0; i < n; i++) {
    aItem = ui->listWidget->item(i);
    if (aItem->checkState() == Qt::Checked) {
      aItem->setCheckState(Qt::Unchecked);
    } else {
      aItem->setCheckState(Qt::Checked);
    }
  }
}

void MainWindow::on_actSelALL_triggered() {
  QListWidgetItem *aItem;
  int n = ui->listWidget->count();
  for (int i = 0; i < n; i++) {
    aItem = ui->listWidget->item(i);
    aItem->setCheckState(Qt::Checked);
  }
}

void MainWindow::on_actSelNone_triggered() {
  int n = ui->listWidget->count();
  for (int i = 0; i < n; i++) {
    QListWidgetItem *aItem = ui->listWidget->item(i);
    aItem->setCheckState(Qt::Unchecked);
  }
}

void MainWindow::on_chkBoxSorting_clicked(bool checked) {
  ui->listWidget->setSortingEnabled(checked);
}

void MainWindow::on_tBtnSortAsc_clicked() {
  ui->listWidget->sortItems(Qt::AscendingOrder);
}

void MainWindow::on_tBtnSortDes_clicked() {
  ui->listWidget->sortItems(Qt::DescendingOrder);
}

void MainWindow::on_listWidget_currentItemChanged(QListWidgetItem *current,
                                                  QListWidgetItem *previous) {
  QString str;
  if (current != nullptr) {
    if (previous == nullptr) {
      str = "当前: " + current->text();
    } else {
      str = "前一项: " + previous->text() + "; 当前项: " + current->text();
    }
    ui->editCutItemText->setText(str);
  }
  ui->plainTextEdit->appendPlainText("currentItemChanged()信号被发射");
}

void MainWindow::on_listWidget_currentRowChanged(int currentRow) {
  ui->plainTextEdit->appendPlainText(
      QString("currentRowChanged()信号被发射, currentRow = %1")
          .arg(currentRow));
}

void MainWindow::on_listWidget_itemChanged(QListWidgetItem *item) {
  ui->plainTextEdit->appendPlainText("itemChanged()信号被发射, " +
                                     item->text());
}

void MainWindow::on_listWidget_itemSelectionChanged() {
  ui->plainTextEdit->appendPlainText("itemSelectionChanged()信号被发射");
}

void MainWindow::on_tBtnAddLine_clicked() {
  ui->plainTextEdit->appendPlainText(QString());
}

void MainWindow::on_listWidget_customContextMenuRequested(const QPoint &pos) {
  Q_UNUSED(pos);
  QMenu *menuList = new QMenu(this);
  menuList->addAction(ui->actListIni);
  menuList->addAction(ui->actListClear);
  menuList->addAction(ui->actListInsert);
  menuList->addAction(ui->actListAppend);
  menuList->addAction(ui->actListDelete);
  menuList->addSeparator();
  menuList->addAction(ui->actSelALL);
  menuList->addAction(ui->actSelNone);
  menuList->addAction(ui->actSelInvs);
  // 在鼠标光标位置显示快捷菜单
  menuList->exec(QCursor::pos());
  // 菜单显示完后, 需要删除对象
  delete menuList;
}
```

#### 15、`QTreeWidget`

- 适合显示具有层级结构的数据。

##### `QDockWidget`的属性

- `floating`停靠区组件是否处于浮动状态。

- `features`停靠区组件的特性。函数`setFeatures()`用于设置此属性值，其函数原型定义：`void QDockWidget::setFeatures(QDockWidget::QDockWidgetFeatures setFeatures)`
- `allowedAreas`允许停靠区域。`void QDockWidget::setAllowedAreas(Qt::DockWidgetAreas areas)`
  - 可以设置在窗口的左侧、右侧、顶点、底部停靠，也可以设置在所有区域都可停靠或不允许停靠。
- `windowTitle`停靠区窗口的标题。

##### `QDockWidget`的信号

- `void allowedAreasChanged(Qt::DockWidgetAreas allowedAreas)`：`allowedAreas`属性值变化时
- `void dockLocationChanged(Qt::DockWidgetArea area)`移动到其他停靠区时
- `void featuresChanged(QDockWidget::DockWidgetFeatures features)`：`features`属性值变化时
- `void topLevelChanged(bool topLevel)`：`floating`属性值变化时
- `void visibilityChanged(bool visible)`：`visible`属性值变化时

##### `QTreeWidget`

- 表头。树形组件有表头，表头可以只是简单的文字，也可以设置为`QTreeWidgetItem`对象。当使用`QTreeWidgetItem`对象作为表头时，不仅可以设置表头每一列的文字，还可以设置字体、对齐方式、背景色、图标等显示特性。
  - 如果只是简单地设置表头文字，可以使用函数`setHeaderLabels()`将字符串列表的内容作为表头各列的标题。`void QTreeWidget::setHeaderLabels(const QStringList &labels)`
  - 还可以使用`headerItem()`返回表头的`QTreeWidgetItem`对象指针。
    - `void QTreeWidget::setHeaderItem(QTreeWidgetItem *item)`设置表头节点
    - `QTreeWidgetItem *QTreeWidget::headerItem()`返回表头节点
- 顶层节点。目录树里一行就是一个节点。节点是`QTreeWidgetItem`对象。节点可以有子节点，子节点就是下一级的节点，可以层层嵌套。目录树里最上层的节点称为顶层节点，顶层节点没有父节点。目录树里可以有任意多个顶层节点。
  - `int topLevelItemCount()`返回顶层节点的个数
  - `void addTopLevelItem(QTreeWidgetItem *item)`添加一个顶层节点
  - `void insertTopLevelItem(int index, QTreeWidgetItem *item)`插入一个顶层节点
  - `int indexOfTopLevelItem(QTreeWidgetItem *item)`返回一个顶层节点的索引号
  - `QTreeWidgetItem *topLevelItem(int index)`根据索引号获取一个顶层节点
  - `QTreeWidgetItem *takeTopLevelItem(int index)`移除一个顶层节点，但是并不删除这个节点对象。
  - 获取一个顶层节点对象后，就可以访问它的所有子节点。
- 次级节点。所有的次级节点都直接或间接挂靠在某个顶层节点下面，顶层节点和次级节点都是`QTreeWidgetItem`类对象。
- 隐藏的根节点。目录树中还有一个隐藏的根节点，其可以看做是所有顶层节点的父节点。函数`invisibleRootItem()`可以返回这个隐藏的根节点：`QTreeWidgetItem *QTreeWidget::invisibleRootItem()`。使用这个隐藏的根节点就可以通过`QTreeWidgetItem`类的接口函数访问所有顶层节点。
- 其它接口函数
  - `int columnCount()`返回表头列数
  - `void setColumnCount(int columns)`设置表头列数
  - `void sortItems(int column, Qt::SortOrder order)`将目录树按照某一列排序
  - `int sortColumn()`返回用于排序的列的序号
  - `QTreeWidgetItem *currentItem()`返回当前节点
  - `QList<QTreeWidgetItem *> selectedItems()`返回选择的节点的列表
- 目录树上有一个当前节点，也就是通过点击鼠标或者按键移动选择的节点。
- 通过`QTreeWidget`的上层父类`QAbstractItemView`的`selectionMode`属性能够设置选择模式，可以设置为多选。

##### `QTreeWidget`的公有槽

- `void clear()`清除整个目录树，但是不清除表头。
- `void collapseItem(const QTreeWidgetItem *item)`折叠节点
- `void expandItem(const QTreeWidgetItem *item)`展开节点
- `void scrollToItem(const QTreeWidgetItem *item, QAbstractItemView::ScrollHint hint = EnsureVisible)`用于确保节点`item`可见，必要时自动移动树形组件的滚卷条

##### `QTreeWidget`的信号

- `void currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem &previous)`当前节点发生变化时被发射。即使当前点击的行没有变化，但是被点击节点的列发生了变化，组件也会发射此信号
- `void itemActivated(QTreeWidgetItem *item, int column)`点击或双击节点时
- `void itemChanged(QTreeWidgetItem *item, int column)`在节点的某一列的属性发生变化时被发射。
- `void itemClicked(QTreeWidgetItem *item, int column)`在点击节点时被发射，不管行和列有没有发生变化
- `void itemCollapsed(QTreeWidgetItem *item)`节点折叠时
- `void itemDoubleClicked(QTreeWidgetItem *item, int column)`
- `void itemEntered(QTreeWidgetItem *item, int column)`鼠标光标移动到节点上时
- `void itemExpanded(QTreeWidgetItem *item)`节点展开时
- `void itemPressed(QTreeWidgetItem *item, int column)`在目录树上按下鼠标左键或右键时。
- `void itemSelectionChanged()`在用户选择的节点发生变化时被发射，在当前节点切换或多选节点变化时都会触发此信号。

##### `QTreeWidgetItem`类

- 该类没有父类，它只用来存储节点的数据和各种属性。
- 创建`QTreeWidgetItem`对象有多种参数形式的构造函数：
  - `QTreeWidgetItem(int type = Type)`可以传递一个整数表示节点的类型，这个参数类型是一个自定义的数据，通过成员函数`type()`可以返回这个类型，如何使用这个类型数据由用户程序决定。节点创建后就不能更改节点类型了。自定义的节点类型数值必须大于1000。
    - 使用这个构造函数创建节点后，还需要调用`QTreeWidgetItem`类的各种接口函数设置节点各列的文字、图标、复选状态等属性。然后可以通过`QTreeWidget::addTopLevelItem()`函数将创建的节点添加成目录树的顶层节点，或者通过`QTreeWidgetItem::addChild()`函数将其添加成一个节点的子节点。
  - `QTreeWidgetItem(const QStringList &strings, int type = Type)`传递字符串列表作为节点各列的文字
  - `QTreeWidgetItem(QTreeWidgetItem *parent, int type = Type)`在某个节点下创建子节点
  - `QTreeWidgetItem(QTreeWidget *parent, int type = Type)`直接在树形组件里创建顶层节点。

###### 访问节点各列数据的接口函数

- 按列设置数据的接口函数：
- `void setBackground(int column, const QBrush &brush)`设置背景色
- `void setForeground(int column, const QBrush &brush)`设置前景色
- `void setText(int column, const QString &text)`设置文字
- `void setTextAlignment(int column, int allignment)`设置文字对齐方式
- `void setToolTip(int column, const QString &toolTip)`设置`toolTip`文字
- `void setStatusTip(int column, const QString &statusTip)`设置`statusTip`文字
- `void setIcon(int column, const QIcon &icon)`设置图标
- `void setCheckState(int column, Qt::CheckStats state)`设置复选状态
- `void setFont(int column, const QFont &font)`设置字体

- 还有一个函数`setData()`可以为节点的某一列设置用户数据，这个数据不显示在界面上。
  - `void setData(int column, int role, const QVariant &value)`
  - `QVariant data(int column, int role)`参数`role`是用户数据角色，可以使用常量`Qt::UserRole`定义第一个用户数据，`1+Qt::UserRole`定义第二个用户数据。

###### 对节点整体特性进行操作的接口函数：

- `int type()`返回在创建节点时设置的`type`参数
- `void setExpanded(bool expand)`使节点展开或折叠
- `bool isExpanded()`
- `void setDisabled(bool disabled)`设置是否禁用此节点
- `bool isDisabled()`
- `void setHidden(bool hide)`设置是否隐藏此节点
- `bool isHidden()`
- `void setSelected(bool select)`设置此节点是否被选中
- `bool isSelected()`

- 还有一个函数`setFlags()`用于设置节点的一些特性：
  - `void setFlags(Qt::ItemFlags flags)`设置节点的标志
  - `Qt::ItemFlags flags()`读取节点的标志。
  - 各枚举值意义如下：
    - `Qt::NoItemFlags`没有任何标志
    - `Qt::ItemIsSelectable`节点可以被选中
    - `Qt::ItemIsEditable`节点可以被编辑
    - `Qt::ItemIsDragEnabled`节点可以被拖动
    - `Qt::ItemIsDropEnabled`节点可以接收拖来的对象
    - `Qt::ItemIsUserCheckable`节点可以被复选，节点前会出现一个复选框
    - `Qt::ItemIsEnabled`节点可用
    - `Qt::ItemIsAutoTristate`自动决定3种复选状态
    - `Qt::ItemNeverHasChildren`不允许有子节点
    - `Qt::ItemIsUserTristate`用户决定3种复选状态

###### 操作子节点的接口函数

- `void addChild(QTreeWidgetItem *child)`添加一个子节点
- `QTreeWidgetItem *child(int index)`根据序号返回一个子节点
- `int childCount()`返回子节点的个数
- `int indexOfChild(QTreeWidgetItem *child)`返回一个子节点的索引号
- `void insertChild(int index, QTreeWidgetItem *child)`插入一个子节点
- `void removeChild(QTreeWidgetItem *child)`移除一个子节点
- `QTreeWidgetItem *takeChild(int index)`移除一个子节点，并返回这个节点的指针。

###### 父节点

- `QTreeWidgetItem *parent()`返回父节点的指针。

##### 用`QLabel`和`QPixmap`显示图片

- 函数`QPixmap::load()`直接从一个文件加载图片。
- 图片的缩放与显示：
  - `scaledToHeight(int height)`返回一个缩放后的图片副本，图片缩放到高度`height`
  - `scaledToWidth(int width)`返回一个缩放后的图片副本，图片缩放到宽度`width`
  - `scaled(int width, int height)`返回一个缩放后的图片副本，图片缩放到宽度`width`和高度`height`默认是不保持比例。
- 使用`void QLabel::setPixmap(const QPixmap &)`显示图片。

#### 16、`QTableWidget`

- 以表格的形式显示和管理数据。
- 表格的第一行称为水平表头，用于设置每一列的标题。第一列称为垂直表头，也可以设置其标题，但一般使用默认的标题，也就是行号。
- 水平表头和垂直表头一般是不可编辑的。一个网格称为单元格（`cell`）。每个单元格关联一个`QTableWidgetItem`对象，可以设置每个单元格的文字内容、字体、前景色、背景色、图标等。单元格还可以有复选框或设置为其他`qidget`组件，每个单元格还可以存储用户数据。

##### 表格的行和列

- `QTableWidget`是`QTableView`的便利类。其属性和接口函数主要是父类定义的。
- `QTableWidget`新增了两个属性，`rowCount`表示数据区行数，`columnCount`表示数据区列数。还定义了如下的公有槽函数：
  - `void insertColumn(int column)`在列号为`column`的位置插入一个空列
  - `void removeColumn(int column)`移除列号为`column`的一列
  - `void insertRow(int row)`在行号为`row`的位置插入一个空行
  - `void removeRow(int row)`移除行号为`row`的一行
- 为表格的一个单元格设置项：`void setItem(int row, int column, QTableWidgetItem *item)`
- 移除关联的项：`QTableWidgetItem *takeItem(int row, int column)`但不删除这个对象。
- 清除整个表格或数据区的全部单元格的项
  - `void clear()`清除表头和数据区的所有项
  - `void clearContents()`清除数据区的所有项

##### 当前单元格和当前项

- `int currentRow()`返回当前单元格的行号
- `int currentColumn()`返回当前单元格的列号
- `void setCurrentCell(int row, int column)`通过行号和列号设置当前单元格。
- `QTableWidgetItem *currentItem()`返回当前单元格的项
- `void setCurrentItem(QTableWidgetItem *item)`设置当前项，改变当前单元格的位置

##### 单元格的索引

- `QTableWidgetItem *item(int row, int column)`通过行号和列号返回项
- `int row(const QTableWidgetItem *item)`返回一个项的行号
- `int column(const QTableWidgetItem *item)`返回一个项的列号

##### 水平表头

- `void setHorizontalHeaderItem(int column, QTableWidgetItem *item)`为某列设置项
- `QTableWidgetItem *horizontalHeaderItem(int column)`返回`column`列的表头项
- `QTableWidgetItem *takeHorizontalHeaderItem(int column)`移除`column`列的表头项
- 如果只是设置表头文字而不需要设置任何特殊的格式，可以使用一个简化的函数来实现：`void setHorizontalHeaderLabels(const QStringList &labels)`，这个函数使用一个字符串列表的每一行作为水平表头每一列的标题，使用默认的格式。

##### 垂直表头

- 默认情况下，垂直表头会自动显示行号，不需要专门的操作。垂直表头的每个单元格可以设置一个项或直接设置标题文字。
- `void setVerticalHeaderItem(int row, QTableWidgetItem *item)`设置`row`行的表头项
- `QTableWidgetItem *verticalHeaderItem(int row)`返回`row`行的表头项
- `QTableWidgetItem *takeVerticalHeaderItem(int row)`移除`row`行的表头项
- `void setVerticalHeaderLabels(const QStringList &labels)`用一个字符串列表设置表头标题

##### `QTableWidget`的信号

- `void cellActivated(int row, int column)`单元格被激活时
- `void cellChanged(int row, int column)`单元格的数据内容改变时
- `void cellClicked(int row, int column)`在单元格上点击鼠标时
- `void cellDoubleClicked(int row, int cloumn)`在单元格上双击鼠标时
- `void cellEntered(int row, int column)`鼠标移动到一个单元格上时
- `void cellPressed(int row, int column)`在单元格上按下鼠标左键或右键时
- `void currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)`当前单元格发生切换时
- `void itemActivated(QTableWidgetItem *item)`
- `void itemChanged(QTableWidgetItem *item)`
- `void itemClicked(QTableWidgetItem *item)`
- `void itemDouble(QTableWidgetItem *item)`
- `void itemEntered(QTableWidgetItem *item)`
- `void itemPressed(QTableWidgetItem *item)`
- `void currentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous)`
- `void itemSelectionChanged()`选择的项发生变化时

##### `QTableWidgetItem`类

- 该类没有父类，它存储了单元格的文字及其格式定义。

###### 创建`QTableWidgetItem`对象

- `QTableWidgetItem(const QIcon &icon, const QString &text, int type = Type)`
- `QTableWidgetItem(const QString &text, int type = Type)`
- `QTableWidgetItem(int type = Type)`
- 创建`QTableWidgetItem`对象后，通过`QTableWidget::setItem()`函数可以将其设置为某个单元格的项。

###### 项的设置

- `void setText(const QString &text)`设置单元格显示的文字
- `void setTextAlignment(int alignment)`设置文字对齐方式
- `void setBackground(const QBrush &brush)`设置背景色
- `void setForeground(const QBrush &brush)`设置前景色，即文字的颜色
- `void setFont(const QFont &font)`设置字体
- `void setIcon(const QIcon &icon)`设置图标
- `void setCheckState(Qt::CheckState state)`设置复选状态
- `void setToolTip(const QString &toolTip)`设置`toolTip`
- `void setStatusTip(const QString &statusTip)`设置`statusTip`
- 函数`setFlags()`可设置项的标志。
- 还可以使用`setData()`设置用户数据。
  - `void setData(int role, const QVariant &value)`
  - `QVariant data(int role)`

###### 与表格相关的接口函数

- `int row()`但会项所在单元格的行号
- `int column()`返回项所在单元格的列号
- `void setSelected(bool select)`设置项的选中状态
- `bool isSelected()`项是否被选中，也就是单元格是否被选中
- `QTableWidget *tableWidget()`返回项所在的`QTableWidget`对象指针

##### 自动调整行高和列宽

- 这几个函数是`QTableWidget`的父类`QTableView`中定义的函数：
  - `void resizeColumnToContents(int column)`自动调整列号为`column`的列的宽度
  - `void resizeColumnsToContents()`自动调整所有列的宽度，以适应其内容
  - `void resizeRowToContents(int row)`自动调整行号为`row`的行的高度
  - `void resizeRowsToContents()`自动调整所有行的高度，以适应其内容。

```c++
#include "mainwindow.h"

#include <QDate>
#include <QLabel>
#include <QRandomGenerator>

#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  // 状态栏初始化
  labCellIndex = new QLabel("当前单元格坐标：", this);
  labCellIndex->setMinimumWidth(250);
  labCellType = new QLabel("当前单元格类型：", this);
  labCellType->setMinimumWidth(200);
  labStudID = new QLabel("学生ID：", this);
  labStudID->setMinimumWidth(200);
  // 添加到状态栏
  ui->statusbar->addWidget(labCellIndex);
  ui->statusbar->addWidget(labCellType);
  ui->statusbar->addWidget(labStudID);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_setHorHeaderBtn_clicked() {
  QStringList headerList;
  headerList << "姓名" << "性别" << "出生日期" << "民族" << "分数"
             << "是否党员";
  // ui->tableInfo->setHorizontalHeaderLabels(headerList); // 只设置标题
  // 设置表格列数
  ui->tableInfo->setColumnCount(headerList.size());
  for (int i = 0; i < ui->tableInfo->columnCount(); i++) {
    QTableWidgetItem *headerItem = new QTableWidgetItem(headerList[i]);
    QFont font = headerItem->font();
    font.setBold(true);
    font.setPointSize(11);
    headerItem->setForeground(QBrush(Qt::red));
    headerItem->setFont(font);
    ui->tableInfo->setHorizontalHeaderItem(i, headerItem);
  }
}

void MainWindow::on_initTableDataBtn_clicked() {
  QDate birth(2000, 1, 1);
  ui->tableInfo->clearContents();
  for (int i = 0; i < ui->tableInfo->rowCount(); i++) {
    QString name = QString("学生%1").arg(i);
    QString sex = i % 2 == 0 ? "男" : "女";
    bool isParty = i % 2 != 0;
    int score = QRandomGenerator::global()->bounded(60, 100);
    createItemsARow(i, name, sex, birth, "汉族", isParty, score);
    birth = birth.addDays(20);
  }
}

void MainWindow::createItemsARow(int rowNo, QString name, QString sex,
                                 QDate birth, QString nation, bool isPM,
                                 int score) {
  quint32 studId = 2024103000;
  // 姓名
  QTableWidgetItem *nameItem = new QTableWidgetItem(name, ctName);
  nameItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  studId += rowNo;
  nameItem->setData(Qt::UserRole, QVariant(studId));
  ui->tableInfo->setItem(rowNo, colName, nameItem);
  // 性别
  QIcon icon(sex == "男" ? ":/icons/boy.ico" : ":/icons/girl.ico");
  QTableWidgetItem *sexItem = new QTableWidgetItem(sex, ctSex);
  sexItem->setIcon(icon);
  sexItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  // 性别单元格不允许编辑
  Qt::ItemFlags flags = Qt::ItemIsSelectable | Qt::ItemIsEnabled;
  sexItem->setFlags(flags);
  ui->tableInfo->setItem(rowNo, colSex, sexItem);
  // 出生日期
  QString birthStr = birth.toString("yyyy-MM-dd");
  QTableWidgetItem *birthItem = new QTableWidgetItem(birthStr, ctBirth);
  birthItem->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
  ui->tableInfo->setItem(rowNo, colBirth, birthItem);
  // 民族
  QTableWidgetItem *nationItem = new QTableWidgetItem(nation, ctNation);
  nationItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  ui->tableInfo->setItem(rowNo, colNation, nationItem);
  // 是否党员
  QTableWidgetItem *partyItem = new QTableWidgetItem("党员", ctPartyM);
  partyItem->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
  // "党员"单元格不允许编辑, 但可以更改复选状态
  flags = Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled;
  partyItem->setFlags(flags);
  partyItem->setCheckState(isPM ? Qt::Checked : Qt::Unchecked);
  partyItem->setBackground(QBrush(Qt::yellow));
  ui->tableInfo->setItem(rowNo, colPartyM, partyItem);
  // 分数
  QString scoreStr = QString::number(score);
  QTableWidgetItem *scoreItem = new QTableWidgetItem(scoreStr, ctScore);
  scoreItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  ui->tableInfo->setItem(rowNo, colScore, scoreItem);
}

void MainWindow::on_tableInfo_currentCellChanged(int currentRow,
                                                 int currentColumn,
                                                 int previousRow,
                                                 int previousColumn) {
  Q_UNUSED(previousRow);
  Q_UNUSED(previousColumn);
  QTableWidgetItem *item = ui->tableInfo->item(currentRow, currentColumn);
  if (item == nullptr) {
    return;
  }
  labCellIndex->setText(QString::asprintf("当前单元格坐标: %d 行, %d 列",
                                          currentRow, currentColumn));
  int cellType = item->type();
  labCellType->setText(QString::asprintf("当前单元格类型: %d", cellType));
  QTableWidgetItem *nameItem = ui->tableInfo->item(currentRow, colName);
  quint32 id = nameItem->data(Qt::UserRole).toUInt();
  labStudID->setText(QString::asprintf("学生ID: %d", id));
}

void MainWindow::on_setRowBtn_clicked() {
  int rowNum = ui->spinBox->value();
  ui->tableInfo->setRowCount(rowNum);
}

void MainWindow::on_insertRowBtn_clicked() {
  int curRow = ui->tableInfo->currentRow();
  ui->tableInfo->insertRow(curRow);
  createItemsARow(curRow, "新学生", "女",
                  QDate::fromString("2002-10-1", "yyyy-MM-dd"), "苗族", true,
                  80);
}

void MainWindow::on_addRowBtn_clicked() {
  int curRow = ui->tableInfo->rowCount();
  ui->tableInfo->insertRow(curRow);
  createItemsARow(curRow, "新生", "女",
                  QDate::fromString("2002-6-5", "yyyy-MM-dd"), "满族", false,
                  76);
}

void MainWindow::on_deleteRowBtn_clicked() {
  int curRow = ui->tableInfo->currentRow();
  ui->tableInfo->removeRow(curRow);
}

void MainWindow::on_tableEditableChk_clicked(bool checked) {
  ui->tableInfo->setEditTriggers(checked ? (QAbstractItemView::DoubleClicked |
                                            QAbstractItemView::SelectedClicked)
                                         : QAbstractItemView::NoEditTriggers);
}

void MainWindow::on_showHorHeaderChk_clicked(bool checked) {
  ui->tableInfo->horizontalHeader()->setVisible(checked);
}

void MainWindow::on_showVerHeaderChk_clicked(bool checked) {
  ui->tableInfo->verticalHeader()->setVisible(checked);
}

void MainWindow::on_backColorChk_clicked(bool checked) {
  ui->tableInfo->setAlternatingRowColors(checked);
}

void MainWindow::on_cellSelectRadio_clicked() {
  ui->tableInfo->setSelectionBehavior(QAbstractItemView::SelectItems);
}

void MainWindow::on_rowSelectRadio_clicked() {
  ui->tableInfo->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void MainWindow::on_readTableContentToTextBtn_clicked() {
  QTableWidgetItem *item = nullptr;
  ui->plainTextEdit->clear();
  for (int i = 0; i < ui->tableInfo->rowCount(); i++) {
    QString str = QString::asprintf("第 %d 行: ", i + 1);
    for (int j = 0; j < ui->tableInfo->columnCount() - 1; j++) {
      item = ui->tableInfo->item(i, j);
      str += item->text() + "\t";
    }
    item = ui->tableInfo->item(i, colPartyM);
    str += item->checkState() == Qt::Checked ? "党员" : "群众";
    ui->plainTextEdit->appendPlainText(str);
  }
}
```

### 五、模型/视图结构概述

- 模型/视图结果是一种将数据存储和界面展示分离的编程方法。模型存储数据，视图组件显示模型中的数据，在视图组件里修改的数据会被自动保存到模型里。模型的数据来源可以是内存中的字符串列表或二维表格型数据，也可以是数据库中的数据表，一种模型可以用不同的视图组件来显示数据。

#### 1、模型/视图结构基本原理

- 模型/视图的基本结构包含以下几个部分：
  - 数据源。是原始数据。
  - 视图。也称为视图组件，是界面组件。
  - 模型。也称为数据模型，与数据源通信，并为视图组件提供数据接口。它从源数据提取需要的数据，用于视图组件进行显示和编辑。
  - 代理。在视图与模型之间交互操作时提供的临时编辑器。模型向视图提供数据是单向的，一般仅用于显示。当需要在视图上编辑数据时，代理会为编辑数据提供一个编辑器，这个编辑器获取模型的数据，接受用户编辑的数据后又将其提交给模型。
- 可以将一个模型在不同的视图中显示，也可以为一些特殊源数据设计自定义模型，或者在不修改模型的情况下设计特殊的视图组件。
- 模型、视图和代理使用信号和槽进行通信。当数据源发生变化时，模型发射信号通知视图组件；当用户在界面上操作数据时，视图组件发射信号表示操作信息；在编辑数据时，代理会发射信号告知模型和视图组件编辑器的状态。

#### 2、模型

- 所有基于项（`item`）的模型类都是基于`QAbstractItemModel`类的，这个类定义了视图组件和代理存取数据的接口。
- `QAbstractItemModel`
  - `QFileSystemModel`用于表示计算机上文件系统的模型类
  - `QAbstractListModel`
    - `QStringListModel`用于表示字符串列表数据的模型类
  - `QStandardItemModel`标准的基于项的模型类，每一个项是一个`QStandardItem`对象
  - `QAbstractTableModel`
    - `QSqlQueryModel`用于表示数据库`SQL`查询结果的模型类
    - `QSqlTableModel`用于表示数据库的一个数据表的模型类

#### 3、模型

- `Qt`提供的视图组件主要有：
  - `QListView`：用于显示单列的列表数据，适用于一维数据的操作
  - `QTreeView`：用于显示树状结构数据，适用于树状结构数据的操作。
  - `QTableView`：用于显示表格数据，适用于二维表格数据的操作
  - `QColumnView`：用多个`QListView`显示树状结构数据，树状结构的一层用一个`QListView`显示
  - `QUndoView`：用于显示`undo`指令栈内数据的视图组件，是`QListView`的子类
- 只需调用视图类的`setModel()`函数为视图组件设置一个模型，模型数据就可以显示在视图组件上。在视图组件上修改数据后，数据可以自动保存到模型里。

#### 4、代理

- 代理就是在视图组件上为编辑数据提供的临时编辑器。代理负责从模型获取相应的数据，然后将其显示在编辑器里，修改数据后又将编辑器里的数据保存到模型中。
- `QAbstractItemDelegate`是所有代理类的基类。它有两个子类，即`QItemDelegate`和`QStyledItemDelegate`，这两个类的功能基本相同，`QStyledItemDelegate`能使用`Qt`样式表定义的当前样式绘制代理组件，所以，`QStyledItemDelegate`是视图组件使用的默认的代理类。

#### 5、模型/视图结构的一些概念

##### 模型的基本结构

- 不管底层的数据结构是如何组织数据的，`QAbstractItemModel`的子类都以表格的层次结构展示数据。
- 模型的3种常见展示形式：列表模型、表格模型和树状模型。不管模型的表现形式是怎样的，模型中存储的基本单元都是项，每个项有一个行号和列号，还有一个父项。3个模型都有一个隐藏的根项。

##### 模型索引

- 通过模型能访问的每个项都有一个模型索引，视图组件和代理都通过索引来获取数据。
- `QModelIndex`是表示模型索引的类。模型索引提供访问数据的临时指针，用于通过模型提取和修改数据。因为模型内部组织数据的结构可能随时改变，所以模型索引是临时的。

##### 行号和列号

- 模型的基本形式是用行和列定义的表格数据，但这并不意味着底层的数据是用二维数组存储的，使用行和列只是为了组件之间交互方便。一个模型索引包含行号和列号。
- 要获得一个模型索引，必须提供3个参数：行号、列号、父项的模型索引。

##### 父项

- 当模型为列表或表格结构时，使用行号、列号访问数据比较直观，所有项的父项就是顶层项。当模型为树状结构时（树状结构中，项一般称为节点），一个节点有父节点，其也可以是其他节点的父节点，在构造节点的模型索引时，必须指定正确的行号、列号和父节点。
- 默认的父项是`QModelIndex()`

##### 项的角色

- 在为模型的一个项设置数据时，可以为项设置不同角色的数据。
- `void QAbstractItemModel::setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole)`其中`index`是项的模型索引，`value`是需要设置的数据，`role`是设置数据的角色。
- 可以为一个项设置不同角色的数据，角色参数`role`用枚举类型`Qt::ItemDataRole`的枚举值表示：

| 枚举值                  | 角色数据类型      | 含义                                       |
| ----------------------- | ----------------- | ------------------------------------------ |
| `Qt::DisplayRole`       | `QString`         | 界面上显示的字符串，例如单元格显示的文字   |
| `Qt::DecorationRole`    | `QIcon`、`QColor` | 在界面上起装饰作用的数据，如图标           |
| `Qt::EditRole`          | `QString`         | 界面上适合在编辑器中显示的数据，一般是文字 |
| `Qt::ToolTipRole`       | `QString`         | 项的`toolTip`字符串                        |
| `Qt::StatusTipRole`     | `QString`         | 项的`statusTip`字符串                      |
| `Qt::FontRole`          | `QFont`           | 项的字体，如单元格内文字的字体             |
| `Qt::TextAlignmentRole` | `Qt::Alignment`   | 项的对齐方式，如单元格内文字的对齐方式     |
| `Qt::BackgroundRole`    | `QBrush`          | 项的背景色，如单元格的背景色               |
| `Qt::ForegroundRole`    | `QBrush`          | 项的前景色，如单元格的文字颜色             |
| `Qt::CheckStateRole`    | `Qt::CheckState`  | 项的复选状态                               |
| `Qt::UserRole`          | `QVariant`        | 自定义的用户数据                           |

- 在获取一个项的数据时也需要指定角色，以获取不同角色的数据。
  - `QVariant QAbstractItemModel::data(const QModelIndex &index, int role = Qt::DisplayRole)`
- 通过为一个项的不同角色定义数据，可以告知视图组件和代理如何展示数据。不同的视图组件对各种角色数据的解释和展示可能不一样，也可能会忽略某些角色的数据。

##### `QAbstractItemModel`类

- 这些函数一般都是虚函数，子类或重新实现其需要用到的函数，以符合模型类的具体操作。

###### 行数和列数

- `int rowCount(const QModelIndex &parent = QModelIndex())`
- `int columnCount(const QModelIndex &parent = QModelIndex())`

###### 插入或删除行

- `bool insertRow(int row, const QModelIndex &parent = QModelIndex())`
- `bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex())`
- `bool removeRow(int row, const QModelIndex &parent = QModelIndex())`
- `bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex())`
- 如果`row`的值超过了模型的行数，新增的行就添加到模型的末尾。

###### 插入或删除列

- `bool insertColumn(int column, const QModelIndex &parent = QModelIndex())`
- `bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex())`
- `bool removeColumn(int column, const QModelIndex &parent = QModelIndex())`
- `bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex())`

###### 移动行或列

- `bool moveRow(const QModelIndex &sourceParent, int sourceRow, const QModelIndex &destinationParent, int destinationChild)`
- `bool moveColumn(const QModelIndex &sourceParent, int sourceColumn, const QModelIndex &destinationParent, int destinationChild)`

###### 数据排序

- `void sort(int column, Qt::SortOrder order = Qt::AscendingOrder)`

###### 设置和读取项的数据

- `bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole)`
- `QVariant data(const QModelIndex &index, int role = Qt::DisplayRole)`

###### 清除一个项的数据

- `bool clearItemData(const QModelIndex &index)`

##### `QAbstractItemView`类

###### 关联数据模型和选择模型

- `void setModel(QAbstractItemModel *model)`设置数据模型
- `QAbstractItemModel  *model()`返回关联的数据模型对象指针
- 视图组件还可以设置选择模型，在界面上选则的项发生变化时，通过选择模型可以获取所有被选择项的模型索引。
- `void setSelectionModel(QItemSelectionModel *selectionModel)`设置选择模型
- `QItemSelectionModel *selectionModel()`返回关联的选择模型对象指针

###### 常用属性

- `editTriggers`。表示视图组件是否可以编辑数据，以及进入编辑状态的方式。
  - `void setEditTriggers(QAbstractItemView::EditTriggers triggers)`
  - `QAbstractItemView::EditTriggers editTriggers()`
  - 该属性值是标志类型`QAbstractItemView::EditTriggers`：
    - `NoEditTriggers`不允许编辑
    - `CurrentChanged`当前项变化时进入编辑状态
    - `DoubleClicked`双击一个项时进入编辑状态
    - `SelectedClicked`点击一个已选择的项时进入编辑状态
    - `AnyKeyPressed`任何键被按下时进入编辑状态
    - `AllEditTriggers`发生以上任何动作时进入编辑状态
  - 视图组件类和模型类都没有`readonly`属性，如果要设置数据是只读的，用函数`setEditTriggers()`设置视图组件为不允许编辑即可。
- `alternatingRowColors`这个属性设置各行是否交替使用不同的背景色。如果设置为`true`，会使用系统默认的一种颜色。如果要自定义背景色，需要使用`Qt`样式表
- `selectionMode`这个属性表示在视图组件上选择项的操作模式，对于`QTableView`比较有意义。这个属性值是枚举类型`QAbstractItemView::SelectionMode`
  - `SingleSelection`单选，只能选择一个项。
  - `ContiguousSelection`连续选择，例如按住<kbd>Shift</kbd>键连续选择多个不连续的单元格。
  - `ExtendedSelection`扩展选择，例如可以按住`Ctrl`键选择多个连续单元格。
  - `MultiSelection`多选，例如通过拖动鼠标选择多个单元格。
  - `NoSelection`不允许选择
- `selectionBehavior`表示点击鼠标时选择操作的行为，对于`QTableView`比较有意义。这个属性值是枚举类型`QAbstractItemView::SelectionBehavior`：
  - `SelectItems`选择单个项，点击一个单元格时，就是选择这个单元格。
  - `SelectRows`选择行，点击一个单元格时，就是选择单元格所在的一整行
  - `SelectColumns`选择列，点击一个单元格时，选择单元格所在的一整列。

###### 常用接口函数

- `QModelIndex currentIndex()`返回当前项的模型索引，例如当前单元格的模型索引
- `void setCurrentIndex(const QModelIndex &index)`设置模型索引为`index`的项为当前项
- `void selectAll()`选择视图中的所有项
- `void clearSelection()`清除所有选择
- 如果设置为单选，视图组件上就只有一个当前项，函数`currentIndex()`返回当前项的模型索引，通过模型索引就可以从模型中获取项的数据。

###### 常用信号

- `void clicked(const QModelIndex &index)`点击某个项时
- `void doubleClicked(const QModelIndex &index)`双击某个项时
- `void entered(const QModelIndex &index)`鼠标移动到某个项上时
- `void pressed(const QModelIndex &index)`鼠标左键或右键被按下时

#### 6、`QStringListModel`和`QListView`

- `QStringListModel`内部存储了一个字符串列表，这个字符串列表的内容自动显示在关联的`QListView`组件上。在`QListView`组件上双击某一行时，可以通过默认的代理组件(`QLineEdit`)修改这一行字符串的内容，修改后的这行字符串自动保存到数据模型的字符串列表里。
- 在字符串列表中添加或删除行是通过`QStringListModel`的接口函数实现的，`QListView`没有接口函数用于修改数据，它只是用作数据显示和编辑的界面组件。
- `QStringListModel(const QStringList &strings, QObject *parent = nullptr)`
- `QStringListModel(QObject *parent = nullptr)`
- `void setStringList(const QStringList &strings)`设置字符串列表，初始化模型数据
- `QStringList stringList()`返回模型内部的字符串列表。

```c++
#include "mainwindow.h"

#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  stringList << "北京" << "上海" << "天津" << "河北" << "山东" << "四川"
             << "重庆" << "广东" << "河南";
  model = new QStringListModel(stringList, this);
  ui->listView->setModel(model);
  ui->listView->setEditTriggers(QAbstractItemView::DoubleClicked |
                                QAbstractItemView::SelectedClicked);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_pushButton_clicked() { model->setStringList(stringList); }

void MainWindow::on_pushButton_2_clicked() {
  model->removeRows(0, model->rowCount());
}

void MainWindow::on_pushButton_3_clicked() {
  // 在末尾插入一个项
  model->insertRow(model->rowCount());
  // 获取刚插入项的模型索引
  QModelIndex index = model->index(model->rowCount() - 1, 0);
  model->setData(index, "new item", Qt::DisplayRole);
  ui->listView->setCurrentIndex(index);
}

void MainWindow::on_pushButton_4_clicked() {
  QModelIndex index = ui->listView->currentIndex();
  model->insertRow(index.row());
  model->setData(index, "insert item", Qt::DisplayRole);
  ui->listView->setCurrentIndex(index);
}

void MainWindow::on_pushButton_5_clicked() {
  QModelIndex index = ui->listView->currentIndex();
  model->removeRow(index.row());
}

void MainWindow::on_pushButton_11_clicked() {
  int curRow = ui->listView->currentIndex().row();
  QModelIndex index = QModelIndex();
  model->moveRow(index, curRow, index, curRow - 1);
}

void MainWindow::on_pushButton_12_clicked() {
  int curRow = ui->listView->currentIndex().row();
  QModelIndex index = QModelIndex();
  model->moveRow(index, curRow, index, curRow + 2);
}

void MainWindow::on_pushButton_13_clicked(bool checked) {
  model->sort(0, checked ? Qt::AscendingOrder : Qt::DescendingOrder);
}

void MainWindow::on_checkBox_clicked(bool checked) {
  ui->listView->setEditTriggers(checked ? (QAbstractItemView::DoubleClicked |
                                           QAbstractItemView::SelectedClicked)
                                        : (QAbstractItemView::NoEditTriggers));
}

void MainWindow::on_listView_clicked(const QModelIndex &index) {
  QString str1 = QString::asprintf("模型索引：row = %d, column = %d;",
                                   index.row(), index.column());
  QVariant var = model->data(index, Qt::DisplayRole);
  QString str2 = var.toString();
  ui->statusbar->showMessage(str1 + str2);
}

void MainWindow::on_pushButton_15_clicked() {
  QStringList tmpList = model->stringList();
  ui->plainTextEdit->clear();
  for (int i = 0; i < tmpList.size(); i++) {
    ui->plainTextEdit->appendPlainText(tmpList[i]);
  }
}
```

#### 7、`QStandardItemModel`和`QTableView`

- `QStandardItemModel`是基于项的模型类，每一个项是一个`QStandardItem`对象，可以存储各种数据。

##### `QTableView`类

- `void QTableView::setHorizontalHeader(QHeaderView *header)`设置水平表头
- `void QTableView::setVerticalHeader(QHeaderView *header)`设置垂直表头
- `QHeaderView *QTableView::horizontalHeader()`返回水平表头对象指针
- `QHeaderView *QTableView::verticalHeader()`返回垂直表头对象指针
- 当`QTableView`组件使用一个`QStandardItemModel`对象作为数据模型时，它会自动创建表头对象，垂直表头一般显示行号，水平表头一般显示列的标题。

##### `QStandardItemModel`类

###### 设置行数和列数

- `void setRowCount(int rows)`设置数据模型的行数
- `void setColumnCount(int columns)`设置数据模型的列数
- 如果设置的列数大于1，模型就是表格模型，如果设置的列数为1，模型就可以看做是列表模型。

###### 设置项

- `void setItem(int row, int column, QStandardItem *item)`用于表格模型
- `void setItem(int row, QStandardItem *item)`用于列表模型

###### 获取项

- `QStandardItem  *item(int row, int column = 0)`根据行号和列号返回项
- `QStandardItem *itemFromIndex(const QModelIndex &index)`根据模型索引返回项
- `QModelIndex indexFromItem(const QStandardItem *item)`根据项返回其模型索引

###### 添加行或列

- `void appendRow(const QList<QStandardItem *> &items)`用于表格模型
- `void appendRow(QStandardItem *item)`用于列表模型
- `void appendColumn(const QList<QStandardItem *> &items)`在表格模型中添加列

###### 插入行或列

- `void insertRow(int row, const QList<QStandardItem *> &items)`用于表格模型
- `void insertRow(int row, QStandardItem *item)`用于列表模型
- `bool insertRow(int row, const QModelIndex &parent = QModelIndex())`用于树状模型
- `void insertColumn(int column, const QList<QStandardItem *> &items)`用于表格模型
- `bool insertColumn(int column, const QModelIndex &parent = QModelIndex())`用于树状模型

###### 移除行、列或项

- 可以从表格模型中移除一列或一行，模型的列数和行数就会相应减一，但是移除的`QStandardItem`对象不会被删除，需要单独使用`delete`删除。
- `QList<QStandardItem *> takeRow(int row)`移除一行，适用于表格模型
- `QList<QStandardItem *> takeColumn(int column)`移除一列，适用于表格模型
- `QStandardItem *takeItem(int row, int column = 0)`移除一个项，适用于列表模型

###### 水平表头和垂直表头

- `void settHorizontalHeaderItem(int column, QStandardItem *item)`为水平表头某列设置项
- 如果只是设置表头各列的文字，可以使用函数`settHorizontalHeaderLabels()`，它用一个字符串列表的内容设置表头各列的文字。
  - `void settHorizontalHeaderLabels(const QStringList &labels)`
- `QStandardItem *horizontalHeaderItem(int column)`返回水平表头中的一个项
- `QStandardItem *takeHorizontalHeaderItem(int column)`移除水平表头中的一个项
- `void setVerticalHeaderItem(int row, QStandardItem *item)`
- `void setVerticalHeaderILabels(const QStringList &labels)`
- `QStandardItem *verticalHeaderItem(int row)`
- `QStandardItem *takeVerticalHeaderItem(int row)`

###### 函数`clear()`

- 用于清除模型内的所有项，行数和列数都会变为0

###### `QStandardItemModel`的信号

- `void itemChanged(QStandardItem *item)`任何一个项的数据发生变化时，此信号就会被发射。

##### `QStandardItem`类

- `QStandardItemModel`管理的是模型的顶层项，如果是列表模型或表格模型，就直接管理模型中的所有项。如果是树状模型，`QStandardItemModel`管理的就是所有的顶层项（不是根项），也就是目录树中的一级节点，而各个一级节点的直接子节点则通过`QStandardItem`类来管理，依次递推下去。

###### 特性读写函数

| 读取函数           | 设置函数             | 设置函数的功能                                          |
| ------------------ | -------------------- | ------------------------------------------------------- |
| `text()`           | `setText()`          | 设置项的显示文字                                        |
| `toolTip()`        | `setToolTip()`       | 设置`toolTip`文字，是鼠标处显示的提示信息               |
| `statusTip()`      | `setStatusTip()`     | 设置`statusTip`文字，是在状态栏上显示的提示信息         |
| `icon()`           | `setIcon()`          | 设置项的图标                                            |
| `font()`           | `setFont()`          | 设置显示文字的字体                                      |
| `textAlignment()`  | `setTextAlignment()` | 设置显示文字的对齐方式                                  |
| `foreground()`     | `setForeground()`    | 设置项的前景色                                          |
| `background()`     | `setBackground()`    | 设置项的背景色                                          |
| `isEnabled()`      | `setEnabled()`       | 设置项是否使能，如果是`true`，用户就可以交互操作这个项  |
| `isEditable()`     | `setEditable()`      | 设置项是否可以编辑                                      |
| `isSelectable()`   | `setSelectable()`    | 设置项是否可以被选择                                    |
| `isCheckable()`    | `setCheckable()`     | 设置项是否可复选，如果设置为`true`会出现复选框          |
| `checkState()`     | `setCheckState()`    | 设置项的复选状态                                        |
| `isAutoTristate()` | `setAutoTristate()`  | 设置是否自动改变3种复选状态，对于树状模型的节点比较有用 |
| `isUserTristate()` | `setUserTristate()`  | 设置是否由用户决定3种复选状态                           |
| `flags()`          | `setFlags()`         | 设置项的标志                                            |
| `row()`            | ——                   | 返回自身在父项的所有子项中的行号                        |
| `column()`         | ——                   | 返回自身在父项的所有子项中的列号                        |

###### 用户自定义数据

- `void QStandardItem::setData(const QVariant &value, int role = Qt::UserRole + 1)`
- `QVariant QStandardItem::data(int role = Qt::UserRole + 1)`
- `void QStandardItem::clearData()`清除所有角色的数据

###### 管理子项的函数

- `void appendRow(const QList<QStandardItem *> &items)`添加一行多个项
- `void appendRow(QStandardItem *item)`添加一行，只有一个项
- `void appendColumn(const QList<QStandardItem *> &items)`添加一列多个项
- `void insertRow(int row, const QList<QStandardItem *> &items)`插入一行多个项
- `void insertRow(int row, QStandardItem *item)`插入一行，只有一个项
- `void insertRows(int row, int count)`在`row`行插入`count`个空行，未设置`item`
- `void insertColumn(int column, const QList<QStandardItem *> &items)`插入一列多个项
- `void insertCloumns(int column, int count)`在`column`列插入`count`个空列，未设置`item`
- `void removeColumn(int column)`删除序号为`column`的列
- `void removeColumns(int column, int count)`从`column`列开始，删除`count`个列
- `void removeRow(int row)`删除序号为`row`的行，存储的项也被删除
- `void removeRows(int row, int count)`从`row`行开始，删除`count`个行
- `int rowCount()`返回子项的行数
- `int columnCount()`返回子项的列数
- `bool hasChildren()`这个项是否有子项
- `QStandardItem *child(int row, int column = 0)`根据行号和列号返回子项。

##### `QItemSelectionModel`类

- 它的功能是跟踪视图组件上的选择操作，给出选择范围。

###### 主要接口函数

- `void QItemSelectionModel::setModel(QAbstractItemModel *model)`为选择模型设置数据模型
- `bool hasSelection()`是否有被选择的项
- `QModelIndex currentIndex()`返回当前项的模型索引
- `bool isSelected(const QModelIndex &index)`模型索引为`index`的项是否被选中
- `QModelIndexList selectedIndexed()`返回所有被选择项的模型索引列表，列表未排序
- `QModelIndexList selectedRows(int column = 0)`返回`column`列所有被选择项的模型索引列表
- `QModelIndexList selectedColumns(int row = 0)`返回`row`行所有被选择项的模型索引列表
- `void clear()`清除选择模型，会触发`selectionChanged()`和`currentChanged()`信号
- `void clearCurrentIndex()`清除当前索引，会触发`currentChanged()`信号
- `void clearSelection()`清除所有选择，会触发`selectionChanged()`信号

###### 信号

- `void currentChanged(const QModelIndex &current, const QModelIndex &previous)`选择的当前项发生变化时
- `void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)`选择发生变化时

#### 8、自定义代理

- 在模型/视图结构中，代理的作用就是在视图组件进入编辑状态编辑某个项时，提供一个临时的编辑器用于数据编辑，编辑完成后再把数据提交给数据模型。
- 要替换`QTableView`组件提供的默认代理组件，就需要为`QTableView`组件的某列或某个单元格设置自定义代理。自定义代理类需要从`QStyledItemDelegate`类继承，创建自定义代理类的实例后，再将其设置为整个视图组件或视图组件的某行某列的代理，以替代默认代理的功能。
- `void setItemDelegate(QAbstractItemDelegate *delegate)`
- `void setItemDelegateForColumn(int column, QAbstractItemDelegate *delegate)`
- `void setItemDelegateForRow(int row, QAbstractItemDelegate *delegate)`

##### `QStyledItemDelegate`类

- 该类是视图组件使用的默认代理类，一般使用该类作为自定义代理类的父类。需要重新实现4个虚函数。

###### `createEditor()`

- 可创建用于编辑模型数据的界面组件，称为代理编辑器。
- `QWidget *QStyledItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index)`
- 其中`parent`是要创建的组件的父组件，一般就是窗口对象；`option`是项的一些显示选项，包含字体、对齐方式、背景色等属性；`index`是项在数据模型中的模型索引，通过`index->model()`可以获取项所属数据模型的对象指针。
- 在视图组件上双击一个单元格使其进入编辑状态时，系统就会自动调用该方法创建代理编辑器。

###### `setEditorData()`

- 功能是从数据模型获取项的某个角色（一般是`EditRole`角色）的数据，然后将其设置为代理编辑器上显示的数据。
- `void QStyledItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index)`

###### `setModelData()`

- 完成对当前单元格的编辑时，系统会自动调用该函数。其功能是将代理编辑器里的输入数据保存到数据模型的项里。
- `void QStyledItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index)`

###### `updateEditorGeometry()`

- 视图组件在界面上显示代理编辑器时，需要调用该方法为组件设置合适的大小。
- `void QStyledItemDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index)`
- 其中，变量`option->rect`是`QRect`类型，表示代理编辑器的建议大小，一般设置为：`editor->setGeometry(option.rect)`

```c++
#include "tfloatspindelegate.h"

#include <QDoubleSpinBox>
TFloatSpinDelegate::TFloatSpinDelegate() {}

QWidget *TFloatSpinDelegate::createEditor(QWidget *parent,
                                          const QStyleOptionViewItem &option,
                                          const QModelIndex &index) {
  Q_UNUSED(option);
  Q_UNUSED(index);
  QDoubleSpinBox *editor = new QDoubleSpinBox(parent);
  editor->setFrame(false);
  editor->setMinimum(0);
  editor->setMaximum(20000);
  editor->setDecimals(2);
  return editor;
}

void TFloatSpinDelegate::setEditorData(QWidget *editor,
                                       const QModelIndex &index) {
  float value = index.model()->data(index, Qt::EditRole).toFloat();
  QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox *>(editor);
  spinBox->setValue(value);
}

void TFloatSpinDelegate::setModelData(QWidget *editor,
                                      QAbstractItemModel *model,
                                      const QModelIndex &index) {
  QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox *>(editor);
  float value = spinBox->value();
  QString str = QString::asprintf("%.2f", value);
  model->setData(index, str, Qt::EditRole);
}

void TFloatSpinDelegate::updateEditorGeometry(
    QWidget *editor, const QStyleOptionViewItem &option,
    const QModelIndex &index) {
  Q_UNUSED(index);
  editor->setGeometry(option.rect);
}
```

```c++
  // 设置自定义代理
  intSpinDelegate = new TFloatSpinDelegate(this);
  ui->tableView->setItemDelegateForColumn(0, intSpinDelegate);
  floatSpinDelegate = new TFloatSpinDelegate(this);
  ui->tableView->setItemDelegateForColumn(1, floatSpinDelegate);
  ui->tableView->setItemDelegateForColumn(2, floatSpinDelegate);
  ui->tableView->setItemDelegateForColumn(3, floatSpinDelegate);
  comboDelegate = new TComboBoxDelegate(this);
  QStringList strList;
  strList << "优" << "良" << "一般" << "不合格";
  comboDelegate->setItems(strList, false);
  ui->tableView->setItemDelegateForColumn(4, comboDelegate);
```

#### 9、`QFileSystemModel`和`QTreeView`

- `QFileSystemModel`为本机的文件系统提供一个模型。使用它提供的接口函数，我们可以创建目录、删除目录、重命名目录，可以获得文件名称、目录名称、文件大小等，还可以获得文件的详细信息。
- 要通过`QFileSystemModel`获得本机的文件系统，需要使用`setRootPath()`设置一个根目录。

##### `QFileSystemModel`类

###### 设置根目录

- `QModelIndex setRootPath(const QString &newPath)`
- 有两个函数用于返回`QFileSystemModel`当前根目录：
  - `QDit rootDirectory()`以`QDir`类型返回当前根目录
  - `QString rootPath()`以`QString`类型返回当前根目录
- 用函数`index()`可以根据完整的目录或文件名字符串，返回目录或文件在文件系统模型中的模型索引。
  - `QModelIndex index(const QString &path, int column = 0)`返回目录或文件的模型索引

###### 设置模型选项

- 模型项的过滤器。`void setFilter(QDir::Filters filters)`
  - `QDir::AllDirs`列出所有目录。必须包含这个选项
  - `QDir::Files`列出文件
  - `QDir::Drives`列出驱动器
  - `QDir::NoDotAndDotDot`不列出目录下的"."和".."特殊项
  - `QDir::Hidden`列出隐藏的文件
  - `QDir::System`列出系统文件
- 文件名过滤器。
  - `void setNameFilters(const QStringList &filters)`设置文件名过滤器
  - 一般使用通配符表示。
  - `void setNameFilterDisables(bool enable)`可以设置未通过文件名过滤器过滤的项的显示方式。如果`enable`设置为`true`，未通过文件名过滤器过滤的项只是被设置为禁用，否则被隐藏。
- 模型选项。
  - `void setOption(QFileSystemModel::Option option, bool on = true)`
  - `QFileSystemModel::DontWatchForChanges`不监视文件系统的变化，默认是监视
  - `QFileSystemModel::DontResolveSymlinks`不解析文件系统的符号连接项，默认是解析
  - `QFileSystemModel::DontUseCustomDirectoryIcons`不使用自定义的目录图标，默认是使用系统的图标。

###### 获取目录和文件的信息

- `QIcon fileIcon(const QModelIndex &index)`返回项的图标
- `QFileInfo fileInfo(const QModelIndex &index)`返回项的文件信息
- `QString fileName(const QModelIndex &index)`返回不含路径的文件名或最后一级文件夹名称
- `QString filePath(const QModelIndex &index)`返回项的路径或包含路径的文件名
- `QDateTime lastModified(const QModelIndex &index)`返回项的最后修改日期
- `bool isDir(const QModelIndex &index)`判断项是不是一个文件夹
- `qint64 size(const QModelIndex &index)`返回文件的大小（字节数），若是文件夹，返回值为0
- `QString type(const QModelIndex &index)`返回项的类型描述文字
- 参数`index`表示模型中的一个项。

###### 操作目录和文件

- `QModelIndex mkdir(const QModelIndex &parent, const QString &name)`创建目录
- `bool rmdir(const QModelIndex &index)`删除目录
- `bool remove(const QModelIndex &index)`删除文件
- 如果不想修改文件系统，可以使用`QFileSystemModel::setReadOnly()`将模型设置为只读的。

```c++
#include "mainwindow.h"

#include <QFileDialog>

#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  fileSystemModel = new QFileSystemModel(this);
  // 设置根目录
  fileSystemModel->setRootPath(QDir::currentPath());
  ui->treeView->setModel(fileSystemModel);
  ui->listView->setModel(fileSystemModel);
  ui->tableView->setModel(fileSystemModel);
  connect(ui->treeView, SIGNAL(clicked(QModelIndex)), ui->listView,
          SLOT(setRootIndex(QModelIndex)));
  connect(ui->treeView, SIGNAL(clicked(QModelIndex)), ui->tableView,
          SLOT(setRootIndex(QModelIndex)));
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_treeView_clicked(const QModelIndex &index) {
  ui->isDirChk->setChecked(fileSystemModel->isDir(index));
  ui->pathLabel->setText(fileSystemModel->filePath(index));
  ui->fileTypeLabel->setText(fileSystemModel->type(index));
  ui->filenameLabel->setText(fileSystemModel->fileName(index));
  int fileSize = fileSystemModel->size(index) / 1024;
  if (fileSize < 1024) {
    ui->fileSizeLabel->setText(QString("%1 KB").arg(fileSize));
  } else {
    ui->fileSizeLabel->setText(QString::asprintf("%.1f MB", fileSize / 1024.0));
  }
}

void MainWindow::on_actSetRootDir_triggered() {
  QString rootDir =
      QFileDialog::getExistingDirectory(this, "选择目录", QDir::currentPath());
  if (!rootDir.isEmpty()) {
    fileSystemModel->setRootPath(rootDir);
    ui->treeView->setRootIndex(fileSystemModel->index(rootDir));
  }
}

void MainWindow::on_radioButton_clicked() {
  ui->checkBox->setEnabled(true);
  fileSystemModel->setFilter(QDir::AllDirs | QDir::Files |
                             QDir::NoDotAndDotDot);
}

void MainWindow::on_radioButton_2_clicked() {
  ui->pushButton->setEnabled(false);
  ui->comboBox->setEnabled(false);
  ui->checkBox->setEnabled(false);
  fileSystemModel->setFilter(QDir::AllDirs | QDir::NoDotAndDotDot);
}

void MainWindow::on_checkBox_clicked(bool checked) {
  fileSystemModel->setNameFilterDisables(!checked);
  ui->comboBox->setEnabled(checked);
  ui->pushButton->setEnabled(checked);
}

void MainWindow::on_pushButton_clicked() {
  QString flts = ui->comboBox->currentText().trimmed();
  QStringList filter = flts.split(";", Qt::SkipEmptyParts);
  fileSystemModel->setNameFilters(filter);
}
```

### 六、事件处理

#### 1、`Qt`的事件系统

##### 事件的产生和派发

###### 事件的产生

- 在`Qt`中，事件是对象，是`QEvent`类或其派生类的实例。
- 按事件的来源，可以分为三类
  - 自生事件：是由窗口系统产生的事件。自生事件会进入系统队列，然后被应用程序的事件循环逐个处理
  - 发布事件：是由`Qt`或应用程序产生的事件。应用程序使用静态函数`QCoreApplication::postEvent()`产生发布事件，发布事件会进入`Qt`事件队列，然后由应用程序的事件循环进行处理。
  - 发送事件：是由`Qt`或应用程序定向发送给某个对象的事件。应用程序使用静态函数`QCoreApplication::sendEvent()`产生发送事件，由对象的`event()`函数直接管理
- 自生事件和发布事件的处理是异步的，也就是事件进入队列后由系统去处理，程序不会在产生事件的地方停止进行等待。
- 应用程序使用静态函数`QCoreApplication::postEvent()`发布事件：
  - `void QCoreApplication::postEvent(QObject *receiver, QEvent *event, int priority = Qt::NormalEventPriority)`
- 在程序调用`QCoreApplication::postEvent()`发布一个事件后，这个函数立刻就会退出，所以，发布事件的处理是异步的。
- `bool QCoreApplication::sendEvent(QObject *receiver, QEvent *event)`这个函数是以同步模式运行的，也就是需要等待对象处理完事件后才退出。

###### 事件的派发

- 函数`QApplication::exec()`的主要功能就是不断地检查系统队列和`Qt`事件队列里是否有未处理的自生事件和发布事件。应用程序的事件循环还可以对队列中的相同事件进行合并处理。
- 解决界面迟滞或无响应的方法：
  - 使用多线程
  - 在长时间占用`CPU`的代码段中，偶尔调用`QCoreApplication`的静态函数`processEvents()`将事件队列里未处理的事件派发出去，让事件接收对象及时处理。
    - `void QCoreApplication::processEvents(QEventLoop::ProcessEventsFlags flags = QEventLoop::AllEvents)`
    - `QEventLoop::AllEvents`处理所有事件
    - `QEventLoop::ExcludeUserInputEvents`排除用户事件，例如鼠标和键盘的事件
    - `QEventLoop::ExcludeSocketNotifiers`排除网络`socket`的通知事件
    - `QEventLoop::WaitForMoreEvents`如果没有未处理的事件，等待更多事件
- `QCoreApplication`还有一个派发事件的静态函数：
  - `void QCoreApplication::sendPostedEvents(QObject *reciver = nullptr, int event_type = 0)`
  - 功能是把前面使用静态函数`QCoreApplication::postEvent()`发送到`Qt`事件队列里的事件立刻派发出去。如果不指定`event_type`，只指定`reciver`，就派发所有这个接受者的事件，如果都不指定，就派发所有用`QCoreApplication::postEvent()`发布的事件。

##### 事件类和事件类型

- `QEvent`有以下几个主要的接口函数：
  - `void accept()`接受事件，设置事件对象的接收标志（`accept flag）`
  - `void ignore()`忽略事件，消除事件对象的接收标志
  - `bool isAccepted()`是否接收事件
  - `bool isInputEvent()`事件对象是不是`QInputEvent`或其派生类的实例
  - `bool isPointerEvent()`事件对象是不是`QPointerEvent`或其派生类的实例
  - `bool isSinglePointEvent()`事件对象是不是`QSinglePointEvent`或其派生类的实例
  - `bool spontaneous()`是不是自生事件，也就是窗口系统的事件
  - `QEvent::Type type()`事件类型
- 常见的事件类型及其所属的事件类如下：

<table>
    <tr>
    	<th>事件类</th>
        <th>事件类型</th>
        <th>事件描述</th>
    </tr>
    <tr>
    	<td rowspan="4"><code>QMouseEvent</code></td>
        <td><Code>QEvent::MouseMove</Code></td>
        <td>鼠标移动</td>
    </tr>
    <tr>
    	<td><code>QEvent::MouseButtonDblClick</code></td>
        <td>鼠标双击</td>
    </tr>
    <tr>
    	<td><code>QEvent::MouseButtonPress</code></td>
        <td>鼠标按键按下，可以是左键或右键</td>
    </tr>
    <tr>
    	<td><code>QEvent::MouseButtonRelease</code></td>
        <td>鼠标按键释放，可以是左键或右键</td>
    </tr>
    <tr>
    	<td><code>QWheelEvent</code></td>
        <td><Code>QEvent::QWheelEvent</Code></td>
        <td>鼠标滚轮滚动</td>
    </tr>
    <tr>
    	<td rowspan="3"><code>QHoverEvent</code></td>
        <td><Code>QEvent::HoverEnter</Code></td>
        <td>鼠标光标移动到组件上方并悬停，组件需要设置<code>Qt::WA_Hover</code>属性才会产生悬停类的事件</td>
    </tr>
    <tr>
    	<td><code>QEvent::HoverLeave</code></td>
        <td>鼠标光标离开某个组件上方</td>
    </tr>
    <tr>
    	<td><code>QEvent::HoverMove</code></td>
        <td>鼠标光标在组件上方移动</td>
    </tr>
    <tr>
    	<td><code>QEnterEvent</code></td>
        <td><Code>QEvent::Enter</Code></td>
        <td>鼠标光标进入组件或窗口边界范围内</td>
    </tr>
    <tr>
    	<td><code>QEvent</code></td>
        <td><Code>QEvent::Leave</Code></td>
        <td>鼠标光标离开组件或窗口边界范围，注意这个事件类型使用的事件类就是<code>QEvent</code></td>
    </tr>
    <tr>
    	<td rowspan="2"><code>QKeyEvent</code></td>
        <td><Code>QEvent::KeyPress</Code></td>
        <td>键盘按键按下</td>
    </tr>
    <tr>
    	<td><code>QEvent::KeyRelease</code></td>
        <td>键盘按键释放</td>
    </tr>
    <tr>
    	<td rowspan="3"><code>QFocusEvent</code></td>
        <td><Code>QEvent::FocusIn</Code></td>
        <td>组件或窗口获得键盘的输入焦点</td>
    </tr>
    <tr>
    	<td><code>QEvent::FocusOut</code></td>
        <td>组件或窗口失去键盘的输入焦点</td>
    </tr>
    <tr>
    	<td><code>QEvent::FocusAboutToChange</code></td>
        <td>组件或窗口的键盘输入焦点即将变化</td>
    </tr>
    <tr>
    	<td><code>QShowEvent</code></td>
        <td><Code>QEvent::Show</Code></td>
        <td>窗口在屏幕上显示出来，或组件变得可见</td>
    </tr>
    <tr>
    	<td><code>QHideEvent</code></td>
        <td><Code>QEvent::Hide</Code></td>
        <td>窗口在屏幕上显示隐藏（例如窗口最小化），或组件变得不可见</td>
    </tr>
    <tr>
    	<td><code>QMoveEvent</code></td>
        <td><Code>QEvent::Move</Code></td>
        <td>组件或窗口的位置移动</td>
    </tr>
    <tr>
    	<td><code>QCloseEvent</code></td>
        <td><Code>QEvent::Close</Code></td>
        <td>窗口被关闭，或组件被关闭，例如<code>QTabWidget</code>的一个页面被关闭</td>
    </tr>
    <tr>
    	<td><code>QPaintEvent</code></td>
        <td><Code>QEvent::Paint</Code></td>
        <td>界面组件需要更新重绘</td>
    </tr>
    <tr>
    	<td><code>QResizeEvent</code></td>
        <td><Code>QEvent::Resize</Code></td>
        <td>窗口或组件改变大小</td>
    </tr>
    <tr>
    	<td><code>QStatusTipEvent</code></td>
        <td><Code>QEvent::StatusTip</Code></td>
        <td>请求显示组件的<code>statusTip</code>信息</td>
    </tr>
    <tr>
    	<td rowspan="2"><code>QHelpEvent</code></td>
        <td><Code>QEvent::ToolTip</Code></td>
        <td>请求显示组件的<code>toolTip</code>信息</td>
    </tr>
    <tr>
    	<td><code>QEvent::WhatsThis</code></td>
        <td>请求显示组件的<code>whatsThis</code>信息</td>
    </tr>
    <tr>
    	<td><code>QDragEnterEvent</code></td>
        <td><Code>QEvent::DragEnter</Code></td>
        <td>在拖放操作中，鼠标光标移动到组件上方</td>
    </tr>
    <tr>
    	<td><code>QDragLeaveEvent</code></td>
        <td><Code>QEvent::DragLeave</Code></td>
        <td>在拖放操作中，鼠标光标离开了组件</td>
    </tr>
    <tr>
    	<td><code>QDropEvent</code></td>
        <td><Code>QEvent::Drop</Code></td>
        <td>拖放操作完成，即放下拖动的对象</td>
    </tr>
    <tr>
    	<td rowspan="4"><code>QTouchEvent</code></td>
        <td><Code>QEvent::TouchBegin</Code></td>
        <td>开始一个触屏事件序列<code>(sequence)</code></td>
    </tr>
    <tr>
        <td><Code>QEvent::TouchCancel</Code></td>
        <td>取消一个触屏事件序列</td>
    </tr>
    <tr>
        <td><Code>QEvent::TouchEnd</Code></td>
        <td>结束一个触屏事件序列</td>
    </tr>
    <tr>
        <td><Code>QEvent::TouchUpdate</Code></td>
        <td>触屏事件</td>
    </tr>
    <tr>
    	<td><code>QGestureEvent</code></td>
        <td><Code>QEvent::Gesture</Code></td>
        <td>手势事件，能识别的手势有轻触、放大、扫屏等</td>
    </tr>
    <tr>
    	<td><code>QNativeGestureEvent</code></td>
        <td><Code>QEvent::NativeGesture</Code></td>
        <td>操作系统检测到手势而产生的事件</td>
    </tr>
    <tr>
    	<td rowspan="3"><code>QActionEvent</code></td>
        <td><Code>QEvent::ActionAdded</Code></td>
        <td>运行<code>QWidget::addAction()</code>函数时会产生这种事件</td>
    </tr>
    <tr>
        <td><Code>QEvent::ActionChanged</Code></td>
        <td><code>Action</code>改变时触发的事件</td>
    </tr>
    <tr>
        <td><Code>QEvent::ActionRemoved</Code></td>
        <td>移除<code>Action</code>时触发的事件</td>
    </tr>
</table>


- `QEvent`
  - `QInputEvent`
    - `QKeyEvent`
    - `QContextMenuEvent`
    - `QPointerEvent`
      - `QSinglePointEvent`
        - `QEnterEvent`
        - `QHoverEvent`
        - `QMouseEvent`
        - `QNativeGestureEvent`
        - `QTableEvent`
        - `QWheelEvent`
      - `QTouchEvent`

##### 事件的处理

###### 事件处理的基本过程

- 任何从`QObject`派生的类都可以处理事件。但其中主要是从`QWidget`派生的窗口类和界面组件类需要处理事件。
- 一个类接收到应用程序派发来的事件后，首先会由函数`event()`处理。
  - `bool QObject::event(QEvent *e)`
  - 通过`e->type()`就可以得到事件的具体类型。
- 任何从`QObject`派生的类都可以重新实现函数`event()`，以便在接收到事件时进行处理。如果一个类重新实现了函数`event()`，需要在函数`event()`的代码里设置是否接受事件。
- 被接收的事件由事件接受者处理，被忽略的事件则传播到事件接收者的父容器组件。

###### `QWidget`类的典型事件处理函数

| 事件处理函数名            | 对应的事件类型                | 参数`event`的类型 | 事件描述                                             |
| ------------------------- | ----------------------------- | ----------------- | ---------------------------------------------------- |
| `mouseDoubleClickEvent()` | `QEvent::MouseButtonDblClick` | `QMouseEvent`     | 鼠标双击                                             |
| `mousePressEvent()`       | `QEvent::MouseButtonPress`    | `QMouseEvent`     | 鼠标按键按下，可以是左键或右键                       |
| `mouseReleaseEvent()`     | `QEvent::MouseButtonRelease`  | `QMouseEvent`     | 鼠标按键释放，可以是左键或右键                       |
| `mouseMoveEvent()`        | `QEvent::MouseMove`           | `QMouseEvent`     | 鼠标移动                                             |
| `wheelEvent()`            | `QEvent::QWheelEvent`         | `QWheelEvent`     | 鼠标滚轮滚动                                         |
| `enterEvent()`            | `QEvent::Enter`               | `QEnterEvent`     | 鼠标光标进入组件或窗口边界范围内                     |
| `leaveEvent()`            | `QEvent::Leave`               | `QEvent`          | 鼠标光标离开组件或窗口边界范围                       |
| `keyPressEvent()`         | `QEvent::KeyPress`            | `QKeyEvent`       | 键盘按键按下                                         |
| `keyReleaseEvent()`       | `QEvent::KeyRelease`          | `QKeyEvent`       | 键盘按键释放                                         |
| `focusInEvent()`          | `QEvent::FocusIn`             | `QFocusEvent`     | 组件或窗口获得键盘的输入焦点                         |
| `focusOutEvent()`         | `QEvent::FocusOut`            | `QFocusEvent`     | 组件或窗口失去键盘的输入焦点                         |
| `showEvent()`             | `QEvent::Show`                | `QShowEvent`      | 窗口在屏幕上显示出来，或组件变得可见                 |
| `hideEvent()`             | `QEvent::Hide`                | `QHideEvent`      | 窗口在屏幕上隐藏（例如窗口最小化），或组件变得不可见 |
| `moveEvent()`             | `QEvent::Move`                | `QMoveEvent`      | 组件或窗口的位置移动                                 |
| `closeEvent()`            | `QEvent::Close`               | `QCloseEvent`     | 窗口被关闭，或组件被关闭                             |
| `paintEvent()`            | `QEvent::Paint`               | `QPaintEvent`     | 界面组件需要重新绘制                                 |
| `resizeEvent()`           | `QEvent::Resize`              | `QResizeEvent`    | 窗口或组件改变大小                                   |
| `dragEnterEvent()`        | `QEvent::DragEnter`           | `QDragEnterEvent` | 在拖放操作中，鼠标光标移动到组件上方                 |
| `dragLeaveEvent()`        | `QEvent::DragLeave`           | `QDragLeaveEvent` | 在拖放操作中，鼠标光标离开了组件                     |
| `dragMoveEvent()`         | `QEvent::DragMove`            | `QDragMoveEvent`  | 拖放操作正在移动过程中                               |
| `dropEvent()`             | `QEvent::Drop`                | `QDropEvent`      | 拖放操作完成，即放下拖动的对象                       |

###### `QSinglePointEvent`

- `button()`返回值是枚举类型`Qt::MouseButton`，表示被按下的是哪个鼠标按键，有`Qt::LeftButton`、`Qt::RightButton`、`Qt::MiddleButton`等多种枚举值
- `buttons()`返回值是标志类型`Qt::MouseButtons`，也就是枚举类型`Qt::MouseButton`的枚举值组合，可用于判断多个按键被按下的情况。
- `pos()`返回值是`QPoint`类型，是鼠标光标在接收此事件的组件上的相对坐标。
- `position()`返回值是`QPointF`类型，是鼠标光标在接收此事件的组件上的相对坐标。
- `scenePosition()`返回值是`QPointF`类型，是鼠标光标在接收此事件的窗口或场景上的相对坐标。
- `globalPosition()`返回值是`QPointF`类型，是鼠标光标在屏幕或虚拟桌面上的绝对坐标。

###### `QKeyEvent`

- `key()`返回值类型是`int`，表示被按下的按键，与枚举类型`Qt::Key`的枚举值对应。
- `modifiers()`返回值是枚举类型`Qt::KeyboardModifier`的枚举值组合，表示一些用于组合使用的按键，如<kbd>Ctrl</kbd>、<kbd>Alt</kbd>、<kbd>Shift</kbd>等按键。
- 按下上、下、左、右方向键时不会产生`QEvent::KeyPress`类型的事件，只会在按键释放时产生`QEvent::KeyRelease`类型的事件。

```c++
#include "widget.h"

#include <QCloseEvent>
#include <QMessageBox>
#include <QPainter>
#include <QPointF>

#include "ui_widget.h"
Widget::Widget(QWidget *parent) : QWidget(parent), ui(new Ui::Widget) {
  ui->setupUi(this);
}

Widget::~Widget() { delete ui; }

void Widget::paintEvent(QPaintEvent *event) {
  // 在需要窗口重绘时, 应用程序会向窗口发送QEvent::Paint类型的事件
  Q_UNUSED(event);
  QPainter painter(this);
  painter.drawPixmap(0, 0, this->width(), this->height(),
                     QPixmap(":/images/background.jpg"));
  // QWidget::paintEvent(event); //
  // 运行父类的paintEvent()函数，以便于执行其内建的一些操作
  // 如果父类的事件函数里没有特殊的处理，可以不运行这行代码
}

void Widget::closeEvent(QCloseEvent *event) {
  // 当窗口被关闭时会产生QEvent::Close类型的事件
  QString title = "消息框";
  QString strInfo = "确定要退出吗？";
  QMessageBox::StandardButton result = QMessageBox::question(
      this, title, strInfo,
      QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
  if (result == QMessageBox::Yes) {
    // 接受事件，窗口被关闭
    event->accept();
  } else {
    // 忽略事件，窗口不被关闭
    event->ignore();
  }
}

void Widget::keyPressEvent(QKeyEvent *event) {
  QPoint pt = ui->pushButton->pos();
  auto key = event->key();
  if (key == Qt::Key_A || key == Qt::Key_Left) {
    ui->pushButton->move(pt.x() - 20, pt.y());
  } else if (key == Qt::Key_D || key == Qt::Key_Right) {
    ui->pushButton->move(pt.x() + 20, pt.y());
  } else if (key == Qt::Key_W || key == Qt::Key_Up) {
    ui->pushButton->move(pt.x(), pt.y() - 20);
  } else if (key == Qt::Key_S || key == Qt::Key_Down) {
    ui->pushButton->move(pt.x(), pt.y() + 20);
  }
  // 接受事件，不会再传播到父容器组件
  event->accept();
}

void Widget::showEvent(QShowEvent *event) {
  Q_UNUSED(event);
  qDebug("showEvent()函数被触发");
}

void Widget::hideEvent(QHideEvent *event) {
  Q_UNUSED(event);
  qDebug("hideEvent()函数被触发");
}

void Widget::mousePressEvent(QMouseEvent *event) {
  // 在窗口上点击鼠标按键时，会触发运行
  // 鼠标左键
  if (event->button() == Qt::LeftButton) {
    // 点击位置在窗口上的相对坐标
    QPoint pt = event->pos();
    // 相对坐标
    QPointF realPt = event->position();
    // 相对坐标
    QPointF winPt = event->scenePosition();
    // 屏幕或虚拟桌面上的绝对坐标
    QPointF globPt = event->globalPosition();
    QString str = QString::asprintf("pos() = (%d, %d)\n", pt.x(), pt.y());
    str += QString::asprintf("nposition() = (%.0f, %.0f)\n", realPt.x(),
                             realPt.y());
    str += QString::asprintf("nscenePosition() = (%.0f, %.0f)\n", winPt.x(),
                             winPt.y());
    str += QString::asprintf("nglobalPosition() = (%.0f, %.0f)\n", globPt.x(),
                             globPt.y());
    ui->label->setText(str);
    ui->label->setStyleSheet("color: red;");
    // 自动调整组件大小
    ui->label->adjustSize();
    // 移动标签到鼠标光标处
    ui->label->move(pt);
  }
}
```

#### 2、事件与信号

- 事件与信号的区别在于，事件通常是由窗口系统或应用程序产生的，信号则是`Qt`定义或用户自定义的。`Qt`为界面组件定义的信号通常是对事件的封装。

##### 函数`event()`的作用

- 应用程序派发给界面组件的事件首先会由其函数`event()`处理，如果函数`event()`不做任何处理，组件就会自动调用`QWidget`中与事件类型对应的默认事件处理函数。
- 如果需要自定义事件处理，就要自定义一个类，重新实现函数`event()`，判断事件类型，进行相应的处理。

```c++
#ifndef MYLABEL_H
#define MYLABEL_H

#include <QLabel>

class MyLabel : public QLabel {
  Q_OBJECT
 public:
  // 界面组件必须有一个父容器组件
  explicit MyLabel(QWidget *parent = nullptr);
  bool event(QEvent *e) override;

 protected:
  // 重新实现鼠标双击事件的默认处理函数
  void mouseDoubleClickEvent(QMouseEvent *event) override;
 signals:
  void doubleClicked();
};

#endif  // MYLABEL_H
```

```c++
#include "mylabel.h"

#include <QEvent>
MyLabel::MyLabel(QWidget *parent) : QLabel(parent) {
  // 必须设置这个属性，才能产生hover事件
  this->setAttribute(Qt::WA_Hover, true);
}

bool MyLabel::event(QEvent *e) {
  if (e->type() == QEvent::HoverEnter) {
    QPalette plet = this->palette();
    plet.setColor(QPalette::WindowText, Qt::red);
    this->setPalette(plet);
  } else if (e->type() == QEvent::HoverLeave) {
    QPalette plet = this->palette();
    plet.setColor(QPalette::WindowText, Qt::black);
    this->setPalette(plet);
  }
  // 运行父类的event()处理其它类型事件
  return QLabel::event(e);
}

void MyLabel::mouseDoubleClickEvent(QMouseEvent *event) {
  Q_UNUSED(event);
  emit doubleClicked();
}
```

```c++
#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui {
class Widget;
}
QT_END_NAMESPACE

class Widget : public QWidget {
  Q_OBJECT

 public:
  Widget(QWidget *parent = nullptr);
  ~Widget();

 private:
  Ui::Widget *ui;

 protected:
  void mouseDoubleClickEvent(QMouseEvent *event) override;
 private slots:
  void do_doubleClick();
};
#endif  // WIDGET_H
```

```c++
#include "widget.h"

#include "ui_widget.h"

Widget::Widget(QWidget *parent) : QWidget(parent), ui(new Ui::Widget) {
  ui->setupUi(this);
  connect(ui->label, SIGNAL(doubleClicked()), this, SLOT(do_doubleClick()));
}

Widget::~Widget() { delete ui; }

void Widget::mouseDoubleClickEvent(QMouseEvent *event) {
  Q_UNUSED(event);
  ui->label->setText("窗口被双击了");
  ui->label->adjustSize();
}

void Widget::do_doubleClick() {
  ui->label->setText("标签被双击了, 信号的槽函数响应");
  ui->label->adjustSize();
}
```

#### 3、事件过滤器

- 一个界面组件如果要对事件进行处理，需要从父类继承定义一个新类，在新类里编写程序直接处理事件，或者将事件转换为信号。
- 如果不想定义一个新的类，可以使用事件过滤器对界面组件的事件进行处理。
- 事件过滤器是`QObject`提供的一种处理事件的办法，他可以将一个对象的事件委托给另一个对象来监视并处理。

##### 事件过滤器的工作原理

- 要完成事件过滤器功能，需要完成两项操作：
  - 被监视对象使用函数`installEventFilter()`将自己注册给监视对象，监视对象就是事件过滤器。
  - 监视对象重新实现函数`eventFilter()`，对监测到的事件进行处理。
- `installEventFilter()`和`eventFilter()`都是`QObject`类定义的公有函数。
  - `void QObject::installEventFilter(QObject *filterObj)`
  - `bool QObject::eventFilter(QObject *watched, QEvent *event)`这个函数有一个返回值，如果返回`true`，事件就不会再传播给其他对象，事件处理结束；如果返回`false`，事件会继续传播给事件接收者做进一步处理
- 使用`Hover`事件时，需要将组件的`mouseTracking`属性设置为`true`，而使用`QEvent::Enter`和`QEvent::Leave`事件时无需设置这个属性。

```c++
#include "widget.h"

#include <QEvent>

#include "ui_widget.h"
Widget::Widget(QWidget *parent) : QWidget(parent), ui(new Ui::Widget) {
  ui->setupUi(this);
  ui->label->installEventFilter(this);
  ui->label_2->installEventFilter(this);
}

Widget::~Widget() { delete ui; }

bool Widget::eventFilter(QObject *watched, QEvent *event) {
  auto eventType = event->type();
  if (watched == ui->label) {
    if (eventType == QEvent::Enter) {
      ui->label->setStyleSheet("background-color: rgb(170, 255, 255);");

    } else if (eventType == QEvent::Leave) {
      ui->label->setStyleSheet("");
      ui->label->setText("靠近点，点击我");
    } else if (eventType == QEvent::MouseButtonPress) {
      ui->label->setText("button pressed");
    } else if (eventType == QEvent::MouseButtonRelease) {
      ui->label->setText("button release");
    }
  }

  if (watched == ui->label_2) {
    switch (eventType) {
      case QEvent::Enter:
        ui->label_2->setStyleSheet("background-color: rgb(85, 255, 127);");
        break;
      case QEvent::Leave:
        ui->label_2->setStyleSheet("");
        ui->label_2->setText("可双击的标签");
        break;
      case QEvent::MouseButtonDblClick:
        ui->label_2->setText("double clicked");
        break;
      default:
        break;
    }
  }
  // 运行父类的eventFilter()函数
  return QWidget::eventFilter(watched, event);
}
```

#### 4、拖放事件与拖放操作

##### 拖放操作相关事件

- 拖放由两个操作组成：拖动和放置。被拖动的组件称为拖动点，接收拖动操作的组件称为放置点。拖动点与放置点可以是不同的组件，甚至是不同的应用程序，也可以是同一个组件。
- 整个拖放操作可以分解为两个过程：
  - 拖动点启动拖动操作。被拖动组件通过`mousePressEvent()`和`mouseMoveEvent()`这两个事件处理函数的处理，检测到鼠标左键按下并移动时就可以启动拖动操作。启动拖动操作需要创建一个`QDrag`对象描述拖动操作，以及创建一个`QMimeData`类的对象用于存储拖动操作的格式信息和数据，并将其赋值为`QDrag`对象的`mimeData`属性。
  - 放置点处理放置操作。当拖动操作移动到放置点范围内时，首先触发`dragEnterEvent()`事件处理函数，在此函数里一般要通过`QDrag`对象的`mimeData`数据判断拖动操作的来源和参数，以决定是否接收此拖动操作只有被接受的拖动操作才可以被放置，并触发`dropEvent()`事件处理函数。
- `QWidget`类有一个属性`acceptDrops`，如果设置为`true`，那么对应的这个组件就可以作为一个放置点。属性`acceptDrops`的默认值是`false`。`QWidget`类中没有定义拖动操作相关的函数，所以一般的界面组件是不能作为拖动点的。

###### `QMimeData`类

- 对常见的`MIME`格式有相应的判断函数、获取数据的函数和设置函数。

| `MIME`格式            | 判断函数     | 获取数据的函数 | 设置函数         |
| --------------------- | ------------ | -------------- | ---------------- |
| `text/plain`          | `hasText()`  | `text()`       | `setText()`      |
| `text/html`           | `hasHtml()`  | `html()`       | `setHtml()`      |
| `text/uri-list`       | `hasUrls()`  | `urls()`       | `setUrls()`      |
| `image/*`             | `hasImage()` | `imageData()`  | `setImageData()` |
| `application/x-color` | `hasColor()` | `colorData()`  | `setColorData()` |

```c++
#include "widget.h"

#include <QFileInfo>
#include <QMimeData>

#include "ui_widget.h"
Widget::Widget(QWidget *parent) : QWidget(parent), ui(new Ui::Widget) {
  ui->setupUi(this);
  // 由窗口接收放置操作
  this->setAcceptDrops(true);
  // 不接受放置操作, 由窗口去处理
  ui->plainTextEdit->setAcceptDrops(false);
  ui->label->setAcceptDrops(false);
}

Widget::~Widget() { delete ui; }

void Widget::resizeEvent(QResizeEvent *event) {
  QSize size = ui->plainTextEdit->size();
  // 只改变宽度
  ui->plainTextEdit->resize(this->width() - 10, size.height());
  ui->label->resize(this->width() - 10, this->height() - size.height() - 20);
  event->accept();
}

void Widget::dragEnterEvent(QDragEnterEvent *event) {
  ui->plainTextEdit->clear();
  ui->plainTextEdit->appendPlainText(
      "dragEnterEvent事件 mimeData()->formats()");
  for (const auto &format : event->mimeData()->formats()) {
    ui->plainTextEdit->appendPlainText(format);
  }
  ui->plainTextEdit->appendPlainText(
      "\n dragEnterEvent事件 mimeData()->urls()");
  for (const auto &url : event->mimeData()->urls()) {
    ui->plainTextEdit->appendPlainText(url.path());
  }
  if (event->mimeData()->hasUrls()) {
    // 获取文件名
    QString filename = event->mimeData()->urls()[0].fileName();
    // 获取文件信息
    QFileInfo fileInfo(filename);
    // 获取文件后缀
    QString ext = fileInfo.suffix().toUpper();
    if (ext == "JPG") {
      // 接收拖动操作
      event->acceptProposedAction();
    } else {
      // 忽略事件
      event->ignore();
    }
  } else {
    event->ignore();
  }
}

void Widget::dropEvent(QDropEvent *event) {
  QString filename = event->mimeData()->urls()[0].path();
  filename = filename.last(filename.length() - 1);
  QPixmap pixmap(filename);
  ui->label->setPixmap(pixmap);
  event->accept();
}
```

#### 5、具有拖放操作功能的组件

- `Qt`类库中有一些类实现了完整的拖放操作功能，例如`QLineEdit`、`QAbstractItemView`、`QStandardItem`等类都有一个函数`setDragEnabled(bool)`，当设置参数为`true`时，组件就可以作为一个拖动点，具有默认的启动拖动的操作。
- `acceptDrops()`返回一个`bool`类型的值，表示组件是否可以作为放置点接受放置操作，`QWidget`定义
- `dragEnabled()`返回一个`bool`类型的值，表示组件是否可以作为拖动点启动拖动操作。`QAbstractItemView`定义
- `dragDropMode()`返回结果是枚举类型`QAbstractItemView::DragDropMode`，表示拖放操作模式。`QAbstractItemView`定义

| 枚举值                            | 数值 | 描述                                                   |
| --------------------------------- | ---- | ------------------------------------------------------ |
| `QAbstractItemView::NoDragDrop`   | 0    | 组件不支持拖放操作                                     |
| `QAbstractItemView::DragOnly`     | 1    | 组件只支持拖动操作                                     |
| `QAbstractItemView::DropOnly`     | 2    | 组件只支持放置操作                                     |
| `QAbstractItemViewDragDrop`       | 3    | 组件支持拖放操作                                       |
| `QAbstractItemView::InternalMove` | 4    | 组件只支持内部项的移动操作，例如目录树内节点的移动操作 |

- `defaultDropAction()`返回结果是枚举类型`Qt::DropAction`，当组件作为放置点时，它表示在完成拖放时数据操作的模式。`QAbstractItemView`定义

| 枚举值             | 数值 | 描述                                     |
| ------------------ | ---- | ---------------------------------------- |
| `Qt::CopyAction`   | 1    | 将数据复制到放置点组件处                 |
| `Qt::MoveAction`   | 2    | 将数据从拖动点组件处移动到放置点组件处   |
| `Qt::LinkAction`   | 4    | 在拖动点组件和放置点组件之间建立数据连接 |
| `Qt::IgnoreAction` | 0    | 对数据不进行任何操作                     |

- `setAcceptDrops(bool)`设置为`true`时，组件作为放置点，可接受放置操作。
- `setDragEnabled(bool)`设置为`true`时，组件作为拖动点，可以启动拖动操作。
- `setDragDropMode(mode)`用于设置拖放操作模式。
- `setDefaultDropAction(dropAction)`用于设置完成拖放操作时源组件的数据操作方式。

```c++
#include "widget.h"

#include <QKeyEvent>

#include "ui_widget.h"
void initDragDrop(QAbstractItemView *atv,
                  Qt::DropAction dropAction = Qt::CopyAction);
Widget::Widget(QWidget *parent) : QWidget(parent), ui(new Ui::Widget) {
  ui->setupUi(this);
  // 安装事件过滤器，由窗口处理4个项数据组件的事件
  ui->listSource->installEventFilter(this);
  ui->listWidget->installEventFilter(this);
  ui->treeWidget->installEventFilter(this);
  ui->tableWidget->installEventFilter(this);

  // 设置4个项数据组件的拖放操作相关属性
  initDragDrop(ui->listSource);
  initDragDrop(ui->listWidget);
  initDragDrop(ui->treeWidget);
  initDragDrop(ui->tableWidget, Qt::MoveAction);
}

void initDragDrop(QAbstractItemView *atv, Qt::DropAction dropAction) {
  atv->setAcceptDrops(true);
  atv->setDragDropMode(QAbstractItemView::DragDrop);
  atv->setDragEnabled(true);
  atv->setDefaultDropAction(dropAction);
}

Widget::~Widget() { delete ui; }

int Widget::getDropActionIndex(Qt::DropAction actionType) {
  switch (actionType) {
    case Qt::MoveAction:
      return 1;
    case Qt::LinkAction:
      return 2;
    case Qt::IgnoreAction:
      return 3;
    default:
      return 0;
  }
}

Qt::DropAction Widget::getDropActionType(int index) {
  switch (index) {
    case 1:
      return Qt::MoveAction;
    case 2:
      return Qt::LinkAction;
    case 3:
      return Qt::IgnoreAction;
    default:
      return Qt::CopyAction;
  }
}

void Widget::refreshToUI(QGroupBox *curGroupBox) {
  ui->checkBox->setChecked(itemView->acceptDrops());
  ui->checkBox_2->setChecked(itemView->dragEnabled());
  ui->comboBox->setCurrentIndex((int)itemView->dragDropMode());
  int index = getDropActionIndex(itemView->defaultDropAction());
  ui->comboBox_2->setCurrentIndex(index);
  QFont font = ui->groupBox_3->font();
  font.setBold(false);
  ui->groupBox_3->setFont(font);
  ui->groupBox_4->setFont(font);
  ui->groupBox_5->setFont(font);
  ui->groupBox_6->setFont(font);
  font.setBold(true);
  curGroupBox->setFont(font);
}

bool Widget::eventFilter(QObject *watched, QEvent *event) {
  if (event->type() != QEvent::KeyPress) {
    return QWidget::eventFilter(watched, event);
  }
  QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
  if (keyEvent->key() != Qt::Key_Delete) {
    return QWidget::eventFilter(watched, event);
  }
  if (watched == ui->listSource) {
    QListWidgetItem *item =
        ui->listSource->takeItem(ui->listSource->currentRow());
    delete item;
  } else if (watched == ui->listWidget) {
    QListWidgetItem *item =
        ui->listWidget->takeItem(ui->listWidget->currentRow());
    delete item;
  } else if (watched == ui->treeWidget) {
    QTreeWidgetItem *item = ui->treeWidget->currentItem();
    if (item->parent() != nullptr) {
      QTreeWidgetItem *parentItem = item->parent();
      parentItem->removeChild(item);
    } else {
      int index = ui->treeWidget->indexOfTopLevelItem(item);
      ui->treeWidget->takeTopLevelItem(index);
    }
    delete item;
  } else if (watched == ui->tableWidget) {
    QTableWidgetItem *item = ui->tableWidget->takeItem(
        ui->tableWidget->currentRow(), ui->tableWidget->currentColumn());
    delete item;
  }
  return true;
}

void Widget::on_radioButton_clicked() {
  itemView = ui->listSource;
  refreshToUI(ui->groupBox_3);
}

void Widget::on_radioButton_2_clicked() {
  itemView = ui->listWidget;
  refreshToUI(ui->groupBox_4);
}

void Widget::on_radioButton_3_clicked() {
  itemView = ui->treeWidget;
  refreshToUI(ui->groupBox_5);
}

void Widget::on_radioButton_4_clicked() {
  itemView = ui->tableWidget;
  refreshToUI(ui->groupBox_6);
}

void Widget::on_checkBox_clicked(bool checked) {
  itemView->setAcceptDrops(checked);
}

void Widget::on_checkBox_2_clicked(bool checked) {
  itemView->setDragEnabled(checked);
}

void Widget::on_comboBox_currentIndexChanged(int index) {
  QAbstractItemView::DragDropMode mode = (QAbstractItemView::DragDropMode)index;
  itemView->setDragDropMode(mode);
}

void Widget::on_comboBox_2_currentIndexChanged(int index) {
  Qt::DropAction actionType = getDropActionType(index);
  itemView->setDefaultDropAction(actionType);
}
```

### 七、对话框和多窗口程序设计

#### 1、标准对话框

- `Qt`为应用程序设计提供了一些常用的标准对话框，如打开文件对话框，选择颜色对话框、信息提示和确认选择对话框、标准输入对话框等。
- `Qt`预定义的各标准对话框及其主要静态函数的功能（省略输入参数）：

<table>
    <tr>
    	<th>对话框类</th>
        <th>主要静态函数</th>
        <th>函数功能</th>
    </tr>
    <tr>
    	<td rowspan="6"><code>QFileDialog</code></td>
        <td><code>QString getOpenFileName()</code></td>
        <td>选择打开一个文件，返回选择文件的文件名</td>
    </tr>
    <tr>
    	<td><code>QStringList getOpenFileNames()</code></td>
        <td>选择打开多个文件，返回选择的所有文件的文件名列表</td>
    </tr>
    <tr>
    	<td><code>QString getSaveFileName()</code></td>
        <td>选择保存一个文件、返回保存文件的文件名</td>
    </tr>
    <tr>
    	<td><code>QString getExistingDirectory()</code></td>
        <td>选择一个已有的目录，返回所选目录的完整路径</td>
    </tr>
    <tr>
    	<td><code>QUrl getOpenFileUrl()</code></td>
        <td>选择打开一个文件，可选择打开远程网络文件</td>
    </tr>
    <tr>
    	<td><code>void saveFileContent()</code></td>
        <td>将一个<code>QByteArray</code>类型的字节数据数组的内容保存为文件</td>
    </tr>
    <tr>
    	<td><code>QColorDialog</code></td>
        <td><code>QColor getColor()</code></td>
        <td>显示颜色对话框用于选择颜色、返回值是选择的颜色</td>
    </tr>
    <tr>
    	<td><code>QFontDialog</code></td>
        <td><code>QFont getFont()</code></td>
        <td>显示选择字体对话框，返回值是选择的字体</td>
    </tr>
    <tr>
    	<td><code>QProgressDialog</code></td>
        <td><code>——</code></td>
        <td>显示进度变化的对话框，没有静态函数</td>
    </tr>
    <tr>
    	<td rowspan="5"><code>QInputDialog</code></td>
        <td><code>QString getText()</code></td>
        <td>显示标准输入对话框，输入单行文字</td>
    </tr>
    <tr>
    	<td><code>int getInt()</code></td>
        <td>显示标准输入对话框，输入整数</td>
    </tr>
    <tr>
    	<td><code>double getDouble()</code></td>
        <td>显示标准输入对话框，输入浮点数</td>
    </tr>
    <tr>
    	<td><code>QString getItem()</code></td>
        <td>显示标准输入对话框，从一个下拉列表框中选择输入</td>
    </tr>
    <tr>
    	<td><code>QString getMultiLineText()</code></td>
        <td>显示标准输入对话框，输入多行字符串</td>
    </tr>
    <tr>
    	<td rowspan="6"><code>QMessageBox</code></td>
        <td><code>StandardButton information()</code></td>
        <td>显示信息提示对话框</td>
    </tr>
    <tr>
    	<td><code>StandardButton question()</code></td>
        <td>显示询问并获取是否确认的对话框</td>
    </tr>
    <tr>
    	<td><code>StandardButton warning()</code></td>
        <td>显示警告信息提示对话框</td>
    </tr>
    <tr>
    	<td><code>StandardButton critical()</code></td>
        <td>显示错误信息提示对话框</td>
    </tr>
    <tr>
    	<td><code>void about()</code></td>
        <td>显示设置自定义信息的关于对话框</td>
    </tr>
    <tr>
    	<td><code>void aboutQt()</code></td>
        <td>显示关于<code>Qt</code>的对话框</td>
    </tr>
    <tr>
    	<td><code>QPrintDialog</code></td>
        <td><code>——</code></td>
        <td>打印机设置对话框，没有静态函数</td>
    </tr>
    <tr>
    	<td><code>QPrintPreviewDialog</code></td>
        <td><code>——</code></td>
        <td>打印机对话框，没有静态函数</td>
    </tr>
</table>


##### 打开一个文件

- `static QString getOpenFileName(QWidget *parent = nullptr, const QString &caption = QString(), const QString &dir = QString(), const QString &filter = QString(), QString *selectedFilter = nullptr, Options options = Options());`
- `parent`是对话框的父窗口，一般设置为调用对话框的窗口对象
- `caption`是对话框的标题
- `dir`是打开对话框时的初始目录，程序中用静态函数`QDir::currentPath()`获取当前目录
- `filter`是文件过滤器，设置选择不同后缀的文件，可以设置多组文件。如：`文本文件(*.txt);;图片文件(*.jpg *.gif *.png);;所有文件(*.*)`。每组文件之间用两个分号隔开，同一组内不同后缀之间用空格隔开。
- `selectedFilter `是选择的文件过滤器，这个参数是返回值，表示选择文件时使用的文件过滤器。
- `options`是对话框选项，是枚举类型`QFileDialog::Option`的枚举值组合，常用如下：
  - `QFileDialog::ShowDirsOnly`只显示目录，默认是显示目录和文件。
  - `QFileDialog::DontConfirmOverwrite`覆盖一个已经存在的文件时不提示，默认是要提示。只有在使用保存文件时，此选项才有意义。
  - `QFileDialog::ReadOnly`对话框显示的文件系统是只读的。

##### 选择已有目录

- `static QString getExistingDirectory(QWidget *parent = nullptr, const QString &caption = QString(), const QString &dir = QString(), Options options = ShowDirsOnly);`

##### `QColorDialog`对话框

- `static QColor getColor(const QColor &initial = Qt::white, QWidget *parent = nullptr, const QString &title = QString(), ColorDialogOptions options = ColorDialogOptions());`
- `initial`是初始颜色
- `parent`是父窗口对象，一般是调用该对话框的窗口对象
- `title`是对话框标题
- `options`是枚举类型`QColorDialog::ColorDialogOption`的枚举值组合：
  - `QColorDialog::ShowAlphaChannel`允许用户选择颜色的`alpha`值
  - `QColorDialog::NoButtons`不显示`OK`和`Cancel`按钮，这种情况一般用于弹出式显示颜色对话框，例如点击一个按钮时，在按钮下方显示颜色对话框，通过颜色对话框的信号和接口函数选择颜色。
  - `QColorDialog::DontUseNativeDialog`使用`Qt`自带的颜色对话框，而不使用操作系统的颜色对话框
- 通过函数`QColor::isValid()`判断选择是否有效。

##### `QFontDialog`对话框

- `static QFont getFont(bool *ok, QWidget *parent = nullptr);`
- `static QFont getFont(bool *ok, const QFont &initial, QWidget *parent = nullptr, const QString &title = QString(), FontDialogOptions options = FontDialogOptions());`
- `ok`是返回变量，表示选择操作是否有效。

##### `QProgressDialog`对话框

- 用于显示进度的对话框，可以在循环操作中显示操作进度，没有静态函数，需要创建一个对话框实例然后显示，并且在操作进度显示过程中不断刷新对话框上的进度条。
- `QProgressDialog(const QString &labelText, const QString &cancelButtonText, int minimum, int maximum, QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());`
- `labelText`是信息标签上显示的文字
- `cancelButtonText`是设置取消按钮的标题
- `minimum`进度条的最小值
- `maximum`进度条的最大值
- `flags`窗口标志。
- `QProgressDialog`有一个`canceled()`信号，点击进度对话框上的“取消”按钮时会触发这个信号。
- 主要接口函数
  - `setAutoReset(bool)`若设置为`true`，当进度条达到最大值时将自动调用函数`reset()`
  - `setAutoClose(bool)`若设置为`true`，运行函数`reset()`时对话框将自动隐藏
  - `setValue(int)`为对话框上的进度条设置一个值
  - `reset()`使进度对话框复位
  - `cancel()`使对话框取消
  - `wasCanceled()`如果调用了函数`cancel()`或者点击了对话框上的“取消”按钮，则此函数返回`true`表示对话框被取消。

##### `QInputDialog`标准输入对话框

###### 输入文字

- ```c++
  static QString getText(QWidget *parent, const QString &title, const QString &label,
                             QLineEdit::EchoMode echo = QLineEdit::Normal,
                             const QString &text = QString(), bool *ok = nullptr,
                             Qt::WindowFlags flags = Qt::WindowFlags(),
                             Qt::InputMethodHints inputMethodHints = Qt::ImhNone);
  ```

- `label`是对话框上的提示文字

- `echo`是编辑框的响应模式，正常情况下设置为`QLineEdit::Normal`，如果是输入密码则设置为`QLineEdit::Password`

- `text`是编辑框内的初始文字

- `ok`是返回参数，表示对话框是否确认选择

###### 输入整数

- ```c++
  static int getInt(QWidget *parent, const QString &title, const QString &label, int value = 0,
                        int minValue = -2147483647, int maxValue = 2147483647,
                        int step = 1, bool *ok = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());
  ```

- `value`是在`SpinBox`上显示的初始值，

- `min`和`max`分别是`SpinBox`的最小值和最大值

- `step`是`SpinBox`的步长

###### 下拉列表框选择输入

- ```c++
  static QString getItem(QWidget *parent, const QString &title, const QString &label,
                             const QStringList &items, int current = 0, bool editable = true,
                             bool *ok = nullptr, Qt::WindowFlags flags = Qt::WindowFlags(),
                             Qt::InputMethodHints inputMethodHints = Qt::ImhNone);
  ```

- `items`是下拉列表的列表初始化内容

- `current`当前索引

- `editable`下拉框是否允许编辑

##### `QMessageBox`消息对话框

###### 简单消息提示

- `QMessageBox::StandardButton QMessageBox::warning(QWidget *parent, const QString &title, const QString &text, QMessageBox::StandardButton buttons = Ok, QMessageBox::StandardButton defaultButton = NoButton)`
- `buttons`是对话框提供的按钮，默认只有一个`OK`按钮
- `defaultButton`是默认的选择按钮
- 返回值表示对话框上的各种按钮。例如`Ok`、`No`、`Cancel`、`Close`等

```c++
#include "widget.h"

#include <QColorDialog>
#include <QDir>
#include <QFileDialog>
#include <QFontDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QProgressDialog>

#include "ui_widget.h"
Widget::Widget(QWidget *parent) : QWidget(parent), ui(new Ui::Widget) {
  ui->setupUi(this);
}

Widget::~Widget() { delete ui; }

void Widget::do_progress_canceled() {
  ui->plainTextEdit->appendPlainText("**进度对话被取消了**");
}

void Widget::on_pushButton_clicked() {
  // 选择单个文件
  QString curPath = QDir::currentPath();
  QString title = "选择一个文件";
  QString filter =
      "文本文件(*.txt);;图片文件(*.jpg *.gif *.png);;所有文件(*.*)";
  QString filename = QFileDialog::getOpenFileName(this, title, curPath, filter);
  if (!filename.isEmpty()) {
    ui->plainTextEdit->appendPlainText(filename);
  }
}

void Widget::on_pushButton_2_clicked() {
  // 选择多个文件
  QString curPath = QDir::currentPath();
  QString title = "选择多个文件";
  QString filter =
      "文本文件(*.txt);;图片文件(*.jpg *.gif *.png);;所有文件(*.*)";
  QStringList filenameList =
      QFileDialog::getOpenFileNames(this, title, curPath, filter);
  for (const auto &item : filenameList) {
    ui->plainTextEdit->appendPlainText(item);
  }
}

void Widget::on_pushButton_4_clicked() {
  QString curPath = QCoreApplication::applicationDirPath();
  QString title = "保存文件";
  QString filter = "文本文件(*.txt);;h文件(*.h);;C++文件(*.cpp);;所有文件(*.*)";
  QString filename = QFileDialog::getSaveFileName(this, title, curPath, filter);
  if (!filename.isEmpty()) {
    ui->plainTextEdit->appendPlainText(filename);
  }
}

void Widget::on_pushButton_3_clicked() {
  QString curPath = QCoreApplication::applicationDirPath();
  QString title = "选择一个目录";
  QString selectedDir = QFileDialog::getExistingDirectory(this, title, curPath);
  if (!selectedDir.isEmpty()) {
    ui->plainTextEdit->appendPlainText(selectedDir);
  }
}

void Widget::on_pushButton_5_clicked() {
  QPalette palette = ui->plainTextEdit->palette();
  // 当前文字颜色
  QColor initColor = palette.color(QPalette::Text);
  QColor color = QColorDialog::getColor(initColor, this, "选择文字颜色");
  if (color.isValid()) {
    palette.setColor(QPalette::Text, color);
    ui->plainTextEdit->setPalette(palette);
  }
}

void Widget::on_pushButton_6_clicked() {
  QFont initFont = ui->plainTextEdit->font();
  bool ok = false;
  QFont font = QFontDialog::getFont(&ok, initFont);
  if (ok) {
    ui->plainTextEdit->setFont(font);
  }
}

void Widget::on_pushButton_7_clicked() {
  QString labText = "正在复制文件...";
  QString btnText = "取消";
  int minV = 0, maxV = 200;
  QProgressDialog pd(labText, btnText, minV, maxV, this);
  connect(&pd, SIGNAL(canceled()), this, SLOT(do_progress_canceled()));
  pd.setWindowTitle("复制文件");
  // 以模态方式显示对话框
  pd.setWindowModality(Qt::WindowModal);
  // value()达到最大值时自动调用reset()
  pd.setAutoReset(true);
  // 调用reset()时隐藏窗口
  pd.setAutoClose(true);

  QElapsedTimer counter;
  for (int i = minV; i <= maxV; i++) {
    pd.setValue(i);
    pd.setLabelText(QString::asprintf("正在复制文件, 第%d个", i));
    counter.start();
    while (true) {
      if (counter.elapsed() > 30) {
        break;
      }
    }
    if (pd.wasCanceled()) {
      break;
    }
  }
}

void Widget::on_pushButton_14_clicked() {
  QString title = "输入文字对话框";
  QString txtLabel = "请输入文件名";
  QString initInput = "新建文件.txt";
  QLineEdit::EchoMode mode = QLineEdit::Password;
  bool ok = false;
  QString text =
      QInputDialog::getText(this, title, txtLabel, mode, initInput, &ok);
  if (ok && !text.isEmpty()) {
    ui->plainTextEdit->appendPlainText(text);
  }
}

void Widget::on_pushButton_15_clicked() {
  QString title = "输入整数对话框";
  QString txtLabel = "设置文本框字体大小";
  int defaultValue = ui->plainTextEdit->font().pointSize();
  int minValue = 6, maxValue = 50, stepValue = 1;
  bool ok = false;
  int inputValue = QInputDialog::getInt(this, title, txtLabel, defaultValue,
                                        minValue, maxValue, stepValue, &ok);
  if (ok) {
    QString str = QString("文本框字体大小被设置为:%1").arg(inputValue);
    ui->plainTextEdit->appendPlainText(str);
    QFont font = ui->plainTextEdit->font();
    font.setPointSize(inputValue);
    ui->plainTextEdit->setFont(font);
  }
}

void Widget::on_pushButton_16_clicked() {
  QString title = "输入浮点数对话框";
  QString txtLabel = "输入一个浮点数";
  double defaultValue = 3.13L;
  double minValue = 0.0L, maxValue = 10000L;
  int decimals = 5;
  bool ok = false;
  double inputValue = QInputDialog::getDouble(
      this, title, txtLabel, defaultValue, minValue, maxValue, decimals, &ok);
  if (ok) {
    QString str = QString::asprintf("输入了一个浮点数:%.2f", inputValue);
    ui->plainTextEdit->appendPlainText(str);
  }
}

void Widget::on_pushButton_17_clicked() {
  QStringList items;
  items << "优秀" << "良好" << "合格" << "不合格";
  QString title = "条目选择对话框";
  QString txtLabel = "请选择级别";
  int curIndex = 0;
  bool editable = true;
  bool ok = false;
  QString text = QInputDialog::getItem(this, title, txtLabel, items, curIndex,
                                       editable, &ok);
  if (ok && !text.isEmpty()) {
    ui->plainTextEdit->appendPlainText(text);
  }
}

void Widget::on_pushButton_8_clicked() {
  QString title = "question消息框";
  QString info = "文件内容已经被修改，是否保存修改";
  QMessageBox::StandardButton defaultBtn = QMessageBox::NoButton;
  QMessageBox::StandardButton result = QMessageBox::question(
      this, title, info,
      QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel, defaultBtn);
  if (result == QMessageBox::Yes) {
    ui->plainTextEdit->appendPlainText("question消息框: Yes被选择");
  } else if (result == QMessageBox::No) {
    ui->plainTextEdit->appendPlainText("question消息框: No被选择");
  } else if (result == QMessageBox::Cancel) {
    ui->plainTextEdit->appendPlainText("question消息框: Cancel被选择");
  } else {
    ui->plainTextEdit->appendPlainText("question消息框: 无选择");
  }
}

void Widget::on_pushButton_9_clicked() {
  QString title = "information消息框";
  QString info = "文件已经打开，请检查";
  QMessageBox::information(this, title, info, QMessageBox::Ok,
                           QMessageBox::NoButton);
}

void Widget::on_pushButton_10_clicked() {
  QString title = "warning消息框";
  QString info = "文件内容已经被修改";
  QMessageBox::warning(this, title, info);
}

void Widget::on_pushButton_11_clicked() {
  QString title = "critical消息框";
  QString info = "有不明程序访问网络";
  QMessageBox::critical(this, title, info);
}

void Widget::on_pushButton_12_clicked() {
  QString title = "about消息框";
  QString info = "SEGY文件查看软件 V1.0 \nDesigned by zq";
  QMessageBox::about(this, title, info);
}

void Widget::on_pushButton_13_clicked() {
  QMessageBox::aboutQt(this, QString("aboutQt消息框"));
}
```

#### 2、设计和使用自定义对话框

- 自定义对话框一般从`QDialog`类继承，而且可以使用`Qt Designer`可视化设计对话框界面。
- 对话框的使用一般包括创建对话框、传递数据给对话框、显示对话框获取输入、判断对话框的返回类型、获取对话框输入数据等步骤。

##### `QDialog`类

- 在默认情况下，关闭对话框时只是隐藏，而不会从内存中删除。可以通过`QWidget`的`setAttribute()`函数将对话框设置为关闭时自动删除，但一般情况下还是要手动删除。

- 新定义的属性有两个：
  - `modal`表示使用函数`QWidget::show()`显示对话框的时候，对话框是否以模态方式显示。为`true`表示以模态方式显示对话框，函数`show()`会阻塞运行，用户只能在对话框上进行操作，必须关闭对话框才能返回应用程序窗口继续操作。为`false`表示以非模态方式显示对话框，函数`show()`会立刻退出，对话框显示后用户仍然可以操作应用程序的其他窗口。
  - `sizeGripEnabled`表示在对话框的右下角是否显示一个用于调整窗口大小的标记，一般设置为`false`。对话框默认是可以调整大小的，也可以通过函数`QWidget::setWindowFlag()`设置对话框不可调整大小。
- 对话框的显示
  - `QWidget::show()`。根据`modal`属性的值，会以模态或非模态方式显示。没有返回值，无法获取对话框的操作结果。
  - `QDialog::exec()`。总以模态方式显示，并且有返回值。
    - `int QDialog::exec()`
    - `QDialog::Accepted`值为1，通常是点击对话框上的`Ok`、`Yes`等按钮时的返回值，表示接受对话框的设置。
    - `QDialog::Rejected`值为0，通常是点击对话框上的`Cancel`、`No`等按钮时的返回值，表示取消对话框的设置。
  - `QDialog::open()`以模态方式显示，然后这个函数会立即退出。

###### 对话框的返回值

- `void QDialog::accept()`使用`exec()`返回`QDialog::Accepted`，触发`accepted()`信号
- `void QDialog::reject()`使用`exec()`返回`QDialog::Rejected`，触发`rejected()`信号
- `void QDialog::done(int r)`使用`exec()`返回一个值`r`，触发`finished()`信号，`r`可以是`QDialog::Accepted`、`QDialog::Rejected`或其他自定义的值
- 一个对话框如果要返回结果，按钮的代码里一般最后要调用`accept()`，这样`exec()`的返回值就是`QDialog::Accepted`，或者`reject()`，这样返回值就是`QDialog::Rejected`

###### `QDialog`的信号

- `void QDialog::accepted()`运行`accept()`函数时触发此信号
- `void QDialog::rejected()`运行`reject()`函数时触发此信号
- `void QDialog::finished(int result)`运行`done()`函数时触发此信号

```c++
#include "mainwindow.h"

#include <QCloseEvent>
#include <QMessageBox>

#include "tdialoglocate.h"
#include "tdialogsize.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  model = new QStandardItemModel(6, 4, this);
  QStringList header;
  header << "姓名" << "性别" << "学位" << "部门";
  model->setHorizontalHeaderLabels(header);
  selection = new QItemSelectionModel(model);
  connect(selection, &QItemSelectionModel::currentChanged, this,
          &MainWindow::do_model_currentChanged);
  ui->tableView->setModel(model);
  ui->tableView->setSelectionModel(selection);
  setCentralWidget(ui->tableView);
  // 创建状态栏组件
  labCellPos = new QLabel("当前单元格：", this);
  labCellPos->setMinimumWidth(180);
  labCellPos->setAlignment(Qt::AlignHCenter);
  ui->statusbar->addWidget(labCellPos);

  labCellText = new QLabel("单元格内容", this);
  labCellText->setMinimumWidth(200);
  ui->statusbar->addWidget(labCellText);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::closeEvent(QCloseEvent *event) {
  QMessageBox::StandardButton result =
      QMessageBox::question(this, "确认", "确定要退出本程序吗？");
  if (result == QMessageBox::Yes) {
    event->accept();
  } else {
    event->ignore();
  }
}

void MainWindow::do_model_currentChanged(const QModelIndex &current,
                                         const QModelIndex &previous) {
  Q_UNUSED(previous);
  if (current.isValid()) {
    labCellPos->setText(QString::asprintf("当前单元格：%d行，%d列",
                                          current.row(), current.column()));
    QStandardItem *item;
    item = model->itemFromIndex(current);
    this->labCellText->setText("单元格内容：" + item->text());
    emit cellIndexChanged(current.row(), current.column(),
                          current.data(Qt::DisplayRole).toString());
  }
}

void MainWindow::do_setCellText(int row, int column, QString &text) {
  QModelIndex index = model->index(row, column);
  selection->clearSelection();
  selection->setCurrentIndex(index, QItemSelectionModel::Select);
  model->setData(index, text, Qt::DisplayRole);
}

void MainWindow::on_actSetRowCol_triggered() {
  TDialogSize *dialogSize = new TDialogSize(this);
  // 固定大小
  dialogSize->setWindowFlag(Qt::MSWindowsFixedSizeDialogHint);
  dialogSize->setRowColumn(model->rowCount(), model->columnCount());
  // 以模态方式显示对话框
  int res = dialogSize->exec();
  // 获取对话框上的输入，设置表格行数和列数
  if (res == QDialog::Accepted) {
    int cols = dialogSize->columnCount();
    model->setColumnCount(cols);
    int rows = dialogSize->rowCount();
    model->setRowCount(rows);
  }
  delete dialogSize;
}

void MainWindow::on_actHeader_triggered() {
  if (headers == nullptr) {
    headers = new TDialogHeaders(this);
    // 固定大小
    headers->setWindowFlag(Qt::MSWindowsFixedSizeDialogHint);
  }
  if (headers->headerList().size() != model->columnCount()) {
    QStringList strList;
    for (int i = 0; i < model->columnCount(); i++) {
      strList.append(
          model->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString());
    }
    headers->setHeaderList(strList);
  }
  int ret = headers->exec();
  if (ret == QDialog::Accepted) {
    QStringList strList = headers->headerList();
    model->setHorizontalHeaderLabels(strList);
  }
}

void MainWindow::on_actPos_triggered() {
  TDialogLocate *dialogLocate = new TDialogLocate(this);
  // 对话框关闭时自动删除
  dialogLocate->setAttribute(Qt::WA_DeleteOnClose);
  // 设置对话框置顶
  dialogLocate->setWindowFlag(Qt::WindowStaysOnTopHint);
  dialogLocate->setSpinRange(model->rowCount(), model->columnCount());
  QModelIndex curIndex = selection->currentIndex();
  if (curIndex.isValid()) {
    dialogLocate->setSpinValue(curIndex.row(), curIndex.column(),
                               curIndex.data(Qt::DisplayRole).toString());
  }
  connect(dialogLocate, SIGNAL(changeCellText(int, int, QString &)), this,
          SLOT(do_setCellText(int, int, QString &)));
  connect(dialogLocate, &TDialogLocate::changeActionEnable, ui->actPos,
          &QAction::setEnabled);
  connect(this, &MainWindow::cellIndexChanged, dialogLocate,
          &TDialogLocate::setSpinValue);
  dialogLocate->setModal(false);
  dialogLocate->show();
}

void MainWindow::on_tableView_clicked(const QModelIndex &index) {
  emit cellIndexChanged(index.row(), index.column(),
                        index.data(Qt::DisplayRole).toString());
}
```

#### 3、多窗口应用程序设计

##### 窗口类重要特性的设置

###### `setAttribute()`

- `void QWidget::setAttribute(Qt::WidgetAttribute attribute, bool on = true)`

| 枚举常量                    | 设置为`true`时的含义                        |
| --------------------------- | ------------------------------------------- |
| `Qt::WA_AcceptDrops`        | 允许窗口接收拖动来的组件                    |
| `Qt::WA_AlwaysShowToolTips` | 总是显示`toolTip`提示信息                   |
| `Qt::WA_DeleteOnClose`      | 窗口关闭时删除自己，释放内存                |
| `Qt::WA_Hover`              | 允许鼠标光标移入或移出窗口时产生`paint`事件 |
| `Qt::WA_MouseTracking`      | 开启鼠标跟踪功能                            |
| `Qt::WA_AcceptTouchEvents`  | 允许窗口接受触屏事件                        |

###### `setWindowFlag()`

- `void QWidget::setWindowFlag(Qt::WindowType flag, bool on = true)`
- 用于表示窗口类型的枚举常量

| 枚举常量           | 含义                                                         |
| ------------------ | ------------------------------------------------------------ |
| `Qt::Widget`       | 这是`QWidget`组件的默认类型，如果有父容器，它就作为一个界面组件，否则是一个独立窗口 |
| `Qt::Window`       | 表明这个组件是一个窗口，通常具有边框和标题栏，无论它是否有父容器组件 |
| `Qt::Dialog`       | 表明这个组件是一个窗口，并且要显示为对话框，例如标题栏没有最大化和最小化按钮。这是`QDialog`类的默认类型 |
| `Qt::Popup`        | 表明这个组件是用作弹出式菜单的窗口                           |
| `Qt::Tool`         | 表明这个组件是工具窗口，具有更小的标题栏和关闭按钮，通常作为工具栏的窗口 |
| `Qt::ToolTip`      | 表明这是用于显示`toolTip`提示信息的组件                      |
| `Qt::SplashScreen` | 表明这个组件是`Splash`窗口，这是`QSplashSceen`类的默认类型   |
| `Qt::SubWindow`    | 表明这个组件是子窗口，例如`QMdiSubWindow`就是这种类型        |

- 用于控制窗口显示效果和外观的枚举常量

| 枚举常量                           | 含义                                                         |
| ---------------------------------- | ------------------------------------------------------------ |
| `Qt::MSWindowsFixedSizeDialogHint` | 在`Windows`平台上，使窗口具有更窄的边框，用于固定大小的对话框 |
| `Qt::FramelessWindowHint`          | 窗口没有边框                                                 |
| `Qt::CustomizeWindowHint`          | 关闭默认的窗口标题栏，使用户可以定制窗口的标题栏             |
| `Qt::WindowTitleHint`              | 窗口有标题栏                                                 |
| `Qt::WindowSystemMenuHint`         | 窗口有系统菜单                                               |
| `Qt::WindowMinimizeButtonHint`     | 窗口有最小化按钮                                             |
| `Qt::WindowMaximizeButtonHint`     | 窗口有最大化按钮                                             |
| `Qt::WindowCloseButtonHint`        | 窗口有关闭按钮                                               |
| `Qt::WindowMinMaxButtonsHint`      | 窗口有最小化最大化按钮                                       |
| `Qt::WindowContextHelpButtonHint`  | 窗口有上下文帮助按钮                                         |
| `Qt::WindowStaysOnTopHint`         | 窗口总是处于最上层                                           |
| `Qt::WindowStaysOnBottomHint`      | 窗口总是处于最下层                                           |
| `Qt::WindowTransparentForInput`    | 窗口只作为输出，不接受输入                                   |
| `Qt::WindowDoesNotAcceptFocus`     | 窗口不接受输入焦点                                           |

###### `setWindowState()`

- `void QWidget::setWindowState(Qt::WindowStates windowState)`

| 枚举常量               | 含义                                       |
| ---------------------- | ------------------------------------------ |
| `Qt::WindowNoState`    | 窗口是正常状态                             |
| `Qt::WindowMinimized`  | 窗口最小化                                 |
| `Qt::WindowMaximized`  | 窗口最大化                                 |
| `Qt::WindowFullScreen` | 窗口填充整个屏幕，而且没有边框，没有标题栏 |
| `Qt::WindowActive`     | 窗口变为活动窗口，例如可以接收键盘输入     |

###### `setWindowModality()`

- 表示窗口的模态类型，只对窗口有用。

| 枚举常量               | 含义                                             |
| ---------------------- | ------------------------------------------------ |
| `Qt::NonModal`         | 无模态，不会阻止其它窗口的输入                   |
| `Qt::WindowModal`      | 窗口对于其父窗口，所有的上级父窗口都是模态的     |
| `Qt::ApplicationModal` | 窗口对于整个应用程序是模态的，阻止所有窗口的输入 |

###### `setWindowOpacity()`

- 表示窗口的透明度。范围是`0.0~1.0`

```c++
#include "mainwindow.h"

#include <QPaintEvent>
#include <QPainter>

#include "tformdoc.h"
#include "tformtable.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  ui->tabWidget->setVisible(false);
  // 清除所有页面
  ui->tabWidget->clear();
  // 各页面有关闭按钮，可被关闭
  ui->tabWidget->setTabsClosable(true);
  this->setCentralWidget(ui->tabWidget);
  // 窗口最大化显示
  this->setWindowState(Qt::WindowMaximized);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::do_changeTabTitle(QString title) {
  int index = ui->tabWidget->currentIndex();
  ui->tabWidget->setTabText(index, title);
}

void MainWindow::paintEvent(QPaintEvent *event) {
  QPainter painter(this);
  painter.drawPixmap(
      0, ui->toolBar->height(), this->width(),
      this->height() - ui->toolBar->height() - ui->statusbar->height(),
      QPixmap(":/images/back2.jpg"));
  event->accept();
}

void MainWindow::on_actEmbed_triggered() {
  TFormDoc *formDoc = new TFormDoc(this);
  formDoc->setAttribute(Qt::WA_DeleteOnClose);
  int cur = ui->tabWidget->addTab(
      formDoc, QString::asprintf("Doc %d", ui->tabWidget->count()));
  ui->tabWidget->setCurrentIndex(cur);
  ui->tabWidget->setVisible(true);
  connect(formDoc, &TFormDoc::titleChanged, this,
          &MainWindow::do_changeTabTitle);
}

void MainWindow::on_actWidget_triggered() {
  TFormDoc *formDoc = new TFormDoc();
  formDoc->setAttribute(Qt::WA_DeleteOnClose);
  formDoc->setWindowTitle("基于QWidget的窗口，无父窗口，关闭时自动删除");
  formDoc->setWindowFlag(Qt::Window, true);
  formDoc->setWindowOpacity(0.9);
  formDoc->show();
}

void MainWindow::on_actEmbedMainWindow_triggered() {
  TFormTable *formTable = new TFormTable(this);
  formTable->setAttribute(Qt::WA_DeleteOnClose);
  int cur = ui->tabWidget->addTab(
      formTable, QString::asprintf("Table %d", ui->tabWidget->count()));
  ui->tabWidget->setCurrentIndex(cur);
  ui->tabWidget->setVisible(true);
}

void MainWindow::on_tabWidget_tabCloseRequested(int index) {
  // 点击标签上的关闭按钮后可关闭页面
  if (index < 0) {
    return;
  }
  QWidget *form = ui->tabWidget->widget(index);
  form->close();
}

void MainWindow::on_tabWidget_currentChanged(int index) {
  // 没有标签时不显示tabWidget
  Q_UNUSED(index);
  bool en = ui->tabWidget->count() > 0;
  ui->tabWidget->setVisible(en);
}

void MainWindow::on_actMainWindow_triggered() {
  TFormTable *formTable = new TFormTable(this);
  formTable->setAttribute(Qt::WA_DeleteOnClose);
  formTable->setWindowTitle("基于QMainWindow的窗口");
  // 如果没有状态栏，就创建状态栏
  formTable->statusBar();
  formTable->show();
}
```

#### 4、`MDI`应用程序设计

- 多文档界面（`multiple document interface, MDI）`是一种应用程序结构，适合用来设计专门处理某种文件的应用程序。
- `MDI`应用程序的设计主要是对`QMdiArea`和`QMdiSubWindow`类的使用

##### `QMdiArea`类

- `MDI`应用程序的主窗口是基于`QMainWindow`的窗口类。在设计`MDI`应用程序时，需要在主窗口的工作区放置一个`QMdiArea`组件，并使用`QMainWindow`的`setCentralWidget()`函数将其设置为主窗口的中心组件，也就是填充满主窗口的工作区。
- `QMdiArea`是管理`MDI`子窗口的类。
- 用户设计的子窗口一般是基于`QWidget`的窗口类。

###### `QMdiArea`的属性

- 显示子窗口有两种模式：子窗口模式和多页模式。
- `void QMdiArea::setViewMode(QMdiArea::ViewMode mode)`
  - `QMdiArea::SubWindowView`表示子窗口模式。
  - `QMdiArea::TabbedView`表示多页模式。

###### 主要接口函数

- `QMdiSubWindow *QMdiArea::addSubWindow(QWidget *widget, Qt::WindowFlags windowFlags = Qt::WindowFlags())`用于添加用户创建的一个子窗口。
- `QMdiSubWindow *QMdiArea::activeSubWindow()`返回当前活动子窗口的指针。
- `void QMdiArea::removeSubWindow(QWidget *widget)`用于移除一个子窗口。
  - 参数可以是`QMdiSubWindow`对象或其内部的用户子窗口。如果参数是`QMdiSubWindow`对象，`MDI`子管理管理器就会移除这个子窗口。如果参数是对象内部的用户子窗口，`MDI`子窗口管理器就会清除这个子窗口的界面。
  - 该方法只移除窗口，并不删除窗口对象。
- `QList<QMdiSubWindow*> QMdiArea::subWindowList(QMdiArea::WindowOrder order = CreationOrder)`返回子窗口列表。

###### 公有槽

- `void activateNextSubWindow()`激活后一个子窗口
- `void activePreviousSubWindow()`激活前一个子窗口
- `void setActiveSubWindow(QMdiSubWindow *window)`激活一个子窗口
- `void cascadeSubWindows()`所有子窗口平铺展开，在子窗口模式下有效
- `void closeActiveSubWindow()`关闭当前活动窗口
- `void closeAllSubWindows()`关闭所有子窗口

###### 信号

- `void subWindowActivated(QMdiSubWindow *window)`在某个子窗口变成活动窗口时

##### `QMdiSubWindow`类

- `QWidget *QMdiSubWindow::widget()`返回用户窗口对象指针，如果要操作用户窗口，需要将用户窗口对象指针转换为用户窗口类型
- `void QMdiSubWindow::setWidget(QWidget *widget)`设置一个用户窗口作为`QMdiSubWindow`窗口的内部组件。

```c++
#include "mainwindow.h"

#include <QDir>
#include <QFileDialog>
#include <QMdiSubWindow>

#include "tformdoc.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  ui->mdiArea->setTabsClosable(true);
  // 页面可移动
  ui->mdiArea->setTabsMovable(true);
  setCentralWidget(ui->mdiArea);
  // 窗口最大化显示
  setWindowState(Qt::WindowMaximized);
  ui->toolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
  ui->actCut->setEnabled(false);
  ui->actCopy->setEnabled(false);
  ui->actPaste->setEnabled(false);
  ui->actFont->setEnabled(false);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_actNew_triggered() {
  TFormDoc *formDoc = new TFormDoc(this);
  ui->mdiArea->addSubWindow(formDoc);
  formDoc->show();
  ui->actCut->setEnabled(true);
  ui->actCopy->setEnabled(true);
  ui->actPaste->setEnabled(true);
  ui->actFont->setEnabled(true);
}

void MainWindow::on_actOpen_triggered() {
  bool needNew = false;
  TFormDoc *formDoc;
  if (ui->mdiArea->subWindowList().size() > 0) {
    formDoc = (TFormDoc *)ui->mdiArea->activeSubWindow()->widget();
    needNew = formDoc->isFileOpened();
  } else {
    needNew = true;
  }
  QString curPath = QDir::currentPath();
  QString filename =
      QFileDialog::getOpenFileName(this, tr("打开一个文件"), curPath,
                                   "C程序文件(*.h *.c *.cpp);;文本文件(*.txt)");
  if (filename.isEmpty()) {
    return;
  }
  if (needNew) {
    formDoc = new TFormDoc(this);
    ui->mdiArea->addSubWindow(formDoc);
  }
  formDoc->loadFromFile(filename);
  formDoc->show();
  ui->actCut->setEnabled(true);
  ui->actCopy->setEnabled(true);
  ui->actPaste->setEnabled(true);
  ui->actFont->setEnabled(true);
}

void MainWindow::on_actViewMode_triggered(bool checked) {
  ui->mdiArea->setViewMode(checked ? QMdiArea::TabbedView
                                   : QMdiArea::SubWindowView);
  ui->mdiArea->setTabsClosable(checked);
  ui->actCascade->setEnabled(!checked);
  ui->actTile->setEnabled(!checked);
}

void MainWindow::on_actSave_triggered() {
  TFormDoc *formDoc =
      static_cast<TFormDoc *>(ui->mdiArea->activeSubWindow()->widget());
  formDoc->saveToFile();
}

void MainWindow::on_actCut_triggered() {
  TFormDoc *formDoc =
      static_cast<TFormDoc *>(ui->mdiArea->activeSubWindow()->widget());
  formDoc->textCut();
}

void MainWindow::on_actCopy_triggered() {
  TFormDoc *formDoc =
      static_cast<TFormDoc *>(ui->mdiArea->activeSubWindow()->widget());
  formDoc->textCopy();
}

void MainWindow::on_actFont_triggered() {
  TFormDoc *formDoc =
      static_cast<TFormDoc *>(ui->mdiArea->activeSubWindow()->widget());
  formDoc->setEditFont();
}

void MainWindow::on_mdiArea_subWindowActivated(QMdiSubWindow *arg1) {
  if (ui->mdiArea->subWindowList().isEmpty()) {
    ui->actCut->setEnabled(false);
    ui->actCopy->setEnabled(false);
    ui->actPaste->setEnabled(false);
    ui->actFont->setEnabled(false);
    ui->statusbar->clearMessage();
  } else {
    TFormDoc *formDoc = static_cast<TFormDoc *>(arg1->widget());
    ui->statusbar->showMessage(formDoc->currentFileName());
  }
}

void MainWindow::on_actPaste_triggered() {
  TFormDoc *formDoc =
      static_cast<TFormDoc *>(ui->mdiArea->activeSubWindow()->widget());
  formDoc->textPaste();
}
```

#### 5、`Splash`与登录窗口

```c++
#include "tdialoglogin.h"

#include <QCryptographicHash>
#include <QMessageBox>
#include <QMouseEvent>
#include <QSettings>

#include "ui_tdialoglogin.h"
TDialogLogin::TDialogLogin(QWidget *parent)
    : QDialog(parent), ui(new Ui::TDialogLogin) {
  ui->setupUi(this);
  this->setAttribute(Qt::WA_DeleteOnClose);
  this->setWindowFlags(Qt::SplashScreen);
  // 设置组织名
  QApplication::setOrganizationName("HUAYUNLIUFENG-Qt");
  // 设置应用程序名
  QApplication::setApplicationName("samp7_5");
  readSettings();
}

TDialogLogin::~TDialogLogin() { delete ui; }

void TDialogLogin::readSettings() {
  QSettings settings;
  bool saved = settings.value("saved", false).toBool();
  user = settings.value("Username", "user").toString();
  QString defaultPsd = encrypt("12345");
  password = settings.value("PSD", defaultPsd).toString();
  if (saved) {
    ui->lineEdit->setText(user);
  }
  ui->checkBox->setChecked(saved);
}

void TDialogLogin::writeSettings() {
  QSettings settings;
  settings.setValue("Username", user);
  settings.setValue("PSD", password);
  settings.setValue("saved", ui->checkBox->isChecked());
}

QString TDialogLogin::encrypt(const QString &str) {
  // 字符串转换为字节数组数据
  QByteArray btArray = str.toLocal8Bit();
  // 使用md5加密
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(btArray);
  QByteArray resultArray = hash.result();
  return resultArray.toHex();
}

void TDialogLogin::mousePressEvent(QMouseEvent *event) {
  // 鼠标按键被按下
  if (event->button() == Qt::LeftButton) {
    isMoving = true;
    lastPos = event->globalPosition().toPoint() - this->pos();
  }
  QDialog::mousePressEvent(event);
}

void TDialogLogin::mouseReleaseEvent(QMouseEvent *event) {
  // 鼠标按键释放
  isMoving = false;
  event->accept();
}

void TDialogLogin::mouseMoveEvent(QMouseEvent *event) {
  // 按下鼠标左键进行移动
  QPoint eventPos = event->globalPosition().toPoint();
  if (isMoving && (event->buttons() & Qt::LeftButton) &&
      (eventPos - lastPos).manhattanLength() >
          QApplication::startDragDistance()) {
    move(eventPos - lastPos);
    lastPos = eventPos - this->pos();
  }
  QDialog::mouseMoveEvent(event);
}

void TDialogLogin::on_pushButton_clicked() {
  QString inputUser = ui->lineEdit->text().trimmed();
  QString psd = ui->lineEdit_2->text().trimmed();
  QString encryptPsd = encrypt(psd);
  if (inputUser == user && encryptPsd == password) {
    writeSettings();
    this->accept();
  } else {
    maxTryCount++;
    if (maxTryCount > 3) {
      QMessageBox::critical(this, "错误", "输入次数太多，强行退出");
      this->reject();
    } else {
      QMessageBox::warning(this, "错误提示", "用户名或密码错误");
    }
  }
}
```

```c++
#include <QApplication>

#include "mainwindow.h"
#include "tdialoglogin.h"

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  TDialogLogin *dialogLogin = new TDialogLogin();
  if (dialogLogin->exec() == QDialog::Accepted) {
    MainWindow w;
    w.show();
    return a.exec();
  }
  return 0;
}
```

##### `Splash`登录对话框的界面设计和类定义

- 使用`setWindowFlags()`设置窗口标志`Qt::SplashScreen`，这样的对话框就没有标题栏和边框，且不会在`Windows`任务栏上显示。
- 使用`QApplication::setOrganizationName("HUAYUNLIUFENG-Qt");`设置组织名
- 使用`QApplication::setApplicationName("samp7_5");`设置应用程序名
- 这两个参数决定了创建`QSettings`对象时默认的注册表目录。

##### 读写注册表

```c++
void TDialogLogin::readSettings() {
  QSettings settings;
  bool saved = settings.value("saved", false).toBool();
  user = settings.value("Username", "user").toString();
  QString defaultPsd = encrypt("12345");
  password = settings.value("PSD", defaultPsd).toString();
  if (saved) {
    ui->lineEdit->setText(user);
  }
  ui->checkBox->setChecked(saved);
}

void TDialogLogin::writeSettings() {
  QSettings settings;
  settings.setValue("Username", user);
  settings.setValue("PSD", password);
  settings.setValue("saved", ui->checkBox->isChecked());
}
```

- 其中一种原型：
  - `explicit QSettings(const QString &organization, const QString &application = QString(), QObject *parent = nullptr);`
  - 指向的目录是`HKEY_CURRENT_USER/Software/组织名/应用程序名`
- 注册表里的参数是以键值对的形式保存的。

### 八、文件系统操作和文件读写

#### 1、文件操作相关类概述

##### 输入输出设备类

- `Qt`中进行文件读写的基本类是`QFile`。他的父类是`QFileDevice`，提供了文件交互的底层功能。`QFileDevice`的父类是`QIODevice`，它有两个父类：`QObject`和`QIODeviceBase`
- `QIODevice`是所有输入输出设备的基础类。
- `QFile`是用于文件操作和文件数据读写的类，使用`QFile`可以读写任意格式的文件。
- `QSaveFile`是用于安全保存文件的类。使用`QSaveFile`保存文件时，它会把数据写入一个临时文件，成功提交后才将数据写入最终的文件。如果保存过程中出现错误，临时文件里的数据不会被写入最终文件，这样就能确保最终文件中不会丢失数据或被写入部分数据。在保存比较大的文件或者复杂格式的文件时可以使用这个类。
- `QTemporaryFile`是用于创建临时文件的类。使用函数`QTemporaryFile::open()`就能创建一个文件名唯一的临时文件，在`QTemporaryFile`对象被删除时，临时文件被自动删除
- `QTcpSocket`和`QUdpSocket`是分别实现了`TCP`和`UDP`的类
- `QSerialPort`是实现了串口通信的类，通过这个类可以实现计算机与串口设备的通信
- `QBluetoothSocket`是用于蓝牙通信的类。
- `QProcess`类用于启动外部程序，并且可以给程序传递参数
- `QBuffer`以一个`QByteArray`对象作为缓冲区，将`QByteArray`对象当做一个`I/O`设备来读写。

##### 特定格式文件的读写

###### 读写`XML`文件

- 基于文档对象模型（`DOM`）
  - 整个`XML`文档用一个`QDomDocument`对象表示，文档树状结构中的节点都用`QDomNode`及其子类表示。
- 基于流
  - 使用`QXmlStreamReader`和`QXmlStreamWriter`类进行`XML`文件的读写，这两个类易于使用，与`XML`标准兼容效果好。

###### 读写`JSON`文件

- `QJsonDocument`是用于读写`JSON`文件的类，`QJsonArray`是封装了`JSON`数组的类，`QJsonObject`是封装了`JSON`对象的类，`QJsonValue`是封装了`JSON`值的类。
- `JSON`的数据有六种基本类型：`bool`、`double`、`string`、`array`、`object`和`null`

###### 读写图片文件

- 使用`QImage`和`QPixmax`可以直接读取图片文件。这两个类都是从`QPaintDevice`继承来的。他们在读取图片文件时总是按图片原始大小读取整张图片。
- `Qt`还提供了一个类`QImageReader`用于在读取图片文件时进行更多的控制，例如通过`setScaledSize()`以指定大小读取图片，可以实现缩略图显示。
- 使用`QImage`和`QPixmap`的函数`save()`可以直接将图片保存为文件。`Qt`还提供了一个`QImageWriter`类，可以实现在保存图片时提供更多的选项，例如设置压缩级别和图片品质。

#### 2、文件和目录操作

- `QCoreApplication`可以提取应用程序路径，程序名等信息
- `QFile`可以对文件进行复制、删除、重命名等操作。
- `QFileInfo`用于获取文件的各种信息，如文件的路径、基本文件名、文件名后缀、文件大小等。
- `QDir`用于目录信息获取和目录操作，包括新建目录、删除目录、获取目录下的文件或子目录等。
- `QTemporaryDir`用于创建临时目录，临时目录在使用后自动删除
- `QTemporaryFile`用于创建临时文件，临时文件在使用后自动删除
- `QFileSystemWatcher`用于监视设定的目录和文件，当所监视的目录或文件出现重复、重命名、删除等操作时会发射相应的信号。

##### `QCoreApplication`类

- 该类是为无`UI`的应用程序提供事件循环的类，是所有应用程序类的基类，其子类`QGuiApplication`是具有`GUI`的应用程序类，它具有主事件循环，能处理和派发来自操作系统的事件或其他来源的事件。
- `QGuiApplication`的子类`QApplication`为基于`QWidget`的应用程序提供支持，包括界面的初始化等。

| 函数原型                                                     | 功能                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `static QString applicationDirPath();`                       | 返回应用程序可执行文件所在的路径                             |
| `static QString applicationFilePath();`                      | 返回应用程序的带有路径的完整文件名                           |
| `static QString applicationName();`                          | 返回应用程序名称，默认是无后缀的可执行文件名                 |
| `static void setApplicationName(const QString &application);` | 设置应用程序名称，替代默认的应用程序名称                     |
| `static QStringList libraryPaths();`                         | 返回一个字符串列表，其是应用程序动态加载库文件时搜索的目录列表 |
| `static void addLibraryPath(const QString &);`               | 将一个路径添加到应用程序的库搜索目录列表中                   |
| `static void setOrganizationName(const QString &orgName);`   | 为应用程序设置一个组织名                                     |
| `static QString organizationName();`                         | 返回应用程序的组织名                                         |
| `static void exit(int retcode = 0);`                         | 退出应用程序                                                 |

##### `QFile`类

- 常用静态函数：

| 函数原型                                                     | 功能                                        |
| ------------------------------------------------------------ | ------------------------------------------- |
| `static bool copy(const T &fileName, const T &newName)`      | 复制一个文件                                |
| `static bool rename(const QString &oldName, const QString &newName);` | 重命名一个文件                              |
| `static bool remove(const QString &fileName);`               | 删除一个文件                                |
| `static bool moveToTrash(const QString &fileName, QString *pathInTrash = nullptr);` | 将一个文件移动到回收站                      |
| `static bool exists(const QString &fileName);`               | 判断一个文件是否存在                        |
| `static bool link(const QString &fileName, const QString &newName);` | 创建文件链接，在`Windows`上就是创建快捷方式 |
| `static bool setPermissions(const QString &filename, Permissions permissionSpec);` | 设置一个文件的权限                          |
| `static Permissions permissions(const QString &filename);`   | 返回文件的权限                              |
| `static QString symLinkTarget(const QString &fileName);`     | 返回一个链接指向的绝对文件名或路径          |

- 常用接口函数：

| 函数原型                                                     | 功能                                                  |
| ------------------------------------------------------------ | ----------------------------------------------------- |
| `void setFileName(const QString &name);`                     | 设置文件名，文件已打开后不能再调用此函数              |
| `QString fileName() const override;`                         | 返回当前所操作的文件名                                |
| `bool copy(const QString &newName);`                         | 当前文件复制为`newName`表示的文件                     |
| `bool rename(const QString &newName);`                       | 将当前文件重命名为`newName`                           |
| `bool remove();`                                             | 删除当前文件                                          |
| `bool moveToTrash();`                                        | 将当前文件移除到回收站                                |
| `bool exists() const;`                                       | 判断当前我还能是否存在                                |
| `bool link(const QString &newName);`                         | 为当前文件创建一个链接，在`Windows`上就是创建快捷方式 |
| `QString symLinkTarget() const;`                             | 返回链接指向的绝对文件名或路径                        |
| `bool setPermissions(QFileDevice::Permissions permissionSpec) override;` | 为当前文件设置权限                                    |
| `QFileDevice::Permissions permissions() const override;`     | 返回当前文件的权限                                    |
| `qint64 size()`                                              | 返回当前文件的大小，单位是字节                        |

##### `QFileInfo`类

- 用于获取文件的各种信息。
- `QFileInfo(const QFileInfo &fileinfo)`指定文件名
- `QFileInfo()`不指定文件名
- 常用接口函数

| 函数原型                                   | 功能                                                         |
| ------------------------------------------ | ------------------------------------------------------------ |
| `void setFile(QString &file)`              | 设置一个文件名，使该文件作为`QFileInfo`对象操作的当前文件    |
| `QString absoluteFilePath()`               | 返回包含文件名的绝对路径                                     |
| `QString absolutePath()`                   | 返回绝对路径，不包含文件名                                   |
| `QDir absoluteDir()`                       | 返回绝对路径，返回值是`QDir`类型                             |
| `QString fileName()`                       | 返回去除路径的文件名                                         |
| `QString filePath()`                       | 返回包含路径的文件名                                         |
| `QString path()`                           | 返回不含文件名的路径                                         |
| `qint64 size()`                            | 返回文件大小，单位是字节                                     |
| `QString baseName()`                       | 返回文件基名，第一个"."之前的文件名                          |
| `QString completeBaseName()`               | 返回文件基名，最后一个"."之前的文件名                        |
| `QString suffix()`                         | 返回最后一个"."之后的后缀                                    |
| `QString completeSuffix()`                 | 返回第一个"."之后的后缀                                      |
| `bool isDir()`                             | 判断当前对象是不是一个目录或目录的快捷方式                   |
| `bool isFile()`                            | 判断当前对象是不是一个文件或文件的快捷方式                   |
| `bool isExecutable()`                      | 判断当前文件是不是可执行文件                                 |
| `QDateTime fileTime(QFile::FileTime time)` | 返回文件的时间，参数`time`用于指定需要返回的时间数据类型，如文件创建时间，最后一次读写时间等 |
| `QDateTime birthTime()`                    | 返回文件创建的时间                                           |
| `QDateTime lastModified()`                 | 返回文件最后被修改的时间                                     |
| `QDateTime lastRead()`                     | 返回文件最后被读取的时间                                     |
| `QDateTime metadataChangeTime()`           | 返回文件的元数据最后被修改的时间                             |
| `void refresh()`                           | 刷新文件信息                                                 |
| `bool exists()`                            | 判断文件是否存在                                             |
| `bool exists(QString &file)`               | 静态函数，判断`file`表示的文件是否存在                       |

- 枚举类型`QFile::FileTime`的各枚举常量含义：
  - `QFileDevice::FileAccessTime`最后一次读或写文件的时间
  - `QFileDevice::FileBirthTime`文件创建时间
  - `QFileDevice::FileMetadataChangeTime`文件的元数据被修改的时间，如文件的权限被修改
  - `QFileDevice::FileModificationTime`文件最后被修改的时间

##### `QDir`类

- `QDir(const QString &path = QString())`
- `QDir`的目录字符串以`/`作为目录分隔符，可以表示相对路径或绝对路径。
- `QDir`还可以操作`Qt`的资源文件，`":/"`是资源文件的目录起始符，资源文件目录是绝对路径
- `QDir`表示的相对路径针对的是应用程序的当前路径。
- 常用静态函数：

| 函数原型                             | 功能                                                         |
| ------------------------------------ | ------------------------------------------------------------ |
| `QString tempPath()`                 | 返回系统的临时目录，在`Windows`系统上就是系统环境变量`TEMP`所指向的目录 |
| `QDir temp()`                        | 返回系统的临时目录，返回值是`QDir`类型                       |
| `QString rootPath()`                 | 返回系统根目录。在`Unix`系统上就是“/”，在`Windows`系统上通常是`C:/` |
| `QDir root()`                        | 返回系统根目录，返回值是`QDir`类型                           |
| `QString homePath()`                 | 返回用户主目录，在`Windows`系统上就是用户主目录              |
| `QDir home()`                        | 返回用户主目录，返回值是`QDir`类型                           |
| `QString currentPath()`              | 返回应用程序的当前目录                                       |
| `QDir current()`                     | 返回应用程序的当前目录，返回值是`QDir`类型                   |
| `bool setCurrent(QString &path)`     | 设置`path`表示的目录作为应用程序的当前目录                   |
| `QFileInfoList drives()`             | 返回系统的根目录列表，在`Windows`系统上返回的就是盘符列表    |
| `bool isAbsolutePath(QString &path)` | 判断`path`表示的目录是不是绝对路径                           |
| `bool isRelativePath(QString &path)` | 判断`path`表示的目录是不是相对路径                           |

- 公共接口函数

| 函数原型                                                     | 功能                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `void setPath(QString &path)`                                | 设置`QDir`对象的当前目录                                     |
| `QString path()`                                             | 返回`QDir`对象的当前目录                                     |
| `QString absoluteFilePath(QString &fileName)`                | 返回当前目录下的文件`fileName`的含有绝对路径的文件名         |
| `QString absolutePath()`                                     | 返回当前目录的绝对路径。如果`QDir`对象的当前目录是绝对路径，返回的是相对于应用程序当前路径的绝对路径。应用程序的当前路径是静态函数`QDir::currentPath()`返回的路径 |
| `QString canonicalPath()`                                    | 返回当前目录的标准路径                                       |
| `QString filePath(QString &fileName)`                        | 如果`fileName`是不带有路径的文件名，函数返回值是带有操作目录的完整文件名 |
| `QString dirName()`                                          | 返回当前目录的最后一级目录名称                               |
| `bool cd(QString &dirName)`                                  | 切换当前目录到`dirName`表示的目录                            |
| `bool cdUp()`                                                | 切换当前目录到上一级目录                                     |
| `bool exists()`                                              | 判断当前目录是否存在                                         |
| `bool exists(QString &name)`                                 | 如果`name`是不带路径的文件名，判断当前目录下是否存在这个文件 |
| `void refresh()`                                             | 刷新目录信息                                                 |
| `bool mkdir(QString &dirName)`                               | 在当前目录下创建`dirName`表示的子目录                        |
| `bool mkpath(QString &dirPath)`                              | 创建绝对目录`dirPath`，如有必要会创建所有上级目录            |
| `bool rmdir(QString &dirName)`                               | 删除指定的目录`dirName`                                      |
| `bool rmpath(QString &dirPath)`                              | 删除`dirPath`表示的目录，包括上级目录，需要上级目录是空的    |
| `bool remove(QString &fileName)`                             | 删除当前目录下的文件`fileName`                               |
| `bool removeRecursively()`                                   | 删除当前目录及其下所有目录和文件                             |
| `bool rename(QString &oldName, QString &newName)`            | 将文件或目录`oldName`更名为`newName`                         |
| `bool isEmpty(QDir::Filters filters = Filters(AllEntries|NoDotAndDotDOt))` | 判断当前目录是否为空                                         |
| `void setFilter(QDir::Filters filters)`                      | 设置使用函数`entryList()`和`entryInfoList()`时的过滤器       |
| `void setSorting(QDir::SortFlags sort)`                      | 设置使用函数`entryList()`和`entryInfoList()`时的排序方式     |
| `QStringList entryList(QDir::Filters filters = NoFilter, QDir::SortFlags sort = NoSort)` | 返回当前目录下的所有文件名、子目录等                         |
| `QFileInfoList entryInfoList(QDir::Filters filters = NoFilter, QDir::SortFlags sort = NoSort)` | 返回当前目录下的所有文件、子目录等，返回值是`QFileInfo`对象列表 |

- `QDir::Filters`常用枚举值：
  - `QDir::AllDirs`列出所有目录
  - `QDir::Files`列出所有文件
  - `QDir::Drives`列出所有驱动器（`Unix`系统下无效）
  - `QDir::NoDotAndDotDot`不列出特殊的项
  - `QDir::AllEntries`列出目录下的所有项

##### `QTemporaryDir`类

- `QTemporaryDir()`在系统的临时目录下创建临时目录
- `QTemporaryDir(const QString &templatePath)`在指定目录下创建临时目录，其中参数`templatePath`是临时目录模板，可以在最后使用六个`X`表示6个随机字母，例如：`"SubDir_XXXXXX"`
- 常用接口函数：

| 函数原型                              | 功能                                                         |
| ------------------------------------- | ------------------------------------------------------------ |
| `void setAutoRemove(bool b)`          | 若设置为`true`，`QTemporaryDir`对象被删除时，其创建的临时目录也被自动删除 |
| `QString path()`                      | 返回创建的临时目录名称，如果临时目录未被成功创建，返回值是空字符串 |
| `bool isValid()`                      | 如果临时目录被成功创建，该函数返回值为`true`                 |
| `QString filePath(QString &fileName)` | 返回临时目录下的一个文件的带有路径的文件名，函数不会检查文件是否存在 |
| `bool remove()`                       | 删除此临时目录及其所有内容                                   |

##### `QTemporaryFile`类

- 常用构造函数：

  - `QTemporaryFile(const QString &templateName, QObject *parent)`

  - `QTemporaryFile(QObject *parent)`

  - `QTemporaryFile(const QString &templateName)`指定临时文件名模板

  - `QTemporaryFile()`在系统临时目录下创建临时文件

- `parent`是父容器对象指针，一般指定为所在的窗口对象
- 创建`QTemporaryFile`对象时只是设置了临时文件的文件名，使用函数`QTemporaryFile::open()`打开文件时才会实际创建文件。文件使用后需要使用函数`QFile::close()`关闭它。
- 常用接口函数：

| 函数原型                              | 功能                                                         |
| ------------------------------------- | ------------------------------------------------------------ |
| `void setAutoRemove(bool b)`          | 若设置为`true`，`QTemporaryFile`对象被删除时，其创建的临时文件也会被自动删除 |
| `void setFileTemplate(QString &name)` | 设置临时文件的文件名模板                                     |
| `QString fileTemplate()`              | 返回临时文件的文件名模板                                     |
| `bool open()`                         | 打开临时文件，且总是以`QIODevice::ReadWrite`模式打开         |

##### `QFileSystemWatcher`类

- 把某些目录或文件添加到`QFileSystemWatcher`对象的监视列表后，当目录下发生新建、删除文件等操作时，`QFileSystemWatcher`会发射`directoryChanged()`信号，当所监视的文件发生修改、重命名等操作时，`QFileSystemWatcher`会发射`fileChanged()`信号
- 主要接口函数：

| 函数原型                                      | 功能                         |
| --------------------------------------------- | ---------------------------- |
| `bool addPath(QString &path)`                 | 添加一个监视的目录或文件     |
| `QStringList addPaths(QStringList &paths)`    | 添加需要监视的目录或文件列表 |
| `QStringList directories()`                   | 返回监视的目录列表           |
| `QStringList files()`                         | 返回监视的文件列表           |
| `bool removePath(QString &path)`              | 移除监视的目录或文件         |
| `QStringList removePaths(QStringList &paths)` | 移除监视的目录或文件列表     |

- 信号：
  - `void QFileSystemWatcher::directoryChanged(const QString &path)`目录发生了变化
  - `void QFileSystemWatcher::fileChanged(const QString &path)`文件发生了变化

#### 3、读写文本文件

##### 用`QFile`读写文本文件

###### `QFile`类

- 读写文件相关的接口函数：

| 函数原型                                   | 功能                                                         |
| ------------------------------------------ | ------------------------------------------------------------ |
| `void setFileName(QString &name)`          | 设置文件名，在调用`open()`函数打开文件后不能再设置文件名     |
| `bool open(QIODeviceBase::OpenMode mode)`  | 打开文件，以只读、只写、可读可写等模式打开文件               |
| `qint64 read(char * data, qint64 maxSize)` | 读取最多`maxSize`字节的数据，存入缓冲区`data`函数返回值是实际读取的字节数 |
| `bool reset()`                             | 返回到文件的起始位置，可以从头开始读写                       |
| `QByteArray read(qint64 maxSize)`          | 读取最多`maxSize`字节的数据，返回为字节数组                  |
| `QByteArray readLine(qint64 maxSize = 0)`  | 读取一行文本，以换行符`\n`判断一行的结束。读取的内容返回为字节数组，在数组末尾会自动添加一个结束符`\0` |
| `QByteArray readAll()`                     | 读取文件的全部内容，返回为字节数组                           |
| `bool getChar(char *c)`                    | 从文件读取一个字符，存入`char*`指针指向的变量里              |
| `qint64 write(char *data, qint64 maxSize)` | 将缓冲区`data`里的数据写入文件，最多写入`maxSize`字节的数据，函数的返回值为实际写入的字节数。这个函数用于写入任意类型的数据 |
| `qint64 write(char *data)`                 | 这个函数用于写入`char*`类型的字符串数据。`data`是以`\0`作为结束符的字符串的首地址，函数返回值是实际写入文件的字节数 |
| `qint64 write(QByteArray &data)`           | 将一个字节数组的数据写入文件，函数返回值是实际写入的字节数   |
| `bool putChar(char c)`                     | 向文件写入一个`char`类型字符。`getChar()`和`putChar()`一般用于`I/O`设备的数据读写，例如串口数据读写 |
| `bool flush()`                             | 将任何缓存的数据保存到文件里，其作用相当于函数`close()`，但是不关闭文件。调用此函数可以防止未正常调用`close()`函数而导致的数据丢失 |
| `void close()`                             | 文件读写操作完成后关闭文件                                   |
| `bool atEnd()`                             | 判断是否到达文件末尾，返回`true`表示已经到达文件末尾         |

- 常用构造函数：
  - `QFile(const QString &name, QObject *parent)`指定文件名和父容器对象
  - `QFile(QObject *parent)`指定父容器对象
  - `QFile(const QString &name)`指定文件名
  - `QFile()`不做任何初始设置
- `QIODeviceBase::OpenMode `的主要枚举值含义：
  - `QIODevice::ReadOnly`以只读模式打开文件，加载文件时使用此模式
  - `QIODevice::WriteOnly`以只写模式打开文件，保存文件时使用此模式
  - `QIODevice::ReadWrite`以读写模式打开文件
  - `QIODevice::Append`以添加模式打开文件，新写入文件的数据添加到文件尾部
  - `QIODevice::Truncate`以截取模式打开文件，文件原有的内容全部被删除
  - `QIODevice::Text`以文本模式打开文件，读取时`\n`被自动翻译为一行的结束符，写入时字符串会被自动翻译为系统平台的编码。

##### 用`QSaveFile`保存文件

- `QSaveFile`专门用于保存文件，可以保存文本文件或二进制文件。
- 在保存文件之前，`QSaveFile`会在目标文件所在的目录下创建一个临时文件，向文件写入数据是先写入临时文件，如果写入操作没有错误，调用`QSaveFile`的函数`commit()`提交修改时临时文件里的内容才被移入目标文件，然后临时文件会被删除。
- 向文件写入数据的函数是在其父类中定义的。在`QSaveFile`中，函数`close()`变成了一个私有函数，不能再调用`close()`

| 函数原型                                    | 功能                                                     |
| ------------------------------------------- | -------------------------------------------------------- |
| `void setFileName(QString &name)`           | 设置文件名，在调用`open()`函数打开文件后不能再设置文件名 |
| `bool commit()`                             | 提交写入文件的修改，返回值为`true`表示提交修改成功       |
| `void cancelWriting()`                      | 取消写入操作，取消后就不能再调用`commit()`函数           |
| `void setDirectWriteFallback(bool enabled)` | 如果设置为`true`，则不使用临时文件，而直接向目标文件写入 |
| `bool directWriteFallback()`                | 返回值表示是否直接向目标文件写入                         |

##### 结合使用`QFile`和`QTextStream`读写文本文件

###### `QTextStream`类

- `QTextStream(QIODevice *device)`指定关联的`QIODevice`对象
- `QTextStream()`不指定任何关联对象
- 主要接口函数：

| 函数原型                                                | 功能                                                         |
| ------------------------------------------------------- | ------------------------------------------------------------ |
| `void setDevice(QIODevice *device)`                     | 设置关联的`QIODevice`设备                                    |
| `void setAutoDetectUnicode(bool enabled)`               | 设置是否自动检测`Unicode`编码                                |
| `void setEncoding(QStringConverter::Encoding encoding)` | 设置读写文本文件时的编码方式，默认是`UTF-8`编码方式          |
| `void setGenerateByteOrderMark(bool generate)`          | 设置是否产生字节序标记（`BOM`）。如果设置为`true`，且使用了某种`UTF`编码，那么`QTextStream`在向设备写入数据之前会插入`BOM`，否则不会插入。 |
| `void setIntegerBase(int base)`                         | 读写整数时使用的进制，默认是十进制。在文本文件中，整数是以字符串形式保存的 |
| `void setRealNumberPrecision(int precision)`            | 设置浮点数精度，即小数位数                                   |
| `QString read(qint64 maxlen)`                           | 读取最多`maxlen`个字符，返回值为`QString`字符串              |
| `QString readAll()`                                     | 读取文件的全部内容，返回值为`QString`字符串                  |
| `QString readLine(qint64 maxlen = 0)`                   | 读取一行文字，遇到`\n`时将其作为一行的结束                   |
| `QTextStream &operator>>(QString &str)`                 | 流读取操作符，将一个单词读取到`QString`字符串里，遇空格结束  |
| `QTextStream &operator>>(int &i)`                       | 流读取操作符，将一个整数字符串读取到`int`类型的变量里，遇空格结束 |
| `QTextStream &operator>>(double &f)`                    | 流读取操作符，将一个双精度浮点数字符串读取到`double`类型的变量里，遇空格结束 |
| `QTextStream &operator<<(QString &string)`              | 流写入操作符，将一个`QString`字符串写入流                    |
| `QTextStream &operator<<(int i)`                        | 流写入操作符，将一个`int`类型整数转换为字符串并将其写入流，字符串使用的整数进制由函数`integerBase()`的值确定 |
| `QTextStream &operator<<(double f)`                     | 流写入操作符，将一个`double`类型浮点数转换为字符串并将其写入流，字符串的小数位数由函数`realNumberPrecision()`的值确定 |
| `bool atEnd()`                                          | 返回值为`true`时，表示流里没有任何数据可读写了               |
| `qint64 pos()`                                          | 读写操作在流里的当前位置                                     |
| `bool seek(qint64 pos)`                                 | 读写位置定位到流的某个位置，如`seek(0)`就表示定位到流的开始位置 |
| `void flush()`                                          | 将任何缓存的数据保存到文件里                                 |

#### 4、读写二进制文件

- 内存中的数据字节序与`CPU`类型和操作系统有关，`Inter x86`、`AMD64`、`ARM`处理器全部采用小端字节序，而`MIPS`采用大端字节序。

##### `QDataStream`类

- 要读写二进制文件，一般可将`QFile`和`QDataStream`类结合使用。
- 流化数据的两种方式：
  - 使用`Qt`的预定义编码方式。将一些基本类型的数据和简单的`Qt`类序列化为二进制数据，这种预定义编码与操作系统、`CPU`类型和字节序无关。主要使用流写入操作符`<<`和流读取操作符`>>`分别进行数据的序列化写入和读取。
  - 使用原始二进制数据方式。所用的主要函数是`readRawData()`和`writeRawData()`，用这种方式向文件写入数据时，需要用户将数据转换为二进制数据然后将其写入文件，从文件读取的二进制数据需要解析为所需的类型的数据。
- 主要接口函数
  - `void setVersion(int v)`设置数据序列化格式版本
    - `Qt`中定义了版本号常数，例如：`QDataStream::Qt_6_8`
  - `void setByteOrder(QDataStream::ByteOrder bo)`设置字节序
    - `QDataStream::BigEndian`表示大端字节序
    - `QDataStream::LittleEndian`表示小端字节序
  - `void setFloatingPointPrecision(QDataStream::FloatingPointPrecision precision)`设置浮点数精度。
    - `QDataStream::SinglePrecision`单精度，数据流中的`float`和`double`都用4字节表示
    - `QDataStream::DoublePrecision`双精度，数据流中的`float`和`double`都用8字节表示
  - 事务处理
    - `void startTransaction()`开始一个读取操作的事务
    - `bool commitTransaction()`提交当前的读取操作事务，返回值为`true`表示读取过程没有错误
    - `void rollbackTransaction()`回滚当前的读取操作事务，一般在检测到错误时回滚
    - `void abortTransaction()`取消当前的读取操作事务
  - 在开始一个读取操作事务时，`QDataStream`会记住当前的文件读取点，如果在读取过程中出现错误，在使用回滚或取消事务操作后，可以回到开始读取操作事务时的文件读取点

```c++
template <class T>
void MainWindow::readByStream(T &value) {
  if (!QFile::exists(testFilename)) {
    QMessageBox::critical(this, "错误", "文件不存在，文件名：" + testFilename);
    return;
  }
  QFile fileDevice(testFilename);
  if (!fileDevice.open(QIODevice::ReadOnly)) {
    return;
  }
  QDataStream fileStream(&fileDevice);
  fileStream.setVersion(QDataStream::Qt_6_8);
  fileStream.setByteOrder(ui->radioButton->isChecked()
                              ? QDataStream::BigEndian
                              : QDataStream::LittleEndian);
  fileStream.setFloatingPointPrecision(ui->radioButton_3->isChecked()
                                           ? QDataStream::SinglePrecision
                                           : QDataStream::DoublePrecision);
  fileStream >> value;
  fileDevice.close();
}

template <class T>
void MainWindow::writeByStream(T value) {
  QFile fileDevice(testFilename);
  if (!fileDevice.open(QIODevice::WriteOnly)) {
    return;
  }
  QDataStream fileStream(&fileDevice);
  fileStream.setVersion(QDataStream::Qt_6_8);
  fileStream.setByteOrder(ui->radioButton->isChecked()
                              ? QDataStream::BigEndian
                              : QDataStream::LittleEndian);
  fileStream.setFloatingPointPrecision(ui->radioButton_3->isChecked()
                                           ? QDataStream::SinglePrecision
                                           : QDataStream::DoublePrecision);
  fileStream << value;
  fileDevice.close();
}
```

##### 使用原始二进制数据方式读写文件

- `int writeRawData(const char *s, int len)`用于将原始数据写入数据流。`s`是`char`类型的缓冲指针，表示待写入流的原始数据。`len`是要写入的数据字节数。`len`需要小于或等于`s`的长度。函数返回值是实际写入的字节数。如果返回`-1`表示写入过程中出现了错误。
- `int readRawData(char *s, int len)`用于从数据流读取原始数据。`s`是用于存储读出数据的缓冲区指针，需要预先分配内存，`len`是要读取的数据字节数，返回实际读取的字节数。如果返回`-1`表示读取过程中出现了错误。
- `QDataStream &writeBytes(const char *s, uint len)`用于将一个字节数组的数据写入流。`s`表示待写入流的原始数据，函数返回值是流的引用。
  - 在将数据写入流时，会先将参数`len`按`uint`类型序列化为4字节数据写在前面，然后写入`s`里的数据
- `QDataStream &readBytes(char *&s, unit &len)`不需要为`s`预先分配内存，`len`是返回值，表示实际读出的字节数。
- `writeRawData()`适合写入各种整数、浮点数等基本类型数据，`writeBytes()`适合写入字符串数据，因为字符串的长度不固定。
- 使用`writeRawData()`函数将基本类型的数据转换为二进制字节数据流时，会自动使用操作系统的字节序，不受函数`setByteOrder()`设置的字节序的影响。
- 使用`writeRawData()`写入的`float`类型数据总是4字节的，写入的`double`类型数据总是8字节的，不受函数`setFloatingPointPrecision()`设置的浮点数精度的影响。

### 九、数据库

#### 1、`Qt`数据库编程概述

##### `Qt SQL`模块

- 如果要遭`Qt`项目中使用数据库编程功能，需要将`Qt SQL`模块添加到项目中，也就是需要在项目配置文件`.pro`中增加：`QT += sql`
- 如果要在头文件或源程序中用到`Qt SQL`模块的类，可以使用包含如下的语句：`#include<QtSql>`，这样会包含常用的类，但不包含`Qt SQL`中所有的类。

###### `Qt SQL`支持的数据库

| 驱动名    | 数据库                                                      |
| --------- | ----------------------------------------------------------- |
| `QDB2`    | `IBM DB2`数据库，7.1及以上版本                              |
| `QMYSQL`  | `MySQL`或`MariaDB`数据库，5.6及以上版本                     |
| `QOCI`    | `Oracle Call Interface`(`OCI`)，12.1及以上版本              |
| `QODBC`   | 支持开放式数据库互连的数据库，例如`MS SQL Server`、`Access` |
| `QPSQL`   | `PostgreSQL`数据库，7.3及以上版本                           |
| `QSQLITE` | `SQLite3`数据库                                             |

###### `Qt SQL`模块中的主要类

<table>
    <tr>
    	<th>类别</th>
        <th>类名称</th>
        <th>功能描述</th>
    </tr>
    <tr>
        <td>数据库连接</td>
        <td><code>QSqlDatabase</code></td>
        <td>用于建立与数据库的连接</td>
    </tr>
    <tr>
        <td rowspan="3">数据库中的对象</td>
        <td><code>QSqlRecord</code></td>
        <td>表示数据表中一条记录的类</td>
    </tr>
    <tr>
        <td><code>QSqlField</code></td>
        <td>表示数据表或视图的字段的类</td>
    </tr>
    <tr>
        <td><code>QSqlIndex</code></td>
        <td>表示数据库中的索引的类</td>
    </tr>
    <tr>
        <td rowspan="2">模型类</td>
        <td><code>QSqlTableModel</code></td>
        <td>表示单个数据表的模型类</td>
    </tr>
    <tr>
        <td><code>QSqlQueryModel</code></td>
        <td>表示<code>SQL</code>查询结果数据的只读模型类</td>
    </tr>
    <tr>
        <td rowspan="3">其它功能类</td>
        <td><code>QSqlQuery</code></td>
        <td>运行各种<code>SQL</code>语句的类</td>
    </tr>
    <tr>
        <td><code>QDataWidgetMapper</code></td>
        <td>用于建立界面组件与字段的映射关系的类</td>
    </tr>
    <tr>
        <td><code>QSqlError</code></td>
        <td>用于表示数据库错误信息的类，可用于访问上一次出错的信息</td>
    </tr>
    <tr>
        <td rowspan="3">关系模型</td>
        <td><code>QSqlRelationalTableModel</code></td>
        <td>表示关系数据库的模型类</td>
    </tr>
    <tr>
        <td><code>QSqlRelationalDelegate</code></td>
        <td>用于<code>QSqlRelationalTableModel</code>模型的一个编码字段的代理类，这个代理类提供一个<code>QComboBox</code>组件作为编辑器</td>
    </tr>
    <tr>
        <td><code>QSqlRelation</code></td>
        <td>用于表示数据表外键信息的类</td>
    </tr>
</table>


#### 2、`QSqlTableModel`的使用

- `QSqlTableModel`是一个模型类，他的实例可以作为一个数据表的模型。通过使用`QSqlTableModel`模型和`QTableView`组件构成模型/视图结构，就可以实现数据表的数据显示和编辑。

##### 主要的类和基本工作原理

###### `QSqlDatabase`类

- 创建数据库连接，即创建`QSqlDatabase`对象，加载指定类型的数据库驱动

  - `QSqlDatabase QSqlDatabase::addDatabase(const QString &type, const QString &connectionName = QLatin1String(defaultConnection))`
  - `type`是要连接的数据库类型
  - `defaultConnection`是所创建的数据库连接的名称
  - 如果成功创建`QSqlDatabase`对象，`QSqlDatabase`的函数`isValid()`会返回`true`
  - `QStringList QSqlDatabase::connectionNames()`返回数据库连接名称列表
  - 引用其中某个数据库连接：`QSqlDatabase QSqlDatabase::database(const QString &connectionName = QLatin1String(defaultConnection), bool open = true)`其中`open`表示是否要打开数据库。

- 打开数据库，设置需要连接的数据库具体参数，例如数据库名称、用户名、用户密码等

  | 读取函数                   | 设置函数              | 设置函数的功能                                               |
  | -------------------------- | --------------------- | ------------------------------------------------------------ |
  | `QString databaseName()`   | `setDatabaseName()`   | 设置需要连接的数据库名称                                     |
  | `QString userName()`       | `setUserName()`       | 设置登录数据库的用户名                                       |
  | `QString password()`       | `setPassword()`       | 设置登录数据库的用户密码                                     |
  | `QString hostName()`       | `setHostName()`       | 设置数据库服务器的主机名或IP地址                             |
  | `int port()`               | `setPort()`           | 设置数据库服务器的端口号                                     |
  | `QString connectOptions()` | `setConnectOptions()` | 设置数据库连接的其它选项，一般是某种类型数据库需要的一些专用选项 |

  - 设置好数据库参数后，用函数`open()`打开数据库，返回`true`表示成功打开数据库。若要关闭数据库，则要使用函数`close()`

- 对数据库进行事务操作，获取数据库的一些信息。

<table>
    <tr>
    	<th>功能分组</th>
        <th>函数名</th>
        <th>功能描述</th>
    </tr>
    <tr>
    	<td rowspan="5">获取数据库的信息</td>
        <td><code>QString driverName()</code></td>
        <td>返回数据库连接的驱动名称</td>
    </tr>
    <tr>
        <td><code>QString connectionName()</code></td>
        <td>返回数据库连接的连接名称</td>
    </tr>
    <tr>
        <td><code>QStringList tables()</code></td>
        <td>返回数据库中的数据表、系统表、视图的名称列表</td>
    </tr>
    <tr>
        <td><code>QSqlIndex primaryIndex()</code></td>
        <td>返回某个数据表的主索引</td>
    </tr>
    <tr>
        <td><code>QSqlRecord record()</code></td>
        <td>返回某个数据表的所有字段名称</td>
    </tr>
    <tr>
    	<td rowspan="4">获取状态信息</td>
        <td><code>bool isValid()</code></td>
        <td>如果创建的<code>QSqlDatabase</code>对象成功加载了数据库驱动，此函数返回<code>true</code></td>
    </tr>
    <tr>
        <td><code>bool isOpen()</code></td>
        <td>如果运行函数<code>open()</code>成功打开了数据库，此函数返回<code>true</code></td>
    </tr>
    <tr>
        <td><code>bool isOpenError()</code></td>
        <td>如果运行函数<code>open()</code>成功打开数据库时出现了错误，此函数返回<code>true</code></td>
    </tr>
    <tr>
        <td><code>QSqlError lastError()</code></td>
        <td>返回数据库操作的上一条错误信息</td>
    </tr>
    <tr>
    	<td rowspan="3">事务操作</td>
        <td><code>bool transaction()</code></td>
        <td>开始一个事务，若此函数返回<code>true</code>，表示成功开始事务</td>
    </tr>
    <tr>
        <td><code>bool commit()</code></td>
        <td>提交事务，若此函数返回<code>true</code>，表示成功提交了事务</td>
    </tr>
    <tr>
        <td><code>bool rollback()</code></td>
        <td>事务回滚，若此函数返回值为<code>true</code>，表示事务回滚操作成功</td>
    </tr>
</table>

- 函数`tables()`可以返回当前数据库里用户权限范围内的数据表、系统表、视图的名称列表。`QStringList QSqlDatabase::tables(QSql::TableType type = QSql::Tables)`
  - `QSql::Tables`：用户可访问的所有数据表；
  - `QSql::SystemTables`：数据库内部使用的数据表
  - `QSql::Views`：用户可以访问的视图，数据库中的视图就是用`SQL`语句查询得到的结果数据集
  - `QSql::AllTables`：用户可以访问的所有数据表和视图

###### `QSqlTableModel`类

- 他是一个模型类，与数据库中的一个数据表关联后就作为该数据表的模型。`QSqlTableModel(QObject *parent = nullptr, const QSqlDatabase &db = QSqlDatabase())`
- 创建`QSqlTableModel`对象时需要指定数据库连接，也就是设置一个`QSqlDatabase`对象。如果不指定数据库连接，就使用应用程序的默认数据库连接。
- 还需要使用`QSqlTableModel`的`setTable()`函数设置需要连接的数据表。
- 其它还有表示记录的类`QSqlRecord`和表示字段的类`QSqlField`

<table>
    <tr>
    	<th>功能分组</th>
        <th>函数</th>
        <th>功能描述</th>
    </tr>
    <tr>
    	<td rowspan="5">模型属性</td>
        <td><code>QSqlDatabase database()</code></td>
        <td>返回模型的数据库连接</td>
    </tr>
    <tr>
        <td><code>void setTable()</code></td>
        <td>设置一个数据表作为数据源, 不会立即刷新数据记录, 但会提取字段信息</td>
    </tr>
    <tr>
        <td><code>QString tableName()</code></td>
        <td>返回设置的数据表名称</td>
    </tr>
    <tr>
        <td><code>bool setHeaderData()</code></td>
        <td>设置表头数据, 一般用于设置字段的显示标题。这是父类中定义的函数</td>
    </tr>
    <tr>
        <td><code>void setEditStrategy()</code></td>
        <td>设置编辑策略, 也就是数据内容被修改后如何将修改提交到数据库</td>
    </tr>
    <tr>
    	<td rowspan="4">数据表信息</td>
        <td><code>int fieldIndex()</code></td>
        <td>根据字段名称返回其在模型中的字段序号, 若字段不存在则返回-1</td>
    </tr>
    <tr>
        <td><code>QSqlIndex primaryKey()</code></td>
        <td>返回数据表的主索引</td>
    </tr>
    <tr>
        <td><code>int rowCount()</code></td>
        <td>返回数据表的记录条数</td>
    </tr>
    <tr>
        <td><code>QSqlError lastError()</code></td>
        <td>返回最后一次错误信息。这是父类中定义的函数</td>
    </tr>
    <tr>
    	<td rowspan="4">过滤和排序</td>
        <td><code>void setFilter()</code></td>
        <td>设置记录过滤条件, 会立即刷新数据</td>
    </tr>
    <tr>
        <td><code>QString filter()</code></td>
        <td>返回当前的过滤条件</td>
    </tr>
    <tr>
        <td><code>void setSort()</code></td>
        <td>设置排序字段和排序规则, 需要用<code>select()</code>函数才能刷新数据</td>
    </tr>
    <tr>
        <td><code>void sort()</code></td>
        <td>按某个字段和某种排序规则进行排序, 并立即刷新数据</td>
    </tr>
    <tr>
    	<td rowspan="2">数据刷新</td>
        <td><code>bool select()</code></td>
        <td>使用设置的排序和过滤规则查询数据表的数据, 并将其刷新到模型</td>
    </tr>
    <tr>
        <td><code>bool selectRow()</code></td>
        <td>获取数据表中指定行号的一条数据, 并将其刷新到模型</td>
    </tr>
    <tr>
    	<td rowspan="9">记录操作</td>
        <td><code>QSqlRecord record()</code></td>
        <td>返回模型中的某一条记录的数据。若不指定行号, 则返回的<code>QSqlRecord</code>对象只有字段名, 可用来获取字段信息</td>
    </tr>
    <tr>
        <td><code>bool setRecord()</code></td>
        <td>更新一条<code>QSqlRecord</code>类型的记录到模型的某一行, 源和目标记录通过字段名称匹配, 而不是按位置匹配</td>
    </tr>
    <tr>
        <td><code>bool setData()</code></td>
        <td>用模型索引定位记录和字段, 为字段设置数据</td>
    </tr>
    <tr>
        <td><code>bool insertRecord()</code></td>
        <td>在指定行之前插入一条<code>QSqlRecord</code>类型的记录</td>
    </tr>
    <tr>
        <td><code>bool insertRow()</code></td>
        <td>在某一行之前插入一个空行。这是父类中定义的函数</td>
    </tr>
    <tr>
        <td><code>bool removeRow()</code></td>
        <td>删除指定行号的某一行。这是父类中定义的函数</td>
    </tr>
    <tr>
        <td><code>bool insertRows()</code></td>
        <td>在某一行之前插入指定数量的空行, 编辑策略为<code>OnFieldChange</code>或<code>OnRowChange</code>时只能插入一行</td>
    </tr>
    <tr>
        <td><code>bool removeRows()</code></td>
        <td>从某一行开始, 删除指定数量的行。若编辑策略为<code>OnManualSubmit</code>, 需调用<code>submitAll()</code>才能从数据表里删除行</td>
    </tr>
    <tr>
        <td><code>void clear()</code></td>
        <td>清除模型的所有记录和字段定义信息</td>
    </tr>
    <tr>
    	<td rowspan="6">取消或提交修改</td>
        <td><code>bool isDirty()</code></td>
        <td>若有未更新到数据库的修改, 返回<code>true</code>, 否则返回<code>false</code></td>
    </tr>
    <tr>
        <td><code>void revertRow()</code></td>
        <td>取消对某一行记录的修改</td>
    </tr>
    <tr>
        <td><code>void revert()</code></td>
        <td>编辑策略为<code>OnRowChange</code>或<code>OnFieldChange</code>时取消对当前行的修改, 对<code>OnManualSubmit</code>编辑策略无效</td>
    </tr>
    <tr>
        <td><code>void revertAll()</code></td>
        <td>取消所有未提交的修改</td>
    </tr>
    <tr>
        <td><code>bool submit()</code></td>
        <td>提交对当前行的修改到数据库, 对<code>OnManualSubmit</code>编辑策略无效</td>
    </tr>
    <tr>
        <td><code>bool submitAll()</code></td>
        <td>提交所有未更新的修改到数据库, 若提交成功则返回<code>true</code>, 否则返回<code>false</code></td>
    </tr>
</table>


- 编辑策略：
  - `QSqlTableModel::OnFieldChange`：字段的值变化时立即更新到数据库
  - `QSqlTableModel::OnRowChange`：当前行变化时，对前一记录所进行的修改自动更新到数据库
  - `QSqlTableModel::OnManualSubmit`：所有修改暂时缓存，调用函数`submitAll()`保存所有修改，或调用函数`revertAll()`取消所有未保存修改。

###### 表示一条记录的类`QSqlRecord`

- `QSqlRecord`类型表示数据当前记录，包含每个字段的内容。

- 不带参数的函数`record()`返回的一个`QSqlRecord`对象只有记录的字段定义。

- `QSqlRecord`类常用函数（省略函数参数中的`const`关键字）：

  | 函数原型                                      | 功能描述                                                 |
  | --------------------------------------------- | -------------------------------------------------------- |
  | `void clear()`                                | 清除记录的所有字段定义和数据                             |
  | `void clearValues()`                          | 清除所有字段的数据，将字段数据内容设置为`null`           |
  | `bool contains(QString &name)`                | 判断记录是否含有名称是`name`的字段                       |
  | `bool isEmpty()`                              | 若记录里没有字段，返回`true`否则返回`false`              |
  | `int count()`                                 | 返回记录的字段的个数                                     |
  | `QString fieldName(int index)`                | 返回序号为`index`的字段的名称                            |
  | `int indexOf(QString &name)`                  | 返回字段名称为`name`的字段的序号，如果字段不存在，返回-1 |
  | `QSqlField field(QString &name)`              | 返回字段名称为`name`的字段对象                           |
  | `QVariant value(QString &name)`               | 返回字段名称为`name`的字段的值                           |
  | `void setValue(QString &name, QVariant &val)` | 设置字段名称为`name`的字段的值为`val`                    |
  | `bool isNull(const QString &name)`            | 判断字段名称为`name`的字段数据是否为`null`               |
  | `void setNull(const QString &name)`           | 设置名称为`name`的字段的值为`null`                       |

- 排序：

  - `void QSqlTableModel::setSort(int column, Qt::SortOrder order)`设置排序条件
  - `void QSqlTableModel::sort(int column, Qt::SortOrder order)`立刻排序

- 过滤：

  - `void QSqlTableModel::setFilter(const QString &filter)`
  - `filter`实际上就是`select`语句里的`where`子句的条件。
  - 运行`setFilter()`函数后无需调用`select()`函数即可立即刷新记录。

###### 表示一个字段的类`QSqlField`

- 常用函数（省略函数参数中的`const`关键字）：

  <table>
      <tr>
      	<th>分组</th>
          <th>函数原型</th>
          <th>功能描述</th>
      </tr>
      <tr>
      	<td rowspan="12">字段定义信息</td>
          <td><code>QString name()</code></td>
          <td>返回字段名称</td>
      </tr>
      <tr>
      	<td><code>QString tableName()</code></td>
          <td>返回字段所在的数据表名称</td>
      </tr>
      <tr>
      	<td><code>QMetaType metaType()</code></td>
          <td>返回字段的类型</td>
      </tr>
      <tr>
      	<td><code>bool isValid()</code></td>
          <td>若返回值为<code>true</code>，表示字段数据类型是有效的</td>
      </tr>
      <tr>
      	<td><code>int length()</code></td>
          <td>返回字段的长度</td>
      </tr>
      <tr>
      	<td><code>int precision()</code></td>
          <td>返回字段的精度，只对数据类型有意义</td>
      </tr>
      <tr>
      	<td><code>QVariant defaultValue()</code></td>
          <td>返回字段的默认值，默认值可能是<code>null</code></td>
      </tr>
      <tr>
      	<td><code>bool isAutoValue()</code></td>
          <td>若返回值是<code>true</code>，表示该字段是数据库自动生成数值的字段，例如自动增长的主键字段</td>
      </tr>
      <tr>
      	<td><code>bool isGenerated()</code></td>
          <td>若返回值为<code>false</code>，表示模型不会为该字段生成<code>SQL</code>语句</td>
      </tr>
      <tr>
      	<td><code>bool isReadOnly()</code></td>
          <td>若返回值为<code>true</code>表示字段数据是只读的</td>
      </tr>
      <tr>
      	<td><code>bool setReadOnly(bool readOnly)</code></td>
          <td>设置一个字段为只读的，只读的字段不能用函数<code>setValue()</code>设置值，也不能用函数<code>clear()</code>清除值</td>
      </tr>
      <tr>
      	<td><code>bool requiredStatus()</code></td>
          <td>字段是否为必填字段，返回<code>true</code>表示字段是必填字段</td>
      </tr>
      <tr>
      	<td rowspan="4">字段数据</td>
          <td><code>void clear()</code></td>
          <td>清除字段数据，字段数据设置为<code>null</code>。如果字段是只读的，则不清除</td>
      </tr>
      <tr>
      	<td><code>bool isNull()</code></td>
          <td>若返回<code>true</code>，表示字段数据为<code>null</code></td>
      </tr>
      <tr>
      	<td><code>QVariant value()</code></td>
          <td>返回字段的值</td>
      </tr>
      <tr>
      	<td><code>void setValue(QVariant &value)</code></td>
          <td>设置字段的值</td>
      </tr>
  </table>

###### `QTableView`类

- 为一个`QTableView`组件设置一个`QSqlTableModel`模型后，他们就组成模型/视图结构，可以显示和编辑数据表的数据。还可以为`QSqlTableModel`模型设置一个`QItemSelectionModel`对象作为选择模型。在`QTableView`组件中可以使用自定义代理。

###### `QDataWidgetMapper`类

- 该对象要设置一个`QSqlTableModel`模型，然后将数据表的某个字段与界面上某个组件建立映射，界面组件就可以自动显示这个字段的数据，成为数据感知组件。

- 一般的数值、字符串、备注等类型的字段可以用`QSpinBox`、`QLineEdit`、`QPlainTextEdit`等界面组件作为数据感知组件。但是`BLOB`类型的字段不能直接与某个界面组件建立映射。

- 常用函数：

  <table>
      <tr>
      	<th>分组</th>
          <th>函数</th>
          <th>功能描述</th>
      </tr>
      <tr>
      	<td rowspan="3">特性设置</td>
          <td><code>void setModel()</code></td>
          <td>设置数据模型，可以使用<code>QSqlTableModel</code>或<code>QSqlQueryModel</code>对象作为数据模型</td>
      </tr>
      <tr>
      	<td><code>void setSubmitPolicy()</code></td>
          <td>设置数据提交方式，指界面组件上的数据更新到模型的方式</td>
      </tr>
      <tr>
      	<td><code>void setOrientation()</code></td>
          <td>设置模型的方向，默认为<code>Qt::Horizontal</code>，即水平方向</td>
      </tr>
      <tr>
      	<td rowspan="5">映射关系管理</td>
          <td><code>void addMapping()</code></td>
          <td>添加一个映射，将某个界面组件与模型的某个字段建立映射</td>
      </tr>
      <tr>
      	<td><code>void removeMapping()</code></td>
          <td>移除某个界面组件的映射</td>
      </tr>
      <tr>
      	<td><code>void clearMapping()</code></td>
          <td>清除与模型有关的所有映射</td>
      </tr>
      <tr>
      	<td><code>int mappedSection()</code></td>
          <td>返回某个界面组件映射的字段的序号</td>
      </tr>
      <tr>
      	<td><code>QWidget *mappedWidgetAt()</code></td>
          <td>返回某个序号的字段映射的界面组件</td>
      </tr>
      <tr>
      	<td rowspan="7">记录移动</td>
          <td><code>void toFirst()</code></td>
          <td>移动到模型的首条记录，并刷新界面数据</td>
      </tr>
      <tr>
      	<td><code>void toPrevious()</code></td>
          <td>移动到模型的前一条记录，并刷新界面数据</td>
      </tr>
      <tr>
      	<td><code>void toNext()</code></td>
          <td>移动到模型的后一条记录，并刷新界面数据</td>
      </tr>
      <tr>
      	<td><code>void toLast()</code></td>
          <td>移动到模型的最后一条记录，并刷新界面数据</td>
      </tr>
      <tr>
      	<td><code>int setCurrentIndex()</code></td>
          <td>移动到指定行号的记录，并刷新界面数据</td>
      </tr>
      <tr>
      	<td><code>int currentIndex()</code></td>
          <td>返回当前行号</td>
      </tr>
      <tr>
      	<td><code>void setCurrentModelIndex()</code></td>
          <td>以模型索引作为参数设置当前行，并刷新界面数据</td>
      </tr>
      <tr>
      	<td rowspan="2">数据保存</td>
          <td><code>bool submit()</code></td>
          <td>将界面组件上修改后的数据提交到模型</td>
      </tr>
      <tr>
      	<td><code>void revert()</code></td>
          <td>重新从模型获取数据并刷新到界面组件上，所有未提交的修改丢失</td>
      </tr>
      <tr>
      	<td>信号</td>
          <td><code>void currentIndexChanged()</code></td>
          <td>当前行变化时发射此信号</td>
      </tr>
  </table>


  - `setSubmitPolicy`的参数可以使用两种枚举值：
    - `QDataWidgetMapper::AutoSubmit`：自动提交。当一个界面组件失去输入焦点时，所进行的修改自动提交到模型。
    - `QDataWidgetMapper::ManualSubmit`：手动提交。需要调用函数`submit()`才能将修改提交到模型。

#### 3、`QSqlQueryModel`的使用

- 它是`QSqlTableModel`的父类。可以设置任意的`select`语句来从数据库查询数据。

- 该模型的数据是只读的。

- 常用接口函数：

  <table>
      <tr>
      	<th>分组</th>
          <th>函数</th>
          <th>功能描述</th>
      </tr>
      <tr>
      	<td rowspan="3">查询数据</td>
          <td><code>void setQuery()</code></td>
          <td>设置一个<code>QSqlQuery</code>对象，或直接设置<code>select</code>语句以从数据库查询数据</td>
      </tr>
      <tr>
          <td><code>QSqlQuery query()</code></td>
          <td>返回当前关联的<code>QSqlQuery()</code>对象</td>
      </tr>
      <tr>
          <td><code>QSqlError lastError()</code></td>
          <td>返回上次的错误信息，可获取错误的类型和文本信息</td>
      </tr>
      <tr>
      	<td rowspan="6">访问数据</td>
          <td><code>QSqlRecord record()</code></td>
          <td>返回模型中的某一条记录的数据。若不指定行号，则返回的<code>QSqlRecord</code>对象只有字段名，可用于获取字段信息</td>
      </tr>
      <tr>
          <td><code>int rowCount()</code></td>
          <td>返回查询到的记录条数</td>
      </tr>
      <tr>
          <td><code>int columnCount()</code></td>
          <td>返回查询到的记录条数</td>
      </tr>
      <tr>
          <td><code>void clear()</code></td>
          <td>清除模型的所有数据</td>
      </tr>
      <tr>
          <td><code>QVariant data()</code></td>
          <td>通过模型索引获取某行某列的数据</td>
      </tr>
      <tr>
          <td><code>void setHeaderData()</code></td>
          <td>设置表头数据，一般用于设置字段的表头标题</td>
      </tr>
  </table>

- 要从数据库查询数据并将其作为`QSqlQueryModel`的数据源，需要运行函数`setQuery()`，其原型：

  - `void QSqlQueryModel::setQuery(const QString &query, const QSqlDatabase &db = QSqlDatabase())`
  - `void QSqlQueryModel::setQuery(QSqlQuery &&query)`
    - `query`是可以运行任何`sql`语句的类。

- 查询出数据后就可以使用`QSqlQueryModel`的函数`record()`访问数据记录。

```c++
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDataWidgetMapper>
#include <QMainWindow>
#include <QtSql>
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

 private:
  Ui::MainWindow *ui;
  // 数据库连接
  QSqlDatabase db;
  // 数据模型
  QSqlQueryModel *queryModel;
  // 选择模型
  QItemSelectionModel *selectionModel;
  // 数据界面映射
  QDataWidgetMapper *dataMapper;
  // 查询数据
  void selectData();
  // 移动记录后刷新tableView上的当前行
  void refreshTableView();
 private slots:
  void do_currentRowChanged(const QModelIndex &current,
                            const QModelIndex &previous);
  void on_actOpen_triggered();
  void on_actFirst_triggered();
  void on_actPrevious_triggered();
  void on_actNext_triggered();
  void on_actLast_triggered();
};
#endif  // MAINWINDOW_H
```

```c++
#include "mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>

#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  // 不可编辑
  ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
  // 行选择
  ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
  // 单行选择
  ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
  ui->tableView->setAlternatingRowColors(true);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::selectData() {
  // 1.创建数据模型, 查询数据
  queryModel = new QSqlQueryModel(this);
  queryModel->setQuery(
      "select empNo,name,gender,birthday,province,department,salary from "
      "employee order by empNo");
  if (queryModel->lastError().isValid()) {
    QMessageBox::critical(
        this, "错误",
        "数据表查询错误，错误信息\n" + queryModel->lastError().text());
    return;
  }
  ui->statusbar->showMessage(
      QString("记录条数: %1").arg(queryModel->rowCount()));
  // 2.设置字段显示标题
  QSqlRecord record = queryModel->record();
  queryModel->setHeaderData(record.indexOf("empNo"), Qt::Horizontal, "工号");
  queryModel->setHeaderData(record.indexOf("name"), Qt::Horizontal, "姓名");
  queryModel->setHeaderData(record.indexOf("gender"), Qt::Horizontal, "性别");
  queryModel->setHeaderData(record.indexOf("birthday"), Qt::Horizontal,
                            "出生日期");
  queryModel->setHeaderData(record.indexOf("province"), Qt::Horizontal, "省份");
  queryModel->setHeaderData(record.indexOf("department"), Qt::Horizontal,
                            "部门");
  queryModel->setHeaderData(record.indexOf("salary"), Qt::Horizontal, "工资");
  // 3.创建选择模型
  selectionModel = new QItemSelectionModel(queryModel, this);
  connect(selectionModel, &QItemSelectionModel::currentChanged, this,
          &MainWindow::do_currentRowChanged);
  ui->tableView->setModel(queryModel);
  ui->tableView->setSelectionModel(selectionModel);
  // 4.创建数据组件映射
  dataMapper = new QDataWidgetMapper(this);
  dataMapper->setSubmitPolicy(QDataWidgetMapper::AutoSubmit);
  dataMapper->setModel(queryModel);
  // 界面组件与模型的具体字段的映射
  dataMapper->addMapping(ui->spinBox, record.indexOf("empNo"));
  dataMapper->addMapping(ui->lineEdit, record.indexOf("name"));
  dataMapper->addMapping(ui->comboBox, record.indexOf("gender"));
  dataMapper->addMapping(ui->dateEdit, record.indexOf("birthday"));
  dataMapper->addMapping(ui->comboBox_2, record.indexOf("province"));
  dataMapper->addMapping(ui->comboBox_3, record.indexOf("department"));
  dataMapper->addMapping(ui->doubleSpinBox, record.indexOf("salary"));
  dataMapper->toFirst();
  ui->actOpen->setEnabled(false);
}

void MainWindow::refreshTableView() {
  // 刷新tableView的当前行
  int index = dataMapper->currentIndex();
  QModelIndex curIndex = queryModel->index(index, 1);
  selectionModel->clearSelection();
  selectionModel->setCurrentIndex(curIndex, QItemSelectionModel::Select);
}

void MainWindow::do_currentRowChanged(const QModelIndex &current,
                                      const QModelIndex &previous) {
  Q_UNUSED(previous);
  if (!current.isValid()) {
    ui->label_8->clear();
    ui->plainTextEdit->clear();
    return;
  }
  // 设置当前行
  dataMapper->setCurrentModelIndex(current);

  bool first = (current.row() == 0);
  bool last = current.row() == queryModel->rowCount() - 1;
  ui->actFirst->setEnabled(!first);
  ui->actPrevious->setEnabled(!first);
  ui->actNext->setEnabled(!last);
  ui->actLast->setEnabled(!last);

  int curRecNo = selectionModel->currentIndex().row();
  QSqlRecord curRec = queryModel->record(curRecNo);
  int empNo = curRec.value("empNo").toInt();
  QSqlQuery query;
  query.prepare("select photo, memo from employee where empNo = :empNo");
  query.bindValue(":empNo", empNo);
  query.exec();
  query.first();

  QVariant va = query.value("photo");
  if (va.isValid()) {
    ui->label_8->clear();
  } else {
    QByteArray data = va.toByteArray();
    QPixmap pic;
    pic.loadFromData(data);
    ui->label_8->setPixmap(pic.scaledToWidth(ui->label_8->size().width()));
  }
  QVariant va2 = query.value("memo");
  ui->plainTextEdit->setPlainText(va2.toString());
}

void MainWindow::on_actOpen_triggered() {
  QString filename = QFileDialog::getOpenFileName(
      this, "选择文件", QDir::currentPath(), "SQLite数据库(*.db)");
  if (filename.isEmpty()) {
    return;
  }
  db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName(filename);
  if (db.open()) {
    selectData();
  } else {
    QMessageBox::warning(this, "错误", "打开数据库失败");
  }
}

void MainWindow::on_actFirst_triggered() {
  dataMapper->toFirst();
  refreshTableView();
}

void MainWindow::on_actPrevious_triggered() {
  dataMapper->toPrevious();
  refreshTableView();
}

void MainWindow::on_actNext_triggered() {
  dataMapper->toNext();
  refreshTableView();
}

void MainWindow::on_actLast_triggered() {
  dataMapper->toLast();
  refreshTableView();
}
```

#### 4、`QSqlQuery`的使用

- 它没有父类，可以执行任何`sql`语句，如果执行查询操作，它查询出的结果可以作为一个数据集，但是不能作为模型/视图结构中的数据模型。

- 常用接口函数（列出了函数名和返回值类型）：

<table>
    <tr>
    	<th>分组</th>
        <th>函数</th>
        <th>功能描述</th>
    </tr>
    <tr>
    	<td rowspan="12">SQL语句的设置和运行</td>
        <td><code>bool prepare()</code></td>
        <td>设置准备运行的<code>SQL</code>语句，一般用于设置带有参数的<code>SQL</code>语句</td>
    </tr>
    <tr>
        <td><code>void bindValue()</code></td>
        <td>为函数<code>prepare()</code>中设置的某个参数绑定数值，也就是设置<code>SQL</code>语句中某个参数的值</td>
    </tr>
    <tr>
        <td><code>void addBindValue()</code></td>
        <td>当函数<code>prepare()</code>中设置的<code>SQL</code>语句中使用问号占位符时，可以使用此函数添加参数值</td>
    </tr>
    <tr>
        <td><code>QVariant boundValue()</code></td>
        <td>返回<code>SQL</code>语句中已绑定的某个参数的值</td>
    </tr>
    <tr>
        <td><code>QVariantList boundValues()</code></td>
        <td>返回<code>SQL</code>语句中已绑定的参数值的列表</td>
    </tr>
    <tr>
        <td><code>bool exec()</code></td>
        <td>该函数有两种参数形式。若不带任何参数，则运行由<code>prepare()</code>和<code>bindValue()</code>设置的<code>SQL</code>语句；若设置的参数是一条<code>SQL</code>语句，则直接运行这条<code>SQL</code>语句</td>
    </tr>
    <tr>
        <td><code>QString executedQuery()</code></td>
        <td>返回上一次成功运行过的<code>SQL</code>语句</td>
    </tr>
    <tr>
        <td><code>QString lastQuery()</code></td>
        <td>返回当前使用的<code>SQL</code>语句</td>
    </tr>
    <tr>
        <td><code>QSqlError lastError()</code></td>
        <td>返回上一次出现错误的信息</td>
    </tr>
    <tr>
        <td><code>bool isActive()</code></td>
        <td>如果成功运行了函数<code>exec()</code>，就返回<code>true</code></td>
    </tr>
    <tr>
        <td><code>bool isSelect()</code></td>
        <td>如果运行的<code>SQL</code>语句是<code>select</code>语句，就返回<code>true</code></td>
    </tr>
    <tr>
        <td><code>void clear()</code></td>
        <td>清除结果数据集，并释放所占用的资源</td>
    </tr>
    <tr>
    	<td rowspan="5">数据集</td>
        <td><code>QSqlRecord record()</code></td>
        <td>若函数<code>isValid()</code>返回<code>true</code>，该函数返回当前记录的结构和数据，否则返回一条空记录</td>
    </tr>
    <tr>
        <td><code>QVariant value()</code></td>
        <td>返回当前记录中某个字段的值，可使用字段名称或字段序号作为输入参数</td>
    </tr>
    <tr>
        <td><code>bool isNull()</code></td>
        <td>判断一个字段是否为空，当<code>isActive()</code>的返回值为<code>false、isValid()</code>的返回值为<code>false</code>、字段名称不存在或字段数据为空时都返回<code>true</code></td>
    </tr>
    <tr>
        <td><code>int size()</code></td>
        <td>返回值是<code>select</code>语句查询到的记录条数。若返回值为-1，说明运行的可能是非<code>select</code>语句，或者数据库不支持返回记录条数信息。对于<code>SQLite</code>数据库，即使运行的是<code>select</code>语句，<code>size()</code>的返回值也是-1</td>
    </tr>
    <tr>
        <td><code>int numRowsAffected()</code></td>
        <td>返回<code>sql</code>语句影响的记录条数，如果返回值是-1，表示无法确定影响的记录条数。如果运行的是<code>select</code>语句，该函数的返回值无意义，应该用函数<code>size()</code>确定查询结果的记录条数</td>
    </tr>
    <tr>
    	<td rowspan="9">记录移动</td>
        <td><code>bool first()</code></td>
        <td>定位到第一条记录，<code>isActive()</code>和<code>isSelect()</code>都为<code>true</code>时才有效</td>
    </tr>
    <tr>
        <td><code>bool previous()</code></td>
        <td>定位到上一条记录，<code>isActive()</code>和<code>isSelect()</code>都为<code>true</code>时才有效</td>
    </tr>
    <tr>
        <td><code>bool next()</code></td>
        <td>定位到下一条记录，<code>isActive()</code>和<code>isSelect()</code>都为<code>true</code>时才有效</td>
    </tr>
    <tr>
        <td><code>bool last()</code></td>
        <td>定位到最后一条记录，<code>isActive()</code>和<code>isSelect()</code>都为<code>true</code>时才有效</td>
    </tr>
    <tr>
        <td><code>bool seek()</code></td>
        <td>定位到指定序号的记录，可采用相对位置或绝对位置</td>
    </tr>
    <tr>
        <td><code>int at()</code></td>
        <td>返回当前记录的序号，首记录序号为0.若当前位置不是有效记录，返回值是-1<code>(QSql::BeforeFirstRow)</code>或-2<code>(QSql::AfterLastRow)</code></td>
    </tr>
    <tr>
        <td><code>bool isValid()</code></td>
        <td>当前记录是不是一个有效的记录，若为<code>false</code>就表示不是有效的。在遍历数据集时，一般用这个函数判断是否遍历到了数据集的结束位置</td>
    </tr>
    <tr>
        <td><code>bool isForwardOnly()</code></td>
        <td>数据集是否仅能向前移动，若此函数返回值是<code>true</code>，则只能用<code>next()</code>函数或参数值为正数的<code>seek()</code>函数移动当前记录。默认状态下，该函数返回值为<code>false</code></td>
    </tr>
    <tr>
        <td><code>void setForwardOnly()</code></td>
        <td>设置数据集是否仅能向前移动，必须在运行函数<code>prepare()</code>或<code>exec()</code>之前运行这个函数。若设置为仅能向前移动，可提高内存使用效率和记录移动速度</td>
    </tr>xecaccept
</table>


- `binddValue()`的第三个参数`paramType`默认值是`QSql::In`，表示传递给数据库的值。如果设置为`QSql::Out`，表示该参数是一个返回值，在运行函数`exec()`后，这个参数会被数据库返回的值覆盖。

```c++
#include "mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QWidget>

#include "tdialogdata.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  setCentralWidget(ui->tableView);
  ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
  ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
  ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
  ui->tableView->setAlternatingRowColors(true);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::selectData() {
  queryModel = new QSqlQueryModel(this);
  selectModel = new QItemSelectionModel(queryModel, this);
  ui->tableView->setModel(queryModel);
  ui->tableView->setSelectionModel(selectModel);
  queryModel->setQuery(
      "select empNo,name,gender,birthday,province,department,salary from "
      "employee order by empNo");
  if (queryModel->lastError().isValid()) {
    QMessageBox::information(
        this, "错误",
        "数据表查询错误，错误信息\n" + queryModel->lastError().text());
    return;
  }
  QSqlRecord record = queryModel->record();
  queryModel->setHeaderData(record.indexOf("empNo"), Qt::Horizontal, "工号");
  queryModel->setHeaderData(record.indexOf("name"), Qt::Horizontal, "姓名");
  queryModel->setHeaderData(record.indexOf("gender"), Qt::Horizontal, "性别");
  queryModel->setHeaderData(record.indexOf("birthday"), Qt::Horizontal,
                            "出生日期");
  queryModel->setHeaderData(record.indexOf("province"), Qt::Horizontal, "省份");
  queryModel->setHeaderData(record.indexOf("department"), Qt::Horizontal,
                            "部门");
  queryModel->setHeaderData(record.indexOf("salary"), Qt::Horizontal, "工资");
  ui->actOpen->setEnabled(false);
  ui->actInsert->setEnabled(true);
  ui->actDel->setEnabled(true);
  ui->actEdit->setEnabled(true);
  ui->actHappy->setEnabled(true);
}

void MainWindow::updateRecord(int recNo) {
  QSqlRecord curRec = queryModel->record(recNo);
  int empNo = curRec.value("empNo").toInt();
  QSqlQuery query;
  query.prepare("select * from employee where empNo = :empNo");
  query.bindValue(":empNo", empNo);
  query.exec();
  query.first();
  if (!query.isValid()) {
    return;
  }
  curRec = query.record();
  TDialogData *dialogData = new TDialogData(this);
  Qt::WindowFlags flags = dialogData->windowFlags();
  // 对话框固定大小
  dialogData->setWindowFlags(flags | Qt::MSWindowsFixedSizeDialogHint);
  dialogData->setUpdateRecord(curRec);
  int ret = dialogData->exec();
  if (ret == QDialog::Accepted) {
    QSqlRecord recData = dialogData->getRecordData();
    query.prepare(
        "update employee set name = :name,gender=:gender,birthday=:birthday,"
        "province=:province,department=:department,salary=:salary,memo=:memo,"
        "photo=:photo where empNo=:empNo");
    query.bindValue(":name", recData.value("name"));
    query.bindValue(":gender", recData.value("gender"));
    query.bindValue(":birthday", recData.value("birthday"));
    query.bindValue(":province", recData.value("province"));
    query.bindValue(":department", recData.value("department"));
    query.bindValue(":salary", recData.value("salary"));
    query.bindValue(":memo", recData.value("memo"));
    query.bindValue(":photo", recData.value("photo"));
    query.bindValue(":empNo", empNo);
    if (query.exec()) {
      QString sqlStr = queryModel->query().executedQuery();
      queryModel->setQuery(sqlStr);
    } else {
      QMessageBox::critical(this, "错误",
                            "记录更新错误\n" + query.lastError().text());
    }
  }
  delete dialogData;
}

void MainWindow::on_actOpen_triggered() {
  QString filename = QFileDialog::getOpenFileName(
      this, "选择文件", QDir::currentPath(), "SQLite数据库(*.db)");
  if (filename.isEmpty()) {
    return;
  }
  db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName(filename);
  if (db.open()) {
    selectData();
  } else {
    QMessageBox::warning(this, "错误", "打开数据库失败");
  }
}

void MainWindow::on_actEdit_triggered() {
  int curRecNo = selectModel->currentIndex().row();
  updateRecord(curRecNo);
}

void MainWindow::on_tableView_doubleClicked(const QModelIndex &index) {
  int curRecNo = index.row();
  updateRecord(curRecNo);
}

void MainWindow::on_actInsert_triggered() {
  QSqlQuery query;
  query.exec("select * from employee where empNo = -1");
  QSqlRecord curRec = query.record();
  curRec.setValue("empNo", queryModel->rowCount() + 2000);
  TDialogData *dialogData = new TDialogData(this);
  Qt::WindowFlags flags = dialogData->windowFlags();
  // 对话框固定大小
  dialogData->setWindowFlags(flags | Qt::MSWindowsFixedSizeDialogHint);
  dialogData->setInsertRecord(curRec);
  int ret = dialogData->exec();
  if (ret == QDialog::Accepted) {
    QSqlRecord recData = dialogData->getRecordData();
    query.prepare(
        "insert into employee (empNo, name,gender,birthday,"
        "province,department,salary,memo,"
        "photo)values(:empNo,:name,:gender,:birthday,:province,:department,:"
        "salary,:memo,:photo)");
    query.bindValue(":name", recData.value("name"));
    query.bindValue(":gender", recData.value("gender"));
    query.bindValue(":birthday", recData.value("birthday"));
    query.bindValue(":province", recData.value("province"));
    query.bindValue(":department", recData.value("department"));
    query.bindValue(":salary", recData.value("salary"));
    query.bindValue(":memo", recData.value("memo"));
    query.bindValue(":photo", recData.value("photo"));
    query.bindValue(":empNo", recData.value("empNo"));
    if (query.exec()) {
      QString sqlStr = queryModel->query().executedQuery();
      queryModel->setQuery(sqlStr);
    } else {
      QMessageBox::critical(this, "错误",
                            "插入记录错误\n" + query.lastError().text());
    }
  }
  delete dialogData;
}

void MainWindow::on_actDel_triggered() {
  int curRecNo = selectModel->currentIndex().row();
  QSqlRecord curRec = queryModel->record(curRecNo);
  if (curRec.isEmpty()) {
    return;
  }
  int empNo = curRec.value("empNo").toInt();
  QSqlQuery query;
  query.prepare("delete from employee where empNo = :empNo");
  query.bindValue(":empNo", empNo);
  if (query.exec()) {
    QString sqlStr = queryModel->query().executedQuery();
    queryModel->setQuery(sqlStr);
  } else {
    QMessageBox::critical(this, "错误",
                          "删除记录错误\n" + query.lastError().text());
  }
}

void MainWindow::on_actHappy_triggered() {
  QSqlQuery queryUpdate;
  queryUpdate.prepare("update employee set salary=:salary where empNo=:empNo");
  QSqlQuery queryEmpList;
  queryEmpList.setForwardOnly(true);
  queryEmpList.exec("select empNo,salary from employee order by empNo");
  while (queryEmpList.next()) {
    int empNo = queryEmpList.value("empNo").toInt();
    float salary = 1000 + queryEmpList.value("salary").toFloat();
    queryUpdate.bindValue(":empNo", empNo);
    queryUpdate.bindValue(":salary", salary);
    queryUpdate.exec();
  }
  QString sqlStr = queryModel->query().executedQuery();
  queryModel->setQuery(sqlStr);
  QMessageBox::information(this, "提示", "涨工资计算完毕");
}
```

#### 5、`QSqlRelationalTableModel`的使用

- 该类是`QSqlTableModel`的子类，可以作为关系数据库的模型类。
- 设置外键关系：
  - `void QSqlRelationalTableModel::setRelation(int column, const QSqlRelation &relation)`其中`column`是外键字段的字段序号，`relation`表示外键字段关联的编码表、编码字段、编码含义字段等信息。
- `QSqlRelation`类的构造函数原型：
  - `QSqlRelation(const QString &tableName, const QString &indexColumn, const QString &displayColumn)`
- 其它函数
  - `void QSqlRelationalTableModel::setJoinMode(QSqlRelationalTableModel::JoinMode joinMode)`
  - 有两种枚举值：
    - `QSqlRelationalTableModel::InnerJoin`：不显示从表中的孤立记录，这是默认值
    - `QSqlRelationalTableModel::LeftJoin`：显示从表中的孤立记录
- `relationModel(int column)`用于返回某个外键字段关联的编码表的`QSqlTableModel`类型数据模型。

```c++
#include "mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>

#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  setCentralWidget(ui->tableView);
  ui->tableView->setSelectionBehavior(QAbstractItemView::SelectItems);
  ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
  ui->tableView->setAlternatingRowColors(true);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::do_currentChanged(const QModelIndex &current,
                                   const QModelIndex &prevoius) {
  Q_UNUSED(current);
  Q_UNUSED(prevoius);
  ui->actSave->setEnabled(tableModel->isDirty());
  ui->actCancel->setEnabled(tableModel->isDirty());
}

void MainWindow::openTable() {
  tableModel = new QSqlRelationalTableModel(this, db);
  tableModel->setJoinMode(QSqlRelationalTableModel::LeftJoin);
  tableModel->setTable("studInfo");
  tableModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
  tableModel->setSort(tableModel->fieldIndex("studId"), Qt::AscendingOrder);
  selectModel = new QItemSelectionModel(tableModel, this);
  connect(selectModel, &QItemSelectionModel::currentChanged, this,
          &MainWindow::do_currentChanged);
  ui->tableView->setModel(tableModel);
  ui->tableView->setSelectionModel(selectModel);
  tableModel->setHeaderData(tableModel->fieldIndex("studId"), Qt::Horizontal,
                            "学号");
  tableModel->setHeaderData(tableModel->fieldIndex("name"), Qt::Horizontal,
                            "姓名");
  tableModel->setHeaderData(tableModel->fieldIndex("gender"), Qt::Horizontal,
                            "性别");
  tableModel->setHeaderData(tableModel->fieldIndex("departId"), Qt::Horizontal,
                            "学院");
  tableModel->setHeaderData(tableModel->fieldIndex("majorId"), Qt::Horizontal,
                            "专业");
  // 设置外键关系
  tableModel->setRelation(
      tableModel->fieldIndex("departId"),
      QSqlRelation("departments", "departId", "department"));
  tableModel->setRelation(tableModel->fieldIndex("majorId"),
                          QSqlRelation("majors", "majorId", "major"));
  // 为外键字段设置默认代理组件
  ui->tableView->setItemDelegate(new QSqlRelationalDelegate(ui->tableView));
  tableModel->select();
  ui->actOpen->setEnabled(false);
  ui->actAdd->setEnabled(true);
  ui->actInsert->setEnabled(true);
  ui->actDel->setEnabled(true);
  ui->actField->setEnabled(true);
  ui->actCancel->setEnabled(true);
}

void MainWindow::on_actOpen_triggered() {
  QString filename = QFileDialog::getOpenFileName(
      this, "选择文件", QDir::currentPath(), "SQLite数据库(*.db)");
  if (filename.isEmpty()) {
    return;
  }
  db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName(filename);
  if (db.open()) {
    openTable();
  } else {
    QMessageBox::warning(this, "错误", "打开数据库失败");
  }
}

void MainWindow::on_actField_triggered() {
  QSqlRecord emptyRec = tableModel->record();
  QString str;
  for (int i = 0; i < emptyRec.count(); ++i) {
    str += emptyRec.fieldName(i) + "\n";
  }
  QMessageBox::information(this, "所有字段名", str);
}

void MainWindow::on_actAdd_triggered() {
  // 添加一条记录
  QSqlRecord rec = tableModel->record();
  rec.setValue(tableModel->fieldIndex("studId"), 2000 + tableModel->rowCount());
  rec.setValue(tableModel->fieldIndex("gender"), "男");
  tableModel->insertRecord(tableModel->rowCount(), rec);

  selectModel->clearSelection();
  QModelIndex curIndex = tableModel->index(tableModel->rowCount() - 1, 1);
  selectModel->setCurrentIndex(curIndex, QItemSelectionModel::Select);
}

void MainWindow::on_actInsert_triggered() {
  QModelIndex curIndex = ui->tableView->currentIndex();
  QSqlRecord rec = tableModel->record();
  rec.setValue(tableModel->fieldIndex("studId"), 2000 + tableModel->rowCount());
  rec.setValue(tableModel->fieldIndex("gender"), "男");
  tableModel->insertRecord(curIndex.row(), rec);
  selectModel->clearSelection();
  selectModel->setCurrentIndex(curIndex, QItemSelectionModel::Select);
}

void MainWindow::on_actDel_triggered() {
  QModelIndex curIndex = selectModel->currentIndex();
  tableModel->removeRow(curIndex.row());
}

void MainWindow::on_actSave_triggered() {
  bool res = tableModel->submitAll();
  if (res) {
    ui->actSave->setEnabled(false);
    ui->actCancel->setEnabled(false);
    tableModel->select();
  } else {
    QMessageBox::information(
        this, "消息",
        "数据保存错误，错误信息\n" + tableModel->lastError().text());
  }
}

void MainWindow::on_actCancel_triggered() {
  tableModel->revertAll();
  ui->actSave->setEnabled(false);
  ui->actCancel->setEnabled(false);
  tableModel->select();
}
```

### 十、绘图

- `Qt`的二维绘图基本功能是使用`QPainter`在绘图设备上绘图，绘图设备包括`QWidget, QPixmap, QPrinter`等。
- 界面组件的显示效果实际上是`QPainter`在`QWidget`上实现的。
- `QPainter`可以在`QWidget`上绘制出自定义的组件形状和实现特定的显示效果，这也是设计自定义界面组件的基础。
- `Qt`嗨提供了图形/视图`(graphics/view)`架构。通过使用`QGraphicsView, QGraphicsScene`和各种`QGraphicsItem`类，可以在一个场景中绘制大量的图形项，且每个图形项是可选择、可交互操作的。

#### 1、`QPainter`绘图

##### 绘图设备

- 绘图设备的基类是`QPainterDevice`，它没有父类。`QPaintDevice`及其子类的继承关系如下：
  - `QPainterDevice`
    - `QWidget`
    - `QImage`
    - `QPixmap`
      - `QBitmap`
    - `QPicture`
    - `QSvgGenerator`
    - `QOpenGLPaintDevice`
    - `QPagedPaintDevice`（`QT += printsupport`）
      - `QPrinter`
      - `QPdfWriter`
- `QImage, QPixmap, QBitmap`和`QPicture`是四个处理图片的类。
- `QImage`是与硬件无关的表示图片的类，是为设备输入输出而优化设计的类，它可以直接进行图片像素数据的访问和操作。
- `QPixmap`是为在屏幕上显示图片而优化设计的类。`QBitmap`是其子类，用于表示1位色深的单色位图。
- `QPicture`是用于记录和回放`QPainter`指令的类
- `QSvgGenerator`是用于创建`SVG`图形的绘图设备类。`Qt`还提供了显示`SVG`图片文件的组件类`QSvgWidget`
- `QOpenGLPaintDevice`是能用`OpenGL`渲染`QPainter`绘图指令的绘图设备类，他需要系统支持2.0以上版本的`OpenGL`或`OpenGL ES`
- `QPagedPaintDevice`是支持多个页面的绘图设备类，通常用于生成打印输出或`PDF`文件。`QPrinter`是用于打印输出的类，打印输出实际上就是在`QPainter`设备上绘图，可以生成一系列用于打印输出的页面。`QPdfWriter`是用于生成`PDF`文件的绘图设备类，使用`QPrinter`在`QPdfWriter`设备上绘图就可以将绘图内容直接保存为`PDF`文件。

##### `QWidget`的绘图事件和绘图区

- `QWidget`类有一个事件处理函数`paintEvent()`，在界面组件需要重绘时，系统会自动运行这个函数。要在界面上绘图，我们需要再此事件处理函数里创建一个`QPainter`对象来获取绘图设备的接口，然后用这个`QPainter`对象在绘图设备上绘图。

  ```c++
  void Widget::paintEvent(QPaintEvent *event){
  	QPainter painter(this);
  	// 创建与绘图设备关联的QPainter对象
  	// 使用painte在设备的界面上绘图
  }
  ```

- 绘图区就是其窗口的内部区域。

##### `QPainter`绘图的特性控制

- `QPen`类：用于控制线条的颜色、宽度、线型等
- `QBrush`类：用于设置一个区域的填充特性，包括填充颜色、填充样式、渐变特性等，还可以采用图片进行材质填充。
- `QFont`类：用于设置文字的字体、样式、大小等属性。

```c++
#include "widget.h"

#include <QPaintEvent>
#include <QPainter>

#include "ui_widget.h"
Widget::Widget(QWidget *parent) : QWidget(parent), ui(new Ui::Widget) {
  ui->setupUi(this);
  // 设置窗口为白色背景
  setPalette(QPalette(Qt::white));
  setAutoFillBackground(true);
  setFixedSize(400, 300);
}

Widget::~Widget() { delete ui; }

void Widget::paintEvent(QPaintEvent *event) {
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setRenderHint(QPainter::TextAntialiasing);
  int w = this->width();
  int h = this->height();
  // 中间区域矩形
  QRect rect(w / 4, h / 4, w / 2, h / 2);
  // 设置画笔
  QPen pen;
  // 线宽
  pen.setWidth(3);
  // 线条颜色
  pen.setColor(Qt::red);
  // 线条样式
  pen.setStyle(Qt::SolidLine);
  // 线条端点样式
  pen.setCapStyle(Qt::FlatCap);
  // 线条的连接样式
  pen.setJoinStyle(Qt::BevelJoin);
  painter.setPen(pen);
  // 设置画刷
  QBrush brush;
  // 画刷颜色
  brush.setColor(Qt::yellow);
  // 画刷填充样式
  brush.setStyle(Qt::SolidPattern);
  painter.setBrush(brush);
  // 绘图
  painter.drawRect(rect);
  event->accept();
}
```

##### `QPen`的主要功能

- 通常一个设置函数对应一个读取函数，以下是常用函数（省略参数中的`const`）：

| 函数原型                                    | 功能                     |
| ------------------------------------------- | ------------------------ |
| `void setColor(QColor &color)`              | 设置画笔颜色，即线条颜色 |
| `void setWidth(int width)`                  | 设置线条宽度，单位是像素 |
| `void setStyle(Qt::PenStyle style)`         | 设置线条样式             |
| `void setCapStyle(Qt::PenCapStyle style)`   | 设置线条端点样式         |
| `void setJoinStyle(Qt::PenJoinStyle style)` | 设置线条连接样式         |

- 线条样式：
  - `Qt::SolidLine`实线
  - `Qt::DashLine`虚线
  - `Qt::DotLine`点划线
  - `Qt::NoPen`不绘制线条
- 线条端点样式，只有当线条比较粗时，线条端点的效果才能显现出来
  - `Qt::FlatCap`方形的线条端，不覆盖线条的端点
  - `Qt::SquareCap`方形的线条端，覆盖线条的端点并延伸1/2线宽的长度
  - `Qt::RoundCap`圆角的线条端
- 线条连接样式
  - `Qt::BevelJoin`
  - `Qt::RoundJoin`
  - `Qt::MiterJoin`

##### `QBrush`的主要功能

| 函数原型                              | 功能                                                         |
| ------------------------------------- | ------------------------------------------------------------ |
| `void setColor(QColor &color)`        | 设置画刷颜色，实体填充时即填充颜色                           |
| `void setStyle(Qt::BrushStyle style)` | 设置画刷填充样式，参数为枚举类型`Qt::BrushStyle`             |
| `void setTexture(QPixmap &pixmap)`    | 设置一个`QPixmap`类型的图片作为画刷的图片，画刷样式自动设置为`Qt::TexturePattern` |
| `void setTextureImage(QImage &image)` | 设置一个`QImage`类型的图片作为画刷的图片，画刷样式自动设置为`Qt::TexturePattern` |

- 画刷填充样式：
  - `Qt::NoBrush`不填充
  - `Qt::SolidPattern`单一颜色填充
  - `Qt::HorPattern`水平线填充
  - `Qt::VerPattern`垂直线填充
  - `Qt::LinearGradientPattern`线性渐变填充，需要使用`QLinearGradient`类对象作为画刷
  - `Qt::RadialGradientPattern`辐射渐变填充，需要使用`QRadialGradient`类对象作为画刷
  - `Qt::ConicalGradientPattern`圆锥形渐变填充，需要使用`QConicalGradient`类对象作为画刷
  - `Qt::TexturePattern`材质填充，需要指定`texture`或`textureImage`图片

###### 渐变填充

- 使用渐变填充需要将渐变类的对象作为`Painter`的画刷，有3个实现渐变填充的类。他们都继承自`QGradient`类
- `QLinearGradient`线性渐变。指定一个起点及其颜色，一个终点及其颜色，还可以指定中间某个点的颜色，起点颜色至终点的颜色会按照线性插值计算确定，得到线性渐变的填充颜色。
- `QRadialGradient`：辐射渐变。有简单辐射渐变和扩展辐射渐变两种方式，其中简单辐射渐变是在一个圆内的一个焦点和一个端点之间生成渐变色，扩展辐射渐变是在一个焦点圆和一个中心圆之间生成渐变色。
- `QConicalGradient`圆锥形渐变。围绕一个中心点逆时针生成渐变色。

###### 延展填充

- 如果填充区域超出定义区域，`QGradient`的函数`setSpread()`设置的参数会影响延展区域的填充效果（对圆锥形渐变不起作用）。
  - `void QGradient::setSpread(QGradient::Spread method)`
- 枚举类型：
  - `QGradient::PadSpread`用结束点的颜色填充外部区域，这是默认的方式
  - `QGradient::RepeatSpread`重复使用渐变方式填充外部区域
  - `QGradient::ReflectSpread`反射式重复使用渐变方式填充外部区域。

##### `QPainter`绘制基本图形

###### 基本图形

- 包括点、直线、椭圆、矩形等。

- 以下常用函数假设已经获取绘制窗口的`painter`，窗口宽度`w`和高度`h`

- `drawPoint()`绘制一个点

  ```c++
  painter.drawPoint(QPoint(w/2, h/2));
  ```

- `drawPoints()`绘制一批点

  ```c++
  QPoint points[] = {QPoint(w/2, h/2), QPoint(w/3, h/3), QPoint(w/4, h/4)};
  painter.drawPoints(points, 3);
  ```

- `drawLine()`绘制直线

  ```c++
  QLine line(w/4, h/4, w/2, h/2);
  painter.drawLine(line);
  ```

- `drawLines()`绘制一批直线

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  QList<QLine> lines;
  lines.append(QLine(rect.topLeft(), rect.bottomRight()));
  lines.append(QLine(rect.topRight(), rect.bottomLeft()));
  painter.drawLines(lines);
  ```

- `drawArc()`绘制弧线

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  int startAngle = 90 * 16; // 起始90°
  int spanAngle = 90 * 16; // 旋转90°
  painter.drawArc(rect, startAngle, spanAngle);
  ```

- `drawChord()`绘制一段弦

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  int startAngle = 90 * 16; // 起始90°
  int spanAngle = 90 * 16; // 旋转90°
  painter.drawChord(rect, startAngle, spanAngle);
  ```

- `drawPie()`绘制扇形

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  int startAngle = 40 * 16; // 起始40°
  int spanAngle = 120 * 16; // 旋转120°
  painter.drawPie(rect, startAngle, spanAngle);
  ```

- `drawConvexPolygon()`根据给定的点绘制凸多边形

  ```c++
  QPoint points[4] = {
  	QPoint(w/2, h/2),
      QPoint(w/3, h/3),
      QPoint(w/4, h/4),
      QPoint(w/5, h/5)
  };
  painter.drawConvexPolygon(points, 4);
  ```

- `drawPolygon()`绘制多边形，最后一个点会和第一个点重合

  ```c++
  QPoint points[4] = {
  	QPoint(w/2, h/2),
      QPoint(w/3, h/3),
      QPoint(w/4, h/4),
      QPoint(w/5, h/5)
  };
  painter.drawPolygon(points, 4);
  ```

- `drawPolyline()`绘制多点连接的线，最后一个点不会和第一个点连接

  ```c++
  QPoint points[4] = {
  	QPoint(w/2, h/2),
      QPoint(w/3, h/3),
      QPoint(w/4, h/4),
      QPoint(w/5, h/5)
  };
  painter.drawPolyline(points, 4);
  ```

- `drawImage()`将`QImage`对象存储的图片绘制在指定的矩形区域内。

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  QImage = image(":images/qt.jpg");
  painter.drawImage(rect, image);
  ```

- `drawPixmap()`将`QPixmap`对象存储的图片绘制在指定的矩形区域内

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  QPixmap = image(":images/qt.jpg");
  painter.drawPixmap(rect, image);
  ```

- `drawText()`绘制文本，只能绘制单行文字，字体属性由`QPainter::font()`决定

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  QFont font;
  font.setPointSize(30);
  font.setBold(true);
  painter.setFont(font);
  painter.drawText(rect, "Hello, Qt");
  ```

- `drawEllipse()`绘制椭圆

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  painter.drawEllipse(rect);
  ```

- `drawRect()`绘制矩形

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  painter.drawRect(rect);
  ```

- `drawRoundedRect()`绘制圆角矩形

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  painter.drawRoundedRect(rect);
  ```

- `fillRect()`填充矩形，无边框线

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  painter.fillRect(rect);
  ```

- `eraseRect()`擦除某个矩形区域，等效于用背景色填充该区域

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  painter.eraseRect(rect);
  ```

- `drawPath()`绘制由`QPainterPath`对象定义的路径

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  QPainterPath path;
  path.addEllipse(rect);
  path.addRect(rect);
  painter.drawPath(path);
  ```

- `fillPath()`填充某个`QPainterPath`对象定义的绘图路径，但是不显示轮廓线

  ```c++
  QRect rect(w/4, h/4, w/2, h/2);
  QPainterPath path;
  path.addEllipse(rect);
  path.addRect(rect);
  painter.fillPath(path, Qt::red);
  ```


###### `QPainterPath`的说明

- 该类用于记录绘图操作序列。一个`PainterPath`由许多基本的图形组成，如绘图点移动、画线、画圆、画矩形等，一个闭合的`PainterPath`是起点和终点连接起来的绘图路径。使用`QPainterPath`的优点是绘制某些复杂图形时只需创建一个`PainterPath`，然后调用`QPainter::drawPath()`就可以重复使用这个`PainterPath`来绘图。

#### 2、坐标系统和坐标转换

- `QPainter`在绘图设备上进行绘图时默认使用设备的物理坐标。
- 窗口坐标时逻辑坐标，使用`QPainter`在逻辑坐标系中绘图，可以自动适应绘图区大小的变化。

##### 坐标变换

<table>
    <tr>
    	<th>分组</th>
        <th>函数原型</th>
        <th>功能</th>
    </tr>
	<tr>
		<td rowspan="4">坐标变换</td>
        <td><code>void translate(qreal dx, qreal dy)</code></td>
        <td>坐标系平移一定的偏移量，坐标原点平移到新的位置</td>
	</tr>
	<tr>
        <td><code>void rotate(qreal angle)</code></td>
        <td>坐标系旋转一定的角度，角度单位是度</td>
	</tr>
	<tr>
        <td><code>void scale(qreal sx, qreal sy)</code></td>
        <td>坐标系缩放</td>
	</tr>
	<tr>
        <td><code>void shear(qreal sh, qreal sv)</code></td>
        <td>坐标系做扭转</td>
	</tr>
    <tr>
		<td rowspan="3">状态保存与恢复</td>
        <td><code>void save()</code></td>
        <td>保存当前坐标状态，就是将当前状态压入栈</td>
	</tr>
	<tr>
        <td><code>void restore()</code></td>
        <td>恢复上一次保存的坐标状态，就是从栈中弹出上次的坐标状态</td>
	</tr>
	<tr>
        <td><code>void resetTransform()</code></td>
        <td>复位所有的坐标变换</td>
	</tr>
</table>

```c++
QPainter painter(this);
  // 启动抗锯齿
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setRenderHint(QPainter::TextAntialiasing);
  qreal r = 100;
  const qreal pi = M_PI;
  qreal deg = pi * 72 / 180;
  QPoint points[5] = {QPoint(r, 0), QPoint(r * qCos(deg), -r * qSin(deg)),
                      QPoint(r * qCos(2 * deg), -r * qSin(2 * deg)),
                      QPoint(r * qCos(3 * deg), -r * qSin(3 * deg)),
                      QPoint(r * qCos(4 * deg), -r * qSin(4 * deg))};
  // 设置字体
  QFont font;
  font.setPointSize(14);
  painter.setFont(font);
  // 设置画笔
  QPen penLine;
  penLine.setWidth(2);
  penLine.setColor(Qt::blue);
  penLine.setStyle(Qt::SolidLine);
  penLine.setCapStyle(Qt::FlatCap);
  penLine.setJoinStyle(Qt::BevelJoin);
  painter.setPen(penLine);
  // 设置画刷
  QBrush brush;
  brush.setColor(Qt::yellow);
  brush.setStyle(Qt::SolidPattern);
  painter.setBrush(brush);
  // 设计五角星的PainterPath, 以便重复使用
  QPainterPath starPath;
  starPath.moveTo(points[0]);
  starPath.lineTo(points[2]);
  starPath.lineTo(points[4]);
  starPath.lineTo(points[1]);
  starPath.lineTo(points[3]);
  // 闭合路径, 最后一个点与第一个点相连
  starPath.closeSubpath();
  starPath.addText(points[0], font, "1");
  starPath.addText(points[1], font, "2");
  starPath.addText(points[2], font, "3");
  starPath.addText(points[3], font, "4");
  starPath.addText(points[4], font, "5");
  // 绘图, 第一个五角星
  painter.save();
  painter.translate(100, 120);
  painter.drawPath(starPath);
  painter.drawText(0, 0, "S1");
  // 恢复坐标状态
  painter.restore();
  // 第二个五角星
  painter.translate(300, 120);
  painter.scale(0.8, 0.8);
  painter.rotate(90);
  painter.drawPath(starPath);
  painter.drawText(0, 0, "S2");
  // 第三个五角星
  painter.resetTransform();
  painter.translate(500, 120);
  painter.rotate(-145);
  painter.drawPath(starPath);
  painter.drawText(0, 0, "S3");
  event->accept();
```

##### 视口和窗口

- 绘图设备的物理坐标系是基本的坐标系。物理坐标系也称为视口坐标系，逻辑坐标系也称为窗口坐标系，通过内部的坐标变换矩阵，`QPainter`能自动将逻辑坐标变换为绘图设备的物理坐标。
- 视口是指绘图设备的任意一个矩形区域，它使用物理坐标系。我们可以只选取物理坐标系中的一个矩形区域来绘图，默认情况下，视口等于绘图设备的整个矩形区域。但是窗口是用逻辑坐标系定义的，窗口可以直接定义矩形区域的逻辑坐标范围。
- `QPainter`的函数`setViewport()`用于定义视口：
  - `void QPainter::setViewport(const QRect &rectangle)`
  - `void QPainter::setViewport(int x, int y, int width, int height)`其中，`(x, y)`是视口左上角在物理坐标系中的坐标。
- `QPainter`的函数`setWindow()`用于定义窗口：
  - `void QPainter::setWindow(const QRect &rectangle)`
  - `void QPainter::setWindow(int x, int y, int width, int height)`其中, `(x, y)`是窗口左上角的坐标。

###### 视口和窗口使用示例

- 使用窗口坐标系的优点是：当实际绘图设备大小变化时，绘制的图形会自动相应改变大小。

```c++
QLinearGradient  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing);
  int w = width();
  int h = height();
  int side = qMin(w, h);
  // 视口矩形区域
  QRect rect((w - side) / 2, (h - side) / 2, side, side);
  // 绘制视口边界
  painter.drawRect(rect);
  // 设置视口
  painter.setViewport(rect);
  // 设置窗口坐标系
  painter.setWindow(-100, -100, 200, 200);
  // 设置画笔
  QPen pen;
  pen.setWidth(1);
  pen.setColor(Qt::red);
  pen.setStyle(Qt::SolidLine);
  pen.setCapStyle(Qt::FlatCap);
  pen.setJoinStyle(Qt::BevelJoin);
  painter.setPen(pen);
  for (int i = 0; i < 36; ++i) {
    painter.drawEllipse(QPoint(50, 0), 50, 50);
    painter.rotate(10);
  }
  event->accept();
```

###### 绘图叠加的效果

```c++
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing);
  int w = width();
  int h = height();
  int side = qMin(w, h);
  // 视口矩形区域
  QRect rect((w - side) / 2, (h - side) / 2, side, side);
  // 绘制视口边界
  painter.drawRect(rect);
  // 设置视口
  painter.setViewport(rect);
  // 设置窗口坐标系
  painter.setWindow(-100, -100, 200, 200);
  // 设置画笔
  QPen pen;
  pen.setWidth(1);
  pen.setColor(Qt::red);
  pen.setStyle(Qt::SolidLine);
  pen.setCapStyle(Qt::FlatCap);
  pen.setJoinStyle(Qt::BevelJoin);
  painter.setPen(pen);
  // 线性渐变, 从左到右
  QLinearGradient linearGrad(0, 0, 100, 0);
  linearGrad.setColorAt(0, Qt::yellow);
  linearGrad.setColorAt(1, Qt::green);
  linearGrad.setSpread(QGradient::PadSpread);
  painter.setBrush(linearGrad);
  // 设置组合模式
  // painter.setCompositionMode(QPainter::CompositionMode_Difference);
  painter.setCompositionMode(QPainter::RasterOp_NotSourceXorDestination);
  // painter.setCompositionMode(QPainter::CompositionMode_Exclusion);
  for (int i = 0; i < 36; ++i) {
    painter.drawEllipse(QPoint(50, 0), 50, 50);
    painter.rotate(10);
  }
  event->accept();
```

#### 3、图形/视图架构

- 采用`QPainter`绘图时需要在绘图设备的事件处理函数`paintEvent()`里编写代码来实现绘图。用这种方法绘制的是位图。
- `Qt`为绘制复杂的可交互的图形提供了图形/视图架构，这是一种基于图形项的模型/视图结构。使用该架构可以绘制复杂的由成千上万个基本图形组成的图形，并且每个图形组件是可选择、可拖放和可修改的，类似于矢量绘图软件的绘图功能。

##### 场景、视图与图形项

###### 场景

- `QGraphicsScene`类提供绘图场景，他的父类是`QObject`。场景不是界面组件，它是不可见的。
- 场景是一个抽象的管理图形项的容器，我们可以向场景添加图形项，可以获取场景中的某个图形项。
- 场景主要具有如下一些功能：
  - 提供管理大量图形项的快速入口
  - 将事件传播给每个图形项
  - 管理每个图形项的状态，例如选择状态、焦点状态等
  - 管理未经变换的渲染功能，主要用于打印。
- 除了图形项，场景还有背景层和前景层。函数`setBackgroundBrush()`用于设置`QBrush`对象为背景层的画刷，函数`setForegroundBrush()`用于设置`QBrush`对象为前景层的画刷。
- 如果从`QGraphicsScene`类继承，可以重新实现受保护的函数`drawBackground()`和`drawForeground()`来自定义背景和前景，以实现一些特殊效果。

###### 视图

- `QGraphicsView`是图形/视图架构中的视图组件。他的间接父类是`QWidget`，所以他是一个界面组件，用于显示场景中的内容。可以为一个场景设置多个视图，用于对同一个场景提供不同的显示界面。
- 默认情况下，当视图大于场景时，场景在视图的中央位置显示，也可以通过设置视图的`Alignment`属性来控制场景在视图中的显示位置。
- 当视图小于场景时，只能显示场景的部分内容，但是会提供滚卷条实现在整个场景内移动。
- 视图接收键盘和鼠标输入并转换为场景的事件，进行坐标交换后这些事件被传送给可视场景。

###### 图形项

- 图形项就是一些基本图形组件，所有图形项类都是从`QGraphicsItem`继承而来的，而该类没有父类。图形项相当于模型中的数据，一个图形项存储了绘制这个图形项的各种参数，场景管理所有图形项，而视图组件则负责绘制这些图形项。
- `Qt`提供了一些基本的图形项类，例如`QGraphicsEllipseItem`用于绘制椭圆，`QGraphicsRectItem`用于绘制矩形，`QGraphicsTextItem`用于绘制文字。
- `QGraphicsItem`支持如下一些操作：
  - 鼠标事件响应
  - 键盘输入，以及按键事件
  - 拖放操作
  - 支持组合，可以是父子图形项关系组合，也可以通过`QGraphicsItemGroup`类进行组合。
- 一个图形项还可以包含子图形项，图形项还支持碰撞检测，即检测是否与其他图形项碰撞。
- 图形项不是界面组件类，但是它能够对事件进行处理，这是因为视图会把接收到的事件传送给场景，场景再把事件传播给对应的图形项。
- 场景是图形项的容器，可以在场景中添加很多图形项，每个图形项就是一个对象。视图是显示场景的全部或部分区域的视口，也就是显示场景中的图形项，这些图形项可以被选择和拖动。

##### 图形/视图架构的坐标系

- 该架构有三个有效的坐标系：场景坐标系、视图坐标系、图形项坐标系。
- 绘图的时候，场景坐标系等价于`QPainter`的逻辑坐标系，一般以场景的中心为原点。
- 视图坐标系与绘图设备坐标系相同，是物理坐标系，默认以左上角为原点。
- 图形项坐标系是局部逻辑坐标系，一般以图形项的中心为原点。

###### 图形项坐标系

- 图形项使用自己的局部坐标系，通常以其中心为原点`(0, 0)`，这也是各种坐标变换的中心。
- 图形项的鼠标事件的坐标是用局部坐标系表示的，若创建自定义图形项类，绘制图形项时只需考虑其局部坐标系，`QGraphicsScene`和`QGraphicsView`会自动进行坐标变换。
- 图形项的位置是指其中心在父对象坐标系中的坐标。对于没有父图形项的图形项，其父对象就是场景，图形项的位置就是指在场景中的坐标。
- 如果一个图形项是其他图形项的父图形项，父图形项进行坐标变换时，子图形项也进行同样的坐标变换。
- `QGraphicsItem`的大多数函数都在其局部坐标系上操作，`QGraphicsItem::pos()`是仅有的几个例外之一，它返回的是图形项在父图形项坐标系中的坐标，如果是顶层图形项，则返回的是在场景中的坐标。

###### 视图坐标系

- 视图坐标系就是视图组件的物理坐标系，单位是像素。视图坐标系只与视图组件或视口有关，而与观察的场景无关。`QGraphicsView`视口的左上角坐标总是`(0, 0)`
- 所有的鼠标事件、拖放事件的坐标首先是由视图坐标系定义的，然后用户需要将这些坐标映射为场景坐标，以便和图形项交互。

###### 场景坐标系

- 场景坐标系定义了所有图形项的基础坐标，场景坐标系描述了每个顶层图形项的位置。创建场景时可以定义场景矩形区域的坐标范围。
- 每个图形项在场景里有一个位置坐标，由函数`QGraphicsItem::scenePos()`给出，还有一个图形项边界矩形，由函数`QGraphicsItem:;sceneBoundingRect()`给出。边界矩形可以使场景知道场景中的哪个区域发生了变化。
- 场景发生变化时会发射`QGraphicsScene::changed()`信号，参数是场景内的矩形区域列表，表示发生变化的矩形区域。

###### 坐标映射

- 在场景中操作图形项时，进行场景到图形项、图形项到图形项或视图到场景的坐标变换是比较有用的。
- 通过函数`QGraphicsView::mapToScene()`可以将视图坐标映射为场景坐标
- 使用`QGraphicsScene::itemAt()`函数可以获取场景中鼠标光标处的图形项。

##### 图形/视图架构相关的类

###### `QGraphicsView`类的主要接口函数

- 它是用于观察场景的物理窗口，当场景小于视图时，整个场景在视图中可见。
- 主要接口函数（一般设置函数有一个对应的读取函数，仅列出函数的返回值类型，省略了输入参数）：

<table>
    <tr>
    	<th>分组</th>
        <th>函数</th>
    	<th>功能</th>
    </tr>
    <tr>
    	<td rowspan="2">场景</td>
        <td><code>void setScene()</code></td>
        <td>设置关联显示的场景</td>
    </tr>
    <tr>
        <td><code>void setSceneRect()</code></td>
        <td>设置场景在视图中可视部分的矩形区域</td>
    </tr>
    <tr>
    	<td rowspan="4">外观</td>
        <td><code>void setAlignment()</code></td>
        <td>设置场景在视图中的对齐方式，默认是上下都居中</td>
    </tr>
    <tr>
        <td><code>void setBackgroundBrush()</code></td>
        <td>设置关联场景的背景画刷</td>
    </tr>
    <tr>
        <td><code>void setForegroundBrush()</code></td>
        <td>设置关联场景的前景画刷</td>
    </tr>
    <tr>
        <td><code>void setRenderHints()</code></td>
        <td>设置视图的绘图选项</td>
    </tr>
    <tr>
    	<td rowspan="5">交互</td>
        <td><code>void setInteractive()</code></td>
        <td>设置是否允许场景交互，若禁止交互，则任何键盘或鼠标操作都被忽略</td>
    </tr>
    <tr>
        <td><code>QRect rubberBandRect()</code></td>
        <td>返回选择的矩形框</td>
    </tr>
    <tr>
        <td><code>void setRubberBandSelectionMode()</code></td>
        <td>选择模式，参数为枚举类型<code>Qt::ItemSelectionMode</code></td>
    </tr>
    <tr>
        <td><code>QGraphicsItem *itemAt()</code></td>
        <td>获取视图坐标系中某个位置的图形项</td>
    </tr>
    <tr>
        <td><code>QList&lt;QGraphicsItem*&gt; items()</code></td>
        <td>获取场景中的所有图形项或者某个选择区域内图形项的列表</td>
    </tr>
    <tr>
    	<td rowspan="3">场景显示</td>
        <td><code>void centerOn()</code></td>
        <td>移动视口中的内容，使得场景中的某个坐标点位于视图的中央</td>
    </tr>
    <tr>
        <td><code>void ensureVisible()</code></td>
        <td>移动视口中的内容，确保场景中的某个矩形区域可见</td>
    </tr>
    <tr>
        <td><code>void fitInView()</code></td>
        <td>视图缩放并移动滚卷条，确保场景中的某个矩形区域显示在视口中</td>
    </tr>
    <tr>
    	<td rowspan="4">坐标变换</td>
        <td><code>void translate()</code></td>
        <td>视图坐标系平移</td>
    </tr>
    <tr>
        <td><code>void scale()</code></td>
        <td>视图坐标系缩放</td>
    </tr>
    <tr>
        <td><code>void rotate()</code></td>
        <td>视图坐标系旋转</td>
    </tr>
    <tr>
        <td><code>void shear()</code></td>
        <td>视图坐标系扭转</td>
    </tr>
    <tr>
    	<td rowspan="2">坐标映射</td>
        <td><code>QPoint mapFromScene()</code></td>
        <td>将场景中的一个坐标映射为视图中的坐标</td>
    </tr>
    <tr>
        <td><code>QPointF mapToScene()</code></td>
        <td>将视图中的一个坐标映射为场景中的坐标</td>
    </tr>
</table>

###### `QGraphicsScene`类的主要接口函数

- 该类是用于管理图形项的场景，是图形项的容器。
- 主要接口函数（仅列出函数的返回值类型，省略输入参数）：

<table>
    <tr>
    	<th>分组</th>
        <th>函数</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="4">场景</td>
        <td><code>void setSceneRect()</code></td>
        <td>设置场景的矩形区域</td>
    </tr>
   	<tr>
        <td><code>void setBackgroundBrush()</code></td>
        <td>设置场景的背景画刷</td>
    </tr>
    <tr>
        <td><code>void setForegroundBrush()</code></td>
        <td>设置场景的前景画刷</td>
    </tr>
    <tr>
        <td><code>void update()</code></td>
        <td>刷新场景显示内容</td>
    </tr>
    <tr>
    	<td rowspan="2">分组</td>
        <td><code>QGraphicsItemGroup* createItemGroup()</code></td>
        <td>创建图形项组</td>
    </tr>
   	<tr>
        <td><code>void destroyItemGroup()</code></td>
        <td>解除一个图形项组</td>
    </tr>
    <tr>
    	<td rowspan="3">输入焦点</td>
        <td><code>QGraphicsItem* focusItem()</code></td>
        <td>返回当前获得焦点的图形项</td>
    </tr>
   	<tr>
        <td><code>void clearFocus()</code></td>
        <td>清除选择的焦点</td>
    </tr>
    <tr>
        <td><code>bool hasFocus()</code></td>
        <td>场景是否有焦点</td>
    </tr>
    <tr>
    	<td rowspan="8">图形项操作</td>
        <td><code>void addItem()</code></td>
        <td>添加或移动一个图形项到场景中</td>
    </tr>
   	<tr>
        <td><code>void removeItem()</code></td>
        <td>删除一个图形项</td>
    </tr>
    <tr>
        <td><code>void clear()</code></td>
        <td>清除场景中的所有图形项</td>
    </tr>
    <tr>
        <td><code>QGraphicsItem* mouseGrabberItem()</code></td>
        <td>返回用鼠标抓取的图形项</td>
    </tr>
    <tr>
        <td><code>QList&lt;QGraphicsItem*&gt; selectedItems()</code></td>
        <td>返回选择的图形项列表</td>
    </tr>
    <tr>
        <td><code>void clearSelection()</code></td>
        <td>清除所有选择</td>
    </tr>
    <tr>
        <td><code>QGraphicsItem* itemAt()</code></td>
        <td>获取某个位置的顶层图形项</td>
    </tr>
    <tr>
        <td><code>QList&lt;QGraphicsItem*&gt; items()</code></td>
        <td>返回某个矩形区域，多边形等选择区域内的图形项列表</td>
    </tr>
    <tr>
    	<td rowspan="9">添加图形项</td>
        <td><code>QGraphicsEllipseItem* addEllipse()</code></td>
        <td>创建并添加一个椭圆到场景中</td>
    </tr>
   	<tr>
        <td><code>QGraphicsLineItem* addLine()</code></td>
        <td>创建并添加一条直线到场景中</td>
    </tr>
    <tr>
        <td><code>QGraphicsPathItem* addPath()</code></td>
        <td>创建并添加一条绘图路径, <code>(QPainterPath对象)</code>到场景中</td>
    </tr>
    <tr>
        <td><code>QGraphicsPixmapItem* addPixmap()</code></td>
        <td>创建并添加一个<code>pixmap</code>图片到场景中</td>
    </tr>
    <tr>
        <td><code>QGraphicsPolygonItem* addPolygon()</code></td>
        <td>创建并添加一个多边形到场景中</td>
    </tr>
    <tr>
        <td><code>QGraphicsRectItem* addRect()</code></td>
        <td>创建并添加一个矩形到场景中</td>
    </tr>
    <tr>
        <td><code>QGraphicsSimpleTextItem* addSimpleText()</code></td>
        <td>创建并添加一个<code>QGraphicsSimpleTextItem</code>对象到场景中</td>
    </tr>
    <tr>
        <td><code>QGraphicsTextItem* addText()</code></td>
        <td>创建并添加一个字符串到场景中</td>
    </tr>
    <tr>
        <td><code>QGraphicsProxyWidget* addWidget()</code></td>
        <td>创建并添加一个<code>QGraphicsProxyWidget</code>对象到场景中</td>
    </tr>
</table>

###### 图形项类

- `QGraphicsItem`是所有图形项的基类，可以从该类继承定义自己的图形项。
- `Qt`定义了一些常见的图形项，继承关系如下：
- `QGraphicsItem`
  - `QAbstractGraphicsShapeItem`
    - `QGraphicsEllipseItem`
    - `QGraphicsPathItem`
    - `QGraphicsPolygonItem`
    - `QGraphicsSimpleTextItem`
  - `QGraphicsItemGroup`
  - `QGraphicsLineItem`
  - `QGraphicsObject`
    - `QGraphicsSvgItem`（`qmake: QT += svgwidgets`）
    - `QGraphicsTextItem`
    - `QGraphicsVideoItem`（`qmake: QT += multimediawidgets`）
    - `QGraphicsWidget`
      - `QChart`	`QPolarChart`
      - `QLegend`
- `QGraphicsItem`类提供了有关图形项操作的函数（仅列出函数的返回值类型，省略输入参数）：

<table>
    <tr>
        <th>分组</th>
        <th>函数</th>
        <th>功能</th>
    </tr>
	<tr>
    	<td rowspan="13">图形项属性</td>
        <td><code>void setFlags()</code></td>
        <td>设置图形项的操作标志，例如可选择、可移动等</td>
    </tr>
    <tr>
        <td><code>void setFlag()</code></td>
        <td>启用或禁用图形项的某个标志</td>
    </tr>
    <tr>
        <td><code>void setOpacity()</code></td>
        <td>设置透明度</td>
    </tr>
    <tr>
        <td><code>qreal opacity()</code></td>
        <td>返回图形项的透明度，0表示透明，1表示完全不透明</td>
    </tr>
    <tr>
        <td><code>void setGraphicsEffect()</code></td>
        <td>设置图形效果</td>
    </tr>
    <tr>
        <td><code>void setSelected()</code></td>
        <td>设置图形项是否被选中</td>
    </tr>
    <tr>
        <td><code>bool isSelected()</code></td>
        <td>图形项是否被选中</td>
    </tr>
    <tr>
        <td><code>void setData()</code></td>
        <td>设置用户自定义数据</td>
    </tr>
    <tr>
        <td><code>void setEnabled()</code></td>
        <td>启用或禁用图形项。禁用的图形项是可见的，但是不能接收任何事件</td>
    </tr>
    <tr>
        <td><code>bool isEnabled()</code></td>
        <td>返回图形项的使能状态</td>
    </tr>
    <tr>
        <td><code>void show()</code></td>
        <td>显示图形项</td>
    </tr>
    <tr>
        <td><code>void hide()</code></td>
        <td>隐藏图形项</td>
    </tr>
    <tr>
        <td><code>bool isVisible()</code></td>
        <td>图形项是否可见</td>
    </tr>
    <tr>
    	<td rowspan="5">坐标</td>
        <td><code>void setX()</code></td>
        <td>设置图形项的x坐标</td>
    </tr>
    <tr>
        <td><code>void setY()</code></td>
        <td>设置图形项的y坐标</td>
    </tr>
    <tr>
        <td><code>void setZValue()</code></td>
        <td>设置图形项的Z值，Z值控制图形项的叠放顺序，<code>zValue</code>值越大的显示在前面</td>
    </tr>
    <tr>
        <td><code>void setPos()</code></td>
        <td>设置图形项在父图形项中的位置</td>
    </tr>
    <tr>
        <td><code>QPointF scenePos()</code></td>
        <td>返回图形项在场景中的坐标，相当于调用函数<code>mapToScene(0, 0)</code></td>
    </tr>
    <tr>
    	<td rowspan="3">坐标变换</td>
        <td><code>void resetTransform()</code></td>
        <td>复位坐标系，取消所有坐标变换</td>
    </tr>
    <tr>
        <td><code>void setRotation()</code></td>
        <td>旋转一定角度，参数为正数时表示顺时针旋转</td>
    </tr>
    <tr>
        <td><code>void setScale()</code></td>
        <td>按比例缩放，默认值是1</td>
    </tr>
    <tr>
    	<td rowspan="6">坐标映射</td>
        <td><code>QPoinfF mapFromItem()</code></td>
        <td>将另一个图形的一个点映射到本图形项的坐标系中</td>
    </tr>
    <tr>
        <td><code>QPointF mapFromParent()</code></td>
        <td>将父图形项的一个点映射到本图形项的坐标系中</td>
    </tr>
    <tr>
        <td><code>QPointF mapFromScene()</code></td>
        <td>将场景中的一个点映射到本图形项的坐标系中</td>
    </tr>
    <tr>
        <td><code>QPointF mapToItem()</code></td>
        <td>将本图形项的一个点映射到另一个图形项的坐标系中</td>
    </tr>
    <tr>
        <td><code>QPointF mapToParent()</code></td>
        <td>将本图形项的一个点映射到父图形项的坐标系中</td>
    </tr>
    <tr>
        <td><code>QPointF mapToScene()</code></td>
        <td>将本图形项的一个点映射到场景坐标系中</td>
    </tr>
</table>

- `setMouseTracking(true)`：开启鼠标跟踪，在鼠标移动时就会产生`mouseMove`事件。默认是不开启的，那么只有在按下鼠标某个键时才会产生`mouseMove`事件。
- `setDragMode(QGraphicsView::RubberBandDrag)`：在场景的背景上点击并拖动鼠标时，显示一个矩形框。默认是不响应（`QGraphicsView::NoDrag`），其值还可以设置为`QGraphicsView::ScrollHandDrag`，拖动鼠标时鼠标光标变为手型，拖动鼠标就会使滚卷条移动。

```c++
#ifndef TGRAPHICSVIEW_H
#define TGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QWidget>
class TGraphicsView : public QGraphicsView {
  Q_OBJECT
 public:
  TGraphicsView(QWidget *parent = nullptr);

 signals:
  void mouseMovePoint(QPoint point);
  void mouseClicked(QPoint point);
  // QWidget interface
 protected:
  void mousePressEvent(QMouseEvent *event) override;
  void mouseMoveEvent(QMouseEvent *event) override;
};

#endif  // TGRAPHICSVIEW_H
```

```c++
#include "tgraphicsview.h"

#include <QMouseEvent>
TGraphicsView::TGraphicsView(QWidget *parent) : QGraphicsView(parent) {}

void TGraphicsView::mousePressEvent(QMouseEvent *event) {
  if (event->button() == Qt::LeftButton) {
    QPoint point = event->pos();
    emit mouseClicked(point);
  }
  QGraphicsView::mousePressEvent(event);
}

void TGraphicsView::mouseMoveEvent(QMouseEvent *event) {
  // 鼠标移动事件
  // 视图中的坐标
  QPoint point = event->pos();
  emit mouseMovePoint(point);
  QGraphicsView::mouseMoveEvent(event);
}
```

```c++
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QGraphicsScene>
#include <QLabel>
#include <QMainWindow>
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
 private slots:
  void do_mouseMovePoint(QPoint point);
  void do_mouseClicked(QPoint point);

 private:
  Ui::MainWindow *ui;
  // 场景对象
  QGraphicsScene *scene;
  // 用于在状态栏上显示坐标信息
  QLabel *labViewCord;
  QLabel *labSceneCord;
  QLabel *labItemCord;
  // 图形/视图架构初始化
  void initGraphicsSystem();
  // QWidget interface
 protected:
  void resizeEvent(QResizeEvent *event) override;
};
#endif  // MAINWINDOW_H
```

```c++
#include "mainwindow.h"

#include <QGraphicsRectItem>

#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  labViewCord = new QLabel("View 坐标: ", this);
  labViewCord->setMinimumWidth(150);
  ui->statusbar->addWidget(labViewCord);

  labSceneCord = new QLabel("Scene 坐标: ", this);
  labSceneCord->setMinimumWidth(150);
  ui->statusbar->addWidget(labSceneCord);

  labItemCord = new QLabel("Item 坐标: ", this);
  labItemCord->setMinimumWidth(150);
  ui->statusbar->addWidget(labItemCord);
  // 自定义组件的view设置
  ui->graphicsView->setCursor(Qt::CrossCursor);
  // 开启鼠标跟踪
  ui->graphicsView->setMouseTracking(true);
  // 矩形选择框
  ui->graphicsView->setDragMode(QGraphicsView::RubberBandDrag);

  connect(ui->graphicsView, &TGraphicsView::mouseMovePoint, this,
          &MainWindow::do_mouseMovePoint);
  connect(ui->graphicsView, &TGraphicsView::mouseClicked, this,
          &MainWindow::do_mouseClicked);
  initGraphicsSystem();
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::do_mouseMovePoint(QPoint point) {
  labViewCord->setText(
      QString::asprintf("View 坐标: %d, %d", point.x(), point.y()));
  // 变换为场景坐标
  QPointF pointScene = ui->graphicsView->mapToScene(point);
  labSceneCord->setText(QString::asprintf("Scene 坐标: %.0f, %.0f",
                                          pointScene.x(), pointScene.y()));
}

void MainWindow::do_mouseClicked(QPoint point) {
  QPointF pointScene = ui->graphicsView->mapToScene(point);
  QGraphicsItem *item = nullptr;
  // 获取光标处的图形项
  item = scene->itemAt(pointScene, ui->graphicsView->transform());
  if (item != nullptr) {
    QPointF pointItem = item->mapFromScene(pointScene);
    labItemCord->setText(QString::asprintf("Item 坐标: %.0f, %.0f",
                                           pointItem.x(), pointItem.y()));
  }
}

void MainWindow::initGraphicsSystem() {
  QRectF rect(-200, -100, 400, 200);
  // 场景视图坐标系定义
  scene = new QGraphicsScene(rect, this);
  // 为视图设置场景
  ui->graphicsView->setScene(scene);
  // 画一个矩形框, 其大小等于场景的大小
  QGraphicsRectItem *item = new QGraphicsRectItem(rect);
  // 可选择, 可以获得焦点, 但是不能移动
  item->setFlags(QGraphicsItem::ItemIsSelectable |
                 QGraphicsItem::ItemIsFocusable);
  QPen pen;
  pen.setWidth(2);
  item->setPen(pen);
  scene->addItem(item);
  // 一个位于场景中心的椭圆, 测试局部坐标
  QGraphicsEllipseItem *item2 = new QGraphicsEllipseItem(-100, -50, 200, 100);
  // 矩形框内创建椭圆, 图形项的局部坐标, 左上角坐标为(-100, -50), 宽200, 高100
  item2->setPos(0, 0);  // 图形项在场景中的坐标
  item2->setBrush(QBrush(Qt::blue));
  // 能移动
  item2->setFlag(QGraphicsItem::ItemIsMovable);
  item2->setFlag(QGraphicsItem::ItemIsSelectable);
  item2->setFlag(QGraphicsItem::ItemIsFocusable);
  scene->addItem(item2);
  // 一个圆, 中心位于场景的边缘
  QGraphicsEllipseItem *item3 = new QGraphicsEllipseItem(-50, -50, 100, 100);
  // 矩形框内创建圆, 图形项的局部坐标, 左上角坐标为(-100, -50), 宽200, 高100
  item3->setPos(rect.right(), rect.bottom());
  item3->setBrush(QBrush(Qt::red));
  item3->setFlag(QGraphicsItem::ItemIsMovable);
  item3->setFlag(QGraphicsItem::ItemIsSelectable);
  item3->setFlag(QGraphicsItem::ItemIsFocusable);
  scene->addItem(item3);
  scene->clearSelection();
}

void MainWindow::resizeEvent(QResizeEvent *event) {
  QString str = QString::asprintf(
      "Graphics View坐标, 左上角总是(0, 0), 宽度 = %d, 高度 = %d",
      ui->graphicsView->width(), ui->graphicsView->height());
  ui->plainTextEdit->appendPlainText(str);
  // 场景的矩形区域
  QRectF rect = ui->graphicsView->sceneRect();
  QString str2 = QString::asprintf(
      "QraphicsView::sceneRect = (Left, Top, Width, Height) = (%.0f, %.0f, "
      "%.0f, %.0f)",
      rect.left(), rect.top(), rect.width(), rect.height());
  ui->plainTextEdit->appendPlainText(str2);
  event->accept();
}
```

#### 4、图像处理

- `QImage`可以访问图像中每个像素的颜色数据，适用于需要对图像数据进行处理的应用，例如可用于旋转或反转图像，改变图像亮度或对比度等。

##### 图像表示和图像处理概述

###### 颜色数据的格式

- 图像是在各占绘图设备上显示的二维的图，图像的数据可以看做二维数组，数组的每个元素就是1像素的颜色数据，在绘图设备上显示图像就是设置每个像素的颜色。
- 任何颜色都是红色、绿色、蓝色三原色的组合，1像素的颜色数据有多种表示格式，常见的有以下几种：
  - `RGB32`用32位无符号整数表示颜色。数据格式是：`0xffRRGGBB`，其中最高字节的`ff`是无意义的，实际是用24位有效数据表示颜色。。因为32位无符号整数是标准的整数格式，所以在计算机上存储的图片文件一般采用这种格式表示像素的颜色。
  - `RGB888`：即红色、绿色、蓝色各用1字节表示，数据格式是`0xRRGGBB`，1像素的颜色占用3字节。`RGB888`与`RGB32`一样也是用`24`位数据表示颜色，但是少占用1字节。在内存有限的嵌入式系统中可能会使用`RGB888`格式。但是3字节数据不是标准类型的整数，所以即使在内存中表示颜色使用了`RGB888`格式，在计算机上保存图片文件时也会使用`RGB32`格式。
  - `ARGB32`：在`RGB32`的基础上用1字节表示`Alpha`值，数据格式是`0xAARRGGBB`，`Alpha`值一般作为不透明度参数。
  - `RGBA32`：与`ARGB32`类似，只是存储格式是`0xRRGGBBAA`。
  - `RGB565`：用16位无符号整数表示1像素的颜色，其中红色占5位，绿色占6位，蓝色占5位，1像素只需2字节就可以表示。
  - `Grayscale8`：用8位无符号整数表示1像素的灰度颜色。
  - `Grayscale16`：用16位无符号整数表示1像素的灰度颜色。

###### 图片文件格式

- 图像数据可以保存为不同格式的图片文件，常见的图片文件格式有`BMP, JPG, PNG, SVG`等。
  - `BMP`是位图文件格式，其文件头存储图像的一些信息，如图像宽度、高度、颜色数据格式等，图像中所有像素的颜色数据被无修改地保存在文件里，例如每个像素的颜色是一个`RGB32`的数据。`BMP`是一种无损图片文件格式，保留了图像的原始颜色数据，文件格式比较简单，但是文件比较大。
  - `JPG`是使用了联合图像专家组图像压缩算法的图片文件格式，是一种有损压缩格式，可以在保存较高图像质量的情况下使文件大小减少很多，从而节省存储空间。
  - 便携式网络图形`PNG`是一种无损压缩图片文件格式，它具有一定的压缩率，文件解压后就是真实的原图。`PNG`图像的颜色数据可以有`Alpha`通道。
  - `SVG`是基于`XML`，描述图像绘制方法的图片格式。`SVG`文件存储的是绘制图像的过程，而不是图像的像素颜色数据。
- 使用`QImage`和`QPixmap`可以直接加载`BMP`、`JPG`和`PNG`
- 要读取和现实`SVG`图片文件，需要使用`QSvgRenderer`和`QSvgWidget`类。

###### `QImage`类的作用

- `QPixmap`主要用于在界面上显示图像，可以对图像进行缩放，但是不能像`QImage`那样对图像进行像素处理。
- 加载和保存图像数据
  - `QImage(const QString &filename, const char *format = nullptr)`指定图片文件名，文件格式使用`BMP`、`JPG`等字符串表示，如果不设置会根据文件名的后缀自动判断。
  - `QImage(int width, int height, QImage::Format format)`设置图像大小。创建指定宽度和高度的图像，单位是像素，参数`format`是像素的颜色数据格式，用这种方式创建图像，一般是为了使用`QPainter`对象在此图像上绘图。
  - `QImage()`不设置任何参数，之后一般会用`load()`加载图片文件。
- 创建`QImage`对象后，可以从文件或其他`I/O`设备加载图像数据，也可以将图像数据保存为文件或保存到其他`I/O`设备中。
- 表中省略的函数的输入参数。

| 函数                  | 功能                                                         |
| --------------------- | ------------------------------------------------------------ |
| `bool load()`         | 从图片文件或其他`I/O`设备加载图像数据                        |
| `bool loadFromData()` | 从字节数据数据中加载图像数据                                 |
| `bool save()`         | 将图像数据保存为文件或保存到其他`I/O`设备中，例如保存到一个`QBuffer`对象中。 |

- `bool QImage::load(const QString &filename, const char *format = nullptr)`
- `bool QImage::save(const QString &filename, const char *format = nullptr, int quality = -1)`
- 其中`format`是使用`BMP`，`JPG`等字符串表示的。参数`quality`是保存为有损压缩图片文件（如`JPG`图片文件）时的品质参数，取值为0表示最低品质，压缩后文件最小，取值为100表示最高品质，图像无压缩；取值为`-1`表示使用默认的品质参数。

- 图像信息（省略了输入参数）

<table>
    <tr>
    	<th>分组</th>
        <th>函数</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="4">基本信息</td>
        <td><code>QImage::Format format()</code></td>
        <td>返回图像的颜色格式信息</td>
    </tr>
    <tr>
        <td><code>int depth()</code></td>
        <td>图像深度, 就是指表示1像素颜色数据的位数，可能是1、8、16、24、32或64</td>
    </tr>
    <tr>
        <td><code>int bitPlaneCount()</code></td>
        <td>返回图像的位平面数。位平面数就是指表示1像素的颜色和透明度数据的位数，例如32位。他的值小于或等于图像深度值</td>
    </tr>
    <tr>
        <td><code>qsizetype sizeInBytes()</code></td>
        <td>返回图像数据的字节数，字节数由图像宽度、图像高度和图像深度决定，不是原始图片文件的大小</td>
    </tr>
    <tr>
    	<td rowspan="8">几何信息</td>
        <td><code>int width()</code></td>
        <td>图像宽度，像素</td>
    </tr>
    <tr>
        <td><code>int height()</code></td>
        <td>图像高度，像素</td>
    </tr>
    <tr>
        <td><code>QRect rect()</code></td>
        <td>返回图像的矩形边框，即矩形<code>QRect(0, 0, width(), height())</code></td>
    </tr>
    <tr>
        <td><code>QSize size()</code></td>
        <td>返回图像的大小，即<code>width()</code>和<code>height()</code></td>
    </tr>
    <tr>
        <td><code>int dotsPerMeterX()</code></td>
        <td>图像在水平方向上的<code>DPM</code>分辨率，若要转换为常用的<code>DPI</code>分辨率，需要将这个函数的返回值乘0.0254</td>
    </tr>
    <tr>
        <td><code>void setDotsPerMeterX()</code></td>
        <td>设置图像在水平方向上的<code>DPM</code>分辨率</td>
    </tr>
    <tr>
        <td><code>int dotsPerMeterY()</code></td>
        <td>图像在垂直方向上的<code>DPM</code>分辨率</td>
    </tr>
    <tr>
        <td><code>void setDotsPerMeterY()</code></td>
        <td>设置图像在垂直方向上的<code>DPM</code>分辨率</td>
    </tr>
</table>

- 图像格式是枚举类型`QImage::Format`常见含义：
  - `QImage::Format_RGB32`数据格式为`0xffRRGGBB`，其中最高字节的`ff`是无意义的，1像素占用4字节
  - `QImage::Format_ARGB32`数据格式为`0xAARRGGBB`
  - `QImage::Format_RGB888`数据格式为`0xRRGGBB`
  - `QImage::Format_RGB16`即`RGB565`格式
  - `QImage::Format_Grayscale8`8位灰度格式
  - `QImage::Format_Grayscale16`16位灰度格式
  - `QImage::Format_Indexed8`8位颜色索引格式
  - `QImage::Format_Mono`1像素用1位表示，只有黑色或白色，组成字节数组时高位在前（`MSB`）
  - `QImage::Format_MonoLSB`1像素用1位表示，组成字节数组时低位在前（`LSB`）
- 除了单色图，其它格式图像的像素都用整数表示，有直接表示和间接表示两种方式。
- 8位颜色索引和8位灰度图是间接表示，间接表示的图像有一个256色的颜色表，颜色表是`QList<QRgb>`，每个颜色都是`ARGB32`格式的。每个像素有一个1字节的索引值，可以通过索引值从颜色表里获取对应的颜色。
- 其它格式的图像采用的是直接表示方式，没有颜色表，每个像素的颜色用一个`QRgb`类型的数表示，各种图形格式的差别在于，颜色是否有`Alpha`通道以及红色、绿色、蓝色的有效位数不同。
- 灰度图的颜色就是`QRgb(c, c, c)`，也就是红蓝绿的值相同。8位灰度图有颜色表，是间接表示的，16位灰度图是直接表示的。
- 图像深度和位平面数：例如对于`RGB32`格式，图像深度是32位，但是位平面数是24位，因为只有24位有效数据。
- 图像的大小：返回图像中所有像素的颜色数据所占用的字节数，其值等于`width()*height()*depth()/8`
- 图像的分辨率：`DPI`和`DPM`的换算关系是`1 DPI = 0.0254 DPM`

- 访问颜色数据（省略了函数的输入参数）

<table>
    <tr>
    	<th>分组</th>
        <th>函数</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="4">直接方式，不带颜色表</td>
        <td><code>QRgb pixel()</code></td>
        <td>返回某一像素的颜色数据，返回值类型<code>QRgb</code>是无符号32位整数</td>
    </tr>
    <tr>
        <td><code>void setPixel()</code></td>
        <td>设置某一像素的颜色数据，颜色数据是<code>QRgb</code>类型</td>
    </tr>
    <tr>
        <td><code>QColor pixelColor()</code></td>
        <td>返回某一像素的颜色数据，颜色数据是<code>QColor</code>类型</td>
    </tr>
    <tr>
        <td><code>void setPixelColor()</code></td>
        <td>设置某一像素的颜色数据，颜色数据是<code>QColor</code>类型</td>
    </tr>
    <tr>
    	<td rowspan="5">间接方式，带颜色表</td>
        <td><code>int colorCount()</code></td>
        <td>返回颜色表的颜色数，只有8位图像有颜色表</td>
    </tr>
    <tr>
        <td><code>QList&lt;QRgb&gt; colorTable()</code></td>
        <td>返回颜色表的颜色数据列表，若返回列表为空，表示图像没有颜色表</td>
    </tr>
    <tr>
        <td><code>int pixelIndex()</code></td>
        <td>返回某一像素的颜色在颜色表中的索引</td>
    </tr>
    <tr>
        <td><code>QRgb color()</code></td>
        <td>根据索引，返回颜色表中某一个颜色值</td>
    </tr>
    <tr>
        <td><code>void setPixel()</code></td>
        <td>设置某一像素的颜色索引</td>
    </tr>
    <tr>
    	<td rowspan="3">颜色信息</td>
        <td><code>bool hasAlphaChannel()</code></td>
        <td>判断图像是否有<code>Alpha</code>通道</td>
    </tr>
    <tr>
        <td><code>bool allGray()</code></td>
        <td>判断图像中的所有像素的颜色是否都是灰度颜色，该函数的处理速度比较慢</td>
    </tr>
    <tr>
        <td><code>bool isGrayscale()</code></td>
        <td>对于有颜色表的图像，判断颜色表中的颜色是否都是灰度颜色；对于无颜色表的图像，该函数的功能和<code>allGray()</code>相同</td>
    </tr>
</table>

- 读取像素颜色：`QRgb QImage::pixel(int x, int y)`
- 用于`QRgb`类型数据处理的一些独立函数：

| 函数原型                                 | 功能                                           |
| ---------------------------------------- | ---------------------------------------------- |
| `int qRed(QRgb rgb)`                     | 返回`QRgb`类型变量`rgb`中红色成分的值          |
| `int qGreen(QRgb rgb)`                   | 返回`QRgb`类型变量`rgb`中蓝色成分的值          |
| `int qBlue(QRgb rgb)`                    | 返回`QRgb`类型变量`rgb`中绿色成分的值          |
| `int qAlpha(QRgb rgb)`                   | 返回`QRgb`类型变量`rgb`中`Alpha`成分的值       |
| `QRgb qRgb(int r, int g, int b)`         | 用`(255, r, g, b)`合成`QRgb`数值               |
| `QRgb qRgba(int r, int g, int b, int a)` | 用`(r, g, b, a)`合成`QRgb`数值                 |
| `int QGray(int r, int g, int b)`         | 从`(r, g, b)`计算出灰度值，灰度值范围值`0~255` |

- `QColor`是表示颜色的类，可以将`QColor`表示的颜色转换为`QRgb`类型的颜色。
  - `QRgb QColor::rgb()`返回颜色的`QRgb`类型数据，`Alpha`成分的值是255
  - `QRgb QColor::rgba()`返回颜色的`QRgb`类型数据，包括`Alpha`通道的值
  - `int QColor::red()`返回颜色中红色成分的数值
  - `int QColor::green()`返回颜色中绿色成分的数值
  - `int QColor::blue()`返回颜色中蓝色成分的数值
  - `int QColor::alpha()`返回颜色中`Alpha`通道的数值
- `QColor`有丰富的接口函数用于颜色数据处理，它可以用`RGB, HSV, CMYK`等模式表示颜色数据，可以修改颜色的饱和度和亮度。

###### 图像处理

- 省略了函数的输入参数：

| 函数                       | 功能                                                         |
| -------------------------- | ------------------------------------------------------------ |
| `void invertPixels()`      | 对像素颜色值取反                                             |
| `void mirror()`            | 对图像进行水平或垂直翻转                                     |
| `QImage mirrored()`        | 返回原图像水平或垂直翻转后的图像，原图像不变                 |
| `void rgbSwap()`           | 互换所有像素颜色数据的红色和蓝色成分，可以有效的将`RGB`格式转换为`BGR`格式 |
| `QImage rgbSwapped()`      | 返回一个`QImage`对象，它表示原图像中所有像素颜色的红色和蓝色互换后的结果 |
| `QImage transformed()`     | 使用一个变换矩阵对原图像进行变换，返回变换后的图像副本       |
| `QImage scaled()`          | 对图像进行缩放，返回缩放后的图像副本                         |
| `QImage scaledToHeight()`  | 对图像进行缩放，使其适应某个高度，返回缩放后的图像副本       |
| `QImage scaledToWidth()`   | 对图像进行缩放，使其适应某个宽度，返回缩放后的图像副本       |
| `QImage copy()`            | 复制图像的一个矩形区域，将其作为一个新的`QImage`对象返回     |
| `void convertTo()`         | 图像格式转换，覆盖原有图像数据                               |
| `QImage convertedTo()`     | 图像格式转换，返回转换后的图像副本，原图像不变               |
| `QImage convertToFormat()` | 图像格式转换，返回转换后的图像副本，原图像不变               |

- 在内存中进行图像处理时可以使用所有的格式，但是图像保存到文件后，文件内一般会自动以32位或8位格式存储图像数据。

- 图像缩放：

  ```c++
  QImage scaled(int w, int h, Qt::AspectRatioMode aspectMode = Qt::IgnoreAspectRatio,
                                  Qt::TransformationMode mode = Qt::FastTransformation) const
  ```

  - `aspectMode`控制是否保持图像缩放比，默认忽略长宽比，`Qt::KeepAspectRatio`表示保持长宽比
  - `mode`表示变换模式，默认表示快速变换，不做平滑处理，也可以设置为`Qt::SmoothTransformation`表示进行平滑处理

##### 打印功能

###### 打印相关的类

- `QPrinter`是实现打印功Q能的绘图设备类，打印输出实际上就是在`QPrinter`上用`QPainter`绘制各种图形和文字。
- `QPrintDialog`是打印设置对话框类，使用这个对话框可以对`QPrinter`对象的各种主要属性进行设置，包括选择打印机，设置纸张大小、纸张方向、打印页面范围、打印份数、是否双面打印等。
- `QPrintPreviewDialog`可以实现打印预览功能。
- `QPagedPaintDevice`类定义了多页打印输出的一些基本函数（省略输入参数）：

| 函数                        | 功能                                                      |
| --------------------------- | --------------------------------------------------------- |
| `bool setPageSize()`        | 设置纸张大小，如`A4, B5`等                                |
| `void setPageOrientation()` | 设置纸张方向，有纵向和横向两种                            |
| `void setPageMargins()`     | 设置纸张的边距，即上下左右不可打印的范围                  |
| `void setPageRanges()`      | 设置打印的页面范围，例如只打印某几页                      |
| `QPageRanges pageRanges()`  | 返回当前的打印页面范围，打印页面范围用`QPageRanges`类表示 |
| `void setPageLayout()`      | 设置页面布局，包括纸张大小，纸张方向，页边距等            |
| `QPageLayout pageLayout()`  | 返回当前的页面布局设置，页面布局设置用`QPageLayout`类表示 |
| `bool newPage()`            | 新建一个打印页面，在新建页面之前需要设置好页面布局        |

###### `QPrinter`类

- 主要接口如下（如果有对应的读取函数，列出设置函数，省略输入参数）：

| 函数                       | 功能                                                         |
| -------------------------- | ------------------------------------------------------------ |
| `void setPrinterName()`    | 设置打印机名称。若设置的名称为空，则打印输出到`PDF`文件；若设置的是有效的打印机名称，则打印输出到打印机 |
| `bool isValid()`           | 若返回值是`true`，表示选择的是有效的打印机或`PDF`输出设备，否则表示选择的是无效的打印设备 |
| `void setOutputFileName()` | 设置输出文件名。若设置的输出文件名为空，表示禁止打印到文件；若设置的输出文件名后缀是`.pdf`，表示打印输出到`PDF`文件 |
| `void setOutputFormat()`   | 设置打印机输出格式，有两种打印输出格式，其中`PdfFormat`表示输出到`PDF`文件，`NativeFormat`表示输出到打印机 |
| `void setDocName()`        | 设置打印文档名称                                             |
| `void setColorMode()`      | 设置打印颜色模式，即彩色或灰度                               |
| `void setResolution()`     | 设置打印机的`DPI`分辨率                                      |
| `void setPdfVersion()`     | 当打印机输出到`PDF`文件时，设置`PDF`版本                     |
| `void setFullPage()`       | 设置是否全页面打印，若设置为`true`，打印机坐标系的起点在纸张的左上角，否则在可打印区域的左上角 |
| `QRectF paperRect()`       | 返回纸张的矩形区                                             |
| `QRectF pageRect()`        | 返回可打印页面的矩形区，`pageRect()`的值通常小于`pagerRect()`的值，因为有页面距 |
| `void setCopyCount()`      | 设置打印份数                                                 |
| `void setPageOrder()`      | 设置页面打印顺序，`FirstPageFirst`表示顺序打印，`LastPageFirst`表示倒序打印 |
| `void setFromTo()`         | 设置打印页面范围，从某页到某页，打印页面从1开始编号          |
| `void setDuplex()`         | 设置双面打印                                                 |

###### 打印预览

- `Qt`中有一个打印预览对话框类`QPrintPreviewDialog`，他是一个独立的对话框。
- 要实现打印预览的功能，关键是要为`QPrintPreviewDialog`的`paintRequested()`信号关联一个槽函数，在槽函数里输出打印内容：
  - `void QPrintPreviewDialog::paintRequested(QPrinter *printer)`

```c++
#include "mainwindow.h"

#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QPainter>
#include <QPrintDialog>
#include <QPrintPreviewDialog>
#include <QTextBlock>

#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  setCentralWidget(ui->splitter);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::do_paintRequestedImage(QPrinter *printer) {
  QPainter painter(printer);
  printImage(&painter, printer);
}

void MainWindow::do_paintRequestedText(QPrinter *printer) {
  QPainter painter(printer);
  printRGB565Data(&painter, printer);
}

void MainWindow::showImageFeatures(bool formatChanged) {
  // 格式转换之后显示全部信息
  if (formatChanged) {
    QImage::Format fmt = this->m_image.format();
    QString fmtStr;
    switch (fmt) {
      case QImage::Format_RGB32:
        fmtStr = "32-bit RGB(0xffRRGGBB)";
        break;
      case QImage::Format_RGB16:
        fmtStr = "16-bit RGB565";
        break;
      case QImage::Format_RGB888:
        fmtStr = "24-bit RGB888";
        break;
      case QImage::Format_Grayscale8:
        fmtStr = "8-bit grayscale";
        break;
      case QImage::Format_Grayscale16:
        fmtStr = "16-bit grayscale";
        break;
      case QImage::Format_ARGB32:
        fmtStr = "32-bit ARGB(0xAARRGGBB)";
        break;
      case QImage::Format_Indexed8:
        fmtStr = "8-bit indexes into a colormap";
        break;
      default:
        fmtStr = QString("Format = %1, 其它格式").arg(fmt);
        break;
    }
    // 图片格式
    ui->lineEdit->setText(fmtStr);
    // 图片深度
    ui->lineEdit_4->setText(
        QString("%1 bits/pixel").arg(this->m_image.depth()));
    // 位平面数
    ui->lineEdit_5->setText(
        QString("%1 bits").arg(this->m_image.bitPlaneCount()));
    // 是否具有Alpha通道
    ui->checkBox_2->setChecked(this->m_image.hasAlphaChannel());
    // 是否灰度图
    ui->checkBox->setChecked(this->m_image.isGrayscale());
  }
  // 缩放或旋转之后显示大小信息
  ui->lineEdit_3->setText(QString("%1 像素").arg(this->m_image.height()));
  ui->lineEdit_2->setText(QString("%1 像素").arg(this->m_image.width()));
  // 图像数据字节数
  qsizetype sz = this->m_image.sizeInBytes();
  if (sz < 1024 * 9) {
    ui->lineEdit_6->setText(QString("%1 Bytes").arg(sz));
  } else {
    ui->lineEdit_6->setText(QString("%1 KB").arg(sz / 1024));
  }
  // 分辨率
  QString dpi = QString::asprintf("DPI_X = %.0f, DPI_Y = %.0f",
                                  this->m_image.dotsPerMeterX() * 0.0254,
                                  this->m_image.dotsPerMeterY() * 0.0254);
  ui->lineEdit_7->setText(dpi);
}

void MainWindow::imageModified(bool modified) {
  ui->actLoad->setEnabled(modified);
  ui->actSave->setEnabled(modified);
}

void MainWindow::printImage(QPainter *painter, QPrinter *printer) {
  // 打印图像
  // 上下左右四个边距
  QMargins margin(20, 40, 20, 40);
  QRectF pageRect = printer->pageRect(QPrinter::DevicePixel);
  int pageW = static_cast<int>(pageRect.width());
  int pageH = static_cast<int>(pageRect.height());
  const int lineInc = 30;
  int curX = margin.left();
  int curY = margin.top();
  painter->drawText(curX, curY, this->m_filename);
  curY += lineInc;
  painter->drawText(curX, curY, QString("Page width = %1 像素").arg(pageW));
  painter->drawText(
      300, curY, QString("Image width = %1 像素").arg(this->m_image.width()));
  curY += lineInc;
  painter->drawText(curX, curY, QString("Page height = %1 像素").arg(pageH));
  painter->drawText(
      300, curY, QString("Image height = %1 像素").arg(this->m_image.height()));
  curY += lineInc;
  // 页面剩余的高度
  int spaceH = pageH - curY;
  // 图像未超出页面范围, 居中显示实际大小的图片
  if (pageW > this->m_image.width() && spaceH > this->m_image.height()) {
    curX = (pageW - this->m_image.width()) / 2;
    painter->drawImage(curX, curY, this->m_image);
    return;
  }
  // 否则缩放后打印
  QImage newImage;
  if (this->m_image.height() > this->m_image.width()) {
    newImage = this->m_image.scaledToHeight(spaceH);
  } else {
    newImage = this->m_image.scaledToWidth(pageW);
  }
  curX = (pageW - newImage.width()) / 2;
  painter->drawImage(curX, curY, newImage);
}

void MainWindow::printRGB565Data(QPainter *painter, QPrinter *printer) {
  // 打印文档
  QMargins margin(20, 40, 20, 40);
  QRectF pageRect = printer->pageRect(QPrinter::DevicePixel);
  int pageW = static_cast<int>(pageRect.width());
  int pageH = static_cast<int>(pageRect.height());
  const int lineInc = 30;
  int curX = margin.left();
  int curY = margin.top();
  QFont font = ui->plainTextEdit->font();
  painter->setFont(font);
  int pageNum = 1;
  // 页脚划线
  painter->drawLine(margin.left(), pageH - margin.bottom() + 1,
                    pageW - margin.right(), pageH - margin.bottom() + 1);
  // 页脚页面编号
  painter->drawText(pageW - 5 * margin.right(), pageH - margin.bottom() + 20,
                    QString("第 %1 页").arg(pageNum));
  QTextDocument *doc = ui->plainTextEdit->document();
  // 回车符是一个块
  int cnt = doc->blockCount();
  for (int i = 0; i < cnt; ++i) {
    QTextBlock textLine = doc->findBlockByNumber(i);
    QString str = textLine.text();
    painter->drawText(curX, curY, str);
    curY += lineInc;
    if (curY >= pageH - margin.bottom()) {
      printer->newPage();
      curY = margin.top();
      pageNum++;
      // 页脚划线
      painter->drawLine(margin.left(), pageH - margin.bottom() + 1,
                        pageW - margin.right(), pageH - margin.bottom() + 1);
      // 页脚页面编号
      painter->drawText(pageW - 5 * margin.right(),
                        pageH - margin.bottom() + 20,
                        QString("第 %1 页").arg(pageNum));
    }
  }
}

void MainWindow::on_actOpen_triggered() {
  QString curPath = QDir::currentPath();
  QString filter = "图片文件(*.bmp *.jpg *.png)";
  QString filename =
      QFileDialog::getOpenFileName(this, "选择图片文件", curPath, filter);
  if (filename.isEmpty()) {
    return;
  }
  ui->statusbar->showMessage(filename);
  this->m_filename = filename;
  QFileInfo fileInfo(filename);
  QDir::setCurrent(fileInfo.absoluteFilePath());
  this->m_image.load(filename);
  QPixmap pixmap = QPixmap::fromImage(this->m_image);
  ui->label_8->setPixmap(pixmap);
  ui->tabWidget->setCurrentIndex(0);
  showImageFeatures();
  ui->actLoad->setEnabled(true);
  ui->actSave2->setEnabled(true);
  ui->actPrintPreview->setEnabled(true);
  ui->actPrint->setEnabled(true);
  ui->actEnlarge->setEnabled(true);
  ui->actNarrow->setEnabled(true);
  ui->actLeft->setEnabled(true);
  ui->actRight->setEnabled(true);
  ui->actTB->setEnabled(true);
  ui->actLR->setEnabled(true);
}

void MainWindow::on_actSave_triggered() {
  this->m_image.save(this->m_filename);
  imageModified(false);
}

void MainWindow::on_actSave2_triggered() {
  QString filter = "图片文件(*.bmp *.jpg *.png)";
  QString filename =
      QFileDialog::getSaveFileName(this, "保存文件", this->m_filename, filter);
  if (filename.isEmpty()) {
    return;
  }
  // 该函数会自动进行格式转换
  this->m_image.save(filename);
  this->m_filename = filename;
  ui->statusbar->showMessage(filename);
  imageModified(false);
}

void MainWindow::on_actLoad_triggered() {
  QString filename = this->m_filename;
  this->m_image.load(filename);
  QPixmap pixmap = QPixmap::fromImage(this->m_image);
  ui->label_8->setPixmap(pixmap);
  ui->tabWidget->setCurrentIndex(0);
  showImageFeatures(true);
  imageModified(false);
}

void MainWindow::on_pushButton_clicked() {
  // 图像格式转换
  int index = ui->comboBox->currentIndex();
  QImage::Format fmt;
  switch (index) {
    case 0:
      fmt = QImage::Format_RGB16;
      break;
    case 1:
      fmt = QImage::Format_RGB888;
      break;
    case 2:
      fmt = QImage::Format_RGB32;
      break;
    case 3:
      fmt = QImage::Format_Grayscale8;
      break;
    case 4:
      fmt = QImage::Format_Grayscale16;
      break;
    case 5:
      fmt = QImage::Format_Indexed8;
      break;
    default:
      return;
  }
  this->m_image.convertTo(fmt);
  QPixmap pixmap = QPixmap::fromImage(this->m_image);
  ui->label_8->setPixmap(pixmap);
  showImageFeatures(true);
  imageModified(true);
}

void MainWindow::on_actEnlarge_triggered() {
  // 放大
  int w = this->m_image.width();
  int h = this->m_image.height();
  this->m_image = this->m_image.scaled(1.1 * w, 1.1 * h, Qt::KeepAspectRatio);
  QPixmap pixmap = QPixmap::fromImage(this->m_image);
  ui->label_8->setPixmap(pixmap);
  showImageFeatures(false);
  imageModified(true);
}

void MainWindow::on_actNarrow_triggered() {
  // 缩小
  int w = this->m_image.width();
  int h = this->m_image.height();
  this->m_image = this->m_image.scaled(0.9 * w, 0.9 * h, Qt::KeepAspectRatio);
  QPixmap pixmap = QPixmap::fromImage(this->m_image);
  ui->label_8->setPixmap(pixmap);
  showImageFeatures(false);
  imageModified(true);
}

void MainWindow::on_actLeft_triggered() {
  // 左旋
  QTransform matrix;
  matrix.reset();
  // 默认Qt::ZAxis
  matrix.rotate(-90);
  this->m_image = this->m_image.transformed(matrix);
  QPixmap pixmap = QPixmap::fromImage(this->m_image);
  ui->label_8->setPixmap(pixmap);
  showImageFeatures(false);
  imageModified(true);
}

void MainWindow::on_actRight_triggered() {
  // 右旋
  QTransform matrix;
  matrix.reset();
  // 默认Qt::ZAxis
  matrix.rotate(90);
  this->m_image = this->m_image.transformed(matrix);
  QPixmap pixmap = QPixmap::fromImage(this->m_image);
  ui->label_8->setPixmap(pixmap);
  showImageFeatures(false);
  imageModified(true);
}

void MainWindow::on_actTB_triggered() {
  // 上下翻转
  bool horizontal = false;
  bool vertical = true;
  this->m_image.mirror(horizontal, vertical);
  QPixmap pixmap = QPixmap::fromImage(this->m_image);
  ui->label_8->setPixmap(pixmap);
  imageModified(true);
}

void MainWindow::on_actLR_triggered() {
  // 左右翻转
  bool horizontal = true;
  bool vertical = false;
  this->m_image.mirror(horizontal, vertical);
  QPixmap pixmap = QPixmap::fromImage(this->m_image);
  ui->label_8->setPixmap(pixmap);
  imageModified(true);
}

void MainWindow::on_pushButton_2_clicked() {
  // 生成RGB565数据
  ui->plainTextEdit->clear();
  int w = this->m_image.width();
  int h = this->m_image.height();
  // 总字节数
  int total = 2 * w * h;
  QFileInfo fileInfo(this->m_filename);
  QString arrayName = fileInfo.baseName();
  QString aLine = QString("const unsigned char RGB565_%1[%2] = {")
                      .arg(arrayName)
                      .arg(total);
  ui->plainTextEdit->appendPlainText(aLine);
  QString onePixel;
  QChar ch0('0');
  int base = 16;
  int count = 0;
  for (int y = 0; y < h; ++y) {
    QApplication::processEvents();
    for (int x = 0; x < w; ++x) {
      QRgb rgb = this->m_image.pixel(x, y);
      quint16 red = qRed(rgb) & 0x00F8;
      quint16 green = qGreen(rgb) & 0x00FC;
      quint16 blue = qBlue(rgb) & 0x00F8;
      quint16 rgb565 = (red << 8) | (green << 3) | (blue >> 3);
      quint8 byteLSB = rgb565 & 0x00FF;  // 低字节
      quint8 byteMSB = rgb565 >> 8;      // 高字节
      if (ui->radioButton->isChecked()) {
        onePixel += QString("0x%1, 0x%2")
                        .arg(byteLSB, 2, base, ch0)
                        .arg(byteMSB, 2, base, ch0);
      } else {
        onePixel += QString("0x%1, 0x%2")
                        .arg(byteMSB, 2, base, ch0)
                        .arg(byteLSB, 2, base, ch0);
      }
      count++;
      // 每行只填8像素数据
      if (count == 8) {
        onePixel = onePixel.toUpper();
        onePixel = onePixel.replace(QChar('X'), 'x');
        ui->plainTextEdit->appendPlainText(onePixel);
        onePixel = "";
        count = 0;
      }
    }
  }
  if (count > 0) {
    onePixel = onePixel.toUpper();
    onePixel = onePixel.replace(QChar('X'), 'x');
    ui->plainTextEdit->appendPlainText(onePixel);
  }
  ui->plainTextEdit->appendPlainText("};");
  ui->tabWidget->setCurrentIndex(1);
  ui->pushButton_3->setEnabled(true);
  QMessageBox::information(this, "提示", "RGB565数据生成已完成");
}

void MainWindow::on_pushButton_3_clicked() {
  QFileInfo fileInfo(this->m_filename);
  QString newName = fileInfo.baseName() + ".h";
  QString filter = "C语言头文件(*.h);;文本文件(*.txt)";
  QString filename =
      QFileDialog::getSaveFileName(this, "保存文件", newName, filter);
  if (filename.isEmpty()) {
    return;
  }
  QFile aFile(filename);
  if (aFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
    QString str = ui->plainTextEdit->toPlainText();
    QByteArray strBytes = str.toUtf8();
    aFile.write(strBytes, strBytes.length());
    aFile.close();
  }
}

void MainWindow::on_actPrintPreview_triggered() {
  // 打印预览
  QPrintPreviewDialog previewDialog(this);
  // 具有最大化按钮
  previewDialog.setWindowFlag(Qt::WindowMaximizeButtonHint);
  if (ui->tabWidget->currentIndex() == 0) {
    connect(&previewDialog, &QPrintPreviewDialog::paintRequested, this,
            &MainWindow::do_paintRequestedImage);
  } else {
    connect(&previewDialog, &QPrintPreviewDialog::paintRequested, this,
            &MainWindow::do_paintRequestedText);
  }
  previewDialog.exec();
}

void MainWindow::on_actPrint_triggered() {
  // 打印
  QPrinter printer;
  QPrintDialog printDialog(&printer, this);
  if (printDialog.exec() == QDialog::Accepted) {
    QPainter painter(&printer);
    if (ui->tabWidget->currentIndex() == 0) {
      printImage(&painter, &printer);
    } else {
      printRGB565Data(&painter, &printer);
    }
  }
}
```

### 十一、自定义插件和库

- 当`Qt Designer`提供的界面组件不满足实际设计需求时，可以从`QWidget`或某个界面组件类继承设计自定义界面组件类。
- 有两种方法可以使用自定义界面组件，一种方法是提升法，另一种是为`Qt Designer`设计自定义`Widget`插件，将其直接安装到`Qt Designer`的组件面板里，其使用方法如同`Qt`自带的界面设计组件。

#### 1、设计和使用自定义界面组件

```c++
#ifndef TBATTERY_H
#define TBATTERY_H

#include <QWidget>

class TBattery : public QWidget {
  Q_OBJECT
  // 自定义属性
  Q_PROPERTY(int powerLevel READ powerLevel WRITE setPowerLevel NOTIFY
                 powerLevelChanged)
  Q_PROPERTY(int warnLevel READ warnLevel WRITE setWarnLevel)
 public:
  explicit TBattery(QWidget *parent = nullptr);
  // 设置当前电量值
  void setPowerLevel(int pow);
  // 返回当前电量值
  int powerLevel();
  // 设置电量低阈值
  void setWarnLevel(int warn);
  // 返回电量低阈值
  int warnLevel();

 private:
  // 背景色
  QColor colorBack = Qt::white;
  // 电池边框颜色
  QColor colorBorder = Qt::black;
  // 电池柱颜色
  QColor colorPower = Qt::green;
  // 电量短缺时的颜色
  QColor colorWarning = Qt::red;
  // 电量值为0-100
  int m_powerLevel = 60;
  // 低电量阈值
  int m_warnLevel = 20;

 protected:
  void paintEvent(QPaintEvent *event) override;

 signals:
  // 自定义信号
  void powerLevelChanged(int);

  // QWidget interface
 public:
  // 重定义的函数, 设置组件的合适大小
  QSize sizeHint() const override;
};

#endif  // TBATTERY_H
```

```c++
#include "tbattery.h"

#include <QPaintEvent>
#include <QPainter>

TBattery::TBattery(QWidget *parent) : QWidget{parent} {}

void TBattery::setPowerLevel(int pow) {
  m_powerLevel = pow;
  emit powerLevelChanged(pow);
  repaint();
}

int TBattery::powerLevel() { return m_powerLevel; }

void TBattery::setWarnLevel(int warn) {
  m_warnLevel = warn;
  repaint();
}

int TBattery::warnLevel() { return m_warnLevel; }

QSize TBattery::sizeHint() const {
  int h = height();
  int w = h * 12 / 5;
  QSize size(w, h);
  return size;
}

void TBattery::paintEvent(QPaintEvent *event) {
  // 绘制界面组件
  QPainter painter(this);
  // 视口矩形区域
  QRect rect(0, 0, width(), height());
  // 设置视口
  painter.setViewport(rect);
  // 设置窗口大小, 逻辑坐标
  painter.setWindow(0, 0, 120, 50);
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setRenderHint(QPainter::TextAntialiasing);
  // 绘制电池边框
  QPen pen(colorBorder);
  pen.setWidth(2);
  pen.setStyle(Qt::SolidLine);
  pen.setCapStyle(Qt::FlatCap);
  pen.setJoinStyle(Qt::BevelJoin);
  painter.setPen(pen);
  QBrush brush(colorBack);
  brush.setStyle(Qt::SolidPattern);
  painter.setBrush(brush);
  rect.setRect(1, 1, 109, 48);
  painter.drawRect(rect);
  brush.setColor(colorBorder);
  painter.setBrush(brush);
  // 电池正极头
  rect.setRect(110, 15, 10, 20);
  painter.drawRect(rect);
  // 画电量柱
  if (m_powerLevel > m_warnLevel) {
    brush.setColor(colorPower);
    pen.setColor(colorPower);
  } else {
    brush.setColor(colorWarning);
    pen.setColor(colorWarning);
  }
  painter.setBrush(brush);
  painter.setPen(pen);
  if (m_powerLevel > 0) {
    rect.setRect(5, 5, m_powerLevel, 40);
    painter.drawRect(rect);
  }
  // 绘制电量百分比文字
  QFontMetrics textSize(this->font());
  QString powStr = QString::asprintf("%d%%", m_powerLevel);
  QRect textRect = textSize.boundingRect(powStr);
  painter.setFont(this->font());
  pen.setColor(colorBorder);
  painter.setPen(pen);
  painter.drawText(55 - textRect.width() / 2, 23 + textRect.height() / 2,
                   powStr);
  event->accept();
}
```

#### 2、设计和使用`Qt Designer Widget`插件

- `Qt`提供了两套用于设计插件的`API`。高级`API`用于设计插件以扩展`Qt`的功能，例如定制数据库驱动、图像格式、文本编码、定制样式等，`Qt`自带的这类插件安装在`Qt Creator`的插件目录下。`Tools\QtCreator\bin\plugins`
- 低级`API`用于创建插件以扩展自行编写应用程序的功能。

#### 3、创建和使用静态库

- 在编译项目时，静态库会被嵌入项目的可执行文件，程序运行时就不在需要单独的静态库文件。
- `Windows`平台上的静态库文件名称后缀是`.lib`，要在项目中编译链接静态库，还需要静态库的`C++`头文件。如果一个库很小，功能比较简单，就适合被编译为静态库，然后直接嵌入可执行文件。
- 动态库不会在编译项目时被嵌入可执行文件，而是在应用程序运行时被链接和使用，所以动态库需要随应用程序一同发布。`Winwods`平台上的动态库文件名后缀是`.dll`，要在项目中使用动态库的函数、类等资源，一般还需要用到动态库对应的`C++`头文件。
- `Qt`中把动态库称为共享库，因为`Qt`是跨平台的，所以可以使用多种类型的编译器。一个`C++`动态库项目，用`MSVC`编译生成的动态库文件名后缀是`.dll`，用`MinGW`编译生成的动态库文件名后缀是`.so`
- 如果一个项目用到一个库，项目的开发套件应该与库的开发套件相同。

##### 创建静态库

```properties
QT += widgets
# 定义项目使用的模板类型, 一般的GUI项目定义为app
TEMPLATE = lib
# 定义项目的一些通用配置, 生成的库文件与使用的编译器有关
# 使用MSVC编译，生成的库文件是.lib
# 使用MinGW，生成的库文件是.a
# 同一个编译器在Release和Debug模式下生成的静态库文件名称是相同的
# 如果GUI项目中会用到静态库, 必须使用与GUI项目相同编译器和编译模式的库文件。
CONFIG += staticlib

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    tpendialog.cpp

HEADERS += \
    tpendialog.h
FORMS += tpendialog.ui

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target
```

#### 4、创建和使用共享库

- 若更新了`.dll`文件版本，只要接口未变，应用程序就可以正常调用动态链接库。

```c++
#ifndef MYSHAREDLIB_GLOBAL_H
#define MYSHAREDLIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(MYSHAREDLIB_LIBRARY)
#define MYSHAREDLIB_EXPORT Q_DECL_EXPORT  // 声明为导出, 共享库中有效
#else
#define MYSHAREDLIB_EXPORT Q_DECL_IMPORT  // 声明为导入, 使用库的项目中有效
#endif

#endif  // MYSHAREDLIB_GLOBAL_H
```

- 共享库里的符号，包括变量、类和函数等，需要声明为导出的公共符号才可以被应用程序使用。所以，共享库要导出的符号前需要加`Q_DECL_EXPORT`宏。
- 而在使用共享库的应用程序中，需要在共享库的头文件里将需要用到的符号声明为导入的，也就是在符号前加`Q_DECL_IMPORT`宏。

##### 使用共享库

- 共享库的调用方式有：隐式链接调用和显式链接调用。
- 隐式链接调用是指在编译项目时有共享库的`.lib`文件（或`.a`文件）和`.h`文件，知道`.dll`文件中有哪些类或函数，编译时就隐式地生成必要的链接信息，使用`.dll`文件中的类或函数时根据头文件中的定义使用即可，应用程序运行时将自动加载`.dll`文件。
- 显示链接调用时只有`.dll`文件，没有`.h`文件和`.lib`文件，因为这个`.dll`文件可能是用其他编程语言生成的。虽然没有`.h`文件，但是我们知道`.dll`文件里的函数原型，那么可以使用`QLibrary`类在应用程序里动态加载`.dll`文件，声明函数原型，并使用`.dll`文件里的函数。这种方式需要在应用程序里声明函数原型，并需要解析`.dll`文件里的函数，一般只用于调用用非`C++`语言编写和生成的比较简单的`.dll`文件。显示链接调用`.dll`文件的应用场景很少。

###### 隐式链接调共享库

- 必须将动态链接库文件`.dll`复制到可执行文件所在的目录下，程序才能正常运行。而且`Debug`和`Release`版本的应用程序必须使用对应版本的动态链接库文件。

### 十二、`Qt Charts`

- `Qt Charts`是一个二维图表模块，可用于绘制各种常见的二维图表。

#### 1、`Qt Charts`模块概述

- 该模块包含一组易于使用的图表组件，它基于图形/视图架构，其核心组件是`QChartView`和`QChart`。`QChartView`的父类是`QGraphicsView`，而`QGraphicsView`是图形/视图架构中的视图组件，所以，`QChartView`是用于显示图表的视图组件。
- 要在项目中使用`Qt Charts`模块，必须在项目的配置文件`.pro`中增加`Qt += charts`
- 在需要使用`Qt Charts`模块中的类的头文件或源程序文件中可以使用如下的包含语句：`#include <QtCharts>`

```c++
void MainWindow::createChart() {
  // 图表视图
  QChartView *chartView = new QChartView(this);
  // 图表, 类似于图形项
  QChart *chart = new QChart();
  chart->setTitle("简单函数曲线");
  chartView->setChart(chart);
  this->setCentralWidget(chartView);
  // 创建曲线序列
  QLineSeries *s0 = new QLineSeries();
  QLineSeries *s1 = new QLineSeries();
  s0->setName("Sin曲线");
  s1->setName("Cos曲线");
  // 序列添加到图标中
  chart->addSeries(s0);
  chart->addSeries(s1);
  // 序列添加数值
  qreal t = 0, y1, y2, intv = 0.1;
  int cnt = 100;
  for (int i = 0; i < cnt; ++i) {
    y1 = qSin(t);
    s0->append(t, y1);
    y2 = qSin(t + 20);
    s1->append(t, y2);
    t += intv;
  }
  // 创建坐标轴
  QValueAxis *axisX = new QValueAxis();
  // 坐标轴范围
  axisX->setRange(0, 10);
  axisX->setTitleText("时间(秒)");
  QValueAxis *axisY = new QValueAxis();
  axisY->setRange(-2, 2);
  axisY->setTitleText("数值");
  // 坐标轴添加到图表和序列中
  chart->addAxis(axisX, Qt::AlignBottom);
  chart->addAxis(axisY, Qt::AlignLeft);
  s0->attachAxis(axisX);
  s0->attachAxis(axisY);
  s1->attachAxis(axisX);
  s1->attachAxis(axisY);
}
```

##### 图表的主要组成部分

- `QChartView`是`QChart`的视图组件，而一个`QChart`图表一般包括序列、坐标轴、图例、图表标题等部分。

###### `QChartView`

- 这是`QChart`的视图组件。在可视化窗口界面时，就是先放置一个`QGraphicsView`组件，然后将其提升为`QChartView`类。

###### `QChart`

- 从`QGraphicsItem`继承而来，它管理在`QChartView`视图组件里绘制图表所需的各种元素。
- 用于绘制一般的笛卡尔坐标系的图表，他的子类`QPolarChart`用于绘制极坐标图。

###### 序列

- 序列是数据的展现方式。图表的类型主要是由序列的类型决定的。
- `QAbstractSeries`
  - `QXYSeries`曲线序列和散点序列
    - `QLineSeries`折现序列 `QSplineSeries`曲线序列
    - `QScatterSeries`散点序列，只显示数据点的序列
  - `QAbstractBarSeries`各种柱状图序列
    - `QBarSeries`
    - `QHorizontalBarSeries`常见的柱状图序列
    - `QStackedBarSeries`
    - `QHorizontalStackedBarSeries`堆叠柱状图序列
    - `QPercentBarSeries`
    - `QHorizontalPercentBarSeries`百分比柱状图序列
  - `QPieSeries`饼图序列
  - `QCandlestickSeries`蜡烛图序列
  - `QBoxPlotSeries`箱线图序列
  - `QAreaSeries`面积图序列

###### 坐标轴

- 各种类型的坐标轴如下：

| 坐标轴类           | 特点           | 用途                                             |
| ------------------ | -------------- | ------------------------------------------------ |
| `QValueAxis`       | 数值坐标轴     | 作为数值型数据的坐标轴                           |
| `QCategoryAxis`    | 分组数值坐标轴 | 可以为数值范围设置文字标签                       |
| `QLogValueAxis`    | 对数坐标轴     | 作为数值型数据的对数坐标轴，可以设置对数的底数   |
| `QBarCategoryAxis` | 类别坐标轴     | 用字符串表示坐标轴的刻度，用于图表的非数值坐标轴 |
| `QDateTimeAxis`    | 日期时间坐标轴 | 作为日期时间数据的坐标轴                         |
| `QColorAxis`       | 颜色坐标轴     | 可以用渐变色表示坐标轴的刻度，还可以添加文字标签 |

###### 图例

- 图例是对图表上展示的序列的示例说明。`QLegend`是封装了图例功能的类，在`QChart`对象中添加序列后会自动生成图例，可以为每个序列设置图例中的文字，可以控制图例显示在图表的上下左右不同位置。

#### 2、通过`QChart`绘制折线图

##### 图表各组成部件的属性设置

###### `QChart`的设置

- 列出设置函数或单独的读取函数，且仅列出函数的返回数据类型，省略输入参数：

<table>
    <tr>
    	<th>分组</th>
        <th>函数</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="9">图表外观</td>
        <td><code>void setTitle()</code></td>
        <td>设置图表标题，其显示在图表上方，支持<code>HTML</code>格式</td>
    </tr>
    <tr>
        <td><code>void setTitleFont()</code></td>
        <td>设置图表标题字体</td>
    </tr>
    <tr>
        <td><code>void setTitleBrush()</code></td>
        <td>设置图表标题画刷</td>
    </tr>
    <tr>
        <td><code>void setTheme()</code></td>
        <td>设置主题，主题为内置的<code>UI</code>设置，定义了图表的配色</td>
    </tr>
    <tr>
        <td><code>void setMargins()</code></td>
        <td>设置绘图区与图表边界的4个边距</td>
    </tr>
    <tr>
        <td><code>void setPlotArea()</code></td>
        <td>设置绘图区矩形</td>
    </tr>
    <tr>
        <td><code>void setAnimationOptions()</code></td>
        <td>设置序列或坐标轴的动画效果</td>
    </tr>
    <tr>
        <td><code>void setAnimationDuration()</code></td>
        <td>设置动画持续时长</td>
    </tr>
    <tr>
        <td><code>QLegend *legend()</code></td>
        <td>返回图表的图例，其是一个<code>QLegend</code>类的对象</td>
    </tr>
    <tr>
    	<td rowspan="7">图表和绘图区的背景</td>
        <td><code>void setBackgroundBrush()</code></td>
        <td>设置图表背景的画刷</td>
    </tr>
    <tr>
        <td><code>void setBackgroundPen()</code></td>
        <td>设置图表背景的画笔</td>
    </tr>
    <tr>
        <td><code>void setBackgroundRoundness()</code></td>
        <td>设置图表背景圆角的直径</td>
    </tr>
    <tr>
        <td><code>void setBackgroundVisible()</code></td>
        <td>设置图表背景是否可见</td>
    </tr>
    <tr>
        <td><code>void setPlotAreaBackgroundBrush()</code></td>
        <td>设置绘图区背景画刷</td>
    </tr>
    <tr>
        <td><code>void setPlotAreaBackgroundPen()</code></td>
        <td>设置绘图区背景画笔</td>
    </tr>
    <tr>
        <td><code>void setPlotAreaBackgroundVisible()</code></td>
        <td>设置绘图区背景是否可见</td>
    </tr>
    <tr>
    	<td rowspan="4">数据序列</td>
        <td><code>void addSeries()</code></td>
        <td>添加序列</td>
    </tr>
    <tr>
        <td><code>QList&lt;QAbstractSeries*&gt; series()</code></td>
        <td>返回图表拥有的序列的列表</td>
    </tr>
    <tr>
        <td><code>void removeSeries()</code></td>
        <td>移除一个序列，但不删除序列对象</td>
    </tr>
    <tr>
        <td><code>void removeAllSeries()</code></td>
        <td>移除图表的所有序列并删除序列对象</td>
    </tr>
    <tr>
    	<td rowspan="4">坐标轴</td>
        <td><code>void addAxis()</code></td>
        <td>为图表的某个方向添加坐标轴</td>
    </tr>
    <tr>
        <td><code>QList&lt;QAbstractAxis*&gt; axes()</code></td>
        <td>返回某个方向的坐标轴列表</td>
    </tr>
    <tr>
        <td><code>void removeAxis()</code></td>
        <td>移除一个坐标轴</td>
    </tr>
    <tr>
        <td><code>void createDefaultAxes()</code></td>
        <td>根据已添加的序列的类型创建默认的坐标轴，已有坐标轴会被删除</td>
    </tr>
</table>QChart::GridAxisAnimations

- `QChart::AnimationOption`的枚举值
  - `QChart::NoAnimation`0，无动画效果
  - `QChart::GridAxisAnimations`1，背景网格有动画效果
  - `QChart::SeriesAnimations`2，序列有动画效果
  - `QChart::AllAnimations`3，背景网格和序列都有动画效果

###### `QLineSeries`序列的设置

- 仅列出函数的返回数据类型，省略输入参数

<table>
    <tr>
    	<th>分组</th>
        <th>函数</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="4">序列整体</td>
        <td><code>void setName()</code></td>
        <td>设置序列的名称，这个名称会显示在图例里，支持<code>HTML</code>格式</td>
    </tr>
    <tr>
        <td><code>QAbstractSeries::SeriesType type() </code></td>
        <td>返回序列的类型</td>
    </tr>
    <tr>
        <td><code>void setUseOpenGL()</code></td>
        <td>设置是否使用<code>OpenGL</code>加速</td>
    </tr>
    <tr>
        <td><code>QChart* chart()</code></td>
        <td>返回序列所属的图表对象</td>
    </tr>
    <tr>
    	<td rowspan="7">序列外观</td>
        <td><code>void setVisible()</code></td>
        <td>设置序列是否可见</td>
    </tr>
    <tr>
        <td><code>bool isVisible()</code></td>
        <td>判断序列是否可见</td>
    </tr>
    <tr>
        <td><code>void show()</code></td>
        <td>显示序列，使序列可见</td>
    </tr>
    <tr>
        <td><code>void hide()</code></td>
        <td>隐藏序列，使序列不可见</td>
    </tr>
    <tr>
        <td><code>void setColor()</code></td>
        <td>设置序列线条的颜色</td>
    </tr>
    <tr>
        <td><code>void setPen()</code></td>
        <td>设置绘制线条的画笔</td>
    </tr>
    <tr>
        <td><code>void setOpacity()</code></td>
        <td>设置序列的透明度</td>
    </tr>
    <tr>
    	<td rowspan="14">数据点</td>
        <td><code>void setBrush()</code></td>
        <td>设置绘制数据点的画刷，主要用于设置数据点的填充颜色</td>
    </tr>
    <tr>
        <td><code>void setMarkerSize()</code></td>
        <td>设置数据点的大小，默认1像素</td>
    </tr>
    <tr>
        <td><code>void setLightMarker()</code></td>
        <td>用一个<code>QImage</code>对象作为数据点的显示标记，便于自定义数据点的形状</td>
    </tr>
    <tr>
        <td><code>void setPointsVisible()</code></td>
        <td>设置数据点是否可见</td>
    </tr>
    <tr>
        <td><code>void pointsVisible()</code></td>
        <td>数据点是否可见</td>
    </tr>
    <tr>
        <td><code>void append()</code></td>
        <td>添加一个数据点到序列中</td>
    </tr>
    <tr>
        <td><code>void insert()</code></td>
        <td>在某个位置插入一个数据点</td>
    </tr>
    <tr>
        <td><code>void replace()</code></td>
        <td>替换某个数据点</td>
    </tr>
    <tr>
        <td><code>void clear()</code></td>
        <td>清除某个数据点</td>
    </tr>
    <tr>
        <td><code>void remove()</code></td>
        <td>移除某个数据点</td>
    </tr>
    <tr>
        <td><code>void removePoints()</code></td>
        <td>从某个位置开始，移除指定个数的数据点</td>
    </tr>
    <tr>
        <td><code>int count()</code></td>
        <td>数据点的个数</td>
    </tr>
    <tr>
        <td><code>QPointF&amp; at()</code></td>
        <td>返回某个位置的数据点</td>
    </tr>
    <tr>
        <td><code>QList&lt;QPointF&gt; points()</code></td>
        <td>返回数据点的列表</td>
    </tr>
    <tr>
    	<td rowspan="11">数据点选择</td>
        <td><code>void setPointSelected()</code></td>
        <td>设置某个序号的数据点的选中状态</td>
    </tr>
    <tr>
        <td><code>bool isPointSelected()</code></td>
        <td>判断某个数据点是否被选中</td>
    </tr>
    <tr>
        <td><code>void toggleSelected()</code></td>
        <td>切换某些数据点的选中状态</td>
    </tr>
    <tr>
        <td><code>void selectAllPoints()</code></td>
        <td>选中所有数据点</td>
    </tr>
    <tr>
        <td><code>void selectPoint()</code></td>
        <td>选中某个数据点，如果设置了数据点可见，处于选中状态的数据点会以<code>setSelectedColor()</code>设置的颜色显示</td>
    </tr>
    <tr>
        <td><code>void selectPoints()</code></td>
        <td>选中某些数据点</td>
    </tr>
    <tr>
        <td><code>void deselectAllPoints()</code></td>
        <td>取消选中所有数据点</td>
    </tr>
    <tr>
        <td><code>void deselectPoint()</code></td>
        <td>取消选中某个数据点</td>
    </tr>
    <tr>
        <td><code>void deselectPoints()</code></td>
        <td>取消选中某些数据点</td>
    </tr>
    <tr>
        <td><code>void setSelectedColor()</code></td>
        <td>设置被选中的数据点的显示颜色，默认与序列的颜色相同</td>
    </tr>
    <tr>
        <td><code>void setSelectedLightMarker()</code></td>
        <td>设置一个<code>QImage</code>对象为被选中数据点的显示标记</td>
    </tr>
    <tr>
    	<td rowspan="5">数据点标签</td>
        <td><code>void setPointLabelsVisible()</code></td>
        <td>设置数据点标签的可见性</td>
    </tr>
    <tr>
        <td><code>void setPointLabelsColor()</code></td>
        <td>设置数据点标签的文字颜色</td>
    </tr>
    <tr>
        <td><code>void setPointLabelsFont()</code></td>
        <td>设置数据点标签的字体</td>
    </tr>
    <tr>
        <td><code>void setPointLabelsFormat()</code></td>
        <td>设置数据点标签的格式</td>
    </tr>
    <tr>
        <td><code>void setPointLabelsClipping()</code></td>
        <td>设置数据点标签的剪裁属性，默认是<code>true</code>，即绘图区外的标签被剪裁掉</td>
    </tr>
    <tr>
    	<td rowspan="3">坐标轴</td>
        <td><code>bool attachAxis()</code></td>
        <td>为序列附加一个坐标轴，需要一个x轴和一个y轴</td>
    </tr>
    <tr>
        <td><code>bool detachAxis()</code></td>
        <td>解除一个附加的坐标轴</td>
    </tr>
    <tr>
        <td><code>QList&lt;QAbstractAxis*&gt; attachedAxes()</code></td>
        <td>返回附加的坐标轴的列表</td>
    </tr>
</table>

- 设置`OpenGL`加速只对`QLineSeries`和`QScatterSeries`序列有效。设置后会在图表的绘图区自动创建一个透明的`QOpenGLWidget`组件，序列在该组件上绘制。
- 使用`OpenGL`加速可以大大提高绘制曲线的速度，但是某些设置是无效的。
- 设置数据点标签格式有两种显示方式：
  - `@xPoint`表示数据点的`x`值
  - `@yPoint`表示数据点的`y`值

###### `QValueAxis`坐标轴的设置

- 仅列出函数的返回数据类型，省略输入参数：

<table>
    <tr>
    	<th>分组</th>
        <th>函数</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="7">坐标轴整体</td>
        <td><code>void setVisible()</code></td>
        <td>设置坐标轴可见性</td>
    </tr>
    <tr>
    	<td><code>Qt::Orientation orientation()</code></td>
        <td>返回坐标轴方向值</td>
    </tr>
    <tr>
    	<td><code>void setMin()</code></td>
        <td>设置坐标轴最小值</td>
    </tr>
    <tr>
    	<td><code>void setMax()</code></td>
        <td>设置坐标轴最大值</td>
    </tr>
    <tr>
    	<td><code>void setRange()</code></td>
        <td>设置坐标轴最小值、最大值表示的范围</td>
    </tr>
    <tr>
    	<td><code>void setReverse()</code></td>
        <td>设置坐标轴是否反向</td>
    </tr>
    <tr>
    	<td><code>void applyNiceNumbers()</code></td>
        <td>自动设置坐标轴范围和刻度个数</td>
    </tr>
    <tr>
    	<td rowspan="4">轴标题</td>
        <td><code>void setTitleVisible()</code></td>
        <td>设置轴标题的可见性</td>
    </tr>
    <tr>
    	<td><code>void setTitleText()</code></td>
        <td>设置轴标题的文字</td>
    </tr>
    <tr>
    	<td><code>void setTitleFont()</code></td>
        <td>设置轴标题的字体</td>
    </tr>
    <tr>
    	<td><code>void setTitleBrush()</code></td>
        <td>设置轴标题的画刷</td>
    </tr>
    <tr>
    	<td rowspan="7">轴刻度标签</td>
        <td><code>void setLabelFormat()</code></td>
        <td>设置轴刻度标签格式</td>
    </tr>
    <tr>
    	<td><code>void setLabelsAngle()</code></td>
        <td>设置轴刻度标签的角度</td>
    </tr>
    <tr>
    	<td><code>void setLabelsBrush()</code></td>
        <td>设置轴刻度标签的画刷</td>
    </tr>
    <tr>
    	<td><code>void setLabelsColor()</code></td>
        <td>设置轴刻度标签的文字颜色</td>
    </tr>
    <tr>
    	<td><code>void setLabelsFont()</code></td>
        <td>设置轴刻度标签的文字字体</td>
    </tr>
    <tr>
    	<td><code>void setLabelsVisible()</code></td>
        <td>设置轴刻度标签文字是否可见</td>
    </tr>
    <tr>
    	<td><code>void setLabelsEditable()</code></td>
        <td>设置轴刻度标签文字是否可以被编辑，如果可以被编辑，用户可以很方便的修改坐标轴的范围。这个函数只适用于<code>QValueAxis</code>和<code>QDateTimeAxis</code></td>
    </tr>
    <tr>
    	<td rowspan="7">轴线和主刻度</td>
        <td><code>void setTickType()</code></td>
        <td>设置主刻度和标签的定位类型，有固定刻度和动态刻度两种模式</td>
    </tr>
    <tr>
    	<td><code>void setTickAnchor()</code></td>
        <td>设置动态刻度的锚点，即动态刻度的起点值</td>
    </tr>
    <tr>
    	<td><code>void setTickInterval()</code></td>
        <td>设置动态刻度的间隔值</td>
    </tr>
    <tr>
    	<td><code>void setTickCount()</code></td>
        <td>设置坐标轴主刻度的个数，固定刻度模式下有效</td>
    </tr>
    <tr>
    	<td><code>void setLineVisible()</code></td>
        <td>设置轴线和主刻度的可见性</td>
    </tr>
    <tr>
    	<td><code>void setLinePen()</code></td>
        <td>设置轴线和主刻度的画笔</td>
    </tr>
    <tr>
    	<td><code>void setLinePenColor()</code></td>
        <td>设置轴线和主刻度的颜色</td>
    </tr>
    <tr>
    	<td rowspan="3">主网格线</td>
        <td><code>void setGridLineColor()</code></td>
        <td>设置网格线的颜色</td>
    </tr>
    <tr>
    	<td><code>void setGridLinePen()</code></td>
        <td>设置网格线的画笔</td>
    </tr>
    <tr>
    	<td><code>void setGridLineVisible()</code></td>
        <td>设置网格线的可见性</td>
    </tr>
    <tr>
    	<td rowspan="4">次刻度和次网格线</td>
        <td><code>void setMinorTickCount()</code></td>
        <td>设置两个主刻度之间的次刻度的个数</td>
    </tr>
    <tr>
    	<td><code>void setMinorGridLineColor()</code></td>
        <td>设置次网格线的颜色</td>
    </tr>
    <tr>
    	<td><code>void setMinorGridLinePen()</code></td>
        <td>设置次网格线的画笔</td>
    </tr>
    <tr>
    	<td><code>void setMinorGridLineVisible()</code></td>
        <td>设置次网格线的可见性</td>
    </tr>
</table>

- `QValueAxis`坐标轴主要包含以下几个组成部分：
  - 坐标轴标题：在坐标轴下方显示的文字，表示坐标轴的名称。
  - 轴线和刻度线
  - 轴标签：在主刻度处显示的数值标签文字
  - 主网格线：在绘图区域与主刻度对应的网格线
  - 次网格线：在绘图区域与次刻度线对应的网格线

#### 3、图表交互操作

##### `QChart`类的功能函数

- `PointF mapToPosition(const QPointF &value, QAbstractSeries *series = nullptr)`
- `QPointF mapToValue(const QPointF &position, QAbstractSeries *series = nullptr)`将屏幕坐标`position`变换为序列的数据点坐标。
- `void zoom(qreal factor)`缩放图表，`factor`值大于1表示放大
- `void zoomIn()`放大两倍
- `void zoomIn(const QRectF &rect)`放大到最大，使得`rect`表示的矩形范围依然能被显示
- `void zoomOut()`缩小到原来的一半
- `void zoomReset()`恢复原始大小
- `void scroll(qreal dx, qreal dy)`移动图表的可视区域
- `void QChartView::setRubberBand(const QChartView::RubberBands &rubberBand)`
  - `QChartView::NoRubberBand`无任何动作，不自动放大
  - `QChartView::VerticalRubberBand`拖动鼠标时，自动绘制一个矩形框，宽度等于整个图的宽度，高度等于鼠标拖动的范围的高度。释放鼠标后，放大显示此矩形框内的内容。
  - `QChartView::HorizontalRubberBand`拖动鼠标时，自动绘制一个矩形框，高度等于整个图的高度，宽度等于鼠标拖动的范围的宽度。释放鼠标时，放大显示此矩形框内的内容。
  - `QChartView::RectangleRubberBand`拖动鼠标时，自动绘制一个矩形框，宽度和高度分别等于鼠标拖动的范围的宽度和高度。释放鼠标后，显示效果与`VerticalRubberBand`模式的基本相同，只是垂直方向放大。
  - `QChartView::ClickThroughRubberBand`这是一个额外的选项，需要与其他选项进行或运算，再作为函数`setRubberBand()`的参数。使用这个选项后，鼠标的`clicked()`信号才会被传递给图表中的序列对象，否则，在自动框选放大模式下，序列接收不到`clicked()`信号。
- `void QGraphicsView::setDragMode(QGraphicsView::DragMode mode)`设置鼠标拖动模式
  - `QGraphicsView::NoDrag`无动作
  - `QGraphicsView::ScrollHandDrag`鼠标光标变成手型，拖动鼠标时会拖动图中的曲线
  - `QGraphicsView::RubberBandDrag`鼠标光标变成十字形，拖动鼠标会自动绘制一个矩形框。

##### `QXYSeries`类的信号

- 对交互式操作比较有用的是如下几个信号：
  - `void clicked(const QPointF &point)`点击了曲线
  - `void doubleClicked(const QPointF &point)`双击了曲线
  - `void hovered(const QPointF &point, bool state)`鼠标光标移入或移出了曲线
  - `void pressed(const QPointF &point)`鼠标光标在曲线上，按下了某个鼠标按键
  - `void released(const QPointF &point)`鼠标光标在曲线上，释放了某个鼠标键。
- 这些信号的参数表示信号被发射时曲线上的数据点坐标，这个数据点不一定是构成曲线的原始数据点，而可以是曲线上的任意点。

##### `QLegendMarker`的使用

- 主要函数如下（省略了`const`关键字）：

| 函数原型                                 | 功能                           |
| ---------------------------------------- | ------------------------------ |
| `void setVisible(bool visible)`          | 设置图例标记的可见性           |
| `void setLabel(QString &label)`          | 设置标签，即图例中的序列的名称 |
| `void setFont(QFont &font)`              | 设置标签的字体                 |
| `QAbstractSeries* series()`              | 返回关联的序列                 |
| `QLegendMarker::LegendMarkerType type()` | 返回图例标记的类型             |

- 图例标记类型：

| 枚举类型取值                                 | 对应的序列类                                                 |
| -------------------------------------------- | ------------------------------------------------------------ |
| `QLegendMarker::LegendMarkerTypeArea`        | `QAreaSeries`                                                |
| `QLegendMarker::LegendMarkerTypeBar`         | `QBarSeries`和`QHorizontalBarSeries`和`QStackedBarSeries`和`QHorizontalStackedBarSeries`和`QPercentBarSeries`和`QHorizontalPercentBarSeries` |
| `QLegendMarker::LegendMarkerTypePie`         | `QPieSeries`                                                 |
| `QLegendMarker::LegendMarkerTypeXY`          | `QLineSeries, QSplineSeries, QScatterSeries`                 |
| `QLegendMarker::LegendMarkerTypeBoxPlot`     | `QBoxPlotSeries`                                             |
| `QLegendMarker::LegendMarkerTypeCandlestick` | `QCandlestickSeries`                                         |

#### 4、饼图和各种柱状图

##### 数据集类`QBarSet`

- 一个柱状图序列有多个`QBarSet`数据集
- 主要函数如下（省略了参数的`const`关键字）：

<table>
    <tr>
    	<th>分组</th>
        <th>函数原型</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="4">标签</td>
        <td><code>void setLabel(QString label)</code></td>
        <td>设置数据集的标签，用作图例显示的文字</td>
    </tr>
    <tr>
    	<td><code>void setLabelBrush(QBrush &amp;brush)</code></td>
        <td>设置标签的画刷</td>
    </tr>
    <tr>
    	<td><code>void setLabelColor(QColor color)</code></td>
        <td>设置标签的文字颜色</td>
    </tr>
    <tr>
    	<td><code>void setLabelFont(QFont &amp;font)</code></td>
        <td>设置标签的字体</td>
    </tr>
    <tr>
    	<td rowspan="4">棒柱的显示</td>
        <td><code>void setBorderColor(QColor color)</code></td>
        <td>设置数据集的棒柱的边框颜色</td>
    </tr>
    <tr>
    	<td><code>void setBrush(QBrush &amp;brush)</code></td>
        <td>设置数据集的棒柱的画刷</td>
    </tr>
    <tr>
    	<td><code>void setColor(QColor color)</code></td>
        <td>设置数据集的棒柱的填充颜色</td>
    </tr>
    <tr>
    	<td><code>void setPen(QPen &amp;pen)</code></td>
        <td>设置数据集的棒柱的边框画笔</td>
    </tr>
    <tr>
    	<td rowspan="7">数据点</td>
        <td><code>void append(qreal value)</code></td>
        <td>添加一个数据点到数据集中</td>
    </tr>
    <tr>
    	<td><code>void insert(int index, qreal value)</code></td>
        <td>在序号index处插入一个数据点到数据集中</td>
    </tr>
    <tr>
    	<td><code>void remove(int index, int count = 1)</code></td>
        <td>从序号index处开始移除count个数据点</td>
    </tr>
    <tr>
    	<td><code>void replace(int index, qreal value)</code></td>
        <td>将序号index处的数据点替换为value</td>
    </tr>
    <tr>
    	<td><code>qreal at(int index)</code></td>
        <td>返回序号为index的数据点</td>
    </tr>
    <tr>
    	<td><code>int count()</code></td>
        <td>返回数据点的个数</td>
    </tr>
    <tr>
    	<td><code>qreal sum()</code></td>
        <td>返回数据集内所有数据点的和</td>
    </tr>
    <tr>
    	<td rowspan="8">棒柱选择</td>
        <td><code>void selectAllBars()</code></td>
        <td>设置数据集的所有棒柱为选中状态</td>
    </tr>
    <tr>
    	<td><code>void selectBar(int index)</code></td>
        <td>将序号为inde的棒柱设置为选中状态</td>
    </tr>
    <tr>
    	<td><code>void setBarSelected(int index, bool selected)</code></td>
        <td>设置序号为inde的棒柱的选中状态</td>
    </tr>
    <tr>
    	<td><code>void toggleSelection(QList&lt;int&gt; &amp;indexes)</code></td>
        <td>将序号列表indexe中的棒柱的选中状态反转</td>
    </tr>
    <tr>
    	<td><code>void selectBars(QList&lt;int&gt; &amp;indexes)</code></td>
        <td>根据序号列表indexe选择多棒柱</td>
    </tr>
    <tr>
    	<td><code>QList&lt;int&gt; selectedBars()</code></td>
        <td>返回被选中的棒柱的序号列表</td>
    </tr>
    <tr>
    	<td><code>void setSelectedColor(QColor &amp;color)</code></td>
        <td>设置选中状态棒柱的颜色</td>
    </tr>
    <tr>
    	<td><code>QColor selectedColor()</code></td>
        <td>返回函数<code>setSeleedColor()</code>设置的颜色</td>
    </tr>
</table>

##### 序列类`QBarSeries`和`QHorizontalBarSeries`

- 柱状图序列类是`QBarSeries`，水平柱状图序列类是`QHorizontalBarSeries`，这两个类的接口函数完全相同。
- 主要接口函数（列出函数原型，省略参数中的`const`）：

<table>
    <tr>
    	<th>分组</th>
        <th>函数原型</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="6">外观</td>
        <td><code>void setBarWidth(qreal width)</code></td>
        <td>设置棒柱的宽度</td>
    </tr>
    <tr>
    	<td><code>void setLabelsVisible(bool visible = true)</code></td>
        <td>设置是否显示棒柱的标签</td>
    </tr>
    <tr>
    	<td><code>void setLabelsFormat(QSTring &amp;format)</code></td>
        <td>设置棒柱的标签的格式，只支持一种格式：<code>@value</code></td>
    </tr>
    <tr>
    	<td><code>void setLabelsAngle(qreal angle)</code></td>
        <td>设置标签显示的角度</td>
    </tr>
    <tr>
    	<td><code>void setLabelsPosition(QAbstractBarSeries::LabelsPosition position)</code></td>
        <td>设置棒柱标签的显示位置，可在棒柱的中间、顶端、底端和顶端的外部显示</td>
    </tr>
    <tr>
    	<td><code>void setLabelsPrecision(int precision)</code></td>
        <td>设置标签显示数字的有效位数</td>
    </tr>
    <tr>
    	<td rowspan="7">数据集</td>
        <td><code>bool append(QBarSet *set)</code></td>
        <td>添加一个QBarSet数据集到序列中</td>
    </tr>
    <tr>
    	<td><code>bool insert(int index, QBarSet* set)</code></td>
        <td>在序号index处插入一个QBarSet数据集到序列中</td>
    </tr>
    <tr>
    	<td><code>bool remove(QBarSet *set)</code></td>
        <td>移除一个数据集，解除所属关系，并删除数据集对象</td>
    </tr>
    <tr>
    	<td><code>bool take(QBarSet *set)</code></td>
        <td>移除一个数据集，但是不删除数据集对象</td>
    </tr>
    <tr>
    	<td><code>void clear()</code></td>
        <td>清除全部数据集，并删除数据集对象</td>
    </tr>
    <tr>
    	<td><code>QList&lt;QBarSet*&gt; barSets()</code></td>
        <td>返回数据集对象的列表</td>
    </tr>
    <tr>
    	<td><code>int count()</code></td>
        <td>返回数据集的个数</td>
    </tr>
</table>

##### 坐标轴类`QBarCategoryAxis`

<table>
    <tr>
        <th>分组</th>
        <th>函数原型</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="9">类别管理</td>
        <td><code>void append(QString &amp;category)</code></td>
        <td>添加一个类别到坐标轴中</td>
    </tr>
    <tr>
        <td><code>void append(QSTringList &amp;categories)</code></td>
        <td>添加一个字符串列表到坐标轴中</td>
    </tr>
    <tr>
        <td><code>void setCategories(QStringList &amp;categories)</code></td>
        <td>设置一个字符串列表作为坐标轴的类别文字</td>
    </tr>
    <tr>
        <td><code>void insert(int index, QString &amp;category)</code></td>
        <td>在序号index处插入一个类别到坐标轴中</td>
    </tr>
    <tr>
        <td><code>void replace(QString &amp;oldCategory, QString &amp;newCategory)</code></td>
        <td>将旧类别替换为新类别</td>
    </tr>
    <tr>
        <td><code>void remove(QString &amp;category)</code></td>
        <td>移除某个类别</td>
    </tr>
    <tr>
        <td><code>void clear()</code></td>
        <td>清除所有类别</td>
    </tr>
    <tr>
        <td><code>QString at(int index)</code></td>
        <td>返回序号为inde的类别文字</td>
    </tr>
    <tr>
        <td><code>int count()</code></td>
        <td>返回类别的个数</td>
    </tr>
    <tr>
    	<td rowspan="5">坐标轴范围</td>
        <td><code>void setMin(QString &amp;min)</code></td>
        <td>设置坐标轴最小类别文字</td>
    </tr>
    <tr>
        <td><code>QString min()</code></td>
        <td>返回坐标轴最小类别文字</td>
    </tr>
    <tr>
        <td><code>void setMax(QString &amp;max)</code></td>
        <td>设置坐标轴最大类别文字</td>
    </tr>
    <tr>
        <td><code>QString max()</code></td>
        <td>返回轴最大类别文字</td>
    </tr>
    <tr>
        <td><code>void setRange(QString &amp;minCategory, QString &amp;maxCategory)</code></td>
        <td>设置坐标轴范围，范围上下限都用类别文字表示</td>
    </tr>
</table>

##### 饼图（`QPieSeries`）

<table>
    <tr>
    	<th>分组</th>
        <th>函数原型</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="10">管理分块</td>
        <td><code>bool append(QPieSlice *slice)</code></td>
        <td>添加一个分块到饼图中</td>
    </tr>
    <tr>
        <td><code>QPieSlice *append(QString &amp;label, qreal value)</code></td>
        <td>根据设置的标签和数值自动创建一个分块，并添加到饼图中</td>
    </tr>
    <tr>
        <td><code>bool insert(int index, QPieSlice *slice)</code></td>
        <td>在序号inde处插入一个分块</td>
    </tr>
    <tr>
        <td><code>bool remove(QPieSlice *slice)</code></td>
        <td>移除并删除一个分块</td>
    </tr>
    <tr>
        <td><code>bool take(QPieSlice *slice)</code></td>
        <td>移除一个分块，但不删除对象</td>
    </tr>
    <tr>
        <td><code>void clear()</code></td>
        <td>清除序列的所有分块</td>
    </tr>
    <tr>
        <td><code>QList&lt;QPieSlice*&gt; slices()</code></td>
        <td>返回序列的所有分块的列表</td>
    </tr>
    <tr>
        <td><code>int count()</code></td>
        <td>返回序列的分块个数</td>
    </tr>
    <tr>
        <td><code>bool isEmpty()</code></td>
        <td>如果序列是空的，返回true，否则返回false</td>
    </tr>
    <tr>
        <td><code>qreal sum()</code></td>
        <td>返回序列各分块的数值的和</td>
    </tr>
    <tr>
    	<td rowspan="3">外观</td>
        <td><code>void setHoleSize(qreal holeSize)</code></td>
        <td>设置饼图中心的空心圆的大小，数值范围是0~1</td>
    </tr>
    <tr>
        <td><code>void setPieSize(qreal relativeSize)</code></td>
        <td>设置饼图占图表矩形区的相对大小，0表示最小，1表示最大</td>
    </tr>
    <tr>
        <td><code>void setLabelsVisible(bool visible = true)</code></td>
        <td>设置分块的标签的可见性</td>
    </tr>
</table>

- `QPieSlice`对应饼图上的一个分块

<table>
    <tr>
        <th>分组</th>
        <th>函数原型</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="3">数据</td>
        <td><code>QPieSeries* series()</code></td>
        <td>返回分块所属的QPieSerie序列对象</td>
    </tr>
    <tr>
        <td><code>void setValue(qreal value)</code></td>
        <td>设置分块的数值，必须是正数</td>
    </tr>
    <tr>
        <td><code>qreal percentage()</code></td>
        <td>返回分块的值在饼图中所占的百分比，数值范围是0~1</td>
    </tr>
    <tr>
    	<td rowspan="5">标签</td>
        <td><code>void setLabelVisible(bool visible = true)</code></td>
        <td>设置标签的可见性</td>
    </tr>
    <tr>
        <td><code>void setLabel(QSTring label)</code></td>
        <td>设置分块的标签文字</td>
    </tr>
    <tr>
        <td><code>void setLabelColor(QColor color)</code></td>
        <td>设置标签的文字颜色</td>
    </tr>
    <tr>
        <td><code>void setLabelFont(QFont &amp;font)</code></td>
        <td>设置标签的字体</td>
    </tr>
    <tr>
        <td><code>void setLabelPosition(QPieSlice::LabelPosition position)</code></td>
        <td>设置标签的位置</td>
    </tr>
    <tr>
    	<td rowspan="6">外观</td>
        <td><code>void setExploded(bool exploded = true)</code></td>
        <td>如果设置为true，分块具有弹出效果</td>
    </tr>
    <tr>
        <td><code>void setPen(QPen &amp;pen)</code></td>
        <td>设置绘制分块的边框的画笔</td>
    </tr>
    <tr>
        <td><code>void setBorderColor(QColor color)</code></td>
        <td>设置分块边框的颜色，是画笔颜色的便捷调用方式</td>
    </tr>
    <tr>
        <td><code>void setBorderWidth(int width)</code></td>
        <td>设置分块边框的线宽，是画笔线宽的便捷调用方式</td>
    </tr>
    <tr>
        <td><code>void setBrush(const QBrush &amp;brush)</code></td>
        <td>设置填充分块的画刷</td>
    </tr>
    <tr>
        <td><code>void setColor(QColor color)</code></td>
        <td>设置分块的填充颜色，是画刷颜色的便捷调用方式</td>
    </tr>
</table>

### 十三、`Qt Data Visualization`

- 用的不多

### 十四、多线程

- `QThread`是实现多线程的核心类，我们一般从`QThread`继承定义自己的线程类。
- `Qt`提供了`QMutex, QWaitCondition, QSemaphore`等多个类用于实现线程同步。
- `Qt`还有一个`Qt Concurrent`模块，其提供一些高级`API`来实现多线程编程，而无需使用互斥量和信号量等基础操作。

#### 1、使用`QThread`创建多线程程序

- 一个`QThread`类的对象管理一个线程。在设计多线程程序的时候，需要从`QThread`继承定义线程类，并重定义`QThread`的虚函数`run()`，在函数`run()`里处理线程的事件循环。
- 主要接口函数如下（省略`const`关键字）：

<table>
    <tr>
    	<th>类型</th>
        <th>函数原型</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="5">共有函数</td>
        <td><code>bool isFinished()</code></td>
        <td>判断线程是否已结束，也就是是否从函数run(退出</td>
    </tr>
    <tr>
        <td><code>bool isRunning()</code></td>
        <td>判断线程是否正在运行</td>
    </tr>
    <tr>
        <td><code>QThread::Priority priority()</code></td>
        <td>返回线程的优先级，优先级用枚举类型QThread::Priority表示</td>
    </tr>
    <tr>
        <td><code>void setPriority(QThread::Priority priority)</code></td>
        <td>设置线程的优先级</td>
    </tr>
    <tr>
        <td><code>bool wait(unsigned long time)</code></td>
        <td>阻塞线程运行，直到线程结束，也就是从函数run()退出，或等待时间超过time毫秒后此函数返回false</td>
    </tr>
    <tr>
    	<td rowspan="4">共有槽函数</td>
        <td><code>void exit(int returnCode = 0)</code></td>
        <td>退出线程的事件循环，设置退出码为returnCode, 其设置为0表示成功退出，设置为其他值表示有错误</td>
    </tr>
    <tr>
        <td><code>void quit()</code></td>
        <td>退出线程的事件循环，并设置退出码为0，quit()等效于exit(0)</td>
    </tr>
    <tr>
        <td><code>void start(QThread::Priority priority = InheritPriority)</code></td>
        <td>设置线程优先级为priority，其内部调用run()开始运行线程，操作系统根据priority参数进行调度</td>
    </tr>
    <tr>
        <td><code>void terminate()</code></td>
        <td>终止线程的运行，但不是立即结束线程，而是等待操作系统结束线程。使用该函数后应调用wait()</td>
    </tr>
    <tr>
    	<td rowspan="2">信号</td>
        <td><code>void finished()</code></td>
        <td>在线程要结束时此信号被发射</td>
    </tr>
    <tr>
        <td><code>void started()</code></td>
        <td>在线程开始运行之前，即函数run()被调用之前此信号被发射</td>
    </tr>
    <tr>
    	<td rowspan="5">静态函数</td>
        <td><code>int idealThreadCount()</code></td>
        <td>返回系统上能运行的线程的理想个数</td>
    </tr>
    <tr>
        <td><code>void yieldCurrentThread()</code></td>
        <td>当前线程让出CPU，使其它可运行的线程占用CPU运行</td>
    </tr>
    <tr>
        <td><code>void msleep(unsigned long msecs)</code></td>
        <td>强制当前线程休眠msecs毫秒</td>
    </tr>
    <tr>
        <td><code>void sleep(unsigned long secs)</code></td>
        <td>强制当前线程休眠secs秒</td>
    </tr>
    <tr>
        <td><code>void usleep(unsigned long usecs)</code></td>
        <td>强制当前线程休眠usecs微秒</td>
    </tr>
    <tr>
    	<td rowspan="2">受保护函数</td>
        <td><code>virtual void run()</code></td>
        <td>start()调用run()开始线程任务的执行，所以在run(函数里实现线程的事件循环</td>
    </tr>
    <tr>
        <td><code>int exec()</code></td>
        <td>进入线程的事件循环，直到运行exit()退出线程</td>
    </tr>
</table>
#### 2、线程同步

##### 基于互斥量的线程同步

- `QMutex`和`QMutexLocker`是基于互斥量的线程同步类。
- `QMutex`定义的实例是互斥量，主要有以下几个函数：
  - `void QMutex::lock()`锁定互斥量，一直等待
  - `void QMutex::unlock()`解锁互斥量
  - `bool QMutex::tryLock()`尝试锁定互斥量，不等待
  - `bool QMutex::tryLock(int timeout)`尝试锁定互斥量，最多等待`timeout`毫秒

###### `QMutexLocker`类

- `QMutex`需要函数`lock()`和`unlock()`配对使用来实现代码片段的保护，在一些逻辑复杂的代码片段或可能发生异常的代码中，配对有可能出错。
- `QMutexLocker`是另一个简化了互斥量处理的类。他的构造函数接受互斥量作为参数并将其锁定，他的析构函数则将此互斥量解锁，所以在`QMutexLocker`实例变量的生存期内的代码片段会得到保护，自动进行互斥量的锁定和解锁。

##### 基于读写锁的线程同步

- 使用互斥量时存在一个问题，即每次只能有一个线程获得互斥量的使用权限。
- 读写锁类`QReadWriteLock`，它是基于读或写的方式进行代码片段锁定的。
  - `void lockForRead()`以只读方式锁定资源，如果有其他线程以写入方式锁定资源，这个函数会被阻塞。
  - `void lockForWrite()`以写入方式锁定资源，如果其他线程以读或写方式锁定资源，这个函数会被阻塞
  - `void unlock()`解锁
  - `bool tryLockForRead()`尝试以只读方式锁定资源，不等待
  - `bool tryLockForRead(int timeout)`尝试以只读方式锁定资源，最多等待`timeout`毫秒
  - `bool tryLockForWrite()`尝试以写入方式锁定资源，不等待
  - `bool tryLockForWrite(int timeout)`尝试以写入方式锁定资源，最多等待`timeout`毫秒
- `QReadLocker`和`QWriteLocker`是`QReadWriteLock`的简便形式，无需对`unlock()`配对使用。

##### 基于条件等待的线程同步

###### `QWaitCondition`原理和功能

- `bool wait(QMutex *lockedMutex, unsigned long time)`释放互斥量，并等待唤醒
- `bool wait(QReadWriteLock *lockedReadWriteLock, unsigned long time)`释放读写锁，并等待唤醒
- `void wakeAll()`唤醒所有处于等待状态的线程，唤醒线程的顺序不确定，由操作系统的调度策略决定
- `void wakeOne()`唤醒一个处于等待状态的线程，唤醒哪个线程不确定，由操作系统的调度策略决定

```c++
#include "tdicethread.h"

#include <QRandomGenerator>
#include <QReadWriteLock>
#include <QWaitCondition>

// 读写锁
QReadWriteLock rwLocker;
// 等待条件
QWaitCondition waiter;
int seq = 0, diceValue = 0;

TDiceThread::TDiceThread(QObject *parent) : QThread(parent) {}

void TDiceThread::run() {
  seq = 0;
  while (1) {
    rwLocker.lockForWrite();
    diceValue = QRandomGenerator::global()->bounded(1, 7);
    seq++;
    rwLocker.unlock();
    waiter.wakeAll();
    msleep(500);
  }
}

void TValueThread::run() {
  while (1) {
    rwLocker.lockForRead();
    // 等待被唤醒
    waiter.wait(&rwLocker);
    emit newValue(seq, diceValue);
    rwLocker.unlock();
  }
}

TValueThread::TValueThread(QObject *parent) : QThread(parent) {}

void TPictureThread::run() {
  while (1) {
    rwLocker.lockForRead();
    waiter.wait(&rwLocker);
    QString filename = QString::asprintf(":/images/d%d.jpg", diceValue);
    emit newPicture(filename);
    rwLocker.unlock();
  }
}

TPictureThread::TPictureThread(QObject *parent) : QThread(parent) {}
```



##### 基于信号量的线程同步

- 信号量是另一种限制对共享资源进行访问的`ITC`技术，它与互斥量相似，但二者有区别。一个互斥量只能被锁定一次，而信号量可以被多次使用。信号量通常用来保护一定数量的相同资源。
- `QSemaphore`是实现信号量功能的类。
  - `QSemaphore(int n = 0)`构造函数，设置资源数为`n`
  - `void acquire(int n = 1)`尝试获得`n`个资源，阻塞等待
  - `int available()`返回当前信号量可以资源的个数
  - `void release(int n = 1)`释放`n`个资源
  - `bool tryAcquire(int n = 1)`尝试获取`n`个资源，不等待
  - `bool tryAcquire(int n, int timeout)`尝试获取`n`个资源，最多等待`timeout`毫秒。

```c++
#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>

class TDaqThread : public QThread {
  Q_OBJECT
 protected:
  bool m_stop = false;
  void run();

 public:
  explicit TDaqThread(QObject *parent = nullptr);
  void stopThread();
};

class TProcessThread : public QThread {
  Q_OBJECT
 protected:
  bool m_stop = false;
  void run();

 public:
  explicit TProcessThread(QObject *parent = nullptr);
  void stopThread();
 signals:
  void dataAvailable(int bufferSeq, int *bufferData, int pointCount);
};

#endif  // MYTHREAD_H
```

```c++
#include "mythread.h"

#include <QSemaphore>
#define BUF_SIZE 10
int buffer1[BUF_SIZE];
int buffer2[BUF_SIZE];
int curBufNum = 1;        // 当前正在写入的缓冲区编号
int bufSeq = 0;           // 缓冲区序号
QSemaphore emptyBufs(2);  // 信号量, 空的缓冲区个数
QSemaphore fullBufs;      // 信号量, 满的缓冲区个数
void TDaqThread::run() {
  curBufNum = 1;
  bufSeq = 0;
  int counter = 0;
  int n = emptyBufs.available();
  if (n < 2) {
    emptyBufs.release(2 - n);
  }
  m_stop = false;
  while (!m_stop) {
    emptyBufs.acquire();
    int *buf = curBufNum == 1 ? buffer1 : buffer2;
    for (int i = 0; i < BUF_SIZE; ++i) {
      *buf = counter;
      buf++;
      counter++;
      msleep(10);
    }
    bufSeq++;
    curBufNum = curBufNum == 1 ? 2 : 1;
    fullBufs.release();
  }
}

TDaqThread::TDaqThread(QObject *parent) : QThread(parent) {}

void TDaqThread::stopThread() { m_stop = true; }

void TProcessThread::run() {
  int n = fullBufs.available();
  if (n > 0) {
    fullBufs.acquire(n);
  }
  int bufData[BUF_SIZE];
  m_stop = false;
  while (!m_stop) {
    if (fullBufs.tryAcquire(1, 50)) {
      int *bufferFull = curBufNum == 1 ? buffer2 : buffer1;
      for (int i = 0; i < BUF_SIZE; ++i, bufferFull++) {
        bufData[i] = *bufferFull;
      }
      emptyBufs.release();
      int pointCount = BUF_SIZE;
      emit dataAvailable(bufSeq, bufData, pointCount);
    }
  }
}

TProcessThread::TProcessThread(QObject *parent) : QThread(parent) {}

void TProcessThread::stopThread() { m_stop = true; }
```

### 十五、网络

- `Qt Network`模块提供了用于编写`TCP/IP`网络应用程序的各种类。
- 要在项目中使用该模块，需要添加：QT       += network

#### 1、主机信息查询

- 使用`QHostInfo`类和`QNetworkInterface`类可以获取主机的一些网络信息。

##### `QHostInfo`类和`QNetworkInterface`类

- `QHostInfo`类可以根据主机名获取主机的`IP`地址，或者通过`IP`地址获取主机名。
- 主要接口函数如下（列出函数原型，省略参数中的`const`关键字）：

<table>
    <tr>
    	<th>类别</th>
        <th>函数原型</th>
        <th>作用</th>
    </tr>
    <tr>
    	<td rowspan="5">公共函数</td>
        <td><code>QList&lt;QHostAddress&gt; addresses()</code></td>
        <td>返回与<code>hostName()</code>对应主机关联的IP地址列表</td>
    </tr>
    <tr>
    	<td><code>HostInfoError error()</code></td>
        <td>如果主机查找失败，返回失败类型</td>
    </tr>
    <tr>
    	<td><code>QString errorString()</code></td>
        <td>如果主机查找失败，返回错误描述字符串</td>
    </tr>
    <tr>
    	<td><code>QString hostName()</code></td>
        <td>返回通过IP地址查找到的主机名</td>
    </tr>
    <tr>
    	<td><code>int lookupId()</code></td>
        <td>返回本次查找到的ID</td>
    </tr>
    <tr>
    	<td rowspan="5">静态函数</td>
        <td><code>void abortHostLookup(int id)</code></td>
        <td>中断主机查找</td>
    </tr>
    <tr>
    	<td><code>QHostInfo fromName(QString &amp;name)</code></td>
        <td>返回指定主机名的IP地址</td>
    </tr>
    <tr>
    	<td><code>QString localDomainName()</code></td>
        <td>返回本机域名系统域名</td>
    </tr>
    <tr>
    	<td><code>QString localHostName()</code></td>
        <td>返回本机主机名</td>
    </tr>
    <tr>
    	<td><code>int lookupHost(QString &amp;name, QObject *receiver, char *member)</code></td>
        <td>以异步方式根据主机名查找主机的IP地址，并返回一个表示本次查找的ID，可用作<code>abortHostLookup()</code>函数的参数</td>
    </tr>
</table>

- `QNetworkInterface`类可以获得运行程序的主机的所有IP地址和网络接口列表。

<table>
    <tr>
    	<th>类别</th>
                <th>函数原型</th>
        <th>作用</th>
    </tr>
    <tr>
    	<td rowspan="6">公共函数</td>
        <td><code>QList&lt;QNetworkAddressEntry&gt; addressEntries()</code></td>
        <td>返回网络接口的IP地址列表，包括子网掩码和广播地址</td>
    </tr>
    <tr>
    	<td><code>QString hardwareAddress()</code></td>
        <td>返回接口的低级硬件地址，以太网里就是MAC地址</td>
    </tr>
    <tr>
    	<td><code>QString humanReadableName()</code></td>
        <td>返回可以读懂的接口名称，如果名称不确定，得到的就是name()函数的返回值</td>
    </tr>
    <tr>
    	<td><code>bool isValid()</code></td>
        <td>如果接口信息有效就返回true</td>
    </tr>
    <tr>
    	<td><code>QString name()</code></td>
        <td>返回网络接口名称</td>
    </tr>
    <tr>
    	<td><code>QNetworkInterface::InterfaceType type()</code></td>
        <td>返回网络接口的类型</td>
    </tr>
    <tr>
    	<td rowspan="2">静态函数</td>
        <td><code>QList&lt;QHostAddress&gt; allAddresses()</code></td>
        <td>返回主机上所有IP地址的列表</td>
    </tr>
    <tr>
    	<td><code>QList&lt;QNetworkInterface&gt; allInterfaces()</td>
        <td>返回主机上所有网络接口的列表</td>
    </tr>
</table>

```c++
#include "mainwindow.h"

#include <QHostInfo>
#include <QNetworkInterface>

#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_pushButton_clicked() {
  ui->plainTextEdit->clear();
  QString hostName = QHostInfo::localHostName();
  ui->plainTextEdit->appendPlainText("本机主机名: " + hostName + "\n");
  QHostInfo hostInfo = QHostInfo::fromName(hostName);
  QList<QHostAddress> addrList = hostInfo.addresses();
  if (addrList.isEmpty()) {
    return;
  }
  for (QHostAddress &host : addrList) {
    bool show = ui->checkBox->isChecked();
    show = show ? (host.protocol() == QAbstractSocket::IPv4Protocol) : true;
    if (show) {
      ui->plainTextEdit->appendPlainText("协议: " +
                                         protocolName(host.protocol()));
      ui->plainTextEdit->appendPlainText("本机IP地址: " + host.toString());
      ui->plainTextEdit->appendPlainText(
          QString("isGlobal() = %1\n").arg(host.isGlobal()));
    }
  }
}

void MainWindow::do_lookedUpHostInfo(const QHostInfo &host) {
  QList<QHostAddress> addrList = host.addresses();
  if (addrList.isEmpty()) {
    return;
  }
  for (QHostAddress &host : addrList) {
    bool show = ui->checkBox->isChecked();
    show = show ? (host.protocol() == QAbstractSocket::IPv4Protocol) : true;
    if (show) {
      ui->plainTextEdit->appendPlainText("协议: " +
                                         protocolName(host.protocol()));
      ui->plainTextEdit->appendPlainText(host.toString());
      ui->plainTextEdit->appendPlainText(
          QString("isGlobal() = %1\n").arg(host.isGlobal()));
    }
  }
}

QString MainWindow::protocolName(
    QAbstractSocket::NetworkLayerProtocol protocol) {
  switch (protocol) {
    case QAbstractSocket::IPv4Protocol:
      return "IPv4";
    case QAbstractSocket::IPv6Protocol:
      return "IPv6";
    case QAbstractSocket::AnyIPProtocol:
      return "Any Internet Protocol";
    default:
      return "Unknown Network Layer Protocol";
  }
}

QString MainWindow::interfaceType(QNetworkInterface::InterfaceType type) {
  switch (type) {
    case QNetworkInterface::Unknown:

      return "Unknown";
    case QNetworkInterface::Loopback:
      return "Loopback";
    case QNetworkInterface::Ethernet:
      return "Ethernet";
    case QNetworkInterface::Wifi:
      return "Wifi";
    default:
      return "Other type";
  }
}

void MainWindow::on_pushButton_2_clicked() {
  ui->plainTextEdit->clear();
  QString hostname = ui->comboBox->currentText();
  ui->plainTextEdit->appendPlainText("正在查找主机信息: " + hostname);
  QHostInfo::lookupHost(hostname, this, SLOT(do_lookedUpHostInfo(QHostInfo)));
}

void MainWindow::on_pushButton_4_clicked() {
  ui->plainTextEdit->clear();
  QList<QNetworkInterface> list = QNetworkInterface::allInterfaces();
  for (QNetworkInterface &interface : list) {
    if (!interface.isValid()) {
      continue;
    }
    ui->plainTextEdit->appendPlainText("设备名称: " +
                                       interface.humanReadableName());
    ui->plainTextEdit->appendPlainText("硬件地址: " +
                                       interface.hardwareAddress());
    ui->plainTextEdit->appendPlainText("接口地址: " +
                                       interfaceType(interface.type()));
    QList<QNetworkAddressEntry> entryList = interface.addressEntries();
    for (QNetworkAddressEntry &entry : entryList) {
      ui->plainTextEdit->appendPlainText("    IP  地址: " +
                                         entry.ip().toString());
      ui->plainTextEdit->appendPlainText("    子网掩码: " +
                                         entry.netmask().toString());
      ui->plainTextEdit->appendPlainText(
          "    广播地址: " + entry.broadcast().toString() + "\n");
    }
  }
}

void MainWindow::on_pushButton_3_clicked() {
  QList<QHostAddress> addrList = QNetworkInterface::allAddresses();
  if (addrList.isEmpty()) {
    return;
  }
  for (QHostAddress &host : addrList) {
    bool show = ui->checkBox->isChecked();
    show = show ? (host.protocol() == QAbstractSocket::IPv4Protocol) : true;
    if (show) {
      ui->plainTextEdit->appendPlainText("协议: " +
                                         protocolName(host.protocol()));
      ui->plainTextEdit->appendPlainText("IP地址: " + host.toString());
      ui->plainTextEdit->appendPlainText(
          QString("isGlobal() = %1\n").arg(host.isGlobal()));
    }
  }
}
```

#### 2、`TCP`通信

- `Qt`提供`QTcpSocket`类和`QTcpServer`类，用于设计`TCP`通信应用程序。

##### `TCP`通信相关的类

- `QTcpServer`的主要接口函数如下（省略输入参数）：

<table>
    <tr>
    	<th>类型</th>
        <th>函数</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="7">公共函数</td>
        <td><code>void close()</code></td>
        <td>关闭服务器，停止网络监听</td>
    </tr>
    <tr>
        <td><code>bool listen()</code></td>
        <td>在设置的IP地址和端口上开始监听，若成功就返回true</td>
    </tr>
    <tr>
        <td><code>bool isListening()</code></td>
        <td>返回true表示服务器处于监听状态</td>
    </tr>
    <tr>
        <td><code>QTcpSocket *nextPendingConnection()</code></td>
        <td>返回下一个等待接入的连接</td>
    </tr>
    <tr>
        <td><code>QHostAddress serverAddress()</code></td>
        <td>如果服务器处于监听状态，就返回服务器地址</td>
    </tr>
    <tr>
        <td><code>quint16 serverPort()</code></td>
        <td>如果服务器处于监听状态，就返回服务器监听端口</td>
    </tr>
    <tr>
        <td><code>bool waitForNewConnection()</code></td>
        <td>以阻塞方式等待新的连接</td>
    </tr>
    <tr>
    	<td rowspan="2">信号</td>
        <td><code>void acceptError()</code></td>
        <td>当接收一个新的连接发生错误时，此信号被发射</td>
    </tr>
    <tr>
        <td><code>void newConnection()</code></td>
        <td>当有新的连接时，此信号被发射</td>
    </tr>
    <tr>
    	<td rowspan="2">保护函数</td>
        <td><code>void incomingConnection()</code></td>
        <td>当有一个新的连接可用时，QTcpServer内部调用此函数，创建一个QTcpSocket对象，将其添加到内部可用新连接列表，然后发射newConnection()信号</td>
    </tr>
    <tr>
        <td><code>void addPendingConnection()</code></td>
        <td>由incomingConnection()调用，将创建的QTcpSocket添加到内部可用新连接列表</td>
    </tr>
</table>

- `QTcpSocket`类提供了`TCP`的接口，可以用它实现标准的网络通信协议，也可以设计自定义协议。
- `QTcpSocket`是从`QIODevice`间接继承的类，因此是一种`I/O`设备类，它具有流数据读写功能。
- `QAbstractSocket`类用于`TCP`通信的主要接口函数如下（省略输入参数）：

<table>
    <tr>
    	<th>类型</th>
        <th>函数</th>
        <th>功能</th>
    </tr>
    <tr>
    	<td rowspan="15">公共函数</td>
        <td><code>void connectToHost()</code></td>
        <td>以异步方式连接到指定IP地址和端口的TCP服务器，连接成功后会发射connected(信号</td>
    </tr>
    <tr>
        <td><code>void disconnectFromHost()</code></td>
        <td>断开socket连接，成功断开后发射disconnected()信号</td>
    </tr>
    <tr>
        <td><code>void close()</code></td>
        <td>关闭socket的I/O设备，会自动调用disconnectFromHost()</td>
    </tr>
    <tr>
        <td><code>bool waitForConnected()</code></td>
        <td>等待建立socket连接，可设置等待时间，默认30秒</td>
    </tr>
    <tr>
        <td><code>bool waitForDisconnected()</code></td>
        <td>等待断开socket连接，可设置等待时间，默认30秒</td>
    </tr>
    <tr>
        <td><code>QHostAddress peerAddress()</code></td>
        <td>在已连接状态下，返回对方socket的地址</td>
    </tr>
    <tr>
        <td><code>QString peerName()</code></td>
        <td>返回connectToHost()连接到的对方的主机名</td>
    </tr>
    <tr>
        <td><code>quint16 peerPort()</code></td>
        <td>在已连接状态下，返回对方socket的端口</td>
    </tr>
    <tr>
        <td><code>qint64 readBufferSize()</code></td>
        <td>返回内部读取缓冲区的大小，这个值决定了read()和readAll()函数能读出的数据的大小</td>
    </tr>
    <tr>
        <td><code>void setReadBufferSize()</code></td>
        <td>设置内部读取缓冲区的大小</td>
    </tr>
    <tr>
        <td><code>qint64 bytesAvailable()</code></td>
        <td>返回需要读取的缓冲区的数据的字节数</td>
    </tr>
    <tr>
        <td><code>bool canReadLine()</code></td>
        <td>如果有一行数据要从socket缓冲区读取，返回true</td>
    </tr>
    <tr>
        <td><code>QAbstractSocket::SocketState state()</code></td>
        <td>返回socket当前的状态</td>
    </tr>
    <tr>
        <td><code>QHostAddress localAddress()</code></td>
        <td>返回本socket的地址</td>
    </tr>
    <tr>
        <td><code>quint16 localPort()</code></td>
        <td>返回被socket的端口</td>
    </tr>
    <tr>
    	<td rowspan="6">信号</td>
        <td><code>void connected()</code></td>
        <td>connectToHost()连接成功后此信号被发射</td>
    </tr>
    <tr>
        <td><code>void disconnected()</code></td>
        <td>socket断开连接后此信号被发射</td>
    </tr>
    <tr>
        <td><code>void errorOccurred()</code></td>
        <td>当socket发生错误时此信号被发射</td>
    </tr>
    <tr>
        <td><code>void hostFound()</code></td>
        <td>调用connectToHost()找到主机后此信号被发射</td>
    </tr>
    <tr>
        <td><code>void stateChanged()</code></td>
        <td>当socket的状态变化时此信号被发射，有参数则其表示socket当前的状态</td>
    </tr>
    <tr>
        <td><code>void readyRead()</code></td>
        <td>当缓冲区有新数据需要读取时此信号被发射，在此信号的槽函数里读取缓冲区的数据。这是父类QIODevice中定义的一个信号</td>
    </tr>
</table>

- 客户端实例

```c++
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QLabel>
#include <QMainWindow>
#include <QTcpSocket>
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
 private slots:
  void do_socketStateChange(QAbstractSocket::SocketState socketState);
  void do_connected();
  void do_disconnected();
  // 读取socket传入的数据
  void do_socketReadyRead();

  void on_actConnect_triggered();

  void on_actClose_triggered();

  void on_pushButton_clicked();

 private:
  Ui::MainWindow *ui;
  QTcpSocket *tcpClient;
  QLabel *labSocketState;
  void getLocalIp();
};
#endif  // MAINWINDOW_H
```

```c++
#include "mainwindow.h"

#include <QHostInfo>

#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  tcpClient = new QTcpSocket(this);
  labSocketState = new QLabel("socket 状态: ");
  labSocketState->setMinimumWidth(250);
  ui->statusbar->addWidget(labSocketState);

  getLocalIp();

  connect(tcpClient, SIGNAL(connected()), this, SLOT(do_connected()));
  connect(tcpClient, SIGNAL(disconnected()), this, SLOT(do_disconnected()));
  connect(tcpClient, SIGNAL(readyRead()), this, SLOT(do_socketReadyRead()));
  connect(tcpClient, &QTcpSocket::stateChanged, this,
          &MainWindow::do_socketStateChange);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::do_socketStateChange(
    QAbstractSocket::SocketState socketState) {
  switch (socketState) {
    case QAbstractSocket::UnconnectedState:
      labSocketState->setText("socket 状态: UnconnectedState");
      break;
    case QAbstractSocket::HostLookupState:
      labSocketState->setText("socket 状态: HostLookupState");
      break;
    case QAbstractSocket::ConnectingState:
      labSocketState->setText("socket 状态: ConnectingState");
      break;
    case QAbstractSocket::ConnectedState:
      labSocketState->setText("socket 状态: ConnectedState");
      break;
    case QAbstractSocket::BoundState:
      labSocketState->setText("socket 状态: BoundState");
      break;
    case QAbstractSocket::ClosingState:
      labSocketState->setText("socket 状态: ClosingState");
      break;
    case QAbstractSocket::ListeningState:
      labSocketState->setText("socket 状态: ListeningState");
      break;
    default:
      break;
  }
}

void MainWindow::do_connected() {
  ui->plainTextEdit->appendPlainText("**已连接到服务器**");
  ui->plainTextEdit->appendPlainText("**peer address: " +
                                     tcpClient->peerAddress().toString());
  ui->plainTextEdit->appendPlainText("**peer port: " +
                                     QString::number(tcpClient->peerPort()));
  ui->actConnect->setEnabled(false);
  ui->actClose->setEnabled(true);
}

void MainWindow::do_disconnected() {
  ui->plainTextEdit->appendPlainText("**已断开和服务器的连接**");
  ui->actConnect->setEnabled(true);
  ui->actClose->setEnabled(false);
}

void MainWindow::do_socketReadyRead() {
  while (tcpClient->canReadLine()) {
    ui->plainTextEdit->appendPlainText("[in] " + tcpClient->readLine());
  }
}

void MainWindow::getLocalIp() {
  QString hostName = QHostInfo::localHostName();
  QHostInfo hostInfo = QHostInfo::fromName(hostName);
  QList<QHostAddress> addrList = hostInfo.addresses();
  if (addrList.isEmpty()) {
    return;
  }
  for (QHostAddress &host : addrList) {
    if (QAbstractSocket::IPv4Protocol == host.protocol()) {
      ui->comboBox->addItem(host.toString());
    }
  }
}

void MainWindow::on_actConnect_triggered() {
  QString addr = ui->comboBox->currentText();
  quint16 port = ui->spinBox->value();
  tcpClient->connectToHost(addr, port);
}

void MainWindow::on_actClose_triggered() {
  if (tcpClient->state() == QAbstractSocket::ConnectedState) {
    tcpClient->disconnectFromHost();
  }
}

void MainWindow::on_pushButton_clicked() {
  QString msg = ui->lineEdit->text();
  ui->plainTextEdit->appendPlainText("[out] " + msg);
  ui->lineEdit->clear();
  ui->lineEdit->setFocus();
  QByteArray str = msg.toUtf8();
  str.append('\n');
  tcpClient->write(str);
}
```

- 服务器实例

```c++
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QLabel>
#include <QMainWindow>
#include <QTcpServer>
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
 private slots:
  void do_newConnection();
  void do_socketStateChange(QAbstractSocket::SocketState socketState);
  void do_clientConnected();
  void do_clientDisconnected();
  // 读取socket传入的数据
  void do_socketReadyRead();

  void on_comboBox_currentTextChanged(const QString &arg1);

  void on_actStart_triggered();

  void on_actStop_triggered();

  void on_pushButton_clicked();

 private:
  Ui::MainWindow *ui;
  // 状态栏标签
  QLabel *labListen;
  QLabel *labSocketState;
  // TCP服务器
  QTcpServer *tcpServer;
  QTcpSocket *tcpSocket = nullptr;
  // 获取本机IP地址
  void getLocalIp();
};
#endif  // MAINWINDOW_H
```

```c++
#include "mainwindow.h"

#include <QHostInfo>
#include <QTcpSocket>

#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  labListen = new QLabel("监听状态:");
  labListen->setMinimumWidth(150);
  ui->statusbar->addWidget(labListen);
  labSocketState = new QLabel("socket状态:");
  labSocketState->setMinimumWidth(200);
  ui->statusbar->addWidget(labSocketState);
  getLocalIp();
  tcpServer = new QTcpServer(this);
  connect(tcpServer, &QTcpServer::newConnection, this,
          &MainWindow::do_newConnection);
}

MainWindow::~MainWindow() {
  if (tcpSocket != nullptr) {
    if (tcpSocket->state() == QAbstractSocket::ConnectedState) {
      tcpSocket->disconnectFromHost();
    }
    if (tcpServer->isListening()) {
      tcpServer->close();
    }
  }
  delete ui;
}

void MainWindow::do_newConnection() {
  tcpSocket = tcpServer->nextPendingConnection();
  connect(tcpSocket, SIGNAL(connected()), this, SLOT(do_clientConnected()));
  do_clientConnected();
  connect(tcpSocket, SIGNAL(disconnected()), this,
          SLOT(do_clientDisconnected()));
  connect(tcpSocket, &QTcpSocket::stateChanged, this,
          &MainWindow::do_socketStateChange);
  do_socketStateChange(tcpSocket->state());
  connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(do_socketReadyRead()));
}

void MainWindow::do_socketStateChange(
    QAbstractSocket::SocketState socketState) {
  switch (socketState) {
    case QAbstractSocket::UnconnectedState:
      labSocketState->setText("socket 状态: UnconnectedState");
      break;
    case QAbstractSocket::HostLookupState:
      labSocketState->setText("socket 状态: HostLookupState");
      break;
    case QAbstractSocket::ConnectingState:
      labSocketState->setText("socket 状态: ConnectingState");
      break;
    case QAbstractSocket::ConnectedState:
      labSocketState->setText("socket 状态: ConnectedState");
      break;
    case QAbstractSocket::BoundState:
      labSocketState->setText("socket 状态: BoundState");
      break;
    case QAbstractSocket::ClosingState:
      labSocketState->setText("socket 状态: ClosingState");
      break;
    case QAbstractSocket::ListeningState:
      labSocketState->setText("socket 状态: ListeningState");
      break;
    default:
      break;
  }
}

void MainWindow::do_clientConnected() {
  ui->plainTextEdit->appendPlainText("**client socket connected**");
  ui->plainTextEdit->appendPlainText("**peer address: " +
                                     tcpSocket->peerAddress().toString());
  ui->plainTextEdit->appendPlainText("peer port: " +
                                     QString::number(tcpSocket->peerPort()));
}

void MainWindow::do_clientDisconnected() {
  ui->plainTextEdit->appendPlainText("**client socket disconnected**");
  tcpSocket->deleteLater();
}

void MainWindow::do_socketReadyRead() {
  while (tcpSocket->canReadLine()) {
    ui->plainTextEdit->appendPlainText("[in] " + tcpSocket->readLine());
  }
}

void MainWindow::getLocalIp() {
  QString hostName = QHostInfo::localHostName();
  QHostInfo hostInfo = QHostInfo::fromName(hostName);
  QList<QHostAddress> addrList = hostInfo.addresses();
  if (addrList.isEmpty()) {
    return;
  }
  for (QHostAddress &host : addrList) {
    if (QAbstractSocket::IPv4Protocol == host.protocol()) {
      ui->comboBox->addItem(host.toString());
    }
  }
}

void MainWindow::on_comboBox_currentTextChanged(const QString &arg1) {
  this->setWindowTitle("服务器程序----绑定的IP地址: " + arg1);
}

void MainWindow::on_actStart_triggered() {
  // 开始监听
  QString ip = ui->comboBox->currentText();
  quint16 port = ui->spinBox->value();
  QHostAddress address(ip);
  tcpServer->listen(address, port);
  ui->plainTextEdit->appendPlainText("**开始监听**");
  ui->plainTextEdit->appendPlainText("**服务器地址: " +
                                     tcpServer->serverAddress().toString());
  ui->plainTextEdit->appendPlainText("**服务器端口: " +
                                     QString::number(tcpServer->serverPort()));
  ui->actStart->setEnabled(false);
  ui->actStop->setEnabled(true);
  labListen->setText("监听状态: 正在监听");
}

void MainWindow::on_actStop_triggered() {
  if (tcpServer->isListening()) {
    if (tcpSocket != nullptr) {
      if (tcpSocket->state() == QAbstractSocket::ConnectedState) {
        tcpSocket->disconnectFromHost();
      }
    }
    tcpServer->close();
    ui->actStart->setEnabled(true);
    ui->actStop->setEnabled(false);
    labListen->setText("监听状态: 已停止监听");
  }
}

void MainWindow::on_pushButton_clicked() {
  QString msg = ui->lineEdit->text();
  ui->plainTextEdit->appendPlainText("[out] " + msg);
  ui->lineEdit->clear();
  ui->lineEdit->setFocus();
  QByteArray str = msg.toUtf8();
  str.append('\n');
  tcpSocket->write(str);
}
```

#### 3、`UDP`通信

- 和`TCP`通信不同，`UDP`通信不区分客户端和服务器端，`UDP`程序都是客户端程序。两个`UDP`客户端之间进行`UDP`通信时，无需预先建立持久的`socket`连接。
- `UDP`客户端每次发送数据都需要指定目标地址和端口。

##### `QUdpSocket`类

- 该类用于实现`UDP`通信，它与`QTcpSocket`具有相同的父类`QAbstractSocket`。
- 这两个类的主要区别是`QUdpSocket`以数据报传输数据，而不是以连续的数据流传输数据。`QUdpSocket::writeDatagram()`函数用于发送数据报，数据报一般少于512字节，每个数据报包含发送者和接收者的IP地址和端口等信息。
- 程序若要实现`UDP`数据接收，需要先用`QUdpSocket::bind()`函数绑定一个端口，用于接收传入的数据报。当有数据报传入时`QUdpSocket`会发射`readyRead()`信号，使用`QUdpSocket::readDatagram()`函数可以读取接收到的数据报。
- `UDP`消息传送有单播、广播和组播三种形式。
- 特有的函数如下（省略输入参数）：

| 函数                           | 功能                                                        |
| ------------------------------ | ----------------------------------------------------------- |
| `bool bind()`                  | 为`UDP`通信绑定一个端口                                     |
| `qint64 writeDatagram()`       | 向目标地址和端口的`UDP`客户端发送数据，返回成功发送的字节数 |
| `bool hasPendingDatagrams()`   | 当至少有一个数据报需要读取时，返回`true`                    |
| `qint64 pendingDatagramSize()` | 返回第一个待读取的数据报的大小                              |
| `qint64 readDatagram()`        | 读取一个数据报，返回成功读取的数据报的字节数                |
| `bool  joinMulticastGroup()`   | 加入一个多播组                                              |
| `bool leaveMulticastGroup()`   | 离开一个多播组                                              |

- 示例

```c++
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QLabel>
#include <QMainWindow>
#include <QUdpSocket>
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
 private slots:
  void do_socketStateChange(QAbstractSocket::SocketState socketState);
  void do_socketReadyRead();

  void on_actBind_triggered();

  void on_actUnbind_triggered();

  void on_pushButton_clicked();

  void on_pushButton_2_clicked();

 private:
  Ui::MainWindow *ui;
  QLabel *labSocketState;
  QUdpSocket *udpSocket;
};
#endif  // MAINWINDOW_H
```

```c++
#include "mainwindow.h"

#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  labSocketState = new QLabel("socket 状态:");
  labSocketState->setMinimumWidth(200);
  ui->statusbar->addWidget(labSocketState);

  udpSocket = new QUdpSocket(this);
  connect(udpSocket, &QUdpSocket::stateChanged, this,
          &MainWindow::do_socketStateChange);
  do_socketStateChange(udpSocket->state());
  connect(udpSocket, SIGNAL(readyRead()), this, SLOT(do_socketReadyRead()));
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::do_socketStateChange(
    QAbstractSocket::SocketState socketState) {
  switch (socketState) {
    case QAbstractSocket::UnconnectedState:
      labSocketState->setText("socket 状态: UnconnectedState");
      break;
    case QAbstractSocket::HostLookupState:
      labSocketState->setText("socket 状态: HostLookupState");
      break;
    case QAbstractSocket::ConnectingState:
      labSocketState->setText("socket 状态: ConnectingState");
      break;
    case QAbstractSocket::ConnectedState:
      labSocketState->setText("socket 状态: ConnectedState");
      break;
    case QAbstractSocket::BoundState:
      labSocketState->setText("socket 状态: BoundState");
      break;
    case QAbstractSocket::ClosingState:
      labSocketState->setText("socket 状态: ClosingState");
      break;
    case QAbstractSocket::ListeningState:
      labSocketState->setText("socket 状态: ListeningState");
      break;
    default:
      break;
  }
}

void MainWindow::do_socketReadyRead() {
  while (udpSocket->hasPendingDatagrams()) {
    QByteArray datagram;
    datagram.resize(udpSocket->pendingDatagramSize());
    QHostAddress peerAddr;
    quint16 peerPort;
    udpSocket->readDatagram(datagram.data(), datagram.size(), &peerAddr,
                            &peerPort);
    QString str = datagram.data();
    QString peer =
        "[From " + peerAddr.toString() + ":" + QString::number(peerPort) + "] ";
    ui->plainTextEdit->appendPlainText(peer + str);
  }
}

void MainWindow::on_actBind_triggered() {
  quint16 port = ui->spinBox->value();
  if (udpSocket->bind(port)) {
    ui->plainTextEdit->appendPlainText("**已成功绑定**");
    ui->plainTextEdit->appendPlainText(
        "**绑定端口: " + QString::number(udpSocket->localPort()) + "**");
    ui->actBind->setEnabled(false);
    ui->actUnbind->setEnabled(true);
    ui->pushButton->setEnabled(true);
    ui->pushButton_2->setEnabled(true);
  } else {
    ui->plainTextEdit->appendPlainText("**绑定失败**");
  }
}

void MainWindow::on_actUnbind_triggered() {
  // 解除绑定, 复位socket
  udpSocket->abort();
  ui->actBind->setEnabled(true);
  ui->actUnbind->setEnabled(false);
  ui->pushButton->setEnabled(false);
  ui->pushButton_2->setEnabled(false);
  ui->plainTextEdit->appendPlainText("**已解除绑定**");
}

void MainWindow::on_pushButton_clicked() {
  QString tIp = ui->comboBox->currentText();
  QHostAddress tAddr(tIp);
  quint16 tPort = ui->spinBox_2->value();
  QString msg = ui->lineEdit->text();
  QByteArray str = msg.toUtf8();
  udpSocket->writeDatagram(str, tAddr, tPort);
  ui->plainTextEdit->appendPlainText("[out] " + msg);
  ui->lineEdit->clear();
  ui->lineEdit->setFocus();
}

void MainWindow::on_pushButton_2_clicked() {
  quint16 tPort = ui->spinBox_2->value();
  QString msg = ui->lineEdit->text();
  QByteArray str = msg.toUtf8();
  qDebug("广播消息");
  udpSocket->writeDatagram(str, QHostAddress::Broadcast, tPort);
  ui->plainTextEdit->appendPlainText("[out] " + msg);
  ui->lineEdit->clear();
  ui->lineEdit->setFocus();
}
```

##### `UDP`组播

- 组播报文的目标地址使用`D`类`IP`地址，D类地址不能出现在`IP`报文的源`IP`地址字段中。
- 关于组播的`IP`地址，有如下一些约定：
  - `224.0.0.0~224.0.0.255`为预留的组播地址（永久组地址），地址`224.00.0`保留不分配，其它地址供路由协议使用
  - `224.0.1.0~224.0.1.255`是公用组播地址，可以用于`Internet`
  - `224.0.2.0~238.255.255.255`为用户可用的组播地址（临时组地址），全网范围内有效
  - `239.0.0.0~239.255.255.255`为本地管理组播地址，仅在特定的本地范围内有效

```c++
#include "mainwindow.h"

#include <QHostInfo>

#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  labSocketState = new QLabel("socket 状态:");
  labSocketState->setMinimumWidth(200);
  ui->statusbar->addWidget(labSocketState);

  getLocalIp();
  udpSocket = new QUdpSocket(this);
  // 设置UDP组播的数据报的生存期, 每跨一个路由该值会减一, 默认为1,
  // 表示只能在同一路由下的局域网内传播
  udpSocket->setSocketOption(QAbstractSocket::MulticastTtlOption, 1);
  connect(udpSocket, &QUdpSocket::stateChanged, this,
          &MainWindow::do_socketStateChange);
  do_socketStateChange(udpSocket->state());
  connect(udpSocket, SIGNAL(readyRead()), this, SLOT(do_socketReadyRead()));
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::do_socketStateChange(
    QAbstractSocket::SocketState socketState) {
  switch (socketState) {
    case QAbstractSocket::UnconnectedState:
      labSocketState->setText("socket 状态: UnconnectedState");
      break;
    case QAbstractSocket::HostLookupState:
      labSocketState->setText("socket 状态: HostLookupState");
      break;
    case QAbstractSocket::ConnectingState:
      labSocketState->setText("socket 状态: ConnectingState");
      break;
    case QAbstractSocket::ConnectedState:
      labSocketState->setText("socket 状态: ConnectedState");
      break;
    case QAbstractSocket::BoundState:
      labSocketState->setText("socket 状态: BoundState");
      break;
    case QAbstractSocket::ClosingState:
      labSocketState->setText("socket 状态: ClosingState");
      break;
    case QAbstractSocket::ListeningState:
      labSocketState->setText("socket 状态: ListeningState");
      break;
    default:
      break;
  }
}

void MainWindow::do_socketReadyRead() {
  while (udpSocket->hasPendingDatagrams()) {
    QByteArray datagram;
    datagram.resize(udpSocket->pendingDatagramSize());
    QHostAddress peerAddr;
    quint16 peerPort;
    udpSocket->readDatagram(datagram.data(), datagram.size(), &peerAddr,
                            &peerPort);
    QString str = datagram.data();
    QString peer =
        "[From " + peerAddr.toString() + ":" + QString::number(peerPort) + "] ";
    ui->plainTextEdit->appendPlainText(peer + str);
  }
}

void MainWindow::getLocalIp() {
  QString hostName = QHostInfo::localHostName();
  QHostInfo hostInfo = QHostInfo::fromName(hostName);
  QList<QHostAddress> addrList = hostInfo.addresses();
  if (addrList.isEmpty()) {
    return;
  }
  for (QHostAddress &host : addrList) {
    if (QAbstractSocket::IPv4Protocol == host.protocol()) {
      ui->comboBox->addItem(host.toString());
    }
  }
}

void MainWindow::on_actJoin_triggered() {
  QString ip = ui->comboBox->currentText();
  groupAddress = QHostAddress(ip);
  quint16 groupPort = ui->spinBox->value();
  // 组播模式时, 第三个参数必须设置为这个值
  if (udpSocket->bind(QHostAddress::AnyIPv4, groupPort,
                      QUdpSocket::ShareAddress)) {
    udpSocket->joinMulticastGroup(groupAddress);
    ui->plainTextEdit->appendPlainText("**加入多播组成功**");
    ui->plainTextEdit->appendPlainText("**组播IP地址: " + ip + "**");
    ui->plainTextEdit->appendPlainText("**绑定端口: " +
                                       QString::number(groupPort));
    ui->actJoin->setEnabled(false);
    ui->actLeave->setEnabled(true);
    ui->comboBox->setEnabled(false);
    ui->spinBox->setEnabled(false);
    ui->pushButton->setEnabled(true);
  } else {
    ui->plainTextEdit->appendPlainText("**绑定端口失败**");
  }
}

void MainWindow::on_actLeave_triggered() {
  udpSocket->leaveMulticastGroup(groupAddress);
  udpSocket->abort();
  ui->actJoin->setEnabled(true);
  ui->actLeave->setEnabled(false);
  ui->comboBox->setEnabled(true);
  ui->spinBox->setEnabled(true);
  ui->pushButton->setEnabled(false);
  ui->plainTextEdit->appendPlainText("**已退出组播, 解除端口绑定**");
}

void MainWindow::on_pushButton_clicked() {
  quint16 groupPort = ui->spinBox->value();
  QString msg = ui->lineEdit->text();
  QByteArray datagram = msg.toUtf8();
  udpSocket->writeDatagram(datagram, groupAddress, groupPort);
  ui->plainTextEdit->appendPlainText("[multicast] " + msg);
  ui->lineEdit->clear();
  ui->lineEdit->setFocus();
}
```

#### 4、基于`HTTP`的网络应用程序

- `QNetworkRequest`类通过`URL`发起网络协议请求，其也保存网络请求信息，目前支持`HTTP`、`FTP`和本地`URL`的下载和上传。
- `QNetworkAccessManager`类用于协调网络操作，在`QNetworkRequest`发起网络请求后，该类负责发送网络请求，已经创建网络响应。
- `QNetworkReply`类表示网络请求的响应。它是`QIODevice`的子类，所以支持流数据读写功能，也支持异步或同步工作模式。

```c++
#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QFile>
#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkReply>
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
 private slots:
  void do_finished();
  void do_readyRead();
  void do_downloadProgress(qint64 bytesRead, qint64 totalBytes);

  void on_pushButton_2_clicked();

  void on_pushButton_clicked();

 private:
  Ui::MainWindow *ui;
  // 网络管理
  QNetworkAccessManager nam;
  // 网络响应
  QNetworkReply *reply;
  // 下载保存的临时文件
  QFile *downloadFile;
};
#endif  // MAINWINDOW_H
```

```c++
#include "mainwindow.h"

#include <QDesktopServices>
#include <QDir>
#include <QMessageBox>

#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::do_finished() {
  QFileInfo fileInfo(downloadFile->fileName());
  downloadFile->close();
  delete downloadFile;
  reply->deleteLater();
  if (ui->checkBox->isChecked()) {
    QDesktopServices::openUrl(QUrl::fromLocalFile(fileInfo.absoluteFilePath()));
    ui->pushButton->setEnabled(true);
  }
}

void MainWindow::do_readyRead() { downloadFile->write(reply->readAll()); }

void MainWindow::do_downloadProgress(qint64 bytesRead, qint64 totalBytes) {
  ui->progressBar->setMaximum(totalBytes);
  ui->progressBar->setValue(bytesRead);
}

void MainWindow::on_pushButton_2_clicked() {
  QString curPath = QDir::currentPath();
  QDir dir(curPath);
  QString sub = "temp";
  dir.mkdir(sub);
  ui->lineEdit_2->setText(curPath + "/" + sub + "/");
}

void MainWindow::on_pushButton_clicked() {
  QString url = ui->lineEdit->text().trimmed();
  if (url.isEmpty()) {
    QMessageBox::information(this, "错误", "请指定要下载的URL");
    return;
  }
  QUrl newUrl = QUrl::fromUserInput(url);
  if (!newUrl.isValid()) {
    QMessageBox::information(
        this, "错误",
        "无效URL: " + url + "\n错误信息: " + newUrl.errorString());
    return;
  }
  QString tempDir = ui->lineEdit_2->text().trimmed();
  if (tempDir.isEmpty()) {
    QMessageBox::information(this, "错误", "请指定保存下载文件的目录");
    return;
  }
  QString fullFileName = tempDir + newUrl.fileName();
  if (QFile::exists(fullFileName)) {
    QFile::remove(fullFileName);
  }
  downloadFile = new QFile(fullFileName);
  if (!downloadFile->open(QIODevice::WriteOnly)) {
    QMessageBox::information(this, "错误", "临时文件打开错误");
    return;
  }
  ui->pushButton->setEnabled(false);
  // 发送网络请求, 创建网络响应
  reply = nam.get(QNetworkRequest(newUrl));
  reply->abort();
  connect(reply, SIGNAL(readyRead()), this, SLOT(do_readyRead()));
  connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this,
          SLOT(do_downloadProgress(qint64, qint64)));
  connect(reply, SIGNAL(finished()), this, SLOT(do_finished()));
}
```

### 十六、多媒体

- `Qt Multimedia`模块为多媒体模块编程提供支持。主要功能包括播放音频和视频文件，通过麦克风录制音频，通过摄像头拍照和录像等。

#### 1、多媒体模块功能概述

- 使用`Qt6`多媒体模块可以实现如下功能：

  - 访问原始音频设备并进行输入或输出
  - 播放低延迟的音频文件，如`WAV`音效文件
  - 播放压缩的音频和视频文件，如`MP3`、`MP4`、`WMV`等格式的文件
  - 录制音频并进行压缩，生成`MP3`、`WMA`等格式的文件
  - 使用摄像头进行预览，拍照和录像
  - 将音频文件解压到内存中用于处理

- `Qt6`多媒体模块包含两个子模块：`Qt Multimedia`模块提供了多媒体编程用到的大部分类；`Qt Multimedia Widgets`模块提供了多媒体编程中用到的界面组件类。

- 需要添加在配置文件中：`multimedia`和`multimediawidgets`

- 使用这两个包含语句可以包含模块中的大部分类：

  - ```c++
    #include <QtMultimedia>
    #include <QtMultimediaWidgets>
    ```

- 主要的`C++`类的功能如下：

| 类名称                 | 功能描述                                                     |
| ---------------------- | ------------------------------------------------------------ |
| `QMediaPlayer`         | 播放音频或视频文件，可以是本地文件或网络上的文件             |
| `QVideoWidget`         | 界面组件类，用于显示视频                                     |
| `QMediaCaptureSession` | 抓取音频或视频的管理器                                       |
| `QCamera`              | 访问连接到系统上的摄像头                                     |
| `QAudioInput`          | 访问连接到系统上的音频输入设备，如麦克风                     |
| `QAudioOutput`         | 访问连接到系统上的音频输出设备，如音箱或耳机                 |
| `QImageCapture`        | 使用摄像头抓取静态图片                                       |
| `QMediaRecorder`       | 在抓取过程中录制音频或视频                                   |
| `QMediaDevices`        | 提供系统中可用的音频输入设备，音频输出设备，视频输入设备的信息 |
| `QMediaFormat`         | 描述音频、视频的编码格式，一级音频、视频的文件格式           |
| `QVideoSink`           | 访问和修改视频中的单帧数据                                   |
| `QAudioSource`         | 通过音频输入设备采集原始音频数据                             |
| `QAudioSink`           | 将原始的音频数据发送到音频输出设备                           |

- 使用`Qt`多媒体模块提供的这些类可以实现各种应用，如下是一些多媒体典型应用功能和用到的类：

| 应用功能                               | 用到的类                                                     |
| -------------------------------------- | ------------------------------------------------------------ |
| 播放音效文件（`WAV`文件）              | `QSoundEffect`                                               |
| 播放编码的`MP3`、`WAV`等格式音频文件   | `QMediaPlayer`                                               |
| 录制音频并保存为`MP3`、`WMA`等格式文件 | `QMediaCaptureSession`、`QAudioInput`、`QMediaRecorder`、`QMediaFormat` |
| 采集原始音频输入数据                   | `QAudioSource`、`QAudioFormat`                               |
| 播放原始音频                           | `QAudioSink`，`QAudioFormat`                                 |
| 播放视频                               | `QMediaPlayer`、`QVideoWidget`、`QGraphicsVideoItem`         |
| 发现音频和视频设备                     | `QMediaDevices`、`QAudioWidget`、`QCameraDevice`             |
| 抓取音频和视频                         | `QMediaCaptureSession`、`QCamera`、`QAudioInput`、`QVideoWidget` |
| 摄像头拍照                             | `QMediaCaptureSession`、`QCamera`、`QImageCapture`           |
| 摄像头录像                             | `QMediaCaptureSession`、`QCamera`、`QMediaRecorder`          |

#### 2、播放音频

- `QMediaPlayer`可以用于播放经过压缩的音频文件
- `QSoundEffect`可以用于播放低延迟音效文件
- 这两个类都可以用于播放本地文件和网络文件

##### `QMediaPlayer`功能概述

- 常用函数如下：

  - `void setAudioOutput(QAudioOutput *output)`设置一个音频输出设备

  - `QAudioOutput *audioOutput() const`返回播放器关联的音频输出设备信息

  - `void setSource(const QUrl &source)`设置播放媒介来源，本地文件或网络文件

  - `QUrl source() const`当前播放的媒介来源

  - ` void setActiveAudioTrack(int index)`设置当前的音频轨道

  - `void setPlaybackRate(qreal rate)`设置播放速度

  - ` void setLoops(int loops)`设置播放的循环次数

  - ` QMediaPlayer::PlaybackState playbackState() const`返回当前播放器状态

  - `QMediaMetaData metaData() const`返回当前媒介的元数据

  - `QMediaPlayer::MediaStatus mediaStatus() const`媒介状态（正在缓冲、已下载等），对于网络媒介比较有用

  - `bool hasAudio() const`当前媒介是否有音频

  - `bool hasVideo() const`当前媒介是否有视频

  - `qint64 duration() const`媒介的持续时间，单位是`ms`

  - `void setPosition(qint64 position)`设置当前的播放位置，单位是`ms`

  - `qint64 position() const`返回当前的播放位置，单位是`ms`

  - ` void play()`开始播放

  - `void pause()`暂停播放
  - `void stop()`停止播放

- 在创建一个`QMediaPlayer`对象后，必须先用函数`setAudioOutput()`设置一个音频输出设备，再用函数`setSource()`设置播放媒介来源，然后就可以用函数`play()`开始播放了。
- 常用信号如下：
  - `void durationChanged(qint64 duration)`媒介的持续时间发生变化
  - `void mediaStatusChanged(QMediaPlayer::MediaStatus status)`媒介状态发生变化
  - `void metaDataChanged()`媒介的元数据发生变化
  - `void playbackStateChanged(QMediaPlayer::PlaybackState newState)`播放器状态发生变化
  - `oid positionChanged(qint64 position)`播放位置发生变化
  - `void sourceChanged(const QUrl &media)`媒介来源发送变化
- 常见枚举常量表示的元数据类型和意义如下（省略前缀`QMediaMetaData`）：

| 枚举常量         | 类型                               | 意义                       |
| ---------------- | ---------------------------------- | -------------------------- |
| `Title`          | `QString`                          | 媒介的标题                 |
| `Author`         | `QStringList`                      | 媒介的作者                 |
| `Description`    | `QString`                          | 对媒介的描述               |
| `FileFormat`     | `QMediaFormat::FileFormat`枚举类型 | 媒介的文件格式             |
| `Duration`       | `qint64`                           | 媒介的持续时间，单位是`ms` |
| `AudioBitRate`   | `int`                              | 音频流的比特率             |
| `AudioCodec`     | `QMediaFormat::AudioCodec`枚举类型 | 音频流的编码格式           |
| `VideoFrameRate` | `qreal`                            | 视频的帧率                 |
| `VideoCodec`     | `QMediaFormat::VideoCodec`枚举类型 | 视频流的编码格式           |
| `AlbumTitle`     | `QString`                          | 专辑标题                   |
| `ThumbnailImage` | `QImage`                           | 嵌入的专辑缩略图           |
| `VideoBitRate`   | `int`                              | 视频的比特率               |

##### `QAudioOutput`

- ` void setDevice(const QAudioDevice &device)`设置一个`QAudioDevice `设备
- `QAudioDevice device() const`返回当前的`QAudioDevice`设备
- `void setMuted(bool muted)`设置是否静音
- `bool isMuted() const`是否静音
- `void setVolume(float volume)`设置音量
- `float volume() const`返回当前音量

##### `QMediaDevices`

- `QAudioDevice defaultAudioInput()`返回默认的音频输入设备信息
- `QAudioDevice defaultAudioOutput()`返回默认的音频输出设备信息
- `QCameraDevice defaultVideoInput()`返回默认的视频输入设备信息

```c++
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtMultimedia>
#include <QtMultimediaWidgets>
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

 private:
  Ui::MainWindow *ui;
  // 播放器
  QMediaPlayer *player;
  // 是否循环播放
  bool loopPlay = true;
  // 总时长, mm:ss字符串
  QString durationTime;
  // 当前播放到的位置
  QString positionTime;
  // 获取item的用户数据
  QUrl getUrlFromItem(QListWidgetItem *item);
  // 事件过滤处理
  bool eventFilter(QObject *watched, QEvent *event);
 private slots:
  // 播放器状态发生变化
  void do_stateChanged(QMediaPlayer::PlaybackState state);
  // 播放源发生变化
  void do_sourceChanged(const QUrl &media);
  // 播放时长发生变化
  void do_durationChanged(qint64 duration);
  // 播放位置发生变化
  void do_positionChanged(qint64 position);
  // 元数据发生变化
  void do_metaDataChanged();
  void on_btnAdd_clicked();
  void on_btnRemove_clicked();
  void on_btnClear_clicked();

  void on_listWidget_doubleClicked(const QModelIndex &index);
  void on_btnPrevious_clicked();
  void on_btnPlay_clicked();
  void on_btnPause_clicked();
  void on_btnStop_clicked();
  void on_doubleSpinBox_valueChanged(double arg1);
  void on_btnLoop_clicked(bool checked);
  void on_sliderPosition_valueChanged(int value);
  void on_btnSound_clicked();
  void on_sliderVolumn_valueChanged(int value);
};
#endif  // MAINWINDOW_H
```

```c++
#include "mainwindow.h"

#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  // 安装事件过滤器
  ui->listWidget->installEventFilter(this);
  // 允许拖放操作
  ui->listWidget->setDragEnabled(true);
  // 列表项可在组件内部被拖放
  ui->listWidget->setDragDropMode(QAbstractItemView::InternalMove);
  player = new QMediaPlayer(this);
  // 音频输出，指向默认的音频输出设备
  QAudioOutput *audioOutput = new QAudioOutput(this);
  player->setAudioOutput(audioOutput);
  connect(player, &QMediaPlayer::positionChanged, this,
          &MainWindow::do_positionChanged);
  connect(player, &QMediaPlayer::durationChanged, this,
          &MainWindow::do_durationChanged);
  connect(player, &QMediaPlayer::sourceChanged, this,
          &MainWindow::do_sourceChanged);
  connect(player, &QMediaPlayer::playbackStateChanged, this,
          &MainWindow::do_stateChanged);
  connect(player, &QMediaPlayer::metaDataChanged, this,
          &MainWindow::do_metaDataChanged);
}

MainWindow::~MainWindow() { delete ui; }

QUrl MainWindow::getUrlFromItem(QListWidgetItem *item) {
  QVariant itemData = item->data(Qt::UserRole);
  QUrl source = itemData.value<QUrl>();
  return source;
}

bool MainWindow::eventFilter(QObject *watched, QEvent *event) {
  if (event->type() != QEvent::KeyPress) {
    return QWidget::eventFilter(watched, event);
  }
  QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
  if (keyEvent->key() != Qt::Key_Delete) {
    return QWidget::eventFilter(watched, event);
  }
  if (watched == ui->listWidget) {
    QListWidgetItem *item =
        ui->listWidget->takeItem(ui->listWidget->currentRow());
    delete item;
  }
  return true;
}

void MainWindow::do_stateChanged(QMediaPlayer::PlaybackState state) {
  ui->btnPlay->setEnabled(state != QMediaPlayer::PlayingState);
  ui->btnPause->setEnabled(state == QMediaPlayer::PlayingState);
  ui->btnStop->setEnabled(state == QMediaPlayer::PlayingState);
  if (loopPlay && (state == QMediaPlayer::StoppedState)) {
    int count = ui->listWidget->count();
    int curRow = ui->listWidget->currentRow();
    curRow++;
    curRow = curRow >= count ? 0 : curRow;
    ui->listWidget->setCurrentRow(curRow);
    player->setSource(getUrlFromItem(ui->listWidget->currentItem()));
    player->play();
  }
}

void MainWindow::do_sourceChanged(const QUrl &media) {
  ui->labCurMedia->setText(media.fileName());
}

void MainWindow::do_durationChanged(qint64 duration) {
  ui->sliderPosition->setMaximum(duration);
  int secs = duration / 1000;
  int mins = secs / 60;
  secs %= 60;
  durationTime = QString::asprintf("%d:%d", mins, secs);
  ui->labRatio->setText(positionTime + "/" + durationTime);
}

void MainWindow::do_positionChanged(qint64 position) {
  if (ui->sliderPosition->isSliderDown()) {
    return;
  }
  ui->sliderPosition->setSliderPosition(position);
  int secs = position / 1000;
  int mins = secs / 60;
  secs %= 60;
  positionTime = QString::asprintf("%d:%d", mins, secs);
  ui->labRatio->setText(positionTime + "/" + durationTime);
}

void MainWindow::do_metaDataChanged() {
  QMediaMetaData metaData = player->metaData();
  QVariant metaImg = metaData.value(QMediaMetaData::ThumbnailImage);
  if (metaImg.isValid()) {
    QImage img = metaImg.value<QImage>();
    QPixmap musicPixmap = QPixmap::fromImage(img);
    if (ui->scrollArea->width() < musicPixmap.width()) {
      ui->labPic->setPixmap(
          musicPixmap.scaledToWidth(ui->scrollArea->width() - 30));
    } else {
      ui->labPic->setPixmap(musicPixmap);
    }
  } else {
    ui->labPic->clear();
  }
}

void MainWindow::on_btnAdd_clicked() {
  QString curPath = QDir::homePath();
  QString dlgTitle = "选择音频文件";
  QString filter = "音频文件(*.mp3 *.wav *.wma);;所有文件(*.*)";
  QStringList fileList =
      QFileDialog::getOpenFileNames(this, dlgTitle, curPath, filter);
  if (fileList.isEmpty()) {
    return;
  }
  for (int i = 0; i < fileList.size(); ++i) {
    QString aFile = fileList[i];
    QFileInfo fileInfo(aFile);
    QListWidgetItem *aItem = new QListWidgetItem(fileInfo.fileName());
    aItem->setIcon(QIcon(":/images/musicFile.png"));
    // 设置用户数据
    aItem->setData(Qt::UserRole, QUrl::fromLocalFile(aFile));
    ui->listWidget->addItem(aItem);
  }
  if (player->playbackState() != QMediaPlayer::PlayingState) {
    ui->listWidget->setCurrentRow(0);
    QUrl source = getUrlFromItem(ui->listWidget->currentItem());
    player->setSource(source);
  }
  player->play();
}

void MainWindow::on_btnRemove_clicked() {
  int index = ui->listWidget->currentRow();
  if (index >= 0) {
    QListWidgetItem *item = ui->listWidget->takeItem(index);
    delete item;
  }
}

void MainWindow::on_btnClear_clicked() {
  loopPlay = false;
  ui->listWidget->clear();
  player->stop();
}

void MainWindow::on_listWidget_doubleClicked(const QModelIndex &index) {
  Q_UNUSED(index);
  loopPlay = false;
  player->setSource(getUrlFromItem(ui->listWidget->currentItem()));
  player->play();
  loopPlay = ui->btnLoop->isChecked();
}

void MainWindow::on_btnPrevious_clicked() {
  int curRow = ui->listWidget->currentRow();
  curRow--;
  curRow = curRow < 0 ? 0 : curRow;
  ui->listWidget->setCurrentRow(curRow);
  loopPlay = false;
  player->setSource(getUrlFromItem(ui->listWidget->currentItem()));
  player->play();
  loopPlay = ui->btnLoop->isChecked();
}

void MainWindow::on_btnPlay_clicked() {
  if (ui->listWidget->currentRow() < 0) {
    ui->listWidget->setCurrentRow(0);
  }
  player->setSource(getUrlFromItem(ui->listWidget->currentItem()));
  loopPlay = ui->btnLoop->isChecked();
  player->play();
}

void MainWindow::on_btnPause_clicked() { player->pause(); }

void MainWindow::on_btnStop_clicked() {
  loopPlay = false;
  player->stop();
}

void MainWindow::on_doubleSpinBox_valueChanged(double arg1) {
  player->setPlaybackRate(arg1);
}

void MainWindow::on_btnLoop_clicked(bool checked) { loopPlay = checked; }

void MainWindow::on_sliderPosition_valueChanged(int value) {
  player->setPosition(value);
}

void MainWindow::on_btnSound_clicked() {
  bool mute = player->audioOutput()->isMuted();
  player->audioOutput()->setMuted(!mute);
  if (mute) {
    ui->btnSound->setIcon(QIcon(":/images/volumn.bmp"));
  } else {
    ui->btnSound->setIcon(QIcon(":/images/mute.bmp"));
  }
}

void MainWindow::on_sliderVolumn_valueChanged(int value) {
  player->audioOutput()->setVolume(value / 100.0);
}
```

#### 3、录制音频

##### `QMediaRecorder`

- 常用函数：

  - `void setAudioBitRate(int bitRate)`设置比特率

  - `void setAudioChannelCount(int channels)`设置通道数

  - `void setAudioSampleRate(int sampleRate)`设置采样频率

  - `void setEncodingMode(QMediaRecorder::EncodingMode mode)`设置编码模式

  - `void setMediaFormat(const QMediaFormat &format)`设置媒介格式

  - `void setMetaData(const QMediaMetaData &metaData)`设置元数据

  - `void setOutputLocation(const QUrl &location)`设置输出文件，可以是本地文件

  - `void setQuality(QMediaRecorder::Quality quality)`设置录制品质

  - `QMediaRecorder::RecorderState recorderState() const`返回`recorder`的当前状态

  - `qint64 duration() const`返回录制已持续的事件

  - ` void pause()`暂停录制

  - ` void record()`开始录制

  - `void stop()`停止录制

- 信号

  - ` void durationChanged(qint64 duration)`录制时间变化时
  - `void recorderStateChanged(QMediaRecorder::RecorderState state)`状态变化时

- `QMediaCaptureSession`是用于管理音频录制和视频录制的类，主要用到下面两个函数：

  - `void setAudioInput(QAudioInput *input)`设置音频输入设备
  - `void setRecorder(QMediaRecorder *recorder)`设置`recorder`

  ```c++
  #ifndef MAINWINDOW_H
  #define MAINWINDOW_H
  
  #include <QMainWindow>
  #include <QtMultimedia>
  #include <QtMultimediaWidgets>
  QT_BEGIN_NAMESPACE
  namespace Ui {
  class MainWindow;
  }
  QT_END_NAMESPACE
  
  class MainWindow : public QMainWindow {
    Q_OBJECT
  
   public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
  
   private:
    Ui::MainWindow *ui;
    // 管理器
    QMediaCaptureSession *session;
    // 用于录音
    QMediaRecorder *recorder;
   private slots:
    void do_stateChanged(QMediaRecorder::RecorderState state);
    // 录制时间变化
    void do_durationChanged(qint64 duration);
    // QWidget interface
    void on_actStart_triggered();
  
    void on_actPause_triggered();
  
    void on_actStop_triggered();
  
    void on_pushButton_clicked();
  
   protected:
    void closeEvent(QCloseEvent *event) override;
  };
  #endif  // MAINWINDOW_H
  ```

  ```c++
  #include "mainwindow.h"
  
  #include "ui_mainwindow.h"
  
  MainWindow::MainWindow(QWidget *parent)
      : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    ui->splitter->setStretchFactor(0, 2);
    ui->splitter->setStretchFactor(2, 3);
    session = new QMediaCaptureSession(this);
    QAudioInput *audioInput = new QAudioInput(this);
    session->setAudioInput(audioInput);
    recorder = new QMediaRecorder(this);
    session->setRecorder(recorder);
    connect(recorder, &QMediaRecorder::recorderStateChanged, this,
            &MainWindow::do_stateChanged);
    connect(recorder, &QMediaRecorder::durationChanged, this,
            &MainWindow::do_durationChanged);
    if (QMediaDevices::defaultAudioInput().isNull()) {
      ui->groupBox->setTitle("录音设置（无设备）");
      ui->actStart->setEnabled(false);
      QMessageBox::information(this, "提示", "无音频输入设备");
      return;
    }
    for (QAudioDevice &device : QMediaDevices::audioInputs()) {
      ui->comboBox->addItem(device.description(), QVariant::fromValue(device));
    }
    // 默认的格式对象
    QMediaFormat format;
    for (QMediaFormat::AudioCodec encoder :
         format.supportedAudioCodecs(QMediaFormat::Encode)) {
      ui->comboBox_2->addItem(QMediaFormat::audioCodecDescription(encoder),
                              QVariant::fromValue(encoder));
    }
    // 支持的文件格式
    for (QMediaFormat::FileFormat fileFormat :
         format.supportedFileFormats(QMediaFormat::Encode)) {
      ui->comboBox_3->addItem(QMediaFormat::fileFormatDescription(fileFormat),
                              QVariant::fromValue(fileFormat));
    }
    // 采样频率
    int minSampRate = audioInput->device().minimumSampleRate();
    ui->comboBox_4->addItem(QString("Minimum %1").arg(minSampRate), minSampRate);
    int maxSampRate = audioInput->device().maximumSampleRate();
    ui->comboBox_4->addItem(QString("Maximum %1").arg(maxSampRate), maxSampRate);
    ui->comboBox_4->addItem("16000", 16000);
    ui->comboBox_4->addItem("44100", 44100);
    ui->comboBox_4->addItem("48000", 48000);
    ui->comboBox_4->addItem("88200", 88200);
    // 通道数
    int minChan = audioInput->device().minimumChannelCount();
    ui->comboBox_5->addItem(QString("Minimum %1").arg(minChan), minChan);
    int maxChan = audioInput->device().maximumChannelCount();
    ui->comboBox_5->addItem(QString("Maximum %1").arg(maxChan), maxChan);
    ui->comboBox_5->addItem("1", 1);
    ui->comboBox_5->addItem("2", 2);
    // 固定品质
    ui->horizontalSlider->setRange(0, int(QImageCapture::VeryHighQuality));
    ui->horizontalSlider->setValue(int(QImageCapture::NormalQuality));
    // 固定比特率
    ui->comboBox_6->addItem("32000");
    ui->comboBox_6->addItem("64000");
    ui->comboBox_6->addItem("96000");
    ui->comboBox_6->addItem("128000");
  }
  
  MainWindow::~MainWindow() { delete ui; }
  
  void MainWindow::do_stateChanged(QMediaRecorder::RecorderState state) {
    bool isRecording = state == QMediaRecorder::RecordingState;
    ui->actStart->setEnabled(!isRecording);
    ui->actPause->setEnabled(isRecording);
    ui->actStop->setEnabled(isRecording);
    ui->pushButton->setEnabled(state == QMediaRecorder::StoppedState);
  }
  
  void MainWindow::do_durationChanged(qint64 duration) {
    ui->label->setText(QString("已录制 %1 秒").arg(duration / 1000));
  }
  
  void MainWindow::closeEvent(QCloseEvent *event) {
    if (recorder->recorderState() != QMediaRecorder::StoppedState) {
      QMessageBox::information(this, "提示", "正在录音, 不能退出");
      event->ignore();
    } else {
      event->accept();
    }
  }
  
  void MainWindow::on_actStart_triggered() {
    if (recorder->recorderState() == QMediaRecorder::PausedState) {
      recorder->record();
      return;
    }
    QString selectedFile = ui->lineEdit->text().trimmed();
    if (selectedFile.isEmpty()) {
      QMessageBox::critical(this, "错误", "请先设置录音输出文件");
      return;
    }
    if (QFile::exists(selectedFile)) {
      QFile::remove(selectedFile);
    }
    recorder->setOutputLocation(QUrl::fromLocalFile(selectedFile));
    session->audioInput()->setDevice(QMediaDevices::defaultAudioInput());
    QMediaFormat mediaFormat;
    QVariant var = ui->comboBox_3->itemData(ui->comboBox_3->currentIndex());
    QMediaFormat::FileFormat fileFormat = var.value<QMediaFormat::FileFormat>();
    mediaFormat.setFileFormat(fileFormat);
  
    var = ui->comboBox_2->itemData(ui->comboBox_2->currentIndex());
    QMediaFormat::AudioCodec audioCodec = var.value<QMediaFormat::AudioCodec>();
    mediaFormat.setAudioCodec(audioCodec);
    recorder->setMediaFormat(mediaFormat);
  
    var = ui->comboBox_4->itemData(ui->comboBox_4->currentIndex());
    recorder->setAudioSampleRate(var.toInt());
    var = ui->comboBox_5->itemData(ui->comboBox_5->currentIndex());
    recorder->setAudioChannelCount(var.toInt());
    recorder->setAudioBitRate(ui->comboBox_6->currentText().toInt());
    recorder->setQuality(QMediaRecorder::Quality(ui->horizontalSlider->value()));
    if (ui->radioButton->isChecked()) {
      recorder->setEncodingMode(QMediaRecorder::ConstantQualityEncoding);
    } else {
      recorder->setEncodingMode(QMediaRecorder::ConstantBitRateEncoding);
    }
    recorder->record();
  }
  
  void MainWindow::on_actPause_triggered() { recorder->pause(); }
  
  void MainWindow::on_actStop_triggered() { recorder->stop(); }
  
  void MainWindow::on_pushButton_clicked() {
    QString curPath = QDir::currentPath();
    QString dlgTitle = "选择输出文件";
    QString filter = "所有文件(*.*);;MP3文件(*.mp3);;WMA文件(*.wma)";
    QString selectedFile =
        QFileDialog::getSaveFileName(this, dlgTitle, curPath, filter);
    if (!selectedFile.isEmpty()) {
      ui->lineEdit->setText(selectedFile);
      QFileInfo fileInfo(selectedFile);
      QDir::setCurrent(fileInfo.absolutePath());
    }
  }
  ```

#### 4、采集和播放原始音频数据

- `Qt`多媒体模块提供了两种方法来实现音频录制：高层次方法和低层次方法。
- 使用`QAudioSource`类录制音频是低层次方法。设置采样频率和采样点格式后就可以通过麦克风采集原始音频数据，不会进行编码压缩。
- 使用该类采集的原始音频数据一般保存为后缀`.raw`的文件。使用`QAudioSink`类可以将这种原始音频数据文件发送到音频输出设备上，能实现这种原始音频数据文件的播放。
- 使用场景例如：语音识别

##### `QAudioSource`类和`QAudioSink`类

###### `QAudioSource`类

- `QAudioSource(const QAudioDevice &audioDevice, const QAudioFormat &format = QAudioFormat(), QObject *parent = nullptr)`参数`audioDevice`表示音频输入设备，`format`表示采集音频的格式设置，包括采样频率、通道数、采样点数据格式等的设置。
- 在构造函数里设置音频格式后，就不能再通过`QAudioSource`的接口函数修改这些设置。
  - ` void setSampleRate(int samplerate)`设置采样频率，单位是`Hz`
  - `void setChannelCount(int channels)`设置音频通道数，一般是1或2
  - `void setChannelConfig(QAudioFormat::ChannelConfig config)`设置通道配置模式
  - `void setSampleFormat(QAudioFormat::SampleFormat format)`设置采样格式
- 采样格式`QAudioFormat::SampleFormat`有以下几种：
  - `QAudioFormat::Unknown`，0，表示未设置格式
  - `QAudioFormat::UInt8`，1，一个采样点是一个`quint8`类型的数，长度1字节
  - `QAudioFormat::Int16`，2，一个采样点是一个`qint16`类型的数，长度2字节
  - `QAudioFormat::Int32`，3，一个采样点是一个`qint32`类型的数，长度4字节
  - `QAudioFormat::Float`，4，一个采样点是一个`float`类型的数，长度4字节
- 其它主要接口函数：
  - `void setBufferSize(qsizetype value)`设置采集缓冲区大小，单位是字节
  - `void setVolume(qreal volume)`设置输入音量，值范围0~1
  - `QAudioFormat format() const`返回在构造函数里设置的音频格式，不能再修改
  - `void start(QIODevice *device)`开始采集音频数据，数据自动写入`I/O`设备`device`
  - `void stop()`停止采集
  - `void suspend()`挂起采集，挂起后可用`resume()`函数恢复
  - `void resume()`恢复采集
  - `void reset()`清空缓冲区内的数据，缓冲区内的数据全部变为0
  - `QtAudio::State state() const`返回采集器的状态
  - `qint64 elapsedUSecs() const`返回上次调用`start()`函数后流逝的事件，包括空闲和挂起的时间，单位是微秒
  - `qint64 processedUSecs() const`返回上次调用`start()`函数后处理数据的时间，单位是微秒
- 停止音频数据采集后，还需要关闭关联的`I/O`设备

###### `QAudioSink`类

- 在构造函数里需要设置输出音频的格式，且设置格式后就不能再修改。还可以指定一个音频输出设备。
- `start()`函数用于开始输出音频数据。

#### 5、播放视频文件

- `QMediaPlayer`类与播放视频相关的几个接口函数定义如下：
  - `void setVideoOutput(QObject *)`设置用于显示视频的界面组件
  - `QObject *videoOutput() const`返回显示视频的界面组件
  - `void setActiveSubtitleTrack(int index)`设置当前的字幕轨道
  - `int activeSubtitleTrack() const`当前的字幕轨道
- 要使用`QMediaPlayer`播放视频，必须用函数`setVideoOutput()`设置用于显示视频的界面组件，有`QVideoWidget`和`QGraphicsVideoItem`两种显示视频的组件。
- 还需要用函数`setAudioOutput()`设置音频输出轨道，否则播放视频时会没有声音

#### 6、摄像头的使用

##### 摄像头控制概述

- 可以为`QMediaCaptureSession`类对象设置一个摄像头作为视频输入设备，然后就可以通过摄像头拍照和录像。

###### 表示摄像头设备的类`QCameraDevice`

- 摄像头属于视频输入设备，摄像头的信息用`QCameraDevice`类封装：
  - `QCameraDevice QMediaDevices::defaultVideoInput()`返回系统默认摄像头的信息
  - `QList<QCameraDevice> QMediaDevices::videoInputs()`返回系统里的摄像头列表
- 有如下一些接口可用来表示摄像头的信息：
  - `QString description() const`摄像头的文字描述
  - `QByteArray id() const`表示摄像头唯一性的`ID`
  - `bool isDefault() const`是不是系统默认摄像头
  - `bool isNull() const`设备是否有效
  - `QCameraDevice::Position position() const`摄像头的位置，如手机的前置摄像头位置和后置摄像头位置
  - `QList<QSize> photoResolutions() const`支持的拍照分辨率列表
  - `QList<QCameraFormat> videoFormats() const`支持的视频格式列表

###### 摄像头控制接口类`QCamera`

- 可以控制摄像头的调焦、曝光补偿、色温调节等功能，前提是摄像头支持这些特性。

###### 通过摄像头拍照的类`QImageCapture`

- 可以设置拍摄图片的分辨率、保存文件的格式、编码的质量等。
- 常用函数如下：
  - `void setFileFormat(QImageCapture::FileFormat format)`设置拍照保存文件格式，默认是`JPG`
  - `void setQuality(QImageCapture::Quality quality)`设置图片编码质量，分5个等级
  - `void setResolution(const QSize &resolution)`设置图片分辨率，不能超过摄像头的分辨率
  - `bool isReadyForCapture() const`摄像头是否准备好可进行拍照
  - ` int capture()`拍摄图片
  - ` int captureToFile(const QString &file = QString())`拍摄图片，并将其保存到文件中
- 常用信号如下：
  - `void imageCaptured(int id, const QImage &preview)`图片被抓取了
  - ` void imageSaved(int id, const QString &fileName)`图片被保存了
  - ` void readyForCaptureChanged(bool ready)`：`isReadyForCapture()`的值变化了

###### 通过摄像头录像的类`QMediaRecorder`

- 录像时需要设置视频的分辨率、帧率、比特率、编码品质、媒介格式等。
- 用于录像的主要接口函数如下：
  - `void setVideoResolution(const QSize &size)`设置视频的分辨率
  - `void setVideoFrameRate(qreal frameRate)`设置视频的帧率
  - `void setVideoBitRate(int bitRate)`设置视频的比特率
  - ` void setQuality(QMediaRecorder::Quality quality)`设置编码品质，有五个等级
  - `void setMediaFormat(const QMediaFormat &format)`设置媒介格式
  - `void setEncodingMode(QMediaRecorder::EncodingMode mode)`设置编码模式，固定品质或固定比特率
  - `QMediaRecorder::RecorderState recorderState() const`返回`recorder`当前状态

###### `QMediaCaptureSession`类的作用

- 是用于控制音频和视频抓取的类，它的一些接口函数用于设置音频输入设备和视频输入设备，还可以将摄像头预览视频输出到一个视频输出组件上。
- 主要接口函数如下：
  - `void setAudioInput(QAudioInput *input)`设置一个音频输入设备，用于录音
  - `void setAudioOutput(QAudioOutput *output)`设置一个音频输出设备，用于回放音频
  - `void setCamera(QCamera *camera)`设置一个`QCamera`对象作为视频输入设备
  - `void setImageCapture(QImageCapture *imageCapture)`设置一个`QImageCapture`对象，用于拍照
  - `void setRecorder(QMediaRecorder *recorder)`设置一个`QMediaRecorder`对象，用于录音或录像
  - `void setVideoOutput(QObject *output)`设置一个视频输出组件，用于接收摄像头预览视频。

### 十七、其它工具软件和技术

#### 1、多语言界面

##### 基本步骤

- ①在设计程序时，代码中用户可见的字符串都用函数`tr()`封装，以便`Qt`提取界面字符串用于生成翻译资源文件。
- ②在项目配置文件`.pro`中设置需要导出的翻译文件`.ts`名称，使用工具软件`lupdate`扫描项目所有文件中需要翻译的字符串，生成翻译文件。
- ③使用`Qt`的工具软件`Linguist`打开生成的翻译文件，将程序中的字符串翻译为需要的语言版本，例如将所有中文字符串翻译为英文字符串。
- ④使用工具软件`lrelease`编译翻译好的翻译文件，生成更为紧凑的`.qm`文件
- 在应用程序中用`QTranslator`加载不同的`.qm`文件，实现不同的语言界面。

##### `tr()`函数的使用

- 为了让`Qt`能自动提取代码中用户可见的字符串，每个字符串都需要用函数`tr()`封装。

- `tr()`是`QObject`的一个静态函数，在插入了`Q_OBJECT`宏的类或`QObject`的子类中，可以直接使用`tr()`函数，否则需要使用静态函数`QObject::tr()`进行调用。

- 如果一个类不是`QObject`的子类，可以在类定义的最上方位置插入宏`Q_DECLARE_TR_FUNCTIONS`，这样也可以在这个类中直接使用`tr()`函数。示例：

  - ```c++
    class TMyClass{
        Q_DECLARE_TR_FUNCTIONS(TMyClass)
    };
    ```

- 静态函数`QObject::tr()`的原型定义如下：

  - `QString tr(const char *sourceText, const char *disambiguation = nullptr, int n = -1)`
  - 其中`sourceText`是源字符串，`disambiguation`是为翻译者提供额外信息的字符串，用于对一些容易混淆的内容进行说明。

- 使用`tr()`函数时需要注意以下一些事项：

  - ①尽量使用字符串常量，不要使用字符串变量。
  - ②使用字符串变量时需要用`QT_TR_NOOP`宏进行标记（例如`QT_TR_NOOP("武汉")`）。但是`QStringList`的内容初始化时可以直接使用`tr()`函数。
  - ③函数`tr()`中不能使用拼接的动态字符串。正确用法是：`tr("第 %1 行").arg(1)`
  - ④`QT_NO_CAST_FROM_ASCII`的作用。在一个需要将字符串翻译为多语言字符串的应用程序中，如果编写程序时忘了对某个字符串使用`tr()`函数，`lupdate`生成的翻译资源文件就会遗漏这个字符串。要避免这种问题，可以在项目配置文件（`.pro`）中添加以下定义：`DEFINES += QT_NO_CAST_FROM_ASCII`这样构建项目时，编译器会禁止从`const char*`到`QString`的隐式转换，强制每个字符串都必须使用`tr()`或`QLatin1String()`封装，避免出现遗漏未翻译的字符串的情况。

##### 示例

- 在项目配置文件中加入以下语句，文件名称任意：

```properties
TRANSLATIONS = samp17_1_zh.ts\
                samp17_1_en.ts
```

- 使用`lupdate`生成或更新翻译文件
- 使用`linguist`翻译`.ts`文件
- 使用`lrelease`生成`qm`文件
- 将`qm`文件复制到项目编译后的可执行文件所在的目录下
- 在`main.cpp`中创建一个全局的`QTranslator`对象，用于加载`qm`文件，创建翻译器后，还需要给应用程序安装翻译器：

```c++
#include <QApplication>
#include <QSettings>
#include <QTranslator>

#include "mainwindow.h"
QTranslator trans;
int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  QApplication::setOrganizationName("ZQ");
  QApplication::setApplicationName("SAMP17_1");
  QSettings settings;
  QString curLang = settings.value("Language", "ZH").toString();
  bool suss = false;
  suss = trans.load(curLang == "EN" ? "samp18_1_en.qm" : "samp18_1_zh.qm");
  if (suss) {
    a.installTranslator(&trans);
  }
  MainWindow w;
  w.show();
  return a.exec();
}
```

- 一个应用程序可以多次调用`installTranslator()`函数安装多个翻译器，但是只有最后安装的翻译器是当前使用的翻译器。
- 动态切换语言：

```c++
  if (trans.load("samp18_1_zh.qm")) {
    ui->retranslateUi(this);
    labInfo->setText(tr("字体名称"));
  }
```

#### 2、`Qt`样式表

- `QSS`是用于定制`UI`显示效果的强有力工具。使用`QSS`可以定义界面组件的样式，从而使应用程序的界面呈现特殊的显示效果。

##### `QSS`

- 进行窗体可视化设计时，设置样式表后立刻就可以显示效果。
- `QSS`的句法与`HTML`的`CSS`几乎完全相同。一条样式规则由一个选择器和一些声明组成。
- `QSS`支持`CSS 2`中定义的所有选择器，下表展示常用的选择器：

| 选择器         | 例子                        | 用途                                                         |
| -------------- | --------------------------- | ------------------------------------------------------------ |
| 通用选择器     | `*`                         | 所有组件                                                     |
| 类型选择器     | `QPushButton`               | 所有`QPushButton`类及其子类组件                              |
| 属性选择器     | `QPushButton[flat="false"]` | 所有`flat`属性为`false`的`QPushButton`类及其子类的组件       |
| 非子类选择器   | `.QPushButton`              | 所有`QPushButton`类的组件，但是不包括他们的子类              |
| `ID`选择器     | `QPushButton#btnOk`         | 对象名称为`btnOk`的`QPushButton`实例                         |
| 从属对象选择器 | `QDialog QPushButton`       | 所有从属于`QDialog`的`QPushButton`类的实例，即`QDialog`对话框里的所有`QPushButton`按钮 |
| 子对象选择器   | `QDialog>QPushButton`       | 所有直接从属于`QDialog`的`QPushButton`类的实例               |

- 在`Qt`中，可以使用`QObject::setProperty()`为一个界面组件设置一个动态属性。

##### 子控件

- 对于一些组合的界面组件，需要对其子控件进行选择。通过选择器的子控件可以对这些界面元素进行显示效果控制，例如：`QComboBox::drop-down{image: url(:/images/down.bmp)}`
- `QSS`中常用的子控件如下：

| 子控件名称         | 说明                                                         |
| ------------------ | ------------------------------------------------------------ |
| `::branch`         | `QTreeView`的分支指示器                                      |
| `::chunk`          | `QProgressBar`的进度显示块                                   |
| `::close-button`   | `QDockWidget`或`QTabBar`页面的关闭按钮                       |
| `::down-arrow`     | `QComboBox`、`QHeaderView`(排序指示器)、`QScrollBar`或`QSpinBox`的下拉箭头 |
| `::down-button`    | `QScrollBar`或`QSpinBox`的向下按钮                           |
| `::drop-down`      | `QComboBox`的下拉按钮                                        |
| `::float-button`   | `QDockWidget`的浮动按钮                                      |
| `::groove`         | `QSlider`的凹槽                                              |
| `::indicator`      | `QAbstractItemView`、`QCheckBox`、`QRadioButton`、可勾选的`QMenu`菜单项，或可勾选的`QGroupBox`的指示器 |
| `::handle`         | `QScrollBar`、`QSplider`或`QSplitter`的滑块                  |
| `::icon`           | `QAbstractItemView`或`QMenu`的图标                           |
| `::item`           | `QAbstractItemView`、`QMenuBar`、`QMenu`或`QStatusBar`的一个项 |
| `::left-arrow`     | `QScrollBar`的向左箭头                                       |
| `::menu-arrow`     | 具有下拉菜单的`QToolButton`的下拉箭头                        |
| `::menu-button`    | `QToolButton`的菜单按钮                                      |
| `::menu-indicator` | `QPushButton`的菜单指示器                                    |
| `::right-arrow`    | `QMenu`或`QScrollBar`的向右箭头                              |
| `::pane`           | `QTabWidget`的面板                                           |
| `::scroller`       | `QMenu`或`QTabBar`的卷轴                                     |
| `::section`        | `QHeaderView`的分段                                          |
| `::separator`      | `QMenu`或`QMainWindow`的分隔器                               |
| `::tab`            | `QTabBar`或`QToolBox`的分页                                  |
| `::tab-bar`        | `QTabWidget`的分页条。这个子控件只用于控制`QTabBar`在`QTabWidget`中的位置，定义分页的样式使用`::tab`子控件 |
| `::text`           | `QAbstractItemView`的文字                                    |
| `::title`          | `QGroupBox`或`QDockWidget`的标题                             |
| `::up-arrow`       | `QHeaderView`、`QScrollBar`或`QSpinBox`的向上箭头            |
| `::up-button`      | `QSpinBox`的向上按钮                                         |

##### 伪状态

- 选择器可以包含伪状态，使得样式规则只能应用于界面组件的某个状态，这就是一种条件应用规则。
- 伪状态出现在选择器的后面，用一个冒号隔开。
- 可以对伪状态取反，方法是在伪状态前面加一个感叹号。
- 伪状态可以串联使用，相当于逻辑与运算。
- 伪状态可以并联使用，相当于逻辑或运算。
- 子控件也可以使用伪状态。
- 常见的伪状态如下：

| 伪状态          | 描述                                                         |
| --------------- | ------------------------------------------------------------ |
| `:active`       | 当组件处于一个活动的窗口中时，此状态为真                     |
| `:adjoins-item` | `QTreeView::branch`与一个条目相邻时，此状态为真              |
| `:alternate`    | 当`QAbstractItemView`的`alternatingRowColors`属性为`true`，绘制交替的行时此状态为真 |
| `:bottom`       | 组件处于底部，例如`QTabBar`的表头位于底部                    |
| `:checked`      | 组件被勾选，例如`QAbstractButton`的`checked`属性为`true`     |
| `:closable`     | 组件可以被关闭，例如当`QDockWidget`的`DockWidgetClosable`属性为`true`时 |
| `:closed`       | 项`(item)`处于关闭状态，例如`QTreeView`的一个没有展开的节点  |
| `:default`      | 项是默认的，例如一个默认的`QPushButton`按钮，或`QMenu`中的一个默认的`Action` |
| `:disabled`     | 项被禁用                                                     |
| `:editable`     | `QComboxBox`是可编辑的                                       |
| `:edit-focus`   | 项有编辑焦点                                                 |
| `:enabled`      | 项被使能                                                     |
| `:exclusive`    | 项是一个排他性组的一部分，例如一个排他性`QActionGroup`的一个菜单项 |
| `:first`        | 第一个项，例如`QTabBar`中的第一页                            |
| `:flat`         | 项是`flat`的，例如当`QPushButton`的`flat`属性设置为`true`时  |
| `:focus`        | 项具有输入焦点                                               |
| `:has-children` | 项有子项，例如`QTreeView`的一个节点具有子节点                |
| `:horizontal`   | 项处于水平方向                                               |
| `:hover`        | 鼠标移动到项上方时                                           |
| `:last`         | 最后一项，例如`QTabBar`中的最后一页                          |
| `:left`         | 项位于左侧，例如`QTabBar`的页头位于左侧                      |
| `:maximized`    | 项处于最大化状态，例如最大化的`QMdiSubWindow`窗口            |
| `:minimized`    | 项处于最小化状态，例如最小化的`QMdiSubWindow`窗口            |
| `:movable`      | 项是可移动的                                                 |
| `:off`          | 对于可以切换状态的项，其处于`off`状态                        |
| `:on`           | 对于可以切换状态的项，其处于`on`状态                         |
| `:open`         | 项处于打开状态，例如`QTreeView`的一个展开的节点              |
| `:pressed`      | 在项上按下了鼠标                                             |
| `:read-only`    | 项是只读或不可编辑的                                         |
| `:right`        | 项位于右侧，例如`QTabBar`的页头位于右侧                      |
| `:selected`     | 项被选中，例如`QTabBar`中的一个被选中的页，或`QMenu`中一个被选中的菜单项 |
| `:top`          | 项位于顶端，例如`QTabBar`的页头位于顶端                      |
| `:unchecked`    | 项处于被选中状态                                             |
| `:vertical`     | 项处于垂直方向                                               |

##### 属性

- `QSS`对每一个选择器可以定义多条样式规则，每条规则是一个“属性:值”对，可以通过`Qt`的帮助文档`Qt Style Sheets Reference`来查看所有属性的详细说明。

##### 样式表的使用

###### 在程序中使用样式表

- 使用样式表的方法一是在使用`Qt Designer`设计界面时，直接使用样式表编辑器为窗口或窗口上的组件设计样式表，这样设计的样式表会保存在窗口的`UI`文件里，创建窗口时会被自动应用。
- 第二种是使用函数`setStyleSheet()`设置样式表，使用`QApplication::setStyleSheet()`函数可以为应用程序设置全局的样式表，使用`QWidget::setStyleSheet()`可以为一个窗口或一个界面组件设置样式。
- 为了实现界面主题效果的切换，一般将样式表保存为后缀是`.qss`的纯文本文件，然后在程序中打开文件，读取文本内容，再调用函数`setStyleSheet()`应用样式。

###### 样式定义的明确性

- 当多条样式规则对一个属性定义了不同值时就会出现冲突。规则应用于更明确的组件。
- 具有伪状态的选择器被认为比没有伪状态的选择器更明确。
- 如果两个选择器具有相同的明确性，则以规则出现的先后顺序为依据，后出现的规则起作用。
- 具有父子关系的两个类作为选择器时，具有相同的明确性。

###### 样式定义的级联性

- 任何一个组件的样式是其父组件、父窗口和`qApp`的样式的融合。当出现冲突时，组件会使用离自己最近的样式定义，即按顺序使用组件自己的样式定义，或父组件的样式定义、或父窗口的样式定义、或`qApp`的样式定义，而不考虑样式选择器的明确性。

#### 3、`Qt`应用程序的发布和安装

- 提取了`Qt`运行库文件后，将应用程序和运行库直接复制到用户计算机上就可以运行。如果要稍微正式一点，可以制作一个安装文件。
- 如果在安装`Qt`时选择了安装`Qt Installer Framework`，也可以使用`Qt`自带的这个工具软件制作应用程序的安装文件。

##### `Windows`平台上的`Qt`应用程序发布

- `windeployqt.exe`是`Qt`自带的`Windows`平台发布工具，他可以为`Qt`应用程序复制其运行所需要的各种库文件、插件和翻译文件，生成可发布的文件或目录。
- 在`Windows`的命令提示符窗口中使用该程序，其句法如下：`windeployqt [options] [files]`
  - 其中`options`是一些选项，`files`是需要	生成发布文件的应用程序文件名。
- `options`常用的一些选项如下：

| 选项                        | 意义                                        |
| --------------------------- | ------------------------------------------- |
| `--release`                 | 发布`Release`版本的二进制文件               |
| `--no-quick-import`         | 忽略`Qt Quick`的相关库                      |
| `--translations<languages>` | 需要发布的语言列表，用逗号分隔，例如`en,fr` |
| `--no-translations`         | 忽略翻译相关的文件                          |
| `--no-virtualkeyboard`      | 忽略虚拟键盘相关的文件                      |
| `--no-compiler-runtime`     | 忽略编译器的运行时文件                      |
| `--no-opengl-sw`            | 忽略`OpenGL`软件渲染                        |
| `--no-system-d3d-compiler`  | 忽略`D3D`编译器                             |

- 示例：`D:\qt\Qt6.8\6.8.0\mingw_64\bin\windeployqt6.exe --release --no-quick-import --no-translations --no-virtualkeyboard --no-compiler-runtime samp17_2.exe`

##### 制作安装文件

###### `Qt Installer Framework`功能简介

- 是`Qt`提供的一个制作安装文件的工具，在`Tools`目录下
- `QIF`可以为任何需要发布的内容制作安装文件，而不仅是为`Qt`开发的应用软件制作安装文件。
- `QIF`具有和`Qt`一样的跨平台功能，使用`QIF`设计好一个安装项目后，可以在不同平台上编译生成安装文件。
- `QIF`具有如下功能和特点：
  - 使用`QIF`可以制作离线安装文件，也可以制作在线安装文件。
  - 安装的内容可以划分为多个模块和层级，并且可以设置依赖性，安装过程中可以选择安装模块。
  - 安装向导的定制性很强，可以使用自定义`UI`文件，可以通过脚本程序添加交互操作功能。
  - 可以用多语言开发工具`lupdate`和`lrelease`生成翻译文件，使安装向导具有本地化语言界面。
  - 用`QIF`生成的安装文件在安装时，会自动安装一个工具软件`maintenancetool.exe`，运行它可以添加、移除、更新或完全卸载软件。

###### 示例

①创建一个空目录

②在该目录中创建一个`qmake`文件（即`.pro`），名称任意

③在该目录中创建目录`config`和`packages`，这两个目录名称固定，不能修改。`config`目录中一般只有一个文件`config.xml`，包含整个安装程序的一些配置内容。`packages`目录中是需要安装的组件，组件就是可选的安装内容，每个组件是一个目录。每个组件的目录下面各有两个固定名称的目录：`data`和`meta`

```properties
#项目模板类型
TEMPLATE = aux
#生成的安装文件名称
INSTALLER = MusicPlayer_installer

INPUT = $$PWD/config/config.xml &&PWD/packages
demo.input = INPUT
demo.output = $$INSTALLER
demo.commands = D:\qt\Qt6.8\Tools\QtInstallerFramework\4.8\bin\binarycreator -c $$PWD/config/config.xml -p $$PWD/packages ${QMAKE_FILE_OUT}
demo.CONFIG += target_predeps no_link combine
QMAKE_EXTRA_COMPILERS += demo
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Installer>
    <Name>Music Player</Name>
    <Version>1.0.0</Version>
    <Title>Music Player Create By Qt 6.8</Title>
    <Publisher>huayunliufeng</Publisher>
    <StartMenuDir>Qt6 Samples</StartMenuDir>
    <TargetDir>@HomeDir@/Qt6Samples</TargetDir>
    <CreateLocalRepository>true</CreateLocalRepository>
    <WizardStyle>Aero</WizardStyle>
    <WizardShowPageList>true</WizardShowPageList>
    <WizardDefaultWidth>650</WizardDefaultWidth>
    <WizardDefaultHeight>430</WizardDefaultHeight>
</Installer>
```

- `Name`定义了应用程序的名称
- `Title`定义了安装向导的标题
- `StartMenuDir`定义了在开始菜单中创建的目录名称
- `TargetDir`定义了初始的安装目录，其中`@HomeDir@`表示使用`QIF`中的预定义变量`HomeDir`，也就是用户主目录。常用的预定义变量如下：

| 预定义变量名      | 意义                                                         |
| ----------------- | ------------------------------------------------------------ |
| `ProductName`     | 产品名称，就是文件`config.xml`中定义的`<Name>`               |
| `Title`           | 安装程序名称，就是文件`config.xml`中定义的`<Title>`          |
| `Publisher`       | 安装文件的发布者，就是文件`config.xml`中定义的`<Publisher>`  |
| `StartMenuDir`    | 开始菜单中的目录名称，只在`Windows`系统中有效                |
| `TargetDir`       | 用户最终设置的安装目录                                       |
| `DesktopDir`      | 用户桌面的目录名称，只在`Windows`系统中有效                  |
| `HomeDir`         | 当前用户的主目录                                             |
| `ApplicationsDir` | 系统的应用程序目录，例如`Windows`系统中是`C:\Program Files`，`Linux`系统中是`/opt` |

- `CreateLocalRepository`设置是否要建立本地仓库，若设置为`true`，则安装完成后，用户可以运行维护工具来添加或删除组件。
- `WizardStyle`设置安装向导样式，有`Modern`、`Mac`、`Aero`和`Classic`几种样式，默认是`Aero`
- `WizardShowPageList`设置是否显示窗口左侧的向导页面列表，默认是显示的。
- `WizardDefaultWidth`设置向导窗口默认宽度
- `WizardDefaultHeight`设置向导窗口默认高度。

###### 配置组件

- `packages`下是需要安装的组件，每个组件是一个目录，每个组件下有`data`和`meta`两个固定名称的目录，`data`目录是需要安装到用户计算机上的文件，`meta`目录里是对组件进行配置的文件
- 将发布的和应用程序正常运行相关的所有文件和目录打包，复制到`data`中，注意，必须压缩为`7z`格式。安装时，压缩文件会被自动解压到安装目录中。
- 一个组件的`meta`目录下必须有一个文件`package.xml`，这是组件的配置文件。`license.txt`是安装过程中显示的许可协议内容，文件`installscript.qs`是一个`Qt`脚本文件，用于为安装后的文件`MusicPlayer.exe`创建开始菜单快捷方式和桌面快捷方式。

```xml
<?xml version="1.0"?>
<Package>
    <DisplayName>Music Player</DisplayName>
    <Description>Music Player应用程序</Description>
    <Version>1.0.0</Version>
    <ReleaseDate>2024-02-12</ReleaseDate>
    <Licenses>
        <License name="GNU Piblic License Agreement" file="license.txt"/>
    </Licenses>
    <ForcedInstallation>true</ForcedInstallation>
    <Script>installscript.qs</Script>
</Package>
```

- `DisplayName`是组件列表里的组件名称
- `Description`是组件描述
- `Licenses`定义许可协议
- `ForcedInstallation`定义是否强制安装该组件，强制安装的组件前面不会出现复选框。
- `Script`定义组件要使用的脚本文件
- `Default`定义这个组件是默认安装的

```typescript
function Component(){
    // default constructor
}

Component.prototype.createOperations = function(){
    component.createOperations();
    if(systemInfo.productType === "windows"){
        component.addOperation("CreateShortcut", "@TargetDir@\\MusicPlayer.exe",
                               "@StartMenuDir@\\Music Player.lnk",
                               "workingDirectory=@TargetDir@");
        component.addOperation("CreateShortcut", "@TargetDir@\\MusicPlayer.exe",
                               "@DesktopDir@\\Music Player.lnk",
                               "workingDirectory=@TargetDir@");
    }
}
```

- 这个脚本创建了开始菜单快捷方式和桌面快捷方式。
- 第一个参数表示操作名称，第二个参数表示源文件，第三个参数表示快捷方式的名称，第四个参数表示程序工作路径
