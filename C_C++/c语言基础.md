### 一、程序结构

#### 1、c程序主要包含以下部分

- 预处理指令
- 函数
- 变量
- 语句&表达式
- 注释

#### 2、使用`CLion`调试

- 在`GDB中`输入`disas`即可查看AT&T汇编指令。也可以改成Intel格式的汇编，输入`set disassembly-flavor intel`并回车，再输入`disas`并回车。

#### 3、编译器、链接器

- 编译器：将源代码翻译为目标平台代码
- 链接器：将目标平台代码和预编译的库代码、启动代码合并为可执行文件
- `gcc`命令常见用法
  - 编译单个源文件：`gcc xxx.c -o output`
  - 编译多个源文件：`gcc xxx1.c xxx2.c -o output`
  - `-g`生成调试信息
  - `-O`，指定编译器优化级别，使用`-O`后跟级别（如`-O1`, `-O2`, `-O3`）
  - `-l`，链接静态库（`.a`结尾）或动态库（`.so`结尾）
  - `-I`指定头文件的搜索路径。
  - `-L`指定库文件的搜索路径
  - `-std`可激活某些特性，例如`-std=c11`，使其支持`c11`特性，`-std=c99`等。
- 使用`g++`可以编译`C++`程序

#### 4、标识符

- 长度最大63、外部标识符最长31

#### 5、函数必须先声明再使用

- 也叫函数原型
- 除非被调用函数在调用函数之前，但是`main()`函数最好在所有函数定义前面，另外，通常把函数放在其它文件中，所以前置声明必不可少。

#### 6、关键字介绍

- `short`是`short int`的简写，其余例如`long int`类似
- `L`或`l`后缀表示`long`类型，`LL`或`ll`表示`long long`类型，`ull`或`ULL`或`llu`或`LLU`表示`unsigned long long`类型
- `f`或`F`后缀表示`float`，普通小数后加`l`或`L`表示`long double`，默认是`double`
- `unsigned `后面默认是`int`
- `C99`新的浮点类型常量格式：用十六进制表示浮点类型常量，即在十六进制数前加上十六进制前缀（`0x`或`0X`），用`p`和`P`分别代替`e`和`E`，用2的幂代替10的幂（即，p计数法），示例：`0xa.1fp10`，十进制为：`12310364.000000`，其中的`f`表示数字15，不是代表这是一个`float`类型

#### 7、格式转换

- `%d`，十进制整数，`%i`和其相同
- `%o`，八进制
- `%x`，十六进制
- `%X`，大写十六进制
- `%s`，字符串
- `%p`，指针
- `%g`，根据值的不同，自动选择`%f`或`%e`，`%e`格式用于指数小于`-4`或大于等于精度时
- `%G`，根据值的不同，自动选择`%f`或`%E`，`%E`格式用于指数小于`-4`或大于等于精度时
- `%e`，指数记数法的浮点数，可用a和A分别代替e和E，变为十六进制表示，`long double`需要加`L`，小写`l`不行
- `%u`，显示无符号的`int`类型
- `%ld`，十进制`long`类型
- `%lx`，十六进制`long`类型
- `%lo`，八进制`long`类型
- `%#o`，加`#`后，将显示进制前缀。
- `%hd`，以十进制表示`short`类型的整数
- `%ho`，以八进制显示`short`类型的整数
- `ll`用法类似
- 浮点数上溢的情况下会给变量赋一个表示无穷大的特定值，而且`printf()`显示该值为`inf`或`infinity`（或者具有无穷含义的其他内容）
- C语言中的三种虚数类型：`float _Imaginary`、`double _Imaginary`和`long double _Imaginary`
- 使用`sizeof()`可以类型占用字节大小，打印时使用`%zd`（即`size_t`类型，实际上是`long long`）或者`%u`、`%lu`

#### 8、字符串

- 在C语言中，可以把多个连续的字符串组合成一个字符串
- 标准明确规定了何时把缓冲区中的内容发送到屏幕：当缓冲区满、遇到换行字符或需要输入的时候（从缓冲区把数据发送到屏幕或文件被称为刷新缓冲区）
- 使用数组存储字符串，数组的长度必须比实际字符串长度多1，例如数组长度40，则只能存储39个字符，最后一个是`\0`表示字符串结束

#### 9、`scanf`

- 修饰符：
  - `*`，抑制赋值，例如`%*d`，会使`scanf`跳过相应的输出项，跳过的值不应该使用变量接收。
  - 数字，最大字段宽度，输入达到最大字段宽度处，或第1次遇到空白字符时停止，示例：`%10s`
  - `hh`，把整数作为`signed char`或`unsigned char`类型读取，示例：`%hhd`、`%hhu`
  - `ll`，把整数作为`long long`或`unsigned long long`类型读取，示例：`%lld`、`%llu`。
  - `h`、`l`或`L`，注意在`e`、`f`和`g`前面使用大写`L`表示`long double`即可
  - `j`，在整型转换说明后面时，表明使用`intmax_t`或`uintmax_t`类型，示例：`%zd`、`%zo`
  - `z`，在整型转换说明后面时，表明使用`sizeof`的返回类型
  - `t`，在整型转换说明后面时，表明使用两个指针差值的类型，示例：`%td`、`%tx`
- 该函数返回成功读取项的数量

#### 10、`printf`

- `*`，可以通过程序指定字段宽度，如果转换说明是`%*d`，那么参数列表中应包含*和`d`对应的值

#### 11、关键字

- `const`

  - `const`放在`*`左侧任意位置，限定了指针指向的数据不能改变；`const`放在`*`的右侧，限定了指针本身不能改变。

- `volatile`

  - `volatile` 限定符告知计算机，代理（而不是变量所在的程序）可以改变该变量的值。通常，它被用于硬件地址以及在其他程序或同时运行的线程中共享数据。警告编译器不要进行假定的优化。

- `restrict`（`c99`）

  - 该关键字允许编译器优化某部分代码以更好地支持计算。它只能用于指针，表明该指针是访问数据对象的唯一且初始的方式。
  - 如果未使用`restrict`关键字，编译器就必须假设最坏的情况（即，在两次使用指针之间，其他的标识符可能已经改变了数据）。如果用了`restrict`关键字，编译器就可以选择捷径优化计算。
  - `restrict `限定符还可用于函数形参中的指针。这意味着编译器可以假定在函数体内其他标识符不会修改该指针指向的数据，而且编译器可以尝试对其优化，使其不做别的用途。
  - 重点：声明为`restrict`说明这个指针是访问相应数据的唯一方式

- `_Atomic`（`c11`，关注`stdatomic.h`库）

- `_Alignof`，给出一个类型的对齐要求，在关键字后面的圆括号中写上类型名：`size_t d_align = _Alignof(float);`假设`d_align`的值是4，意思是`float`类型对象的对齐要求是4。也就是说，4是储存该类型值相邻地址的字节数。对齐值都应该是2的非负整数次幂。较大的对齐值被称为`stricter`或`stronger`，较小的对齐值被称为`weaker`。（用来获取指定类型的对齐要求）

- `_Alignas` ：指定变量或数据类型的对齐要求。可以使用`_Alignas` 说明符指定一个变量或类型的对齐值。但是，不应该要求该值小于基本对齐值。例如，如果`float`类型的对齐要求是4，不要请求其对齐值是1或2。该说明符用作声明的一部分，说明符后面的圆括号内包含对齐值或类型：

  ```c
  _Alignas(double) char c1;
  _Alignas(8) char c2;
  unsigned char _Alignas(long double) c_arr[sizeof(long double)];
  ```

  

### 二、标准库

#### 1、`stdint.h`

> 定义了各种类型，确保在不同系统中功能相同

- 精确宽度整数类型，例如`int32_t`和`uint32_t`
- 最小宽度类型，例如`int_least8_t`和`uint_least8_t`
- 最快最小宽度类型，例如`int_fast8_t`和`uint_fast8_t`。
- 最大的有符号整数类型，例如`intmax_t`和`uintmax_t`

#### 2、`inttypes.h`

> 提供了一些字符串宏来显示可移植类型

- 示例

```c
printf("uint32_t = %d\n", i);
printf("uint32_t = %" PRId32 "\n", i);
```

#### 3、`complex.h`

> 复数相关

- `I`表示-1的平方根

#### 4、`string.h`

> 字符串相关的标准库

- `size_t strlen(const char * s);`：统计字符串长度
- `char *strcat(char * restrict s1, const char * restrict s2);`：该函数把`s2`指向的字符串拷贝至`s1`指向的字符串末尾。`s2`字符串的第1个字符将覆盖`s1`字符串末尾的空字符。该函数返回`s1`。
- `char *strncat(char * restrict s1, const char * restrict s2, size_t n);`：该函数把`s2`字符串中的n个字符拷贝至`s1`字符串末尾。`s2`字符串的第1个字符将覆盖`s1`字符串末尾的空字符。不会拷贝`s2`字符串中空字符和其后的字符，并在拷贝字符的末尾添加一个空字符。该函数返回`s1`。
  - `int strcmp(const char * s1, const char * s2);`：如果`s1`字符串在机器排序序列中位于`s2`字符串的后面，该函数返回一个正数；如果两个字符串相等，则返回0；如果`s1`字符串在机器排序序列中位于`s2`字符串的前面，则返回一个负数。
- `int strncmp(const char * s1, const char * s2, size_t n);`：该函数的作用和`strcmp()`类似，不同的是，该函数在比较`n`个字符后或遇到第1个空字符时停止比较。
- `char *strcpy(char * restrict s1, const char * restrict s2);`：该函数把`s2`指向的字符串（包括空字符）拷贝至`s1`指向的位置，返回值是`s1`。
- `char *strncpy(char * restrict s1, const char * restrict s2, size_t n);`：该函数把`s2`指向的字符串拷贝至`s1`指向的位置，拷贝的字符数不超过n，其返回值是`s1`。该函数不会拷贝空字符后面的字符，如果源字符串的字符少于`n`个，目标字符串就以拷贝的空字符结尾；如果源字符串有`n`个或超过n个字符，就不拷贝空字符。
- `char *strchr(const char * s, int c);`如果`s`中包含`c`字符，返回指向`s`字符串首位置的指针（包含末尾的`\0`），如果未找到，返回空指针
- `char *strpbrk(const char * s1, const char * s2);`如果`s1`字符串包含`s2`字符串中的任意字符函，数返回指向 `s1` 字符串首位置的指针；如果在`s1`字符串中未找到任何`s2`字符串中的字符，则返回空字符。
- `char *strrchr(const char * s, int c);`该函数返回`s`字符串中`c`字符的最后一次出现的位置（末尾的空字符也是字符串的一部分，所以在查找范围内）。如果未找到`c`字符，则返回空指针。
- `char *strstr(const char * s1, const char * s2);`该函数返回指向`s1`字符串中`s2`字符串出现的首位置。如果在`s1`中没有找到`s2`，则返回空指针。
- `void *memcpy(void *_Dst, const void *_Src, size_t _Size) `，从`_Src`指向的位置拷贝`n`字节到`_Dst`指向的位置。相当于复制，要求两个地址不相同。
- `void *memmove(void *_Dst, const void *_Src, size_t _Size) `，从`_Src`指向的位置拷贝`n`字节到`_Dst`指向的位置。相当于移动，两个地址可以相同。

#### 5、`limits.h`

> 定义了整数类型的限制值。这些限制值包括各类整数类型的最大值、最小值等

#### 6、`float.h`

> 定义了浮点数类型的限制值

#### 7、`ctype.h`

> 处理字符的标准库

- `isalnum`：是不是字母或数字
- `isalpha`：是不是字母
- `isblank`：是不是标准的空白字符（空格、水平制表符或换行符）或任何其它本地化指定为空白的字符
- `iscntrl`：是不是控制字符，如`Ctrl+B`
- `isdigit`：是不是数字
- `isgraph`：是不是除空格之外的任意可打印字符
- `islower`：是不是小写字母
- `isprint`：是不是可打印字符
- `ispunct`：是不是标点符号（除空格或字母数字字符以外的任何可打印字符）
- `isspace`：是不是空白字符（空格、换行符、换页符、回车符、垂直制表符、水平制表符或其他本地化定义的字符）
- `isupper`：是不是大写字母
- `isxdigit`：是不是十六进制字符
- `tolower`：如果参数是大写字符，该函数返回小写字符，否则返回原始参数
- `toupper`：如果参数是小写字符，该函数返回大写字符，否则返回原始参数

#### 8、`stdio.h`

> 标准输入输出

- `getchar()`：从输入队列返回一个字符

- `putchar()`：打印他的参数
- `puts`：只显示字符串，并且在末尾自动加上换行符
- `gets`：读取一行输入，直到遇到换行符，然后丢弃换行符，存储其余的字符，在末尾添加`\0`，但是不安全，不推荐使用
- `fgets`：通过第二个参数限制读入的字符数，第三个参数指定要读入的文件。`stdin`表示键盘输入，`stdout`表示标准输出。它会把换行符放在字符串的末尾（假设输入行不溢出）。
- `fputs`：不在末尾添加空行。返回指向`char`的指针，如果进行顺利，返回的地址和传入的第一个参数相同，但是如果读到文件末尾，将返回一个空指针，该指针保证不会指向有效的数据，所以可以标识这种特殊情况。在代码中可以使用数字0来代替，但使用宏`NULL`更常见

```c
#include <stdio.h>
#define MAX_LENGTH 10
int main(void) {
    char buf[MAX_LENGTH];
    fgets(buf, MAX_LENGTH, stdin);
    puts(buf);
    fputs(buf, stdout);
    return 0;
}
```

- `sprintf`：把多个元素组合成一个字符串，用法和`printf`基本类似

  ```c
  char spt[20];
  sprintf(spt, "%s -> %d", "年龄", 18);
  printf("%s\n", spt);
  ```

- `int getc(FILE *_File) `，从文件中读取一个字符
- `int putc(int _Ch, FILE *_File) `，向文件中写入一个字符
- `FILE *fopen(const char *_Filename, const char *_Mode) `，以指定模式打开文件
- `int fclose(FILE *_File) `关闭文件，关闭成功返回0，否则返回`EOF`。
- `inline static int fprintf(FILE *__stream, const char *__format, ...) `，第一个参数必须是文件指针，向文件写入数据
- `inline static int fscanf(FILE *__stream, const char *__format, ...) `，从文件读取数据
- `void rewind(FILE *_File) `，返回到文件开始处
- `int fseek(FILE *_File, long _Offset, int _Origin) `第一个参数指向待查找的文件，第二个参数是偏移量，表示从起点开始要移动的距离，正数前移，负数后移，第三个参数是模式，受`long`限制，确定起始点：
  - `SEEK_SET`：文件开始处，`0L`
  - `SEEK_CUR`：当前位置`1L`
  - `SEEK_END`：文件末尾`2L`
  - 使用示例：
  - `fseek(fp, 0L, SEEK_SET)`定位至文件开始处
  - `fseek(fp, 10L, SEEK_SET)`定位至文件中的第10个字节
  - `fseek(fp, 0L, SEEK_END);` 定位至文件结尾
  - `fseek(fp, 2L, SEEK_CUR); `从文件当前位置前移2个字节
  - `fseek(fp, -10L, SEEK_END); `从文件结尾处回退10个字
  - 正常返回0，异常返回-1
- `long ftell(FILE *_File) `，返回文件当前位置，受`long`限制
- `int fgetpos(FILE *_File, fpos_t *_Pos) `，把`fpos_t`类型的值放在`pos`指向的位置上，该值描述了文件中的一个位置。
- `int fsetpos(FILE *_File, const fpos_t *_Pos) `，使用`pos`指向位置上的`fpos_t`类型值来设置文件指针指向该值指定的位置。
- `int ungetc(int _Ch, FILE *_File) `，把`_Ch`指定的字符放回输入流中。如果把一个字符放回输入流，下次调用标准输入函数时将读取该字符。
- `int fflush(FILE *fp) `，调用该函数引起输出缓冲区中所有的未写入数据被发送到`fp`指定的输出文件。这个过程称为刷新缓冲区。如果 `fp`是空指针，所有输出缓冲区都被刷新。在输入流中使用`fflush()`函数的效果是未定义的。只要最近一次操作不是输入操作，就可以用该函数来更新流（任何读写模式）。
- `int setvbuf(FILE * restrict fp, char * restrict buf, int mode, size_t size); `，创建一个供标准`I/O`函数替换使用的缓冲区。在打开文件后且未对流进行其他操作之前，调用该函数。针`fp`识别待处理的流，`buf`指向待使用的存储区。如果`buf`的值不是`NULL`，则必须创建一个缓冲区。如果把`NULL`作为`buf`的值，该函数会为自己分配一个缓冲区。变量`size`告诉`setvbuf()`数组的大小，`mode`的选择如下：
  - `_IONBF`表示无缓冲。
  - `_IOFBF`表示完全缓冲（在缓冲区满时刷新）
  - `_IOLBF`表示行缓冲（在缓冲区满时或写入一个换行符时）
  - 如果操作成功，函数返回0，否则返回一个非零值。
  - 假设一个程序要储存一种数据对象，每个数据对象的大小是`3000`字节。可以使用`setvbuf()`函数创建一个缓冲区，其大小是该数据对象大小的倍数。
- `size_t fread(void * restrict ptr, size_t size, size_t nmemb,FILE * restrict fp); `，用于以二进制形式处理数据，`ptr`用来保存读取到的数据，`fp`指定待读取的文件。该函数用于读取被`fwrite()`写入文件的数据。返回成功读取项的数量。正常情况下，该返回值就是`nmemb`，但如果出现读取错误或读到文件结尾，该返回值就会比`nmemb`小。
- `size_t fwrite(const void * restrict ptr, size_t size, size_t nmemb,FILE * restrict fp); `把二进制数据写入文件。`size_t`是根据标准C类型定义的类型，它是`sizeof`运算符返回的类型，通常是`unsigned int`，但是实现可以选择使用其他类型。指针`ptr`是待写入数据块的地址。`size`表示待写入数据块的大小（以字节为单位），`nmemb`表示待写入数据块的数量（一般数组需要，单个值指定为1）。和其他函数一样， `fp`指定待写入的文件。返回成功写入项的数量，正常情况下，该返回值就是`nmemb`，但如果出现写入错误，返回值会比`nmemb`小。
- `int feof(FILE *_File) `当上一次输入调用检测到文件结尾时，`feof()`函数返回一个非零值，否则返回0。
- `int ferror(FILE *_File) `当读或写出现错误，`ferror()`函数返回一个非零值，否则返回0。

示例代码1：

```c
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SLEN 81
#define BUF_SIZE 4096

char *s_gets(char *st, int n);

void append(FILE *dst_fp, FILE *src_fp);

int main(void) {
    FILE *dst_fp, *src_fp;
    int files = 0; // 附加的文件数量
    char file_dst_name[SLEN]; // 目标文件名
    char file_src_name[SLEN]; // 源文件名
    int ch;
    puts("输入目标文件名称: ");
    s_gets(file_dst_name, SLEN);
    if ((dst_fp = fopen(file_dst_name, "a+")) == NULL) {
        fprintf(stderr, "不能打开 %s。\n", file_dst_name);
        exit(EXIT_FAILURE);
    }
    if (setvbuf(dst_fp, NULL, _IOFBF, BUF_SIZE)) {
        fputs("不能创建输出缓冲区。\n", stderr);
        exit(EXIT_FAILURE);
    }
    puts("输入第一个源文件: ");
    while (s_gets(file_src_name, SLEN) && file_src_name[0] != '\0') {
        if (strcmp(file_dst_name, file_src_name)) {
            if ((src_fp = fopen(file_src_name, "r")) == NULL) {
                fprintf(stderr, "不能打开文件 %s。\n", file_src_name);
            } else {
                if (setvbuf(src_fp, NULL, _IOFBF, BUF_SIZE)) {
                    fputs("不能创建输出缓冲区。\n", stderr);
                    continue;
                }
                append(dst_fp, src_fp);
                if (ferror(src_fp)) {
                    fprintf(stderr, "读取文件失败 %s。\n", file_src_name);
                }
                if (ferror(dst_fp)) {
                    fprintf(stderr, "写入文件失败 %s。\n", file_dst_name);
                }
                fclose(src_fp);
                files++;
                printf("文件 %s 已添加。\n", file_src_name);
                puts("下一个文件: ");
            }
        } else {
            fputs("不能添加给自己。", stderr);
        }
    }
    printf("%d 个文件已完成添加。\n", files);
    rewind(dst_fp);
    printf("%s 文件内容: \n", file_dst_name);
    while ((ch = getc(dst_fp)) != EOF) {
        putchar(ch);
    }
    puts("\n完成展示。");
    fclose(dst_fp);
    return 0;
}

void append(FILE *dst_fp, FILE *src_fp) {
    size_t bytes;
    static char temp[BUF_SIZE];
    while ((bytes = fread(temp, sizeof(char), BUF_SIZE, src_fp)) > 0) {
        fwrite(temp, sizeof(char), bytes, dst_fp);
    }
}

char *s_gets(char *st, const int n) {
    char *ret_val = fgets(st, n, stdin);
    if (ret_val) {
        char *find = strchr(st, '\n');
        if (find) {
            *find = '\0';
        } else {
            while (getchar() != '\n') {
                continue;
            }
        }
    }
    return ret_val;
}
```

示例代码2：

```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define AR_SIZE 10

int main(void) {
    double numbers[AR_SIZE];
    const char *file = "numbers.dat";
    FILE *iofile;
    srand(time(NULL));
    // 创建一组double类型的值
    for (int i = 0; i < AR_SIZE; ++i) {
        const double rand_num = 100.0 * i + 1.0 / rand();
        printf("%f ", rand_num);
        numbers[i] = rand_num;
    }
    printf("\n");
    if ((iofile = fopen(file, "wb")) == NULL) {
        fprintf(stderr, "不能打开文件: %s。\n", file);
        exit(EXIT_FAILURE);
    }
    // 以二进制格式把数组写入文件
    fwrite(numbers, sizeof(double), AR_SIZE, iofile);
    fclose(iofile);
    // 以二进制格式读取文件
    if ((iofile = fopen(file, "rb")) == NULL) {
        fprintf(stderr, "不能打开文件: %s。\n", file);
        exit(EXIT_FAILURE);
    }
    // 从文件中读取选定的内容
    printf("输入索引 0-%d: \n", AR_SIZE - 1);
    int index;
    double value;
    while (scanf("%d", &index) == 1 && index >= 0 && index < AR_SIZE) {
        const long pos = (long) index * sizeof(double);
        fseek(iofile, pos, SEEK_SET);
        fread(&value, sizeof(double), 1, iofile);
        printf("这个值是 %f。\n", value);
        printf("下一个索引: \n");
    }
    fclose(iofile);
    puts("Bye!");
    return 0;
}
```



#### 9、`ios646.h`

> 可代替逻辑运算符的拼写，例如可以使用`and`代替`&&`，使用`or`代替`||`，使用`not`代替`!`

```c
#ifndef _ISO646_H
#define _ISO646_H

#ifndef __cplusplus
#define and	&&
#define and_eq	&=
#define bitand	&
#define bitor	|
#define compl	~
#define not	!
#define not_eq	!=
#define or	||
#define or_eq	|=
#define xor	^
#define xor_eq	^=
#endif

#endif
```

#### 10、`stdbool.h`

> 为`_Bool`类型提供定义。例如可以使用`bool`来代表`_Bool`类型，`true`代表`1`，`false`代表`0`

#### 11、`stdlib.h`

- `int atoi(const char *_Str) `：将字符串转换为整数；如果开头是数字，会截止到第一个非数字，如果开头不是数字，返回0或者未定义的结果。

- `atof`：把字符串转换成 `double` 类型的值

- `atol`：把字符串转换成`long`类型的值

- `atoll`：把字符串转换成`long long`类型的值

- `long strtol(const char *_Str, char **_EndPtr, int _Radix) `，把字符串转换成long类型的值，可指定进制，例如：

  ```c
  #include <stdio.h>
  #include <stdlib.h>
  
  #include <stdio.h>
  #include <stdlib.h>
  #define LIM 30
  
  char *s_gets(char *st, int n);
  
  int main() {
      char number[LIM];
      char *end;
      long value;
      puts("Enter a number (empty line to quit):");
      while (s_gets(number, LIM) && number[0] != '\0') {
          value = strtol(number, &end, 10); /* 十进制 */
          printf("base 10 input, base 10 output: %ld, stopped at %s (%d)\n",
                 value, end, *end);
          value = strtol(number, &end, 16); /* 十六进制 */
          printf("base 16 input, base 10 output: %ld, stopped at %s (%d)\n",
                 value, end, *end);
          puts("Next number:");
      }
      printf("Bye! %d\n", 't');
      return 0;
  }
  
  char *s_gets(char *st, int n) {
      char *ret_val;
      int i = 0;
      ret_val = fgets(st, n, stdin);
      if (ret_val) {
          while (st[i] != '\n' && st[i] != '\0')
              i++;
          if (st[i] == '\n')
              st[i] = '\0';
          else
              while (getchar() != '\n')
                  continue;
      }
      return ret_val;
  }
  ```

  - 最多支持`36`进制
  - 许多实现使用` itoa()`和` ftoa()`函数分别把整数和浮点数转换成字符串。但是这两个函数并不是 C标准库的成员，可以用`sprintf()`函数代替它们，因为`sprintf()`的兼容性更好。

- `unsigned long strtoul(const char *_Str, char **_EndPtr, int _Radix) `把字符串转换成unsigned long类型的值，`strtod()`把字符串转换成double类型的值。

- `void srand(unsigned int _Seed) `设置`rand`函数的种子。

- `void *malloc(size_t _Size) `，分配内存。

- `void free(void *_Memory) `，释放内存。

- `void exit(int _Code) `，结束程序。`exit()`执行完`atexit()`指定的函数后，会完成一些清理工作：刷新所有输出流、关闭所有打开的流和关闭由标准I/O函数`tmpfile()`创建的临时文件。然后exit()把控制权返回主机环境，如果可能的话，向主机环境报告终止状态。

- `int atexit(void(*)()) `通过退出时注册被调用的函数提供这种功能，`atexit()`函数接受一个函数指针作为参数。这个函数使用函数指针。

  ```c
  #include <stdio.h>
  #include <stdlib.h>
  
  void sign_off(void);
  
  void too_bad(void);
  
  int main(void) {
      int n;
      atexit(sign_off);
      puts("Enter an integer:");
      if (scanf("%d", &n) != 1) {
          puts("That's no integer!");
          atexit(too_bad);
          exit(EXIT_FAILURE);
      }
      printf("%d is %s.\n", n, n % 2 == 0 ? "even" : "odd");
      return 0;
  }
  
  void sign_off(void) {
      puts("Thus terminates another magnificent program from");
      puts("SeeSaw Software!");
  }
  
  void too_bad(void) {
      puts("SeeSaw Software extends its heartfelt condolences");
      puts("to you upon the failure of your program.");
  }
  ```

  - 要使用` atexit()`函数，只需把退出时要调用的函数地址传递给` atexit()`即可。函数名作为函数参数时相当于该函数的地址，所以该程序中把`sign_off`或`too_bad`作为参数。然后，`atexit()`注册函数列表中的函数，当调用`exit()`时就会执行这些函数。ANSI保证，在这个列表中至少可以放 32 个函数。最后调用 `exit()`函数时，`exit()`会执行这些函数（执行顺序与列表中的函数顺序相反，即最后添加的函数最先执行）。
  - `atexit()`注册的函数（如`sign_off()`和`too_bad()`）应该不带任何参数且返回类型为`void`。

- `void *calloc(size_t _NumOfElements, size_t _SizeOfElements) `，1个参数是所需的存储单元数量，第2个参数是存储单元的大小（以字节为单位）。它把块中的所有位都设置为0。

- `void qsort(void *_Base, size_t _NumOfElements, size_t _SizeOfElements, int(*_PtFuncCompare)(const void *, const void *)) `，快速排序算法。

  - 第一个参数是指针，指向待排序数组的首元素

  - 第二个参数是待排序项的数量

  - 第三个参数是数组中每个元素的大小

  - 第四个参数指向比较函数，用于确定排序顺序。该函数应该接收两个参数，分别指向待比较两项的指针，如果第一项大于第二项，返回正数，相同返回0，否则返回负数。

    ```c
    #include <stdio.h>
    #include <stdlib.h>
    #include <time.h>
    #define ARRAY_SIZE 10
    
    void fillarray(double ar[], int n);
    
    void showarray(const double ar[], int n);
    
    int asc(const void *p1, const void *p2);
    
    int desc(const void *p1, const void *p2);
    
    int main(void) {
        double arr[ARRAY_SIZE];
        fillarray(arr, ARRAY_SIZE);
        showarray(arr, ARRAY_SIZE);
        qsort(arr, ARRAY_SIZE, sizeof(double), asc);
        showarray(arr, ARRAY_SIZE);
        qsort(arr, ARRAY_SIZE, sizeof(double), desc);
        showarray(arr, ARRAY_SIZE);
        return 0;
    }
    
    void fillarray(double ar[], int n) {
        srand(time(0));
        for (int i = 0; i < n; ++i) {
            ar[i] = rand() % 100 / 100.0;
        }
    }
    
    void showarray(const double ar[], int n) {
        for (int i = 0; i < n; ++i) {
            printf("%f ", ar[i]);
        }
        printf("\n");
    }
    
    int asc(const void *p1, const void *p2) {
        const double *e1 = (const double *) p1;
        const double *e2 = (const double *) p2;
        return *e1 > *e2 ? 1 : *e1 == *e2 ? 0 : -1;
    }
    
    int desc(const void *p1, const void *p2) {
        const double *e1 = (const double *) p1;
        const double *e2 = (const double *) p2;
        return *e1 > *e2 ? -1 : *e1 == *e2 ? 0 : 1;
    }
    ```

#### 12、`time.h`

- `inline static time_t time(time_t *_Time) `，接受一个`time_t`类型对象的地址，时间存储在传入的地址上，也可以传入空指针作为参数（即0）。这种情况下，只能通过返回值来提供值。

#### 13、`math.h`

> 函数中涉及的角度都以弧度为单位（1弧度=180/π=27.196度）

- `double acos(double x)`，返回余弦值为`x`的角度（0~π弧度）
- `double asin(double x)`，返回正弦值为`x`的角度（`-π/2~π/2`弧度）
- `double atan(double x)`，返回正切值为`x`的角度（`-π/2~π/2`弧度）
- `double atan2(double y, double x)`，返回正弦值为`y/x`的角度（`-π~π`弧度）
- `double cos(double x)`返回`x`的余弦值，`x`的单位为弧度
- `double sin(double x)`返回x的正弦值，`x`的单位为弧度
- `double tan(double x)`返回x的正切值，`x`的单位为弧度
- `double exp(double x)`返回x的指数函数的值（e<sup>x</sup>）
- `double log(double x)`返回x的自然对数值
- `double log10(double)`返回x的以10为底的对数值
- `double pow(double x, double y)`返回x的y次幂
- `double sqrt(double x)`返回x的平方根
- `double cbrt(double x)`返回x的立方根
- `double ceil(double x)`返回不小于x的最小整数值
- `double fabs(double x)`返回x的绝对值
- `double floor(double x)`返回不大于x的最大整数值
- 类型变体：C标准专门为`float`类型和`long double`类型提供了标准函数，即在原函数名前加上f或l前缀。因此，`sqrtf()`是`sqrt()`的`float`版本，`sqrtl()`是`sqrt()`的`long double`版本。利用`C11 `新增的泛型选择表达式定义一个泛型宏，根据参数类型选择最合适的数学函数版本。

#### 14、`tgmath.h`

- `C99`标准提供的`tgmath.h`头文件中定义了泛型类型宏

#### 15、`assert.h`

> 该头文件支持的断言库是一个用于辅助调试程序的小型库。它由 `assert()`宏组成，接受一个整型表达式作为参数。如果表达式求值为假（非零），`assert()`宏就在标准错误流`（stderr）`中写入一条错误信息，并调用abort()函数终止程序（abort()函数的原型在`stdlib.h`头文件中）。`assert()`宏是为了标识出程序中某些条件为真的关键位置，如果其中的一个具体条件为假，就用 assert()语句终止程序。通常，`assert()`的参数是一个条件表达式或逻辑表达式。如果 `assert()`中止了程序，它首先会显示失败的测试、包含测试的文件名和行号。把`#define NDEBUG`的定义写在`#include <assert.h>`前面可以关闭所有`assert()`

- `assert()`可以导致正在运行的程序终止
- `_Static_assert(12 == 12);`在编译时检查，可以导致程序无法通过编译。接收两个参数，第一个参数是整型常量表达式，第二个参数是字符串，如果第一个表达式求值是0，编译器会显示字符串，而且不编译该程序。

#### 16、`stdarg.h`

> 可变参数

- ```c
  #include <stdio.h>
  #include <stdarg.h>
  
  // 1, 提供一个使用省略号的函数原型
  void sum(int lim, ...);
  
  int main(void) {
      sum(3, 1.0, 2.0, 3.4);
      return 0;
  }
  
  void sum(const int lim, ...) {
      // 2, 在函数定义中创建一个va_list类型的变量
      va_list ap;
      // 3, 用宏把该变量初始化为一个参数列表, 把参数列表拷贝到va_list类型的变量中
      // 在该例中，lim是parmN形参，它表明变参列表中参数的数量。
      va_start(ap, lim);
      // 因为va_arg()不提供退回之前参数的方法，所以有必要保存va_list类型变量的副本。
      // C99新增了一个宏用于处理这种情况：va_copy()。该宏接受两个va_list类型的变量作为参数，它把第2个参数拷贝给第1个参数：
      va_list ap_copy;
      va_copy(ap_copy, ap);
      // 4, 用va_arg宏访问参数列表, 该宏接收两个参数
      // 一个va_list类型的变量和一个类型名
      // 第1次调用va_arg()时，它返回参数列表的第1项；第2次调用时返回第2项，以此类推。
      // 注意，传入的参数类型必须与宏参数的类型相匹配。
      double sum_result = 0;
      for (int i = 0; i < lim; ++i) {
          sum_result += va_arg(ap, double);
      }
      printf("sum = %.2lf\n", sum_result);
      // 5, 用va_end宏完成清理工作, 例如，释放动态分配用于储存参数的内存。该宏接受一个va_list类型的变量
      // 调用va_end后, 只有用va_start重新初始化ap后, 才能使用变量ap
      va_end(ap);
  }
  ```

### 三、一般约定

#### 1、命名

- 在名称前带`c_`或`k_`前缀来表示常量（如，`c_level`或`k_line`）。
- 用大写表示符号常量

#### 2、`typedef`可以给类型定义别名

#### 3、`++`和`--`

- 如果一个变量出现在一个函数的多个参数中，不要对该变量使用递增或递减运算符；
- 如果一个变量多次出现在一个表达式中，不要对该变量使用递增或递减运算符。

#### 4、缓冲

- 完全缓冲`I/O`：当缓冲区被填满时才刷新缓冲区，通常用在文件输入中
- 行缓冲`I/O`：出现换行时刷新缓冲区

#### 5、逗号运算符

- 整个逗号表达式的值是最右侧项的值。

#### 6、输入输出重定向

- 在`UNIX`、`DOS`和`Windows`中，输入重定向是`<`，输出重定向是`>`或`>>`，还有管道`|`等

```sh
3.exe < ../src/resources/1.txt > ../src/resources/1_copy.txt
```

- 命令与重定向运算符的顺序无关
- 遵循原则：
  - 重定向运算符连接一个可执行程序（包括标准操作系统命令）和一个数据文件，不能用于连接一个数据文件和另一个数据文件，也不能用于连接一个程序和另一个程序。
  - 使用重定向运算符不能读取多个文件的输入，也不能把输出定向至多个文件。
  - 通常，文件名和运算符之间的空格不是必须的，除非是偶尔在`UNIX shell`、`Linux shell`或`Windows`命令行提示模式中使用的有特殊含义的字符。

### 四、运算符

#### 1、求模运算

- 求模结果的正负取决于第一个运算符的正负，另外，可以通过`a-a/b*b`来求模。

#### 2、类型转换

- 当类型转换出现在表达式时，无论是`unsigned`还是`signed`的`char`和`short`都会被自动转换成`int`，如有必要会被转换成`unsigned int`（如果`short`与`int`的大小相同，`unsigned short`就比`int`大。这种情况下，`unsigned short`会被转换成`unsigned int`）
- 涉及两种类型的运算，两个值会被分别转换成两种类型的更高级别
- 类型的级别从高至低依次是`long double`、`double`、`float`、`unsigned long long`、`long long`、`unsigned long`、`long`、`unsigned int`、`int`。例外的情况是，当 `long` 和 `int` 的大小相同时，`unsigned int`比`long`的级别高。之所以`short`和`char`类型没有列出，是因为它们已经被升级到`int`或`unsigned int`
- 当作为函数参数传递时，`char`和`short`被转换成`int`，`float`被转换成`double`
- 如果待转换的值与目标类型不匹配：
  - 目标类型是无符号整型，且待赋的值是整数时，额外的位将被忽略。例如，如果目标类型是 8 位`unsigned char`，待赋的值是原始值求模256。
  - 如果目标类型是一个有符号整型，且待赋的值是整数，结果因实现而异。
  - 如果目标类型是一个整型，且待赋的值是浮点数，该行为是未定义的。
  - 当浮点类型被降级为整数类型时，原来的浮点值会被截断。

#### 3、函数原型

- 务必确保函数原型和被调用的函数相同，尤其是参数部分，虽然可以省略，但会导致类型转换问题。

### 五、注意事项

- `C`编译器不会检查数组越界问题。
- 对于`else if`的嵌套层数，`C99`要求最少支持`127`层
- 带参数的`main`函数示例

```c
#include <stdio.h>
int main(int argc, char *argv[]) {
    printf("The command line has %d arguments:\n", argc - 1);
    for (int i = 1; i < argc; ++i) {
        printf("%d: %s\n", i, argv[i]);
    }
    printf("\n");
    return 0;
}
```

- 说明：`c`编译器允许`main`没有参数或者有两个参数（有些实现运行有更多参数，属于对标准的扩展），有两个参数时，第一个参数是命令行中的字符串数量，过去，这个`int`类型的参数被称为`argc`，系统用空格表示一个字符串的结束和下一个字符串的开始。程序把命令行字符串储存在内存中，并把每个字符串的地址储存在指针数组中。而该数组的地址则被储存在` main()`的第 2 个参数中（`char **argv`和`char *[]`等价），按照惯例，这个指向指针的指针称为`argv`。如果系统允许（一些操作系统不允许这样），就把程序本身的名称赋给`argv[0]`，然后把随后的第1个字符串赋给`argv[1]`，以此类推。允许使用双引号把多个单词括起来形成一个参数。

- 声明和定义

  - ```c
    int tern = 1; /* tern被定义 */
    main()
    {
    extern int tern; /* 使用在别处定义的tern */
    }
    ```

  - 这里，`tern`被声明了两次。第1次声明为变量预留了存储空间，该声明构成了变量的定义。第2次声明只告诉编译器使用之前已创建的`tern`变量，所以这不是定义。第1次声明被称为定义式声明（defining declaration），第2次声明被称为引用式声明（referencing declaration）。关键字`extern`表明该声明不是定义，因为它指示编译器去别处查询其定义。

- `C99`允许把类型限定符和存储类别说明符`static`放在函数原型和函数头的形式参数的初始方括号中。，例如以下两种声明等价：

  ```c
  void ofmouth(int * const a1, int * restrict a2, int n); // 以前的风格
  void ofmouth(int a1[const], int a2[restrict], int n); // C99允许
  ```

  - 现在，`static`除了表明静态存储类别变量的作用域或链接外，新的用法告知编译器如何使用形式参数。

  ```c
  double stick(double ar[static 20]);
  ```

  - `static` 的这种用法表明，函数调用中的实际参数应该是一个指向数组首元素的指针，且该数组至少有20个元素。这种用法的目的是让编译器使用这些信息优化函数的编码。

### 六、指针

#### 1、注意事项

- 在类型后加`*`来声明对应的指针，但是指针类型本身是`int`类型的（存储地址）
- 假设`intdate[10]; int *ptr = date`，那么`ptr + 2 == &date[2]`，他们有相同的地址，`*(ptr + 2) == date[2]`，他们有相同的值
- 假设`int nums[10]; int *ptr = nums`那么这两个语句等价：`ptr[2]`和`nums[2]`
- 只有在函数原型或函数定义中，才可以使用`int ar[]`代替`int *ar`。
- `int *ar`形式和`int ar[]`形式都表示`ar`是一个指向`int`的指针。但是，`int ar[]`只能用于声明形式参数。第2种形式`（int ar[]）`提醒指针`ar`指向的不仅仅一个`int`类型值，还是一个`int`类型数组的元素。
- 注意，函数中的数组参数，其`sizeof`值是8字节，因为他表示的是指向数组首元素的指针。（取决于系统使用多少字节表示地址）
- 将指针声明为`const`，则不允许修改指针指向的地址的值，但是可以将指针指向别处。这表明：声明为`const`的地址可以保护数据。
- 不要将非`const`的指针指向`const`的数据
- 不要将`const`的数据传递给参数不是`const`的函数
- 如果想声明一个不能指向别处的指针，可以这样声明：`int *const ptr = rates;`这个指针可以修改它所指向的值，再添加一个`const`，则既不能指向别处，也不能修改值。`const int *const ptr = rates;`

#### 2、指针的八种操作

> `int nums[10]; int *ptr`

1. 把一个地址赋值给指针：`ptr = nums`或`ptr = &nums[1]`

2. 解引用指针：`*ptr`

3. 获得指针的地址：`&ptr`

4. 指针加法：`ptr + 4`

5. 递增指针：`ptr++`

6. 递减指针：`ptr--`

7. 一个指针减去另外一个指针：`ptr2 - ptr1`，假如这两个指针分别指向数组的不同元素，通过计算差值求出两个元素之间的距离，如果指向两个不同的数组，求差可能导致运行错误。

   ```c
   int nums[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
   int *ptr1 = &nums[3];
   int *ptr2 = &nums[6];
   printf("%zd\n", ptr1 - ptr2);
   // 结果 -3
   ```

8. 一个指针减去一个整数：`ptr - 2`

- 指针减指针得到整数，指针减整数得到另一个指针，两个指针不能相加
- C 只能保证指向数组任意元素的指针和指向数组后面第 1 个位置的指针有效。
- 不要解引用未初始化的指针。指针未初始化时，其指向的地址不确定，如果解引用（例如`int *pt; *pt = 5`），则可能不会出错，可能会擦写数据或代码，或导致程序崩溃。
- 如果一个函数按值传递数组，则必须分配足够的空间来储存原数组的副本，然后把原数组所有的数据拷贝至新的数组中。如果把数组的地址传递给函数，让函数直接处理原数组则效率要高。意思是，如果函数的参数是数组，传值时会传递整个数组，使用指针则只传递数组首元素的地址。

#### 3、指针和多维数组

- 假设有`int zippo[4][2]`，那么，`zippo`的是`int *[]`类型的指针，`zippo[0]`是`int *`类型的指针

- `zippo[0]`和`*zippo`完全相同，`zippo + 1`操作后指向第三个元素的地址。

- 声明一个指向二维数组的指针：`int (*p)[2] = zippo;`注意：`int *p[2]`表示一个内含两个指针元素的数组，一定要加圆括号才表示一个内含两个`int`类型值的数组，因为`[]`的优先级高于`*`

- ```c
  zippo[m][n] == *(*(zippo + m) + n);
  pz[m][n] == *(*(pz + m) + n);
  ```

- `C11`支持变长数组，以下声明正确：`int sum2d(int rows, int cols, int ar[rows][cols]);`)，但是如果把`rows`和`cols`放在`ar`后面则错误，如果省略原型中的形参名，必须使用`*`来代替省略的维度：`int sum2d(int, int, int [*][*]);`其中最左边方括号中的`*`可以省略。
- 变长数组名实际上是一个指针，修改其值会影响原数据
- 变长数组允许动态内存分配，程序可以在运行时指定数组的大小，普通`C`数组都是静态内存分配，即在编译时确定数组的大小。

#### 4、复合字面量

> 不指定数组名，将类型定义使用括号括起来，例如: `(int [2]){1,2}`，其中的数组长度可以省略。

- 用法1，使用指针记录地址：`int *pt1; pt1 = (int[]){10, 20};`，假设是二维数组：

```c
int main(void) {
    int *pt1;
    pt1 = (int[]){10, 20};
    printf("%d\n", pt1[0]);
    int (*pt2)[2];
    pt2 = (int [][2]){{1, 2}, {3, 4}};
    printf("%d\n", pt2[1][1]);
    return 0;
}
```

- 用法2：作为实际参数传递给带有匹配形式参数的函数，例如：

```c
inc((int[]){1, 2}, 2);
void inc(int array[], const int n) {
    for (int i = 0; i < n; ++i) {
        array[i]++;
    }
}
```

### 七、数组

#### 1、初始化

- 可以通过类似这种方式初始化指定位置的值：`int nums[4] = {[3] = 12};`其余的值被初始化为0。
- `C`语言中，数组和下标可以互换。

### 八、字符串

#### 1、字符串的表示

> 特别注意字符数组和字符串的区别。字符串的末尾一定有`\0`

```c
#include <stdio.h>
#define MAX_LENGTH 81
#define MSG "这是一段文本"

int main(void) {
    char words[MAX_LENGTH] = "I am a string in an array.";
    const char *pt1 = "Something is pointing at me.";
    puts("Here ara some strings:");
    puts(MSG);
    puts(words);
    puts(pt1);
    words[8] = 'p';
    puts(words);
    return 0;
}
```

- 字符串字面量（字符串常量）：使用双引号括起来的内容，编译器会在末尾自动加上`\0`。如果字符串字面量之间没有间隔，或者使用空白字符分隔，会将其视为串起来的字符串常量。例如: `char words[MAX_LENGTH] = "I am a string in an array." "--//";`。字符串常量属于静态存储类别。用双引号括起来的内容被视为指向该字符串储存位置的指针。这类似于把数组名作为指向该数组位置的指针。
- `char`类型数组
- 指向`char`的指针

#### 2、注意事项

- 字符数组名和其他数组名一样，是该数组首元素的地址。
- 使用字符数组创建的字符串：
  - 当把程序载入内存中时，也载入了程序中的字符串。字符串存储在静态存储区中，但是，在程序开始运行时才会为该数组分配内存，此时才将字符串拷贝到数组中，注意，这个时候字符串有两个副本，一个是在静态内存中的字符串字面量，另一个是存储在数组中的字符串。
  - 在数组形式中，数组名是地址常量，不能更改，可以进行`ar1 + 1`操作，但是不能进行`++`操作（递增运算符和递减运算符只能用于可修改的左值，不能用于常量）
- 使用指针形式创建字符串
  - 也使得编译器为字符串在静态存储区域预留相应元素的空间，一旦开始执行程序，它会为指针变量留出一个存储位置，并把字符串的地址存储在指针变量中，该变量最初指向该字符串的首字符，但是它的值可以改变，因此，可以使用递增和递减运算符。
  - 字符串字面量被视为`const`数据，应该把`pt1`声明为指向`const`数据的指针，这意味着不能使用`pt1`修改它所指向的数据，但是仍然可以改变`pt1`的值，如果把一个字符串字面量拷贝给一个数组，就可以随意改变数据，除非把数组声明为`const`
- 总结：初始化数组把静态存储区域的字符串拷贝到数组中，初始化指针只把字符串的地址拷贝给指针。数组名是常量，指针名是变量。
- 相同的字面量存储在相同的位置。
- 静态数据使用的内存与`ar`使用的动态内存不同。

#### 3、字符数组

- 使用指针创建

```c
#include <stdio.h>
#define MAX_LENGTH 81
#define MSG "abcdef"

int main(void) {
    const char *pstrs[] = {
        "apple",
        "red",
        "green",
        "blue"
    };
    char strs[][10] = {
        "apple",
        "red",
        "green",
        "blue"
    };
    for (int i = 0; i < 4; ++i) {
        puts(pstrs[i]);
    }
    printf("\n");
    for (int i = 0; i < 4; ++i) {
        puts(strs[i]);
    }
    printf("\nsizeof　pstrs:　%zd,　sizeof　strs:　%zd\n",
           sizeof(pstrs), sizeof(strs));

    return 0;
}
```

### 九、存储类别、链接和内存管理

- 数据存储在内存中，被储存的每个值都占用一定的物理内存，C 语言把这样的一块内存称为对象，对象可以储存一个或多个值。
- 标识符是一个名称，在这种情况下，标识符可以用来指定特定对象的内容，变量名不是指定对象的唯一途径。

- 可以使用作用域和链接描述标识符，标识符的作用域和链接表明了程序的哪些部分可以使用它。
- 使用存储期描述对象，所谓存储期是指对象在内存中保留了多长时间。
- 不同的存储类别具有不同的存储期、作用域和链接。

#### 1、作用域

- 一个C变量的作用域可以是块作用域、函数作用域、函数原型作用域或文件作用域
- 函数作用域（function scope）仅用于`goto`语句的标签。
- 函数原型作用域（function prototype scope）用于函数原型中的形参名（变量名）
- 变量的定义在函数的外面，具有文件作用域（file scope）.具有文件作用域的变量，从它的定义处到该定义所在文件的末尾均可见。编译器源代码文件和所有的头文件都看成是一个包含信息的单独文件。这个文件被称为翻译单元。描述一个具有文件作用域的变量时，它的实际可见范围是整个翻译单元。如果程序由多个源代码文件组成，那么该程序也将由多个翻译单元组成。每个翻译单元均对应一个源代码文件和它所包含的文件。

#### 2、链接

- C 变量有 3 种链接属性：外部链接、内部链接或无链接。
- 具有块作用域、函数作用域或函数原型作用域的变量都是无链接变量。这意味着这些变量属于定义它们的块、函数或原型私有。
- 具有文件作用域的变量可以是外部链接或内部链接。外部链接变量可以在多文件程序中使用，内部链接变量只能在一个翻译单元中使用。
- 使用`static`修饰的文件作用域变量具有内部链接，否则是外部链接

#### 3、存储期

##### 静态存储期

- 如果对象具有静态存储期，那么它在程序的执行期间一直存在。文件作用域变量具有静态存储期。注意，对于文件作用域变量，关键字 `static`表明了其链接属性，而非存储期。以 `static`声明的文件作用域变量具有内部链接。但是无论是内部链接还是外部链接，所有的文件作用域变量都具有静态存储期。

##### 线程存储期

- 线程存储期用于并发程序设计，程序执行可被分为多个线程。具有线程存储期的对象，从被声明时到线程结束一直存在。以关键字`_Thread_local`声明一个对象时，每个线程都获得该变量的私有备份。

##### 自动存储期

- 块作用域的变量通常都具有自动存储期。当程序进入定义这些变量的块时，为这些变量分配内存；当退出这个块时，释放刚才为变量分配的内存。这种做法相当于把自动变量占用的内存视为一个可重复使用的工作区或暂存区。
- 变长数组稍有不同，它们的存储期从声明处到块的末尾，而不是从块的开始处到块的末尾。
- 块作用域变量也能具有静态存储期。为了创建这样的变量，要把变量声明在块中，且在声明前加上关键字`static`

##### 动态分配存储期

- 静态存储类别所用的内存数量在编译时确定，只要程序还在运行，就可访问储存在该部分的数据。该类别的变量在程序开始执行时被创建，在程序结束时被销毁。
- 使用动态内存通常比使用栈内存慢。
- 程序把静态对象、自动对象和动态分配的对象储存在不同的区域。

#### 4、存储类别

| 存储类别     | 存储期 | 作用域 | 链接 | 声明方式                       |
| ------------ | ------ | ------ | ---- | ------------------------------ |
| 自动         | 自动   | 块     | 无   | 块内                           |
| 寄存器       | 自动   | 块     | 无   | 块内，使用关键字`register`     |
| 静态外部链接 | 静态   | 文件   | 外部 | 所有函数外                     |
| 静态内部链接 | 静态   | 文件   | 内部 | 所有函数外，使用关键字`static` |
| 静态无链接   | 静态   | 块     | 无   | 块内，使用关键字`static`       |

##### 自动变量

- 默认情况下，声明在块或函数头中的任何变量都属于自动存储类别。为了更清楚地表达你的意图（例如，为了表明有意覆盖一个外部变量定义，或者强调不要把该变量改为其他存储类别），可以显式使用关键字`auto`。
- 关键字`auto`是存储类别说明符。`auto`关键字在`C++`中的用法完全不同，如果编写`C/C++`兼容的程序，最好不要使用`auto`作为存储类别说明符。
- 自动变量不会初始化，除非显示初始化它。

##### 寄存器变量

- 寄存器变量储存在CPU的寄存器中，或者概括地说，储存在最快的可用内存中。与普通变量相比，访问和处理这些变量的速度更快。由于寄存器变量储存在寄存器而非内存中，所以无法获取寄存器变量的地址。绝大多数方面，寄存器变量和自动变量都一样。也就是说，它们都是块作用域、无链接和自动存储期。使用存储类别说明符`register`便可声明寄存器变量。
- 注意，使用该关键字只是请求让这个变量成为寄存器变量，不一定会成功，编译器必须根据寄存器或最快可用内存的数量衡量你的请求，或者直接忽略你的请求。在这种情况下，寄存器变量就变成普通的自动变量。即使是这样，仍然不能对该变量使用地址运算符。
- 在函数头中使用关键字`register`，便可请求形参是寄存器变量。
- 可声明为`register`的数据类型有限。例如，处理器中的寄存器可能没有足够大的空间来储存`double`类型的值。

##### 块作用域的静态变量

- 静态的意思是该变量在内存中原地不动，并不是说它的值不变。

- 具有文件作用域的变量自动具有（也必须是）静态存储期。

  ```c
  void trystat(void);
  int main(int argc, char **argv) {
      trystat();
      trystat();
      trystat();
      return 0;
  }
  
  void trystat(void)
  {
      int fade = 1;
      static int stay = 1;
      printf("fade = %d and stay = %d\n", fade++, stay++);
  }
  // 结果
  fade = 1 and stay = 1
  fade = 1 and stay = 2
  fade = 1 and stay = 3
  ```

  - 静态变量只在编译`trystat()`时被初始化一次，如果未显示初始化静态变量，会被初始化为0。
  - `int fade = 1;`声明是函数的一部分，每次调用这个函数都会执行这条声明。这是运行时行为。
  - `static int stay = 1;`这条声明实际上不是函数的一部分，这是因为静态变量和外部变量在程序被加载时已经执行完毕。把这条声明放在函数中是为了告诉编译器只有这个函数才能看到该变量。这一条声明没有在运行时执行。
  - 不能在函数的形参中使用`static`

##### 外部链接的静态变量

- 该类别有时称为外部存储类别，属于该类别的变量称为外部变量。把变量的定义性声明放在在所有函数的外面便创建了外部变量。当然，为了指出该函数使用了外部变量，可以在函数中用关键字`extern`再次声明。如果一个源代码文件使用的外部变量定义在另一个源代码文件中，则必须用`extern`在该文件中声明该变量。
- 注意，外部变量的作用域是从声明处到文件结尾，所以声明之前的函数不可见。
- 只能使用常量表达式初始化文件作用域变量。（只要不是变长数组，`sizeof`表达式可被视为常量表达式。）
- 外部变量只能初始化一次，且必须在定义该变量时进行。

##### 内部链接的静态变量

- 使用`static`修饰

- 普通的外部变量可用于同一程序中任意文件中的函数，但是内部链接的静态变量只能用于同一个文件中的函数。
- 只有当程序由多个翻译单元组成时，才体现区别内部链接和外部链接的重要性。

##### 多文件

- C通过在一个文件中进行定义式声明，然后在其他文件中进行引用式声明来实现共享。也就是说，除了一个定义式声明外，其他声明都要使用`extern`关键字。而且，只有定义式声明才能初始化变量。
- 注意，如果外部变量定义在一个文件中，那么其他文件在使用该变量之前必须先声明它（用 `extern`关键字）。也就是说，在某文件中对外部变量进行定义式声明只是单方面允许其他文件使用该变量，其他文件在用`extern`声明之前不能直接使用它。

##### 存储类别和函数

- 外部函数（默认`extern`关键字）
  - 可以被其它文件的函数访问
- 静态函数（使用`static`修饰）
  - 只能用于其定义所在的文件，可以避免函数重名问题
  - 通常的做法是：用 extern 关键字声明定义在其他文件中的函数。这样做是为了表明当前文件中使用的函数被定义在别处。除非使用`static`关键字，否则一般函数声明都默认为`extern`。
- 内联函数（`c99`新增）
- 保护性程序设计的黄金法则是：“按需知道”原则。尽量在函数内部解决该函数的任务，只共享那些需要共享的变量。

##### 分配内存

###### `malloc`

- 该函数接受一个参数：所需的内存字节数。`malloc()`函数会找到合适的空闲内存块，这样的内存是匿名的。也就是说， `malloc()`分配内存，但是不会为其赋名。然而，它确实返回动态分配内存块的首字节地址。因此，可以把该地址赋给一个指针变量，并使用指针访问这块内存。
- 从ANSI C标准开始，C使用一个新的类型：指向void的指针。该类型相当于一个“通用指针”。`malloc()`函数可用于返回指向数组的指针、指向结构的指针等，所以通常该函数的返回值会被强制转换为匹配的类型。在ANSI C中，应该坚持使用强制类型转换，提高代码的可读性。
- 在`c++`中必须使用强制类型转换，`c`中不强制
- 如果 `malloc()`分配内存失败，将返回空指针。
- `malloc()`要与`free()`配套使用。
- 声明二维数组：```int (*ptl)[m] = (int (*)[m]) malloc(n * m * sizeof(int));`

###### `free()`

- 函数的参数是之前`malloc()`返回的地址，该函数释放之前`malloc()`分配的内存。
- 不能用 `free()`释放通过其他方式（如，声明一个数组）分配的内存。
- 静态内存的数量在编译时是固定的，在程序运行期间也不会改变。自动变量使用的内存数量在程序执行期间自动增加或减少。但是动态分配的内存数量只会增加，除非用 `free()`进行释放。
- 不能释放同一块内存两次

###### `calloc`

- 如果要储存不同的类型，应使用强制类型转换运算符。
- 1个参数是所需的存储单元数量，第2个参数是存储单元的大小（以字节为单位）。
- 它把块中的所有位都设置为0。
- free()函数也可用于释放`calloc()`分配的内存。

### 十、文件输入输出

#### 1、标准文件

- C程序会自动打开3个文件，它们被称为标准输入（standard input）、标准输出（standard output）和标准错误输出（standard error output）。

#### 2、`fopen`

- `r`：以读模式打开
- `w`：以写模式打开，把现有文件的长度截为0，如果文件不存在，则创建一个新文件。
- `a`：以写模式打开，在现有文件末尾添加内容，如果文件不存在，则创建一个新文件。
- `r+`：以更新模式打开文件（即可以读写文件）
- `w+`：以更新模式打开文件（即，读和写），如果文件存在，则将其长度截为0；如果文件不存在，则创建一个新文件
- `a+`：以更新模式打开文件（即，读和写），在现有文件的末尾添加内容，如果文件不存在，则创建一个新文件；可以读整个文件，但只能从末尾添加内容。
- `rb, wb, ab, ab+, a+b, wb+, w+b, ab+, a+b`：与上一个模式类似，但是以二进制模式而不是文本模式打开文件。
- `wx, wbx, w+x, wb+x`或`w+bx`：（`c11`）类似非`x`模式，但是如果文件已存在或以独占模式打开文件，则打开文件失败。
- 像`UNIX`和`Linux`这样只有一种文件类型的系统，带b字母的模式和不带b字母的模式相同。
- 新的`C11`新增了带x字母的写模式，与以前的写模式相比具有更多特性。第一，如果以传统的一种写模式打开一个现有文件，`fopen()`会把该文件的长度截为 0，这样就丢失了该文件的内容。但是使用带` x`字母的写模式，即使`fopen()`操作失败，原文件的内容也不会被删除。第二，如果环境允许，`x`模式的独占特性使得其他程序或线程无法访问正在被打开的文件。
- 警告：如果使用任何一种`w`模式（不带`x`）打开一个现有文件，该文件的内容会被删除。

#### 3、`fseek`

- `SEEK_END`：定位到文件末尾

### 十一、结构体

#### 1、创建结构体

```c
struct book {
    char title[MAX_TITLE];
    char author[MAX_AUTL];
    float value;
};
```

- 其中结构的标记名（即`book`）是可选的。还可以：

```c
struct {
    char *title;
    char *author;
    float value;
} typedef book;
```

- 则声明变量无需`struct`关键字：`book library;`

#### 2、声明结构体变量

```c
struct book library; /* 把 library 声明为一个 book 类型的变量 */
```

- 声明结构的过程和定义结构变量的过程可以组合成一个步骤。

#### 3、访问成员变量

- 使用`.`运算符即可
- 如果使用指针，则使用`->`运算符：`barney.income == (*him).income == him->income // 假设 him == &barney`

#### 4、作用域

- 和变量的作用域类似

#### 5、初始化结构体

- 初始化一个结构变量与初始化数组的语法类似：

```c
book bk = {
    "西游记",
    "吴承恩",
    99.9
};
```

- 使用逗号分隔即可，不强制要求独占一行。

- 只想初始化部分字段：

```c
book l1 = {
    .value = 20,
    .author = "//-/"
};
```

#### 6、符合字面量和结构`c99`

- 如果只需要一个临时结构值，复合字面量很好用。例如，可以使用复合字面量创建一个数组作为函数的参数或赋给另一个结构。语法是把类型名放在圆括号中，后面紧跟一个用花括号括起来的初始化列表。
- 示例：`(struct book) {"The Idiot", "Fyodor Dostoyevsky", 6.99}`

#### 7、伸缩型数组成员

- 第1个特性是，该数组不会立即存在。第2个特性是，使用这个伸缩型数组成员可以编写合适的代码，就好像它确实存在并具有所需数目的元素一样。
- 伸缩型数组成员必须是结构的最后一个成员
- 结构中必须至少有一个成员
- 伸缩数组的声明类似于普通数组，只是它的方括号中是空的

```c
struct flex {
    int count;
    double average;
    double scores[];　// 伸缩型数组成员
};
```

- 实际上，`C99`的意图并不是让你声明`struct flex`类型的变量，而是希望你声明一个指向`struct flex`类型的指针，然后用`malloc()`来分配足够的空间，以储存`struct flex`类型结构的常规内容和伸缩型数组成员所需的额外空间。例如，假设用`scores`表示一个内含5个`double`类型值的数组，可以这样做：

```c
struct flex * pf; // 声明一个指针
// 请求为一个结构和一个数组分配存储空间
pf = malloc(sizeof(struct flex) + 5 * sizeof(double));
```

- 现在有足够的存储空间储存`count`、`average`和一个内含5个`double`类型值的数组。可以用指针访问这些成员
- 带伸缩型数组成员的结构确实有一些特殊的处理要求。第一，不能用结构进行赋值或拷贝（因为只能拷贝除伸缩型数组成员以外的其他成员，确实要进行拷贝，应使用`memcpy()`函数）
- 第二，不要以按值方式把这种结构传递给结构。
- 第三，不要使用带伸缩型数组成员的结构作为数组成员或另一个结构的成员。
- 这种类似于在结构中最后一个成员是伸缩型数组的情况，称为`struct hack`。除了伸缩型数组成员在声明时用空的方括号外，`struct hack`特指大小为0的数组。然而，`struct hack`是针对特殊编译器`（GCC）`的，不属于C标准。这种伸缩型数组成员方法是标准认可的编程技巧。

```c
#include <stdio.h>
#include <stdlib.h>

struct {
    int n;
    double avg;
    double nums[];
} typedef book;

void printBook(book *bk, int n);

int main(void) {
    book *bk1, *bk2;
    printBook(bk1, 5);
    printBook(bk2, 9);
    return 0;
}

void printBook(book *bk, const int n) {
    bk = malloc(sizeof(book) + n * sizeof(double));
    bk->n = n;
    double sum = 0;
    for (int i = 0; i < n; ++i) {
        bk->nums[i] = i * 10 + i / 10.0;
        sum += bk->nums[i];
    }
    bk->avg = sum / n;
    printf("n = %d, avg = %.2f\n", n, bk->avg);
    free(bk);
}
```

#### 8、匿名结构

- 示例：

```c
#include <stdio.h>
#include <string.h>

struct person {
    int id;

    struct {
        char first[20];
        char last[20];
    };
};

int main(void) {
    struct person p;
    strcpy(p.first, "xxx");
    strcpy(p.last, "qqq");
    p.id = 10;
    printf("first = %s\n", p.first);
    printf("last = %s\n", p.last);
    printf("id = %d\n", p.id);
    return 0;
}
```

- 如上，可以直接在一个结构体内定义另外一个结构体，访问时简化了步骤。（但是不能重名）

#### 9、联合`union`

##### 定义

```c
union hold {
    int digit;
    double bigfl;
    char letter;
};
```

- 根据以上形式声明的结构可以储存一个`int`类型、一个`double`类型和`char`类型的值。然而，声明的联合只能储存一个`int`类型的值或一个`double`类型的值或`char`类型的值。

##### 声明

```c
#include <stdio.h>

union hold {
    int digit;
    double bigfl;
    char letter;
};

int main(void) {
    union hold fit; // hold类型的联合变量
    union hold save[10]; // 内含10个联合变量的数组
    union hold *pu; // 指向hold类型联合变量的指针
    printf("%zd\n", sizeof(fit)); // 8
    printf("%zd\n", sizeof(save)); // 80
    return 0;
}
```

- 编译器分配足够的空间以便它能储存联合声明中占用最大字节的类型。

##### 初始化

- 可以初始化联合。需要注意的是，联合只能储存一个值，这与结构不同。
- 有 3 种初始化的方法：把一个联合初始化为另一个同类型的联合；初始化联合的第1个元素；或者根据`C99`标准，使用指定初始化器：

```c
union hold valA;
valA.letter = 'R';
union hold valB = valA; // 使用另外一个联合来初始化
union hold valC = {88}; // 初始化联合的digit成员
union hold valD = {.bigfl = 118.2}; //指定初始化器
```

##### 用法

```c
union hold fit;
fit.digit = 23; // 把23存储在tit, 占用4字节
fit.bigfl = 2.0; // 清除23, 存储2.0, 占用8字节
fit.letter = 'H'; // 清除2.0, 储存H, 占用1字节
```

- 点运算符表示正在使用哪种数据类型。在联合中，一次只储存一个值。即使有足够的空间，也不能同时储存一个`char`类型值和一个`int`类型值。
- 和用指针访问结构使用->运算符一样，用指针访问联合时也要使用->运算符
- 用一个成员把值储存在一个联合中，然后用另一个成员查看内容，这种做法有时很有用
- 联合的另一种用法是，在结构中储存与其成员有从属关系的信息。

##### 匿名`union`

- 匿名联合和匿名结构的工作原理相同，即匿名联合是一个结构或联合的无名联合成员。

#### 10、枚举`enum`

- 可以使用枚举类型声明符号名称来表示整型常量。使用`enum`关键字，可以创建一个新"类型"并指定它可具有的值。
- 如果不想创建枚举变量，就不用在声明中使用标记。

##### 定义

```c
enum spectrum {
    red, orange, yellow, green, blue, violet
};
enum spectrum color;
```

- 第1个声明创建了`spetrum`作为标记名，允许把`enum spetrum`作为一个类型名使用。
- 第2个声明使`color`作为该类型的变量。第1个声明中花括号内的标识符枚举了`spectrum`变量可能有的值。
- 虽然枚举符（如red和blue）是`int`类型，但是枚举变量可以是任意整数类型，前提是该整数类型可以储存枚举常量。
- C允许枚举变量使用++运算符，但是C++标准不允许。

##### 赋值

- 默认情况下，枚举列表中的常量都被赋予0、1、2等，依次递增
- 在枚举声明中，可以为枚举常量指定整数值：

```c
enum levels {
    low = 100, medium = 500, high = 2000, max = yellow
};
```

- 如果只给一个枚举常量赋值，没有对后面的枚举常量赋值，那么后面的常量会被赋予后续的值。

#### 11、共享名称空间

- C语言使用名称空间`（namespace）`标识程序中的各部分，即通过名称来识别。
- 作用域是名称空间概念的一部分：两个不同作用域的同名变量不冲突；两个相同作用域的同名变量冲突。
- 名称空间是分类别的。在特定作用域中的结构标记、联合标记和枚举标记都共享相同的名称空间，该名称空间与普通变量使用的空间不同。这意味着在相同作用域中变量和标记的名称可以相同，不会引起冲突，但是不能在相同作用域中声明两个同名标签或同名变量。

#### 12、`typedef`

- `typedef`工具是一个高级数据特性，利用`typedef`可以为某一类型自定义名称。
- 与`#define`不同，`typedef`创建的符号名只受限于类型，不能用于值。
- `typedef`由编译器解释，不是预处理器。
- 在其受限范围内，`typedef`比`#define`更灵活。
- 该定义的作用域取决于`typedef`定义所在的位置。如果定义在函数中，就具有局部作用域，受限于定义所在的函数。如果定义在函数外面，就具有文件作用域。
- `typedef`中使用的名称遵循变量的命名规则。

#### 13、函数和指针

- 声明一个函数指针时，必须声明指针指向的函数类型。为了指明函数类型，要指明函数签名，即函数的返回类型和形参类型。示例：

```c
void toUpper(char *); // 函数原型
void (*pf)(char *); // 指向函数的指针
pf = toUpper; // 赋值
char mis[] = "Hello";
(*pf)(mis); // 用法一
pf(mis); // 用法2
```

- `toUpper`参数是`char *`，返回值是`void`

- 如果想声明一个指向某类型函数的指针，可以写出该函数的原型后把函数名替换成`(*pf)`形式的表达式，创建函数指针声明。
- 通常情况下，函数名就是函数的地址，可以直接赋值给函数指针。（前提是指针类型匹配）
- 用法一：由于`pf`表示函数的地址，因此`*pf`就表示这个函数，`(*pf)(mis); `和`toUpper(mis)`等价。
- 用法二：由于函数名是指针，那么函数名和指针可以互换使用，所以`pf(mis)`和`toUpper(mis)`等价。
- 虽然没有函数数组，但是可以有函数指针数组。

### 十二、位操作

#### 1、二进制整数

- 1字节有8位，从左到右编号为7-0， 其中7被称为高价位，0被称为低阶位

#### 2、按位运算符

- 按位逻辑运算符用于整型数据，包括`char`。
- `~`，二进制反码或按位取反，把1变为0，0变为1。

```
~(10011010) -> 01100101
```

- 按位与`&`
- 按位或`|`
- 按位异或`^`，即不相同为真

#### 3、设置位

- 打开位：`flag & MASK`
- 关闭位：`flag & ~MASK`
- 切换位：`flag ^= MASK`
- 检查位：`(flag & MASK) == MASK`

#### 4、移位运算符

- 左移`<<`，将其左侧运算对象的每一位向左移动指定位数，高位舍弃，低位补零。
- 右移`>>`，对无符号类型，高位补零，低位舍弃，有符号类型，其结果取决于机器。

#### 5、位字段

- 位字段是一个`signed int`或`unsigned int`类型变量中的一组相邻的位（`C99和C11`新增了`_Bool`类型的位字段）。位字段通过一个结构声明来建立，该结构声明为每个字段提供标签，并确定该字段的宽度。

```c
struct {
    unsigned int autfd: 1;
    unsigned int bldfc: 1;
    unsigned int undln: 1;
    unsigned int itals: 1;
} typedef prnt;

int main(void) {
    prnt num;
    num.itals = 0;
    num.undln = 1;
    return 0;
}
```

- 由于每个字段恰好为1位，所以只能为其赋值1或0。变量被储存在`int`大小的内存单元中，但是在本例中只使用了其中的4位。
- 带有位字段的结构提供一种记录设置的方便途径。许多设置（如，字体的粗体或斜体）就是简单的二选一。例如，开或关、真或假。如果只需要使用 1 位，就不需要使用整个变量。内含位字段的结构允许在一个存储单元中储存多个设置。
- 有时，某些设置也有多个选择，因此需要多位来表示。
- 如果声明的总位数超过了一个`unsigned int`类型的大小，会用到下一个`unsigned int`类型的存储位置，一个字段不允许跨越两个`unsigned int`之间的边界。编译器会自动移动跨界的字段，保持`unsigned int`的边界对齐。一旦发生这种情况，第1个`unsigned int`中会留下一个未命名的“洞”。可以用未命名的字段宽度“填充”未命名的“洞”。
- 使用一个宽度为0的未命名字段迫使下一个字段与下一个整数对齐：

```c
struct {
    unsigned int field1: 1;
    unsigned int : 2;
    unsigned int field2: 1;
    unsigned int : 0;
    unsigned int field3: 1;
} typedef stuff;
```

- 这里，在`stuff.field1`和`stuff.field2`之间，有一个2位的空隙；`stuff.field3`将储存在下一个`unsigned int`中。
- 通常，把位字段作为一种更紧凑储存数据的方式。
- C 以`unsigned int`作为位字段结构的基本布局单元。因此，即使一个结构唯一的成员是1位字段，该结构的大小也是一个`unsigned int`类型的大小
- 警告：位字段和位的位置之间的相互对应因实现而异。

#### 6、对齐特性

- `C11 `的对齐特性比用位填充字节更自然，它们还代表了C在处理硬件相关问题上的能力。在这种上下文中，对齐指的是如何安排对象在内存中的位置。
- `C11`在`stdlib.h`库还添加了一个新的内存分配函数，用于对齐动态分配的内存。该函数的原型如下：`void *_aligned_malloc(size_t _Size, size_t _Alignment) `第1个参数代表指定的对齐，第2个参数是所需的字节数，其值应是第1个参数的倍数。与其他内存分配函数一样，要使用`_aligned_free`函数释放之前分配的内存。

### 十三、`C`预处理器和`C`库

#### 1、翻译程序的过程

- 在预处理之前，编译器必须对该程序进行一些翻译处理。首先，编译器把源代码中出现的字符映射到源字符集。该过程处理多字节字符和三字符序列
- 第二，编译器定位每个反斜杠后面跟着换行符的实例，并删除它们。
- 第三，编译器把文本划分成预处理记号序列、空白序列和注释序列（记号是由空格、制表符或换行符分隔的项）。这里要注意的是，编译器将用一个空格字符替换每一条注释。而且，实现可以用一个空格替换所有的空白字符序列（不包括换行符）。
- 最后，程序已经准备好进入预处理阶段，预处理器查找一行中以#号开始的预处理指令。

#### 2、明示常量：`#define`

- 指令可以出现在源文件的任何地方，其定义从指令出现的地方到该文件末尾有效。
- 预处理器指令从`#`开始运行，到后面的第1个换行符为止。也就是说，指令的长度仅限于一行。然而，前面提到过，在预处理开始前，编译器会把多行物理行处理为一行逻辑行。可以使用反斜杠`\`把定义延续到下一行
- 每个`#define`都有三部分组成。第一部分是`#define`指令本身；第二部分是选定的缩写，也称为宏。有些宏代表值（如本例），这些宏被称为类对象宏（object-like macro）。C 语言还有类函数宏（function-like macro）。宏的名称中不允许有空格，而且必须遵循C变量的命名规则；第三部分（指令行的其余部分）称为替换列表或替换体。一旦预处理器在程序中找到宏的示例后，就会用替换体代替该宏（也有例外）。从宏变成最终替换文本的过程称为宏展开（macro expansion）。特别注意：注释会被一个空格替代。
- 预处理器不做计算，不对表达式求值，它只进行替换。
- 注意，宏定义还可以包含其他宏（一些编译器不支持这种嵌套功能）。
- 如果确实需要重定义常量，使用`const`关键字和作用域规则更容易些。
- 带参数：`#define PSQR(X) printf("The square of X is %d.\n", ((X)*(X)));`使用宏：`PSQR(8);`
- `C`允许在字符串中包含宏参数。在类函数宏的替换体中，#号作为一个预处理运算符，可以把记号转换成字符串。这个过程称为字符串化`（stringizing）`。

```c
#include <stdio.h>
#define PSQR(x) printf("The square of "#x" is %d.\n", ((x)*(x)))

int main(void) {
    int y = 5;
    PSQR(y);
    PSQR(2 + 4);
    return 0;
}
/*
The square of y is 25.
The square of 2 + 4 is 36.
*/
```

#### 3、预处理器黏合剂：`##`运算符

- 该运算符可用于类函数宏的替换部分。而且，`##`还可用于对象宏的替换部分。`##`运算符把两个记号组合成一个记号。

```c
#include <stdio.h>
#define XNAME(n) x ## n
#define PRINT_XN(n) printf("x"#n" = %d\n", x##n)

int main(void) {
    int XNAME(1) = 14; // 变成int x1 = 14;
    int XNAME(2) = 20; // 变成int x2 = 20;
    int x3 = 30;
    PRINT_XN(1);
    PRINT_XN(2);
    PRINT_XN(3);
    return 0;
}
```

#### 4、变参宏：`...`和`__VA_ARGS__`

- 通过把宏参数列表中最后的参数写成省略号（即，3个点...）来实现这一功能。
- 这样，预定义宏`_ _VA_ARGS_ _`可用在替换部分中，表明省略号代表什么。示例：`#define PR(...) printf(_ _VA_ARGS_ _)`

#### 5、宏和函数的选择

- 宏生成内联代码，即在程序中生成语句。如果调用20次宏，即在程序中插入20行代码。如果调用函数20次，程序中只有一份函数语句的副本，所以节省了空间。然而另一方面，程序的控制必须跳转至函数内，随后再返回主调程序，这显然比内联代码花费更多的时间。
- 宏的一个优点是，不用担心变量类型（这是因为宏处理的是字符串，而不是实际的值）。
- 对于简单的函数，通常使用宏
- 用圆括号把宏的参数和整个替换体括起来。这样能确保被括起来的部分正确地展开
- 用大写字母表示宏函数的名称。该惯例不如用大写字母表示宏常量应用广泛。但是，大写字母可以提醒程序员注意，宏可能产生的副作用。
- 如果打算使用宏来加快程序的运行速度，那么首先要确定使用宏和使用函数是否会导致较大差异。在程序中只使用一次的宏无法明显减少程序的运行时间。在嵌套循环中使用宏更有助于提高效率。许多系统提供程序分析器以帮助程序员压缩程序中最耗时的部分。

#### 6、文件包含：`#include`

- 当预处理器发现`#include` 指令时，会查看后面的文件名并把文件的内容包含到当前文件中，即替换源文件中的`#include`指令。这相当于把被包含文件的全部内容输入到源文件`#include`指令所在的位置。
- `#include`指令有两种形式：

```c
#include <stdio.h>　　　　 //文件名在尖括号中
#include "mystuff.h"　　　//文件名在双引号中
```

- 在` UNIX` 系统中，尖括号告诉预处理器在标准系统目录中查找该文件。双引号告诉预处理器首先在当前目录中（或文件名中指定的其他目录）查找该文件，如果未找到再查找标准系统目录
- 包含一个大型头文件不一定显著增加程序的大小。在大部分情况下，头文件的内容是编译器生成最终代码时所需的信息，而不是添加到最终代码中的材料。

#### 7、其它指令

- `#undef`，取消之前的`#define`定义：如果想使用一个名称，又不确定之前是否已经用过，为安全起见，可以用`#undef `指令取消该名字的定义。
- `#if`、`#ifdef`、`#ifndef`、`#else`、`#elif`和`#endif`指令用于指定什么情况下编写哪些代码。
  - `#ifdef #else`很像C的`if else`。两者的主要区别是，预处理器不识别用于标记块的花括号（{}），因此它使用`#else`（如果需要）和`#endif`（必须存在）来标记指令块。这些指令结构可以嵌套。也可以用这些指令标记C语句块
  - `#if`指令很像C语言中的if。#if后面跟整型常量表达式，如果表达式为非零，则表达式为真。可以在指令中使用C的关系运算符和逻辑运算符。可以按照`if else`的形式使用`#elif`（早期的实现不支持`#elif`）。
  - 较新的编译器提供另一种方法测试名称是否已定义，即用`#if defined (VAX)`代替`#ifdef VAX。`
- `#line`，重置`__LINE__`和`__FILE__`宏报告的行号和文件名。示例：

```c
#line 1000 // 把当前行号重置为1000
#line 10 "cool.c" // 把行号重置为10，把文件名重置为cool.c
```

- `#error`让预处理器发出一条错误信息，该消息包含指令中的文本。如果可能的话，编译过程应该中断。示例：

```c
#if __STDC_VERSION__ != 201112L
#error Not C11
#endif
// 如果编译器只支持旧标准，则会编译失败，如果支持C11标准，就能成功编译。
```

- `#pragma`用于向编译器发出指令，在现在的编译器中，可以通过命令行参数或`IDE`菜单修改编译器的一些设置。`#pragma`把编译器指令放入源代码中。例如，在开发`C99`时，标准被称为`C9X`，可以使用下面的编译指示`（pragma）`让编译器支持`C9X`：`#pragma c9x on`。

  - 一般而言，编译器都有自己的编译指示集。例如，编译指示可能用于控制分配给自动变量的内存量，或者设置错误检查的严格程度，或者启用非标准语言特性等。
  - `C99`还提供`_Pragma`预处理器运算符，该运算符把字符串转换为普通的编译指示。示例：

  ```c
  _Pragma("nonstandardtreatmenttypeB on")
  ```

  - 等价于指令：`#pragma nonstandardtreatmenttypeB on`
  - 由于该运算符不使用`#`符号，所以可以把它作为宏展开的一部分

#### 8、预定义宏

- `__DATE__`预处理的日期，`Mmm dd yyyy`形式的字符串字面量。
- `__FILE__`表示当前源代码文件名的字符串字面量
- `__LINE__`表示当前源代码文件中行号的整型常量
- `__STDC__`设置为1时，表明实现遵循C标准
- `__STDC_HOSTED__`本机环境设置为1，否则设置为0
- `__STDC_VERSION__`支持`C99`标准，设置为`199901L`；支持`C11`标准，设置为`201112L`
- `__TIME__`翻译代码的时间，格式为`hh:mm:ss`
- `__func__`，代表函数名的字符串，具有函数作用域。（是预定义标识符）

#### 9、泛型选择

- `C11`新增了一种表达式，叫作泛型选择表达式，可根据表达式的类型选择一个值。泛型选择表达式不是预处理器指令，但是在一些泛型编程中它常用作`#define`宏定义的一部分。
- 示例：

```c
_Generic(x, int: 0, float: 1, double: 2, default: 3)
```

- `_Generic`是`C11`的关键字。后面的圆括号中包含多个使用逗号分隔的项。第一个项是表达式，后面每一个项都是由一个类型、一个冒号和一个值组成。第一个项的类型匹配哪个标签，整个表达式的值是该标签后面的值。例如，假设`x`是`int`类型的变量，`x`的类型匹配`int:`标签，那么整个表达式的值就是0。如果没有与类型匹配的标签，表达式的值就是`default:`标签后面的值。
- 泛型选择语句与`switch`语句类似，只是前者用表达式的类型匹配标签，而后者使用表达式的值匹配标签。
- 泛型选择语句和宏定义组合的示例：

```c
#include <stdio.h>

struct {
}typedef ST;

#define MY_TYPE(X) _Generic((X), \
    int: "int",\
    float: "float",\
    double: "double",\
    ST: "ST", \
    default: "other"\
    )

int main(void) {
    printf("%s\n", MY_TYPE(5));
    printf("%s\n", MY_TYPE(5L));
    printf("%s\n", MY_TYPE(5.0));
    printf("%s\n", MY_TYPE("112"));
    ST st;
    printf("%s\n", MY_TYPE(st));
    printf("%s\n", MY_TYPE((ST){}));
    return 0;
}
```

- 对一个泛型选择表达式求值时，程序不会先对第一个项求值，它只确定类型。只有匹配标签的类型后才会对表达式求值。

#### 10、内联函数`C99`

- 通常，函数调用都有一定的开销，因为函数的调用过程包括建立调用、传递参数、跳转到函数代码并返回。使用宏使代码内联，可以避免这样的开销。99还提供另一种方法：内联函数
- 函数变成内联函数，编译器可能会用内联代码替换函数调用，并（或）执行一些其他的优化，但是也可能不起作用。
- 标准规定具有内部链接的函数可以成为内联函数，还规定了内联函数的定义与调用该函数的代码必须在同一个文件中
- 最简单的方法是使用函数说明符` inline `和存储类别说明符`static`。通常，内联函数应定义在首次使用它的文件中，所以内联函数也相当于函数原型。
- 由于并未给内联函数预留单独的代码块，所以无法获得内联函数的地址（实际上可以获得地址，不过这样做之后，编译器会生成一个非内联函数）。另外，内联函数无法在调试器中显示。
- 内联函数应该比较短小。把较长的函数变成内联并未节约多少时间，因为执行函数体的时间比调用函数的时间长得多。
- 编译器优化内联函数必须知道该函数定义的内容。这意味着内联函数定义与函数调用必须在同一个文件中。鉴于此，一般情况下内联函数都具有内部链接。因此，如果程序有多个文件都要使用某个内联函数，那么这些文件中都必须包含该内联函数的定义。最简单的做法是，把内联函数定义放入头文件，并在使用该内联函数的文件中包含该头文件即可。
- 一般都不在头文件中放置可执行代码，内联函数是个特例。因为内联函数具有内部链接，所以在多个文件中定义同一个内联函数不会产生什么问题。
- 与C++不同的是，C还允许混合使用内联函数定义和外部函数定义。

#### 11、`_Noreturn`函数`C11`

- `C99`新增`inline`关键字时，它是唯一的函数说明符。（关键字`extern`和`static`是存储类别说明符，可应用于数据对象和函数）。`C11`新增了第2个函数说明符`_Noreturn`，表明调用完成后函数不返回主调函数。`exit()`函数是`_Noreturn` 函数的一个示例，一旦调用`exit()`，它不会再返回主调函数。
- `_Noreturn`的目的是告诉用户和编译器，这个特殊的函数不会把控制返回主调程序。告诉用户以免滥用该函数，通知编译器可优化一些代码。

#### 12、`C`库

- 如何访问C库取决于实现，因此你要了解当前系统的一般情况。首先，可以在多个不同的位置找到库函数。

##### 自动访问

- 在一些系统中，只需编译程序，就可使用一些常用的库函数。
- 记住，在使用函数之前必须先声明函数的类型，通过包含合适的头文件即可完成。
- 过去，不同的实现使用的头文件名不同。ANSI C标准把库函数分为多个系列，每个系列的函数原型都放在一个特定的头文件中。

##### 文件包含

- 如果函数被定义为宏，那么可以通过`#include `指令包含定义宏函数的文件。通常，类似的宏都放在合适名称的头文件中。例如，许多系统（包括所有的`ANSI C`系统）都有`ctype.h`文件，该文件中包含了一些确定字符性质（如大写、数字等）的宏。

##### 库包含

- 在编译或链接程序的某些阶段，可能需要指定库选项。即使在自动检查标准库的系统中，也会有不常用的函数库。必须通过编译时选项显式指定这些库。注意，这个过程与包含头文件不同。头文件提供函数声明或原型，而库选项告诉系统到哪里查找函数代码。

### 十四、常见问题

#### 1、数组和指针

- 数组和指针可以互换。

  ```c
  char chs[10] = {'a', 'c', 'd', 'e', 'f'};
  printf("chs[1] = %c\n", 1[chs]);
  printf("chs[1] = %c\n", chs[1]);
  ```

- 对于`int a[10]`，对a的引用的类型是`int型的指针`，而`&a`是`10个int的数组的指针`，对于二维数组`int aa[5][6]`，对`aa`的引用是`6`个`int`的数组的指针，而`&aa`是`5个6个int的数组的数组的指针`

- 不能向一个接受指针的指针的函数传入二维数组。

#### 2、系统相关操作

- 捕获键盘中断：

  ```c
  extern void func(int);
  if (signal(SIGINT, SIG_IGN) != SIG_IGN) {
      signal(SIGINT, func);
  }
  ```

#### 3、杂项

- 判断机器的字节顺序的指针方法：

```c
int x = 1;
if (*(char *) &x == 1) {
    printf("little - endian\n");
} else {
    printf("big - endian\n");
}
```

- 使用联合判断

```c
union {
    int i;
    char c[sizeof(int)];
} x;
x.i = 1;
if (x.c[0] == 1) {
    printf("little - endian\n");
} else {
    printf("big - endian\n");
}
```

- 达夫设备（`Duff's Device`）

```c
#include <stdio.h>

void copyArrayDuff(int *to, const int *from, int count) {
    int n = (count + 7) / 8;  // 计算循环次数

    switch (count % 8) {
        case 0: do { *to++ = *from++;
            case 7:      *to++ = *from++;
            case 6:      *to++ = *from++;
            case 5:      *to++ = *from++;
            case 4:      *to++ = *from++;
            case 3:      *to++ = *from++;
            case 2:      *to++ = *from++;
            case 1:      *to++ = *from++;
        } while (--n > 0);
    }
}

int main() {
    int source[] = {1, 2, 3, 4, 5, 6, 7, 8};
    int destination[8];

    copyArrayDuff(destination, source, 8);

    // 打印目标数组以验证结果
    for (int i = 0; i < 8; i++) {
        printf("%d ", destination[i]);
    }

    return 0;
}
```



