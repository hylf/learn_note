### 一、简介和准备

#### 1、学习方向

- 区块链研发
- 服务器端，游戏软件工程师
- 分布式，云计算
- 官网：https://www.goland.org，https://tour.go-zh.org/
- 国内：http://docscn.studygolang.com/，https://studygolang.com/pkgdoc

#### 2、环境变量

+ GOROOT，SDK路径
+ GOPATH，工作目录
+ PATH，添加 `%GOROOT%\bin`
+ 查看是否安装成功：`go version`
+ 没有`godoc`命令时的手动安装步骤：

  + `go env -w GO111MODULE=on`
  + `go env -w GOPROXY=https://mirrors.aliyun.com/goproxy/,direct`

  - `go get golang.org/x/tools/cmd/godoc`（可能需要在`$GOPATH/src`下执行，并且可能需要预先在`src`下创建`golang.org/x`目录）
  - `go install golang.org/x/tools/cmd/godoc`，该命令可能需要在`$GOPATH/src`下执行
  - 将`$GOPATH/bin`下生成的`godoc.exe`放到`$GOROOT/bin`下
  - 执行命令`godoc -http=:xxx`后可以在`localhost:xxx`查看本地文档
  - `go env -w GO111MODULE=auto`

#### 3、基础认识

```go
package main
import "fmt"
func main(){
	fmt.Println("hello, world!")
}
```

+ GO文件的后缀是`.go`
+ **GO有25个关键字，36个预定标识符**
  + 特别说明一下，**预定标识符可以作为变量名**（或者叫标识符），但是强烈不推荐

+ 变量名、函数名、常量名尽量采用驼峰命名
+ **GO语言的语法非常严格，有利于代码风格的统一**
+ **如果变量名、函数名、常量名首字母大写，则可以被其它包访问，如果首字母小写，则只能在本包中使用**。可以简单理解为：首字母大写是`public`，首字母小写是`private`
+ **每个文件都必须归属于一个包**，例如`package main`
+ 保持`package`的名字和目录保持一致，尽量采用有意义的包名，简短，有意义，不要和标准库冲突
+ 通过`go build`可以编译并生成一个`.exe`文件
  + 使用`-o`参数可以指定生成的exe文件名。
+ `go run`命令可以直接运行程序，类似于执行脚本。
+ 严格区分大小写
+ 可以不用加分号
+ go语言定义的变量或者引用的包如果没有被使用则编译不通过
  + 可以在包前面加一个`_`，可以忽略包的检查
+ `gofmt -w xxx.go`可以格式化go文件

#### 4、Dos常用指令

+ `md xxx`，创建目录xxx
+ `rd xxx`，删除空目录xxx
  + `rd /q/s xxx`，删除目录，不询问
+ `dir`，类似于`ls`
+ `cd /d xxx`，切换盘符
+ `cd xxx`，切换目录
  + `cd \`，回到根目录
+ `echo xxx > file`，将文本追加到file
+ `copy src dst`，复制
+ `move src dst`，移动
+ `del xxx`，删除文件，可以使用正则匹配
+ `cls`，清屏
+ `exit`，退出dos

### 二、GO语言基础

#### 1、变量

##### ①使用方式

+ 指定变量类型，声明后如果不赋值会使用默认值

+ 根据值自动判定类型（类型推导）

+ 省略`var`，`:=`左侧的变量不应该是已经声明过的，否则编译不通过

+ 变量名可以是中文，但是不推荐使用

  ```go
  package main
  
  import "fmt"
  
  func main() {
  	var i int = 10
  	fmt.Println("i=", i)
  	var j = 10.11
  	fmt.Println("j=", j)
  	name := "tom"
  	fmt.Println("name=", name)
  }
  ```

+ 多变量声明

  ```go
  package main
  
  import "fmt"
  
  // 定义全局变量，也可以使用普通的定义方式：var n1 = 100或 n1 := 100
  var (
  	n1 = 100
  	n2 = 200
  )
  
  func main() {
  	// 同时声明多个变量
  	var n01, n02, n03 int
  	fmt.Printf("n01=%d, n02=%d, n03=%d\n", n01, n02, n03)
  	// 变量类型不同
  	var n11, n12, n13 = 100, "tom", 88.1
  	fmt.Printf("n11=%d, n12=%s, n13=%f\n", n11, n12, n13)
  	// 使用类型推导
  	n21, n22, n23 := true, "张三", 12.123
  	fmt.Printf("n21=%t, n22=%s, n23=%f\n", n21, n22, n23)
  	fmt.Printf("n1=%v, n2=%#v\n", n1, n2)
  }
  /*
  通用：
  %v	值的默认格式表示
  %+v	类似%v，但输出结构体时会添加字段名
  %#v	值的Go语法表示
  %T	值的类型的Go语法表示
  %%	百分号
  
  布尔值：
  %t	单词true或false
  
  整数：
  %b	表示为二进制
  %c	该值对应的unicode码值
  %d	表示为十进制
  %o	表示为八进制
  %q	该值对应的单引号括起来的go语法字符字面值，必要时会采用安全的转义表示
  %x	表示为十六进制，使用a-f
  %X	表示为十六进制，使用A-F
  %U	表示为Unicode格式：U+1234，等价于"U+%04X"
  
  浮点数与复数的两个组分：
  %b	无小数部分、二进制指数的科学计数法，如-123456p-78；参见strconv.FormatFloat
  %e	科学计数法，如-1234.456e+78
  %E	科学计数法，如-1234.456E+78
  %f	有小数部分但无指数部分，如123.456
  %F	等价于%f
  %g	根据实际情况采用%e或%f格式（以获得更简洁、准确的输出）
  %G	根据实际情况采用%E或%F格式（以获得更简洁、准确的输出）
  
  字符串和[]byte：
  %s	直接输出字符串或者[]byte
  %q	该值对应的双引号括起来的go语法字符串字面值，必要时会采用安全的转义表示
  %x	每个字节用两字符十六进制数表示（使用a-f）
  %X	每个字节用两字符十六进制数表示（使用A-F）    
  
  指针：
  %p	表示为十六进制，并加上前导的0x    
  */
  ```

##### ②使用注意事项

+ 不能改变一个已经定义的变量的数据类型
+ 变量在同一个作用域内不能重名

#### 2、数据类型

##### ①基本数据类型

+ 数值型

  + 整数型`int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64, byte`
  + 浮点类型`float32, float64`

+ 字符型，没有专门的字符型，使用`byte`来保存单个字母（不能存汉字）

+ 布尔型`bool`

+ 字符串`string`，在GO中是基本类型

  | 类型     | 有无符号 | 占用空间                                 | 范围                                                         |
  | -------- | -------- | ---------------------------------------- | ------------------------------------------------------------ |
  | `int8`   | 有       | 1字节                                    | -128~127                                                     |
  | `int16`  | 有       | 2字节                                    | -2<sup>15</sup>~2<sup>15</sup>-1                             |
  | `int32`  | 有       | 4字节                                    | -2<sup>31</sup>~2<sup>31</sup>-1                             |
  | `int64`  | 有       | 8字节                                    | -2<sup>63</sup>~2<sup>63</sup>-1                             |
  | `uint8`  | 无       | 1字节                                    | 0~255                                                        |
  | `uint16` | 无       | 2字节                                    | 0~2<sup>16</sup>-1                                           |
  | `uint32` | 无       | 4字节                                    | 0~2<sup>32</sup>-1                                           |
  | `uint64` | 无       | 8字节                                    | 0~2<sup>64</sup>-1                                           |
  | `int`    | 有       | 32位系统占用4字节<br />64位系统占用8字节 | -2<sup>31</sup>~2<sup>31</sup>-1<br />-2<sup>63</sup>~2<sup>63</sup>-1 |
  | `uint`   | 无       | 32位系统占用4字节<br />64位系统占用8字节 | 0~2<sup>32</sup>-1<br />0~2<sup>64</sup>-1                   |
  | `rune`   | 有       | 与`int32`一样                            | -2<sup>31</sup>~2<sup>31</sup>-1                             |
  | `byte`   | 无       | 与`uint8`等价                            | 0~255                                                        |

- 当要存储字符时选用`byte`
- `rune`表示一个`Unicode`码

##### ②派生/复杂数据类型

+ 指针`Pointer`
+ 数组
+ 结构体`struct`
+ 管道`Channel`
+ 函数
+ 切片`slice`
+ 接口`interface`
+ `map`

##### ③变量的使用细节

+ 不指定类型时，赋值整数的变量默认类型是`int`，小数是`float64`

  ```go
  package main
  
  import (
  	"fmt"
  	"unsafe"
  )
  
  // 变量的使用细节
  func main() {
  	n1 := 100
  	fmt.Printf("赋值整数时默认变量类型：%T\n", n1)
  	// 获取每个变量占用的字节大小
  	fmt.Printf("占用字节数：%d\n", unsafe.Sizeof(n1))
  	n2 := 10.1
  	fmt.Printf("赋值小数时默认变量类型：%T\n", n2)
  }
  ```

+ 在保证程序正确的前提下尽量使用占用内存小的类型

+ 浮点数=符号位+指数位+尾数位，浮点数都是有符号的

+ 尾数部分可能丢失，造成精度损失

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // float32和float64的精度有区别
  func main() {
  	var num1_f32, num2_f32 float32 = 0.00001, 0.00009
  	fmt.Println(num1_f32 + num2_f32)
  	var num1_f64, num2_f64 float64 = 0.00001, 0.00009
  	fmt.Println(num1_f64 + num2_f64)
  	num1 := 5.12e2
  	fmt.Printf("num1 = %f\n", num1)
  	num2 := 5.12e-2
  	fmt.Printf("num2 = %f\n", num2)
  	num3 := 5.12E2
  	fmt.Printf("num3 = %f\n", num3)
  }
  // 0.000100000005
  // 0.0001
  ```

+ GO的浮点类型默认声明是`float64`类型

+ `.123`等价于`0.123`

+ 浮点类型支持科学计数法形式

+ 通常情况下尽量使用`float64`

+ GO中没有专门的字符类型，如果要存储单个字符，一般使用`byte`类型

+ GO的字符串是由字节`byte`组成的

+ 当直接输出`byte`类型的值变量时，默认会输出对应字符的`ascii`码值

+ 字符类型可以进行运算，相当于一个整数

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // 字符类型
  func main() {
  	var c1 byte = 'a'
  	var c2 byte = '0'
  	fmt.Println("c1=", c1)
  	fmt.Println("c2=", c2)
  	fmt.Printf("c1=%c, c2=%c\n", c1, c2)
  	var c3 int16 = '武'
  	fmt.Printf("c3=%c, c3对应码值=%d\n", c3, c3)
  	fmt.Printf("%c%c\n", 20013, 22269)
  	var n1 = 3 + 'a'
  	fmt.Printf("n1=%d\n", n1)
  }
  /*
  c1= 97
  c2= 48
  c1=a, c2=0
  c3=武, c3对应码值=27494
  中国
  n1=100
  */
  ```

+ GO使用`utf-8`编码，英文字母1个字节，汉字3个字节

+ 在字符串中，双引号会识别转义字符，反引号会将字符串以原生形式输出，包括换行符和特殊字符。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // 字符串类型
  func main() {
  	// 使用双引号
  	str1 := "hello\tworld"
  	fmt.Println("str1=", str1)
  	// 使用反引号
  	str2 := `hello\tworld`
  	fmt.Println("str2=", str2)
  	// 使用转义字符
  	str3 := "hello\\tworld"
  	fmt.Println("str3=", str3)
  	// 字符串拼接（超长字符串）+需要保留在上一行
  	str := "11111111111111111111111" +
  		"22222222222222222222"
  	fmt.Println("str=", str)
  }
  /*
  str1= hello     world
  str2= hello\tworld
  str3= hello\tworld
  str= 1111111111111111111111122222222222222222222 
  */
  ```

##### ④基本数据类型的转换

+ GO中基本数据类型必须**显示转换**，不能自动转换

+ 基本语法：表达式`T(v)`，将值`v`转换为类型`T`

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // 基本数据类型的转换，%v表示按值的默认格式表示
  func main() {
  	var i int32 = 100
  	// 希望转换为float32
  	var n1 float32 = float32(i)
  	// 转换为int8
  	var n2 int8 = int8(i)
  	// 转换为int64
  	var n3 int64 = int64(i)
  	fmt.Printf("i = %v, n1 = %v, n2 = %v, n3 = %v\n", i, n1, n2, n3)
  	var num int16 = 258
  	// 转换为int8，会出现溢出处理，(258-1)%255=2
  	var tmp int8 = int8(num)
  	fmt.Printf("tmp = %v\n", tmp)
  }
  /*
  i = 100, n1 = 100, n2 = 100, n3 = 100
  tmp = 2
  */
  ```

+ 基本类型转`string`类型

  ```go
  package main
  
  import (
  	"fmt"
  	"strconv"
  )
  
  // 基本数据类型转string
  func main() {
  	var str string
  	var num1 int32 = 99
  	var num2 float64 = 23.456
  	var b bool = true
  	var myChar byte = 'h'
  	fmt.Println("方式一: fmt.Sprintf()")
  	str = fmt.Sprintf("%d", num1) // 此处%d可改为%v, 下面同理
  	// %q, 该值对应的单引号括起来的go语法字符字面值，必要时会采用安全的转义表示
  	fmt.Printf("str type is %T, str = %q\n", str, str)
  	str = fmt.Sprintf("%f", num2)
  	fmt.Printf("str type is %T, str = %q\n", str, str)
  	str = fmt.Sprintf("%t", b)
  	fmt.Printf("str type is %T, str = %q\n", str, str)
  	str = fmt.Sprintf("%c", myChar)
  	fmt.Printf("str type is %T, str = %q\n", str, str)
  	fmt.Println("方式二: strconv")
  	str = strconv.FormatInt(int64(num1), 10) // 第一个参数必须是int64类型，第二个参数表示进制
  	fmt.Printf("str type is %T, str = %q\n", str, str)
  	str = strconv.FormatFloat(num2, 'f', 10, 64)
  	fmt.Printf("str type is %T, str = %q\n", str, str)
  	str = strconv.FormatBool(b)
  	fmt.Printf("str type is %T, str = %q\n", str, str)
  	str = strconv.Itoa(int(num1))
  	fmt.Printf("str type is %T, str = %q\n", str, str)
  }
  /*
  方式一: fmt.Sprintf()
  str type is string, str = "99"
  str type is string, str = "23.456000"
  str type is string, str = "true"
  str type is string, str = "h"
  方式二: strconv
  str type is string, str = "99"
  str type is string, str = "23.4560000000"
  str type is string, str = "true"
  str type is string, str = "99"
  */
  ```

+ `string`类型转基本数据类型

  ```go
  package main
  
  import (
  	"fmt"
  	"strconv"
  )
  
  // string转基本数据类型
  func main() {
  	// 字符串转bool
  	var str1 string = "true"
  	var b bool
  	b, _ = strconv.ParseBool(str1)
  	fmt.Printf("b type is %T, b = %v\n", b, b)
  	// 字符串转int
  	var str2 string = "123456"
  	var num int64
  	num, _ = strconv.ParseInt(str2, 10, 32)
  	fmt.Printf("num type is %T, num = %v\n", num, num)
  	// 字符串转浮点型
  	var str3 string = "123456.123456"
  	var f1 float64
  	f1, _ = strconv.ParseFloat(str3, 64)
  	fmt.Printf("f1 type is %T, f1 = %v\n", f1, f1)
  
  	// 转换失败的情况，其它类似
  	var str4 string = "hello"
  	var n1 int64
  	var err error
  	n1, err = strconv.ParseInt(str4, 10, 64)
  	fmt.Printf("n1 = %v, err = %v\n", n1, err)
  }
  /*
  b type is bool, b = true
  num type is int64, num = 123456
  f1 type is float64, f1 = 123456.123456
  n1 = 0, err = strconv.ParseInt: parsing "hello": invalid syntax
  */
  ```

+ 将`string`类型转换为基本类型时，如果转换失败，被转换的变量会被赋默认值

##### ⑤指针

+ 基本数据类型，变量存的就是值，也叫值类型

+ 获取变量的地址，用`&`

+ 指针类型，指针变量存的是一个地址，这个地址指向的空间存的才是值

+ 获取指针类型所指向的值，使用`*`

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // 指针
  func main() {
  	// 基本数据类型
  	var i int32 = 10
  	// 取出i的地址，取地址用&，类似C
  	fmt.Printf("i的地址: %v\n", &i)
  	// *int是一个指向int的指针
  	var ptr *int32 = &i
  	fmt.Printf("ptr = %v\n", ptr)
  	// ptr的值也有个地址
  	fmt.Printf("ptr的地址: %v\n", &ptr)
  	// 获取指针所指向地址的值，用*
  	fmt.Printf("ptr指向的值: %v\n", *ptr)
  }
  /*
  i的地址: 0xc00001c098
  ptr = 0xc00001c098
  ptr的地址: 0xc00000a030
  ptr指向的值: 10
  */
  ```

+ 通过指针修改变量的值

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // 通过指针修改变量的值
  func main() {
  	var num int32 = 10
  	fmt.Printf("改前num的值: %v\n", num)
  	// 定义一个指针变量
  	var ptr *int32 = &num
  	*ptr = 20
  	// 修改后的值
  	fmt.Printf("改后num的值: %v\n", num)
  }
  /*
  改前num的值: 10
  改后num的值: 20
  */
  ```

+ 值类型都有对应的指针类型，值类型包括整数型、浮点型、`bool`型、`string`、数组和结构体`struct`

+ GO语言的指针移动和C不一样：

  ```go
  package main
  
  import (
  	"fmt"
  	"unsafe"
  )
  
  func main() {
  	var a [5]int
  	alloc(a)
  }
  
  func alloc(a [5]int) {
  	for i := 0; i < len(a); i++ {
  		a[i] = i*i + 1
  	}
  	var ptr *int = &a[0]
  	for i := 0; i < len(a); i++ {
  		fmt.Printf("i=%d, *p=%d;\n", i, *ptr)
  		p := uintptr(unsafe.Pointer(ptr))
  		p = p + unsafe.Sizeof(int(0)) // 指针移动
  		ptr = (*int)(unsafe.Pointer(p))
  	}
  }
  /*
  i=0, *p=1;
  i=1, *p=2;
  i=2, *p=5;
  i=3, *p=10;
  i=4, *p=17;
  */
  ```

  - 利用Go语言的指针访问动态数组不能直接像C语言一样利用++的形式进行数组访问，而需要加上实际的元素大小进行移动。

##### ⑥常见的值类型和引用类型

- 值类型：整数型、浮点型、`bool`型、`string`、数组和结构体`struct`。
  - 变量直接存储值，内存中通常在栈中分配

- 引用类型：指针、`slice`切片、`map`、管道`channel`、`interface`等
  - 变量存储的是一个地址，这个地址对应的空间才真正存储数据（值），内存中通常在堆上分配，当没有任何变量引用这个地址时，该地址对应的数据空间就成为一个垃圾，由GC回收

#### 3、运算符

##### ①基本认识

+ 算术运算符和C语言相同

+ 取模的本质：`a % b = a - a / b * b`

+ 自增和自减只能独立使用，不能使用例如这样的用法：`a = i++`，可以写成`i++; a = i;`**只有`i++`和`i--`，没有`++i`和`–-i`**

+ 对于`&&`，只要遇到`false`的情况，该条件后面的情况就不会判断了，也就是对应的代码不会被执行

+ 对于`||`，只要遇到`true`的情况，该条件后面的情况就不会判断了，也就是对应的代码不会被执行

+ `%b`，二进制输出；`%o`，八进制输出，`%d`，十进制输出，`%x`，小写十六进制输出，`%X`，大写十六进制输出

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // 各种进制的表示
  func main() {
  	var num_2 int16 = 0b11 // 二进制的表示, 0-1
  	var num_8 int16 = 0o11 // 八进制的表示, 0-7
  	var num_10 int16 = 11 // 十进制的表示, 0-9
  	var num_16 int16 = 0x11 // 十六进制的表示, 0-f或0-F
  	// 默认按照10进制输出
  	fmt.Printf("num_2 = %v, num_8 = %v, num_10 = %v, num_16 = %v\n", num_2, num_8, num_10, num_16)
  	num_10 = 59
  	// 将num_10按照不同进制输出
  	fmt.Printf("二进制：%b, 八进制: %o, 十进制: %d, 十六进制: %x或%X\n", num_10, num_10, num_10, num_10, num_10)
  }
  /*
  num_2 = 3, num_8 = 9, num_10 = 11, num_16 = 17
  二进制：111011, 八进制: 73, 十进制: 59, 十六进制: 3b或3B
  */
  ```

##### ②细节

+ GO有一种特殊的用法来交换两个变量的值：`a, b = b, a`
+ 注意`^`运算符。
  + 作为二元运算符表示异或。规则：`1^1=0; 0^0=0; 1^0=1; 0^1=1`
  + 作为一元运算符表示按位取反，包括符号位。规则：`^1=0; ^0=1`，一个有符号位的`^`操作为这个数+1的相反数。


- **正数的原码、反码和补码相同**

- **负数的反码等于符号位不变，其他位置取反**

- **负数的补码等于反码加一**

- **计算机的运算都是采用补码进行的**

- **`>>`，右移，低位溢出，符号位不变，并用符号位补溢出的高位**

- **`>>`，左移，符号位不变，低位补0**

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // 左移和右移的计算
  func main() {
  	// 重要: 计算机内所有的运算都是在补码基础上进行
  	// 正数的原码、反码和补码都相同
  	// 负数的反码等于符号位不变, 其他位置取反, 补码等于反码加一
  	// ^是异或运算符
  	// -2的原码: 1000 0010, 反码: 1111 1101, 补码: 1111 1110
  	// 									 2的补码: 0000 0010
  	//								      -2^2 = 1111 1100
  	// 注意1111 1100是结果的补码, 需要转换为原码
  	// 1111 1100的反码 = 补码-1 = 1111 1011, 原码 = 1000 0100
  	fmt.Printf("-2^2 = %b\n", -2^2)
  	// ^作为一元运算符表示按位取反
  	// -10的原码1000 1010, 反码1111 0101, 补码1111 0110
  	// 								  ^-10 = 0000 1001
  	fmt.Printf("^-10 = %b\n", ^-10)
  	// >>，右移运算符, 低位溢出符号位不变，并用符号位补溢出的高位
  	// -3原码1000 0011, 反码1111 1100, 补码1111 1101,
  	// 移位实际上是在补码的基础上进行(计算机内所有的运算都是在补码基础上进行)
  	// 补码右移一位: 1111 1110, 这是结果的补码
  	// 1111 1110的反码 = 1111 1101, 原码 = 1000 0010
  	fmt.Printf("-3>>1 = %b\n", -3>>1)
  	// <<, 左移运算符, 符号位不变, 低位补0
  	// 2原码0000 0010, 补码0000 0010
  	// 补码左移两位0000 1000
  	fmt.Printf("2<<2 = %b\n", 2<<2)
  }
  /*
  -2^2 = -100
  ^-10 = 1001
  -3>>1 = -10
  2<<2 = 1000
  */
  ```

#### 4、获取用户输入

```go
package main

import (
	"fmt"
)

// 获取键盘输入
func main() {
	// 方式1，使用fmt.Println
	var name string
	var age int8
	var sal float64
	var isPass bool = false
	// fmt.Printf("请输入姓名：")
	// fmt.Scanln(&name)

	// fmt.Printf("请输入年龄：")
	// fmt.Scanln(&age)

	// fmt.Printf("请输入分数：")
	// fmt.Scanln(&sal)

	// if sal >= 60{
	// 	isPass = true
	// }

	// fmt.Printf("学生%v, 年龄%v, 分数%v, 是否通过考试: %v\n",name, age, sal, isPass)

	// 方式2，使用fmt.Scanf(), 可以按指定的格式输出
	fmt.Printf("请分别输入姓名、年龄和分数, 并使用空格分开:")
	fmt.Scanf("%s%d%f", &name, &age, &sal)
	if sal >= 60 {
		isPass = true
	}
	fmt.Printf("学生%v, 年龄%v, 分数%v, 是否通过考试: %v\n", name, age, sal, isPass)

}
/*
请分别输入姓名、年龄和分数, 并使用空格分开:
张三 18 59.5
学生张三, 年龄18, 分数59.5, 是否通过考试: false
*/
```

#### 5、基本流程细节

+ `if`语句中可以直接定义一个变量

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // 流程控制
  func main() {
  	if age := 20; age >= 18 {
  		fmt.Println("已成年")
  	} else {
  		fmt.Println("未成年")
  	}
  }
  // 已成年
  ```

  - `else`必须写在11行的`}`后面，不能写在12行

+ **`switch`语句每个匹配项后面不需要加`break`**，默认加了`break`，加上`fallthrough`来实现穿透

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // switch语句
  func main() {
  	// switch穿透, fallthrough
  	switch num := 10; {
  	case num == 10:
  		fmt.Println("OK1")
  		fallthrough // 默认只穿透一层
  	case num == 20:
  		fmt.Println("Ok2")
  	case num == 30:
  		fmt.Println("OK3")
  		fallthrough // 这里会穿透到default
  	default:
  		fmt.Println("没有匹配到")
  	}
  
  }
  
  ```

+ `switch`后面也可以不写表达式，和`if`类似，在`switch`后面也可以直接定义变量

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // switch语句
  func main() {
  	var score uint8
  	fmt.Printf("请输入成绩: ")
  	fmt.Scanln(&score)
  	switch {
  	case score > 90:
  		fmt.Println("A")
  	case score >= 70:
  		fmt.Println("B")
  	case score >= 60:
  		fmt.Println("C")
  	default:
  		fmt.Println("不及格")
  	}
  }
  ```

+ `case`后面可以有多个表达式，用逗号分隔

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // switch语句
  func main() {
  	var weekStr string
  	var weekDay string
  	fmt.Printf("请输入星期: ")
  	fmt.Scanln(&weekStr)
  	switch weekStr {
  	case "1", "一":
  		weekDay = "星期一"
  	case "2", "二":
  		weekDay = "星期二"
  	case "3", "三":
  		weekDay = "星期三"
  	case "4", "四":
  		weekDay = "星期四"
  	case "5", "五":
  		weekDay = "星期五"
  	case "6", "六":
  		weekDay = "星期六"
  	case "7", "七":
  		weekDay = "星期日"
  	default:
  		weekDay = "输入错误!"
  	}
  	fmt.Println(weekDay)
  }
  
  ```

+ `case`后面的表达式还可以是数据类型，`nil`，`int`，函数类型等

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // 乘法口诀表
  func main() {
  	// 正梯形
  	fmt.Println("正梯形")
  	for i := 1; i <= 9; i++ {
  		for j := 1; j <= i; j++ {
  			fmt.Printf("%d * %d = %d\t", i, j, i*j)
  		}
  		fmt.Println()
  	}
  	// 反梯形
  	fmt.Println("反梯形")
  	for i := 9; i >= 1; i-- {
  		for j := 1; j <= i; j++ {
  			fmt.Printf("%d * %d = %d\t", i, j, i*j)
  		}
  		fmt.Println()
  	}
  	// 矩形
  	fmt.Println("矩形")
  	for i := 1; i <= 9; i++ {
  		for j := 1; j <= 9; j++ {
  			fmt.Printf("%d * %d = %d\t", i, j, i*j)
  		}
  		fmt.Println()
  	}
  }
  ```

+ 另外一种方式

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // for-range遍历
  func main() {
  
  	// 只有英文的情况
  	str1 := "hello world"
  	for i := 0; i < len(str1); i++ {
  		fmt.Printf("%c", str1[i])
  	}
  	fmt.Println()
  	for index, val := range str1 {
  		fmt.Printf("index = %d, val = %c\n", index, val)
  	}
  	
  	// 有中文的情况
  	str2 := "hello world!武汉"
  	// 这种方式是按照每个字节遍历, 对中文要特殊处理
  	var tmp = []rune(str2)
  	for i := 0; i < len(tmp); i++ {
  		fmt.Printf("%c", tmp[i])
  	}
  	// 这种方式不用特殊处理
  	fmt.Println()
  	for index, val := range str2 {
  		fmt.Printf("index = %d, val = %c\n", index, val)
  	}
  }
  /*
  hello world        
  index = 0, val = h 
  index = 1, val = e 
  index = 2, val = l 
  index = 3, val = l 
  index = 4, val = o 
  index = 5, val =   
  index = 6, val = w
  index = 7, val = o
  index = 8, val = r
  index = 9, val = l
  index = 10, val = d
  hello world!武汉
  index = 0, val = h
  index = 1, val = e
  index = 2, val = l
  index = 3, val = l
  index = 4, val = o
  index = 5, val =
  index = 6, val = w
  index = 7, val = o
  index = 8, val = r
  index = 9, val = l
  index = 10, val = d
  index = 11, val = !
  index = 12, val = 武
  index = 15, val = 汉
  */
  ```

+ **GO语言没有`while`和`do...while`的语法**，用`for`完全可以实现。

+ `break`语句基本使用

  ```go
  package main
  
  import (
  	"fmt"
  	"math/rand"
  	"time"
  )
  
  /*
  break语句
  取随机数
  */
  func main() {
  	// 生成随机数种子
  	// time.Now().Unix()会返回从1970年1月1日到现在的秒数
  	rand.Seed(time.Now().Unix())
  	count := 0
  	for {
  		// 生成[0, n)的随机数
  		num := rand.Intn(101)
  		fmt.Printf("num = %v\n", num)
  		if num == 100 {
  			fmt.Printf("count = %v\n", count)
  			break
  		}
  		count++
  	}
  
  }
  ```

+ 和其它语言相比，**GO语言`break`的特点：可以通过标签指明要终止的是哪一层语句块**

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  /*
  break语句
  指定标签的方式
  */
  func main() {
  lable2:
  	for i := 0; i < 4; i++ {
  		// 设置一个标签, 名称随意
  	lable1:
  		for j := 0; j < 10; j++ {
  			if i == 2 {
  				fmt.Println("j=", j)
  				break lable2
  			}
  			if j == 4 {
  				fmt.Println("j=", j)
  				break lable1
  			}
  		}
  	}
  
  }
  /*
  j= 4
  j= 4
  j= 0
  */
  ```

  - `break`默认会跳出最近的`for`循环
  - `break`后面可以指定标签，跳出标签对应的`for`循环

+ `continue`和`break`类似，也可以通过标签指明要跳过的位置

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  /*
  continue语句
  指定标签的方式
  */
  func main() {
  	// 设置lable
  label1:
  	for i := 0; i <= 9; i++ {
  		for j := 9; j >= 0; j-- {
  			if i * j % 2 == 0 {
  				continue label1
  			} else {
  				fmt.Printf("i * j = %v * %v = %v\n", i, j, i*j)
  			}
  		}
  	}
  }
  /*
  i * j = 1 * 9 = 9
  i * j = 3 * 9 = 27
  i * j = 5 * 9 = 45
  i * j = 7 * 9 = 63
  i * j = 9 * 9 = 81
  */
  ```

+ GO语言支持`goto`语句，基本语法：

  ```go
  goto label
  // TODO
  label: statement
  ```

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  /*
  goto语句
  */
  func main() {
  	var n = 1
  	fmt.Println("OK1")
  	if n == 1 {
  		goto lablel1
  	}
  	fmt.Println("OK2")
  	fmt.Println("OK3")
  	fmt.Println("OK4")
  	fmt.Println("OK5")
  	fmt.Println("OK6")
  lablel1:
  	fmt.Println("OK7")
  }
  /*
  OK1
  OK7
  */
  ```

#### 6、函数

##### ①GO语言没有重载的概念。

```go
func 函数名 (形参列表) (返回值列表){
	// TODO
}
```

```go
package main

import (
	"fmt"
)
func main() {
	fmt.Println(CalcSubAndSum(20, 10))
	fmt.Println(CalcDiv(11, 5))
}
//有多个返回值时必须加括号
func CalcSubAndSum(num1 int32, num2 int32) (int32, int32) {
	return num1 - num2, num1 + num2
}
// 只有一个返回值时可以省略括号
func CalcDiv(num1 float64, num2 float64) float64 {
	return num1 * 1.0 / num2
}
```

##### ②基本数据类型和**数组**都是值传递。可以通过指针的方式来修改值。

##### ③函数也是一种数据类型

```go
package main

import "fmt"

// 在GO中, 函数也是一种数据类型
// 可以赋值给一个变量, 则该变量就是一个函数类型的变量
func main() {
	var a func(int32, int32) int32 = GetSum
	fmt.Printf("a的类型: %T, GetSum的类型是: %T\n", a, GetSum)
	res := a(10, 40) //等价于 res := GetSum(10, 40)
	fmt.Printf("res = %v\n", res)

	res2 := MyFun(GetSum, 50, 60)
	fmt.Printf("res2 = %v\n", res2)
}

func GetSum(n1 int32, n2 int32) int32 {
	return n1 + n2
}

// 函数可以作为形参
func MyFun(funvar func(int32, int32) int32, num1 int32, num2 int32) int32 {
	return funvar(num1, num2)
}
/*
a的类型: func(int32, int32) int32, GetSum的类型是: func(int32, int32) int32
res = 50
res2 = 110
*/
```

##### ④可以使用`type`来给类型定义别名

```go
package main

import "fmt"

// 可以简化函数类型的定义
type MyFunc func(int32, int32) int32

func main() {
	// 给int32取别名INT
	type INT int32
	var num1 INT = 10
	var num2 int = int(num1)
	fmt.Printf("num1的类型是: %T, num2的类型是: %T\n", num1, num2)
	fmt.Printf("num1 + num2 = %v\n", num1+INT(num2))

	res := test(GetSum, 1, 2)
	fmt.Printf("res = %v\n", res)

}

func test(funvar MyFunc, num1 int32, num2 int32) int32 {
	return funvar(num1, num2)
}

func GetSum(n1 int32, n2 int32) int32 {
	return n1 + n2
}
/*
num1的类型是: main.INT, num2的类型是: int
num1 + num2 = 20
res = 3
*/
```

##### ⑤支持对返回值的命名

```go
package main

import "fmt"

func main() {
	num1, num2 := GetSumAndSub(10, 5)
	fmt.Printf("num1 = %v, num2 = %v\n", num1, num2)
}

func GetSumAndSub(n1 int32, n2 int32) (sub int32, sum int32) {
	sub = n1 - n2
	sum = n1 + n2
	return sub, sum
}
// num1 = 5, num2 = 15
```

##### ⑥可以使用`_`标识符，忽略返回值。

##### ⑦支持可变参数

```go
// 支持0到多个参数
func sum(args... int32) int{}
```

- `args`是`slice`切片，通过`args[index]`可以访问各个值

```go
package main

import "fmt"

func main() {
	fmt.Printf("sum = %v\n", sum(1, 2, 3, 4, 5))
}

func sum(args ...int32) int32 {
	var sum int32 = 0
	for i := 0; i < len(args); i++ {
		sum += args[i]
	}
	// for-range的写法, index不使用时使用_忽略
	// for _, e := range args {
	// 	sum += e
	// }
	return sum
}
```

##### ⑧`init`函数

```go
package main

import "fmt"

var age = getAge()

func getAge() int{
	fmt.Println("getAge...")
	return 90
}

// init函数, 通常可以在init函数中完成初始化的工作
func init() {
	fmt.Println("init...")
}

func main() {
	// init函数执行完后才会执行main函数
	fmt.Println("main...", age)
}
```

- 执行顺序：全局变量初始化，`init`函数，`main`函数。
- 在使用`import`引入其它包时就会执行全局变量初始化和`init`函数的操作。

##### ⑨函数参数的传递方式

- 值类型：基本数据类型、`int`系列、`float`系列、`bool`、`string`、数组和结构体`struct`。值类型默认是值传递。
- 引用类型：指针、`slice`切片、`map`、管道`chan`、`interface`等。引用类型默认是传地址。地址类型的大小是`8bit`
- 一般类说，**传入地址的效率比较高**。

#### 7、包

+ 每个GO文件都要属于一个包；

  

+ 小写开头的变量和方法只能在本包使用，大写开头的变量和方法可以被其它包使用

  ```go
  // package_test/utils目录下的utils.go
  package utils
  
  var (
  	Name1 = "张三"
  	name2 = "李四"
  )
  
  func calcSub(num1 int32, num2 int32) int32 {
  	return num1 - num2
  }
  
  func CalcSum(num1 int32, num2 int32) int32 {
  	return num1 + num2
  }
  ```

  ```go
  // package_test/utils目录下的utils2.go
  package utils
  
  import "fmt"
  
  // 小写的函数可以被本包使用
  func CalcSub(num1 int32, num2 int32) int32 {
  	return calcSub(num1, num2)
  }
  
  // 小写的变量可以被本包使用
  func PrintStr() {
  	fmt.Println(name2)
  }
  
  ```

  ```go
  // package_test/main目录下的main.go
  package main
  
  import (
  	"fmt"
  	"go_code/package_test/utils"
  )
  
  func main() {
  	// 大写的变量可以被其它包使用
  	fmt.Println(utils.Name1)
  	// 小写的变量获取不到
  	// fmt.Println(utils.str2)
  	fmt.Println(utils.CalcSum(1, 2))
  	// 小写的函数也不可以调用
  	// fmt.Println(utils.calcSub(1, 2))
  	fmt.Println(utils.CalcSub(1, 2))
  	utils.PrintStr()
  }
  ```

+ 包名可以任意取，但是一般建议和目录名相同

+ 可以给引入的包取别名，例如：

  ```go
  import ft "fmt"
  ```

+ 这样在后面就要使用别名来调用，例如：

  ```go
  ft.Println("Hello")
  ```

+ 显然别名也不能重复

+ 编译时需要编译`main`包所在的目录，编译路径从`$GOPATH`的`src`后一层开始

+ 可以指定编译后的可执行文件名和目录，例如

  ```
  go build -o 生成的位置/文件名.exe main包所在路径 
  ```


#### 8、函数中的`defer`

- 为了在函数执行完毕后，及时的释放资源，GO提供了`defer`（延时机制）。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // defer机制
  func sum(n1 int, n2 int) int {
  	// 执行到defer时, 系统会将defer后面的语句压入到独立的栈中(defer栈)
  	// 暂时不执行
  	// 当函数执行完毕后, 再从defer栈中按照先入后出的方式出栈
  	defer fmt.Println("ok1 n1 =", n1)
  	defer fmt.Println("ok2 n2 =", n2)
  	n1++
  	n2++
  	res := n1 + n2
  	fmt.Println("ok3 res =", res)
  	return res
  }
  
  // 闭包
  func main() {
  	res := sum(10, 20)
  	fmt.Println("res =", res)
  }
  /*
  ok3 res = 32
  ok2 n2 = 20
  ok1 n1 = 10
  res = 32
  */
  ```

- 当`defer`入栈时，会把相应的值也保存下来。

#### 9、常量

- 常量只能修饰`bool`，数值类型和`string`类型。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  const (
  	a = iota
  	b
  	c, d = iota, iota
  	e    = iota
  )
  
  func main() {
  	fmt.Println(a, b, c, d, e)
  }
  /*
  0 1 2 2 3
  */
  ```

### 三、常用的基本功能

#### 1、字符串

##### ①`len()`

- 返回字符串占用的字节大小，这是一个内建函数，返回`V`的大小（长度），取决于具体的类型，比如数组等。

- 注意：字母和数字占一个字节，汉字占用三个字节。

- 遍历带有汉字的字符串时要转成`[]rune(str)`，或者使用`for-range`的方式。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  func main() {
  	str := []rune("武汉")
  
  	for i := 0; i < len(str); i++ {
  		fmt.Printf("%c\t", str[i])
  	}
  }
  ```

##### ②`strconv.Atoi(str)`

- 将字符串转成整数，返回`(int, err)`

##### ③`strconv.Itoa()`

- 将整数转成字符串，一定会成功

##### ④字符串转`[]byte`

- `var bytes = []bytes("hello")`

##### ⑤`strings.Contains(str1, str2)`

- 在`str1`中查找子串`str2`，返回`bool`

##### ⑥`strings.Count(str1, str2)`

- 统计字符串`str1`中有多少个`str2`

##### ⑦`strings.EqualFold(str1, str2)`

- 不区分大小写的字符串比较，正常的比较使用`==`，区分大小写。

##### ⑧`strings.Index(str1, str2)`

- 返回`str2`在`str1`中第一次出现的位置，没找到返回`-1`。
- 类似的还有`strings.LastIndex(str1, str2)`，返回最后出现的位置。

##### ⑨`strings.Replace(str, str1, str2, num)`

- 将`str`中的`str1`替换成`str2`，`num`可以指定替换几个，`num`小于0表示全部替换，返回替换后的字符串。`str`本身不会被改变。

##### ⑩`strings.Split(str, str1)`

- 使用`str1`对`str`进行分割，返回字符串数组。

##### ⑪`strings.ToLower(str)`

- 将`str`转换成小写
- 类似的`strings.ToUpper(str)`转大写

##### ⑫`strings.TrimSpace(str)`

- 去掉字符串两边的空格
- 类似的还有`strings.Trim(str, str1)`，去掉`str`左右两边的`str1`，`str1`中的每个字符都会被去掉。
- `strings.TrimLeft(str, str1)`和`strings.TrimRight(str, str1)`用法类似。

##### ⑬`strings.HasSuffix(str, str1)`

- 判断`str`是不是`str1`结尾
- 类似的还有`strings.HasPrefix(str, str1)`，判断`str`是不是`str1`开头

#### 2、日期时间函数

- 需要导入`time`包
- `time.Time`用来表示时间类型

##### ①`time.Now()`

- 获取当前时间

##### ②`Format("2006-01-02 15:04:05.999999999 -0700 MST")`

- 格式化时间

  ```go
  package main
  
  import (
  	"fmt"
  	"time"
  )
  
  func main() {
  	now := time.Now()
  	fmt.Printf("now = %v, type = %T\n", now, now)
  	println("年 =", now.Year())
  	println("月 =", now.Month())
  	println("日 =", now.Day())
  	println("时 =", now.Hour())
  	println("分 =", now.Minute())
  	println("秒 =", now.Second())
  	// 格式化时间, 这里
  	// 2006-01-02 15:04:05.999999999 -0700 MST
  	// 里面的数字是固定的必须这样写......
  	// 需要格式化哪个位置就写那个位置, 分隔符是随意的
  	println(now.Format("2006-01-02 15:04:05.999999999 -0700 MST"))
  	println(now.Format("2006-01-02 15:04:05"))
  	println(now.Format("20060102150405"))
  	i := 0
  	for {
  		println(i)
  		// 这里必须传入整数类型, 可使用的单位如下
  		// const (
  		// Nanosecond  Duration = 1
  		// Microsecond          = 1000 * Nanosecond
  		// Millisecond          = 1000 * Microsecond
  		// Second               = 1000 * Millisecond
  		// Minute               = 60 * Second
  		// Hour                 = 60 * Minute
  		//)
  		// 比如5s可以使用5 * time.Second实现
  		// time.Sleep(time.Second * 5)
  		if i >= 2 {
  			break
  		}
  		i++
  	}
  	println(now.Unix())
  	println(now.UnixNano())
  	// 获取运行时间, 返回纳秒, 可以转换
  	println(time.Since(now).Milliseconds())
  }
  ```

- 放上部分`time`包的源码：

  ```go
  // These are predefined layouts for use in Time.Format and time.Parse.
  // The reference time used in these layouts is the specific time stamp:
  //
  //	01/02 03:04:05PM '06 -0700
  //
  // (January 2, 15:04:05, 2006, in time zone seven hours west of GMT).
  // That value is recorded as the constant named Layout, listed below. As a Unix
  // time, this is 1136239445. Since MST is GMT-0700, the reference would be
  // printed by the Unix date command as:
  //
  //	Mon Jan 2 15:04:05 MST 2006
  //
  // It is a regrettable historic error that the date uses the American convention
  // of putting the numerical month before the day.
  //
  // The example for Time.Format demonstrates the working of the layout string
  // in detail and is a good reference.
  //
  // Note that the RFC822, RFC850, and RFC1123 formats should be applied
  // only to local times. Applying them to UTC times will use "UTC" as the
  // time zone abbreviation, while strictly speaking those RFCs require the
  // use of "GMT" in that case.
  // In general RFC1123Z should be used instead of RFC1123 for servers
  // that insist on that format, and RFC3339 should be preferred for new protocols.
  // RFC3339, RFC822, RFC822Z, RFC1123, and RFC1123Z are useful for formatting;
  // when used with time.Parse they do not accept all the time formats
  // permitted by the RFCs and they do accept time formats not formally defined.
  // The RFC3339Nano format removes trailing zeros from the seconds field
  // and thus may not sort correctly once formatted.
  //
  // Most programs can use one of the defined constants as the layout passed to
  // Format or Parse. The rest of this comment can be ignored unless you are
  // creating a custom layout string.
  //
  // To define your own format, write down what the reference time would look like
  // formatted your way; see the values of constants like ANSIC, StampMicro or
  // Kitchen for examples. The model is to demonstrate what the reference time
  // looks like so that the Format and Parse methods can apply the same
  // transformation to a general time value.
  //
  // Here is a summary of the components of a layout string. Each element shows by
  // example the formatting of an element of the reference time. Only these values
  // are recognized. Text in the layout string that is not recognized as part of
  // the reference time is echoed verbatim during Format and expected to appear
  // verbatim in the input to Parse.
  //
  //	Year: "2006" "06"
  //	Month: "Jan" "January" "01" "1"
  //	Day of the week: "Mon" "Monday"
  //	Day of the month: "2" "_2" "02"
  //	Day of the year: "__2" "002"
  //	Hour: "15" "3" "03" (PM or AM)
  //	Minute: "4" "04"
  //	Second: "5" "05"
  //	AM/PM mark: "PM"
  //
  // Numeric time zone offsets format as follows:
  //
  //	"-0700"     ±hhmm
  //	"-07:00"    ±hh:mm
  //	"-07"       ±hh
  //	"-070000"   ±hhmmss
  //	"-07:00:00" ±hh:mm:ss
  //
  // Replacing the sign in the format with a Z triggers
  // the ISO 8601 behavior of printing Z instead of an
  // offset for the UTC zone. Thus:
  //
  //	"Z0700"      Z or ±hhmm
  //	"Z07:00"     Z or ±hh:mm
  //	"Z07"        Z or ±hh
  //	"Z070000"    Z or ±hhmmss
  //	"Z07:00:00"  Z or ±hh:mm:ss
  //
  // Within the format string, the underscores in "_2" and "__2" represent spaces
  // that may be replaced by digits if the following number has multiple digits,
  // for compatibility with fixed-width Unix time formats. A leading zero represents
  // a zero-padded value.
  //
  // The formats __2 and 002 are space-padded and zero-padded
  // three-character day of year; there is no unpadded day of year format.
  //
  // A comma or decimal point followed by one or more zeros represents
  // a fractional second, printed to the given number of decimal places.
  // A comma or decimal point followed by one or more nines represents
  // a fractional second, printed to the given number of decimal places, with
  // trailing zeros removed.
  // For example "15:04:05,000" or "15:04:05.000" formats or parses with
  // millisecond precision.
  //
  // Some valid layouts are invalid time values for time.Parse, due to formats
  // such as _ for space padding and Z for zone information.
  const (
  	Layout      = "01/02 03:04:05PM '06 -0700" // The reference time, in numerical order.
  	ANSIC       = "Mon Jan _2 15:04:05 2006"
  	UnixDate    = "Mon Jan _2 15:04:05 MST 2006"
  	RubyDate    = "Mon Jan 02 15:04:05 -0700 2006"
  	RFC822      = "02 Jan 06 15:04 MST"
  	RFC822Z     = "02 Jan 06 15:04 -0700" // RFC822 with numeric zone
  	RFC850      = "Monday, 02-Jan-06 15:04:05 MST"
  	RFC1123     = "Mon, 02 Jan 2006 15:04:05 MST"
  	RFC1123Z    = "Mon, 02 Jan 2006 15:04:05 -0700" // RFC1123 with numeric zone
  	RFC3339     = "2006-01-02T15:04:05Z07:00"
  	RFC3339Nano = "2006-01-02T15:04:05.999999999Z07:00"
  	Kitchen     = "3:04PM"
  	// Handy time stamps.
  	Stamp      = "Jan _2 15:04:05"
  	StampMilli = "Jan _2 15:04:05.000"
  	StampMicro = "Jan _2 15:04:05.000000"
  	StampNano  = "Jan _2 15:04:05.000000000"
  )
  ```

##### ③时间的常量

```go
const (
	Nanosecond  Duration = 1
	Microsecond          = 1000 * Nanosecond
	Millisecond          = 1000 * Microsecond
	Second               = 1000 * Millisecond
	Minute               = 60 * Second
	Hour                 = 60 * Minute
)
```

##### ④`func (t Time) Unix() int64`

- Unix 将 t 表示为 Unix 时间，即从时间点`January 1, 1970 UTC`到时间点t所经过的时间（单位秒）。
- 类似的还有`func (t Time) UnixNano() int64`，`UnixNano`将t表示为`Unix`时间，即从时间点`January 1, 1970 UTC`到时间点t所经过的时间（单位纳秒）。如果纳秒为单位的`unix`时间超出了`int64`能表示的范围，结果是未定义的。注意这就意味着`Time`零值调用`UnixNano`方法的话，结果是未定义的。

#### 3、内置函数

- 所有内置函数可查看`builtin`包

##### ①`len()`

- 用来求长度

##### ②`new()`

- 用来分配内存，主要用来分配***值**类型*，比如`int, float32, struct...`返回的是指针。

##### ③`make()`

- 用来分配内存，主要给***引用***类型分配。

##### ④`println()`

- 类似的还有`print()`

#### 4、错误处理

- GO不支持`try-catch-finally`，GO使用`defer, panic, recover`来处理

- GO中可以抛出一个`painc`异常，然后在`defer`中通过`recover`捕获异常后处理。

  ```go
  package main
  
  import "fmt"
  
  func test(num1 int32, num2 int32) {
  	// 使用defer、recover来处理异常
  	defer func() {
  		// recover()是一个内齿函数, 可以捕获到异常
  		// err := recover()
  		// 说明捕获到异常
  		if err := recover(); err != nil {
  			fmt.Printf("err = %v\n", err)
  		}
  	}()
  	res := num1 / num2
  	println("res =", res)
  }
  
  func main() {
  
  	test(10, 0)
  	println("main...")
  }
  /*
  err = runtime error: integer divide by zero
  main...
  */
  ```

- 使用`errors.New()`和`panic`来自定义错误

  - `panic`可以接收一个`interface{}`类型的值，也就是任何类型作为参数。输出错误信息并退出程序。

  ```go
  package main
  
  import (
  	"errors"
  	"strings"
  )
  
  // 自定义错误
  func main() {
  	invoke("config.ini")
  	invoke("config")
  	println("main...")
  }
  
  func invoke(name string) {
  	err := readConfig(name)
  	if err != nil {
  		panic(err)
  	}
  	println("continue...")
  }
  
  func readConfig(name string) error {
  	if strings.HasSuffix(name, ".ini") {
  		return nil
  	} else {
  		return errors.New("读取文件错误")
  	}
  }
  /*
  continue...
  panic: 读取文件错误
  
  goroutine 1 [running]:
  main.invoke(...)
          e:/goworkspace/src/go_code/self_demo/demo03/main.go:18
  main.main()
          e:/goworkspace/src/go_code/self_demo/demo03/main.go:11 +0x11d     
  exit status 2
  */
  ```

#### 5、数组

- 在GO语言中，数组是**值类型**

  ```go
  package main
  
  import "fmt"
  
  // 数组的定义和初始化
  func main() {
  	var numberArr01 [3]int = [3]int{1, 2, 3}
  	fmt.Printf("numberArr01 = %v\n", numberArr01)
  	var numberArr02 = [3]int{1, 2, 3}
  	fmt.Printf("numberArr02 = %v\n", numberArr02)
  	var numberArr03 = []int{1, 2, 3}
  	fmt.Printf("numberArr03 = %v\n", numberArr03)
  	numberArr04 := []string{1: "tom", 0: "jack", 2: "mary"}
  	fmt.Printf("numberArr04 = %v\n", numberArr04)
  
  }
  /*
  numberArr01 = [1 2 3]
  numberArr02 = [1 2 3]
  numberArr03 = [1 2 3]
  numberArr04 = [jack tom mary]
  */
  ```

- 数组的长度一旦声明了就不能变化，其长度是固定的。

- 数组默认是值传递。

- 数组的排序

  ```go
  package main
  
  import (
  	"fmt"
  	"sort"
  )
  
  // 数组的排序和查找
  func main() {
  
  	var size int
  	var arr []int
  	var number int
  	var maxChangeTime int = 0
  	print("输入数组大小: ")
  	fmt.Scanln(&size)
  	println("输入数组每一个元素: ")
  	for i := 0; i < size; i++ {
  		fmt.Scanf("%d", &number)
  		arr = append(arr, number)
  	}
  
  	// for i := 0; i < len(arr)-1; i++ {
  	// 	for j := i + 1; j < len(arr); j++ {
  	// 		if arr[i] > arr[j] {
  	// 			tmp := arr[i]
  	// 			arr[i] = arr[j]
  	// 			arr[j] = tmp
  	// 		}
  	// 		maxChangeTime++
  	// 	}
  	// }
  
  	// 逆序
  	sort.Sort(sort.Reverse(sort.IntSlice(arr)))
  	// 正序
  	sort.Ints(arr)
  
  	// for i := 0; i < len(arr)-1; i++ {
  	// 	for j := 0; j < len(arr)-1-i; j++ {
  	// 		if arr[j] > arr[j+1] {
  	// 			tmp := arr[j]
  	// 			arr[j] = arr[j+1]
  	// 			arr[j+1] = tmp
  	// 		}
  	// 		maxChangeTime++
  	// 	}
  	// }
  
  	println("排序后：")
  	for i := 0; i < size; i++ {
  		fmt.Printf("%d ", arr[i])
  	}
  	println("\n交换次数:", maxChangeTime)
  }
  ```

- 二维数组

#### 6、切片

- 切片是数组的一个引用，它的使用和数组类似，切片的长度是可以变化的。

- 切片的定义：`var 切片名 []类型`

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  func main() {
  	// 声明数组
  	var intArr = []int{1, 2, 3, 4, 5}
  	fmt.Printf("intArr = %v\n", intArr)
  	// 声明切片
  	var slice = intArr[2:4]
  	fmt.Printf("slice = %v, len = %v, cap = %v\n", slice, len(slice), cap(slice))
  	fmt.Printf("intArr[2]的地址是: %v, slice[0]的地址是: %v\n", &intArr[2], &slice[0])
  	fmt.Printf("intArr[3]的地址是: %v, slice[1]的地址是: %v\n", &intArr[3], &slice[1])
  	slice[0] = 22
  	fmt.Printf("intArr[2] = %v, slice[0] = %v\n", intArr[2], slice[0])
  }
  /*
  intArr = [1 2 3 4 5]
  slice = [3 4], len = 2, cap = 3
  intArr[2]的地址是: 0xc000010400, slice[0]的地址是: 0xc000010400
  intArr[3]的地址是: 0xc000010408, slice[1]的地址是: 0xc000010408
  intArr[2] = 22, slice[0] = 22
  */
  ```

  - 不指定的情况下，切片的`len`是元素个数，`cap`是`数组长度-start`
  - `:`的使用类似`python`

- 切片实际上保存的是对数组的引用。在切片中改变值后，对应的数组的值也会被改变。也就是说，**切片和数组实际上是操作同一块内存区域**。

- 使用`make`创建切片：`make([]type, len, cap)`，`cap`必须大于`len`，`len`必须有，`cap`可选。因为使用`make`创建切片时会创建一个数组。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  func main() {
  	// 使用make声明, 对于切片，必须make后使用
  	var sliceMake = make([]float64, 5)
  	fmt.Printf("sliceMake = %v, len = %v, cap = %v\n", sliceMake, len(sliceMake), cap(sliceMake))
  }
  /*
  sliceMake = [0 0 0 0 0], len = 5, cap = 5
  */
  ```

  - 这种方式下相当于创建了一个没有名字的数组，只能通过切片来访问了。

- 使用内置函数`append`方法添加元素时，也会覆盖原数组

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  func main() {
  	// 声明数组
  	var intArr = []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
  	fmt.Printf("intArr = %v\n", intArr)
  	var intSlice = intArr[:1]
  	fmt.Printf("intSlice: %v\n", intSlice)
  	intSlice = append(intSlice, 100)
  	fmt.Printf("intArr[1] = %v, %v\n", intArr[1], &intArr[1])
  	fmt.Printf("intSlice[1] = %v, %v\n", intSlice[1], &intSlice[1])
  	fmt.Printf("intArr[1] = %v, %v\n", intArr[1], &intArr[1])
  	fmt.Printf("intArr = %v\n", intArr)
  }
  /*
  intArr = [0 1 2 3 4 5 6 7 8 9]
  intSlice: [0]
  intArr[1] = 100, 0xc000012738
  intSlice[1] = 100, 0xc000012738
  intArr[1] = 100, 0xc000012738
  intArr = [0 100 2 3 4 5 6 7 8 9]
  */
  ```

- 如果`append`后的长度大于了数组的长度，会产生一个新的切片，也就是说新的切片保存的引用不指向原数组了。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  func main() {
  	// 声明数组
  	var intArr = []int{0, 1, 2, 3}
  	var intSce = intArr[:]
  	for i := range intArr {
  		fmt.Printf("intArr[%v]: %v\t", i, &intArr[i])
  	}
  	println()
  	for i := range intSce {
  		fmt.Printf("intSli[%v]: %v\t", i, &intSce[i])
  	}
  	println("\n----------------------")
  	intSce[0] = 100
  	fmt.Printf("\nintArr[0] = %v, intSce[0] = %v\n\n", intArr[0], intSce[0])
  
  	// 添加一个元素后超过数组长度
  	intSce = append(intSce, 100)
  	for i := range intArr {
  		fmt.Printf("intArr[%v]: %v\t", i, &intArr[i])
  	}
  	println()
  	for i := range intSce {
  		fmt.Printf("intSce[%v]: %v\t", i, &intSce[i])
  	}
  	println("\n----------------------")
  	intSce[0] = 200
  	fmt.Printf("\nintArr[0] = %v, intSce[0] = %v\n\n", intArr[0], intSce[0])
  	
  }
  /*
  intArr[0]: 0xc00001a1e0 intArr[1]: 0xc00001a1e8 intArr[2]: 0xc00001a1f0 intArr[3]: 0xc00001a1f8
  intSli[0]: 0xc00001a1e0 intSli[1]: 0xc00001a1e8 intSli[2]: 0xc00001a1f0 intSli[3]: 0xc00001a1f8
  ----------------------
  
  intArr[0] = 100, intSce[0] = 100
  
  intArr[0]: 0xc00001a1e0 intArr[1]: 0xc00001a1e8 intArr[2]: 0xc00001a1f0 intArr[3]: 0xc00001a1f8
  intSce[0]: 0xc0000144c0 intSce[1]: 0xc0000144c8 intSce[2]: 0xc0000144d0 intSce[3]: 0xc0000144d8 intSce[4]: 0xc0000144e0
  ----------------------
  
  intArr[0] = 100, intSce[0] = 200
  */
  ```

- 切片的嵌套也遵循相同的规则。只要记住切片保存的是引用即可。

- 使用内置函数`copy`来拷贝两个切片时，这两个切片的引用相互独立。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  func main() {
  	var intArr = [3]int{1, 2, 3}
  	var slice1 []int = intArr[:]
  	var slice2 []int = make([]int, 10)
  	copy(slice2, slice1)
  	for i := 0; i < len(slice1); i++ {
  		fmt.Printf("intArr[%v] = %v, 地址: %v\n", i, intArr[i], &intArr[0])
  		fmt.Printf("slice1[%v] = %v, 地址: %v\n", i, slice1[i], &slice1[0])
  		fmt.Printf("slice2[%v] = %v, 地址: %v\n\n", i, slice2[i], &slice2[0])
  	}
  	slice1[0] = 100
  	fmt.Printf("intArr[0] = %v, slice1[0] = %v, slice2[0] = %v\n", intArr[0], slice1[0], slice2[0])
  	slice2[1] = 200
  	fmt.Printf("intArr[1] = %v, slice1[1] = %v, slice2[1] = %v\n", intArr[1], slice1[1], slice2[1])
  }
  /*
  intArr[0] = 1, 地址: 0xc00000e1b0
  slice1[0] = 1, 地址: 0xc00000e1b0
  slice2[0] = 1, 地址: 0xc000012730
  
  intArr[1] = 2, 地址: 0xc00000e1b0
  slice1[1] = 2, 地址: 0xc00000e1b0
  slice2[1] = 2, 地址: 0xc000012730
  
  intArr[2] = 3, 地址: 0xc00000e1b0
  slice1[2] = 3, 地址: 0xc00000e1b0
  slice2[2] = 3, 地址: 0xc000012730
  
  intArr[0] = 100, slice1[0] = 100, slice2[0] = 1
  intArr[1] = 2, slice1[1] = 2, slice2[1] = 200
  */
  ```

- 处理带汉字的`string`时需要转成`[]rune`

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  func main() {
  	// string底层是一个byte数组, 可以进行切片处理
  	str1 := "hello@world"
  	fmt.Printf("%v\n", str1[6:])
  	str2 := "你好，世界"
  	tmp := []rune(str2)
  	fmt.Printf("%c\n", tmp[3:])
  }
  /*
  world
  [世 界]
  */
  ```


#### 7、map

- 基本语法：`var 变量名 map[keytype]valuetype`，例如`var myMap map[string]string`

- **`slice, map和func`不能作为`key`**

- 只声明不会分配内存，需要使用`make`来分配内存

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // map
  func main() {
  	var myMap = make(map[string]string, 1)
  	myMap["名字"] = "tom"
  	myMap["age"] = "18"
  	myMap["籍贯"] = "北京"
  	fmt.Printf("myMap = %v\n", myMap)
  	for key, value := range myMap {
  		fmt.Printf("key = %v, value = %v\n", key, value)
  	}
  	println("len =", len(myMap))
  	// 还可以在声明的时候直接赋值
  	var myMap2 = map[string]string{"编号": "001", "单价": "12.8"}
  	fmt.Printf("myMap2: %v\n", myMap2)
  	// 添加数据
  	myMap2["库存"] = "20"
  	// 删除数据, map只能一次删除一个数据, 如果想删除所有数据, 只能逐个遍历
  	// 或者将其重新分配空间, 之前的数据被gc回收, myMap2 = make(map[string]string)
  	delete(myMap2, "单价")
  	// 修改数据
  	myMap2["编号"] = "002"
  	// 查询数据
  	if val, ok := myMap2["单价"]; ok {
  		fmt.Println(val)
  	} else {
  		fmt.Println(`没有这个key: "单价"`)
  	}
  }
  /*
  myMap = map[age:18 名字:tom 籍贯:北京]
  key = age, value = 18
  key = 籍贯, value = 北京
  key = 名字, value = tom
  len = 3
  myMap2: map[单价:12.8 编号:001]
  没有这个key: "单价"
  */
  ```

- 如果切片的类型是`map`，则这是一个`map`切片

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  type Student struct {
  	name    string
  	age     uint16
  	address string
  }
  
  // struct
  func main() {
  	myMap := make(map[string]Student)
  	myMap["001"] = Student{"小明", 18, "北京"}
  	myMap["002"] = Student{"小华", 20, "上海"}
  	myMap["003"] = Student{"小红", 19, "广州"}
  	for k, s := range myMap {
  		fmt.Printf("学号: %v, ", k)
  		fmt.Printf("姓名: %v, 年龄： %v, 住址: %v\n", s.name, s.age, s.address)
  	}
  }
  /*
  学号: 001, 姓名: 小明, 年龄： 18, 住址: 北京
  学号: 002, 姓名: 小华, 年龄： 20, 住址: 上海
  学号: 003, 姓名: 小红, 年龄： 19, 住址: 广州
  */
  ```

#### 8、GO的面向对象编程

- GO没有类的概念，使用结构体`struct`来实现类似功能。结构体的语法和C类似。

  ```go
  // utils.go
  package utils
  
  // 这里的address是小写, 其他包不可访问
  type Student struct {
  	Name    string
  	Age     uint16
  	address string
  }
  
  var Stu = Student{"", 0, "上海"}
  
  // main.go
  package main
  
  import (
  	"fmt"
  	"go_code/self_demo/demo14/utils"
  )
  
  // struct
  func main() {
  	var stu1 = utils.Stu
  	stu1.Name = "张三"
  	stu1.Age = 18
  	// address是小写开头, 因此无法在其它包被使用
  	// stu1.address = "北京"
  	fmt.Printf("%v\n", stu1)
  }
  ```

- 结构体是值类型，默认是值传递。

- `struct`的每个字段上，可以写一个`tag`，该`tag`可以通过反射机制获取，常见的使用场景就是序列化和反序列化。

  ```go
  package main
  
  import (
  	"encoding/json"
  	"fmt"
  )
  
  type Student struct {
  	// 添加tag
  	Name    string `json:"name"`
  	Age     uint16 `json:"age"`
  	Address string `json:"address"`
  }
  
  // struct
  func main() {
  	stu := Student{"张三", 18, "北京"}
  	fmt.Printf("stu: %v\n", stu)
  	// 序列化, json格式, 使用到了反射
  	jsonStu, err := json.Marshal(stu)
  	if err != nil {
  		println("序列化失败")
  	} else {
  		fmt.Printf("jsonStu = %s\n", jsonStu)
  	}
  }
  /*
  stu: {张三 18 北京}
  jsonStu = {"name":"张三","age":18,"address":"北京"}
  */
  ```

- 继承

  ```go
  package model
  
  import "fmt"
  
  type Goods struct {
  	Name  string
  	price float64
  }
  
  func (goods *Goods) PrintName() {
  	fmt.Printf("Name = %v\n", goods.Name)
  }
  
  ```

  ```go
  package main
  
  import (
  	"fmt"
  	"go_code/demo_oop/demo02/model"
  )
  
  type Book struct {
  	model.Goods
  	Number string
  }
  
  func main() {
  	// 用法1
  	var book1 = &Book{}
  	book1.Goods.Name = "张三"
  	// 跨包依然不能访问消协开头字段
  	// book.Goods.price = 10.2
  	book1.Number = "001"
  	// 可以简写为book.PrintName()
  	book1.Goods.PrintName()
  	fmt.Printf("book1 = %v\n", *book1)
  	// 用法2
      // Go创建结构体实例的时候要么全有字段名，要么全省略字段名，不能一些有一些没有，否则编译报错。
  	var book2 = &Book{
  		model.Goods{
  			Name: "书",
  		},
  		"002",
  	}
  	fmt.Printf("book2 = %v\n", *book2)
  }
  ```

  - 如果结构体和匿名结构体有相同的字段或方法时，采用就近原则，要想访问匿名结构体的方法或字段，则需要写明匿名结构体的名字。
  - 如果嵌入的多个结构体有相同的字段或方法，并且本结构体没有该字段和方法，则必须显式指定调用的是哪一个匿名结构体的方法和字段。否则编译出错。
  - 推荐使用时都指明调用的是哪个结构体的方法和字段。
  - 如果嵌套的结构体有名字，这种情况叫组合，必须通过结构体名来访问其字段和方法。
  - Go创建结构体实例的时候要么全有字段名，要么全省略字段名，不能一些有一些没有，否则编译报错。

#### 9、方法

- Go中的方法是作用在指定的数据类型上的（即：和指定的数据类型绑定），因此自定义类型都可以有方法，而不仅仅是`struct`

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  type Student struct {
  	name    string
  	age     uint16
  	address string
  }
  
  // 给Student类型绑定方法
  func (stu Student) getName() string {
  	return stu.name
  }
  
  func (stu Student) getAge() uint16 {
  	return stu.age
  }
  
  func (stu Student) getAddress() string {
  	return stu.address
  }
  
  // struct
  func main() {
  	stu := Student{"张三", 19, "北京"}
  	fmt.Printf("name = %v\n", stu.getName())
  	fmt.Printf("age = %v\n", stu.getAge())
  	fmt.Printf("address = %v\n", stu.getAddress())
  }
  /*
  ame = 张三
  age = 19
  address = 北京
  */
  ```

- 方法的访问控制规则和函数一致。

- 如果一个类型实现了`String`这个方法，那么`fmt.Println`会默认调用这个方法。

- GO在创建结构体实例时，可以直接指定字段的值。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  type Student struct {
  	name    string
  	age     uint16
  	address string
  }
  
  // 给Student类型绑定方法
  func (stu Student) getName() string {
  	return stu.name
  }
  
  func (stu Student) getAge() uint16 {
  	return stu.age
  }
  
  func (stu Student) getAddress() string {
  	return stu.address
  }
  
  // struct
  func main() {
  	stu := Student{
  		name:    "张三",
  		address: "北京",
  	}
  	fmt.Printf("name = %v\n", stu.getName())
  	fmt.Printf("age = %v\n", stu.getAge())
  	fmt.Printf("address = %v\n", stu.getAddress())
  }
  /*
  name = 张三
  age = 0
  address = 北京
  */
  ```

#### 10、接口

- **GO语言的接口不能有变量，只能有方法。**必须实现接口的所有方法才能叫实现了该接口，并且参数和返回值要完全相同。

- 接口里的方法只能定义，不能有任何实现。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  type Usb interface {
  	// 声明了两个没有实现的方法
  	// 方法可以有返回值, 可以有参数
  	Start()
  	Stop()
  }
  
  type Phone struct {
  	name string
  }
  
  type Computer struct {
  }
  
  // 让Phone实现Usb接口的方法
  func (phone Phone) Start() {
  	fmt.Println(phone.name + "开始工作")
  }
  
  func (phone Phone) Stop() {
  	fmt.Println(phone.name + "停止工作")
  }
  
  // 实现接口, 就是实现接口声明中的所有方法
  func (computer Computer) Working(usb Usb) {
  	usb.Start()
  	usb.Stop()
  }
  func main() {
  	computer := Computer{}
  	phone := Phone{"手机"}
  	computer.Working(phone)
  }
  ```

- 只要是自定义类型，都可以实现接口，而不仅仅是`struct`。

- 只有实现了的接口才能被赋值给变量。（A变量实现了B接口，就可以将A变量赋值给B接口）

- 可以实现多个接口。

- 接口也可以继承别的接口。如果有同名的方法，其参数和返回值必须完全相同，也就是不同的接口定义的必须是相同的方法才能被其他接口同时继承。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  type A interface {
  	test01()
  }
  
  type B interface {
  	test02()
  }
  
  type C interface {
  	A
  	B
  	test03()
  }
  
  type Stu struct {
  }
  
  func (stu *Stu) test01() {
  }
  func (stu *Stu) test02() {
  }
  
  func (stu *Stu) test03() {
  }
  
  func main() {
  	var stu Stu
  	var a A = &stu
  	var b B = &stu
  	var c C = &stu
  	a.test01()
  	b.test02()
  	c.test03()
  	fmt.Println()
  }
  ```

- 接口是引用类型。

#### 11、类型断言

- 由于接口是一般类型，不知道具体类型，如果要转成具体类型，就需要使用类型断言。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  type Point struct {
  	x int
  	y int
  }
  
  func main() {
  	var a interface{}
  	var point = Point{1, 2}
  	a = point
  	// 这就是类型断言
  	var b Point = a.(Point)
  	fmt.Println(b)
  }
  ```

- 进行类型断言时，如果类型不匹配就会报错。所以需要带上检测机制。

  ```go
  // 检测是否转换成功
  c, ok := a.(float32)
  if ok {
      fmt.Println(c)
  } else {
      fmt.Println("转换失败")
  }
  ```

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  func main() {
  	TypeJudge(10, 10.9, true, nil, "张三", 'A', -1)
  }
  
  func TypeJudge(typeArrs ...interface{}) {
  	for index, value := range typeArrs {
  		switch value.(type) {
  		case bool:
  			fmt.Printf("第%d个参数是bool类型, 值是%v\n", index, value)
  		case int, int16, int32, int64:
  			fmt.Printf("第%d个参数是int类型, 值是%v\n", index, value)
  		case string:
  			fmt.Printf("第%d个参数是string类型, 值是%v\n", index, value)
  		case float32, float64:
  			fmt.Printf("第%d个参数是float类型, 值是%v\n", index, value)
  		case uint, uint16, uint32, uint64:
  			fmt.Printf("第%d个参数是uint类型, 值是%v\n", index, value)
  		default:
  			fmt.Printf("第%d个参数是其它类型, 值是%v\n", index, value)
  		}
  
  	}
  }
  ```


### 四、文件操作

- `os.File`结构体封装文件所有操作。

#### 1、常用操作

##### ①打开文件

- `os.Open(name string)`

##### ②关闭文件

- `os.Close(name string)`

```go
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	// 打开文件
	file, err := os.Open("E:/goworkspace/src/test_code/demo01/text.txt")
	// 要及时关闭文件
	defer file.Close()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	} else {
		fmt.Println(file.Name())
	}
	// 创建一个*Reader, 是带缓冲的, 默认缓冲大小是4096
	reader := bufio.NewReader(file)
	// 循环读取文件的内容
	for {
		// 读取到\n就结束
		str, err := reader.ReadString('\n')
		// io.EOF代表文件末尾
		if err == io.EOF {
			break
		}
		// 输出内容
		fmt.Print(str)
	}
}
```

```go
package main

import (
	"fmt"
	"os"
)

func main() {
	filePath := "E:/goworkspace/src/test_code/demo02/text.txt"
	// 打开文件, 这种一次读取所有文件
	content, err := os.ReadFile(filePath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// 显示内容
	fmt.Printf("%v", string(content))
}
```

```go
package main

import (
   "bufio"
   "fmt"
   "os"
   "strconv"
)

func main() {
   filePath := "E:\\goworkspace\\src\\test_code\\demo03\\text.txt"
   // 写文件
   file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0666)
   if err != nil {
      fmt.Println(err)
      return
   }
   // 写入五句话
   str := "hello world"
   // 使用带缓存的*Writer
   writer := bufio.NewWriter(file)
   for i := 0; i < 5; i++ {
      // 先写入缓存
      writer.WriteString(str + strconv.Itoa(i) + "\n")
   }
   // 将缓存的数据写入文件中
   writer.Flush()
   defer file.Close()
}
```

```go
package main

import (
	"fmt"
	"os"
)

func main() {

	filePath := "E:/goworkspace/src/test_code/demo04/text01.txt"
	// 写文件, 追加
	// 判断文件是否存在
	_, err := os.Stat(filePath)
	if err == nil {
		fmt.Println("文件或目录存在")
	} else if os.IsNotExist(err) {
		fmt.Println("文件或目录不存在")
	}
	defer func() {
		err := recover()
		if err != nil {
			fmt.Println(err)
		}
	}()
}
```

```go
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func copyFile(srcPath string, dstPath string) (written int64, err error) {
	srcFile, err := os.Open(srcPath)
	if err != nil {
		return 0, err
	}
	reader := bufio.NewReader(srcFile)

	dstFile, err := os.OpenFile(dstPath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return 0, err
	}
	writer := bufio.NewWriter(dstFile)
	return io.Copy(writer, reader)
}

func main() {
	// 拷贝文件
	srcPath := "E:/goworkspace/src/test_code/demo06/text.txt"
	dstPath := "E:/goworkspace/src/test_code/demo06/text_copy.txt"
	written, err := copyFile(srcPath, dstPath)
	if err != nil {
		fmt.Println("拷贝失败", err)
	} else {
		fmt.Println("拷贝成功", written)
	}
}
```

### 五、命令行参数

- `os.Args`是一个`string`的切片，用来存储所有的命令行参数

  ```go
  package main
  
  import (
  	"fmt"
  	"os"
  )
  
  func main() {
  	fmt.Printf("命令行的参数: %v\n", len(os.Args))
  	for index, value := range os.Args {
  		fmt.Printf("index = %v, value = %v\n", index, value)
  	}
  
  }
  ```

- 用`flag`包来解析命令行参数

  ```go
  package main
  
  import (
  	"flag"
  	"fmt"
  )
  
  func main() {
  	// 定义变量接收命令行参数
  	var user string
  	var pass string
  	var host string
  	var port uint
  	// &user就是接收用户命令行中输入 -u 后面的参数值
  	flag.StringVar(&user, "u", "", "用户名, 默认为空")
  	flag.StringVar(&pass, "P", "", "密码, 默认为空")
  	flag.StringVar(&host, "h", "localhost", "主机名, 默认为localhost")
  	flag.UintVar(&port, "p", 0, "端口, 默认0")
  	// 此方法必须调用
  	flag.Parse()
  
  	fmt.Printf("user = %v\n", user)
  	fmt.Printf("pass = %v\n", pass)
  	fmt.Printf("host = %v\n", host)
  	fmt.Printf("port = %v\n", port)
  }
  ```

### 六，JSON操作

#### 1、序列化

```go
package main

import (
	"encoding/json"
	"fmt"
	"time"
)

type Student struct {
	// 如果字段小写, 则无法序列化
	// 可以指定序列化后的名字
	Name     string `json:"name"`
	Age      int    `json:"age"`
	Birthday string `json:"birthday"`
}

// 序列化结构体
func serializeStruct() {
	stu := &Student{
		Name:     "张三",
		Age:      18,
		Birthday: "2000-01-01",
	}
	data, err := json.Marshal(stu)
	if err != nil {
		fmt.Printf("struct序列化错误: %v\n", err)
	} else {
		fmt.Printf("struct序列化后: %s\n", data)
	}
}

// 序列化map
func serializeMap() {
	stu := make(map[string]interface{})
	stu["name"] = "李四"
	stu["age"] = 19
	stu["birthday"] = time.Now()
	stu["hobby"] = [...]string{"篮球", "足球", "电影"}
	data, err := json.Marshal(stu)
	if err != nil {
		fmt.Printf("map序列化错误: %v\n", err)
	} else {
		fmt.Printf("map序列化后: %s\n", data)
	}
}

// 序列化切片
func serializeSlice() {
	var stus []map[string]interface{}
	stu1 := make(map[string]interface{})
	stu1["name"] = "王五"
	stu1["age"] = 20
	stu1["birthday"] = time.Now().Format("2006-01-02")
	stu1["hobby"] = [...]string{"篮球", "足球", "电影"}
	stus = append(stus, stu1)

	stu2 := make(map[string]interface{})
	stu2["name"] = "赵六"
	stu2["age"] = 21
	stu2["birthday"] = time.Now()
	stu2["hobby"] = [...]string{"篮球", "足球", "电影"}
	stus = append(stus, stu2)

	data, err := json.Marshal(stus)
	if err != nil {
		fmt.Printf("slice序列化错误: %v\n", err)
	} else {
		fmt.Printf("slice序列化后: %s\n", data)
	}
}

// 基本数据类型序列化
func serializeFloat64() {
	num := 123.123
	data, err := json.Marshal(num)
	if err != nil {
		fmt.Printf("float64序列化错误: %v\n", err)
	} else {
		fmt.Printf("float64序列化后: %s\n", data)
	}
}

// json序列化
func main() {
	serializeStruct()
	serializeMap()
	serializeSlice()
	// 对基本数据类型的序列化意义不大，一般不需要
	serializeFloat64()
}
```

#### 2、反序列化

```go
package main

import (
	"encoding/json"
	"fmt"
)

type Student struct {
	Name     string
	Age      int
	Birthday string
}

// 反序列化为结构体
func unmarshalStruct() {
	str := `{"name":"张三","age":18,"birthday":"2000-01-01"}`
	var stu Student
	// 必须要传地址, 这样才能改变stu的值
	err := json.Unmarshal([]byte(str), &stu)
	if err != nil {
		fmt.Printf("struct反序列化失败: %v\n", err)
	} else {
		fmt.Printf("struct反序列化结果: %v\n", stu)
	}
}

// 反序列化为map
func unmarshalMap() {
	str := `{"age":19,"birthday":"2022-12-07T20:53:36.8779417+08:00","hobby":["篮球","足球","电 "],"name":"李四"}`
	// 反序列化map不需要make
	// 因为make操作被封装到Unmarshal中
	var a map[string]interface{}
	err := json.Unmarshal([]byte(str), &a)
	if err != nil {
		fmt.Printf("map反序列化失败: %v\n", err)
	} else {
		fmt.Printf("map反序列化结果: %v\n", a)
	}
}

// 反序列化切片
func unmarshalSlice(){
	str := `[{"age":20,"birthday":"2022-12-07","hobby":["篮球","足球","电影"],"name":"王五"},{"age":21,"birthday":"2022-12-07T20:53:36.9401508+08:00","hobby":["篮球","足球","电影"],"name":"赵六"}]`
	var slice []map[string]interface{}
	err := json.Unmarshal([]byte(str), &slice)
	if err != nil {
		fmt.Printf("slice反序列化失败: %v\n", err)
	} else {
		fmt.Printf("slice反序列化结果: %v\n", slice)
	}
}

// json序列化
func main() {
	unmarshalStruct()
	unmarshalMap()
	unmarshalSlice()
}
```

### 七、单元测试

- 使用轻量级的`testing`测试框架和`go test`命令来实现单元测试和性能测试。

- 测试文件需要以`_test.go`结尾，包含`Test`开头的函数，将该文件放在与被测试的包相同的包中。该文件将被排除在正常的程序包之外，但在运行`go test`命令时将被包含。

- **测试`case`的形参类型必须是`*testing.T`**

- `go test`：如果运行正确，无日志，错误时会输出日志

- `go test -v`：无论是否正确都有日志

- 如果只想测试单个文件（目录下有多个测试文件），则需要带上文件名，默认运行所有测试文件。

- 如果只想运行某一个测试方法，使用`go test -run 方法名`

  ```go
  // utils.go
  package utils
  
  func addUpper(n int) int {
  	sum := 0
  	for i := 1; i <= n; i++ {
  		sum += i
  	}
  	return sum
  }
  ```

  ```go
  // utils_test.go
  package utils
  
  import (
  	"fmt"
  	"testing"
  )
  
  // 编写测试用例, 函数名必须使用Test开头
  func TestAddUpper(t *testing.T) {
  	res := addUpper(10)
  	if res != 55 {
  		t.Fatalf("AddUpper(10)执行错误, 期望值: %v, 实际值: %v\n", 55, res)
  	} else {
  		t.Logf("AddUpper(10)执行正确...")
  	}
  }
  
  func TestHello(t *testing.T) {
  	fmt.Println("hello world!")
  }
  ```


### 八、goroutine和channel

- GO语言协程的特点

  - 有独立的栈空间
  - 共享程序堆空间
  - 调度由用户控制
  - 协程是轻量级的线程

  ```go
  package main
  
  import (
  	"fmt"
  	"time"
  )
  
  // 每隔一秒输出hello world
  func printHello() {
  	for i := 1; i <= 10; i++ {
  		fmt.Printf("printHello() hello world---%d\n", i)
  		time.Sleep(time.Second)
  	}
  
  }
  
  // goroutine(协程)
  func main() {
  	go printHello()
  	for i := 1; i <= 5; i++ {
  		fmt.Printf("main() hello world---%d\n", i)
  		time.Sleep(time.Second)
  	}
  
  }
  ```

- 如果主线程结束，则协程不管是否执行完毕都会结束。

#### 1、MPG模式

- M：操作系统的主线程（是物理线程）
- P：协程执行需要的上下文
- G：协程

```go
package main

import (
	"fmt"
	"runtime"
)

// 设置运行的CPU数
func main() {
	// 获取CPU数
	cpuNum := runtime.NumCPU()
	fmt.Printf("cpuNum = %v\n", cpuNum)
	// 可以自己设置使用几个cpu
	// go1.8之后的版本可以不需要设置
	i := runtime.GOMAXPROCS(cpuNum / 2)
	fmt.Printf("旧的设置: %v\n", i)
}

```

- **`-race`参数可以查看是否存在资源竞争问题**

```go
package main

import (
	"fmt"
	"sync"
	"time"
)

/*
计算1-200的各个数的阶乘
*/
var (
	resMap = make(map[int]uint)
	// 声明一个全局的互斥锁
	lock sync.Mutex
)

// 计算一个数的阶乘, 返回计算结果
func calcFactorial(num int) {
	var res uint = 1
	for i := 1; i <= num; i++ {
		res *= uint(i)
	}
	// 写操作需要加锁
	lock.Lock()
	resMap[num] = res
	lock.Unlock()
	fmt.Printf("%v的阶乘结果: %v\n", num, res)
}

func main() {
	// 开启多个协程
	for i := 1; i <= 60; i++ {
		go calcFactorial(i)
	}

	time.Sleep(time.Second * 3)

	lock.Lock()
	for k, v := range resMap {
		fmt.Printf("map[%v] = %v\n", k, v)
	}
	lock.Unlock()
}
```

#### 2、管道

- `channel`的本质是一个队列（先进先出），它是线程安全的。

- 管道是有类型的，比如`string`类型的管道只能存放`string`类型的数据。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // 管道
  func main() {
  	var myChan = make(chan int, 3)
  	// myChan的值是一个地址，说明channel是一个引用类型
  	fmt.Printf("myChan = %v\n", myChan)
  	// 向管道写入数据, 使用 <- 运算符
  	myChan <- 10
  	num := 211
  	myChan <- num
  	// 输出管道的长度和容量(cap)
  	fmt.Printf("myChan len = %v, cap = %v\n", len(myChan), cap(myChan))
  	// 当给管道写入数据时, 不能超过其容量
  	myChan <- 100
  	// myChan <- 100 会报错
  	fmt.Printf("myChan len = %v, cap = %v\n", len(myChan), cap(myChan))
  	// 从管道中读取数据
  	var n int = <-myChan
  	fmt.Printf("n = %v\n", n)
  	fmt.Printf("myChan len = %v, cap = %v\n", len(myChan), cap(myChan))
  	// 在没有使用协程的情况下, 如果管道的数据已经全部取出
  	// 再取就会报错: deadlock
  	<-myChan // 可以只取数据不接收
  	n = <-myChan
  	fmt.Printf("n = %v\n", n)
  	fmt.Printf("myChan len = %v, cap = %v\n", len(myChan), cap(myChan))
  	// n = <-myChan 再取就会报错
  }
  
  /*
  myChan = 0xc00010c080
  myChan len = 2, cap = 3
  myChan len = 3, cap = 3
  n = 10
  myChan len = 2, cap = 3
  n = 100
  myChan len = 0, cap = 3
  */
  ```

- 如果管道存放的是`interface{}`，取数据时注意类型断言

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  type Cat struct {
  	Name string
  	Age  int
  }
  
  // 管道
  func main() {
  	allChan := make(chan interface{}, 5)
  	allChan <- 10
  	allChan <- "hello"
  	allChan <- Cat{"花猫", 4}
  	// 希望获取管道中第三个, 则需要将前两个推出
  	<-allChan
  	<-allChan
  	cat := <-allChan
  	// 虽然直接输出是Cat类型, 但是不能获取Name和Age
  	// 如果输出cat.Name则编译错误
  	fmt.Printf("cat = %v, %T\n", cat, cat)
  	// 正确输出需要类型断言
  	// 上面也可以写成: cat := (<-allChan).(Cat)
  	newCat := cat.(Cat)
  	fmt.Printf("cat.Name = %v, %T\n", newCat.Name, cat)
  }
  ```

- 使用内置函数`close`可以关闭管道，关闭后只能读数据不能写了。

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // 管道的关闭和遍历
  func main() {
  	intChan := make(chan int, 3)
  	intChan <- 1
  	intChan <- 2
  	close(intChan)
  	// 关闭后不能写数据了
  	// intChan <- 3 这里会报错: panic: send on closed channel
  	fmt.Println("OK")
  	// 关闭后可以读数据
  	n1 := <-intChan
  	fmt.Printf("n1 = %v\n", n1)
  	n2 := <-intChan
  	fmt.Printf("n2 = %v\n", n2)
  	// 在关闭后, 如果取完所有数据, 再从管道取值会得到该类型的零值
  	n3 := <-intChan
  	fmt.Printf("n3 = %v\n", n3)
  
  	strChan := make(chan string, 3)
  	strChan <- "aaa"
  	strChan <- "bbb"
  	strChan <- "ccc"
  	// 遍历管道要使用for-range的方式
  	// 在遍历时, 如果管道没有关闭会出现deadlock错误, 但数据可以正常取出
  	// 关闭管道后可以正常遍历
  	close(strChan)
  	for e := range strChan {
  		fmt.Printf("e = %v\n", e)
  	}
  }
  /*
  n1 = 1
  n2 = 2
  n3 = 0
  e = aaa
  e = bbb
  e = ccc
  */
  ```

- 管道和协程配合

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  var (
  	length  = 50
  	intChan = make(chan int, length)
  	// 设置一个退出管道用来控制主线程的退出
  	exitChan = make(chan bool, 1)
  )
  
  func readData() {
  	for {
  		val, ok := <-intChan
  		if ok {
  			fmt.Printf("读取到的值: %v\n", val)
  		} else {
  			break
  		}
  	}
  	exitChan <- true
  	close(exitChan)
  }
  
  func writeData() {
  	for i := 1; i <= length; i++ {
  		intChan <- i
  		fmt.Printf("写入的数据: %v\n", i)
  	}
  	close(intChan)
  }
  
  // goroutine和channel配合使用
  func main() {
  	go writeData()
  	go readData()
  	// 如果管道中没有值并且管道没关闭, 则从管道读取数据时会阻塞
  	// 本程序中当exitChan读取到数据时主线程就结束了
  	<-exitChan
  }
  ```

- 管道的阻塞

  - 如果只向管道写入数据而不取，当数据达到容量时再写就会出现阻塞
  - 如果管道没有关闭，并且管道中没有数据时，从该管道读取数据会阻塞，直到有数据被写入管道。
  - 读和写的速度不一致不影响管道的运行。

- 求素数

  ```go
  package main
  
  import (
  	"fmt"
  	"sync"
  	"time"
  )
  
  var (
  	// 范围
  	maxRange = 100000
  	// 协程数
  	goroutineNum = 4
  	// 读写锁
  	rwLock sync.RWMutex
  	// 存放结果
  	resArr []int
  	// 结束判断
  	exitChan = make(chan bool, 4)
  )
  
  // 计算素数
  func calcPrime(start int, end int) {
  	for i := start; i < end; i++ {
  		if i == 1 || i == 0 {
  			continue
  		}
  		flag := true
  		for j := 2; j*j <= i; j++ {
  			if i%j == 0 {
  				flag = false
  				break
  			}
  		}
  		rwLock.Lock()
  		if flag {
  			resArr = append(resArr, i)
  		}
  		rwLock.Unlock()
  	}
  	exitChan <- true
  }
  
  func main() {
  	startTime := time.Now()
  	tmpNum := maxRange / goroutineNum
  	for i := 0; i < goroutineNum; i++ {
  		go calcPrime(i*tmpNum, (i+1)*tmpNum)
  	}
  
  	for i := 0; i < goroutineNum; i++ {
  		<-exitChan
  	}
  
  	for i, v := range resArr {
  		fmt.Printf("i = %v, v = %v\n", i, v)
  	}
  	endTime := time.Since(startTime).Milliseconds()
  	fmt.Printf("运行花费时间: %v毫秒\n", endTime)
  }
  ```

  ```go
  package main
  
  import (
  	"fmt"
  	"time"
  )
  
  var (
  	// 范围
  	maxRange = 100000
  	// 协程数
  	goroutineNum = 10
  	// 存放数据
  	intChan = make(chan int, 100)
  	// 存放结果
  	resChan = make(chan int, 100)
  	// 结束判断
  	exitChan = make(chan bool, goroutineNum)
  	// 计数
  	i = 0
  )
  
  // 写入数据
  func putNum() {
  	for i := 1; i <= maxRange; i++ {
  		intChan <- i
  	}
  	close(intChan)
  }
  
  // 计算素数
  func calcPrime() {
  	for {
  		num, ok := <-intChan
  		if ok {
  			if num != 1 && num != 0 {
  				flag := true
  				for i := 2; i*i <= num; i++ {
  					if num%i == 0 {
  						flag = false
  						break
  					}
  				}
  				if flag {
  					resChan <- num
  				}
  			}
  		} else {
  			// 这里还不能退出resChan管道
  			exitChan <- true
  			break
  		}
  	}
  
  }
  
  // 打印结果
  func printPrime() {
  	for {
  		num, ok := <-resChan
  		if ok {
  			fmt.Printf("i = %v, num = %v\n", i, num)
  			i++
  		} else {
  			break
  		}
  	}
  }
  
  func main() {
  	startTime := time.Now()
  	go putNum()
  	for i := 0; i < goroutineNum; i++ {
  		go calcPrime()
  	}
  	go func() {
  		for i := 0; i < goroutineNum; i++ {
  			<-exitChan
  		}
  		close(resChan)
  	}()
  	printPrime()
  	endTime := time.Since(startTime).Milliseconds()
  	fmt.Printf("运行花费时间: %v毫秒\n", endTime)
  }
  ```

- 管道可以声明为只读或只写

  ```go
  package main
  
  import (
  	"fmt"
  )
  
  // 管道
  func main() {
  	// 在默认情况下, 管道是双向的
  	// var chan1 chan int
  	// 声明为只写
  	var chan2 = make(chan<- int)
  	chan2 <- 20
  	// num1 := <-chan2 错误
  	fmt.Printf("chan2 = %v\n", chan2)
  	// 声明为只读
  	var chan3 <-chan int
  	num2 := <-chan3
  	// chan3 <- 30 报错
  	fmt.Printf("num2 = %v\n", num2)
  }
  ```

#### 3、select

- 使用`select`可以解决从管道获取数据阻塞的问题

  ```go
  package main
  
  import (
  	"fmt"
  	"time"
  )
  
  // 管道
  func main() {
  	intChan := make(chan int, 10)
  	for i := 0; i < 10; i++ {
  		intChan <- i
  	}
  
  	strChan := make(chan string, 5)
  	for i := 0; i < 5; i++ {
  		strChan <- "hello" + fmt.Sprintf("%d", i)
  	}
  	// 传统的方法在遍历管道时, 如果不关闭会阻塞而导致deadlock
  	// 在实际开发中, 有可能无法确认什么时候关闭管道
  	// 可以使用select方法来解决
  label:
  	for {
  		select {
  		// 这里如果管道一直没有关闭, 不会一直阻塞
  		// 会自动到下一个case匹配
  		case v := <-intChan:
  			fmt.Printf("从intChan读取的数据%v\n", v)
  			time.Sleep(time.Millisecond * 500)
  		case v := <-strChan:
  			fmt.Printf("从strChan读取的数据%v\n", v)
  			time.Sleep(time.Millisecond * 500)
  		default:
  			fmt.Println("未取到数据")
  			break label // 推荐使用return
  		}
  	}
  }
  ```

- 协程里的异常必须处理，否则会影响主线程的运行

  ```go
  package main
  
  import (
  	"fmt"
  	"time"
  )
  
  func test1() {
  	for i := 0; i < 10; i++ {
  		fmt.Println("hello word")
  		time.Sleep(time.Second)
  	}
  
  }
  
  func test2() {
  	// 使用defer+recover来处理异常
  	// 协程的异常如果没有处理会影响主线程的运行
  	defer func() {
  		// 捕获本方法抛出的panic
  		if err := recover(); err != nil {
  			fmt.Println("test2() 发生错误", err)
  		}
  	}()
  	var myMap map[int]string
  	myMap[0] = "go"
  }
  
  // 管道
  func main() {
  	go test1()
  	go test2()
  	for i := 0; i < 10; i++ {
  		fmt.Println("main() ok =", i)
  		time.Sleep(time.Second)
  	}
  }
  ```

### 九、反射

- 变量、`interface{}`和`reflect.Value`是可以相互转换的。

  ```go
  package main
  
  import (
  	"fmt"
  	"reflect"
  )
  
  func reflectTest01(b interface{}) {
  	// 通过反射获取传入的变量的type, kind, 值
  	// 先获取到reflect.Type类型
  	rType := reflect.TypeOf(b)
  	fmt.Println("rType =", rType)
  	// 获取reflect.Value类型
  	rVal := reflect.ValueOf(b)
  	fmt.Printf("rVal = %v, type = %T\n", rVal, rVal)
  	// 将rVal转为interface{}
  	iV := rVal.Interface()
  	// 使用类型断言转成合适的类型
  	num := iV.(int)
  	fmt.Println("num =", num)
  }
  
  type Student struct {
  	Name string
  	Age  int
  }
  
  func reflectTest02(b interface{}) {
  	// 先获取到reflect.Type类型
  	rType := reflect.TypeOf(b)
  	fmt.Println("rType =", rType)
  	// 获取reflect.Value类型
  	rVal := reflect.ValueOf(b)
  	// 获取Kind, 也可以通过rType.Kind()获取
  	rKind := rVal.Kind()
  	fmt.Printf("rKind = %v\n", rKind)
  	// 将rVal转为interface{}
  	iV := rVal.Interface()
  	fmt.Printf("iV = %v, iV type = %T\n", iV, iV)
  	stu, ok := iV.(Student)
  	if ok {
  		fmt.Printf("Name = %v, Age = %v\n", stu.Name, stu.Age)
  	}
  }
  
  func main() {
  	num := 10
  	reflectTest01(num)
  	stu := Student{"张三", 19}
  	reflectTest02(stu)
  }
  /*
  rType = int
  rVal = 10, type = reflect.Value
  num = 10
  rType = main.Student
  rKind = struct
  iV = {张三 19}, iV type = main.Student
  Name = 张三, Age = 19
  */
  ```

- `Type`是类型，`Kind`是类别，它们可能相同，也可能不同。例如`var num int = 10`，`Type`和`Kind`都是`int`，`var stu Student`的`Type`是`xxx(包名).Student`,`Kind`是`struct`。

- 通过反射修改值：

  ```go
  package main
  
  import (
  	"fmt"
  	"reflect"
  )
  
  func reflect01(b interface{}){
  	rVal := reflect.ValueOf(b)
  	fmt.Printf("rVal kind = %v\n", rVal.Kind())
  	// Elem()返回v持有的接口保管的值的value封装,
  	// 或者v持有的指针指向的值的value封装
  	rVal.Elem().SetInt(20)
  }
  
  func main() {
  	num := 1
  	reflect01(&num)
  	fmt.Printf("num = %v\n", num)
  }
  /*
  rVal kind = ptr
  num = 20
  */
  ```

- 使用反射动态修改和创建结构体示例：

  ```go
  package main
  
  import (
  	"fmt"
  	"reflect"
  )
  
  type Student struct {
  	Name    string `json:"name"`
  	Age     int    `json:"age"`
  	Address string
  	Sex     string
  }
  
  func (stu Student) Print() {
  	fmt.Println("start----->")
  	fmt.Println(stu)
  	fmt.Println(" end ----->")
  }
  
  func (stu Student) GetSum(n1, n2 int) int {
  	return n1 + n2
  }
  
  func (stu *Student) Set(name string, age int, address string, sex string) {
  	stu.Name = name
  	stu.Age = age
  	stu.Address = address
  	stu.Sex = sex
  }
  
  func TestStruct(a interface{}) {
  	rType := reflect.TypeOf(a)
  	rValue := reflect.ValueOf(a)
  	// 传入的是一个指针, val现在是一个struct
  	typ := rType.Elem()
  	val := rValue.Elem()
  	fmt.Printf("rValue = %v\n", rValue.Kind().String())
  	fmt.Printf("val = %v\n", val.Kind().String())
  	fmt.Printf("rType = %v\n", rType.Kind().String())
  	fmt.Printf("typ = %v\n", typ.Kind().String())
  	if val.Kind() != reflect.Struct {
  		fmt.Println("不是struct类型。")
  		return
  	}
  	// 获取字段数
  	numField := val.NumField()
  	fmt.Printf("numField = %v\n", numField)
  	// 使用反射修改字段的值
  	val.FieldByName("Address").SetString("广州")
  
  	// 遍历字段
  	for i := 0; i < numField; i++ {
  		fmt.Printf("Field %d: 值为 \"%v\"\n", i, val.Field(i))
  		// 获取标签(通过reflect.Type获取tag)
  		tagVal := typ.Field(i).Tag.Get("json")
  		if tagVal != "" {
  			fmt.Printf("Field %d, tag = %v\n", i, tagVal)
  		}
  	}
  
  	// 获取方法数
  	numMethod := val.NumMethod()
  	fmt.Printf("numMethod = %v\n", numMethod)
  	// 方法的排序按照方法名
  	val.Method(1).Call(nil)
  	// 声明一个切片
  	var params []reflect.Value
  	params = append(params, reflect.ValueOf(10))
  	params = append(params, reflect.ValueOf(20))
  	// 如果方法有参数, 要通过切片的方法传参
  	res := val.MethodByName("GetSum").Call(params)
  	// 返回的也是一个切片
  	fmt.Printf("res = %v\n", res[0].Int())
  
  	// elem是一个新的*Student类型的指针
  	elem := reflect.New(typ)
  	fmt.Printf("elem = %v\n", elem.Kind().String())
  	fmt.Printf("elem.Elem() = %v\n", elem.Elem().Kind().String())
  	// 可以使用反射创建结构体
  	// model是*Student, 它的指向和elem是一样的
  	model := elem.Interface().(*Student)
  	// 这一步后, elem是表示Student真正的地址空间
  	// model依旧表示指向那部分空间的地址
  	elem = elem.Elem()
  	elem.FieldByName("Name").SetString("王五")
  	elem.FieldByName("Age").SetInt(20)
  	elem.FieldByName("Address").SetString("武汉")
  	elem.FieldByName("Sex").SetString("女")
  	// 使用model直接操作也是可以的
  	model.Sex = "男"
  	fmt.Printf("model = %v, %T\n", model, model)
  	fmt.Printf("elem = %v, %T\n", elem, elem)
  
  }
  
  func main() {
  	stu := Student{"张三", 18, "北京", "男"}
  	TestStruct(&stu)
  	stu.Set("李四", 19, "上海", "男")
  	stu.Print()
  }
  /*
  rValue = ptr
  val = struct
  rType = ptr
  typ = struct
  numField = 4
  Field 0: 值为 "张三"
  Field 0, tag = name
  Field 1: 值为 "18"
  Field 1, tag = age
  Field 2: 值为 "广州"
  Field 3: 值为 "男"
  numMethod = 2
  start----->
  {张三 18 广州 男}
   end ----->
  res = 30
  elem = ptr
  elem.Elem() = struct
  model = &{王五 20 武汉 男}, *main.Student
  elem = {王五 20 武汉 男}, reflect.Value
  start----->
  {李四 19 上海 男}
   end ----->
  */
  ```

### 十、网络编程

- ```go
  // server.go
  package main
  
  import (
  	"fmt"
  	"io"
  	"net"
  	"strings"
  )
  
  var (
  	protocol = "tcp"
  	address  = "0.0.0.0:9000"
  )
  
  func process(conn net.Conn) {
  	fmt.Println("接收到一个请求。")
  	defer conn.Close()
  	fmt.Printf("客户端地址 = %v\n", conn.RemoteAddr().String())
  	// 创建一个新的切片接收数据
  	// 这里如果设置的值比较小, 一次接收不了全部数据会分次接收
  	buf := make([]byte, 1024)
  	dataLen := 0
  	data := strings.Builder{}
  	fmt.Println("等待客户端的输入……")
  	// 此处会阻塞, 等待客户端发送数据
  	for {
  		n, err := conn.Read(buf)
  		if err == io.EOF {
  			fmt.Println("客户端输入完毕")
  			break
  		} else if err != nil {
  			fmt.Printf("接收数据失败, %v\n", err)
  			return
  		} else {
  			dataLen += n
  			data.WriteString(string(buf))
  		}
  	}
  	fmt.Printf("接收的数据大小: %v, buf = %v\n", dataLen, data.String())
  
  }
  
  func main() {
  	// 第一个参数表示使用的网络协议是tcp
  	// 第二个参数表示监听的地址和端口
  	srv, err := net.Listen(protocol, address)
  	if err != nil {
  		fmt.Printf("服务启动失败, %v\n", err)
  		return
  	}
  	fmt.Printf("开始监听: %v, %v……\n", protocol, address)
  	defer srv.Close() // 延时关闭
  	// 等待客户端连接
  	for {
  		fmt.Println("开始接收连接……")
  		conn, err := srv.Accept()
  		if err != nil {
  			fmt.Printf("获取连接失败, %v\n", err)
  			continue
  		}
  		// 启动协程处理
  		go process(conn)
  	}
  }
  ```

- ```go
  // client.go
  package main
  
  import (
  	"bufio"
  	"fmt"
  	"net"
  	"os"
  )
  
  var (
  	protocol = "tcp"
  	address  = "127.0.0.1:9000"
  )
  
  func main() {
  	conn, err := net.Dial(protocol, address)
  	if err != nil {
  		fmt.Printf("连接服务器失败!%v\n", err)
  		return
  	}
  	defer conn.Close()
  	fmt.Printf("服务器地址 = %v\n", conn.RemoteAddr().String())
  	// 发送数据, os.Stdin代表标准输入[终端]
  	reader := bufio.NewReader(os.Stdin)
  	// 从终端读取一行用户输入, 并准备发送给服务器
  	line, err := reader.ReadString('\n')
  	if err != nil {
  		fmt.Printf("读取失败, %v\n", err)
  		return
  	}
  	// 发送数据
  	n, err := conn.Write([]byte(line))
  	if err != nil {
  		fmt.Printf("发送失败, %v\n", err)
  	}
  	fmt.Printf("发送字节数n = %v\n", n)
  }
  ```

- 加强版，可反复发送

  ```go
  // server.go
  package main
  
  import (
  	"fmt"
  	"io"
  	"net"
  )
  
  var (
  	protocol = "tcp"
  	address  = "0.0.0.0:9000"
  )
  
  func process(conn net.Conn) {
  	fmt.Println("接收到一个请求。")
  	fmt.Printf("客户端地址 = %v\n", conn.RemoteAddr().String())
  	// 创建一个新的切片接收数据
  	// 这里如果设置的值比较小, 一次接收不了全部数据会分次接收
  	buf := make([]byte, 1024)
  	fmt.Println("等待客户端的输入……")
  	// 此处会阻塞, 等待客户端发送数据
  	for {
  		n, err := conn.Read(buf)
  		if err == io.EOF {
  			fmt.Println("客户端退出")
  			break
  		} else if err != nil {
  			fmt.Printf("接收数据失败, %v\n", err)
  			return
  		} else {
  			fmt.Printf("%v", string(buf[:n]))
  		}
  
  	}
  
  }
  
  func main() {
  	// 第一个参数表示使用的网络协议是tcp
  	// 第二个参数表示监听的地址和端口
  	srv, err := net.Listen(protocol, address)
  	if err != nil {
  		fmt.Printf("服务启动失败, %v\n", err)
  		return
  	}
  	fmt.Printf("开始监听: %v, %v……\n", protocol, address)
  	defer srv.Close() // 延时关闭
  	// 等待客户端连接
  	for {
  		fmt.Println("开始接收连接……")
  		conn, err := srv.Accept()
  		if err != nil {
  			fmt.Printf("获取连接失败, %v\n", err)
  			continue
  		}
  		// 启动协程处理
  		go process(conn)
  	}
  }
  ```

  ```go
  // client.go
  package main
  
  import (
  	"bufio"
  	"fmt"
  	"net"
  	"os"
  )
  
  var (
  	protocol = "tcp"
  	address  = "127.0.0.1:9000"
  )
  
  func main() {
  	conn, err := net.Dial(protocol, address)
  	if err != nil {
  		fmt.Printf("连接服务器失败!%v\n", err)
  		return
  	}
  	fmt.Printf("服务器地址 = %v\n", conn.RemoteAddr().String())
  	// 发送数据, os.Stdin代表标准输入[终端]
  	reader := bufio.NewReader(os.Stdin)
  	for {
  		// 从终端读取一行用户输入, 并准备发送给服务器
  		line, err := reader.ReadString('\n')
  		if err != nil {
  			fmt.Printf("读取失败, %v\n", err)
  			return
  		}
  		if line == "exit\r\n" {
  			conn.Close()
  			break
  		}
  		// 发送数据
  		_, err = conn.Write([]byte(line))
  		if err != nil {
  			fmt.Printf("发送失败, %v\n", err)
  		}
  	}
  }
  ```

#### `redis`

- http://redisdoc.com/
- 安装开源`redis`库，`go get github.com/go-redis/redis`

```go
package main

import (
	"context"
	"fmt"
	"github.com/go-redis/redis"
)

var (
	ctx         = context.Background()
	redisClient *redis.Client
)

const (
	ip       = "127.0.0.1"
	port     = 6379
	pass     = ""
	db       = 0
	poolSize = 100
)

func init() {
	redisAddr := fmt.Sprintf("%s:%d", ip, port)
	fmt.Printf("连接redis服务器: %v\n", redisAddr)
	redisClient = redis.NewClient(&redis.Options{
		Addr:     redisAddr, // ip:port
		Password: pass,      // 密码
		DB:       db,        // 连接的数据库
		PoolSize: poolSize,  // 连接池大小
	})
	res, err := redisClient.Ping(ctx).Result()
	if err != nil {
		fmt.Println("连接失败!")
		panic(err)
	} else {
		fmt.Println("连接成功!", res)
	}
}

func main() {
	conn := redisClient.Conn()
	defer func() {
		conn.Close()
		err := recover()
		if err != nil {
			fmt.Println(err)
		}
	}()
	conn.HSet(ctx, "person", "name", "张三")
	name, _ := conn.HGet(ctx, "person", "name").Result()
	fmt.Printf("name = %v\n", name)
	conn.HMSet(ctx, "person", "age", 18, "address", "武汉")
	result, _ := conn.HMGet(ctx, "person", "age", "address").Result()
	fmt.Printf("age = %v, address = %v\n", result[0], result[1])
}
```

### 十一、数据结构和算法

#### 1、稀疏数组

```go
package main

type ValNode struct {
	row int
	col int
	val int // 1是黑, 2是白
}

// 稀疏数组
func main() {

	var chessMap [6][6]int
	chessMap[1][2] = 1
	chessMap[2][3] = 2

	var sparseArr []ValNode
	// 保存大小和有效数据个数
	sparseArr = append(sparseArr, ValNode{6, 6, 2})
	// 压缩数据
	for i, rows := range chessMap {
		for j, val := range rows {
			if val != 0 {
				valNode := ValNode{i, j, val}
				sparseArr = append(sparseArr, valNode)
			}
		}
	}
	// 保存
	// 恢复
}
```

#### 2、队列

##### ①数组实现

```go
package main

import (
	"errors"
	"fmt"
)

type Queue struct {
	maxSize int
	array   []int
	isFull  bool // 是否为空队列
	len     int  //当前大小
	head    int  //队头, 初始为0
	tail    int  // 队尾, 初始为1
}

func GetQueue(maxSize int) *Queue {
	return &Queue{
		maxSize: maxSize,
		array:   make([]int, maxSize),
		isFull:  false,
		len:     0,
		head:    0,
		tail:    0,
	}
}

// 添加
func (q *Queue) push(val int) (bool, error) {
	// 先判断队列是否满了
	if q.isFull {
		fmt.Println("队列已满")
		return false, errors.New("队列已满")
	} else {
		q.array[q.tail] = val
		q.tail = (q.tail + 1) % q.maxSize
		q.len++
		if q.len >= q.maxSize {
			q.isFull = true
		}
		return true, nil
	}
}

// 弹出队列
func (q *Queue) pop() (int, error) {
	if q.len == 0 {
		fmt.Println("队列已空")
		return 0, errors.New("队列已空")
	} else {
		val := q.array[q.head]
		fmt.Printf("弹出队列: array[%d] = %v\n", q.head, val)
		q.head = (q.head + 1) % q.maxSize
		q.len--
		q.isFull = false
		return val, nil
	}
}

// 打印出队列中的所有元素
func (q *Queue) showAll() {
	fmt.Println("show all element:")
	if q.len == 0 {
		fmt.Println("队列已空")
	} else {
		index := q.head
		for i := 0; i < q.len; i++ {
			fmt.Printf("array[%v] = %v\n", index, q.array[index])
			index = (index + 1) % q.maxSize
		}
	}
}

// 队列的数组实现
func main() {
	queue := GetQueue(5)
	queue.push(1)
	queue.push(2)
	queue.push(3)
	queue.push(4)
	queue.push(5)
	queue.pop()
	queue.push(6)
	queue.pop()
	queue.showAll()
	queue.pop()
	queue.pop()
	queue.showAll()
}
```

##### ②单链表实现

```go
package main

import (
	"fmt"
)

type Node struct {
	data int
	next *Node // 指向下一个节点
}

// 在单链表的最后加入(尾插法)
func (head *Node) lastPush(newNode *Node) {
	// head.next就是第一个数据
	temp := head
	for temp.next != nil {
		temp = temp.next
	}
	temp.next = newNode
}

// 在单链表的开头加入(头插法)
func (head *Node) firstPush(newNode *Node) {
	temp := head.next
	head.next = newNode
	newNode.next = temp
}

// 在单链表尾部弹出
func (head *Node) lastPop() *Node {
	temp := head
	if temp.next == nil {
		fmt.Println("链表为空。")
		return nil
	}
	for temp.next.next != nil {
		temp = temp.next
	}
	res := temp.next
	fmt.Printf("链表最后一个节点: %v\n", res)
	temp.next = nil
	return res
}

// 在单链表头部弹出
func (head *Node) firstPop() *Node {
	temp := head
	if temp.next == nil {
		fmt.Println("链表为空。")
		return nil
	}
	delNode := temp.next
	fmt.Printf("链表第一个节点: %v\n", delNode)
	head.next = temp.next.next
	return delNode
}

// 显示链表所有节点数据
func (head *Node) getAllNodes() []Node {
	temp := head
	i := 0
	var nodeList []Node
	for temp.next != nil {
		temp = temp.next
		fmt.Printf("[%v]%v\n", i, temp.data)
		nodeList = append(nodeList, *temp)
		i++
	}
	return nodeList
}

func main() {
	// 先创建一个头结点, 头结点不保存数据
	head := &Node{}
	// 创建子节点
	node1 := &Node{
		data: 100,
	}
	node2 := &Node{
		data: 200,
	}
	node3 := &Node{
		data: 300,
	}
	head.lastPush(node2)
	head.lastPush(node1)
	head.firstPush(node3)
	head.getAllNodes()
	head.lastPop()
	head.getAllNodes()
	head.firstPop()
	head.getAllNodes()
}
```



