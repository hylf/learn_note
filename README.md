# 一、标题

+ 语法：#加空格

# 一级标题

## 二级标题

### 三级标题

#### 四级标题

##### 五级标题

###### 六级标题

# 二、字体

这是普通文字

+ 语法：\**需要加粗的文字**

**这是加粗的文字**

+ 语法：\*需要斜体的文字*

*这是斜体文字*

+ 语法：\*\*\*需要斜体加粗的文字***，或者：\_\_\_这是需要斜体加粗的文字\_\_\_

***这是斜体加粗的文字***

+ 语法：\~~需要加删除线的文字~~

~~这是加了删除线的文字~~

# 三、引用

+ 语法：\>引用的内容，必须放在开头，否则不生效

> 这是引用的内容
> 
> > 这是引用的内容
> > 
> > > 嵌套引用

# 四、分割线

---

----

***

*****

(---，----，***，***\**效果相同)

# 五、 图片

+ 语法：\!\[图片alt]\(图片uri，''图片title'')

<img title="HPE" src="https://img0.baidu.com/it/u=3350721434,829805131&fm=253&fmt=auto&app=138&f=JPEG?w=499&h=208" alt="HPE" data-align="inline">

# 六、超链接

+ 语法：\[链接名]\(超链接地址 "title")，直接使用HTML里的\<a>标签也可以。

[百度](https://www.baidu.com "百度")，[GitHub](https://www.github.com "github")

<a href="https://www.baidu.com" target="_blank">百度</a>

# 七、列表

+ 无序列表语法：-，+，*三种都可以，后面跟一个空格，例如本行。
1. 有序列表语法：数字加英文的.
+ 上下级嵌套，一个<kbd>tab</kbd>键
  
  1. 1313131
     
     + 21313
  
  2. 3131313
     
     + eee
       
       + ee1e

+ 待办清单语法：- [x]，必须放在开头，例如：

+ [x] 已完成的项目1
  
  - [x] 已完成
  
  - [ ] 未完成
- [x] 已完成的项目2
  
  - [ ] 未完成
  
  - [x] 已完成

# 八、表格

+ 语法：

```
| 左对齐 | 右对齐 | 居中对齐 |
| :-----| ----: | :----: |
| 单元格 | 单元格 | 单元格 |
| 单元格 | 单元格 | 单元格 |

第二行分割表头和内容。
- 有一个就行，为了对齐，多加了几个，文字默认居左
-两边加：表示文字居中
-右边加：表示文字居右
注：原生的语法两边都要用 | 包起来。
```

| 左对齐 | 右对齐 | 居中对齐 |
|:--- | ---:|:----:|
| 单元格 | 单元格 | 单元格  |
| 单元格 | 单元格 | 单元格  |

# 九、代码

+ 单行代码语法：\`内容`

`内容121313ss`

+ 代码块：
  
  ````python
  ```
     代码...
     代码...
     代码...
  ```
  ````
  
  ```java
  public class Test(){
    public static void main(String[] args){
        System.out.println("Hello Word!");
    }
  }
  ```

# 十、高级技巧

1. 支持的HTML元素有：\<kbd>，\<b>，\<i>，\<em>，\<sup>，\<sub>，\<br>等。
   
   + \<kbd> 标签定义键盘文本样式。例如：保存文件请使用<kbd>Ctrl</kbd>+<kbd>S</kbd>。
   
   + \<b>标签定义粗体的文本。例如：<b>粗体</b>
   
   + \<i>标签定义斜体文本。例如：<i>斜体</i>
   
   + \<em>标签呈现为被强调的文本。例如：<em>强调</em>
   
   + \<sup>标签定义上标文本。例如2<sup>10</sup>
   
   + \<sub>标签定义下标。例如：A<sub>n</sub>
   
   + \<br>标签定义一个换行。例如在该标签后面输入的文字都会出现在下一行<br>换行后的文字。

2. 转义字符为反斜杠\。

3. 公式：
   
   + 单行公式：\$内容\$，例如：$f(x)=sin(x)+cos(x)$
   
   + 多行公式：\$\$内容\$\$，必须换行，例如：
     
     $$
     \sum_{n=1}^{100} n
     $$
     
     $$
     \begin{Bmatrix}
     a & b \\
     c & d
     \end{Bmatrix}
     $$
     
     $$
     \begin{CD}
     A @>a>> B \\
     @VbVV @AAcA \\
     C @= D
     \end{CD}
     $$

4. 时序图：
+ 横向流程图源码格式：

```mermaid
graph LR
A[方形] -->B(圆角)
    B --> C{条件a}
    C -->|a=1| D[结果1]
    C -->|a=2| E[结果2]
    F[横向流程图] 
```

---

+ 竖向流程图源码格式：

```mermaid
graph TD
A[方形] --> B(圆角)
    B --> C{条件a}
    C --> |a=1| D[结果1]
    C --> |a=2| E[结果2]
    F[竖向流程图]
```

---

+ 标准流程图源码格式：

```flow
st=>start: 开始框
op=>operation: 处理框
cond=>condition: 判断框(是或否?)
sub1=>subroutine: 子流程
io=>inputoutput: 输入输出框
e=>end: 结束框
st->op->cond
cond(yes)->io->e
cond(no)->sub1(right)->op
```

---

+ 标准流程图源码格式（横向）：

```flow
st=>start: 开始框
op=>operation: 处理框
cond=>condition: 判断框(是或否?)
sub1=>subroutine: 子流程
io=>inputoutput: 输入输出框
e=>end: 结束框
st(right)->op(right)->cond
cond(yes)->io(bottom)->e
cond(no)->sub1(right)->op
```

---

+ UML时序图源码样例：

```sequence
对象A->对象B: 对象B你好吗?（请求）
Note right of 对象B: 对象B的描述
Note left of 对象A: 对象A的描述(提示)
对象B-->对象A: 我很好(响应)
对象A->对象B: 你真的好吗？
```

---

+ UML时序图源码复杂样例：

```sequence
Title: 标题：复杂使用
对象A->对象B: 对象B你好吗?（请求）
Note right of 对象B: 对象B的描述
Note left of 对象A: 对象A的描述(提示)
对象B-->对象A: 我很好(响应)
对象B->小三: 你好吗
小三-->>对象A: 对象B找我了
对象A->对象B: 你真的好吗？
Note over 小三,对象B: 我们是朋友
participant C
Note right of C: 没人陪我玩
```

---

+ UML标准时序图样例：

```mermaid
%% 时序图例子,-> 直线，-->虚线，->>实线箭头
  sequenceDiagram
    participant 张三
    participant 李四
    张三->王五: 王五你好吗？
    loop 健康检查
        王五->王五: 与疾病战斗
    end
    Note right of 王五: 合理 食物 <br/>看医生...
    李四-->>张三: 很好!
    王五->李四: 你怎么样?
    李四-->王五: 很好!
```

---

+ 甘特图样例：

```mermaid
gantt
        dateFormat  YYYY-MM-DD
        title 软件开发甘特图
        section 设计
        需求                      :done,    des1, 2014-01-06,2014-01-08
        原型                      :active,  des2, 2014-01-09, 3d
        UI设计                     :         des3, after des2, 5d
    未来任务                     :         des4, after des3, 5d
        section 开发
        学习准备理解需求                      :crit, done, 2014-01-06,24h
        设计框架                             :crit, done, after des2, 2d
        开发                                 :crit, active, 3d
        未来任务                              :crit, 5d
        耍                                   :2d
        section 测试
        功能测试                              :active, a1, after des3, 3d
        压力测试                               :after a1  , 20h
        测试报告                               : 48h                         : 48h
```

---
