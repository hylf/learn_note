

### 一、基础

- 官方文档：`https://github.com/AlmasB/FXGL.wiki.git`

#### 1、`maven`导入

- `FXGL`版本和`Java`版本最好一一对应，最低`JDK11`。

```xml
<dependency>
    <groupId>com.github.almasb</groupId>
    <artifactId>fxgl</artifactId>
    <version>21</version>
</dependency>
```

#### 2，游戏入口

- 需要继承`GameApplication`类，实现以下主要方法：

##### ①`initSettings`初始化应用设置

```java
@Override
protected void initSettings(GameSettings gameSettings) {
    // 标题和版本会同时显示在窗口的标题栏上
    gameSettings.setTitle("坦克大战");
    gameSettings.setVersion("0.0.1");
    // gameSettings.setHeight(400);
    // gameSettings.setWidth(600);
    // assets 是所有资源的父目录
    // textures 是专门用于存储图片的目录
    // 获取改目录下图片可以省略这两个路径
    gameSettings.setAppIcon("tank.png");
    gameWindowHeight = gameSettings.getHeight();
    gameWindowWidth = gameSettings.getWidth();
}
```

##### ②`initGameVars`初始化游戏参数

```java
@Override
protected void initGameVars(Map<String, Object> vars) {
    // 这里可以初始化游戏参数, 初始化值必须和实际类型相同, 不能给null
    // int
    vars.put("score", 0);
    // FXGL.geti("score");
    // FXGL.getip("score");
    // double
    vars.put("x", 0.0);
    // FXGL.getd("x");
    // FXGL.getdp("x");
    // Object
    vars.put("list", new ArrayList<>());
    // FXGL.geto("list");
    // FXGL.getop("list");
    // String
    vars.put("name", "");
    // FXGL.gets("name");
    // FXGL.getsp("name");
    // 其他类似
}
```

##### ③`initGame`初始化游戏世界

```java
@Override
protected void initGame() {
    shootTimer = FXGL.newLocalTimer();
    Canvas canvas = new Canvas(100, 100);
    GraphicsContext gc2D = canvas.getGraphicsContext2D();

    // 画出这个坦克
    gc2D.setFill(Color.web("#ffec03"));
    gc2D.fillRect(0, 0, 80, 30);

    gc2D.setFill(Color.web("#cebc17"));
    gc2D.fillRect(15, 30, 50, 40);

    gc2D.setFill(Color.web("#ffec03"));
    gc2D.fillRect(0, 70, 80, 30);

    gc2D.setFill(Color.web("#f9ee8a"));
    gc2D.fillRect(40, 40, 60, 20);

    // 游戏里面的对象Entity类似于Java Object
    // 创建坦克对象
    tankEntity = FXGL.entityBuilder()
        // view 决定的是游戏实体的外观
        // bbox决定游戏实体的真实大小
        // viewWithBBox相当于是.view(canvas).bbox(BoundingShape.box(100,100))
        // 的简写
        .viewWithBBox(canvas).view(new Text("玩家: huayunliufeng")).build();
    // 设置旋转中心点
    tankEntity.setRotationOrigin(new Point2D(50, 50));
    // 添加到游戏世界
    FXGL.getGameWorld().addEntity(tankEntity);

    createEnemy();

    FXGL.getip("score").addListener((observable, oldValue, newValue) -> {
        if (newValue.intValue() >= 3) {
            FXGL.getNotificationService().pushNotification("哇，你好棒！");
        }
    });
}
```

##### ④`initInput`初始化用户输入

```java
@Override
protected void initInput() {
    // 获取用户输入
    FXGL.getInput().addAction(new UserAction("Move Up") {
        @Override
        protected void onAction() {
            tankEntity.setRotation(-90);
            if (isMoving || tankEntity.getY() <= 0) {
                return;
            }
            dir = Dir.UP;
            isMoving = true;
            tankEntity.translateY(-speed);
        }
    }, KeyCode.UP);

    FXGL.getInput().addAction(new UserAction("Move Down") {
        @Override
        protected void onAction() {
            tankEntity.setRotation(90);
            if (isMoving || tankEntity.getY() + tankEntity.getHeight() >= gameWindowHeight) {
                return;
            }
            dir = Dir.DOWN;
            isMoving = true;
            tankEntity.translateY(speed);
        }
    }, KeyCode.DOWN);

    FXGL.getInput().addAction(new UserAction("Move Left") {
        @Override
        protected void onAction() {
            tankEntity.setRotation(180);
            if (isMoving || tankEntity.getX() <= 0) {
                return;
            }
            dir = Dir.LEFT;
            isMoving = true;
            tankEntity.translateX(-speed);
        }
    }, KeyCode.LEFT);

    FXGL.getInput().addAction(new UserAction("Move Right") {
        @Override
        protected void onAction() {
            tankEntity.setRotation(0);
            if (isMoving || tankEntity.getX() + tankEntity.getWidth() >= gameWindowWidth) {
                return;
            }
            dir = Dir.RIGHT;
            isMoving = true;
            tankEntity.translateX(speed);
        }
    }, KeyCode.RIGHT);

    FXGL.getInput().addAction(new UserAction("shoot") {
        @Override
        protected void onAction() {
            // 判断时间差有没有超过0.25秒
            if (!shootTimer.elapsed(shootDelay)) {
                return;
            }
            FXGL.play("shoot.wav");
            // 如果有, 重新计算
            shootTimer.capture();
            // 创建子弹实体并添加到游戏世界中
            Point2D point2D = CommonUtils.getPoint2D(dir);
            Entity build = FXGL.entityBuilder().type(GameType.BULLET)
                // 放到中间位置生成
                .at(tankEntity.getCenter().getX() - 10, tankEntity.getCenter().getY() - 10)
                // 以面向右的方向和1.0的速度构建组件。
                .with(new ProjectileComponent(point2D, 600)).viewWithBBox(new Rectangle(20, 20))
                // 超出屏幕会移除该组件
                .with(new OffscreenCleanComponent())
                // 可碰撞属性
                .collidable().buildAndAttach();

        }
    }, KeyCode.SPACE);
}
```

##### ⑤`initUI`初始化UI

```java
@Override
protected void initUI() {
    // 将分数组件添加到UI中
    // 写法1
    // Text text = FXGL.addVarText("score", 20, 50);
    // text.setFill(Color.BLUE);
    // text.fontProperty().unbind();
    // text.setFont(Font.font(35));
    // 写法2
    Text text = FXGL.getUIFactoryService().newText(FXGL.getip("score").asString("score:%d"));
    text.setLayoutX(30);
    text.setLayoutY(30);
    text.setFill(Color.BLUE);
    FXGL.addUINode(text);
}
```

##### ⑥`initPhysics`初始化物理属性（碰撞等）

```java
@Override
protected void initPhysics() {
    // 类型的顺序决定了回调中实体的顺序。
    FXGL.getPhysicsWorld().addCollisionHandler(new CollisionHandler(GameType.BULLET, GameType.ENEMY) {
        @Override
        protected void onCollisionBegin(Entity bullet, Entity enemy) {
            // 写法1
            // int score = FXGL.geti("score") + 1;
            // FXGL.set("score", score);
            // 写法2
            FXGL.inc("score", 1);
            bullet.removeFromWorld();
            Point2D center = enemy.getCenter();
            enemy.removeFromWorld();
            // 简单的爆炸效果
            Duration duration = Duration.seconds(0.5);
            Circle circle = new Circle(10, Color.RED);
            // 需要把效果添加到游戏世界中
            Entity entity = FXGL.entityBuilder().at(center).view(circle)
                // 超过指定时间就移除组件
                .with(new ExpireCleanComponent(duration)).buildAndAttach();
            // 缩放10倍
            ScaleTransition st = new ScaleTransition(duration, circle);
            st.setToX(10);
            st.setToY(10);
            // 变透明
            FadeTransition ft = new FadeTransition(duration, circle);
            ft.setToValue(0);
            // 并行动画
            ParallelTransition pt = new ParallelTransition(st, ft);
            // 可手动设置播放完毕后移除
            // pt.setOnFinished(e -> entity.removeFromWorld());
            pt.play();
            createEnemy();
        }
    });
}
```

##### ⑦`onUpdate`每一帧都会调用

```java
@Override
protected void onUpdate(double tpf) {
    // 游戏每一帧都会调用, tpf是每一帧消耗的时间
    // System.out.println(FXGL.getGameWorld().getEntities().size());
    isMoving = false;
}
```

##### ⑧`onPreInit`预先加载一些资源

- 在应用程序的每个生命周期中，在`initGame()`之前调用一次。

```java
@Override
protected void onPreInit() {
    // 预先加载一些资源
    // 设置游戏的初始化音量
    FXGL.getSettings().setGlobalMusicVolume(0.3);
    FXGL.getSettings().setGlobalSoundVolume(0.8);
    FXGL.loopBGM("bg.mp3");
}
```

#### 3、常用方法

- 默认情况下，在游戏界面按下<kbd>Esc</kbd>键进入设置。

##### ①创建敌人

```java
private void createEnemy() {
    FXGL.entityBuilder()
        .type(GameType.ENEMY)
        .at(FXGLMath.random(60, 700 - 60), FXGLMath.random(60, 500 - 60))
        .viewWithBBox(new Rectangle(60, 60, Color.BLUE))
        // 相当于简写 .with(new CollidableComponent(true))
        .collidable()
        .buildAndAttach();
}
```

#### 4、`assets`目录说明

- 资源放置到`src/main/resources`中，下列目录放在该目录中

##### ①`music`

- 格式`mp3`

- 只要音频文件，例如背景音乐或录制的对话。 

##### ②`data`

- 任意自定义资源

##### ③`sounds`

- `wav`

##### ③`sounds`

- `wav`

- 任意短的音效

##### ④`textures`

- 格式`jpg`，`png`

- 放置图片，例如游戏背景图片，特效帧序列图片等

##### ⑤`ui`

- 放置界面相关资源，例如`.fxml`文件

##### ⑥`ui/fonts`

- 放置字体文件，支持`ttf`、`otf`

##### ⑦`ui/cursors`

- `jpg`，`png`

- 放置光标图像

##### ⑧`ui/css`

- `css`

- 放置`fx`的`css`文件

##### ⑨`properties`

- ` `properties``

- 放置配置信息

##### ⑩`dialogues`

- 放置对话图（聊天场景）文件

##### ⑪`text`

- `txt`

- 放置文本信息，最后会读取到一个`List`文件

##### ⑫`json`

- `json`
- 有效的`json`数据，例如铺瓷砖的地图，一些定义的格式。 

##### ⑬`tmx`

- `tmx`
- 平铺地图数据。 

##### ⑭`scripts`

- `js`
- 有效的脚本，可以运行，例如`AI`脚本或行为，动作代码段

##### ⑮`kv`

- `kv`
- 相似性，但该文件被分析为自己定制的数据结构，有助于实体配置。 

##### ⑯`ai`

- `tree`
- 这是一个标准的`gdxAI `[行为树 ](https://github.com/libgdx/gdx-ai/wiki/Behavior-Trees)。

##### ⑰`ui/icons`

- `jpg`，`png`
- 图标的图像被用于任务栏或窗口的标题。 

#### 5、打包游戏

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>3.10.0</version>
            <configuration>
                <release>21</release>
                <encoding>utf-8</encoding>
            </configuration>
        </plugin>

        <plugin>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-maven-plugin</artifactId>
            <version>0.0.8</version>
            <configuration>
                <stripDebug>true</stripDebug>
                <compress>2</compress>
                <noHeaderFiles>true</noHeaderFiles>
                <noManPages>true</noManPages>
                <launcher>tank</launcher>
                <jlinkImageName>tank</jlinkImageName>
                <jlinkZipName>tank</jlinkZipName>
                <mainClass>priv.zq.fxgltest.HelloWorldApp</mainClass>
            </configuration>
        </plugin>
    </plugins>
</build>
```

- 接着执行命令`mvn clean javafx:jlink`：

- 或者打包为`jar`，使用`java -jar xxx.jar`执行：

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-assembly-plugin</artifactId>
    <version>3.7.1</version>
    <configuration>
        <archive>
            <manifest>
            	<mainClass>priv.zq.fxgltest.HelloWorldApp</mainClass>
            </manifest>
        </archive>
        <descriptorRefs>
        	<descriptorRef>jar-with-dependencies</descriptorRef>
        </descriptorRefs>
    </configuration>
    <executions>
        <execution>
        	<phase>package</phase>
            <goals>
            	<goal>single</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

- `jpackage`：`jpackage.exe --input .\in -t exe -n fxglTest --main-jar fxglTest-1.0-SNAPSHOT-jar-with-dependencies.jar --main-class priv.zq.fxgltest.HelloWorldApp  --win-dir-chooser --win-shortcut --win-shortcut-prompt`
- 一定要注意`jar`文件最好单独放在一个目录中，使用`--input`指定该目录。安装后必须卸载才能再次安装。

#### 6、`GameApplication`方法执行顺序

```java
public static void main(String[] args) {
    System.out.println("TestMethodApp.main: " + Thread.currentThread().getName());
    launch(args);
}

/**
     * 游戏的预处理
     */
@Override
protected void onPreInit() {
    System.out.println("TestMethodApp.onPreInit: " + Thread.currentThread().getName());
}

/**
     * 获取游戏的输入
     */
@Override
protected void initInput() {
    System.out.println("TestMethodApp.initInput: " + Thread.currentThread().getName());
}

/**
     * 设置游戏的变量, 方便其他类进行访问
     *
     * @param vars vars
     */
@Override
protected void initGameVars(Map<String, Object> vars) {
    System.out.println("TestMethodApp.initGameVars: " + Thread.currentThread().getName());
    localTimer = FXGL.newLocalTimer();
}

/**
     * 初始化游戏
     */
@Override
protected void initGame() {
    System.out.println("TestMethodApp.initGame: " + Thread.currentThread().getName());
}

/**
     * 初始化物理设置, 比如碰撞检测等
     */
@Override
protected void initPhysics() {
    System.out.println("TestMethodApp.initPhysics: " + Thread.currentThread().getName());
}

/**
     * 初始化一些界面组件
     */
@Override
protected void initUI() {
    System.out.println("TestMethodApp.initUI: " + Thread.currentThread().getName());
}

/**
     * 游戏开始后每一帧都会调用该方法
     *
     * @param tpf time per frame
     */
@Override
protected void onUpdate(double tpf) {
    if (localTimer.elapsed(Duration.seconds(1))) {
        System.out.println("TestMethodApp.onUpdate: " + Thread.currentThread().getName());
        localTimer.capture();
    }
}

/**
     * 初始化游戏的设置, 比如宽高、版本、图标、菜单等等
     *
     * @param settings settings
     */
@Override
protected void initSettings(GameSettings settings) {
    settings.setMainMenuEnabled(true);
    System.out.println("TestMethodApp.initSettings: " + Thread.currentThread().getName());
}
```

```
TestMethodApp.main: main
TestMethodApp.initSettings: main
TestMethodApp.initInput: JavaFX Application Thread
TestMethodApp.onPreInit: JavaFX Application Thread
TestMethodApp.initGameVars: FXGL Background Thread 2
TestMethodApp.initGame: FXGL Background Thread 2
TestMethodApp.initPhysics: FXGL Background Thread 2
TestMethodApp.initUI: FXGL Background Thread 2
TestMethodApp.onUpdate: JavaFX Application Thread
```

- `initSettings`、`initInput`和`onPreInit`方法只执行一次
- 暂停游戏时`onUpdate`方法不执行
- 创建新游戏时会从`initGameVars`方法开始执行后面的
- `initInput`及其以后才可以调用`FXGL`相关方法

### 二、常用

#### 1、`EntityFactory`创建实体

```java
public class TankFactory implements EntityFactory {

    /**
     * 方法名称随意, @Spawns里的名称使用逗号分隔
     *
     * @param data 指定用于生成特定类型实体的数据。
     * @return Entity
     */
    @Spawns("rect,square")
    public Entity newRect(SpawnData data) {
        return FXGL.entityBuilder(data).view(new Rectangle(60, 60, Color.LIGHTBLUE)).build();
    }
}
```

```java
// 可以使用import static com.almasb.fxgl.dsl.FXGL.*;
getGameWorld().addEntityFactory(new TankFactory());
// 创建实体, 并添加到游戏世界中
getGameWorld().spawn("rect", new SpawnData(50, 80));
// 创建实体, 并添加到游戏世界中
spawn("rect", new SpawnData(80, 180));
// create方法只创建实体, 不会自动添加到游戏世界中
Entity entity = getGameWorld().create("rect", new SpawnData(200, 200));
getGameWorld().addEntity(entity);

spawn("square", new SpawnData(0, 10));
```

```java
/*
@Spawns("rect,square")
public Entity newRect(SpawnData data) {
    return FXGL.entityBuilder(data)
        .view(new Rectangle(
            data.get("w"),
            data.get("h"),
            data.get("color")
        ))
        .build();
}
*/
Entity square = spawn("square", new SpawnData(50, 50).put("w", 60.0).put("h", 60.0).put("color", Color.YELLOW));
double w = square.getDouble("w");
Color color = square.getObject("color");
System.out.println(w);
System.out.println(color);
PropertyMap properties = square.getProperties();
for (String key : properties.keys()) {
	System.out.println(key + " = " + properties.getValue(key));
}
```

#### 2、过场对话

- 格式参考：`https://github.com/AlmasB/FXGL/wiki/Narrative-and-Dialogue-System-(FXGL-11)#cutscenes`

```
# 角色定义
p1.name=村长
p1.image=p1.png
p2.name=勇者
p2.image=p2.png
p3.name=MM
p3.image=p3.png
p4.name=恶龙
p4.image=long.gif

# 聊天开始
p1:大事不好啦，村口来了一条恶龙
p2:真的假的
p3:应该是真的~我早上也听到隔壁大婶说啦！
p2:那我去收拾他
p1:我给你一个头盔，保护好自己
p2.image=p2pro.png
p2:哇，谢谢村长
p3:哥哥好酷，我好喜欢~
p4:我来啦~
```

```java
public class ChatFactory implements EntityFactory {
    @Spawns("bg")
    public Entity newBg(SpawnData data) {
        return FXGL.entityBuilder(data)
                .view("bg.png")
                .build();
    }
}
```

```java
@Override
protected void initGame() {
    FXGL.getGameWorld().addEntityFactory(new ChatFactory());
    FXGL.spawn("bg");

    // 加载资源
    FXGL.runOnce(() -> {
        List<String> lines = FXGL.getAssetLoader().loadText("chat.txt");
        Cutscene cutscene = new Cutscene(lines);
        FXGL.getCutsceneService().startCutscene(cutscene);
    }, Duration.ONE);
}
```

#### 3、`Entity`之间的距离

```java
@Override
protected void initGame() {
    getGameWorld().addEntityFactory(new GameEntityFactory());
    Entity e1 = spawn("rect", new SpawnData(0, 0)
                      .put("w", 100.0)
                      .put("h", 100.0)
                      .put("color", Color.RED)
                     );

    Entity e2 = spawn("rect", new SpawnData(100, 100)
                      .put("w", 100.0)
                      .put("h", 100.0)
                      .put("color", Color.BLUE)
                     );
    // 计算的是entity的坐标距离
    System.out.println("distance: " + e1.distance(e2)); // 142
    // 计算的是实体之间bbox的距离, 如果有交集, 结果是0
    System.out.println("distanceBBox: " + e1.distanceBBox(e2));
}
```

#### 4、`Entity Active`状态

- `Entity`只有被添加到游戏世界当中的时候，才是活跃的

```java
Entity e3 = getGameWorld().create("rect", new SpawnData(200, 200)
                                  .put("w", 100.0)
                                  .put("h", 100.0)
                                  .put("color", Color.GREEN)
                                 );

e3.setOnActive(() -> {
    System.out.println("e3活跃了");
});

e3.setOnNotActive(()->{
    System.out.println("e3不活跃了");
});

System.out.println("e3仅仅被创建: active = " + e3.isActive());
getGameWorld().addEntity(e3);
System.out.println("e3被添加到游戏世界当中了: active = " + e3.isActive());
getGameWorld().removeEntities(e3);
// 或者这样写: e3.removeFromWorld();
System.out.println("e3被移除: active = " + e3.isActive());
/*
e3仅仅被创建: active = false
e3活跃了
e3被添加到游戏世界当中了: active = true
e3不活跃了
e3被移除: active = false
*/
```

#### 5、4大核心组件

- 所有实体都有的组件
- 可以通过例如：

```java
boolean b = e1.hasComponent(TypeComponent.class);
System.out.println("是否有type组件: " + b);
```

来判断是否包含某类组件。

- 已经有的组件不能重复添加
- 四大核心组件不能移除

##### ①`TypeComponent`

- 指定类型

##### ②`BoundingBoxComponent`

- 指定实体大小

##### ③`ViewComponent`

- 指定实体的外观

##### ④`TransformComponent`

- 指定实体的位置

#### 6、图片的使用

##### ①对称图片的使用

```java
@Spawns("boat")
public Entity newBoat(SpawnData data) {
    // 读取图片时可以设置大小(推荐)
    Texture boatImgL = FXGL.texture("imgapp/boat.png", 80, 132);
    Texture boatImgR = boatImgL.copy();
    // 水平翻转
    boatImgR.setScaleX(-1);
    // 移动右边的图片
    boatImgR.setTranslateX(boatImgL.getWidth());
    return FXGL.entityBuilder(data)
        .view(boatImgL)
        .view(boatImgR)
        .bbox(BoundingShape.box(boatImgL.getWidth() * 2, boatImgL.getHeight()))
        .build();
}
```

##### ②横向序列帧图片的使用

```java
@Spawns("boom")
public Entity newBoom(SpawnData data) {
    Duration seconds = Duration.seconds(1.5);
    AnimationChannel anc = new AnimationChannel(
        FXGL.image("imgapp/bz.png", 1230 * 2, 68 * 2),
        seconds,
        15);
    AnimatedTexture ant = new AnimatedTexture(anc);
    // 循环播放: ant.loop()
    ant.play();
    return FXGL.entityBuilder(data)
        .view(ant)
        .with(new ExpireCleanComponent(seconds))
        .build();
}
```

##### ③多张图片的序列帧使用

```java
@Spawns("light")
public Entity newLight(SpawnData data) {
    Duration seconds = Duration.seconds(1.5);
    List<Image> imageList = new ArrayList<>();
    for (int i = 0; i < 6; i++) {
        imageList.add(FXGL.image("light/" + (i + 1) + ".png", 38 * 1.5, 38 * 1.5));
    }
    AnimationChannel anc = new AnimationChannel(imageList, seconds);
    AnimatedTexture ant = new AnimatedTexture(anc);
    ant.loop();
    return FXGL.entityBuilder(data)
        .view(ant)
        .build();
}
```

##### ④九宫格类型的序列帧使用

```java
@Spawns("ren")
public Entity newRen(SpawnData data) {
    Duration seconds = Duration.seconds(3);
    AnimationChannel anc = new AnimationChannel(
        FXGL.image("imgapp/ren.png"), // 图片
        6, // 一行有几个帧
        246 / 6, // 每个帧的宽度
        246 / 6, // 每个帧的宽度
        seconds, // 播放时间
        0, // 从第几个帧开始
        35 // 到第几个帧结束
    );
    AnimatedTexture ant = new AnimatedTexture(anc);
    ant.loop();
    return FXGL.entityBuilder(data)
        .view(ant)
        .build();
}
```

##### ⑤改变图片颜色，以达到复用素材效果

```java
@Spawns("mn")
public Entity newMn(SpawnData data) {
    Texture texture = FXGL.texture("imgapp/mn.png", 300.0 / 2, 400.0 / 2);
    Texture texture1 = texture.multiplyColor(FXGLMath.randomColor()).brighter();

    return FXGL.entityBuilder(data)
        .view(texture1)
        .with(new MnComponent())
        // 让组件不自动更新, 提升性能
        // 相当于entity.setEverUpdated(false)
        // 只在添加到游戏世界之前有效
        .neverUpdated()
        .build();
}

// initGame
for (int i = 0; i < 5; i++) {
    spawn("mn", FXGLMath.randomPoint(
        new Rectangle2D(0, 0, 400, 400)
    ));
}
```

#### 7、自定义`Component`

```java
public class MnComponent extends Component {
    /**
     * 当组件被添加到实体中时会调用
     */
    @Override
    public void onAdded() {
        ViewComponent vc = entity.getViewComponent();

        vc.addOnClickHandler(e -> {
            Texture node = (Texture) vc.getChildren().get(0);
            node.dispose();
            vc.clearChildren();
            vc.addChild(
                    texture("imgapp/mn.png").multiplyColor(FXGLMath.randomColor()).brighter()
            );
        });
    }

    @Override
    public void onUpdate(double tpf) {
        System.out.println("更新中。。。");
    }
}
```

- 可以直接定义`Component`类型的成员变量供方法使用（前提是该组件已经添加了，注意添加顺序）

#### 8、资源的缓存

- 默认情况下资源会被框架自动缓存
- 如果不想缓存，使用这种方式：

```java
Image image = new Image(getAssetLoader().getStream("/assets/textures/bg.png"));
Texture texture = new Texture(image);
FXGL.entityBuilder().view(texture).buildAndAttach();
```

#### 9、多个资源配合使用

- 可以通过在文件中定义位置信息

```properties
body1.image=imgapp/hero_body.png
body1.framesPerRow=1
body1.frameWidth=104
body1.frameHeight=37
body1.duration=1.0
body1.startFrame=0
body1.endFrame=1
body1.tx=0.0
body1.ty=-25.0

body2.image=imgapp/hero_body2.png
body2.framesPerRow=1
body2.frameWidth=102
body2.frameHeight=39
body2.duration=1.0
body2.startFrame=0
body2.endFrame=1
body2.tx=0.0
body2.ty=-25.0
```

```java
public class HeroComponent extends Component {

    private AnimatedTexture antHero;

    @Override
    public void onAdded() {
        AnimationChannel anc = new AnimationChannel(
                FXGL.image("imgapp/hero_foot.png"),
                1,
                58,
                58 / 2,
                Duration.seconds(1),
                0,
                1);
        antHero = new AnimatedTexture(anc);
        antHero.loop();
        entity.getViewComponent().addChild(antHero);
    }

    public void setBody(String filepath, String body) {
        entity.getViewComponent().clearChildren();
        PropertyMap propertyMap = FXGL.getAssetLoader().loadPropertyMap(filepath);
        String imagePath = propertyMap.getString(body + ".image");
        Integer framesPerRow = propertyMap.getInt(body + ".framesPerRow");
        Integer frameWidth = propertyMap.getInt(body + ".frameWidth");
        Integer frameHeight = propertyMap.getInt(body + ".frameHeight");
        Double duration = propertyMap.getDouble(body + ".duration");
        Integer startFrame = propertyMap.getInt(body + ".startFrame");
        Integer endFrame = propertyMap.getInt(body + ".endFrame");
        Double tx = propertyMap.getDouble(body + ".tx");
        Double ty = propertyMap.getDouble(body + ".ty");

        AnimationChannel ancBody = new AnimationChannel(
                FXGL.image(imagePath),
                framesPerRow,
                frameWidth,
                frameHeight,
                Duration.seconds(duration),
                startFrame,
                endFrame);
        AnimatedTexture antBody = new AnimatedTexture(ancBody);
        antBody.setTranslateX(tx);
        antBody.setTranslateY(ty);
        antBody.loop();
        entity.getViewComponent().addChild(antBody);
        entity.getViewComponent().addChild(antHero);
    }
}
```

```java
@Override
protected void initGame() {
    getGameScene().setBackgroundColor(Color.BLACK);
    getGameWorld().addEntityFactory(new ImgEntityFactory());
    Entity hero = spawn("hero", new SpawnData(100, 100));
    HeroComponent heroComponent = hero.getComponent(HeroComponent.class);
    ViewComponent vc = hero.getViewComponent();
    String[] array = {"body1", "body2"};
    vc.addOnClickHandler(e->{
        heroComponent.setBody(
            "data/heroBody.kv",
            FXGLMath.random(array).get());
    });
}
```

#### 10、四方向行走

```java
public class SpriteEntityFactory implements EntityFactory {
    @Spawns("sc")
    public Entity newSc(SpawnData data) {
        return FXGL.entityBuilder(data)
                .with(new KeepOnScreenComponent())
                .with(new ScComponent())
                .bbox(BoundingShape.box(80, 80))
                .build();
    }
}
```

```java
public class ScComponent extends Component {

    private static final double INIT_SPEED = 1.5;
    private static double speed = INIT_SPEED;
    private static Dir dir = Dir.DOWN;
    private final AnimationChannel ancUp;
    private final AnimationChannel ancDown;
    private final AnimationChannel ancLeft;
    private final AnimationChannel ancRight;
    private final AnimatedTexture ant;

    private boolean isPlay = false;

    public ScComponent() {
        ancUp = createAnc(8, 11);
        ancDown = createAnc(0, 3);
        ancLeft = createAnc(4, 7);
        ancRight = createAnc(12, 15);
        ant = new AnimatedTexture(ancDown);
    }

    private AnimationChannel createAnc(int startFrame, int endFrame) {
        return new AnimationChannel(
                FXGL.image("imgapp/神採.png"),
                4,
                80,
                80,
                Duration.seconds(0.5),
                startFrame,
                endFrame
        );
    }

    @Override
    public void onAdded() {
        entity.getViewComponent().addChild(ant);
    }

    @Override
    public void onUpdate(double tpf) {
        switch (ScComponent.dir) {
            case UP -> {
                if (ant.getAnimationChannel() != ancUp || !isPlay) {
                    ant.loopAnimationChannel(ancUp);
                    isPlay = true;
                }
                entity.translateX(0);
                entity.translateY(speed);
            }
            case DOWN -> {
                if (ant.getAnimationChannel() != ancDown || !isPlay) {
                    ant.loopAnimationChannel(ancDown);
                    isPlay = true;
                }
                entity.translateX(0);
                entity.translateY(speed);
            }
            case LEFT -> {
                if (ant.getAnimationChannel() != ancLeft || !isPlay) {
                    ant.loopAnimationChannel(ancLeft);
                    isPlay = true;
                }
                entity.translateX(speed);
                entity.translateY(0);
            }
            case RIGHT -> {
                if (ant.getAnimationChannel() != ancRight || !isPlay) {
                    ant.loopAnimationChannel(ancRight);
                    isPlay = true;
                }
                entity.translateX(speed);
                entity.translateY(0);
            }
            default -> {
                entity.translateX(0);
                entity.translateY(0);
                ant.stop();
                isPlay = false;
            }
        }
        // 有一个类似于刹车的效果
        speed *= 0.9;
        if (Math.abs(speed) < 1) {
            speed = 0;
            ant.stop();
            isPlay = false;
        }
    }

    public Unit move(Dir dir) {
        ScComponent.dir = dir;
        switch (dir) {
            case UP, LEFT -> speed = -INIT_SPEED;
            case DOWN, RIGHT -> speed = INIT_SPEED;
        }
        return Unit.INSTANCE;
    }
}
```

```java
public class SpriteApp extends GameApplication {

    private ScComponent scComponent;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    protected void initSettings(GameSettings settings) {

    }

    @Override
    protected void initInput() {
        onKey(KeyCode.DOWN, () -> scComponent.move(Dir.DOWN));
        onKey(KeyCode.UP, () -> scComponent.move(Dir.UP));
        onKey(KeyCode.LEFT, () -> scComponent.move(Dir.LEFT));
        onKey(KeyCode.RIGHT, () -> scComponent.move(Dir.RIGHT));
    }

    @Override
    protected void initGame() {
        getGameScene().setBackgroundColor(Color.GRAY);
        getGameWorld().addEntityFactory(new SpriteEntityFactory());
        Entity sc = spawn("sc");
        scComponent = sc.getComponent(ScComponent.class);
    }
}
```

#### 11、添加支持中文

- 在`assets`下新增`languages`目录，添加`chinese.lang`文件，将原`english.lang`对应值改为中文即可。

```java
// 添加中文支持
settings.getSupportedLanguages().add(Language.CHINESE);
settings.setDefaultLanguage(Language.CHINESE);
```

#### 12、自定义菜单

```java
// 自定义菜单
settings.setSceneFactory(new SceneFactory(){
    @NotNull
    @Override
    public FXGLMenu newGameMenu() {
        return new TestMainMenu();
    }
});
```

```java
public class TestMainMenu extends FXGLMenu {
    private VBox box;
    public TestMainMenu() {
        super(MenuType.MAIN_MENU);

        Button btn1 = new Button("开始新游戏");
        btn1.setOnAction(e -> getController().startNewGame());

        Button btn2 = new Button("设置");
        btn2.setOnAction(e -> getController().gotoGameMenu());

        Button btn3 = new Button("退出");
        btn3.setOnAction(e -> getController().exit());

        Button btn4 = new Button("其他");

        Button btn5 = new Button("帮助");
        btn5.setOnAction(e -> new Alert(Alert.AlertType.INFORMATION).showAndWait());

        box = new VBox(btn1, btn2, btn3, btn4, btn5);
        getContentRoot().getChildren().setAll(box);
    }

    @Override
    public void onCreate() {
        TranslateTransition tt = new TranslateTransition(Duration.seconds(1), box);
        tt.setToX(0);
        tt.setToX(300);
        tt.play();
    }
}
```

#### 13、自定义加载场景

```java
public class CustomSceneApp extends GameApplication {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    protected void initSettings(GameSettings settings) {
        settings.setSceneFactory(new SceneFactory(){
            @NotNull
            @Override
            public FXGLMenu newMainMenu() {
                return new TestMainMenu();
            }

            @NotNull
            @Override
            public LoadingScene newLoadingScene() {
                return new TestLoadingScene();
            }
        });
    }

    @Override
    protected void initGame() {
        for (int i = 0; i < 50000; i++) {
            FXGL.entityBuilder()
                    .at(FXGLMath.randomPoint(new Rectangle2D(0, 0, 800, 600)))
                    .view(new Rectangle(2, 2, FXGLMath.randomColor()))
                    .buildAndAttach();
        }
    }
}
```

```java
public class TestLoadingScene extends LoadingScene {
    private final FillTransition ft;

    public TestLoadingScene() {
        Circle circle = new Circle(190, FXGLMath.randomColor());
        circle.setLayoutX(220);
        circle.setLayoutY(180);
        ft = new FillTransition(Duration.seconds(1), circle);
        ft.setFromValue(FXGLMath.randomColor());
        ft.setToValue(FXGLMath.randomColor());
        ft.setCycleCount(Animation.INDEFINITE);
        getContentRoot().getChildren().add(circle);
    }
    
    @Override
    public void onCreate() {
        ft.play();
    }

    @Override
    public void onDestroy() {
        ft.stop();
    }
}
```

#### 14、从`json`读取数据

```java
public class ReadJsonApp extends GameApplication {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    protected void initSettings(GameSettings settings) {
    }

    @Override
    protected void initGame() {
        FXGL.getGameWorld().addEntityFactory(new GameEntityFactory());
        Optional<RectInfo> rectInfoOptional = FXGL.getAssetLoader().loadJSON("json/rect1.json", RectInfo.class);
        rectInfoOptional.ifPresent(e -> {
            FXGL.spawn("jsonRect", new SpawnData().put("rectInfo", e));
        });
    }
}
```

```json
{
  "x": 200,
  "y": 180,
  "color": "red",
  "width": 100,
  "height": 100,
  "moveInfo": {
    "dir": "hor",
    "duration": 2,
    "distance": 260
  }
}
```

```java
public class GameEntityFactory implements EntityFactory {
    @Spawns("jsonRect")
    public Entity newJsonRect(SpawnData data) {
        return FXGL.entityBuilder(data)
                .with(new RectComponent())
                .build();
    }
}
```

```java
public class RectComponent extends Component {
    public RectComponent() {
    }

    @Override
    public void onAdded() {
        RectInfo rectInfo = entity.getObject("rectInfo");
        entity.setPosition(rectInfo.getX(), rectInfo.getY());
        entity.getViewComponent().addChild(new Rectangle(
                rectInfo.getWidth(),
                rectInfo.getWidth(),
                Color.web(rectInfo.getColor())
        ));

        LiftComponent liftComponent = new LiftComponent();
        MoveInfo moveInfo = rectInfo.getMoveInfo();
        if ("hor".equals(rectInfo.getMoveInfo().getDir())) {
            liftComponent.setGoingRight(true);
            liftComponent.xAxisDistanceDuration(moveInfo.getDistance(), Duration.seconds(moveInfo.getDuration()));
        } else {
            liftComponent.setGoingUp(true);
            liftComponent.yAxisDistanceDuration(moveInfo.getDistance(), Duration.seconds(moveInfo.getDuration()));
        }
        entity.addComponent(liftComponent);
    }
}
```

#### 15、手动缓存与获取`GameApplication`实例

- 使用`FXGL.getAppCast();`获取`GameApplication`实例，将需要缓存的数据存入变量中。

#### 16、去除默认`log`打印

- `Logger.removeAllOutputs();`最好放在`initGame`方法中

#### 17、对话框

##### ①一般对话框

```java
FXGL.getDialogService().showMessageBox(entity.getPosition().toString(), ()->{
     System.out.println("对话框被关闭了");
});
System.out.println("123");
// 或者
FXGL.showMessage(entity.getPosition().toString());
```

##### ②带加载中的效果

```java
FXGL.showMessage(entity.getPosition().toString());
//带加载处理
DialogBox dialogBox = FXGL.getDialogService().showProgressBox("处理中");
//暂停动画
PauseTransition pt = new PauseTransition(Duration.seconds(2));
pt.setOnFinished(e -> dialogBox.close());
pt.play();
```

##### ③带进度处理

```java
// 带进度处理
Image image = new Image(
  "https://pic.rmb.bdstatic.com/bjh/events/70bb4729b4e7a9257dcb5d5fc788e7553078.jpeg@h_1280",
    true);
FXGL.getDialogService().showProgressBox("下载图片...", image.progressProperty(), () -> {
    ImageView iv = new ImageView(image);
    iv.setFitHeight(600);
    iv.setSmooth(true);
    iv.setPreserveRatio(true);
    FXGL.spawn("bg", new SpawnData().put("imageView", iv));
});
```

##### ④输入对话框

```java
// 输入对话框
FXGL.getDialogService()
    .showInputBox("请输入用户名",
                  name -> !"admin".equalsIgnoreCase(name),
                  name -> FXGL.showMessage("你的名字: " + name));
```

##### ⑤选择对话框

- 如果选项很多会变为一个下拉列表

```java
FXGL.getDialogService().showChoiceBox("选择颜色", rect::setFill, Color.RED, Color.BLUE, Color.GREEN);
```

##### ⑥确认对话框

```java
FXGL.getDialogService().showConfirmationBox("是否移除实体", b -> {
    if (b) {
    	entity.removeFromWorld();
    }
});
```

##### ⑦错误对话框

```java
FXGL.getDialogService().showErrorBox("错误对话框", () -> {
    System.out.println("回调函数");
});
```

##### ⑧自定义对话框

```java
FXGL.getDialogService()
.showBox("自定义对话框", new StackPane(new Label("---")), new Button("btn1"), new Button("btn2"));
```

#### 18、默认`CSS`样式修改

- 参考源码，在`assets/ui/css`目录下新增`CSS`文件，修改对应`CSS`样式，然后添加该`CSS`文件，例如：

  ```java
  @Override
  protected void initSettings(GameSettings settings) {
      settings.getCSSList().add("test.css");
  }
  ```

#### 19、子场景

```java
public class SceneApp extends GameApplication {
    private final LazyValue<MySubScene> subSceneLazyValue = new LazyValue<>(()->{
        System.out.println("创建了mySubScene");
        return new MySubScene();
    });

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    protected void initSettings(GameSettings settings) {
    }

    @Override
    protected void initGame() {
        FXGL.getGameWorld().addEntityFactory(new GameEntityFactory());
        Entity rect = spawn("rect", new SpawnData(0, 0)
                .put("w", 100.0)
                .put("h", 100.0)
                .put("color", Color.RED)
        );
        rect.getViewComponent().addOnClickHandler(event -> {
            FXGL.getSceneService().pushSubScene(subSceneLazyValue.get());
        });
    }
}
```

```java
public class MySubScene extends SubScene {

    private Label label;
    public MySubScene() {
        label = new Label();
        Button ok = new Button("ok");
        ok.setOnAction(event -> FXGL.getSceneService().popSubScene());
        BorderPane center = new BorderPane();
        center.setCenter(label);
        center.setBottom(ok);
        center.setMaxSize(320, 180);
        center.setStyle("-fx-background-color: white;");
        StackPane stackPane = new StackPane(center);
        stackPane.setStyle("-fx-background-color: #0007;-fx-border-color: skyblue;");
        stackPane.setPrefSize(FXGL.getAppWidth(), FXGL.getAppHeight());
        getContentRoot().getChildren().add(stackPane);
    }

    @Override
    public void onCreate() {
        Entity entity = FXGL.getGameWorld().getEntities().getFirst();
        label.setText(entity.getPosition().toString());
    }
}
```

#### 20、地图的制作与加载

- `Tield`：`https://www.mapeditor.org/`

```java
@Override
protected void initGame() {
    Level level = FXGL.setLevelFromMap("level1.tmx");

    String level1 = level.getProperties().getString("level");
    System.out.println(level1);
    Integer max = level.getProperties().getInt("max");
    System.out.println(max);
}
```

- 视口抖动

```java
FXGL.getGameScene().getViewport().shakeTranslational(10);
```

#### 21、无缝滚动

```java
public class ScrollApp extends GameApplication {
    private Entity player;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    protected void initInput() {
        FXGL.onKey(KeyCode.LEFT, () -> player.translateX(-10));
        FXGL.onKey(KeyCode.RIGHT, () -> player.translateX(10));
    }

    @Override
    protected void initSettings(GameSettings settings) {
        settings.setWidth(1280);
        settings.setHeight(650);
    }

    @Override
    protected void initGame() {
        FXGL.getGameWorld().addEntityFactory(new ScrollEntityFactory());
        FXGL.spawn("bg");
        player = FXGL.spawn("player", 0, FXGL.getAppHeight() / 2.0);
        FXGL.getGameScene().getViewport().bindToEntity(player, FXGL.getAppWidth() / 2.0, FXGL.getAppHeight() / 2.0);
        // 设置懒加载
        // FXGL.getGameScene().getViewport().setLazy(true);
        // 浮动效果(和懒加载不能同时使用)
        FXGL.getGameScene().getViewport().setFloating(true);
    }
}
```

```java
public class ScrollEntityFactory implements EntityFactory {
    @Spawns("bg")
    public Entity newBg(SpawnData data) {
        ScrollingBackgroundView sbv = new ScrollingBackgroundView(
                FXGL.image("bg.png"),
                FXGL.getAppWidth(),
                FXGL.getAppHeight(),
                Orientation.HORIZONTAL
        );
        return FXGL.entityBuilder(data)
                .view(sbv)
                .build();
    }

    @Spawns("player")
    public Entity newPlayer(SpawnData data) {
        return FXGL.entityBuilder(data)
                .view(new Rectangle(30,30, Color.SKYBLUE))
                .build();
    }
}
```







