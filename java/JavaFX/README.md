### 一、基础

#### 1、需要的包(`JDK8`以上的版本)

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>priv.zq</groupId>
    <artifactId>JavaFXTest</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <JavaFX.Version>17.0.8</JavaFX.Version>
    </properties>
    <dependencies>
        <!--    从jdk11起, javafx已经从jdk中独立, 需要添加JavaFX的依赖    -->
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-controls</artifactId>
            <version>${JavaFX.Version}</version>
        </dependency>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-fxml</artifactId>
            <version>${JavaFX.Version}</version>
        </dependency>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-web</artifactId>
            <version>${JavaFX.Version}</version>
        </dependency>
    </dependencies>

</project>
```

##### 简单的启动示例：

```java
package priv.zq;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @author huayunliufeng
 * @date_time 2023/8/21 16:21
 * @desc
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.show();
    }
}
```

如果提示找不到，在根包所在目录（例如maven中的java目录）创建`module-info.java`文件，添加以下内容：

```java
module JavaFXTest {
    requires javafx.controls;
    requires javafx.base;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.media;
    requires javafx.web;
    requires javafx.swing;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.ikonli.fontawesome5;
    requires lombok;
    opens priv.zq.controller to javafx.fxml;
    opens priv.zq.builder to javafx.fxml;
    exports priv.zq;
}
```

如果需要获取`resources`目录下的文件，要通过以下方式：

```java
URL url = Main.class.getResource("/fxml/myfxml.fxml");
```

其中`Main`是此项目中任意的类名，**重点是路径开头要加`/`**

#### 2、`application`的启动方式和生命周期

- 必须继承`Application`类，并实现`start`方法，在`main`中调用`launch`方法。

- ##### 生命周期：

  ```java
  import javafx.application.Application;
  import javafx.stage.Stage;
  
  /**
   * @author huayunliufeng
   * @date_time 2023/8/21 16:21
   * @desc
   */
  public class Main extends Application {
  
      public static void main(String[] args) {
          System.out.println("main: " + getThreadName());
          launch(args);
      }
  
      @Override
      public void start(Stage stage) {
          stage.show();
          stage.setTitle("这是第一个JavaFX程序");
          System.out.println("start: " + getThreadName());
      }
  
      @Override
      public void init() {
          System.out.println("init: " + getThreadName());
      }
  
      @Override
      public void stop() {
          System.out.println("stop: " + getThreadName());
      }
  
      private static String getThreadName() {
          return Thread.currentThread().getName();
      }
  }
  /*
  main: main
  init: JavaFX-Launcher
  start: JavaFX Application Thread
  stop: JavaFX Application Thread
  */
  ```

#### 3、`Stage`

##### ①常用方法

```java
package priv.zq;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * @author huayunliufeng
 * @date_time 2023/8/21 16:21
 * @desc
 */
public class Main extends Application {

    private static final String iconPath = "src\\main\\resources\\favicon.png";

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        // 有该方法才能显示界面
        stage.show();
        stage.setTitle("这是第一个JavaFX程序");
        try {
            // 设置图标
            stage.getIcons().add(new Image(new FileInputStream(iconPath)));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        // 设置最小化, 执行该方法即可将窗口最小化, 相似方法同理
        // stage.setIconified(true);
        // 设置最大化
        // stage.setMaximized(true);
        // 关闭窗口, 执行即关闭
        // stage.close();
        // 设置窗口宽和高
        stage.setWidth(500);
        stage.setHeight(500);
        // 设置初始位置
        stage.setX(200);
        stage.setY(200);
        // 设置窗口大小不可更改
        // stage.setResizable(false);
        // 设置最大最小宽和高
        // stage.setMinWidth(200);
        // stage.setMinHeight(200);
        // stage.setMaxWidth(800);
        // stage.setMaxHeight(800);
        // 获取宽和高, 没设置则获取默认大小, 如果show()方法在后面会报错
        // System.out.println("宽度 = " + stage.getWidth());
        // System.out.println("高度 = " + stage.getHeight());
        // 给窗口高度变化绑定事件
        // stage.widthProperty().addListener((observable, oldValue, newValue) -> System.out.println("新的宽度 = " + newValue));
        // 给高度变化绑定事件
        // stage.heightProperty().addListener((observable, oldValue, newValue) -> System.out.println("新的高度 = " + newValue));
        /* 设置全屏, show()方法要紧跟着后面
        stage.setFullScreen(true);
        stage.setScene(new Scene(new Group()));
        stage.show();
        */
        // 设置窗口透明度
        // stage.setOpacity(0.8);
        // 设置窗口置顶
        // stage.setAlwaysOnTop(true);
        // 给横坐标和纵坐标变化添加事件
        // stage.xProperty().addListener((observable, oldValue, newValue) -> System.out.println("x轴坐标 = " + newValue));
        // stage.yProperty().addListener((observable, oldValue, newValue) -> System.out.println("y轴坐标 = " + newValue));
    }
}
```

##### ②多窗口

- 一个`Stage`对象就是一个新的窗口

  ```java
  stage.initStyle(StageStyle.UTILITY);
  /*
  DECORATED: 定义具有纯白色背景和平台装饰的普通Stage样式。
  UNIFIED: 定义带有平台装饰的Stage样式，并消除客户端区域和装饰之间的边界。客户区背景与装饰相统一。
  UTILITY: 定义具有纯白色背景和用于实用程序窗口的最小平台装饰的Stage样式。
  UNDECORATED: 定义具有纯白色背景且没有装饰的Stage样式。
  TRANSPARENT: 定义具有透明背景且没有装饰的Stage样式。
  */
  ```

- 多个窗口之间的关系

  ```java
  public void start(Stage stage) {
      stage.setTitle("第一个窗口");
      Stage tmpStage1 = new Stage();
      Stage tmpStage2 = new Stage();
      tmpStage1.setTitle("第二个窗口");
      tmpStage2.setTitle("第三个窗口");
      // 设置窗口关联
      tmpStage1.initOwner(stage);
      // 定义一个模态窗口，该窗口阻止事件传递给其整个所有者窗口层次结构。
      tmpStage1.initModality(Modality.WINDOW_MODAL);
      // 定义一个模态窗口，该窗口阻止事件传递到任何其他应用程序窗口。
      tmpStage2.initModality(Modality.APPLICATION_MODAL);
      // show()的顺序也要注意
      stage.show();
      tmpStage1.show();
      tmpStage2.show();
  }
  ```

##### ③`Platform`类

- 主要是检查平台相关特性

  ```java
  public void start(Stage stage) {
      // 使用的线程和start线程相同, 不适合执行大量任务
      Platform.runLater(() -> System.out.println("AAA " + getThreadName()));
      System.out.println("BBB " + getThreadName());
      /*
          如果此属性为true，则JavaFX运行时将在关闭最后一个窗口时隐式关闭;JavaFX启动器将调用应用程序。方法并终止JavaFX应用程序线程.
          如果此属性为false，则应用程序将继续正常运行，即使在关闭最后一个窗口之后，直到应用程序调用exit。默认值为true。这个方法可以从任何线程调用。
           */
      // Platform.setImplicitExit(false);
      // 是否支持某个属性
      System.out.println(Platform.isSupported(ConditionalFeature.SCENE3D));
      // stage.show();
  }
  ```

##### ④`Screen`类

```java
public void start(Stage stage) {
    Screen screen = Screen.getPrimary();
    System.out.println(screen);
    // dpi, 即图像每英寸长度内像素点数
    System.out.println(screen.getDpi());
    // 获得完整的宽高
    Rectangle2D bounds = screen.getBounds();
    System.out.println(bounds);
    // 获得可见的宽高
    Rectangle2D visualBounds = screen.getVisualBounds();
    System.out.println(visualBounds);
    Platform.exit();
}
```

##### ⑤窗口层次图

- Stage（窗口）
  - Scene（场景，在窗口上）
    - Node（各个组件，例如文本框、按钮等，在Scene上）

```java
public void start(Stage stage) {
    URL url = this.getClass().getClassLoader().getResource("favicon.png");
    Button button = new Button("按钮");
    button.setPrefWidth(200);
    button.setPrefHeight(100);
    button.setCursor(Cursor.HAND);

    Group group = new Group();
    group.getChildren().add(button);

    Scene scene = new Scene(group);
    if (url != null) {
        scene.setCursor(Cursor.cursor(url.toExternalForm()));
    }
    stage.setScene(scene);
    stage.setHeight(800);
    stage.setWidth(800);
    stage.setTitle("窗口");
    stage.show();
    // 打开一个网页
    HostServices hostServices = getHostServices();
    System.out.println(hostServices.getCodeBase());
    System.out.println(hostServices.getDocumentBase());
    hostServices.showDocument("https://www.baidu.com");
}
```

- 其他事件

  ```java
  public void start(Stage stage) {
      Button button1 = new Button("button1");
      // 设置布局
      button1.setLayoutX(0);
      button1.setLayoutY(0);
      button1.setOpacity(0.2);
      Button button2 = new Button("button2");
      button2.setLayoutX(0);
      button2.setLayoutY(100);
      Button button3 = new Button("button3");
      button3.setLayoutX(0);
      button3.setLayoutY(200);
      Group group = new Group(button1, button2, button3);
      // 删除指定索引的Node
      // group.getChildren().remove(0);
      // 清除所有Node
      // group.getChildren().clear();
      // group.setAutoSizeChildren(false);
      // 给group设置的属性会影响所有子组件
      group.setOpacity(0.8);
      // 指定位置是否有Node, 只能定位左上角的位置
      System.out.println(group.contains(0, 0));
      // 监听Node数量变化的事件
      group.getChildren().addListener((ListChangeListener<Node>) c -> {
          int size = c.getList().size();
          System.out.println(size);
      });
      // 给按钮添加点击事件
      button1.setOnAction(event -> {
          Button button4 = new Button("button4");
          button4.setLayoutX(0);
          button4.setLayoutY(300);
          group.getChildren().add(button4);
      });
      Scene scene = new Scene(group);
  
      stage.setScene(scene);
      stage.setTitle("JavaFX");
      stage.setWidth(800);
      stage.setHeight(800);
      stage.show();
  }
  ```

#### 4、打包为`exe`可执行文件

##### ①如果使用了`lombok`，需要添加以下插件：

- 对于一些额外的上下文 -  在“模块模式”编译出现之前，类路径上的任何注释处理器都会自动应用。在模块模式编译领域，事情就没那么简单了。最好向编译器显式提及所有  `AP（“&lt;annotationProcessorPaths&gt;”`就是这样做的）。

```xml
<build>
    <plugins>
        <plugin>
            
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>3.11.0</version>
            <configuration>
                <source>${maven.compiler.source}</source>
                <target>${maven.compiler.target}</target>
                <annotationProcessorPaths>
                    <annotationProcessorPath>
                        <groupId>org.projectlombok</groupId>
                        <artifactId>lombok</artifactId>
                        <version>1.18.28</version>
                    </annotationProcessorPath>
                </annotationProcessorPaths>
            </configuration>
        </plugin>
    </plugins>
</build>
```





#### 5、调用动态链接库`dll`

- 导入`JNA`库

```xml
<!-- 调用dll -->
<dependency>
    <groupId>net.java.dev.jna</groupId>
    <artifactId>jna</artifactId>
    <version>5.12.1</version>
</dependency>
<!-- jna-platform 引用了 jna, 所以引入该包即可 -->
<dependency>
    <groupId>net.java.dev.jna</groupId>
    <artifactId>jna-platform</artifactId>
    <version>5.12.1</version>
</dependency>
```

- 编写一个接口

```java
public interface MyDll extends Library {

    // 名称任意
    MyDll mydll = Native.load("dll", MyDll.class);

    int add(int a, int b);
}
```

- 使用类库

```java
import com.sun.jna.Platform;
import priv.zq.dll.MyDll;
public class JnaTest {
    public static void main(String[] args) {
        // 判断平台
        System.out.println(Platform.isWindows());
        int add = MyDll.mydll.add(10, 20);
        System.out.println(add);
    }
}
```

### 二、常用组件

#### 1、按钮

##### ①设置样式

```java
/**
// 这是使用Java实现的样式
button.setLayoutX(20);
button.setLayoutY(300);
button.setPrefWidth(150);
button.setPrefHeight(80);
button.setFont(Font.font("微软雅黑", 18));
// 设置文字颜色, 第7位和第八位表示透明度, 范围是00-ff
button.setTextFill(Paint.valueOf("#cd0000"));
// 依次是: 背景颜色, 圆角半径, 外边距(css中的padding)
button.setBackground(new Background(new BackgroundFill(Paint.valueOf("#ffeeaa"), new CornerRadii(15), new Insets(5))));
button.setBorder(new Border(new BorderStroke(Paint.valueOf("#000"), BorderStrokeStyle.DOTTED, new CornerRadii(15), new BorderWidths(3))));
*/
// 建议使用css样式, 只能在一个setStyle中使用, 否则会覆盖
button.setStyle("-fx-pref-width: 150px;-fx-translate-x: 20;-fx-translate-y: 300;" +
                "-fx-background-radius: 15px;" +
                "-fx-pref-height: 80px;" +
                "-fx-background-color: #fea;" +
                "-fx-background-insets: 5px;" +
                "-fx-border-color: #000;" +
                "-fx-border-style: dotted;" +
                "-fx-border-radius: 15px;" +
                "-fx-border-width: 3px");
button.setOnAction(event -> {
    // 获取事件源
    // event.getSource();
});
```

##### ②事件

```java
// 通用的事件绑定
button.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
    System.out.println("鼠标按键: " + event.getButton().name());
    if (event.getClickCount() == 2 && event.getButton().equals(MouseButton.PRIMARY)) {
        System.out.println("双击事件");
    }
});
// 鼠标进入事件
button.addEventHandler(MouseEvent.MOUSE_ENTERED, event -> button.setStyle("-fx-background-color: #dc1a4f"));
// 鼠标退出事件
button.addEventHandler(MouseEvent.MOUSE_EXITED, event -> button.setStyle("-fx-background-color: #ded4d4"));

// 键盘按下事件, 当前选中该按钮才能触发
button.setOnKeyPressed(event -> {
    bt.setText(event.getCode().getName() + "键按下");
    if (KeyCode.A.equals(event.getCode())) {
        System.out.println("按下了A按钮");
    }

});

// 键盘弹起事件
button.setOnKeyReleased(event -> bt.setText(event.getCode().getName() + "键弹起"));
```

##### ③设置快捷键

```java
// 设置快捷键
// 第一种, 按下ALT+Q键触发点击事件
KeyCombination kcc1 = new KeyCodeCombination(KeyCode.Q, KeyCombination.ALT_DOWN);
Mnemonic mnemonic1 = new Mnemonic(button, kcc1);
scene.addMnemonic(mnemonic1);

// 第二种, 按下ALT+W键触发点击事件
KeyCombination kcc2 = new KeyCharacterCombination("W", KeyCombination.ALT_DOWN);
Mnemonic mnemonic2 = new Mnemonic(button, kcc2);
scene.addMnemonic(mnemonic2);

// 第三种, 推荐, 按下CTRL+E触发事件
KeyCombination kcc3 = new KeyCodeCombination(KeyCode.E, KeyCombination.CONTROL_DOWN);
// 并没有开启新的线程
scene.getAccelerators().put(kcc3, button::fire);

// 第四种
KeyCombination kcc4 = KeyCombination.valueOf("ctrl+f");
Mnemonic mnemonic = new Mnemonic(button, kcc4);
scene.addMnemonic(mnemonic);
```

##### ④组合按钮

```java
// 将一组按钮使用水平布局放在一起, 默认统一尺寸
ButtonBar buttonBar = new ButtonBar();
Button[] buttonList = Utils.getButtonList(3);
ButtonBar.setButtonData(buttonList[0], ButtonBar.ButtonData.APPLY);
ButtonBar.setButtonData(buttonList[1], ButtonBar.ButtonData.NO);
ButtonBar.setButtonData(buttonList[2], ButtonBar.ButtonData.HELP);
// 设置样式(不同系统的样式)
buttonBar.setButtonOrder(ButtonBar.BUTTON_ORDER_LINUX);
// 设置某个按钮不同
buttonList[0].setStyle("-fx-pref-width: 100px;-fx-background-color: #c56161;");
ButtonBar.setButtonUniformSize(buttonList[0], false);
buttonBar.getButtons().addAll(buttonList);
AnchorPane anchorPane = new AnchorPane(buttonBar);
```

#### 2、输入框

##### ①常用方法

```java
// 输入框
TextField textField = new TextField();
textField.setStyle("-fx-translate-x: 40px; -fx-translate-y: 20px;-fx-font-family: Microsoft YaHei;");
TextField tmp = new TextField();
tmp.setStyle("-fx-translate-x: 20px; -fx-translate-y: 100px;");
// 设置文本
textField.setText("123456");
// 设置提示气泡
textField.setTooltip(new Tooltip("这是一段提示文本"));
// 设置提示信息
tmp.setPromptText("请输入一段文本");
// 设置获取焦点
textField.setFocusTraversable(true);
// 默认事件是按下Enter键
textField.setOnAction(event -> System.out.println("Enter按下, " + event.getEventType()));
// 文本更改事件
textField.textProperty().addListener((observable, oldValue, newValue) -> System.out.println("文本更改事件"));
// 第二种文本更改事件方式
textField.setTextFormatter(new TextFormatter<>(change -> {
    String newText = change.getControlNewText();
    String oldText = change.getControlText();
    // 获取选择的文本, 不建议使用该方式
    // IndexRange selection = change.getSelection();
    // System.out.println("选中的文本: " + newText.substring(selection.getStart(), selection.getEnd()));
    // 最大最小长度
    if (newText.length() <= 10 && newText.length() >= 4) {
        return change;
    }
    // 限制输入汉字(\u4E00-\u9FA5)
    if (newText.matches("[一-龥]*")) {
        return change;
    }
    return null;
}));

// 建议使用这种方式获取选中的文本
textField.selectedTextProperty().addListener((observable, oldValue, newValue) -> System.out.println("选中的文本: " + newValue));
// 密码框, 基本方法和TextField相同
PasswordField passwordField = new PasswordField();
passwordField.setStyle("-fx-translate-x: 40px; -fx-translate-y:60px;");
// 标签
Label label1 = new Label("账号: ");
label1.setStyle("-fx-translate-x: 5px;-fx-translate-y: 25px;");
Label label2 = new Label("密码: ");
label2.setStyle("-fx-translate-x: 5px;-fx-translate-y: 65px;");
```

```java
// 多行文本
TextArea textArea = new TextArea("1111111111111111111111111111111dqeqeq");
textArea.setFont(Font.font(16));
// 是否自动换行
// textArea.setWrapText(true);
// 设置n行n列(不表示实际字符数)
textArea.setPrefRowCount(10);
textArea.setPrefColumnCount(10);
// 设置宽高, 会覆盖行和列的设置
textArea.setPrefWidth(300);
textArea.setPrefHeight(200);
// 追加文本
textArea.appendText("\n123447");
// 删除文本
// textArea.deleteText(1, 3);
// 插入文本
// textArea.insertText(4, "插入");
// 替换文本
// textArea.replaceText(7,9, "替换");
// 选择所有文本
// textArea.selectAll();
// 选择光标后的一个字符
// textArea.selectForward();
// 选择光标处到指定位置的字符
// textArea.selectPositionCaret(3);
// 选中指定位置的文字
// textArea.selectRange(4,7);
// 设置光标位置, 特殊的, end()移动到末尾, home()移动到开头
// textArea.positionCaret(10);
// 选中开头到光标位置的文字, 同理的还有selectEnd()
// textArea.selectHome();
// 设置是否可以编辑
// textArea.setEditable(false);
// 复制选中文本到剪切板
// textArea.copy();
// 清空文本
// textArea.clear();
// 监听选择的文本
textArea.selectedTextProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue));
// 监听滚动
textArea.scrollLeftProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue));
AnchorPane anchorPane = new AnchorPane(textArea);
anchorPane.setOnMouseClicked(event -> {
    // 设置滚动条的水平位置
    textArea.setScrollLeft(pos = pos + 20);
});
```

##### ②限制用户输入

```java
// 多行文本
TextArea textArea = new TextArea();
// 限制输入
textArea.setTextFormatter(new TextFormatter<String>(change -> {
    System.out.println(change.getText());
    return change;
}));
// 单行文本
TextField textField = new TextField();
// 数据进入fromString方法, 从fromString返回后进入toString方法, 最后返回
// fromString的返回值类型必须和toString的入参类型对应, 不一定是String类型
textField.setTextFormatter(new TextFormatter<String>(new StringConverter<>() {
    @Override
    public String toString(String str) {
        System.out.println("toString: " + str);
        return str;
    }

    @Override
    public String fromString(String str) {
        // 可以对输入的文本进行处理
        str = str.replace("AAA", "BBB");
        return str;
    }
}));
textField.commitValue();
```

##### ③关键字查询和文本排序

```java
TextField textField = new TextField("bc");
Button findButton = new Button("查询");
Button sortButton = new Button("排序");
HBox hBox = new HBox(textField, findButton, sortButton);
hBox.setAlignment(Pos.CENTER);
hBox.setSpacing(20);
TextArea textArea = new TextArea("abbc bcbc eqhdoa dnakhn\nkaclncxa aac");
VBox vBox = new VBox(hBox, textArea);
vBox.setSpacing(20);
vBox.setPadding(new Insets(10));
AnchorPane anchorPane = new AnchorPane(vBox);
// 自动居中
anchorPane.widthProperty().addListener((observable, oldValue, newValue) -> {
    vBox.setPrefWidth(anchorPane.getWidth());
    vBox.setAlignment(Pos.TOP_CENTER);
});
Scene scene = new Scene(anchorPane);
stage.setScene(scene);
// 查找
findButton.setOnAction(event -> {
    String targetText = textField.getText();
    if (preIndex >= textArea.getLength()) {
        return;
    }
    String sourceText = textArea.getText().substring(preIndex, textArea.getLength());
    int index = sourceText.indexOf(targetText);
    if (index != -1) {
        index += preIndex;
        preIndex = index + targetText.length();
        textArea.selectRange(index, preIndex);
        textArea.requestFocus();
    }else {
        stage.setTitle(targetText + " 未找到");
    }
});
// 排序
sortButton.setOnAction(event -> {
    char[] charArray = textArea.getText().toCharArray();
    Arrays.sort(charArray);
    textArea.setText(new String(charArray));
});
// 布局
stage.setTitle("关键字查询和文本排序");
stage.setWidth(600);
stage.setHeight(400);
stage.show();
```





#### 3、布局类

##### ①绝对布局

- 基本用法

```java
Button button1 = new Button("button1");
Button button2 = new Button("button2");
Button button3 = new Button("button3");
Button button4 = new Button("button4");
// 相当于组合
Group group1 = new Group(button1, button2);
// 现在设置边距是相对于group1
button2.setStyle("-fx-translate-x: 80px;");
Group group2 = new Group(button3, button4);
button4.setStyle("-fx-translate-x: 80px;");

// 定位布局(绝对定位)
AnchorPane anchorPane = new AnchorPane(group1, group2);
anchorPane.setStyle("-fx-background-color: #2daad7;-fx-translate-x: 50px;-fx-translate-y: 50px;");
// 使用这种方式可以做简单的UI适配
AnchorPane.setTopAnchor(group1, 10.0);
AnchorPane.setLeftAnchor(group1, 10.0);
AnchorPane.setTopAnchor(group2, 50.0);
AnchorPane.setLeftAnchor(group2, 10.0);
// 设置内边距
anchorPane.setPadding(new Insets(10));
/*
        使用AnchorPane定位后下面的设置无效, 但setStyle仍然有效
        button1.setLayoutX(100);
        button1.setLayoutY(100);
        */
anchorPane.setOnMouseClicked(event -> System.out.println("点击事件"));
Scene scene = new Scene(anchorPane);
```

- 嵌套的布局

```java
Button button1 = new Button("button1");
Button button2 = new Button("button2");
Button button3 = new Button("button3");
Button button4 = new Button("button4");

// 定位布局(绝对定位)
AnchorPane anchorPane2 = new AnchorPane(button1, button2);
// 将button1放在右下角
AnchorPane.setRightAnchor(button1, 0.0);
AnchorPane.setBottomAnchor(button1, 0.0);
// 将button2放在左上角
AnchorPane.setLeftAnchor(button2, 0.0);
AnchorPane.setTopAnchor(button2, 0.0);
// 嵌套的布局
AnchorPane anchorPane = new AnchorPane(anchorPane2, button3, button4);
// 将button3放在左下角
AnchorPane.setLeftAnchor(button3, 0.0);
AnchorPane.setBottomAnchor(button3,0.0);
// 将button4放在右上角
AnchorPane.setRightAnchor(button4, 0.0);
AnchorPane.setTopAnchor(button4,0.0);
// 默认背景是透明的
anchorPane2.setStyle("-fx-background-color: #ce607e;");
anchorPane.setStyle("-fx-background-color: #2daad7;");

Scene scene = new Scene(anchorPane);
stage.setScene(scene);
stage.setTitle("JavaFX");
stage.setWidth(800);
stage.setHeight(800);
stage.show();
AnchorPane.setLeftAnchor(anchorPane2, 0.0);
AnchorPane.setRightAnchor(anchorPane2, anchorPane.getWidth() / 2);
AnchorPane.setTopAnchor(anchorPane2, 0.0);
AnchorPane.setBottomAnchor(anchorPane2, anchorPane.getHeight() / 2);
anchorPane.heightProperty().addListener((observable, oldValue, newValue) -> {
    AnchorPane.setRightAnchor(anchorPane2, anchorPane.getWidth() / 2);
});
anchorPane.widthProperty().addListener((observable, oldValue, newValue) -> {
    AnchorPane.setBottomAnchor(anchorPane2, anchorPane.getHeight() / 2);
});
// 脱离管理
// button1.setManaged(false);
// 设置不可见
// button2.setVisible(false);
```

##### ②水平和垂直布局

```java
Button button1 = new Button("button1");
Button button2 = new Button("button2");
Button button3 = new Button("button3");
// 水平布局, 不会自动换行, 类似的还有VBox, 垂直布局
HBox hBox = new HBox(button1, button2, button3);
hBox.setStyle("-fx-background-color: #cc9393;-fx-pref-width: 300px;-fx-pref-height: 300px;");
// 设置组件之间的间距(所有组件)
hBox.setSpacing(10);
// 设置单个组件的外边距
HBox.setMargin(button1, new Insets(10,0,10,0));
// 对齐方式
hBox.setAlignment(Pos.CENTER);
// 设置内边距
// hBox.setPadding(new Insets(10));

// 定位布局(绝对定位)
AnchorPane anchorPane = new AnchorPane(hBox);
anchorPane.setStyle("-fx-background-color: #b6d2af;");
```

##### ③`Border`布局

```java
VBox vBoxLeft = new VBox();
vBoxLeft.setStyle("-fx-background-color: #d99a9a;-fx-pref-width: 100px;");
HBox hBoxTop = new HBox();
hBoxTop.setStyle("-fx-background-color: #d2ae62;-fx-pref-height: 100px;");
VBox vBoxRight = new VBox();
vBoxRight.setStyle("-fx-background-color: #b9d25f;-fx-pref-width: 100px;");
HBox hBoxBottom = new HBox();
hBoxBottom.setStyle("-fx-background-color: #51c969;-fx-pref-height: 100px;");
HBox hBoxCenter = new HBox();
hBoxCenter.setStyle("-fx-background-color: #609cce;");
// 方位布局
BorderPane borderPane = new BorderPane();
borderPane.setLeft(vBoxLeft);
borderPane.setTop(hBoxTop);
borderPane.setRight(vBoxRight);
borderPane.setBottom(hBoxBottom);
borderPane.setCenter(hBoxCenter);
borderPane.setPadding(new Insets(10));
/*
根据情况可以设置居中
BorderPane.setAlignment(vBoxLeft, Pos.CENTER);
BorderPane.setAlignment(hBoxTop, Pos.CENTER);
BorderPane.setAlignment(vBoxRight, Pos.CENTER);
BorderPane.setAlignment(hBoxBottom, Pos.CENTER);
BorderPane.setAlignment(hBoxCenter, Pos.CENTER);
*/
```

##### ④流式布局

```java
// 会自动换行
FlowPane flowPane = new FlowPane(Utils.getButtonList(9));
//        for (Node child : flowPane.getChildren()) {
//            FlowPane.setMargin(child, new Insets(10));
//        }
// 设置水平间距
flowPane.setHgap(10);
// 设置垂直间距
flowPane.setVgap(10);
// 设置布局方向, 默认为水平方向
flowPane.setOrientation(Orientation.VERTICAL);
flowPane.setAlignment(Pos.CENTER);
```

##### ⑤网格布局

```java
// 网格布局, 不会自动换行, 位置固定
GridPane gridPane = new GridPane();
// 设置组件在第几行第几列
Button[] buttonList = Utils.getButtonList(9);
for (int i = 0; i < 9; i++) {
    gridPane.add(buttonList[i], new Random().nextInt(9), new Random().nextInt(9));
}
gridPane.setHgap(10);
gridPane.setVgap(10);
gridPane.setAlignment(Pos.CENTER);
```

##### ⑥`Stack`布局

```java
// 类似于栈, 默认居中
StackPane stackPane = new StackPane(Utils.getButtonList(9));
stackPane.setPadding(new Insets(10));
```

##### ⑦`TextFlow`

```java
// 可以自动换行
TextFlow textFlow = new TextFlow(Utils.getTextList(9));
textFlow.setTextAlignment(TextAlignment.LEFT);
// 行间距
textFlow.setLineSpacing(20);
textFlow.widthProperty().addListener(new ChangeListener<Number>() {
    @Override
    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
        System.out.println(newValue);
    }
});
```

##### ⑧`TilePane`瓦片布局

```java
// 瓦片布局, 部分属性会影响所有组件, 强调整齐
Button[] buttonList = Utils.getButtonList(9);
TilePane tilePane = new TilePane(buttonList);
// 影响所有组件
TilePane.setMargin(buttonList[0], new Insets(20));
buttonList[0].setStyle("-fx-text-fill: #20af22");
```

##### ⑨可分割布局

```java
HBox hBox1 = new HBox(new Button("button1"));
HBox hBox2 = new HBox(new Button("button2"));
HBox hBox3 = new HBox(new Button("button3"));
HBox hBox4 = new HBox(new Button("button4"));
SplitPane splitPane = new SplitPane(hBox1, hBox2, hBox3, hBox4);
splitPane.setStyle("-fx-pref-width: 500px;");
// 设置方向
splitPane.setOrientation(Orientation.HORIZONTAL);
// 设置所占区域大小
splitPane.setDividerPositions(0.1, 0.3, 0.6, 1);
```

#### 4、对话框

##### ①`DialogPane`实现（不推荐）

```java
Button button = new Button("button");
BorderPane borderPane = new BorderPane();
borderPane.setCenter(button);
Scene scene = new Scene(borderPane);
button.setOnAction(event -> {
    Stage dialogStage = new Stage(StageStyle.UTILITY);
    DialogPane dialogPane = new DialogPane();
    dialogPane.setHeaderText("标题");
    dialogPane.setContentText("主体");
    // 如果设置了setContent, 则setContentText不会生效, header同理
    // dialogPane.setContent(new Button("di2uhndq"));
    // 添加按钮
    dialogPane.getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
    // 给按钮添加事件
    Button applyButton = (Button)dialogPane.lookupButton(ButtonType.OK);
    applyButton.setOnAction(event1 -> System.out.println("点击了OK按钮"));
    ((Button)dialogPane.lookupButton(ButtonType.CANCEL)).setOnAction(event12 -> dialogStage.close());
    // 添加图像
    dialogPane.setGraphic(Utils.getFaviconPNG());
    // 设置扩展内容, 默认是关闭的
    dialogPane.setExpandableContent(new Text("这是扩展内容"));
    // 设置扩展内容默认打开
    dialogPane.setExpanded(true);

    dialogStage.setScene(new Scene(dialogPane));
    dialogStage.setResizable(false);
    dialogStage.setTitle("弹出窗口");
    dialogStage.initOwner(stage);
    dialogStage.initModality(Modality.WINDOW_MODAL);
    dialogStage.show();
});
```

#### 5、超链接

```java
// 像html中的a标签, 但是不会跳转
Hyperlink hyperlink = new Hyperlink("https://www.baidu.com", new Button("点击跳转"));
hyperlink.setBorder(null);
hyperlink.setOnAction(new EventHandler<ActionEvent>() {
    @Override
    public void handle(ActionEvent event) {
        getHostServices().showDocument(hyperlink.getText());
    }
});
VBox vBox = new VBox(hyperlink);
```

#### 6、菜单

```java
stage.setTitle("JavaFX");
stage.setWidth(400);
stage.setHeight(400);
stage.show();

Menu fileMenu = new Menu("文件(F)");
MenuItem menuItem1 = new MenuItem("新建");
MenuItem menuItem2 = new MenuItem("打开");
// 分割线
SeparatorMenuItem separatorMenuItem = new SeparatorMenuItem();
// 多级菜单
Menu menuItem3 = new Menu("最近(R)");
MenuItem menuItem3_1 = new MenuItem("文件1");
MenuItem menuItem3_2 = new MenuItem("文件2");
menuItem3.getItems().addAll(menuItem3_1, menuItem3_2);
MenuItem menuItem4 = new MenuItem("退出");
fileMenu.getItems().addAll(menuItem1, menuItem2, menuItem3, separatorMenuItem, menuItem4);

Menu editMenu = new Menu("编辑(E)");
// 添加单选框组, 一个组内的单选框才是真正的单选
ToggleGroup toggleGroup = new ToggleGroup();
RadioMenuItem radioMenuItem1 = new RadioMenuItem("单选框1");
RadioMenuItem radioMenuItem2 = new RadioMenuItem("单选框2");
// 默认为选中状态
radioMenuItem2.setSelected(true);
RadioMenuItem radioMenuItem3 = new RadioMenuItem("单选框3");
radioMenuItem1.setToggleGroup(toggleGroup);
radioMenuItem2.setToggleGroup(toggleGroup);
radioMenuItem3.setToggleGroup(toggleGroup);
editMenu.getItems().addAll(radioMenuItem1, radioMenuItem2, radioMenuItem3);
// 给单选菜单添加事件
radioMenuItem1.setOnAction(event -> {
    RadioMenuItem radioMenuItem = (RadioMenuItem) event.getSource();
    System.out.println(radioMenuItem.getText() + radioMenuItem.isSelected());
});

Menu viewMenu = new Menu("视图(V)");
// 添加复选框
CheckMenuItem checkMenuItem1 = new CheckMenuItem("复选框1");
CheckMenuItem checkMenuItem2 = new CheckMenuItem("复选框2");
CheckMenuItem checkMenuItem3 = new CheckMenuItem("复选框3");
// 设置菜单禁用
checkMenuItem3.setDisable(true);
viewMenu.getItems().addAll(checkMenuItem1, checkMenuItem2, checkMenuItem3);

Menu helpMenu = new Menu("帮助(H)");
Button buttonMenuItem = new Button("buttonMenuItem");
// 进度条
ProgressBar progressBar = new ProgressBar(0.8);
HBox hBox = new HBox(Utils.getButtonList(3));
hBox.setStyle("-fx-background-color: #abd9a6;-fx-padding: 5px;");
// 自定义菜单项
CustomMenuItem customMenuItem1 = new CustomMenuItem(buttonMenuItem);
CustomMenuItem customMenuItem2 = new CustomMenuItem(progressBar);
CustomMenuItem customMenuItem3 = new CustomMenuItem(hBox);
helpMenu.getItems().addAll(customMenuItem1, customMenuItem2, customMenuItem3);

MenuBar menuBar = new MenuBar(fileMenu, editMenu, viewMenu, helpMenu);
menuBar.setPrefWidth(stage.getWidth());
// 其他菜单组件
MenuButton menuButton = new MenuButton("menuButton", null, Utils.getMenuItems(3));
SplitMenuButton splitMenuButton = new SplitMenuButton(Utils.getMenuItems(3));
splitMenuButton.setText("splitMenuButton");

AnchorPane anchorPane = new AnchorPane(menuBar, menuButton, splitMenuButton);

// 右键菜单
ContextMenu contextMenu = new ContextMenu(Utils.getMenuItems(3));
// 可以绑定在组件上, 例如按钮
// Button button = new Button("button");
// button.setContextMenu(contextMenu);
// 也可以指定触发的组件
anchorPane.setOnContextMenuRequested(event -> contextMenu.show(anchorPane, event.getScreenX(), event.getScreenY()));

AnchorPane.setTopAnchor(menuButton, 25.0);
AnchorPane.setTopAnchor(splitMenuButton, 50.0);
Scene scene = new Scene(anchorPane);
stage.setScene(scene);

// 自适应宽度
anchorPane.widthProperty().addListener((observable, oldValue, newValue) -> menuBar.setPrefWidth(stage.getWidth()));
// 添加菜单事件
menuItem1.setOnAction(event -> System.out.println("文件>新建"));
menuItem3_2.setOnAction(event -> System.out.println("文件>最近>文件2"));
menuItem3.setOnShowing(event -> {
    menuItem3.getItems().add(new MenuItem("文件3"));
});
// 设置菜单的快捷方式
menuItem1.setAccelerator(KeyCombination.valueOf("N"));
menuItem2.setAccelerator(KeyCombination.valueOf("O"));
menuItem4.setAccelerator(KeyCombination.valueOf("X"));
```

#### 7、可折叠组件

```java
// 标题窗格
TitledPane titledPane1 = new TitledPane();
titledPane1.setText("titledPane1");
VBox hBox = new VBox(Utils.getButtonList(5));
hBox.setSpacing(10);
titledPane1.setContent(hBox);
// 设置展开和收起
titledPane1.setExpanded(true);
titledPane1.setAnimated(true);
// 设置为不可折叠
// titledPane1.setCollapsible(false);
// 设置箭头方向
titledPane1.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);

TitledPane titledPane2 = new TitledPane("titledPane2", new HBox(Utils.getButtonList(6)));
TitledPane titledPane3 = new TitledPane("titledPane3", new VBox(Utils.getButtonList(2)));
// 可折叠组件(手风琴)
Accordion accordion = new Accordion(titledPane1, titledPane2, titledPane3);

// 展开事件
accordion.expandedPaneProperty().addListener((observable, oldValue, newValue) -> {
    // oldValue是收起的TitledPane, newValue是展开的TitledPane
    if (newValue != null) {
        System.out.println(newValue.getText());
    }
    // 删除监听器
    // observable.removeListener(this);
});
```

#### 8、面板切换

```java
Tab tab1 = new Tab("tab1");
tab1.setContent(new HBox(Utils.getButtonList(3)));
// 默认是可关闭的, 设置为不可关闭
tab1.setClosable(false);
Tab tab2 = new Tab("tab2");
tab2.setContent(new VBox(Utils.getButtonList(4)));
Tab tab3 = new Tab("tab3");
ImageView imageView = Utils.getFaviconPNG("favicon.png");
imageView.setFitWidth(15);
imageView.setFitHeight(15);
tab3.setGraphic(imageView);

TabPane tabPane = new TabPane(tab1, tab2, tab3);
// 设置默认选择的tab
tabPane.getSelectionModel().select(1);
// 设置方向
tabPane.setSide(Side.LEFT);
// 图标是否跟着旋转, 默认是false
tabPane.setRotateGraphic(true);
// tab被选中的事件
tabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue.getText()));
AnchorPane anchorPane = new AnchorPane(tabPane);
// 动态添加tab
anchorPane.setOnMouseClicked(event -> tabPane.getTabs().add(new Tab("新的tab")));

```

#### 9、单选框和复选框

```java
// 单选
ToggleGroup toggleGroup = new ToggleGroup();
RadioButton[] radioButtonList = Utils.getRadioButtonList(4);
for (RadioButton radioButton : radioButtonList) {
    radioButton.setToggleGroup(toggleGroup);
}
// RadioButton选择事件
radioButtonList[0].selectedProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue));
// ToggleGroup的选择事件
toggleGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
    RadioButton radioButton = (RadioButton) newValue;
    System.out.println(radioButton.getText());
});
// 设置默认选择方式1
//radioButtonList[2].setSelected(true);
// 设置默认选择方式2
toggleGroup.selectToggle(radioButtonList[1]);
VBox vBox1 = new VBox(radioButtonList);
vBox1.setSpacing(10);

// 多选, 方法基本相同
CheckBox[] checkBoxList = Utils.getCheckBoxList(4);
// 设置默认不确定状态
checkBoxList[0].setIndeterminate(true);
// 设置有不确定状态
checkBoxList[1].setAllowIndeterminate(true);
VBox vBox2 = new VBox(checkBoxList);
vBox2.setSpacing(10);

AnchorPane anchorPane = new AnchorPane(vBox1, vBox2);
// 获取所有复选按钮的状态
anchorPane.setOnMouseClicked(event -> {
    for (Node child : vBox2.getChildren()) {
        CheckBox checkBox = (CheckBox) child;
        // 是否选中状态和是否不确定状态
        System.out.println(checkBox.getText() + ": " + checkBox.isSelected() + ", " + checkBox.isIndeterminate());
    }
});
```

#### 10、下拉列表

##### ①基本用法

```java
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage stage) {
        Student student1 = new Student("张三", 80.0);
        Student student2 = new Student("李四", 90.0);
        Student student3 = new Student("王五", 60.0);
        Student student4 = new Student("赵六", 59.0);
        // 下拉列表, 单选
        ChoiceBox<Student> choiceBox = new ChoiceBox<>();
        choiceBox.getItems().addAll(student1, student2, student3, student4);
        // 这样可以固定大小
        choiceBox.setMinWidth(100);
        choiceBox.setMaxWidth(100);
        choiceBox.setConverter(new StringConverter<>() {
            @Override
            public String toString(Student object) {
                return object == null ? null : object.getName();
            }
            @Override
            public Student fromString(String string) {
                return null;
            }
        });
        // 设置默认值
        choiceBox.setValue(student3);
        choiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue));
        AnchorPane anchorPane = new AnchorPane(choiceBox);
        Scene scene = new Scene(anchorPane);
        stage.setScene(scene);
        // 布局
        stage.setTitle("关键字查询和文本排序");
        stage.setWidth(600);
        stage.setHeight(400);
        stage.show();
    }
}

@Data
class Student {
    private static int ID_GENERATOR = 0;
    private Integer id;
    private String name;
    private Double score;
    public Student(String name, Double score) {
        this.id = ID_GENERATOR++;
        this.name = name;
        this.score = score;
    }
}
```

##### ②组合使用

```java
ObservableList<String> list1 = FXCollections.observableArrayList("水果", "蔬菜", "其他");
ObservableList<String> list2_1 = FXCollections.observableArrayList("苹果", "菠萝");
ObservableList<String> list2_2 = FXCollections.observableArrayList("萝卜", "辣椒");
ObservableList<String> list2_3 = FXCollections.observableArrayList("1", "2", "3", "4", "5");
// 下拉列表, 单选
ChoiceBox<String> choiceBox1 = new ChoiceBox<>();
choiceBox1.setItems(list1);
ChoiceBox<String> choiceBox2 = new ChoiceBox<>();
choiceBox1.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
    switch (newValue) {
        case "水果": {
            choiceBox2.setItems(list2_1);
            break;
        }
        case "蔬菜": {
            choiceBox2.setItems(list2_2);
            break;
        }
        default: {
            list2_3.sort(Comparator.reverseOrder());
            choiceBox2.setItems(list2_3);
        }
    }
    choiceBox2.show();
});
```

##### ③`ComboBox`组合下拉列表

```java
final HashSet<String> set = new HashSet<>();
Student student1 = new Student("张三", 80.0);
Student student2 = new Student("李四", 90.0);
Student student3 = new Student("王五", 60.0);
Student student4 = new Student("赵六", 59.0);
Student student5 = new Student("赵七", 89.0);
ComboBox<Student> comboBox = new ComboBox<>();
comboBox.setConverter(new StringConverter<>() {
    @Override
    public String toString(Student object) {
        if (object != null) {
            if (!set.contains(object.getName())) {
                comboBox.getItems().add(object);
                students.add(object);
                set.add(object.getName());
            }
        }
        return object == null ? null : object.getName();
    }

    @Override
    public Student fromString(String string) {
        // 此处获取输入的值
        StringTokenizer tokenizer = new StringTokenizer(string, " ");
        Student student = null;
        if (tokenizer.countTokens() == 2) {
            student = new Student(tokenizer.nextToken(), Double.valueOf(tokenizer.nextToken()));
        }
        return student;
    }
});
comboBox.editorProperty().get().textProperty().addListener((observable, oldValue, newValue) -> {
    if (!newValue.isEmpty()) {
        List<Student> filteredStudents = students.stream()
            .filter(student -> student.getName().startsWith(newValue))
            .collect(Collectors.toList());
        comboBox.setItems(FXCollections.observableArrayList(filteredStudents));
    } else {
        comboBox.setItems(students);
    }
    comboBox.show();
});
comboBox.getItems().addAll(student1, student2, student3, student4, student5);
students = comboBox.getItems();
set.addAll(Arrays.asList(student1.getName(), student2.getName(), student3.getName(), student4.getName(), student5.getName()));
// 设置可编辑
comboBox.setEditable(true);
// 设置提示
comboBox.setPromptText("请输入XXX");
// 设置失去焦点
comboBox.setFocusTraversable(false);
// 设置占位符, 没有元素时显示
comboBox.setPlaceholder(new Text("没有元素时显示"));
// 设置默认显示多少行
// comboBox.setVisibleRowCount(3);
comboBox.focusedProperty().addListener((observable, oldValue, newValue) -> {
    if (newValue) {
        // 防止切换窗口后不显示
        Platform.runLater(comboBox::show);
    } else {
        comboBox.hide();
    }
});
comboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue));
        
```

##### ④自定义选项样式

```java
// 控制当前选中的文本格式
comboBox.setConverter(new StringConverter<>() {
    @Override
    public String toString(Student object) {
        return object == null ? null : object.getName() + "-" + object.getScore();
    }

    @Override
    public Student fromString(String string) {
        return null;
    }
});
// 设置每个选项的样式
comboBox.setCellFactory(new Callback<>() {
    @Override
    public ListCell<Student> call(ListView<Student> param) {
        return new ListCell<>() {
            @Override
            protected void updateItem(Student item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty) {
                    //this.setGraphic(new Label(item.getName()));
                    this.setText(item.getName());
                }
            }
        };
    }
});
```

#### 11、颜色/日期选择器

```java
TextField textField = new TextField();
// 颜色选择器, 不指定默认选择白色
ColorPicker colorPicker = new ColorPicker(Color.RED);
colorPicker.valueProperty().addListener((observable, oldValue, newValue) -> {
    System.out.println(newValue);
    textField.setStyle("-fx-background-color: " + newValue.toString().replace("0x", "#"));
});

// 日期选择器
DatePicker datePicker = new DatePicker(LocalDate.now());
datePicker.setEditable(false);
//        datePicker.setDayCellFactory(new Callback<DatePicker, DateCell>() {
//            @Override
//            public DateCell call(DatePicker param) {
//                return null;
//            }
//        });
datePicker.valueProperty().addListener((observable, oldValue, newValue) -> textField.setText(newValue.toString()));

```

#### 12、分页组件

```java
// 分页组件
Pagination pagination = new Pagination();
// 设置总页数
pagination.setPageCount(10);
// 设置当前页面最大页数
pagination.setMaxPageIndicatorCount(3);
// 设置当前页数, index从0开始
pagination.setCurrentPageIndex(4);
Button[] buttonList = Utils.getButtonList(10);
// 设置样式
// pagination.getStyleClass().add(Pagination.STYLE_CLASS_BULLET);
pagination.currentPageIndexProperty().addListener((observable, oldValue, newValue) -> {
    buttonList[newValue.intValue()].setText(newValue.toString());
    System.out.println(newValue);
});

pagination.setPageFactory(param -> {
    // 可以根据不同页数返回不同Node
    return buttonList[param];
});
```

#### 13、滑块

```java
TextField textField = new TextField();
Slider slider = new Slider(0, 100, 20);
slider.setStyle("-fx-pref-width: 300px;");
// 设置增量
slider.setBlockIncrement(10);
// 显示刻度
slider.setShowTickMarks(true);
slider.setShowTickLabels(true);
// 设置刻度增量
slider.setMajorTickUnit(10);
// 设置滑块一定会对齐到刻度
slider.setSnapToTicks(true);
// 设置方向
slider.setOrientation(Orientation.HORIZONTAL);
slider.valueProperty().addListener((observable, oldValue, newValue) -> textField.setText(String.valueOf(newValue)));
slider.setLabelFormatter(new StringConverter<>() {
    @Override
    public String toString(Double object) {
        return object + "m";
    }

    @Override
    public Double fromString(String string) {
        return null;
    }
});
```

#### 14、进度条

```java
// 矩形进度条
ProgressBar progressBar = new ProgressBar();
// 圆形进度条
ProgressIndicator progressIndicator = new ProgressIndicator();
// 失效?
// progressIndicator.setStyle("-fx-pref-width: 100px; -fx-pref-height: 100px;");
// 有效
progressIndicator.setMinSize(100,100);
// 设置进度
// progressBar.setProgress(0.5);
progressBar.setStyle("-fx-pref-width: 300px;-fx-pref-height: 50px;");
// 默认为-1, 有一个动画效果
// progressBar.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
ss = new ScheduledService<>() {
    double i = 0;
    @Override
    protected Task<Double> createTask() {
        return new Task<>() {
            @Override
            protected void updateValue(Double value) {
                progressBar.setProgress(i += 0.1);
                progressIndicator.setProgress(i);
                if(i>=1){
                    ss.cancel();
                    System.out.println("任务结束");
                }
            }

            @Override
            protected Double call() {
                return null;
            }
        };
    }
};
ss.setPeriod(Duration.seconds(1));
ss.start();
```

#### 15、微调控制项

```java
Student student1 = new Student("张三", 80.0);
Student student2 = new Student("李四", 90.0);
Student student3 = new Student("王五", 60.0);
Student student4 = new Student("赵六", 59.0);
Student student5 = new Student("赵七", 89.0);
Spinner<Object> os1 = new Spinner<>(0, 10, 5);
os1.valueProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue));
// 设置样式
os1.getStyleClass().add(Spinner.STYLE_CLASS_SPLIT_ARROWS_HORIZONTAL);
Spinner<Object> os2 = new Spinner<>(0, 10, 0, 2);
os2.setEditable(true);
ObservableList<String> strs = FXCollections.observableArrayList("B", "C", "A", "D", "E", "F");
Spinner<String> os3 = new Spinner<>(strs);
ObservableList<Student> students = FXCollections.observableArrayList(student1, student2, student3, student4, student5);
Spinner<Student> os4 = getStudentSpinner(students);

private static Spinner<Student> getStudentSpinner(ObservableList<Student> students) {
    Spinner<Student> os4 = new Spinner<>();
    //        MySpinnerValueFactory<Student> mySpinnerValueFactory = new MySpinnerValueFactory<>(students);
    //        mySpinnerValueFactory.setConverter(new StringConverter<>() {
    //            @Override
    //            public String toString(Student object) {
    //                return object == null ? null : object.getName();
    //            }
    //
    //            @Override
    //            public Student fromString(String string) {
    //                return null;
    //            }
    //        });
    SpinnerValueFactory.ListSpinnerValueFactory<Student> slvf = new SpinnerValueFactory.ListSpinnerValueFactory<>(students);
    slvf.setConverter(new StringConverter<>() {
        @Override
        public String toString(Student object) {
            System.out.println(object);
            return object == null ? null : object.getName();
        }

        @Override
        public Student fromString(String string) {
            return null;
        }
    });
    os4.setValueFactory(slvf);
    return os4;
}

class MySpinnerValueFactory<T> extends SpinnerValueFactory<T> {
    private final ObservableList<T> observableList;
    private int index = -1;

    public MySpinnerValueFactory(ObservableList<T> observableList) {
        this.observableList = observableList;
    }

    @Override
    public void decrement(int steps) {
        System.out.println("decrement " + steps);
        if (index > 0) {
            index -= steps;
        } else {
            index = observableList.size() - 1;
        }
        this.setValue(observableList.get(index));
    }

    @Override
    public void increment(int steps) {
        System.out.println("increment = " + steps + ", index = " + index);
        if (index < observableList.size() - 1) {
            index += steps;
        } else {
            index = 0;
        }
        this.setValue(observableList.get(index));
    }
}
```

#### 16、滚动条和滚动面板

```java
// 布局
stage.setTitle("JavaFX");
stage.setWidth(600);
stage.setHeight(400);
stage.show();

VBox vBox = new VBox(10, Utils.getButtonList(10));
// 滚动条
ScrollBar scrollBar = new ScrollBar();
// 设置方向
scrollBar.setOrientation(Orientation.VERTICAL);
// 设置滚动条按钮的长度
scrollBar.setVisibleAmount(50);

// 设置当前位置
scrollBar.setValue(20);
// 默认范围是0-100, 可更改
scrollBar.setMax(200);
scrollBar.valueProperty().addListener((observable, oldValue, newValue) -> {
System.out.println(newValue);
vBox.setLayoutY(newValue.doubleValue());
});
```

```java
HBox hBox = new HBox(10, Utils.getButtonList(10));
VBox vBox = new VBox(10, Utils.getButtonList(10));
vBox.getChildren().add(hBox);

// 滚动面板, 其它方法和滚动条类似
ScrollPane scrollPane = new ScrollPane(vBox);
scrollPane.setPrefWidth(200);
scrollPane.setPrefHeight(300);
```

##### ①分隔符

```java
// 分隔符
Separator separator = new Separator();
separator.setStyle("-fx-pref-width: 300px;-fx-pref-height: 26px;");
separator.setHalignment(HPos.CENTER);
separator.setValignment(VPos.CENTER);
HBox hBox = new HBox(10, button1, separator, button2);
```

#### 17、文件/文件夹选择器

```java
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {

        TextArea textArea = new TextArea();
        textArea.setWrapText(true);
        Button oneFileSelectButton = getOneFileSelectButton(stage);
        Button multipleFileSelectButton = getMultipleFileSelectButton(stage);
        Button openFileButton = getOpenFileButton(stage, textArea);
        Button fileSaveButton = getFileSaveButton(stage, textArea);
        Button catalogueSelectButton = getCatalogueSelectButton(stage);

        VBox vBox = new VBox(10, oneFileSelectButton, multipleFileSelectButton, openFileButton, fileSaveButton, catalogueSelectButton, textArea);
        vBox.setPadding(new Insets(10));
        HBox hBox = new HBox(10, vBox);
        AnchorPane anchorPane = new AnchorPane(hBox);
        Scene scene = new Scene(anchorPane);
        stage.setScene(scene);
        // 布局
        stage.setTitle("JavaFX");
        stage.setWidth(600);
        stage.setHeight(400);
        stage.show();
    }

    private static void listAllFiles(File file, String level) {
        if (file != null) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File file1 : files) {
                    System.out.println(level + file1.getName());
                    if (file1.isDirectory()) {
                        listAllFiles(file1, "--" + level);
                    }
                }
            }
        }
    }

    private static Button getCatalogueSelectButton(Stage stage) {
        Button catalogueSelectButton = new Button("选择文件夹");
        catalogueSelectButton.setOnAction(event -> {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            // 获取当前工作目录
            String userDir = System.getProperty("user.dir");
            directoryChooser.setInitialDirectory(new File(userDir));
            File file = directoryChooser.showDialog(stage);
            if (file != null) {
                listAllFiles(file, "");
            }
        });
        return catalogueSelectButton;
    }

    private static Button getFileSaveButton(Stage stage, TextArea textArea) {
        Button fileSaveButton = new Button("保存文件");
        fileSaveButton.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            // 设置默认保存的文件名称
            fileChooser.setInitialFileName("保存的文件");
            // 设置文件类型
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("文本类型", "*.txt"));
            // 保存文件
            File file = fileChooser.showSaveDialog(stage);
            if (file == null) {
                return;
            }
            try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file))) {
                outputStreamWriter.write(textArea.getText());
                outputStreamWriter.flush();
                System.out.println("文件 " + file.getName() + " 保存成功!");
            } catch (IOException ignored) {
            }
        });
        return fileSaveButton;
    }

    private static Button getOpenFileButton(Stage stage, TextArea textArea) {
        Button openFileButton = new Button("打开文件");
        openFileButton.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("文本文件", "*.txt"));
            File file = fileChooser.showOpenDialog(stage);
            if (file == null) {
                return;
            }
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
                String line;
                textArea.clear();
                while ((line = bufferedReader.readLine()) != null) {
                    textArea.appendText(line + "\n");
                }
            } catch (IOException e) {
                System.out.println("读取文件失败。" + e.getMessage());
            }

        });

        return openFileButton;
    }

    private static Button getMultipleFileSelectButton(Stage stage) {
        Button multipleFileSelectButton = new Button("选择多个文件");
        multipleFileSelectButton.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            // 可选择多个文件, 如果没选返回null
            List<File> files = fileChooser.showOpenMultipleDialog(stage);
            for (File file : files) {
                System.out.println(file);
            }
        });
        return multipleFileSelectButton;
    }

    private static Button getOneFileSelectButton(Stage stage) {
        Button oneFileSelectButton = new Button("选择一个文件");
        oneFileSelectButton.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            // 设置标题
            fileChooser.setTitle("选择一个文件");
            // 设置初始目录
            fileChooser.setInitialDirectory(new File("D:"));
            // 设置文件类型
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("图片类型", "*.jpg", "*.png"),
                    new FileChooser.ExtensionFilter("文本类型", "*.java", "*.xml", "*.txt"),
                    new FileChooser.ExtensionFilter("所有类型", "*.*"));
            // 设置显示在哪一个窗口上, 返回值是所选文件的绝对路径
            // 如果没选返回null
            File file = fileChooser.showOpenDialog(stage);
            System.out.println(file);
        });
        return oneFileSelectButton;
    }
}
```

#### 18、提示工具组件

##### ①基础用法

```java
Button button = new Button("button");
// 提示工具
Tooltip tooltip = new Tooltip("这是提示内容··········32213`13··················");
tooltip.setFont(new Font(18));
tooltip.setPrefWidth(200);
tooltip.setPrefHeight(200);
// 超出宽度内容效果
tooltip.setTextOverrun(OverrunStyle.ELLIPSIS);
// 设置自动换行
tooltip.setWrapText(true);
// 设置对齐方式
tooltip.setTextAlignment(TextAlignment.CENTER);
// 设置显示位置
tooltip.setAnchorLocation(PopupWindow.AnchorLocation.WINDOW_TOP_LEFT);
// 取消关联
// Tooltip.uninstall(button, tooltip);
// 建立关联
// Tooltip.install(button, tooltip);
// 设置自动隐藏
// tooltip.setAutoHide(true);
// 显示在哪一个窗口上
// tooltip.show(stage);

tooltip.setOnShowing(event -> System.out.println("正在显示"));
tooltip.setOnShown(event -> System.out.println("显示"));
tooltip.setOnHiding(event -> System.out.println("正在隐藏"));
tooltip.setOnHidden(event -> System.out.println("隐藏"));
button.setTooltip(tooltip);
```

##### ②自定义提示组件

```java
Button button = new Button("button");
Tooltip tooltip = new Tooltip();
tooltip.setPrefWidth(200);
tooltip.setPrefHeight(200);
// 不显示边框
tooltip.setStyle("-fx-background-color: #ffffff00;");
Button button1 = new Button("button");
HBox hBox = new HBox(10, button1);
hBox.setAlignment(Pos.CENTER);
hBox.setStyle("-fx-pref-width: 200px;-fx-pref-height: 200px;");
hBox.setBackground(new Background(new BackgroundFill(Paint.valueOf("#a7ce9e"), new CornerRadii(20), Insets.EMPTY)));
tooltip.setGraphic(hBox);
button.setTooltip(tooltip);
```

#### 19、图片组件

##### ①`Image`

```java
String path = "D:/表情包.jpeg";
// FileInputStream fileInputStream = new FileInputStream(path);
// 保持宽高比, 异步加载等参数
// 如果直接填绝对路径, 需要写成 file:/D:xxx 的格式, 相对路径则不需要
// Image image = new Image("file:/" + path, 100, 100, true, true, true);
// 加载网络图片
Image image = new Image("https://lmg.jj20.com/up/allimg/4k/s/02/2109250006343S5-0-lp.jpg", 600, 600, true, true, true);
ImageView imageView = new ImageView(image);
// 获取图片加载进度(异步加载时有效)
image.progressProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue));
image.errorProperty().addListener((observable, oldValue, newValue) -> System.out.println("是否加载出错: " + newValue));
image.exceptionProperty().addListener((observable, oldValue, newValue) -> System.out.println("异常信息: " + newValue.getMessage()));
```

##### ②`ImageView`

```java
String path = "file:/D:\\dev\\ideaworkspace\\JavaFXTest\\src\\main\\resources\\表情包.jpeg";
Image image = new Image(path);
ImageView imageView = new ImageView(image);
// 保持宽高比
// imageView.setPreserveRatio(true);
// 设置较好的压缩质量
imageView.setSmooth(true);
imageView.setFitHeight(300);
imageView.setFitWidth(300);
System.out.println(imageView.getFitHeight());
System.out.println(imageView.getFitWidth());
// 获得实际宽度
System.out.println(imageView.prefWidth(-1));
// 添加圆角效果
Rectangle rectangle = new Rectangle(imageView.prefWidth(-1), imageView.prefHeight(-1));
double radius = 50;
rectangle.setArcWidth(radius);
rectangle.setArcHeight(radius);
// imageView.setClip(rectangle);
// 鼠标拖动事件
imageView.setOnMouseDragged(event -> {
    // 显示图片的一部分
    Rectangle2D rectangle2D = new Rectangle2D(event.getX(), event.getY(), 300, 300);
    imageView.setViewport(rectangle2D);
});
```

##### ③解析图片

```java
String path = "file:/D:\\dev\\ideaworkspace\\JavaFXTest\\src\\main\\resources\\获取颜色.png";
Image image = new Image(path);
int imageWidth = (int) image.getWidth();
int imageHeight = (int) image.getHeight();
// 获取该图片信息
PixelReader pixelReader = image.getPixelReader();
// 获取指定位置的argb值, 每个值占八位
// 纯红色
printARGB(pixelReader, 0, 0);
// 纯绿色
printARGB(pixelReader, 0, 1);
// 透明区域的值
printARGB(pixelReader, 1, 1);
// 获取图片格式
System.out.println(pixelReader.getPixelFormat().getType());
System.out.println("--------------------------------");
Color color = pixelReader.getColor(0, 0);
// 使用16进制表示颜色和透明度
System.out.println(color);
// 通过color类获取透明度和各种颜色值, 范围是0-1
System.out.println(color.getOpacity());
System.out.println(color.getRed());
System.out.println(color.getGreen());
System.out.println(color.getBlue());
System.out.println("--------------------------------");
// 获取所有像素的两种实现方式
// 返回一个描述像素布局的类, 依次为blue, green, red, alpha
WritablePixelFormat<ByteBuffer> bytePixelFormat = PixelFormat.getByteBgraInstance();
// 每个像素4个字节
byte[] byteBuffer = new byte[imageWidth * imageHeight * 4];
pixelReader.getPixels(0, 0, imageWidth, imageHeight, bytePixelFormat, byteBuffer, 0, imageWidth * 4);
for (int i = 0; i < byteBuffer.length; i += 4) {
    int blue = (byteBuffer[i] & 0xff);
    int green = (byteBuffer[i + 1] & 0xff);
    int red = (byteBuffer[i + 2] & 0xff);
    int alpha = (byteBuffer[i + 3] & 0xff);
    System.out.println("blue = " + blue + ", green = " + green + ", red = " + red + ", alpha = " + alpha);
}
System.out.println("--------------------------------");
WritablePixelFormat<IntBuffer> intPixelFormat = PixelFormat.getIntArgbPreInstance();
int[] intBuffer = new int[imageWidth * imageHeight];
pixelReader.getPixels(0, 0, imageWidth, imageHeight, intPixelFormat, intBuffer, 0, imageWidth);
for (int argbValue : intBuffer) {
	printARGB(argbValue);
}

private static void printARGB(PixelReader pixelReader, int x, int y) {
    printARGB(pixelReader.getArgb(x, y));
}

private static void printARGB(int argbValue) {
    // 获取透明度
    int alpha = argbValue >> 24 & 0xff;
    // 红色值
    int red = argbValue >> 16 & 0xff;
    // 绿色值
    int green = argbValue >> 8 & 0xff;
    // 蓝色值
    int blue = argbValue & 0xff;
    System.out.println("alpha = " + alpha + ", red = " + red + ", green = " + green + ", blue = " + blue);
}
```

##### ④将像素写入图片中

```java
String path = "file:/D:\\dev\\ideaworkspace\\JavaFXTest\\src\\main\\resources\\表情包.jpeg";
int width = 100, height = 100;
// 直接创建一个新图片
WritableImage writableImage1 = new WritableImage(width, height);
PixelWriter pixelWriter1 = writableImage1.getPixelWriter();
for (int i = 0; i < width; i++) {
	for (int j = 0; j < height; j++) {
		pixelWriter1.setArgb(i, j, getRandomARGB());
		//setColor(i, j, new Color());
	}
}
System.out.println("--------------------------------");
Image image = new Image(path);
width = (int) image.getWidth();
height = (int) image.getHeight();
// 也可以在原图上修改
WritableImage writableImage2 = new WritableImage(image.getPixelReader(), width, height);
PixelWriter pixelWriter2 = writableImage2.getPixelWriter();
for (int i = 0; i < width; i++) {
	pixelWriter2.setColor(i, i, Color.valueOf("#ff0000"));
}
System.out.println("--------------------------------");
// 合并图片
Image image1 = new Image(Utils.getFaviconExternalForm("img1.jpg"));
Image image2 = new Image(Utils.getFaviconExternalForm("img2.jpg"));
width = (int) image2.getWidth();
height = (int) image2.getHeight();
WritableImage writableImage3 = new WritableImage(image2.getPixelReader(), width, height);
PixelWriter pixelWriter3 = writableImage3.getPixelWriter();
PixelReader pixelReader = image1.getPixelReader();
// 方法一
WritablePixelFormat<IntBuffer> pixelFormat = PixelFormat.getIntArgbPreInstance();
// int[] intBuffer = new int[width / 2 * height];
// pixelReader.getPixels(0, 0, width / 2, height, pixelFormat, intBuffer, 0, width / 2);
// pixelWriter3.setPixels(0, 0, width / 2, height, pixelFormat, intBuffer, 0, width / 2);
// 方法二
pixelWriter3.setPixels(0, 0, width / 2, height, pixelReader, 0, 0);
System.out.println("--------------------------------");
ImageView imageView = new ImageView(writableImage3);
imageView.setFitWidth(width);
imageView.setFitHeight(height);
private static Button getSaveButton(Stage stage, Image image) {
    Button button = new Button("保存");
    button.setOnAction(event -> {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setInitialDirectory(new File("D:\\dev\\ideaworkspace\\JavaFXTest\\src\\main\\resources"));
    fileChooser.setInitialFileName("img3.jpg");
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("保存类型", "*.jpg"));
    File file = fileChooser.showSaveDialog(stage);
    if (file != null) {
    	System.out.println(file);
    	// 输出为jpg格式的方式
    	BufferedImage bufferedImage = new BufferedImage((int) image.getWidth(), (int) image.getHeight(), BufferedImage.TYPE_INT_RGB);
    try {
        ImageIO.write(SwingFXUtils.fromFXImage(image, bufferedImage), "jpg", file);
        System.out.println("保存成功");
    } catch (IOException e) {
    	System.out.println("保存失败。" + e.getMessage());
      }
    }
    });
    return button;
}

private static int getRandomARGB() {
    int alpha = getIntRandom(0, 256);
    int red = getIntRandom(0, 256);
    int green = getIntRandom(0, 256);
    int blue = getIntRandom(0, 256);
    return ((blue & 0xFF) << 24) | ((green & 0xFF) << 16) | ((red & 0xFF) << 8) | (alpha & 0xFF);
}

private static int getIntRandom(int min, int max) {
    return new Random().nextInt(max) + min;
}
```

#### 20、剪切板

##### ①获取剪切板内容

```java
Clipboard clipboard = Clipboard.getSystemClipboard();
// 设置快捷键
KeyCombination kc = KeyCodeCombination.valueOf("ctrl+v");
scene.getAccelerators().put(kc, () -> {
    if (clipboard.hasString()) {
        // 获取文本
        label.setText(clipboard.getString());
    }
    if (clipboard.hasImage()) {
        // 获取图片(网络图片可以)
        imageView.setImage(clipboard.getImage());
    }
    if (clipboard.hasFiles()) {
        // 方式一
        Image image = (Image) clipboard.getContent(DataFormat.IMAGE);
        imageView.setImage(image);
        /*方式二
        // 获取文件(直接复制文件可以通过这种方式粘贴)
        List<File> files = clipboard.getFiles();
        try {
            imageView.setImage(new Image(new FileInputStream(files.get(0))));
          } catch (FileNotFoundException ignored) {
        }
        */
    }
});
```

##### ②设置剪切板内容

```java
Clipboard clipboard = Clipboard.getSystemClipboard();
// 给剪切板设置内容
ClipboardContent clipboardContent = new ClipboardContent();
clipboardContent.put(DataFormat.PLAIN_TEXT, "hello word");
clipboardContent.put(DataFormat.IMAGE, new Image(Utils.getImgExternalForm("img1.jpg")));
// 将内容放入剪切板
clipboard.setContent(clipboardContent);
// 清空剪切板
clipboard.clear();
```

#### 21、列表视图

##### ①基本使用

```java
 ListView<String> listView = new ListView<>();
listView.setPlaceholder(new Label("没有数据"));
listView.setPrefWidth(200);
listView.setPrefHeight(200);
// 设置单元格尺寸
listView.setFixedCellSize(50);
// 设置数据, 超出时会自动出现滚动条
ObservableList<String> observableList = FXCollections.observableArrayList();
observableList.addAll("data-a", "data-b", "data-c", "data-d", "data-e", "data-f", "data-g");
listView.setItems(observableList);
// 添加数据
listView.getItems().add("data-h");
// 默认是单选模式, 可设置为单选
listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
// 设置选择哪一个元素
// listView.getSelectionModel().select(0);
// listView.requestFocus();
// 获取选择的项
// listView.getSelectionModel().getSelectedItem();
// 设置为可编辑状态
listView.setEditable(true);
listView.setCellFactory(TextFieldListCell.forListView());
// 设置无法获取焦点
// listView.setFocusTraversable(false);
// 设置焦点模式
// listView.setFocusModel();
listView.getFocusModel().focus(1);

// 选择事件
listView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue));
listView.setOnScrollTo(event -> {
    // 滚动事件, 对scrollTo方法有效
    System.out.println(event.getScrollTarget());
});

Button button = new Button("修改");
button.setOnAction(event -> {
    // 可以直接替换某一个元素
    // observableList.set(0,"aaa");
    // 滚动到某一个元素
    listView.scrollTo(1);
});
```

##### ②设置编辑模式

- 修改是同步对数据源进行修改

###### 普通输入框

```java
listView.setEditable(true);
listView.setCellFactory(TextFieldListCell.forListView(new StringConverter<String>() {
    @Override
    public String toString(String object) {
        return object;
    }

    @Override
    public String fromString(String string) {
        return string + "-new";
    }
}));

listView.setOnEditStart(event -> System.out.println("开始编辑"));
listView.setOnEditCancel(event -> System.out.println("取消编辑"));
listView.setOnEditCommit(event -> {
    System.out.println("提交编辑");
    System.out.println(event.getIndex());
    System.out.println(event.getNewValue());
    observableList.set(event.getIndex(), event.getNewValue());
});


Button button = new Button("修改");
button.setOnAction(event -> {
    listView.edit(2);
});
```

###### 下拉列表

```java
// 下拉列表的编辑方式
listView.setCellFactory(ComboBoxListCell.forListView(new StringConverter<String>() {
    @Override
    public String toString(String object) {
        return object;
    }

    @Override
    public String fromString(String string) {
        return string + "-new";
    }
}, "data-a", "data-b", "data-c", "data-d"));
```

```java
listView.setCellFactory(ChoiceBoxListCell.forListView(new StringConverter<String>() {
    @Override
    public String toString(String object) {
        return object;
    }

    @Override
    public String fromString(String string) {
        // 没有编辑所以不会调用fromString
        return string + "-new";
    }
}, "data-a", "data-b", "data-c", "data-d"));
```

###### 复选框

```java
// 下拉列表的编辑方式
listView.setCellFactory(CheckBoxListCell.forListView(param -> {
    // 默认都没选中, 此处可设置
    if("data-a".equals(param)){
        return new SimpleBooleanProperty(true);
    }
    return new SimpleBooleanProperty(false);
}, new StringConverter<>() {
    @Override
    public String toString(String object) {
        return object;
    }

    @Override
    public String fromString(String string) {
        return null;
    }
}));
```

##### ③更新内容

```java
SimpleStringProperty stringProperty1 = new SimpleStringProperty("A");
SimpleStringProperty stringProperty2 = new SimpleStringProperty("B");
SimpleStringProperty stringProperty3 = new SimpleStringProperty("C");
SimpleStringProperty stringProperty4 = new SimpleStringProperty("D");
ObservableList<SimpleStringProperty> observableList = FXCollections.observableArrayList(param -> new Observable[]{param});
observableList.addAll(stringProperty1, stringProperty2, stringProperty3, stringProperty4);
listView.setItems(observableList);

listView.setCellFactory(TextFieldListCell.forListView(new StringConverter<>() {
    @Override
    public String toString(SimpleStringProperty object) {
        return object.get();
    }

    @Override
    public SimpleStringProperty fromString(String string) {
        return null;
    }
}));

observableList.addListener((ListChangeListener<SimpleStringProperty>) c -> {
    while (c.next()) {
        if (c.wasUpdated()) {
            System.out.println("更新数据");
        }
        if (c.wasAdded()) {
            System.out.println("添加数据");
        }
        if (c.wasRemoved()) {
            System.out.println("移除数据");
        }
        if (c.wasReplaced()) {
            System.out.println("替换数据");
        }
        if(c.wasPermutated()){
            System.out.println("排序数据");
        }
    }
});


Button button = new Button("修改");
button.setOnAction(event -> {
    // 直接修改原数据需要刷新, 但影响性能(虽然大部分使用情况下无感知)
    stringProperty1.set("121wqe12q");
    // listView.refresh();
});
```

##### ④自定义选项样式

```java
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        // 布局
        stage.setTitle("JavaFX");
        stage.setWidth(800);
        stage.setHeight(800);
        stage.show();
        ListView<Person> listView = new ListView<>();
        listView.setPlaceholder(new Label("没有数据"));
        listView.setPrefWidth(500);
        listView.setPrefHeight(200);
        // 设置单元格尺寸
        // listView.setFixedCellSize(50);

        // 设置数据, 超出时会自动出现滚动条
        Person person1 = new Person("张三", 12);
        Person person2 = new Person("李四", 12);
        Person person3 = new Person("王五", 12);
        Person person4 = new Person("赵六", 12);

        ObservableList<Person> observableList = FXCollections.observableArrayList();
        observableList.addAll(person1, person2, person3, person4);
        listView.setItems(observableList);

        listView.setEditable(true);
        listView.setCellFactory(new Callback<>() {
            private Person person = null;

            @Override
            public ListCell<Person> call(ListView<Person> param) {
                param.setOnEditStart(event -> person = param.getItems().get(event.getIndex()));

                return new ListCell<>() {
                    @Override
                    protected void updateItem(Person item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty) {
                            this.setGraphic(getCustomNode(false, item, this));
                        }
                    }

                    @Override
                    public void startEdit() {
                        super.startEdit();
                        this.setGraphic(getCustomNode(true, person, this));
                    }

                    @Override
                    public void cancelEdit() {
                        super.cancelEdit();
                        this.setGraphic(getCustomNode(false, person, this));
                    }

                    @Override
                    public void commitEdit(Person newValue) {
                        super.commitEdit(newValue);
                    }
                };
            }
        });

        observableList.addListener((ListChangeListener<Person>) c -> {
            while (c.next()) {
                if (c.wasUpdated()) {
                    System.out.println("更新数据");
                }
                if (c.wasAdded()) {
                    System.out.println("添加数据");
                }
                if (c.wasRemoved()) {
                    System.out.println("移除数据");
                }
                if (c.wasReplaced()) {
                    System.out.println("替换数据");
                }
                if (c.wasPermutated()) {
                    System.out.println("排序数据");
                }
            }
        });

        Button button = new Button("修改");
        button.setOnAction(event -> {
            person1.setName("121wqe12q");
        });

        VBox vBox = new VBox(20, listView, button);
        vBox.setPadding(new Insets(20));
        AnchorPane anchorPane = new AnchorPane(vBox);
        Scene scene = new Scene(anchorPane);
        stage.setScene(scene);
    }


    private static Node getCustomNode(boolean isEdited, Person person, ListCell<Person> listCell) {
        ImageView imageView = new ImageView(Utils.getImgExternalForm("favicon.png"));
        imageView.setPreserveRatio(true);
        imageView.setFitHeight(30);
        Button button = new Button(person.getName() + "button");
        Node name, age;
        if (isEdited) {
            name = new TextField(person.getName());
            age = new TextField(person.getAge());
            name.setOnKeyPressed(event -> {
                TextField textField1 = (TextField) name;
                TextField textField2 = (TextField) age;
                String newName = textField1.getText();
                String newAge = textField2.getText();
                if (event.getCode() == KeyCode.ENTER) {
                    if (!newName.isBlank()) {
                        person.setName(newName);
                        person.setAge(Integer.parseInt(newAge));
                    }
                    listCell.commitEdit(person);
                }
            });
        } else {
            name = new Label(person.getName());
            age = new Label(person.getAge());
        }
        HBox hBox = new HBox(10, imageView, button, name, age);
        hBox.setAlignment(Pos.CENTER_LEFT);
        return hBox;
    }
}
```

##### ⑤排序和查找

```java
TextField textField = new TextField();
textField.textProperty().addListener((observable, oldValue, newValue) -> listView.setItems(observableList.filtered(person -> person.getName().startsWith(newValue) || person.getName().isBlank())));
Button sortButton = new Button("排序");
sortButton.setOnAction(event -> {
    // 按照名称排序, 最好不要直接修改源数据
    // listView.setItems(observableList.sorted(Comparator.comparing(Person::getName)));
    // 按照年龄排序
    listView.setItems(observableList.sorted(Comparator.comparingInt(Person::getAge)));
});
```

##### ⑥拖拽排序和鼠标滚轮事件

###### 鼠标指针所在位置元素特殊显示

```java
listView.setCellFactory(new Callback<>() {
    @Override
    public ListCell<Person> call(ListView<Person> param) {
        Label label = new Label();
        ListCell<Person> listCell = new ListCell<>() {
            @Override
            protected void updateItem(Person item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty && item != null) {
                    label.setText(item.getName() + " - " + item.getAge());
                    label.setPrefHeight(20);
                    label.setFont(new Font(15));
                    this.setGraphic(label);
                }
            }
        };
        // 鼠标悬停事件
        listCell.hoverProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                label.setPrefHeight(30);
                label.setFont(new Font(22));
                // param.getFocusModel().focus(listCell.getIndex());
                listCell.setBorder(new Border(
                    new BorderStroke(
                        Color.RED,
                        BorderStrokeStyle.SOLID,
                        CornerRadii.EMPTY,
                        new BorderWidths(1),
                        Insets.EMPTY
                    )
                ));
            } else {
                label.setPrefHeight(20);
                label.setFont(new Font(15));
                listCell.setBorder(null);
            }
        });
        // listCell.setOnScroll(event -> System.out.println(event.getDeltaX() + " " + event.getDeltaY()));
        return listCell;
    }
});
```

###### 拖拽排序

```java
DataFormat dataFormat = new DataFormat("beans/person");
listView.setCellFactory(new Callback<>() {
    @Override
    public ListCell<Person> call(ListView<Person> param) {
        Label label = new Label();
        Label text = new Label();

        ListCell<Person> listCell = new ListCell<>() {
            @Override
            protected void updateItem(Person item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty && item != null) {
                    label.setText(item.getName() + " - " + item.getAge());
                    label.setPrefHeight(20);
                    label.setFont(new Font(15));
                    this.setGraphic(label);
                }
            }
        };
        listCell.setOnDragDetected(event -> {
            Dragboard dragboard = listCell.startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            content.put(dataFormat, listCell.getItem());
            content.put(DataFormat.PLAIN_TEXT, String.valueOf(listCell.getIndex()));
            dragboard.setContent(content);
            // 设置拖拽图标
            anchorPane.getChildren().add(text);
            text.setText(listCell.getItem().toString());
            text.setStyle("-fx-background-color: red;");
            text.setPrefWidth(param.getPrefWidth());
            WritableImage writableImage = new WritableImage((int) text.getPrefWidth(), 20);
            text.snapshot(new SnapshotParameters(), writableImage);
            anchorPane.getChildren().remove(text);
            dragboard.setDragView(writableImage);
        });

        // 拖拽进入
        listCell.setOnDragEntered(event -> {
            label.setPrefHeight(30);
            label.setFont(new Font(22));
            param.getFocusModel().focus(listCell.getIndex());
        });

        // 拖拽退出
        listCell.setOnDragExited(event -> {
            label.setPrefHeight(20);
            label.setFont(new Font(15));
        });

        listCell.setOnDragOver(event -> event.acceptTransferModes(TransferMode.MOVE));

        listCell.setOnDragDropped(event -> {
            Dragboard dragboard = event.getDragboard();
            Person person = (Person) dragboard.getContent(dataFormat);
            int index = Integer.parseInt(dragboard.getString());
            int newIndex = Math.min(listCell.getIndex(), param.getItems().size() - 1);
            // 插入到新的位置
            param.getItems().remove(index);
            param.getItems().add(newIndex, person);
            // 在新位置获取焦点
            param.getSelectionModel().select(newIndex);
        });
        return listCell;
    }
});
```

#### 22、弹窗组件

##### ①`Dialog`

```java
Button button = new Button("点击弹出对话框");
button.setOnAction(event -> {
    Dialog<ButtonType> dialog = new Dialog<>();
    dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CLOSE);
    Button closeButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
    Button okButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);

    closeButton.setOnAction(event13 -> System.out.println("关闭"));

    okButton.setOnAction(event12 -> System.out.println("ok"));
    dialog.setTitle("自定义标题");
    dialog.setHeaderText("标题");
    dialog.setContentText("主要内容");
    dialog.setGraphic(new Button("图标"));
    // 设置宽高
    dialog.getDialogPane().setPrefSize(300, 400);
    /*
            使用这种方式要将show()方法放在前面
            dialog.setWidth(300);
            dialog.setHeight(400);
            */
    // 设置弹出位置
    // dialog.setX(100);
    // dialog.setY(100);
    dialog.setOnCloseRequest(event1 -> System.out.println("弹窗关闭事件"));
    dialog.setResultConverter(param -> {
        System.out.println(param);
        return param;
    });
    Optional<ButtonType> buttonType = dialog.showAndWait();
    buttonType.ifPresent(buttonType1 -> {
        if (buttonType1.equals(ButtonType.CLOSE)) {
            dialog.setContentText("1111111111111");
        } else if (buttonType1.equals(ButtonType.OK)) {
            dialog.setContentText("22222222222222");
        }
    });
    dialog.show();
});
```

##### ②`Alert`

```java
Button button = new Button("点击弹出对话框");
button.setOnAction(event -> {
    // 需要定义弹窗类型
    Alert alert = new Alert(Alert.AlertType.WARNING);
    alert.setTitle("窗体标题");
    alert.setHeaderText("内容标题");
    alert.setContentText("主要内容");
    Button okButton = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
    okButton.setOnAction(event1 -> System.out.println("ok"));
    // 删除按钮
    // alert.getDialogPane().getButtonTypes().remove(0);
    alert.initModality(Modality.APPLICATION_MODAL);
    // 非阻塞
    alert.show();
    // 阻塞
    // alert.showAndWait();
});
```

##### ③选择弹窗

```java
button.setOnAction(event -> {
    ObservableList<String> observableList = FXCollections.observableArrayList();
    observableList.addAll("item-1", "item-2", "item-3");
    ChoiceDialog<String> choiceDialog = new ChoiceDialog<>("item-1", observableList);
    // 获得选择的值
    choiceDialog.selectedItemProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue));
    choiceDialog.show();
});
```

##### ④输出弹窗

```java
button.setOnAction(event -> {
    TextInputDialog inputDialog = new TextInputDialog("这是默认值");
    Button okButton = (Button) inputDialog.getDialogPane().lookupButton(ButtonType.OK);
    okButton.setOnAction(event1 -> System.out.println(inputDialog.getEditor().getText()));
    inputDialog.show();
});
```

##### ⑤自定义弹窗

```java
button.setOnAction(event -> {
    AnchorPane anchorPane = new AnchorPane();
    Scene scene = new Scene(anchorPane);
    Stage stage1 = new Stage();
    stage1.setScene(scene);
    stage1.initOwner(stage);
    stage1.initModality(Modality.WINDOW_MODAL);
    stage1.initStyle(StageStyle.UTILITY);
    stage1.setResizable(false);
    stage1.setAlwaysOnTop(true);
    stage1.setTitle("自定义标题");
    stage1.setWidth(300);
    stage1.setHeight(280);
    stage1.show();
});
```

#### 23、表格视图

##### ①基本使用

```java
Goods goods1 = new Goods("苹果", 3.2, 10);
Goods goods2 = new Goods("梨子", 4.2, 20);
Goods goods3 = new Goods("菠萝", 5.2, 8);
Goods goods4 = new Goods("葡萄", 6.2, 13);
ObservableList<Goods> observableList = FXCollections.observableArrayList();
observableList.addAll(goods1, goods2, goods3, goods4);
TableView<Goods> tableView = new TableView<>(observableList);
// 创建列
TableColumn<Goods, String> nameColumn = new TableColumn<>("名称");
TableColumn<Goods, Number> priceColumn = new TableColumn<>("价格");
TableColumn<Goods, Number> inventoryColumn = new TableColumn<>("库存");
// 给列添加数据, 方式一
nameColumn.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getName()));
priceColumn.setCellValueFactory(param -> new SimpleDoubleProperty(param.getValue().getPrice()));
inventoryColumn.setCellValueFactory(param -> new SimpleIntegerProperty(param.getValue().getInventory()));
// 方式二
//nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
// 添加列
tableView.getColumns().add(nameColumn);
tableView.getColumns().add(priceColumn);
tableView.getColumns().add(inventoryColumn);
Button button = new Button("修改");
button.setOnAction(event -> {
    // tableView.refresh();
    goods1.setName("苹果111");
});
```

##### ②合并列

```java
// 合并列
TableColumn<Goods, Number> group = new TableColumn<>("信息");
group.getColumns().add(priceColumn);
group.getColumns().add(inventoryColumn);
```

##### ③常用方法

```java
// 设置列宽
double columnWidth = tableView.getPrefWidth() / tableView.getColumns().size();
nameColumn.setPrefWidth(columnWidth);
priceColumn.setPrefWidth(columnWidth);
inventoryColumn.setPrefWidth(columnWidth);
// 使列宽不可更改
tableView.setColumnResizePolicy(param -> true);
// 或者
// 设置列宽, 默认效果是TableView.CONSTRAINED_RESIZE_POLICY
// tableView.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);

// 隐藏列
// nameColumn.setVisible(false);
// 显示菜单
tableView.setTableMenuButtonVisible(true);
// 设置滚动
tableView.scrollTo(7);
// 设置单元格尺寸
tableView.setFixedCellSize(30);
// 设置可编辑
tableView.setEditable(true);
nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
priceColumn.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<>() {
    @Override
    public String toString(Number object) {
        return object.toString();
    }

    @Override
    public Number fromString(String string) {
        return Double.valueOf(string);
    }
}));
// 设置选择模式
tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
    // System.out.println(newValue.getName());
    for (Goods selectedItem : tableView.getSelectionModel().getSelectedItems()) {
        // System.out.println(selectedItem);
    }
});
// 设置可以选择单元格
// tableView.getSelectionModel().setCellSelectionEnabled(true);

tableView.getSelectionModel().getSelectedCells().addListener((InvalidationListener) observable -> {
    ObservableList<TablePosition> obs = (ObservableList<TablePosition>) observable;
    for (TablePosition tablePosition : obs) {
        Object cellData = tablePosition.getTableColumn().getCellData(tablePosition.getRow());
        System.out.println("行 = " + tablePosition.getRow() + ", 列 = " + tablePosition.getColumn() + ", 数据 = " + cellData);
    }
});
// 不允许排序
// nameColumn.setSortable(false);
// 设置排序优先级
// tableView.getSortOrder().addAll(nameColumn, priceColumn);
// 设置自定义排序
tableView.setSortPolicy(param -> {
    for (TableColumn<Goods, ?> column : param.getColumns()) {
        if (column.equals(priceColumn)) {
            if (column.getSortType() == TableColumn.SortType.ASCENDING) {
                column.setSortNode(new Label("升序"));
                param.getItems().sort(Comparator.comparingDouble(Goods::getPrice));
            } else {
                column.setSortNode(new Label("降序"));
                param.getItems().sort((o1, o2) -> Double.compare(o2.getPrice(), o1.getPrice()));
            }
        }
    }
    return true;
});
// 设置方向
tableView.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);

Button button = new Button("修改");
button.setOnAction(event -> {
    // 选择某行某列
    // tableView.getSelectionModel().select(2, priceColumn);
    // 同时选择左边单元格
    // tableView.getSelectionModel().selectLeftCell();
    // 主动触发某一种排序
    // priceColumn.setSortType(TableColumn.SortType.DESCENDING);
});
```

##### ④自带单元格样式

```java
// 自定义单元格
tableView.setEditable(true);
// 下拉列表
priceColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(new StringConverter<>() {
    @Override
    public String toString(Number object) {
        return object.toString();
    }

    @Override
    public Number fromString(String string) {
        return Double.valueOf(string);
    }
}, 100.0, 200.3, 300.3));

nameColumn.setCellFactory(ComboBoxTableCell.forTableColumn(new StringConverter<>() {
    @Override
    public String toString(String object) {
        return object;
    }

    @Override
    public String fromString(String string) {
        return string;
    }
}, "1111", "222", "333"));
// 复选框
changeColumn.setCellFactory(CheckBoxTableCell.forTableColumn(changeColumn));
// 进度条
progressColumn.setCellFactory(ProgressBarTableCell.forTableColumn());
MyScheduledService service = new MyScheduledService(goods3.getProgress());
service.setDelay(Duration.ZERO);
service.setPeriod(Duration.seconds(1));
service.valueProperty().addListener((observable, oldValue, newValue) -> {
    if (newValue != null) {
        if (newValue >= 1.0) {
            service.cancel();
            System.out.println("任务取消");
            goods3.setChanged(false);
        } else {
            goods3.setProgress(newValue);
        }
        tableView.refresh();
    }
});
```

##### ⑤自定义单元格样式

```java
inventoryColumn.setCellFactory(new Callback<>() {
    @Override
    public TableCell<Goods, Number> call(TableColumn<Goods, Number> param) {
        return new TableCell<>() {
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty && item != null) {
                    Label label = new Label(item.toString());
                    HBox hBox = new HBox(label);
                    hBox.setStyle("-fx-background-color: #9db486;");
                    hBox.setAlignment(Pos.CENTER);
                    this.setGraphic(hBox);
                }
            }
        };
    }
});
```

```java
inventoryColumn.setCellFactory(new Callback<>() {
    @Override
    public TableCell<Goods, Number> call(TableColumn<Goods, Number> param) {

        return new TableCell<>() {
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                this.setUserData(item);
                if (!empty && item != null) {
                    Label label = new Label(item.toString());
                    HBox hBox = new HBox(label);
                    hBox.setStyle("-fx-background-color: #9db486;");
                    hBox.setAlignment(Pos.CENTER);
                    this.setGraphic(hBox);
                }
            }

            @Override
            public void startEdit() {
                super.startEdit();
                TableCell<Goods, Number> tableCell1 = this;
                TextField textField = new TextField(this.getUserData().toString());
                textField.setOnKeyPressed(event -> {
                    if (event.getCode() == KeyCode.ENTER) {
                        String text = textField.getText();
                        tableCell1.commitEdit(Integer.valueOf(text));
                    }
                });
                this.setGraphic(textField);
            }

            @Override
            public void cancelEdit() {
                super.cancelEdit();
                Label label = new Label(this.getUserData().toString());
                HBox hBox = new HBox(label);
                hBox.setStyle("-fx-background-color: #9db486;");
                hBox.setAlignment(Pos.CENTER);
                this.setGraphic(hBox);
            }

            @Override
            public void commitEdit(Number newValue) {
                super.commitEdit(newValue);
                this.getTableRow().getItem().setInventory(newValue.intValue());
            }
        };
    }
});
```

```java
// 自定义行样式
tableView.setRowFactory(new Callback<>() {
    @Override
    public TableRow<Goods> call(TableView<Goods> param) {

        return new TableRow<>() {
            @Override
            protected void updateItem(Goods item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty && item != null) {
                    // this.setBorder(new Border(
                    //  new BorderStroke(
                    //  Paint.valueOf("#ff1aa1"),
                    //  BorderStrokeStyle.SOLID,
                    //  CornerRadii.EMPTY,
                    //  new BorderWidths(1)
                    //  )
                    //  ));
                    if (item.getInventory() < 20) {
                        this.setStyle("-fx-background-color: #a67272;");
                    }
                    this.setTooltip(new Tooltip(item.toString()));
                }
            }
        };
    }
});
```

#### 24、树组件

##### ①基本使用

```java
TreeView<String> treeView = new TreeView<>();
TreeItem<String> rootTreeItem = new TreeItem<>("中国");
TreeItem<String> treeItem1 = new TreeItem<>("湖北");
TreeItem<String> treeItem1_1 = new TreeItem<>("武汉");
TreeItem<String> treeItem1_2 = new TreeItem<>("黄石");
TreeItem<String> treeItem1_3 = new TreeItem<>("十堰");
TreeItem<String> treeItem2 = new TreeItem<>("广东");
TreeItem<String> treeItem2_1 = new TreeItem<>("深圳");
TreeItem<String> treeItem2_2 = new TreeItem<>("广州");
TreeItem<String> treeItem2_3 = new TreeItem<>("香港");
TreeItem<String> treeItem3 = new TreeItem<>("台湾");
TreeItem<String> treeItem3_1 = new TreeItem<>("台北");
TreeItem<String> treeItem3_2 = new TreeItem<>("新竹");
TreeItem<String> treeItem3_3 = new TreeItem<>("高雄");
// 指定根节点
treeView.setRoot(rootTreeItem);
// 给一级节点添加子节点
treeItem1.getChildren().addAll(treeItem1_1, treeItem1_2, treeItem1_3);
treeItem2.getChildren().addAll(treeItem2_1, treeItem2_2, treeItem2_3);
treeItem3.getChildren().addAll(treeItem3_1, treeItem3_2, treeItem3_3);
// 将一级节点添加到根节点
rootTreeItem.getChildren().addAll(treeItem1, treeItem2, treeItem3);
// 是否隐藏根节点
treeView.setShowRoot(false);
// 设置展开
// rootTreeItem.setExpanded(true);
// 判断是否展开
//System.out.println(rootTreeItem.isExpanded());
// 设置单元格尺寸
treeView.setFixedCellSize(30);
// 更改值
// rootTreeItem.setValue("华夏");
// 是不是叶子结点
//System.out.println(treeItem1.isLeaf());
// 获取前一个节点
//System.out.println(treeItem1_2.previousSibling().getValue());
//System.out.println(treeItem2.nextSibling().getValue());
// 获取父节点
System.out.println(treeItem1_2.getParent().getValue());
// 设置选择模式
treeView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
//treeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue.getValue()));
// 获取展开的节点数
//System.out.println(treeView.getExpandedItemCount());
// 设置可编辑
treeView.setEditable(true);
treeView.setCellFactory(TextFieldTreeCell.forTreeView());

// 父节点会影响所有的子节点
treeItem1.addEventHandler(TreeItem.valueChangedEvent(), (EventHandler<TreeItem.TreeModificationEvent<String>>) event -> {
    System.out.println(event.getNewValue());
    System.out.println(event.getTreeItem().getValue());
});

treeItem2.addEventHandler(TreeItem.branchExpandedEvent(), event -> System.out.println("展开"));
treeItem2.addEventHandler(TreeItem.branchCollapsedEvent(), event -> System.out.println("收起"));

//treeItem3.addEventHandler(TreeItem.childrenModificationEvent(), (EventHandler<TreeItem.TreeModificationEvent<String>>) event -> {
//System.out.println(event.getTreeItem().getValue());
//System.out.println(event.wasAdded());
//for (TreeItem<String> addedChild : event.getAddedChildren()) {
//    System.out.println(addedChild.getValue());
//  }
//});
rootTreeItem.addEventHandler(TreeItem.treeNotificationEvent(), event -> System.out.println(event.getTreeItem().getValue()));
```

##### ②自定义效果

```java
// treeView.setCellFactory(TextFieldTreeCell.forTreeView(new StringConverter<String>() {
//  @Override
//  public String toString(String object) {
//    return object;
//   }
//
//   @Override
//   public String fromString(String string) {
//      return string;
//    }
// }));

// treeView.setCellFactory(ComboBoxTreeCell.forTreeView("A", "B"));
// treeView.setCellFactory(ChoiceBoxTreeCell.forTreeView("C", "D"));

treeView.setCellFactory(CheckBoxTreeCell.forTreeView(param -> {
    // 根节点是0级
    if (treeView.getTreeItemLevel(param) == 1) {
        return new SimpleBooleanProperty(true);
    }
    return new SimpleBooleanProperty(false);
}));
```

```java
treeView.setCellFactory(new Callback<>() {
    @Override
    public TreeCell<String> call(TreeView<String> param) {
        return new TreeCell<>() {
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty) {
                    if (this.getTreeItem().isExpanded()) {
                        ImageView imageView = new ImageView(Utils.getImgExternalForm("favicon.png"));
                        imageView.setPreserveRatio(isResizable());
                        imageView.setFitWidth(15);
                        // 自定义图标
                        this.setDisclosureNode(imageView);
                    }
                    Label label = new Label(item);
                    HBox hBox = new HBox(label);
                    hBox.setStyle("-fx-background-color: #8ed0c5;");
                    this.setGraphic(hBox);
                } else {
                    this.setGraphic(null);
                }
            }

            @Override
            public void startEdit() {
                super.startEdit();
                TreeCell<String> treeCell = this;
                TextField textField = new TextField(this.getItem());
                textField.requestFocus();
                textField.setOnKeyPressed(event -> {
                    if (event.getCode() == KeyCode.ENTER) {
                        treeCell.commitEdit(textField.getText());
                    }
                });
                HBox hBox = new HBox(textField);
                this.setGraphic(hBox);
            }

            @Override
            public void cancelEdit() {
                super.cancelEdit();
                Label label = new Label(this.getItem());
                HBox hBox = new HBox(label);
                hBox.setStyle("-fx-background-color: #8ed0c5;");
                this.setGraphic(hBox);
            }
        };
    }
});
```

#### 25、树形表格视图

- 相当于表格的每一行都可以是一个`TreeView`

##### ①基本使用

```java
Student student1 = new Student("张三", 85.5, false);
Student student2 = new Student("李四", 78.9, true);
Student student3 = new Student("王五", 100.1, true);
Student student4 = new Student("赵六", 59, false);

TreeItem<Student> rootTreeItem = new TreeItem<>(student1);
TreeItem<Student> rootTreeItem1 = new TreeItem<>(student2);
TreeItem<Student> rootTreeItem2 = new TreeItem<>(student3);
TreeItem<Student> rootTreeItem3 = new TreeItem<>(student4);
rootTreeItem.getChildren().addAll(rootTreeItem1, rootTreeItem2, rootTreeItem3);

TreeTableView<Student> treeTableView = new TreeTableView<>(rootTreeItem);
treeTableView.setColumnResizePolicy(TreeTableView.CONSTRAINED_RESIZE_POLICY);
// 设置默认展开
rootTreeItem.setExpanded(true);
// 可以选择单元格
treeTableView.getSelectionModel().setCellSelectionEnabled(true);
treeTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
// treeTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue.getValue()));

treeTableView.getSelectionModel().getSelectedCells().addListener((InvalidationListener) observable -> {
    ObservableList<TreeTablePosition<Student, ?>> list = (ObservableList<TreeTablePosition<Student, ?>>) observable;
    for (TreeTablePosition<Student, ?> ttp : list) {
        Object cellData = ttp.getTableColumn().getCellData(ttp.getRow());
        System.out.println("行 = " + ttp.getRow() + ", 列 = " + ttp.getColumn() + ", 数据 = " + cellData);
    }
});
// 创建列
TreeTableColumn<Student, Integer> idColumn = new TreeTableColumn<>("编号");
TreeTableColumn<Student, String> nameColumn = new TreeTableColumn<>("姓名");
TreeTableColumn<Student, Double> scoreColumn = new TreeTableColumn<>("分数");
TreeTableColumn<Student, Boolean> isChangedColumn = new TreeTableColumn<>("操作");

// 给列添加数据
idColumn.setCellValueFactory(new TreeItemPropertyValueFactory<>("id"));
nameColumn.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
scoreColumn.setCellValueFactory(new TreeItemPropertyValueFactory<>("score"));
isChangedColumn.setCellValueFactory(new TreeItemPropertyValueFactory<>("isChanged"));

// 将列添加到treeTableView
treeTableView.getColumns().addAll(idColumn, nameColumn, scoreColumn, isChangedColumn);
```

##### ②自定义单元格

```java
treeTableView.setEditable(true);
idColumn.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn(new StringConverter<>() {
    @Override
    public String toString(Integer object) {
        return object.toString();
    }

    @Override
    public Integer fromString(String string) {
        return Integer.valueOf(string);
    }
}));
// nameColumn.setCellFactory(ComboBoxTreeTableCell.forTreeTableColumn("A", "B", "C"));
nameColumn.setCellFactory(new Callback<>() {
    @Override
    public TreeTableCell<Student, String> call(TreeTableColumn<Student, String> param) {

        return new TreeTableCell<>() {
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty) {
                    Label label = new Label(item);
                    HBox hBox = new HBox(label);
                    this.setGraphic(hBox);
                } else {
                    this.setGraphic(null);
                }
            }

            @Override
            public void startEdit() {
                super.startEdit();
                TreeTableCell<Student, String> treeTableCell1 = this;
                TextField textField = new TextField(this.getItem());
                textField.setOnKeyPressed(event -> {
                    if (event.getCode() == KeyCode.ENTER) {
                        treeTableCell1.commitEdit(textField.getText());
                    }
                });
                HBox hBox = new HBox(textField);
                this.setGraphic(hBox);
                textField.requestFocus();
            }

            @Override
            public void cancelEdit() {
                super.cancelEdit();
                Label label = new Label(this.getItem());
                HBox hBox = new HBox(label);
                this.setGraphic(hBox);
            }
        };
    }
});
isChangedColumn.setCellFactory(CheckBoxTreeTableCell.forTreeTableColumn(isChangedColumn));
scoreColumn.setCellFactory(ProgressBarTreeTableCell.forTreeTableColumn());
```

#### 26、图表

##### ①饼状图`PieChart`

```java
// 饼状图的数据
// 后面的数据相当于权重
PieChart.Data data1 = new PieChart.Data("data1", 10);
PieChart.Data data2 = new PieChart.Data("data2", 20);
PieChart.Data data3 = new PieChart.Data("data3", 30);
PieChart.Data data4 = new PieChart.Data("data4", 40);
ObservableList<PieChart.Data> observableList = FXCollections.observableArrayList();
observableList.addAll(data1, data2, data3, data4);
PieChart pieChart = new PieChart(observableList);
// 设置线长
// pieChart.setLabelLineLength(50);
// 旋转图形(正值逆时针旋转)
//pieChart.setStartAngle(45);
// 设置图例说明是否显示
//pieChart.setLabelsVisible(false);
// pieChart.setLegendVisible(false);
// 设置标题方位
pieChart.setLegendSide(Side.RIGHT);
// 设置绘制方向, true为顺时针
// pieChart.setClockwise(false);
// 设置标题
pieChart.setTitle("这是标题");
// 设置标题位置
pieChart.setTitleSide(Side.TOP);
// 设置是否有动画
//pieChart.setAnimated(false);

// 设置提示
for (PieChart.Data datum : pieChart.getData()) {
    Node node = datum.getNode();
    Tooltip tooltip = new Tooltip(datum.getName() + ": " + datum.getPieValue());
    tooltip.setFont(new Font(16));
    Tooltip.install(node, tooltip);
    datum.pieValueProperty().addListener((observable, oldValue, newValue) -> tooltip.setText(datum.getName() + ": " + newValue));
    node.setOnMouseClicked(event -> System.out.println("AAA"));
}

Button button = new Button("button");
button.setOnAction(event -> data1.setPieValue(50));
```

##### ②柱状图`BarChart`

###### 一般用法

```java
private static BarChart<String, Number> getView1() {
    CategoryAxis x = new CategoryAxis();
    x.setLabel("国家");
    NumberAxis y = new NumberAxis();
    y.setLabel("GDP");
    ObservableList<XYChart.Series<String, Number>> observableList = FXCollections.observableArrayList();
    XYChart.Series<String, Number> xyData = new XYChart.Series<>();
    xyData.setName("生产总值");
    ObservableList<XYChart.Data<String, Number>> data = FXCollections.observableArrayList();

    XYChart.Data<String, Number> data1 = new XYChart.Data<>("中国", 80);
    XYChart.Data<String, Number> data2 = new XYChart.Data<>("美国", 98);
    XYChart.Data<String, Number> data3 = new XYChart.Data<>("日本", 25);
    XYChart.Data<String, Number> data4 = new XYChart.Data<>("德国", 60);
    data.addAll(data1, data2, data3, data4);
    xyData.setData(data);
    observableList.add(xyData);
    BarChart<String, Number> barChart = new BarChart<>(x, y, observableList);
    barChart.setTitle("方式一");
    barChart.setPrefWidth(250);
    barChart.setPrefHeight(300);
    return barChart;
}
```

###### 按类别分组

```java
private static BarChart<String, Number> getView2() {
    CategoryAxis x = new CategoryAxis();
    x.setLabel("类别");
    NumberAxis y = new NumberAxis();
    y.setLabel("总值 / 万亿");
    XYChart.Series<String, Number> xyData1 = new XYChart.Series<>();
    xyData1.setName("中国");
    XYChart.Series<String, Number> xyData2 = new XYChart.Series<>();
    xyData2.setName("美国");
    XYChart.Series<String, Number> xyData3 = new XYChart.Series<>();
    xyData3.setName("日本");
    XYChart.Series<String, Number> xyData4 = new XYChart.Series<>();
    xyData4.setName("德国");

    XYChart.Data<String, Number> data1 = new XYChart.Data<>("GDP", 80);
    XYChart.Data<String, Number> data2 = new XYChart.Data<>("GDP", 98);
    XYChart.Data<String, Number> data3 = new XYChart.Data<>("GDP", 25);
    XYChart.Data<String, Number> data4 = new XYChart.Data<>("GDP", 60);

    XYChart.Data<String, Number> data5 = new XYChart.Data<>("GNP", 800);
    XYChart.Data<String, Number> data6 = new XYChart.Data<>("GNP", 980);
    XYChart.Data<String, Number> data7 = new XYChart.Data<>("GNP", 250);
    XYChart.Data<String, Number> data8 = new XYChart.Data<>("GNP", 600);

    xyData1.getData().addAll(data1, data5);
    xyData2.getData().addAll(data2, data6);
    xyData3.getData().addAll(data3, data7);
    xyData4.getData().addAll(data4, data8);

    BarChart<String, Number> barChart = new BarChart<>(x, y);
    barChart.getData().addAll(xyData1, xyData2, xyData3, xyData4);
    barChart.setTitle("方式二");
    barChart.setPrefWidth(250);
    barChart.setPrefHeight(300);
    return barChart;
}
```

###### 按照集合分组

```java
private static BarChart<String, Number> getView3() {
    CategoryAxis x = new CategoryAxis();
    x.setLabel("国家");
    NumberAxis y = new NumberAxis();
    y.setLabel("GDP");

    XYChart.Series<String, Number> xyData1 = new XYChart.Series<>();
    xyData1.setName("GDP");
    XYChart.Series<String, Number> xyData2 = new XYChart.Series<>();
    xyData2.setName("GNP");

    // name一样的会被放在一起
    XYChart.Data<String, Number> data1 = new XYChart.Data<>("中国", 80);
    XYChart.Data<String, Number> data2 = new XYChart.Data<>("美国", 98);
    XYChart.Data<String, Number> data3 = new XYChart.Data<>("日本", 25);
    XYChart.Data<String, Number> data4 = new XYChart.Data<>("德国", 60);

    XYChart.Data<String, Number> data5 = new XYChart.Data<>("中国", 800);
    XYChart.Data<String, Number> data6 = new XYChart.Data<>("美国", 980);
    XYChart.Data<String, Number> data7 = new XYChart.Data<>("日本", 250);
    XYChart.Data<String, Number> data8 = new XYChart.Data<>("德国", 600);

    xyData1.getData().addAll(data1, data2, data3, data4);
    xyData2.getData().addAll(data5, data6, data7, data8);

    BarChart<String, Number> barChart = new BarChart<>(x, y);
    barChart.getData().addAll(xyData1, xyData2);
    barChart.setTitle("方式三");
    barChart.setPrefWidth(250);
    barChart.setPrefHeight(300);
    return barChart;
}
```

###### 交换横纵坐标

```java
private static BarChart<Number, String> getView4() {
    CategoryAxis y = new CategoryAxis();
    y.setLabel("国家");
    NumberAxis x = new NumberAxis();
    x.setLabel("GDP");

    XYChart.Series<Number, String> xyData1 = new XYChart.Series<>();
    xyData1.setName("生产总值");

    // name一样的会被放在一起
    XYChart.Data<Number, String> data1 = new XYChart.Data<>(80, "中国");
    XYChart.Data<Number, String> data2 = new XYChart.Data<>(98,"美国" );
    XYChart.Data<Number, String> data3 = new XYChart.Data<>(25,"日本");
    XYChart.Data<Number, String> data4 = new XYChart.Data<>(60,"德国");

    xyData1.getData().addAll(data1, data2, data3, data4);

    BarChart<Number, String> barChart = new BarChart<>(x, y);
    barChart.getData().addAll(xyData1);
    barChart.setTitle("方式四");
    barChart.setPrefWidth(250);
    barChart.setPrefHeight(300);
    return barChart;
}
```

###### 基本设置

```java
// 柱状图
BarChart<String, Number> barChart = getView1();
// 设置动画
//barChart.setAnimated(false);
//barChart.setLegendVisible(false);
//barChart.setLegendSide(Side.RIGHT);
// 设置柱子之间的间隔
//barChart.setBarGap(20);
// 不同类别的间隔
//barChart.setCategoryGap(20);
// 设置方向
//barChart.setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
// 隐藏网格横线
//lineChart.setHorizontalGridLinesVisible(false);
// 隐藏网格竖线
//lineChart.setVerticalGridLinesVisible(false);
// 显示水平0线
//lineChart.setHorizontalZeroLineVisible(true);
// 显示垂直0线
//lineChart.setVerticalZeroLineVisible(true);
// 定制网格样式, 需要使用CSS
// lineChart.setAlternativeColumnFillVisible(true);
// lineChart.setAlternativeRowFillVisible(true);
// 格式化显示的值
y.setTickLabelFormatter(new StringConverter<>() {
    @Override
    public String toString(Number object) {
        return object.toString() + "%";
    }

    @Override
    public Number fromString(String string) {
        return null;
    }
});
```

```java
CategoryAxis x = new CategoryAxis();
x.setLabel("国家");
// 距离左边距离
//x.setStartMargin(100);
// 距离右边距离
//x.setEndMargin(100);
// 设置x轴位置
//x.setSide(Side.TOP);
// 设置标签字体和颜色
//x.setTickLabelFill(Paint.valueOf("#ff345c"));
//x.setTickLabelFont(Font.font(16));
// 类型名称对于轴刻度的距离
// x.setTickLabelGap(50);
// 设置类型名称的旋转
x.setTickLabelRotation(45);
// 类型名称是否显示
//x.setTickLabelsVisible(false);
// 类型名称线的长度
//x.setTickLength(100);
// 是否显示刻度
x.setTickMarkVisible(false);
```

```java
XYChart.Series<String, Number> xyData = new XYChart.Series<>();
xyData.setName("生产总值");
xyData.setData(data);
// 设置柱子里显示的内容
for (XYChart.Data<String, Number> datum : xyData.getData()) {
Text text = new Text(datum.getYValue().toString());
HBox hBox = new HBox(text);
text.setTextOrigin(VPos.CENTER);
//hBox.setAlignment(Pos.CENTER);
datum.setNode(hBox);
}
```

##### ③堆积柱状图`StackedBarChart`

```java
 public static StackedBarChart<String, Number> getView1() {
        XYChart.Data<String, Number> data1 = new XYChart.Data<>("一", 10);
        XYChart.Data<String, Number> data2 = new XYChart.Data<>("二", 20);
        XYChart.Data<String, Number> data3 = new XYChart.Data<>("三", 30);
        XYChart.Data<String, Number> data4 = new XYChart.Data<>("四", 40);
        XYChart.Data<String, Number> data5 = new XYChart.Data<>("一", 20);
        XYChart.Data<String, Number> data6 = new XYChart.Data<>("二", 70);
        XYChart.Data<String, Number> data7 = new XYChart.Data<>("三", 56);
        XYChart.Data<String, Number> data8 = new XYChart.Data<>("四", 24);
        XYChart.Series<String, Number> xyData1 = new XYChart.Series<>();
        xyData1.setName("xy1");
        xyData1.getData().addAll(data1, data2, data3, data4);
        XYChart.Series<String, Number> xyData2 = new XYChart.Series<>();
        xyData2.setName("xy2");
        xyData2.getData().addAll(data5, data6, data7, data8);
        CategoryAxis x = new CategoryAxis();
        x.setLabel("x轴");
        NumberAxis y = new NumberAxis("y轴", 0, 200, 10);
        StackedBarChart<String, Number> stackedBarChart = new StackedBarChart<>(x, y);
        stackedBarChart.getData().addAll(xyData1, xyData2);
        // 设置柱子之间的间距
        // stackedBarChart.setCategoryGap(0);
        return stackedBarChart;
    }
```

##### ④折线图`LineChart`

###### 一般用法

```java
public static LineChart<Number, Number> getView1() {
    XYChart.Data<Number, Number> data1 = new XYChart.Data<>(10, 10);
    XYChart.Data<Number, Number> data2 = new XYChart.Data<>(20, 20);
    XYChart.Data<Number, Number> data3 = new XYChart.Data<>(30, 30);
    XYChart.Data<Number, Number> data4 = new XYChart.Data<>(40, 40);
    XYChart.Data<Number, Number> data5 = new XYChart.Data<>(10, 20);
    XYChart.Data<Number, Number> data6 = new XYChart.Data<>(20, 70);
    XYChart.Data<Number, Number> data7 = new XYChart.Data<>(30, 56);
    XYChart.Data<Number, Number> data8 = new XYChart.Data<>(40, 24);
    XYChart.Series<Number, Number> xyData1 = new XYChart.Series<>();
    xyData1.setName("xy1");
    xyData1.getData().addAll(data1, data2, data3, data4);
    for (XYChart.Data<Number, Number> datum : xyData1.getData()) {
        HBox hBox = new HBox();
        datum.setNode(hBox);
        Tooltip tooltip = new Tooltip(datum.getXValue() + " - " + datum.getYValue());
        Tooltip.install(datum.getNode(), tooltip);
        datum.getNode().setOnMouseClicked((event) -> {
            System.out.println(datum.getXValue() + " " + datum.getYValue());
        });
    }
    XYChart.Series<Number, Number> xyData2 = new XYChart.Series<>();
    xyData2.setName("xy2");
    xyData2.getData().addAll(data5, data6, data7, data8);
    NumberAxis x = new NumberAxis("x轴", 0, 100, 10);
    NumberAxis y = new NumberAxis("y轴", 0, 100, 10);
    LineChart<Number, Number> lineChart = new LineChart<Number, Number>(x, y);
    lineChart.getData().addAll(xyData1, xyData2);
    // 不显示折线上的点
    //lineChart.setCreateSymbols(false);
    lineChart.setPrefWidth(250);
    return lineChart;
}
```

###### 横坐标为字符串

```java
public static LineChart<String, Number> getView2() {
    XYChart.Data<String, Number> data1 = new XYChart.Data<>("一", 10);
    XYChart.Data<String, Number> data2 = new XYChart.Data<>("二", 20);
    XYChart.Data<String, Number> data3 = new XYChart.Data<>("三", 30);
    XYChart.Data<String, Number> data4 = new XYChart.Data<>("四", 40);
    XYChart.Data<String, Number> data5 = new XYChart.Data<>("一", 20);
    XYChart.Data<String, Number> data6 = new XYChart.Data<>("二", 70);
    XYChart.Data<String, Number> data7 = new XYChart.Data<>("三", 56);
    XYChart.Data<String, Number> data8 = new XYChart.Data<>("四", 24);
    XYChart.Series<String, Number> xyData1 = new XYChart.Series<>();
    xyData1.setName("xy1");
    xyData1.getData().addAll(data1, data2, data3, data4);
    XYChart.Series<String, Number> xyData2 = new XYChart.Series<>();
    xyData2.setName("xy2");
    xyData2.getData().addAll(data5, data6, data7, data8);
    CategoryAxis x = new CategoryAxis();
    x.setLabel("x轴");
    NumberAxis y = new NumberAxis("y轴", 0, 100, 10);
    LineChart<String, Number> lineChart = new LineChart<>(x, y);
    lineChart.getData().addAll(xyData1, xyData2);
    lineChart.setPrefWidth(250);
    return lineChart;
}
```

###### 纵坐标为字符串

```java
public static LineChart<Number, String> getView3() {
    XYChart.Data<Number, String> data1 = new XYChart.Data<>(10, "一");
    XYChart.Data<Number, String> data2 = new XYChart.Data<>(20, "二");
    XYChart.Data<Number, String> data3 = new XYChart.Data<>(30, "三");
    XYChart.Data<Number, String> data4 = new XYChart.Data<>(40, "四");
    XYChart.Data<Number, String> data5 = new XYChart.Data<>(20, "一");
    XYChart.Data<Number, String> data6 = new XYChart.Data<>(70, "二");
    XYChart.Data<Number, String> data7 = new XYChart.Data<>(56, "三");
    XYChart.Data<Number, String> data8 = new XYChart.Data<>(24, "四");
    XYChart.Series<Number, String> xyData1 = new XYChart.Series<>();
    xyData1.setName("xy1");
    xyData1.getData().addAll(data1, data2, data3, data4);
    XYChart.Series<Number, String> xyData2 = new XYChart.Series<>();
    xyData2.setName("xy2");
    xyData2.getData().addAll(data5, data6, data7, data8);
    CategoryAxis y = new CategoryAxis();
    y.setLabel("y轴");
    NumberAxis x = new NumberAxis("x轴", 0, 100, 10);
    LineChart<Number, String> lineChart = new LineChart<>(x, y);
    lineChart.getData().addAll(xyData1, xyData2);
    lineChart.setPrefWidth(250);
    return lineChart;
}
```

##### ⑤气泡图`BubbleChart`

```java
public static BubbleChart<Number, Number> getView1() {
    XYChart.Data<Number, Number> data1 = new XYChart.Data<>(10, 10);
    // 可以更改气泡大小
    // data1.setExtraValue(5);
    XYChart.Data<Number, Number> data2 = new XYChart.Data<>(20, 20);
    XYChart.Data<Number, Number> data3 = new XYChart.Data<>(30, 30);
    XYChart.Data<Number, Number> data4 = new XYChart.Data<>(40, 40);
    XYChart.Data<Number, Number> data5 = new XYChart.Data<>(10, 20);
    XYChart.Data<Number, Number> data6 = new XYChart.Data<>(20, 70);
    XYChart.Data<Number, Number> data7 = new XYChart.Data<>(30, 56);
    XYChart.Data<Number, Number> data8 = new XYChart.Data<>(40, 24);
    XYChart.Series<Number, Number> xyData1 = new XYChart.Series<>();
    xyData1.setName("xy1");
    xyData1.getData().addAll(data1, data2, data3, data4);
    XYChart.Series<Number, Number> xyData2 = new XYChart.Series<>();
    xyData2.setName("xy2");
    xyData2.getData().addAll(data5, data6, data7, data8);
    NumberAxis x = new NumberAxis("x轴", 0, 100, 10);
    NumberAxis y = new NumberAxis("y轴", 0, 100, 10);
    BubbleChart<Number, Number> bubbleChart = new BubbleChart<Number, Number>(x, y);
    bubbleChart.getData().addAll(xyData1, xyData2);
    return bubbleChart;
}
```

##### ⑥散点图`ScatterChart`

```java
public static ScatterChart<Number, Number> getView1() {
    XYChart.Data<Number, Number> data1 = new XYChart.Data<>(10, 10);
    XYChart.Data<Number, Number> data2 = new XYChart.Data<>(20, 20);
    XYChart.Data<Number, Number> data3 = new XYChart.Data<>(30, 30);
    XYChart.Data<Number, Number> data4 = new XYChart.Data<>(40, 40);
    XYChart.Data<Number, Number> data5 = new XYChart.Data<>(10, 20);
    XYChart.Data<Number, Number> data6 = new XYChart.Data<>(20, 70);
    XYChart.Data<Number, Number> data7 = new XYChart.Data<>(30, 56);
    XYChart.Data<Number, Number> data8 = new XYChart.Data<>(40, 24);
    XYChart.Series<Number, Number> xyData1 = new XYChart.Series<>();
    xyData1.setName("xy1");
    xyData1.getData().addAll(data1, data2);
    XYChart.Series<Number, Number> xyData2 = new XYChart.Series<>();
    xyData2.setName("xy2");
    xyData2.getData().addAll(data3, data4);
    XYChart.Series<Number, Number> xyData3 = new XYChart.Series<>();
    xyData3.setName("xy2");
    xyData3.getData().addAll(data5, data6);
    XYChart.Series<Number, Number> xyData4 = new XYChart.Series<>();
    xyData4.setName("xy2");
    xyData4.getData().addAll(data7, data8);
    NumberAxis x = new NumberAxis("x轴", 0, 100, 10);
    x.setLabel("x轴");
    NumberAxis y = new NumberAxis("y轴", 0, 100, 10);
    ScatterChart<Number, Number> scatterChart = new ScatterChart<>(x, y);
    scatterChart.getData().addAll(xyData1, xyData2, xyData3, xyData4);
    return scatterChart;
}
```

##### ⑦区域图`AreaChart`

```java
public static AreaChart<Number, Number> getView1() {
    XYChart.Data<Number, Number> data1 = new XYChart.Data<>(10, 10);
    XYChart.Data<Number, Number> data2 = new XYChart.Data<>(20, 20);
    XYChart.Data<Number, Number> data3 = new XYChart.Data<>(30, 30);
    XYChart.Data<Number, Number> data4 = new XYChart.Data<>(40, 40);
    XYChart.Data<Number, Number> data5 = new XYChart.Data<>(10, 20);
    XYChart.Data<Number, Number> data6 = new XYChart.Data<>(20, 70);
    XYChart.Data<Number, Number> data7 = new XYChart.Data<>(30, 56);
    XYChart.Data<Number, Number> data8 = new XYChart.Data<>(40, 24);
    XYChart.Series<Number, Number> xyData1 = new XYChart.Series<>();
    xyData1.setName("xy1");
    xyData1.getData().addAll(data1, data2,data3, data4);
    XYChart.Series<Number, Number> xyData2 = new XYChart.Series<>();
    xyData2.setName("xy2");
    xyData2.getData().addAll(data5, data6,data7, data8);
    NumberAxis x = new NumberAxis("x轴", 0, 100, 10);
    x.setLabel("x轴");
    NumberAxis y = new NumberAxis("y轴", 0, 100, 10);
    AreaChart<Number, Number> areaChart = new AreaChart<>(x, y);
    areaChart.getData().addAll(xyData1, xyData2);
    return areaChart;
}
```

##### ⑧堆积区域图`StackedAreaChart`

```java
public static StackedAreaChart<Number, Number> getView1() {
    XYChart.Data<Number, Number> data1 = new XYChart.Data<>(10, 10);
    XYChart.Data<Number, Number> data2 = new XYChart.Data<>(20, 20);
    XYChart.Data<Number, Number> data3 = new XYChart.Data<>(30, 30);
    XYChart.Data<Number, Number> data4 = new XYChart.Data<>(40, 40);
    XYChart.Data<Number, Number> data5 = new XYChart.Data<>(10, 20);
    XYChart.Data<Number, Number> data6 = new XYChart.Data<>(20, 70);
    XYChart.Data<Number, Number> data7 = new XYChart.Data<>(30, 56);
    XYChart.Data<Number, Number> data8 = new XYChart.Data<>(40, 24);
    XYChart.Series<Number, Number> xyData1 = new XYChart.Series<>();
    xyData1.setName("xy1");
    xyData1.getData().addAll(data1, data2,data3, data4);
    XYChart.Series<Number, Number> xyData2 = new XYChart.Series<>();
    xyData2.setName("xy2");
    xyData2.getData().addAll(data5, data6,data7, data8);
    NumberAxis x = new NumberAxis("x轴", 0, 100, 10);
    x.setLabel("x轴");
    NumberAxis y = new NumberAxis("y轴", 0, 100, 10);
    StackedAreaChart<Number, Number> stackedAreaChart = new StackedAreaChart<>(x, y);
    stackedAreaChart.getData().addAll(xyData1, xyData2);
    return stackedAreaChart;
}
```

#### 27、`HTML`编辑器

```java
HTMLEditor htmlEditor = new HTMLEditor();
htmlEditor.setPrefWidth(600);
htmlEditor.setPrefHeight(300);
Button button1 = new Button("button1");
button1.setOnAction(event -> {
htmlEditor.setHtmlText("<a href='https://www.baidu.com'>百度</a>");
});

Button button2 = new Button("button2");
button2.setOnAction(event -> {
System.out.println(htmlEditor.getHtmlText());
});
```

#### 28、`WebView`浏览器组件

##### ①基本使用

```java
WebView webView = new WebView();
WebEngine engine = webView.getEngine();
engine.load("https://www.baidu.com");

Button button1 = new Button("button1");
button1.setOnAction(event -> {
    // 缩小文字
    //webView.setFontScale(0.5);
    // 整体缩放
    webView.setZoom(0.5);

});

Button button2 = new Button("button2");
button2.setOnAction(event -> {
    // 加载html内容
    engine.loadContent("3211e");
});
```

##### ②前进、后退、历史信息

```java
WebView webView = new WebView();
// 浏览器引擎
WebEngine engine = webView.getEngine();
engine.load("https://www.baidu.com");
// 浏览记录
WebHistory history = engine.getHistory();
ObservableList<WebHistory.Entry> entries = history.getEntries();
Button button1 = new Button("前进");
button1.setOnAction(event -> {
    if (history.getCurrentIndex() < entries.size() - 1) {
        history.go(1);
    }
});
Button button2 = new Button("后退");
button2.setOnAction(event -> {
    if (history.getCurrentIndex() > 0) {
        history.go(-1);
    }
});
Button button3 = new Button("信息");
button3.setOnAction(event -> {
    System.out.println("当前页面索引 = " + history.getCurrentIndex());
    System.out.println("最大存储数量 = " + history.getMaxSize());
    System.out.println("当前存储数量 = " + entries.size());
    for (WebHistory.Entry entry : entries) {
        System.out.println(entry.getTitle());
        System.out.println(entry.getUrl());
        System.out.println(entry.getLastVisitedDate());
    }
    System.out.println("------------------------------------------");
});
```

### 三、基本操作

#### 1、多任务

##### ①`Task`

```java
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * @author huayunliufeng
 * @date_time 2023/8/21 16:21
 * @desc
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage){
        Button okButton = new Button("确认");
        Button cancelButton = new Button("取消");
        ProgressBar progressBar = new ProgressBar();
        Label label1 = new Label("start");
        Label label2 = new Label("value");
        Label label3 = new Label("title");
        Label label4 = new Label("message");
        HBox hBox = new HBox(10, okButton, cancelButton, progressBar, label1, label2, label3, label4);
        hBox.setPadding(new Insets(20));
        AnchorPane anchorPane = new AnchorPane(hBox);
        Scene scene = new Scene(anchorPane);
        stage.setScene(scene);
        // 布局
        stage.setTitle("JavaFX");
        stage.setWidth(800);
        stage.setHeight(800);
        stage.show();

        MyTask myTask = new MyTask();
        Thread thread = new Thread(myTask);

        myTask.stateProperty().addListener((observable, oldValue, newValue) -> label1.setText(newValue.toString()));
        myTask.progressProperty().addListener((observable, oldValue, newValue) -> progressBar.setProgress(newValue.doubleValue()));
        myTask.titleProperty().addListener((observable, oldValue, newValue) -> label3.setText(newValue));
        myTask.valueProperty().addListener((observable, oldValue, newValue) -> label2.setText(newValue.toString()));
        myTask.messageProperty().addListener((observable, oldValue, newValue) -> label4.setText(newValue));
        myTask.exceptionProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue.getMessage()));
        
        okButton.setOnAction(event -> thread.start());
        cancelButton.setOnAction(event -> myTask.cancel());
    }
}

class MyTask extends Task<Double> {
    @Override
    protected Double call() throws Exception {
        this.updateTitle("copy");
        // 进度
        double progressValue = 0;
        // 已经读取的字节数
        int sumSize = 0;
        // 总字节数
        int countSize;
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(Utils.getResourcesPath("fillFile_1024MB")));
             BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(Utils.getResourcesPath("fillFile_1024MB_copy")))) {
            countSize = bis.available();
            byte[] bytes = new byte[1024 * 4];
            int len;
            while ((len = bis.read(bytes)) != -1 && !this.isCancelled()) {
                bos.write(bytes, 0, len);
                sumSize += len;
                this.updateProgress(sumSize, countSize);
                progressValue = sumSize * 1.0 / countSize;
                this.updateValue(progressValue);
                if (progressValue < 0.5) {
                    this.updateMessage("请耐心等待");
                } else if (progressValue < 0.8) {
                    this.updateMessage("马上就好");
                } else if (progressValue < 1) {
                    this.updateMessage("即将完成");
                } else if (progressValue >= 1) {
                    this.updateMessage("搞定了");
                }
            }
            bos.flush();
            System.out.println("复制完成");
        }
        return progressValue;
    }
}
```

##### ②`Service`

```java
MyService myService = new MyService();
myService.stateProperty().addListener((observable, oldValue, newValue) -> label1.setText(newValue.toString()));
myService.progressProperty().addListener((observable, oldValue, newValue) -> {
    progressBar.setProgress(newValue.doubleValue());
    label2.setText(String.format("%.2f", newValue.doubleValue()));
});
myService.titleProperty().addListener((observable, oldValue, newValue) -> label3.setText(newValue));
myService.messageProperty().addListener((observable, oldValue, newValue) -> label4.setText(newValue));
myService.exceptionProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue.getMessage()));

okButton.setOnAction(event -> myService.start());
cancelButton.setOnAction(event -> myService.cancel());
rebootButton.setOnAction(event -> myService.restart());
resetButton.setOnAction(event -> myService.reset());

class MyService extends Service<Number>{
    
    @Override
    protected Task<Number> createTask() {
        return new Task<>() {
            @Override
            protected Number call() throws Exception {
                return null;
            }
        };
    }
}
```

##### ③`ScheduledService`

```java
class MyScheduledService extends ScheduledService<Integer> {
    private final DialogPane dialogPane;
    private final Stage stage;
    private int num = 10;
    public MyScheduledService(DialogPane dialogPane, Stage stage) {
        this.dialogPane = dialogPane;
        this.stage = stage;
    }
    @Override
    protected Task<Integer> createTask() {
        return new Task<>() {
            @Override
            protected void updateValue(Integer value) {
                // 此方法使用start线程
                if (num <= 0) {
                    stage.close();
                    // 关闭定时器
                    MyScheduledService.this.cancel();
                } else {
                    dialogPane.setContentText("" + num--);
                }
            }
            @Override
            protected Integer call() throws Exception {
                // 此方法每次创建新的线程
                System.out.println("call " + Utils.getThreadName());
                return 0;
            }
        };
    }
}
```

使用：

```java
// 做一个倒计时关闭
MyScheduledService myScheduledService = new MyScheduledService(dialogPane, dialogStage);
// 延时执行, 默认是0
// myScheduledService.setDelay(Duration.seconds(3));
myScheduledService.setPeriod(Duration.seconds(1));
myScheduledService.start();
```

#### 2、属性绑定

##### ①基础使用

```java
// 单向绑定
SimpleIntegerProperty x = new SimpleIntegerProperty(1);
SimpleIntegerProperty y = new SimpleIntegerProperty(2);
x.bind(y);
System.out.println(x.isBound());
System.out.println(y.isBound());
y.set(10);
System.out.println(x.get());
System.out.println(y.get());
// x必须解绑后才能使用
x.unbind();
x.set(100);
// 双向绑定
SimpleIntegerProperty a = new SimpleIntegerProperty(10);
SimpleIntegerProperty b = new SimpleIntegerProperty(20);
a.bindBidirectional(b);
System.out.println(a.isBound());
System.out.println(b.isBound());
a.set(100);
System.out.println(a.get());
System.out.println(b.get());
a.unbindBidirectional(b);
```

##### ②在组件上的应用

```java
TextField textField1 = new TextField();
TextField textField2 = new TextField();
TextField textField3 = new TextField();
TextField textField4 = new TextField();
// 绑定
textField1.textProperty().bind(stage.widthProperty().asString());
textField2.textProperty().bind(stage.heightProperty().asString());
// 将textField4的值单向绑定到textField3上
// textField4.textProperty().bind(textField3.textProperty());
// 双向绑定
textField3.textProperty().bindBidirectional(textField4.textProperty(), new StringConverter<>(){

    @Override
    public String toString(String object) {
        return object;
    }

    @Override
    public String fromString(String string) {
        return string.replace("1", "壹");
    }
});
HBox hBox1 = new HBox(10, textField1, textField2);
HBox hBox2 = new HBox(10, textField3, textField4);
VBox vBox = new VBox(10, hBox1, hBox2);
vBox.setPadding(new Insets(10));
AnchorPane anchorPane = new AnchorPane(vBox);
```

##### ③绑定计算

```java
// 绑定计算
SimpleIntegerProperty a = new SimpleIntegerProperty(1);
SimpleIntegerProperty b = new SimpleIntegerProperty(1);

// 其它运算类似
IntegerBinding aB6 = a.add(6);
NumberBinding aBb = a.add(b);

// 不会改变a
System.out.println(a.get());
System.out.println(aB6.get());
System.out.println(aBb.intValue());
// 取反
System.out.println(a.negate().get());
a.set(10);
// 绑定后会实时更新
System.out.println(aB6.get());
b.set(10);
System.out.println(aBb.intValue());
```

使用示例：

```java
ObservableList<String> observableList = FXCollections.observableArrayList("A", "B", "C", "D", "E", "F");
SimpleListProperty<String> strings = new SimpleListProperty<>(observableList);
TextField textField1 = new TextField();
TextField textField2 = new TextField();
HBox hBox = new HBox(10, textField1, textField2);
hBox.setPadding(new Insets(10));
VBox vBox = new VBox(10);
for (int i = 0; i < strings.size(); i++) {
    Label label = new Label(strings.get(i));
    label.textProperty().bind(strings.valueAt(i));
    vBox.getChildren().add(label);
}
textField2.textProperty().addListener((observable, oldValue, newValue) -> {
    String text = textField1.getText();
    if (!text.isBlank()) {
        try {
            int index = Integer.parseInt(text);
            if (index >= 0 && index < strings.size()) {
                observableList.set(index, newValue);
            }
        } catch (Exception ignored) {
        }
    }
});
```

##### ④绑定判断

```java
// 绑定判断
SimpleIntegerProperty a = new SimpleIntegerProperty(1);
SimpleIntegerProperty b = new SimpleIntegerProperty(0);
// 大于判断
BooleanBinding booleanBinding1 = a.greaterThan(b);
// 小于判断
BooleanBinding booleanBinding2 = a.lessThan(b);
// 等于判断, 可以带一个误差
BooleanBinding equalTo = a.isEqualTo(b);
// 小于等于
BooleanBinding booleanBinding = a.lessThanOrEqualTo(b);
System.out.println(booleanBinding1.get());
```

```java
// 三元运算符
SimpleIntegerProperty x = new SimpleIntegerProperty(10);
SimpleIntegerProperty y = new SimpleIntegerProperty(20);
SimpleBooleanProperty simpleBooleanProperty = new SimpleBooleanProperty(true);

When when = new When(simpleBooleanProperty);
NumberBinding n = when.then(x).otherwise(y);
System.out.println(n.intValue());
```

##### ⑤自定义绑定计算

```java
SimpleIntegerProperty simpleIntegerProperty = new SimpleIntegerProperty();
MyIntegerBinding myIntegerBinding = new MyIntegerBinding(100);
simpleIntegerProperty.bind(myIntegerBinding);
System.out.println(myIntegerBinding.get());
System.out.println(simpleIntegerProperty.get());
myIntegerBinding.set(200);
System.out.println(myIntegerBinding.get());
System.out.println(simpleIntegerProperty.get());

class MyIntegerBinding extends IntegerBinding {

    private final SimpleIntegerProperty value;

    public MyIntegerBinding(int value) {
        this.value = new SimpleIntegerProperty(value);
        this.bind(this.value);
    }

    public void set(int value) {
        this.value.set(value);
    }

    @Override
    protected int computeValue() {
        // 例如计算面积等
        int tmpValue = value.get();
        return tmpValue * 2 - 1;
    }
}
```

##### ⑥对集合的监听

```java
SimpleStringProperty s1 = new SimpleStringProperty("A");
SimpleStringProperty s2 = new SimpleStringProperty("B");
ObservableList<SimpleStringProperty> list = FXCollections.observableArrayList(param -> {
    System.out.println("call " + param);
    return new Observable[]{param};
});

list.addListener((ListChangeListener<SimpleStringProperty>) c -> {
    while (c.next()) {
        System.out.println("wasUpdated = " + c.wasUpdated());
    }
});
list.addAll(s1, s2);
s1.set("B");
```

#### 3、键盘事件

```java
Button button1 = new Button("button1");
Button button2 = new Button("button2");
TextField textField = new TextField();
Rectangle rectangle = new Rectangle(100, 100);
rectangle.setFill(Paint.valueOf("red"));
// 某键按下
button1.setOnKeyPressed(event -> {
    System.out.println("button1 setOnKeyPressed 事件类型: " + event.getEventType());
    System.out.println("button1 setOnKeyPressed 事件源: " + event.getSource());
    System.out.println("button1 setOnKeyPressed 事件目标: " + event.getTarget());
    System.out.println("button1 setOnKeyPressed 事件文本: " + event.getText());
    System.out.println("button1 setOnKeyPressed 按键: " + event.getCode());
    System.out.println("button1 setOnKeyPressed 是否Ctrl键: " + event.isControlDown());
});
// 某键弹起
button1.setOnKeyReleased(event -> System.out.println("button1 setOnKeyReleased 释放了: " + event.getCode()));
// 对输入框有用
textField.setOnKeyTyped(event -> System.out.println("textField setOnKeyTyped 输入内容: " + event.getCharacter()));
// 对图形监听按键需要先获取焦点
rectangle.setOnKeyPressed(event -> System.out.println("rectangle setOnKeyPressed 按键: " + event.getCode()));
rectangle.setOnMouseClicked(event -> rectangle.requestFocus());
```

#### 4、鼠标事件

##### ①基础事件

```java
// 鼠标单击事件
button1.setOnMouseClicked(event -> {
    /*
    System.out.println("相对于scene坐标 x = " + event.getSceneX() + ", y = " + event.getSceneY());
    System.out.println("相对于screen坐标 x = " + event.getScreenX() + ", y = " + event.getScreenY());
    System.out.println("相对于当前组件左上角坐标 x = " + event.getX() + ", y = " + event.getY());
    System.out.println("事件源: " + event.getSource());
    System.out.println("事件目标: " + event.getTarget());
    System.out.println("事件类型: " + event.getEventType());
    System.out.println("鼠标按键: " + event.getButton());
    System.out.println("Ctrl键是否按下: " + event.isControlDown());
    System.out.println("点击次数: " + event.getClickCount());
    */
    if (event.getClickCount() == 2 && event.getButton() == MouseButton.PRIMARY) {
        System.out.println("双击");
    }
});
// 鼠标按下事件
button1.setOnMousePressed(event -> System.out.println("鼠标按下"));
// 鼠标释放事件
button1.setOnMouseReleased(event -> {
    System.out.println("鼠标释放");
    // 在按下右键的同时点击左键有效
    System.out.println("鼠标右键是否被按下: " + event.isSecondaryButtonDown());
});
// 鼠标进入事件
button1.setOnMouseEntered(event -> System.out.println("鼠标进入"));
// 鼠标退出
button1.setOnMouseExited(event -> System.out.println("鼠标退出"));
// 鼠标移动事件
button1.setOnMouseMoved(event -> System.out.println("鼠标移动"));

Circle circle = new Circle(100, Color.RED);
// 设置为true后该组件所占区域内事件都有效
circle.setPickOnBounds(true);
// 阻止事件传递到子组件
circle.setMouseTransparent(true);

button1.setOnMousePressed(event -> {
    System.out.println("button1.setOnMousePressed");
    // 上一次点击和本次点击区域是否相同
    System.out.println(event.isStillSincePress());
});

circle.setOnMouseClicked(event -> System.out.println("circle.setOnMouseClicked"));
HBox hBox = new HBox(20, button1, button2, circle);
hBox.setOnMouseClicked(event -> {
    System.out.println("hBox.setOnMouseClicked");
    System.out.println(event.getPickResult());
});

KeyEvent keyEvent = new KeyEvent(button1, button2, KeyEvent.KEY_PRESSED, "a", "a", KeyCode.A, false, false, false, false);
button1.setOnMouseClicked(event -> {
    System.out.println("button1.setOnMouseClicked");
    // 点击button1时, 同时触发button2的相同的event事件
    // 不能循环使用, 否则会导致堆栈溢出
    // Event.fireEvent(button2, event);
    // Event.fireEvent(button2, keyEvent);
    // MouseEvent mouseEvent = event.copyFor(button2, button2);
    // System.out.println(mouseEvent);
    // Event.fireEvent(mouseEvent.getTarget(), mouseEvent);
    button2.fireEvent(keyEvent);
});
button2.setOnMouseClicked(event -> System.out.println("button2.setOnMouseClicked"));
button2.setOnKeyPressed(event -> System.out.println("button2.setOnKeyPressed"));
```

##### ②拖拽事件

```java
// 鼠标拖拽事件(只要在拖拽就有效)
button1.setOnMouseDragged(event -> System.out.println("button1 拖拽事件"));
// 拖拽检测
button1.setOnDragDetected(event -> {
    // 以该节点作为手势源，启动全按拖动释放手势。
    // 只能从DRAG_DETECTED鼠标事件处理程序调用此方法。
    button1.startFullDrag();
    System.out.println("button1 setOnDragDetected");
});
// 调用了startFullDrag()方法后该事件有效, 且鼠标指针在节点上时有效
button1.setOnMouseDragOver(event -> System.out.println("button1 setOnMouseDragOver"));
// 拖拽检测
button2.setOnDragDetected(event -> {
    // 以该节点作为手势源，启动全按拖动释放手势。
    // 只能从DRAG_DETECTED鼠标事件处理程序调用此方法。
    button2.startFullDrag();
});
// 拖拽进入, startFullDrag()执行后有效
button2.setOnMouseDragEntered(event -> System.out.println("button2 setOnMouseDragEntered"));
// 拖拽退出, startFullDrag()执行后有效
button2.setOnMouseDragExited(event -> System.out.println("button2 setOnMouseDragExited"));
// 拖拽释放, 鼠标指针在节点上时有效
button2.setOnMouseDragReleased(event -> System.out.println("button2 setOnMouseDragReleased"));
```

##### ③事件过滤和事件处理

- `filter`先于`handler`执行
- `button`天然可以阻止事件传递

```java
// 向该节点注册事件筛选器。当节点在事件传递的捕获阶段接收到指定类型的事件时，会调用筛选器。
//        button.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
//            System.out.println("button.addEventFilter");
//            System.out.println("事件源: " + event.getSource() + ", 事件目标: " + event.getTarget());
//        });
//
//        hBox.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
//            System.out.println("hBox.addEventFilter");
//            System.out.println("事件源: " + event.getSource() + ", 事件目标: " + event.getTarget());
//
//        });
//
//        // 事件从父组件向子组件传递
//        anchorPane.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
//            System.out.println("anchorPane.addEventFilter");
//            System.out.println("事件源: " + event.getSource() + ", 事件目标: " + event.getTarget());
//        });

// filter先于handler执行
// button天然可以阻止事件传递

// 向该节点注册事件处理程序。当节点在事件传递的冒泡阶段接收到指定类型的事件时，会调用处理程序。
label.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
    System.out.println("button.addEventHandler");
    System.out.println("事件源: " + event.getSource() + ", 事件目标: " + event.getTarget());

});

hBox.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
    System.out.println("hBox.addEventHandler");
    System.out.println("事件源: " + event.getSource() + ", 事件目标: " + event.getTarget());
    // 阻止事件传递
    event.consume();
});

// 事件从子组件向父组件传递
anchorPane.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
    System.out.println("anchorPane.addEventHandler");
    System.out.println("事件源: " + event.getSource() + ", 事件目标: " + event.getTarget());
});
```

##### ④鼠标拖动事件

###### 拖动按钮移动

```java
// 获取点击时的鼠标坐标
button.setOnMousePressed(event -> {
    cursorX = event.getX();
    cursorY = event.getY();
});
button.setOnMouseDragged(event -> {
    double x = event.getSceneX() - cursorX;
    double y = event.getSceneY() - cursorY;
    button.setLayoutX(x);
    button.setLayoutY(y);
});
```

###### 拖拽一个组件到另外一个组件上

```java
Label label = new Label("你好111111111111");
TextField textField = new TextField();

// 拖拽检测, 即拖拽开始
label.setOnDragDetected(event -> {
    // 拖动模式, 参数对代码没有影响, 实际拖拽时的效果会有不同
    Dragboard dragboard = label.startDragAndDrop(TransferMode.COPY_OR_MOVE);
    ClipboardContent content = new ClipboardContent();
    content.putString(label.getText());
    dragboard.setContent(content);
    // 更改拖拽的图标
    Text text = new Text(label.getText());
    WritableImage writableImage = new WritableImage((int) label.getWidth(), (int) label.getHeight());
    // 给文本截图
    text.snapshot(new SnapshotParameters(), writableImage);
    // 设置图标以及鼠标指针相对于图标的位置
    dragboard.setDragView(writableImage, event.getX(), event.getY());
});

// 进入到目标组件上
textField.setOnDragOver(event -> {
    // 此处设置的模式要和源头的相同且必须调用
    // 例如原来的COPY_OR_MOVE, 此处应该是COPY或者MOVE或者COPY_OR_MOVE
    event.acceptTransferModes(TransferMode.COPY);
});

// 拖拽结束事件
textField.setOnDragDropped(event -> {
    textField.setText(event.getDragboard().getString());
    // 拖拽事件完成(不是必须调用)
    event.setDropCompleted(true);
});

// 拖拽事件完成, setDropCompleted(true)方法执行后才有效
label.setOnDragDone(event -> {
    // 此处的值和上同理
    if (event.getTransferMode() == TransferMode.COPY) {
        label.setText("");
    }
});
```

###### 拖拽图片

```java
// 拖拽进入时出现外边框
hBox.setOnDragEntered(event -> {
    hBox.setBorder(new Border(
        new BorderStroke(
            Paint.valueOf("#f00"),
            BorderStrokeStyle.SOLID,
            CornerRadii.EMPTY,
            new BorderWidths(2),
            Insets.EMPTY
        )
    ));
    hBox.setBackground(null);
});

// 进入到目标组件上
hBox.setOnDragOver(event -> {
    event.acceptTransferModes(event.getTransferMode());
});

// 拖拽结束事件
hBox.setOnDragDropped(event -> {
    Dragboard dragboard = event.getDragboard();
    if (dragboard.hasFiles()) {
        File file = dragboard.getFiles().get(0);
        try {
            imageView.setImage(new Image(new FileInputStream(file)));
        } catch (FileNotFoundException ignored) {
        }
    } else if (dragboard.hasImage()) {
        imageView.setImage(dragboard.getImage());
    }
});

// 拖拽退出时取消边框
hBox.setOnDragExited(event -> hBox.setBorder(null));
```

###### 自定义拖拽

```java
package priv.zq;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lombok.Data;

import java.io.Serializable;

/**
 * @author huayunliufeng
 * @date_time 2023/8/21 16:21
 * @desc
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        AnchorPane anchorPane = new AnchorPane();
        Person person = new Person();
        person.setName("张三");
        person.setAge(20);
        person.setPhoto("favicon.png");

        VBox dataPane = getVBox();
        dataPane.setStyle("-fx-border-color: red;");
        // 创建截图
        VBox view = getVBox();
        setPersonValue(person, view);

        Button button = new Button(person.getName());
        // 自定义数据类型
        DataFormat personDataFormat = new DataFormat("data/person");
        // 拖拽检测
        button.setOnDragDetected(event -> {
            Dragboard dragboard = button.startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            content.put(personDataFormat, person);
            dragboard.setContent(content);
            // 设置图标
            anchorPane.getChildren().add(view);
            WritableImage writableImage = new WritableImage((int) view.getPrefWidth(), (int) view.getPrefHeight());
            view.snapshot(new SnapshotParameters(), writableImage);
            anchorPane.getChildren().remove(view);
            dragboard.setDragView(writableImage);

        });

        dataPane.setOnDragOver(event -> event.acceptTransferModes(TransferMode.MOVE));
        dataPane.setOnDragDropped(event -> {
            // 另外一种获取类型的方法: DataFormat.lookupMimeType("data/person");
            Person person1 = (Person) event.getDragboard().getContent(personDataFormat);
            setPersonValue(person1, dataPane);
        });

        anchorPane.getChildren().addAll(button, dataPane);
        AnchorPane.setLeftAnchor(dataPane, 100.0);
        AnchorPane.setTopAnchor(dataPane, 100.0);
        Scene scene = new Scene(anchorPane);
        stage.setScene(scene);
        // 布局
        stage.setTitle("JavaFX");
        stage.setWidth(800);
        stage.setHeight(800);
        stage.show();
    }

    private void setPersonValue(Person person, VBox view) {
        TextField textField1 = (TextField) view.getChildren().get(1);
        TextField textField2 = (TextField) view.getChildren().get(2);
        ImageView imageView = (ImageView) view.getChildren().get(3);
        textField1.setText(person.getName());
        textField2.setText(person.getAge());
        imageView.setImage(new Image(person.getPhoto()));
    }

    public static VBox getVBox() {
        VBox dataPane = new VBox(10);
        dataPane.setPadding(new Insets(10));
        dataPane.setPrefWidth(300);
        dataPane.setPrefHeight(500);
        Button button = new Button("个人详情");
        TextField textField1 = new TextField();
        TextField textField2 = new TextField();
        ImageView imageView = new ImageView();
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(dataPane.getPrefWidth());
        dataPane.getChildren().addAll(button, textField1, textField2, imageView);
        return dataPane;
    }
}

@Data
class Person implements Serializable {
    private String name;
    private int age;
    private String photo;

    public String getAge() {
        return String.valueOf(age);
    }

    public String getPhoto() {
        return Utils.getImgExternalForm(photo);
    }
}
```

#### 5、设置字体

```java
Text text1 = new Text("hello word");
Font courier = new Font("华文楷体", 25);
text1.setFont(courier);
// 设置填充颜色(文字颜色)
text1.setFill(Paint.valueOf("#20b2aa"));
// 设置描边颜色
text1.setStroke(Paint.valueOf("#cd5c5c"));
// 设置描边宽度
text1.setStrokeWidth(2);
// 是否抗锯齿
text1.setSmooth(true);
// 下划线
text1.setUnderline(true);
// 删除线
text1.setStrikethrough(true);
// 字体平滑
text1.setFontSmoothingType(FontSmoothingType.LCD);
// 设置多行对其方式
// text1.setTextAlignment(TextAlignment.CENTER);
// 行间距
// text1.setLineSpacing(20);
// 用像素限制宽度
// text1.setWrappingWidth(200);
// 设置原点位
// text1.setTextOrigin(VPos.CENTER);

Text text2 = new Text("hello word");
Font impact = new Font("Impact", 25);
text2.setFont(impact);

Text text3 = new Text("hello word");
// 从文件中加载字体
Font zapfino = Font.loadFont("file:/C:/Windows/Fonts/BRADHITC.TTF", 25);
text3.setFont(zapfino);

Text text4 = new Text("hello word默认");
// 获取系统所有字体
for (String family : Font.getFamilies()) {
    System.out.println(family);
}
```

#### 6、坐标系转换问题

###### 坐标转换

```java
// 有些组件有默认宽和高
Button button = new Button("button");
// 有些没有
// Rectangle rectangle = new Rectangle(100, 100);
System.out.println("button是否会被重新设置大小: " + button.isResizable());
// System.out.println("rectangle是否会被重新设置大小: " + rectangle.isResizable());
HBox hBox = new HBox(10, button);
hBox.setAlignment(Pos.CENTER);
hBox.setStyle("-fx-pref-width: 200px;-fx-pref-height: 200;-fx-background-color: #96a9d2;");
AnchorPane anchorPane = new AnchorPane(hBox);
AnchorPane.setLeftAnchor(hBox, 100.0);
AnchorPane.setTopAnchor(hBox, 100.0);
anchorPane.setStyle("-fx-background-color: #ccd7b6;");
Scene scene = new Scene(anchorPane);
stage.setScene(scene);
// 布局
stage.setTitle("JavaFX");
stage.setWidth(600);
stage.setHeight(400);
stage.show();

System.out.println("相对父组件X = " + button.getLayoutX() + ", 相对父组件Y = " + button.getLayoutY());
Bounds bounds = button.getLayoutBounds();
System.out.println("左上角X = " + bounds.getMinX() + ", 左上角Y = " + bounds.getMinY());
System.out.println("右下角X = " + bounds.getMaxX() + ", 右下角Y = " + bounds.getMaxY());
System.out.println("宽度 = " + bounds.getWidth() + ", 高度 = " + bounds.getHeight());
// 转换坐标系
Point2D point2D1 = button.localToParent(bounds.getMinX(), bounds.getMinY());
System.out.println("在hBox的坐标: x = " + point2D1.getX() + ", y = " + point2D1.getY());
Point2D point2D2 = button.localToScene(bounds.getMinX(), bounds.getMinY());
System.out.println("在scene的坐标: x = " + point2D2.getX() + ", y = " + point2D2.getY());
Point2D point2D3 = button.localToScreen(bounds.getMinX(), bounds.getMinY());
System.out.println("在整个屏幕的坐标: x = " + point2D3.getX() + ", y = " + point2D3.getY());
// 从父坐标系转换到local坐标系
Point2D point2D = button.parentToLocal(point2D1.getX(), point2D1.getY());
System.out.println("在本地的坐标: x = " + point2D.getX() + ", y = " + point2D.getY());
```

###### 碰撞判断

```java
Button button1 = new Button("button1");
Button button2 = new Button("button2");
AnchorPane anchorPane = new AnchorPane(button1, button2);
AnchorPane.setLeftAnchor(button2, 100.0);
AnchorPane.setTopAnchor(button2, 100.0);
Scene scene = new Scene(anchorPane);
stage.setScene(scene);
// 布局
stage.setTitle("JavaFX");
stage.setWidth(600);
stage.setHeight(400);
stage.show();
Bounds button2Bounds = button2.getLayoutBounds();
Point2D button2point2DMin = button2.localToParent(button2Bounds.getMinX(), button2Bounds.getMinY());
Point2D button2point2DMax = button2.localToParent(button2Bounds.getMaxX(), button2Bounds.getMaxY());
System.out.println(button2point2DMin);
System.out.println(button2point2DMax);
scene.setOnKeyPressed(new EventHandler<>() {
    final Bounds button1Bounds = button1.getLayoutBounds();
    Point2D button1point2DMin;
    Point2D button1point2DMax;
    int x = 0;
    int y = 0;
    final int steps = 5;
    @Override
    public void handle(KeyEvent event) {
        switch (event.getCode()) {
            case A: {
                x -= steps;
                break;
            }
            case D: {
                x += steps;
                break;
            }
            case W: {
                y -= steps;
                break;
            }
            case S: {
                y += steps;
                break;
            }
        }
        button1.setLayoutX(x);
        button1.setLayoutY(y);
        button1point2DMin = button1.localToParent(button1Bounds.getMinX(), button1Bounds.getMinY());
        button1point2DMax = button1.localToParent(button1Bounds.getMaxX(), button1Bounds.getMaxY());
        if (button1point2DMin.getX() >= button2point2DMin.getX()
            && button1point2DMin.getY() >= button2point2DMin.getY()
            && button1point2DMax.getX() <= button1point2DMax.getX()
            && button1point2DMax.getY() <= button1point2DMax.getY()) {
            System.out.println("进入");
        } else {
            System.out.println("退出");
        }
    }
});
```

#### 7、`FXML`

- 官网：https://openjfx.cn/javadoc/18/javafx.fxml/javafx/fxml/doc-files/introduction_to_fxml.html

##### ①基本使用

```xml
<?xml version="1.0" encoding="UTF-8"?>
<?import javafx.collections.FXCollections?>
<?import javafx.scene.control.Button?>
<?import javafx.scene.control.ComboBox?>
<?import javafx.scene.control.Label?>
<?import javafx.scene.control.ListView?>
<?import javafx.scene.image.Image?>
<?import javafx.scene.image.ImageView?>
<?import javafx.scene.layout.*?>
<?import org.kordamp.ikonli.javafx.FontIcon?>
<?import java.lang.*?>
<VBox xmlns="http://javafx.com/javafx"
      xmlns:fx="http://javafx.com/fxml"
      fx:controller="priv.zq.controller.MyController"
      spacing="10"
      alignment="TOP_CENTER">

    <Label text="这是一个Label" fx:id="label1"
           AnchorPane.leftAnchor="50"
           AnchorPane.topAnchor="50"/>
    <Button text="fxmlbutton"
            prefWidth="100"
            prefHeight="100"
            fx:id="button1"
            onAction="#action"
            AnchorPane.leftAnchor="100"
            AnchorPane.topAnchor="100">
        <graphic>
            <FontIcon iconLiteral="fab-accessible-icon"/>
        </graphic>
    </Button>
    <ImageView preserveRatio="true" fitWidth="100">
        <Image url="@/img5.jpg"/>
    </ImageView>
    <BorderPane
            prefWidth="50" prefHeight="100" style="-fx-background-color: #87a262;">
        <left>
            <ListView fx:id="listview">
                <items>
                    <FXCollections fx:factory="observableArrayList">
                        <String fx:value="data-1"/>
                        <String fx:value="data-2"/>
                        <String fx:value="data-3"/>
                        <String fx:value="data-4"/>
                        <String fx:value="data-5"/>
                    </FXCollections>
                </items>
            </ListView>
        </left>
        <right>
            <ComboBox>
                <items>
                    <FXCollections fx:factory="observableArrayList">
                        <String fx:value="data-1"/>
                        <String fx:value="data-2"/>
                        <String fx:value="data-3"/>
                        <String fx:value="data-4"/>
                        <String fx:value="data-5"/>
                    </FXCollections>
                </items>
            </ComboBox>
        </right>
    </BorderPane>
</VBox>
```

- 在代码中使用

```java
URL url = Main.class.getResource("/fxml/myfxml.fxml");
/*
//方式一
FXMLLoader fxmlLoader = new FXMLLoader();
fxmlLoader.setLocation(url);
AnchorPane anchorPane = fxmlLoader.load();
*/
// 方式二
assert url != null;
AnchorPane anchorPane = FXMLLoader.load(url);
// 获取组件
// 找到一个就返回
Button button1 = (Button) anchorPane.lookup("#button1");
button1.setOnAction(event -> System.out.println(button1.getText()));
```

##### ②控制器

```java
@Getter
public class MyController {
    @FXML
    // 属性名和fxml中的id相同
    private Button button1;
    @FXML
    private Label label1;
    @FXML
    private void initialize() {
        System.out.println("初始化完成");
        System.out.println(label1.getText());
        System.out.println(button1.getText());
    }
    @FXML
    // 如果是私有方法, 需要添加@FXML注解, public方法则不用
    private void action(ActionEvent event) {
        System.out.println("action 执行了");
    }
}
```

- 在`fxml`文件中指定

```xml
fx:controller="priv.zq.controller.MyController" // 指定控制器
onAction="#action" // 指定事件处理的方法
```

- 在`module-info.java`文件中要添加

```java
opens priv.zq.controller to javafx.fxml;
```

- 获取控制器

```java
// 获取控制器
MyController myController = fxmlLoader.getController();
myController.getButton1().setOnAction(event -> System.out.println("会覆盖fxml中指定的onAction事件"));
```

##### ③自定义`fxml`标签

```java
@Getter
@Setter
public class PersonBuilder implements Builder<Person> {
    private String name;
    private int age;
    private double score;
    @Override
    public Person build() {
        return new Person(name, age, score);
    }
}
```

```java
public class PersonBuilderFactory implements BuilderFactory {
    private final JavaFXBuilderFactory builderFactory = new JavaFXBuilderFactory();
    @Override
    public Builder<?> getBuilder(Class<?> type) {
        if (type == Person.class) {
            return new PersonBuilder();
        }
        return builderFactory.getBuilder(type);
    }
}
```

```java
<?import priv.zq.bean.Person?>
<?import java.util.ArrayList?>
<ArrayList>
    <Person name="张三" score="59.6" age="18"/>
    <Person name="李四" score="59.6" age="18"/>
    <Person name="王五" score="59.6" age="18"/>
</ArrayList>
```

```java
URL url = Main.class.getResource("/fxml/data1.fxml");
FXMLLoader fxmlLoader = new FXMLLoader();
fxmlLoader.setLocation(url);
fxmlLoader.setBuilderFactory(new PersonBuilderFactory());
List<Person> personList = fxmlLoader.load();
```

##### ④高级用法

```java
<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ © 2023 huayunliufeng保留所有权利, 依据GPL许可证发布。
  ~ 请勿更改或删除版权声明或此文件头。
  ~ 此代码是免费软件, 您可以重新分发和/或修改它。
  ~ 开源是希望它有用, 但不对代码做任何保证。
  ~ 如有疑问请联系: huayunliufeng@163.com
  -->

<?import javafx.geometry.Insets?>
<?import javafx.scene.control.*?>
<?import javafx.scene.layout.*?>
<AnchorPane xmlns="http://javafx.com/javafx"
            xmlns:fx="http://javafx.com/fxml"
            fx:controller="priv.zq.controller.MainController"
            prefHeight="400.0" prefWidth="600.0">
    <!--  定义内容  -->
    <fx:define>
        <ToggleGroup fx:id="group"/>
        <Insets fx:id="margin" topRightBottomLeft="10"/>
        <Button fx:id="button" text="button"/>
    </fx:define>
    <HBox alignment="CENTER" prefWidth="500" prefHeight="100"
          style="-fx-background-color: #b1c989;">
        <!--    使用定义的值    -->
        <RadioButton text="A" toggleGroup="$group"/>
        <RadioButton text="B" toggleGroup="$group"/>
        <RadioButton text="C" toggleGroup="$group"/>
        <Button text="button1" HBox.margin="$margin"/>
        <Button text="button2" HBox.margin="$margin"/>
        <!--    引用其他组件, 只能使用一次    -->
        <fx:reference source="button"/>
        <!--    导入其他fxml文件    -->
        <fx:include source="other.fxml"/>
    </HBox>

    <VBox alignment="CENTER" style="-fx-background-color: #a5d3d3;" prefWidth="100" prefHeight="300">
        <Button text="button1" fx:id="button1" prefWidth="200"/>
        <!--     绑定其它组件的值   -->
        <Button text="button2" prefWidth="${button1.prefWidth}"/>
    </VBox>

</AnchorPane>
```

#### 8、可视化编辑工具

- https://gluonhq.com/products/scene-builder/
- https://www.oracle.com/java/technologies/javafxscenebuilder-1x-archive-downloads.html
- https://openjfx.cn/scene-builder/#download

#### 9、国际化

##### ①创建对应语言的`properties`文件

- 例如`language_zh.properties`

```properties
name=按钮
```

##### ②使用

- 使用`%`加上`key`的名称

```xml
<Button text="%name"/>
```

```java
// 获取本机语言
Locale locale = Locale.getDefault();
System.out.println("语言: " + locale.getLanguage());
System.out.println("国家: " + locale.getCountry());
// 加载资源文件
ResourceBundle resourceBundle = ResourceBundle.getBundle("i18n/language");
```

- 可以不加语言后缀，自动识别

##### ③和控制器结合使用

```java
public class MainController implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println(location.getPath());
        System.out.println(resources.getLocale());
    }
}
```

#### 10、`CSS`

- 官网：https://openjfx.cn/javadoc/19/javafx.graphics/javafx/scene/doc-files/cssref.html

```java
/*id选择器*/
#anchorPane {
    -fx-background-color: #af7575;
}

/*类型选择器, 所有的button都会被影响*/
.button {
    -fx-font-size: 12px;
}

/*伪选择器*/
.button:hover {
    -fx-background-color: #79bb73;
}

/*类选择器, 在fxml中使用: styleClass="my-css", 在代码中使用: button.getStyleClass().add("my-css");*/
.my-css:hover {
    -fx-background-color: #875cb4;
}
```

```java
Scene scene = new Scene(fxmlLoader.load());
// 设置样式表
URL cssFile = Main.class.getResource("/css/main.css");
assert cssFile != null;
scene.getStylesheets().add(cssFile.toExternalForm());
```

#### 11、自适应布局

- 编码方式

```java
public void start(Stage primaryStage) throws Exception {
    double width = 800;
    double height = 600;
    // 布局
    AnchorPane anchorPane = new AnchorPane();
    Scene scene = new Scene(anchorPane);
    primaryStage.setScene(scene);
    primaryStage.setTitle("JavaFX");
    primaryStage.setWidth(width);
    primaryStage.setHeight(height);
    primaryStage.show();

    HBox top = new HBox();
    top.setStyle("-fx-background-color: #d0e7cc;");
    // 绑定父组件高度的0.15
    top.prefHeightProperty().bind(anchorPane.heightProperty().multiply(0.15));
    // 绑定父组件的宽度
    top.prefWidthProperty().bind(anchorPane.widthProperty());

    VBox centerLeft = new VBox();
    centerLeft.setStyle("-fx-background-color: #786991;");
    // 绑定父组件高度的0.75
    centerLeft.prefHeightProperty().bind(anchorPane.heightProperty().multiply(0.75));
    // 绑定父组件宽度的0.3
    centerLeft.prefWidthProperty().bind(anchorPane.widthProperty().multiply(0.3));

    VBox centerRight = new VBox();
    centerRight.setStyle("-fx-background-color: #8f95b6;");
    // 绑定父组件高度的0.75
    centerRight.prefHeightProperty().bind(anchorPane.heightProperty().multiply(0.75));
    // 绑定父组件宽度的0.7
    centerRight.prefWidthProperty().bind(anchorPane.widthProperty().multiply(0.7));
    HBox center = new HBox(centerLeft, centerRight);

    HBox bottom = new HBox();
    bottom.setStyle("-fx-background-color: #b6b298;");
    // 绑定父组件高度的0.1
    bottom.prefHeightProperty().bind(anchorPane.heightProperty().multiply(0.1));
    // 绑定父组件的宽度
    bottom.prefWidthProperty().bind(anchorPane.widthProperty());

    VBox vBox = new VBox(top, center, bottom);
    anchorPane.getChildren().addAll(vBox);
}
```

- `fxml`

```java
<?xml version="1.0" encoding="UTF-8"?>
<?import javafx.scene.control.Button?>
<?import javafx.scene.control.Label?>
<?import javafx.scene.layout.AnchorPane?>
<AnchorPane xmlns="http://javafx.com/javafx"
            xmlns:fx="http://javafx.com/fxml"
            prefHeight="600.0" prefWidth="800.0"
            fx:id="root">
    <Label fx:id="label" AnchorPane.topAnchor="100" AnchorPane.leftAnchor="100" text="label"/>

    <!--  绑定属性, 可附带自定义计算  -->
    <Button prefWidth="${root.width / 2 + 100}" fx:id="button" AnchorPane.topAnchor="200" text="${label.text}"/>
</AnchorPane>
```

#### 12、加载`swing`组件

```java
SwingNode swingNode = new SwingNode();
SwingUtilities.invokeLater(()->{
    JPanel root = new JPanel();
    JButton button1 = new JButton("button1");
    JButton button2 = new JButton("button2");
    root.add(button1);
    root.add(button2);
    FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER, 100, 100);
    root.setLayout(flowLayout);
    button1.addActionListener(e -> System.out.println("swing button1"));
    button2.addActionListener(e -> System.out.println("swing button2"));
    swingNode.setContent(root);
});
anchorPane.getChildren().addAll(swingNode);
```

#### 13、自定义组件

- 示例

```java
class MyNode extends VBox {
    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        ObservableList<Node> children = this.getChildren();
        for (int i = 0; i < children.size(); i++) {
            if (i % 2 == 0) {
                children.get(i).setTranslateX(20);
            } else {
                children.get(i).setTranslateX(-20);
            }
        }
    }
}
```

#### 14、系统托盘

- 若果遇到中文菜单乱码，加上`VM`参数：`-Dfile.encoding=gbk`

```java
public void start(Stage primaryStage) throws Exception {
    double width = 800;
    double height = 600;
    AnchorPane anchorPane = new AnchorPane();
    Scene scene = new Scene(anchorPane);
    primaryStage.setScene(scene);
    primaryStage.setTitle("JavaFX");
    primaryStage.setWidth(width);
    primaryStage.setHeight(height);
    primaryStage.show();
    Button button1 = new Button("button1");
    Button button2 = new Button("button2");
    HBox hBox = new HBox(20, button1, button2);
    // 设置关闭窗口不退出
    Platform.setImplicitExit(false);
    button1.setOnAction(event -> {
        // 获取系统托盘
        SystemTray systemTray = SystemTray.getSystemTray();
        Image image = Toolkit.getDefaultToolkit().getImage(App.class.getResource("/img/favicon.png"));
        String tooltip = "JavaFX系统托盘";
        PopupMenu popupMenu = new PopupMenu();
        MenuItem menuItem1 = new MenuItem("显示");
        MenuItem menuItem2 = new MenuItem("退出");
        popupMenu.add(menuItem1);
        popupMenu.add(menuItem2);
        TrayIcon trayIcon = new TrayIcon(image, tooltip, popupMenu);
        try {
            systemTray.add(trayIcon);
        } catch (AWTException e) {
            throw new RuntimeException(e);
        }
        menuItem1.addActionListener((e) -> {
            systemTray.remove(trayIcon);
            Platform.runLater(primaryStage::show);
        });
        menuItem2.addActionListener((e) -> {
            systemTray.remove(trayIcon);
            Platform.exit();
        });
        primaryStage.hide();
    });
    primaryStage.setOnCloseRequest(event -> {
        Platform.setImplicitExit(true);
    });
    anchorPane.getChildren().addAll(hBox);
}
```

### 四、组件效果

#### 1、变换

##### ①位移

```java
Translate translate1 = new Translate(100, 100);
Translate translate2 = new Translate(100, 100);
button2.getTransforms().addAll(translate1, translate2);
System.out.println(button2.getLayoutX());
System.out.println(button2.getLocalToParentTransform().getTx());
```

##### ②缩放

```java
// x轴缩放比例, y轴缩放比例和基于某个点的坐标缩放
Scale scale = new Scale(0.5, 0.5, 0, 0);
button2.getTransforms().add(scale);
// 也可以通过set方法设置
//        button2.setScaleX(0.5);
//        button2.setScaleY(0.5);

// 缩放后这种方式获取的是原来的宽和高
//System.out.println(button2.getPrefWidth());
//System.out.println(button2.getWidth());
// 通过这种方式获取实际宽和高
Bounds layoutBounds = button2.getBoundsInParent();
System.out.println(layoutBounds.getMaxX() - layoutBounds.getMinX());
System.out.println(layoutBounds.getWidth());
```

##### ③旋转

```java
// 缩放角度和旋转的中心点
Rotate rotate = new Rotate(45,50,50);
button2.getTransforms().add(rotate);
AnchorPane an = new AnchorPane(button1, button2);
an.setStyle("-fx-pref-width: 300px;-fx-pref-height: 300px;" +
            "-fx-border-color: red;" +
            "-fx-border-width: 1px;");
// 该布局内所有组件跟着一起旋转, 并且基于中心点进行旋转
an.setRotate(45);
```

##### ④错切

```java
// 变形
Shear shear = new Shear(0.3, 0.4,200,100);
button2.getTransforms().add(shear);
```

##### ⑤仿射

```java
// 镜像效果
Scale scale = new Scale(1, -1, 200, 100);
Translate translate = new Translate(0,-50);
button2.getTransforms().addAll(scale, translate);
```

#### 2、`Paint`类

##### ①颜色填充

```java
GridPane gridPane = new GridPane();
gridPane.setHgap(10);
gridPane.setVgap(10);
for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
        Rectangle rectangle = new Rectangle((width - 100) / size, (height - 100) / size);
        rectangle.setFill(Paint.valueOf(Utils.getRandomColor(true)));
        gridPane.add(rectangle, i, j);
    }
}
```

##### ②`ImagePattern`图片填充

```java
GridPane gridPane = new GridPane();
gridPane.setHgap(10);
gridPane.setVgap(10);
for (int i = 0; i < size; i++) {
    Rectangle rectangle = new Rectangle(100, 100);
    rectangle.setFill(Paint.valueOf(Utils.getRandomColor(true)));
    gridPane.add(rectangle, i, 0);
}
Circle circle = new Circle(100);
Polygon polygon = new Polygon(100, 0, 0, 200, 200, 200);
// 图片填充
((Rectangle) gridPane.getChildren().get(0)).setFill(new ImagePattern(new Image(Utils.getImgExternalForm("img1.jpg"))));
((Rectangle) gridPane.getChildren().get(1)).setFill(new ImagePattern(new Image(Utils.getImgExternalForm("img2.jpg")), 0, 0, 0.5, 0.5, true));
((Rectangle) gridPane.getChildren().get(2)).setFill(new ImagePattern(new Image(Utils.getImgExternalForm("img3.jpg")), 0, 0, 25, 25, false));
((Rectangle) gridPane.getChildren().get(3)).setFill(new ImagePattern(new Image(Utils.getImgExternalForm("img4.jpg")), 50, 50, 25, 25, false));
((Rectangle) gridPane.getChildren().get(4)).setFill(new ImagePattern(new Image(Utils.getImgExternalForm("img5.jpg")), 50, 25, 50, 50, false));
circle.setFill(new ImagePattern(new Image(Utils.getImgExternalForm("img5.jpg")), 50, 50, 50, 50, false));
polygon.setFill(new ImagePattern(new Image(Utils.getImgExternalForm("img5.jpg")), 0, 50, 100, 100, false));
```

##### ③`LinearGradient`线性渐变

```java
for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
        Rectangle rectangle = new Rectangle(200, 200);
        Stop[] stops = new Stop[]{new Stop(0, Color.valueOf(Utils.getRandomColor(false)))
            , new Stop(0.5, Color.valueOf(Utils.getRandomColor(false)))
            , new Stop(1, Color.valueOf(Utils.getRandomColor(false)))};
        LinearGradient linearGradient = new LinearGradient(0, 0, 1, 1, true, CycleMethod.NO_CYCLE, stops);
        rectangle.setFill(linearGradient);
        gridPane.add(rectangle, i, j);
    }
}
```

##### ④`RadialGradient`圆形径向渐变

```java
for (int i = 0; i < size + 1; i++) {
    for (int j = 0; j < size; j++) {
        Rectangle rectangle = new Rectangle(200, 200);
        Stop[] stops = new Stop[]{new Stop(0, Color.valueOf(Utils.getRandomColor(true)))
            , new Stop(0.5, Color.valueOf(Utils.getRandomColor(true)))
            , new Stop(1, Color.valueOf(Utils.getRandomColor(true)))};
        RadialGradient radialGradient = new RadialGradient(
            45, 1, Utils.getIntRandom(0, 200), Utils.getIntRandom(0, 200), Utils.getIntRandom(0, 200), false, CycleMethod.NO_CYCLE, stops
        );
        rectangle.setFill(radialGradient);
        gridPane.add(rectangle, i, j);
    }
}
```

#### 3、`Effect`效果

##### ①外阴影`DropShadow`

```java
public Effect getEffect() {
    DropShadow dropShadow = new DropShadow();
    dropShadow.setColor(Color.valueOf(Utils.getRandomColor(true)));
    // 设置阴影大小
    //dropShadow.setWidth(50);
    //dropShadow.setHeight(50);
    // 设置阴影半径
    //dropShadow.setRadius(20);
    // 设置模糊程度
    dropShadow.setSpread(0.5);
    // 设置阴影位移
    dropShadow.setOffsetX(10);
    dropShadow.setOffsetY(10);
    // 设置模糊效果
    dropShadow.setBlurType(BlurType.GAUSSIAN);
    return dropShadow;
}
// 父组件会影响子组件
hBox.setEffect(getEffect());
```

##### ②内阴影`InnerShadow`

```java
public Effect getEffect2() {
    InnerShadow innerShadow = new InnerShadow();
    innerShadow.setColor(Color.valueOf(Utils.getRandomColor(false)));
    // 设置阴影大小
    innerShadow.setWidth(50);
    innerShadow.setHeight(50);
    // 设置阴影位移
    innerShadow.setOffsetX(10);
    innerShadow.setOffsetY(10);
    // 设置阴影半径
    innerShadow.setRadius(20);
    // 设置模糊程度
    innerShadow.setChoke(0.5);
    // 组合效果
    innerShadow.setInput(getEffect1());
    return innerShadow;
}
```

##### ③普通阴影`Shadow`

- 将组件变模糊

```java
public Effect getEffect3() {
    Shadow shadow = new Shadow();
    shadow.setColor(Color.valueOf(Utils.getRandomColor(false)));
    // 设置阴影大小
    shadow.setWidth(50);
    shadow.setHeight(50);
    return shadow;
}
```

##### ④方框模糊`BoxBlur`

```java
public Effect getEffect4() {
    BoxBlur boxBlur = new BoxBlur();
    // 设置模糊范围
    boxBlur.setWidth(100);
    boxBlur.setHeight(100);
    // 设置模糊程度
    boxBlur.setIterations(1);
    return boxBlur;
}
```

##### ⑤高斯模糊`GaussianBlur`

```java
public Effect getEffect5() {
    GaussianBlur gaussianBlur = new GaussianBlur();
    // 模糊半径(0-63)
    gaussianBlur.setRadius(20);
    return gaussianBlur;
}
```

##### ⑥动感模糊`MotionBlur`

```java
public Effect getEffect6() {
    MotionBlur motionBlur = new MotionBlur();
    // 设置角度
    motionBlur.setAngle(-45);
    // 设置范围
    motionBlur.setRadius(100);
    return motionBlur;
}
```

##### ⑦发光效果

###### `Bloom`

- 基于可配置的阈值，使输入图像的较亮部分看起来发光的高级效果。

```java
public Effect getEffect7() {
    Bloom bloom = new Bloom();
    // 阈值, 0-1
    bloom.setThreshold(0.5);
    return bloom;
}
```

###### `Glow`

- 基于可配置的阈值，使输入图像看起来发光的高级效果。

```java
public Effect getEffect8() {
    Glow glow = new Glow();
    // 级别, 0-1
    glow.setLevel(0);
    return glow;
}
```

##### ⑧棕褐色调`SepiaTone`

- 怀旧风格

```java
public Effect getEffect9() {
    SepiaTone sepiaTone = new SepiaTone();
    // 设置级别, 0-1
    sepiaTone.setLevel(0.5);
    return sepiaTone;
}
```

##### ⑨反射效果`Reflection`

- 类似倒影效果

```java
public Effect getEffect10() {
    Reflection reflection = new Reflection();
    // 倒影离原图的距离
    reflection.setTopOffset(10);
    // 倒影的范围, 0-1
    reflection.setFraction(1);
    // 设置上部分透明度
    reflection.setTopOpacity(1);
    // 设置下部分透明度
    reflection.setBottomOpacity(0);
    return reflection;
}
```

##### ⑩置换贴图效果`DisplacementMap`

```java
public Effect getEffect11() {
    DisplacementMap displacementMap = new DisplacementMap();
    displacementMap.setOffsetX(0.3);
    displacementMap.setOffsetY(0.3);
    displacementMap.setWrap(true);
    int w = 100, h = 100;
    FloatMap floatMap = new FloatMap(w, h);
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            float temp;
            if (j < h / 2) {
                temp = 0.5f;
            } else {
                temp = -0.5f;
            }
            floatMap.setSamples(i, j, temp, temp);
        }
    }
    displacementMap.setMapData(floatMap);
    displacementMap.setScaleX(0.5);
    displacementMap.setScaleY(0.5);
    return displacementMap;
}
```

##### ⑪矩形区域颜色填充`ColorInput`

- 将所有类型的组件填充为一个矩形

```java
public Effect getEffect12() {
    ColorInput colorInput = new ColorInput();
    colorInput.setWidth(100);
    colorInput.setHeight(100);
    colorInput.setX(0);
    colorInput.setY(0);
    colorInput.setPaint(Paint.valueOf(Utils.getRandomColor(false)));
    return colorInput;
}
```

##### ⑫颜色调整`ColorAdjust`

```java
public Effect getEffect13() {
    ColorAdjust colorAdjust = new ColorAdjust();
    // 设置色调
    colorAdjust.setHue(80);
    // 设置饱和度
    colorAdjust.setSaturation(0.5);
    // 设置亮度
    colorAdjust.setBrightness(0.5);
    // 设置对比度
    colorAdjust.setContrast(-1);
    return colorAdjust;
}
```

##### ⑬显示图像`ImageInput`

- 将原节点使用指定的图片代替

```java
public Effect getEffect14() {
    ImageInput imageInput = new ImageInput();
    imageInput.setX(0);
    imageInput.setY(0);
    imageInput.setSource(new Image(Utils.getImgExternalForm("img1.jpg")));
    return imageInput;
}
```

##### ⑭透视变换`PerspectiveTransform`

```java
public Effect getEffect15() {
    PerspectiveTransform pt = new PerspectiveTransform();
    // 左上角
    pt.setUlx(-20);
    pt.setUly(20);
    // 右上角
    pt.setUrx(100);
    pt.setUry(20);
    // 左下角
    pt.setLlx(0);
    pt.setLly(120);
    // 右下角
    pt.setLrx(100);
    pt.setLry(80);
    return pt;
}
```

##### ⑮图层混合`Blend`

```java
public Effect getEffect16() {
    Blend blend = new Blend();
    // 设置混合模式
    blend.setMode(BlendMode.EXCLUSION);
    // 设置不透明度
    blend.setOpacity(0.5);
    // 设置上面图层的效果
    blend.setTopInput(getEffect2());
    // 设置下面图层的效果
    blend.setBottomInput(getEffect3());
    return blend;
}
```

##### ⑯光照效果`Lighting`

```java
public Effect getEffect17() {
    Light.Distant light = new Light.Distant();
    // 设置光源方位
    light.setAzimuth(-45.0);
    // 设置光的颜色
    light.setColor(Color.valueOf(Utils.getRandomColor(false)));
    // 光的高度
    light.setElevation(90);
    Lighting lighting = new Lighting();
    // 指定光源
    lighting.setLight(light);
    // 立体程度, 0-10
    lighting.setSurfaceScale(5.0);
    // 漫反射程度, 0-2
    lighting.setDiffuseConstant(2);
    // 镜面常数, 0-2
    lighting.setSpecularConstant(2);
    // 镜面指数, 0-40
    //lighting.setSpecularExponent(2);
    // 可选的凹凸贴图输入。如果未指定，将从默认输入自动生成凹凸贴图。
    // 如果设置为null，或者不指定，则效果所附加的节点的图形图像将用于生成默认凹凸贴图。
    // lighting.setBumpInput();
    return lighting;
}
```

#### 4、毛玻璃效果

```java
public void start(Stage primaryStage) throws Exception {
    double width = 800;
    double height = 600;
    AnchorPane anchorPane = new AnchorPane();
    Scene scene = new Scene(anchorPane);
    primaryStage.setScene(scene);
    primaryStage.setTitle("JavaFX");
    primaryStage.setWidth(width);
    primaryStage.setHeight(height);
    primaryStage.show();
    Button button1 = new Button("展开");
    Button button2 = new Button("收起");
    ImageView imageView = new ImageView(App.class.getResource("/img/img1.jpg").toExternalForm());
    imageView.fitWidthProperty().bind(anchorPane.widthProperty());
    imageView.fitHeightProperty().bind(anchorPane.heightProperty());
    HBox hBox = new HBox(20, button1, button2);
    Node node = getView(anchorPane);
    AnchorPane.setLeftAnchor(hBox, 200.0);
    AnchorPane ap = new AnchorPane(imageView, hBox, node);
    node.translateXProperty().addListener((observable, oldValue, newValue) -> {
        int w = (int)(200 + newValue.doubleValue());
        int h = (int)node.prefHeight(-1);
        if (w > 0) {
            WritableImage wi = new WritableImage(w, h);
            imageView.snapshot(new SnapshotParameters(), wi);
            iv.setImage(wi);
        }
    });
    TranslateTransition tt = new TranslateTransition(Duration.seconds(1), node);
    tt.setInterpolator(Interpolator.EASE_OUT);
    button1.setOnAction(event -> {
        if (!flag) {
            flag = true;
            tt.setFromX(-200);
            tt.setToX(0);
            tt.play();
        }
    });
    button2.setOnAction(event -> {
        if (flag) {
            flag = false;
            tt.setFromX(0);
            tt.setToX(-200);
            tt.play();
        }
    });
    anchorPane.getChildren().addAll(ap);
}
private Node getView(Pane parent) {
    VBox vBox = new VBox(20, Utils.getTextList(5));
    vBox.setStyle("-fx-background-color: #ffffff45;");
    vBox.setPrefWidth(200);
    vBox.prefHeightProperty().bind(parent.heightProperty());
    iv = new ImageView();
    iv.setEffect(new GaussianBlur(10));
    AnchorPane ap = new AnchorPane(iv);
    AnchorPane.setRightAnchor(iv, 0.0);
    StackPane stackPane = new StackPane(ap, vBox);
    stackPane.setTranslateX(-200);
    return stackPane;
}
```

### 五、`2D/3D`图形

##### ①线条`Line`

```java
// 开始和结束位置
Line line1 = new Line(0, 0, 200, 200);
// 设置颜色
line1.setStroke(Paint.valueOf(Utils.getRandomColor(false)));
// 设置粗细
line1.setStrokeWidth(5);
// 顶点类型
line1.setStrokeLineCap(StrokeLineCap.ROUND);
// 抗锯齿
line1.setSmooth(true);
// 虚线效果
line1.getStrokeDashArray().addAll(10.0, 20.0, 30.0);
// 设置位移量
line1.setStrokeDashOffset(5);
```

##### ②连接线`Polyline`

```java
Polyline polyline = new Polyline(0, 0, 100, 100, 100, 0, 0, 100, 0, 0);
// 设置宽度
polyline.setStrokeWidth(5);
// 设置颜色
polyline.setStroke(Utils.getRandomPaintColor(false));
// 填充
polyline.setFill(Utils.getRandomPaintColor(false));
// 设置圆角
// polyline.setStrokeLineJoin(StrokeLineJoin.ROUND);
// polyline.setStrokeLineCap(StrokeLineCap.ROUND);
// 设置转角的比率
//polyline.setStrokeMiterLimit(5);
// 定义围绕“形状”节点的边界绘制笔划的位置。
//polyline.setStrokeType(StrokeType.INSIDE);
// 设置虚线
polyline.getStrokeDashArray().addAll(20.0, 20.0);
```

##### ③矩形`Rectangle`

```java
Rectangle rectangle = new Rectangle();
// 左上角坐标
rectangle.setX(50);
rectangle.setY(50);
// 宽度
rectangle.setWidth(100);
// 高度
rectangle.setHeight(100);
// 填充颜色
rectangle.setFill(Utils.getRandomPaintColor(true));
// 设置圆角
rectangle.setArcWidth(100);
rectangle.setArcHeight(100);
// 设置边框
rectangle.setStrokeWidth(10);
rectangle.setStroke(Utils.getRandomPaintColor(true));
// 设置边框类型
rectangle.setStrokeType(StrokeType.CENTERED);
// 设置边框变为虚线
rectangle.getStrokeDashArray().addAll(30.0, 30.0);
```

##### ④圆形`Circle`

```java
Circle circle = new Circle();
// 设置半径
circle.setRadius(100);
// 设置圆心坐标
circle.setCenterX(120);
circle.setCenterY(120);
circle.setFill(Utils.getRandomPaintColor(false));
// 设置边框
circle.setStrokeWidth(10);
circle.setStroke(Utils.getRandomPaintColor(false));
circle.setStrokeType(StrokeType.CENTERED);
// 是否平滑
circle.setSmooth(true);
// 添加虚线
circle.getStrokeDashArray().addAll(30.0, 30.0);
```

##### ⑤椭圆形`Ellipse`

```java
Ellipse ellipse = new Ellipse();
// 设置中心点坐标
ellipse.setCenterX(120);
ellipse.setCenterY(120);
ellipse.setRadiusX(100);
ellipse.setRadiusY(50);
ellipse.setFill(Utils.getRandomPaintColor(false));
// 设置边框
ellipse.setStroke(Utils.getRandomPaintColor(false));
ellipse.setStrokeType(StrokeType.CENTERED);
ellipse.setStrokeWidth(10);
// 设置虚线
ellipse.getStrokeDashArray().addAll(30.0, 30.0);
```

##### ⑥扇形`Arc`

```java
Arc arc = new Arc();
arc.setCenterX(120);
arc.setCenterY(120);
arc.setRadiusX(50);
arc.setRadiusY(100);
// 扇形的角度
arc.setLength(270);
// 开始的角度
arc.setStartAngle(90);
// 开合类型
arc.setType(ArcType.ROUND);
arc.setFill(Utils.getRandomPaintColor(false));
// 设置边框
arc.setStroke(Utils.getRandomPaintColor(false));
arc.setStrokeType(StrokeType.CENTERED);
arc.setStrokeWidth(5);
// 设置虚线
arc.getStrokeDashArray().addAll(30.0, 30.0);
```

##### ⑦多边形`Polygon`

```java
Polygon polygon = new Polygon(50, 50, 50, 150, 150, 50);
polygon.setFill(Utils.getRandomPaintColor(false));
// 设置边框
polygon.setStroke(Utils.getRandomPaintColor(false));
polygon.setStrokeType(StrokeType.CENTERED);
polygon.setStrokeWidth(5);
// 设置虚线
polygon.getStrokeDashArray().addAll(30.0, 30.0);
```

##### ⑧曲线

###### 二次曲线`QuadCurve`

```java
QuadCurve quadCurve = new QuadCurve();
quadCurve.setStartX(0.0f);
quadCurve.setStartY(50.0f);
quadCurve.setEndX(50.0f);
quadCurve.setEndY(50.0f);
quadCurve.setControlX(25.0f);
quadCurve.setControlY(0.0f);
quadCurve.setFill(Utils.getRandomPaintColor(false));

quadCurve.setStrokeWidth(3);
quadCurve.setStroke(Utils.getRandomPaintColor(false));
```

###### 三次曲线`CubicCurve`

```java
CubicCurve cubicCurve = new CubicCurve();
cubicCurve.setStartX(0.0f);
cubicCurve.setStartY(50.0f);
cubicCurve.setControlX1(25.0f);
cubicCurve.setControlY1(0.0f);
cubicCurve.setControlX2(75.0f);
cubicCurve.setControlY2(100.0f);
cubicCurve.setEndX(100.0f);
cubicCurve.setEndY(50.0f);
cubicCurve.setFill(Utils.getRandomPaintColor(false));
cubicCurve.setStrokeWidth(3);
cubicCurve.setStroke(Utils.getRandomPaintColor(false));
```

##### ⑨路径`Path`

```java
// 可重复使用
PathElement[] pathElements = {
    new MoveTo(100, 100), new LineTo(200, 0),
    new QuadCurveTo(0, 0, 170, 200),
    new HLineTo(100), new VLineTo(100),
    new CubicCurveTo(50, -50, 100, -100, 200, 0),
    new ArcTo(100, 50, 0, 100, 100, true, false),
    new ClosePath()
};
Path path = new Path(pathElements);
path.setFill(Utils.getRandomPaintColor(false));
path.setStrokeWidth(3);
path.setStroke(Utils.getRandomPaintColor(false));
path.getStrokeDashArray().addAll(30.0, 30.0);
// 使用.setAbsolute(false)方法改为相对路径
```

##### ⑩`SVGPath`路径

###### 语法

- M = moveto

  M x y 移动到指定坐标，xy分别为x轴和y轴的坐标点，类似画笔的起点。

  path中的起点，必须存在（文档中虽然没有提到过，但是path的其他命令都需要依赖一个初始位置，而实际操作过程中也没有需要到可以不使用M的情况，后面发现有例外我再过来补充。

- L = lineto

  L x y 在初始位置（M 画的起点）和xy确定的坐标画一条线。
  两点一线，直线，绘图中很常见的方式。

- H = horizontal lineto

  H x 沿着x轴移动一段位置

- V = vertical lineto

  V y 沿着y轴移动一段位置

- C = curveto

  C x1 y1 x2 y2 x y
  三次贝塞尔曲线
  当前点为起点，xy为终点，起点和x1y1控制曲线起始的斜率，终点和x2y2控制结束的斜率。

- S = smooth curveto

  S x2 y2 x y
  简化的贝塞尔曲线
  1.如果S命令跟在一个C命令或者另一个S命令的后面，它的第一个控制点，就会被假设成前一个控制点的对称点。

  2.如果S命令单独使用，前面没有C命令或者另一个S命令，那么它的两个控制点就会被假设为同一个点。

- Q = quadratic Bézier curve

  Q x1 y1 x y
  二次贝塞尔曲线Q
  只需要一个控制点，用来确定起点和终点的曲线斜率。因此它需要两组参数，控制点和终点坐标。

  T = smooth quadratic Bézier curveto

  Q命令的简写命令。</br>
  与S命令相似，T也会通过前一个控制点，推断出一个新的控制点。
  1.T命令前面必须是一个Q命令，或者是另一个T命令

  2.如果T单独使用，那么控制点就会被认为和终点是同一个点，所以画出来的将是一条直线

- A = elliptical Arc

  A rx,ry x-axis-rotation large-arc-flag sweep-flag x,y
  rx 弧的半长轴长度
  ry 弧的半短轴长度
  x-axis-rotation 是此段弧所在的x轴与水平方向的夹角，即x轴的逆时针旋转角度，负数代表顺时针旋转角度。
  large-arc-flag 为1表示大角度弧线，0表示小角度弧线
  sweep-flag 为1表示从起点到终点弧线绕中心顺时针方向，0表示逆时针方向。
  xy 是终点坐标。

- Z = closepath

  从当前位置到起点画一条直线闭合。

```java
SVGPath svgPath1 = new SVGPath();
// 大写表示绝对坐标, 小写表示相对坐标
svgPath1.setContent("m0,0 l100,100 v100 h-100 z");
setAttribute(svgPath1);
SVGPath svgPath2 = new SVGPath();
svgPath2.setContent("m0,0 c50,-50,150,50,200,0 z");
setAttribute(svgPath2);
SVGPath svgPath3 = new SVGPath();
svgPath3.setContent("m0,0 s50,-50,100,0 z");
setAttribute(svgPath3);
SVGPath svgPath4 = new SVGPath();
svgPath4.setContent("m0,0 q100,50,200,0 t200,0 z");
setAttribute(svgPath4);
SVGPath svgPath5 = new SVGPath();
svgPath5.setContent("m0,0 a100,100,0,1,1,100,100 z");
setAttribute(svgPath5);
HBox hBox = new HBox(svgPath1, svgPath2, svgPath3, svgPath4, svgPath5);
```

##### ⑪图形计算

```java
// 交集
Shape intersect = Shape.intersect(circle, rectangle);
intersect.setFill(Utils.getRandomPaintColor(true));
// 差集
Shape subtract1 = Shape.subtract(circle, rectangle);
subtract1.setFill(Utils.getRandomPaintColor(true));
Shape subtract2 = Shape.subtract(rectangle, circle);
subtract2.setFill(Utils.getRandomPaintColor(true));
// 合并
Shape union = Shape.union(circle, rectangle);
union.setFill(Utils.getRandomPaintColor(true));
```

##### ⑫画布`Canvas`

```java
Canvas canvas = new Canvas(width, height);
GraphicsContext context2D = canvas.getGraphicsContext2D();
// 设置填充颜色
context2D.setFill(Utils.getRandomPaintColor(false));
// 设置边框颜色
context2D.setStroke(Utils.getRandomPaintColor(false));
// 设置画笔粗细(必须先设置属性才能有效果)
context2D.setLineWidth(5);
context2D.fillRect(50, 50, 100, 100);
context2D.strokeRect(100, 100, 100, 100);
double[] xPoints = {250, 350, 300};
double[] yPoints = {100, 100, 0};
context2D.fillPolygon(xPoints, yPoints, 3);
context2D.setFont(Font.font(50));
context2D.fillText("Hello Word!", 200, 200);
context2D.setLineWidth(2);
context2D.strokeText("Hello Word!", 200, 300);
// 擦除
context2D.clearRect(150, 150, 100, 100);
```

```java
Canvas canvas = new Canvas(width, height);
GraphicsContext context2D = canvas.getGraphicsContext2D();
context2D.beginPath();
context2D.moveTo(0,0);
context2D.rotate(45);
context2D.lineTo(100,100);
context2D.closePath();
context2D.stroke();
```

##### ⑬`3D`坐标空间

```java
public void start(Stage primaryStage) throws Exception {
    double width = 600;
    double height = 400;
    // 是否支持3D效果
    System.out.println(Platform.isSupported(ConditionalFeature.SCENE3D));

    Button button1 = new Button("button1");
    button1.setTranslateZ(0);
    // 用于设置按钮深度测试的方法。它用于确定按钮在渲染时是否应该被其他对象遮挡。
    // 默认继承父组件的值
    button1.setDepthTest(DepthTest.ENABLE);

    Button button2 = new Button("button2");
    button2.setTranslateZ(300);
    button2.setDepthTest(DepthTest.DISABLE);

    Button button3 = new Button("button3");
    button3.setTranslateZ(600);

    AnchorPane anchorPane = new AnchorPane( button1, button2, button3);
    // 设置背景透明, 否则不可见
    anchorPane.setStyle("-fx-background-color: #00000000");
    // 启用3D渲染
    Scene scene = new Scene(anchorPane, width, height, true);
    // 设置相机
    scene.setCamera(new PerspectiveCamera());
    primaryStage.setScene(scene);
    // 布局
    primaryStage.setTitle("JavaFX");
    primaryStage.setWidth(width);
    primaryStage.setHeight(height);
    primaryStage.show();
}
```

##### ⑭常见`3D`图形

- 可以通过设置不同的三角形数量实现不同的`3D`图形

###### 立方体

```java
public void start(Stage primaryStage) throws Exception {
        double width = 600;
        double height = 400;
        int size = 5;

        HBox hBox = new HBox(50);
        hBox.setPadding(new Insets(50));
        for (int i = 0; i < size; i++) {
            Box box = new Box(100, 100, 200);
            hBox.getChildren().add(box);
        }

        Box box0 = (Box) hBox.getChildren().get(0);
        Box box1 = (Box) hBox.getChildren().get(1);
        Box box2 = (Box) hBox.getChildren().get(2);
        Box box3 = (Box) hBox.getChildren().get(3);
        Box box4 = (Box) hBox.getChildren().get(4);
        // 缩放
        box0.setScaleX(0.5);
        // 切换旋转的坐标系(默认沿Z轴旋转)
        box1.setRotationAxis(new Point3D(1,0,0));
        // 旋转
        box1.setRotate(45);
        box2.setRotationAxis(new Point3D(1,1,1));
        box2.setRotate(45);
        // 填充
        box3.setDrawMode(DrawMode.LINE);
        // 剔除所有背面多边形。BACK定义为顺时针缠绕顺序。
        box3.setCullFace(CullFace.BACK);
        box4.setDrawMode(DrawMode.FILL);
        AnchorPane anchorPane = new AnchorPane(hBox);
        // 启用3D渲染, 实现抗锯齿优化以实现质量和性能的平衡
        Scene scene = new Scene(anchorPane, width, height, true, SceneAntialiasing.BALANCED);
        // 设置相机(透视摄像机)
        scene.setCamera(new PerspectiveCamera());
        primaryStage.setScene(scene);
        // 布局
        primaryStage.setTitle("JavaFX");
        primaryStage.setWidth(width);
        primaryStage.setHeight(height);
        primaryStage.show();
    }
```

###### 圆柱体

```java
public void start(Stage primaryStage) throws Exception {
    double width = 600;
    double height = 400;
    int size = 5;

    HBox hBox = new HBox(50);
    hBox.setPadding(new Insets(50));
    for (int i = 0; i < size; i++) {
        // 设置三角形数量
        Cylinder cylinder = new Cylinder(100, 100, 100);
        // 获取默认三角形数量
        System.out.println(cylinder.getDivisions());
        hBox.getChildren().add(cylinder);
    }

    Cylinder cylinder0 = (Cylinder) hBox.getChildren().get(0);
    Cylinder cylinder1 = (Cylinder) hBox.getChildren().get(1);
    Cylinder cylinder2 = (Cylinder) hBox.getChildren().get(2);
    Cylinder cylinder3 = (Cylinder) hBox.getChildren().get(3);
    Cylinder cylinder4 = (Cylinder) hBox.getChildren().get(4);
    // 缩放
    cylinder0.setScaleX(0.5);
    // 切换旋转的坐标系(默认沿Z轴旋转)
    cylinder1.setRotationAxis(new Point3D(1,0,0));
    // 旋转
    cylinder1.setRotate(45);
    cylinder2.setRotationAxis(new Point3D(1,1,1));
    cylinder2.setRotate(45);
    // 填充
    cylinder3.setDrawMode(DrawMode.LINE);
    // 剔除所有背面多边形。BACK定义为顺时针缠绕顺序。
    cylinder3.setCullFace(CullFace.BACK);
    cylinder4.setDrawMode(DrawMode.FILL);

    AnchorPane anchorPane = new AnchorPane(hBox);
    // 启用3D渲染, 实现抗锯齿优化以实现质量和性能的平衡
    Scene scene = new Scene(anchorPane, width, height, true, SceneAntialiasing.BALANCED);
    // 设置相机(透视摄像机)
    scene.setCamera(new PerspectiveCamera());
    primaryStage.setScene(scene);
    // 布局
    primaryStage.setTitle("JavaFX");
    primaryStage.setWidth(width);
    primaryStage.setHeight(height);
    primaryStage.show();
}
```

###### 球体

```java
public void start(Stage primaryStage) throws Exception {
    double width = 600;
    double height = 400;
    int size = 5;
    HBox hBox = new HBox(50);
    hBox.setPadding(new Insets(50));
    for (int i = 0; i < size; i++) {
        // 设置三角形数量
        Sphere sphere = new Sphere(100, 64);
        // 获取默认三角形数量
        System.out.println(sphere.getDivisions());
        hBox.getChildren().add(sphere);
    }
    Sphere sphere0 = (Sphere) hBox.getChildren().get(0);
    Sphere sphere1 = (Sphere) hBox.getChildren().get(1);
    Sphere sphere2 = (Sphere) hBox.getChildren().get(2);
    Sphere sphere3 = (Sphere) hBox.getChildren().get(3);
    Sphere sphere4 = (Sphere) hBox.getChildren().get(4);
    // 缩放
    sphere0.setScaleX(0.5);
    // 切换旋转的坐标系(默认沿Z轴旋转)
    sphere1.setRotationAxis(new Point3D(1,0,0));
    // 旋转
    sphere1.setRotate(45);
    sphere2.setRotationAxis(new Point3D(1,1,1));
    sphere2.setRotate(45);
    // 填充
    sphere3.setDrawMode(DrawMode.LINE);
    // 剔除所有背面多边形。BACK定义为顺时针缠绕顺序。
    sphere3.setCullFace(CullFace.BACK);
    sphere4.setDrawMode(DrawMode.FILL);

    AnchorPane anchorPane = new AnchorPane(hBox);
    // 启用3D渲染, 实现抗锯齿优化以实现质量和性能的平衡
    Scene scene = new Scene(anchorPane, width, height, true, SceneAntialiasing.BALANCED);
    // 设置相机(透视摄像机)
    scene.setCamera(new PerspectiveCamera());
    primaryStage.setScene(scene);
    // 布局
    primaryStage.setTitle("JavaFX");
    primaryStage.setWidth(width);
    primaryStage.setHeight(height);
    primaryStage.show();
}
```

##### ⑮材质贴图`Material`

```java
Box box = new Box(100, 100, 200);
Cylinder cylinder = new Cylinder(100, 200);
Sphere sphere = new Sphere(100, 100);
HBox hBox = new HBox(50, box, cylinder, sphere);
hBox.setPadding(new Insets(50));
for (Node child : hBox.getChildren()) {
    Shape3D shape3D = (Shape3D) child;
    PhongMaterial pm = new PhongMaterial();
    // 漫反射
    pm.setDiffuseColor(Utils.getRandomColorColor(false));
    //pm.setDiffuseMap(new Image(Utils.getImgExternalForm("img4.jpg")));
    // 高光
    pm.setSpecularColor(Color.WHITE);
    //pm.setSpecularMap(new Image(Utils.getImgExternalForm("img4.jpg")));
    // 高光强度
    //pm.setSpecularPower(10);
    // 凹凸贴图
    //pm.setBumpMap(new Image(Utils.getImgExternalForm("img4.jpg")));
    // 自发光
    pm.setSelfIlluminationMap(new Image(Utils.getImgExternalForm("img4.jpg")));
    shape3D.setMaterial(pm);
}
```

##### ⑯摄像机`Camera`

- 平行相机`ParallelCamera`，默认
- 透视相机`PerspectiveCamera`

```java
// 启用3D渲染, 实现抗锯齿优化以实现质量和性能的平衡
Scene scene = new Scene(anchorPane, width, height, true, SceneAntialiasing.BALANCED);
// 设置相机(透视摄像机)
PerspectiveCamera camera = new PerspectiveCamera();
// 设置相机位置
camera.setRotationAxis(Rotate.Y_AXIS);
camera.setRotate(45);
// 设置相机观察范围
camera.setFieldOfView(90);
camera.setNearClip(1.2);
camera.setFarClip(1.6);
scene.setCamera(camera);
```

##### ⑰环境光`AmbientLight`和点光源`PointLight`

```java
// 环境光
AmbientLight ambientLight = new AmbientLight(Utils.getRandomColorColor(false));
// 关闭光源
//ambientLight.setLightOn(false);
// 设置受影响的物体, 默认场景中的物体都受影响
ambientLight.getScope().addAll(box, sphere);
PointLight pointLight = new PointLight(Utils.getRandomColorColor(false));
// 关闭光源
//pointLight.setLightOn(false);
// 设置受影响的物体, 默认场景中的物体都受影响
pointLight.getScope().addAll(box, sphere);
// 移动光源
pointLight.setTranslateX(600);
pointLight.setTranslateY(600);
pointLight.setTranslateZ(-300);
AnchorPane anchorPane = new AnchorPane(hBox, pointLight);
```

###### ⑱子场景`SubScene`

- 使用方式基本和`Scene`相同
- 继承`Node`类

##### ⑲自定义`3D`图形`MeshView`

- 自定义正方体

```java
// 创建三角形
TriangleMesh tm = new TriangleMesh();
float[] points = {
    0, 0, 0, 100, 0, 0, 100, 100, 0, 0, 100, 0,// 前面
    100, 0, 100, 100, 100, 100,//右面
    0, 0, 100,
    0, 100, 100,
};
float[] texCoords = {
    0, 0, 0.5f, 0, 0.5f, 0.5f, 0, 0.5f,
    1, 0, 1, 0.5f,
    0, -0.5f,
    0, 1,
};
int[] faces = {
    0, 0, 3, 3, 1, 1,
    0, 0, 1, 1, 3, 3,
    1, 1, 3, 3, 2, 2,
    1, 1, 2, 2, 3, 3,
    1, 1, 5, 5, 4, 4,
    1, 1, 4, 4, 5, 5,
    2, 2, 5, 5, 1, 1,
    2, 2, 1, 1, 5, 5,
    6, 6, 1, 1, 4, 4,
    6, 6, 4, 4, 1, 1,
    6, 6, 0, 0, 1, 1,
    6, 6, 1, 1, 0, 0,
    5, 5, 2, 2, 3, 3,
    5, 5, 3, 3, 2, 2,
    7, 7, 5, 5, 3, 3,
    7, 7, 3, 3, 5, 5,
    6, 6, 5, 5, 7, 7,
    6, 6, 7, 7, 5, 5,
    6, 6, 4, 4, 5, 5,
    6, 6, 5, 5, 4, 4,
    6, 6, 3, 3, 0, 0,
    6, 6, 0, 0, 3, 3,
    6, 6, 7, 7, 3, 3,
    6, 6, 3, 3, 7, 7,
};
tm.getPoints().addAll(points);
tm.getTexCoords().addAll(texCoords);
tm.getFaces().addAll(faces);

// 自定义3D图形
MeshView meshView = new MeshView(tm);
meshView.setRotationAxis(new Point3D(1, 1, 0));
meshView.setRotate(-60);
```

- 椎体

```java
// 创建三角形
TriangleMesh tm = new TriangleMesh();
float[] points = {
    100, 0, 0,
    0, 100, 0,
    200, 100, 0,
    0, 100, 100,
    200, 100, 100,
};
float[] texCoords = {
    0.5f, 0, 0, 1, 1, 1,
    0, -1, 1, -1,
};
int[] faces = {
    0, 0, 1, 1, 2, 2,
    0, 0, 2, 2, 1, 1,
    0, 0, 4, 4, 3, 3,
    0, 0, 3, 3, 4, 4,
    3, 3, 2, 2, 4, 4,
    3, 3, 4, 4, 2, 2,
    3, 3, 1, 1, 2, 2,
    3, 3, 2, 2, 1, 1,
    0, 0, 2, 2, 4, 4,
    0, 0, 4, 4, 2, 2,
    0, 0, 3, 3, 1, 1,
    0, 0, 1, 1, 3, 3,
};
tm.getPoints().addAll(points);
tm.getTexCoords().addAll(texCoords);
tm.getFaces().addAll(faces);
// 自定义3D图形
MeshView meshView = new MeshView(tm);
meshView.setRotationAxis(new Point3D(1, 1, 0));
meshView.setRotate(-60);
```

##### ⑳导入`3D`模型

```java
MeshView meshView = new MeshView();
meshView.setMesh(Mesh.load(getClass().getResource("/path/to/model.obj").toExternalForm()));
PhongMaterial material = new PhongMaterial();
material.setDiffuseColor(Color.RED);
meshView.setMaterial(material);
```

### 六、动画

#### 1、时间线动画`TimeLine`

```java
KeyValue kv1 = new KeyValue(button.translateXProperty(), 0);
KeyFrame kf1 = new KeyFrame(Duration.seconds(0), "kf1", event -> System.out.println("kf1"), kv1);

// Interpolator.DISCRETE表示不显示中间效果
KeyValue kv2_1 = new KeyValue(button.translateXProperty(), 300, Interpolator.EASE_BOTH);
KeyValue kv2_2 = new KeyValue(button.translateYProperty(), 0);
KeyFrame kf2 = new KeyFrame(Duration.seconds(2), "kf2", event -> System.out.println("kf2"), kv2_1, kv2_2);

KeyValue kv3_1 = new KeyValue(button.translateXProperty(), 300);
KeyValue kv3_2 = new KeyValue(button.translateYProperty(), 300);
KeyFrame kf3 = new KeyFrame(Duration.seconds(4), "kf3", event -> System.out.println("kf3"), kv3_1, kv3_2);

Timeline timeline = new Timeline(60);
timeline.getKeyFrames().addAll(kf1, kf2, kf3);
// 设置延迟
//timeline.setDelay(Duration.seconds(2));
// 循环次数, Timeline.INDEFINITE是无限循环
timeline.setCycleCount(4);
// 反方向循环, 反方向也会消耗循环次数
timeline.setAutoReverse(true);
// 加速播放
timeline.setRate(2);
// 输出默认帧数
System.out.println(timeline.getTargetFramerate());
// 动画结束的监听
timeline.setOnFinished(event -> {
    System.out.println("动画结束");
});
// 播放状态
timeline.statusProperty().addListener((observable, oldValue, newValue) -> System.out.println(newValue));
// 添加提示点, 可通过jumpTo()跳转
//timeline.getCuePoints().put("提示点", Duration.seconds(2));
startButton.setOnAction(event -> {
    timeline.play();
});
pauseButton.setOnAction(event -> {
    timeline.pause();
});
stopButton.setOnAction(event -> {
    timeline.stop();
});
jumpButton.setOnAction(event -> {
    // 跳转动画
    //timeline.jumpTo("kf2");
    // 从头开始
    // timeline.playFromStart();
    // 当前时间
    System.out.println(timeline.getCurrentTime().toMillis());
});
```

#### 2、不按照默认位置变换

```java
// 不按照中心点进行缩放
Scale scale = new Scale(1, 1, 0, 0);
rectangle.getTransforms().addAll(scale);
KeyValue kv1_1 = new KeyValue(scale.xProperty(), 1);
KeyValue kv1_2 = new KeyValue(scale.yProperty(), 1);
KeyFrame kf1 = new KeyFrame(Duration.seconds(0), kv1_1, kv1_2);
KeyValue kv2_1 = new KeyValue(scale.xProperty(), 2);
KeyValue kv2_2 = new KeyValue(scale.yProperty(), 2);
KeyFrame kf2 = new KeyFrame(Duration.seconds(1), kv2_1, kv2_2);

// 不按照中心点进行旋转
Rotate rotate = new Rotate(0, 0, 0);
rectangle.getTransforms().addAll(rotate);
KeyValue kv1_1 = new KeyValue(rotate.angleProperty(), 0);
KeyFrame kf1 = new KeyFrame(Duration.seconds(0), kv1_1);
KeyValue kv2_1 = new KeyValue(rotate.angleProperty(), 360);
KeyFrame kf2 = new KeyFrame(Duration.seconds(1), kv2_1);

// 自转和公转
Rotate rotate1 = new Rotate(0, 200, 200);
Rotate rotate2 = new Rotate(0, 25, 25);
rectangle.getTransforms().addAll(rotate1, rotate2);
KeyValue kv1_1 = new KeyValue(rotate1.angleProperty(), 0);
KeyValue kv1_2 = new KeyValue(rotate2.angleProperty(), 0);
KeyFrame kf1 = new KeyFrame(Duration.seconds(0), kv1_1, kv1_2);
KeyValue kv2_1 = new KeyValue(rotate1.angleProperty(), 360);
KeyValue kv2_2 = new KeyValue(rotate2.angleProperty(), -720);
KeyFrame kf2 = new KeyFrame(Duration.seconds(2), kv2_1, kv2_2);
```

#### 3、`3D`图形动画

```java
public void start(Stage primaryStage) throws Exception {
    double width = 600;
    double height = 400;
    Button button = new Button("播放");
    Box box = new Box(100, 100, 100);
    Rotate rotate = new Rotate(0, -50, -50, -50);
    rotate.setAxis(new Point3D(1,0,0));
    Scale scale = new Scale(1, 1, 1, 0, 0, 0);
    box.getTransforms().addAll(rotate, scale);
    KeyValue kv1_1 = new KeyValue(box.translateXProperty(), 0);
    KeyValue kv1_2 = new KeyValue(rotate.angleProperty(), 0);
    KeyValue kv1_3 = new KeyValue(scale.xProperty(), 1);
    KeyFrame kf1 = new KeyFrame(Duration.seconds(0), kv1_1, kv1_2, kv1_3);
    KeyValue kv2_1 = new KeyValue(box.translateXProperty(), 300);
    KeyValue kv2_2 = new KeyValue(rotate.angleProperty(), 360);
    KeyValue kv2_3 = new KeyValue(scale.xProperty(), 2);
    KeyFrame kf2 = new KeyFrame(Duration.seconds(2), kv2_1, kv2_2, kv2_3);
    Timeline timeline = new Timeline(60);
    timeline.getKeyFrames().addAll(kf1, kf2);
    timeline.setAutoReverse(true);
    timeline.setCycleCount(Timeline.INDEFINITE);
    button.setOnAction(event -> {
        timeline.play();
    });
    AnchorPane subAnchorPane = new AnchorPane(box);
    AnchorPane.setLeftAnchor(box, 100.0);
    AnchorPane.setTopAnchor(box,100.0);
    SubScene subScene = new SubScene(subAnchorPane, width, height, true, SceneAntialiasing.BALANCED);
    PerspectiveCamera camera = new PerspectiveCamera();
    subScene.setCamera(camera);
    AnchorPane anchorPane = new AnchorPane(button, subScene);
    AnchorPane.setLeftAnchor(subScene, 100.0);
    AnchorPane.setTopAnchor(subScene, 100.0);
    Scene scene = new Scene(anchorPane);
    primaryStage.setScene(scene);
    // 布局
    primaryStage.setTitle("JavaFX");
    primaryStage.setWidth(width);
    primaryStage.setHeight(height);
    primaryStage.show();
}
```

#### 4、位移动画`TranslateTransition`

```java
Button button = new Button("播放");
Rectangle rectangle = new Rectangle(50, 50, Utils.getRandomPaintColor(false));
TranslateTransition tt = new TranslateTransition();
// 持续时间
tt.setDuration(Duration.seconds(2));
// 应用到哪个组件
tt.setNode(rectangle);
// 从哪到哪
tt.setFromX(0);
tt.setFromY(0);
// 绝对位置
tt.setToX(300);
tt.setToY(100);
// 相对位移
//tt.setByX(300);
// 反向
tt.setAutoReverse(true);
// 播放次数
tt.setCycleCount(Animation.INDEFINITE);
// 自定义插值器
tt.setInterpolator(Interpolator.SPLINE(0.5,0,0.5,1));
button.setOnAction(event -> {
    tt.play();
});
button.translateXProperty().bind(rectangle.translateXProperty());
```

#### 5、旋转动画`RotateTransition`

```java
Rectangle rectangle = new Rectangle(50, 50, Utils.getRandomPaintColor(false));
RotateTransition rt = new RotateTransition(Duration.seconds(2), rectangle);
rt.setFromAngle(0);
rt.setToAngle(360);
// 更改旋转轴
rt.setAxis(new Point3D(0, 1, 0));
rt.setInterpolator(Interpolator.LINEAR);
rt.setCycleCount(Animation.INDEFINITE);
```

#### 6、缩放动画`ScaleTransition`

```java
ScaleTransition st = new ScaleTransition(Duration.seconds(1), rectangle);
st.setFromX(1);
st.setToX(2);
st.setFromY(1);
st.setToY(2);
st.setInterpolator(Interpolator.LINEAR);
st.setAutoReverse(true);
st.setCycleCount(Animation.INDEFINITE);
```

#### 7、透明度动画`FadeTransition`

```java
FadeTransition ft = new FadeTransition(Duration.seconds(0.5), rectangle);
ft.setFromValue(1);
ft.setToValue(0);
ft.setAutoReverse(true);
ft.setCycleCount(Animation.INDEFINITE);
```

#### 8、颜色填充动画`FillTransition`

```java
FillTransition ft = new FillTransition(Duration.seconds(3), rectangle);
ft.setFromValue(Utils.getRandomColorColor(false));
ft.setToValue(Utils.getRandomColorColor(false));
ft.setAutoReverse(true);
ft.setCycleCount(Animation.INDEFINITE);
```

#### 9、图形边框颜色过渡动画

```java
Rectangle rectangle = new Rectangle(50, 50, Utils.getRandomPaintColor(false));
rectangle.setStrokeWidth(10);
rectangle.setStrokeType(StrokeType.OUTSIDE);
Color color = Utils.getRandomColorColor(false);
rectangle.setStroke(color);
StrokeTransition st = new StrokeTransition(Duration.seconds(0.5), rectangle);
st.setFromValue(color);
st.setToValue(Utils.getRandomColorColor(false));
st.setAutoReverse(true);
st.setCycleCount(Animation.INDEFINITE);
```

#### 10、路径动画

```java
public void start(Stage primaryStage) throws Exception {
    double width = 600;
    double height = 400;
    Button button = new Button("播放");
    Rectangle rectangle = new Rectangle(50, 50, Utils.getRandomPaintColor(false));
    rectangle.setTranslateX(-25);
    rectangle.setTranslateY(-25);
    PathTransition pt = new PathTransition();
    pt.setDuration(Duration.seconds(3));
    pt.setNode(rectangle);
    PathElement[] pathElements = {
        new MoveTo(0, 0), new LineTo(200, 0),
        new QuadCurveTo(0, 0, 170, 200),
        new HLineTo(100), new VLineTo(100),
        new CubicCurveTo(50, -50, 100, -100, 200, 0),
        new ArcTo(100, 50, 0, 100, 100, true, false),
        new ClosePath()
    };
    Path path = new Path(pathElements);
    AnchorPane an = new AnchorPane(path);
    pt.setPath(path);
    // 方向跟随
    pt.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
    pt.setAutoReverse(true);
    pt.setInterpolator(Interpolator.SPLINE(0.5,0.8,1,0));
    pt.setCycleCount(Animation.INDEFINITE);
    button.setOnAction(event -> {
        pt.play();
    });
    AnchorPane anchorPane = new AnchorPane(button, rectangle, an);
    AnchorPane.setLeftAnchor(rectangle, 100.0);
    AnchorPane.setTopAnchor(rectangle, 100.0);
    AnchorPane.setLeftAnchor(an, 100.0);
    AnchorPane.setTopAnchor(an, 100.0);
    Scene scene = new Scene(anchorPane);
    primaryStage.setScene(scene);
    // 布局
    primaryStage.setTitle("JavaFX");
    primaryStage.setWidth(width);
    primaryStage.setHeight(height);
    primaryStage.show();
}
```

#### 11、并行动画和串行动画

- 并行动画`TranslateTransition`

```java
TranslateTransition tt = new TranslateTransition(Duration.seconds(2));
tt.setFromX(0);
tt.setToX(300);
RotateTransition rt = new RotateTransition(Duration.seconds(2), rectangle);
rt.setFromAngle(0);
rt.setToAngle(360);
rt.setAxis(new Point3D(1,0,0));
ParallelTransition pt = new ParallelTransition(rectangle);
pt.getChildren().addAll(tt, rt);
pt.setAutoReverse(true);
pt.setCycleCount(Animation.INDEFINITE);
button.setOnAction(event -> {
    pt.play();
});
```

- 串行动画`SequentialTransition`，组合使用

```java
Button button = new Button("播放");
Rectangle rectangle = new Rectangle(50, 50, Utils.getRandomPaintColor(false));
TranslateTransition tt = new TranslateTransition(Duration.seconds(2));
tt.setFromX(0);
tt.setToX(300);
RotateTransition rt = new RotateTransition(Duration.seconds(2), rectangle);
rt.setFromAngle(0);
rt.setToAngle(360);
rt.setAxis(new Point3D(1,0,0));
FillTransition ft = new FillTransition(Duration.seconds(1));
ft.setToValue(Utils.getRandomColorColor(false));
ParallelTransition pt = new ParallelTransition(rectangle);
pt.getChildren().addAll(tt, rt);
pt.setAutoReverse(true);
SequentialTransition st = new SequentialTransition(rectangle);
st.getChildren().addAll(ft, pt);
st.setAutoReverse(true);
st.setCycleCount(Animation.INDEFINITE);
button.setOnAction(event -> {
    st.play();
});
```

#### 12、动画计时器`AnimationTimer`

```java
Rectangle rectangle = new Rectangle(50, 50, Utils.getRandomPaintColor(false));
TranslateTransition tt = new TranslateTransition(Duration.seconds(1), rectangle);
tt.setFromX(0);
tt.setToX(300);
AnimationTimer animationTimer = new AnimationTimer() {
    int i = 0;
    @Override
    public void handle(long now) {
        // 帧数跟随显示器
        System.out.println(i++);
        rectangle.setRotate(rectangle.getRotate() + 3);
    }
};
tt.setOnFinished(event -> animationTimer.stop());
button1.setOnAction(event -> {
    tt.play();
    animationTimer.start();
});
button2.setOnAction(event -> {
    animationTimer.stop();
});
```

### 七、媒体类

#### 1、播放短音频和音效`AudioClip`

- 适合播放短音频，例如音效

```java
String audioFile = Utils.getFileURL("穿越火线经典bgm.mp3").toExternalForm();
AudioClip audioClip = new AudioClip(audioFile);
// 左右声道, -1左声道, 0左右都有, 1右声道
audioClip.setBalance(0);
//audioClip.setPan(-1);
// 优先级
//audioClip.setPriority(10);
// 速率, 0.125-8
audioClip.setRate(1.5);
// 播放次数
audioClip.setCycleCount(AudioClip.INDEFINITE);
// 音量, 0-1
audioClip.setVolume(0.5);
button1.setOnAction(event -> {
    audioClip.play();
});
button2.setOnAction(event -> {
    audioClip.stop();
});
```

#### 2、普通音乐播放`MediaPlayer`

##### ①基础

```java
String audioFile = Utils.getFileURL("穿越火线经典bgm.mp3").toExternalForm();
Media media = new Media(audioFile);
MediaPlayer mediaPlayer = new MediaPlayer(media);
// 设置音量
mediaPlayer.setVolume(0.5);
// 静音
mediaPlayer.setMute(true);
// 音乐准备完毕后自动播放
mediaPlayer.setAutoPlay(true);
// 音乐平衡(左右声道)
mediaPlayer.setBalance(0);
// 速率
mediaPlayer.setRate(0.5);
// 开始时间
mediaPlayer.setStartTime(Duration.seconds(5));
// 结束时间
mediaPlayer.setStopTime(Duration.seconds(10));
// 播放次数
mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
// 获取当前时间, 音乐准备完毕可获取
//System.out.println(mediaPlayer.getCurrentCount());
// 释放资源
//mediaPlayer.dispose();

// 音乐准备完毕事件
mediaPlayer.setOnReady(() -> {
    // 获取音乐时长
    System.out.println(mediaPlayer.getTotalDuration().toSeconds());
    System.out.println(media.getDuration().toSeconds());
});

button1.setOnAction(event -> {
    mediaPlayer.play();
});
button2.setOnAction(event -> {
    mediaPlayer.stop();
});
button3.setOnAction(event -> {
    mediaPlayer.pause();
});
```

##### ②生命周期

```java
// 音乐准备完毕事件
mediaPlayer.setOnReady(() -> {
    // 获取音乐时长
    System.out.println(mediaPlayer.getTotalDuration().toSeconds());
    System.out.println(media.getDuration().toSeconds());
});

// 发生错误时的事件
mediaPlayer.setOnError(() -> {

});

// 当播放器在缓冲或加载媒体时发生停顿时，该事件会被触发。
mediaPlayer.setOnStalled(()->{
    System.out.println("进入缓存阶段");
});

mediaPlayer.setOnStopped(()->{
    System.out.println("音乐停止");
});

mediaPlayer.setOnPlaying(()->{
    System.out.println("正在播放");
});

// 当播放器遇到严重错误或异常情况时，导致媒体播放无法继续时，该事件会被触发。
mediaPlayer.setOnHalted(()->{

});

// 当媒体播放器播放完整个媒体文件时，该事件会被触发。
mediaPlayer.setOnEndOfMedia(()->{

});

// 在媒体文件中设置标记点（Marker）时，当媒体播放器到达标记点时，该事件会被触发。
mediaPlayer.setOnMarker(event->{

});
```

##### ③滑动音量和进度条

```java
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author huayunliufeng
 * @date_time 2023/9/10 22:07
 * @desc
 */
public class App extends Application {
    private int index = 0;
    private MediaPlayer mediaPlayer;
    private boolean isPressed;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        double width = 400;
        double height = 300;
        // 布局
        AnchorPane anchorPane = new AnchorPane();
        Scene scene = new Scene(anchorPane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("JavaFX - MediaPlayer");
        primaryStage.setWidth(width);
        primaryStage.setHeight(height);
        primaryStage.show();

        Button button1 = new Button("播放");
        Button button2 = new Button("停止");
        Button button3 = new Button("暂停");
        Button button4 = new Button("上一首");
        Button button5 = new Button("下一首");
        Slider volume = new Slider(0, 1, 0.2);
        volume.setPrefWidth(150);
        volume.setBlockIncrement(0.1);
        volume.setShowTickLabels(true);
        volume.setShowTickMarks(true);
        Slider progress = new Slider();
        progress.setPrefWidth(300);
        progress.setShowTickLabels(true);
        progress.setShowTickMarks(true);
        progress.setBlockIncrement(0.01);

        progress.setOnMousePressed(event -> {
            isPressed = true;
        });

        progress.setOnMouseReleased(event -> {
            isPressed = false;
            mediaPlayer.seek(Duration.seconds(progress.getValue()));
        });

        GridPane gridPane = new GridPane();
        gridPane.setVgap(50);
        gridPane.add(volume, 0, 0);
        gridPane.add(progress, 0, 1, 2, 1);
        HBox hBox = new HBox(20, button1, button2, button3, button4, button5);
        VBox vBox = new VBox(20, hBox, gridPane);
        vBox.setAlignment(Pos.CENTER_LEFT);
        vBox.setPadding(new Insets(30));

        String audioFile1 = Objects.requireNonNull(App.class.getResource("/music/穿越火线经典bgm.mp3")).toExternalForm();
        String audioFile2 = Objects.requireNonNull(App.class.getResource("/music/Janji - Horizon.mp3")).toExternalForm();
        String audioFile3 = Objects.requireNonNull(App.class.getResource("/music/筷子兄弟 - 老男孩.mp3")).toExternalForm();

        List<Media> list = new ArrayList<>();
        list.add(new Media(audioFile1));
        list.add(new Media(audioFile2));
        list.add(new Media(audioFile3));

        mediaPlayer = new MediaPlayer(list.get(index = (index + 1) % list.size()));
        setListener(volume, progress);
        button1.setOnAction(event -> {
            mediaPlayer.play();
        });
        button2.setOnAction(event -> {
            mediaPlayer.stop();
        });
        button3.setOnAction(event -> {
            mediaPlayer.pause();
        });
        button4.setOnAction(event -> {
            mediaPlayer.dispose();
            mediaPlayer = new MediaPlayer(list.get(index = index - 1 < 0 ? list.size() - 1 : index - 1));
            setListener(volume, progress);
            button1.fire();
        });
        button5.setOnAction(event -> {
            mediaPlayer.dispose();
            mediaPlayer = new MediaPlayer(list.get(index = (index + 1) % list.size()));
            setListener(volume, progress);
            button1.fire();
        });

        anchorPane.getChildren().addAll(vBox);
    }
    
    private void setListener(Slider volume, Slider progress) {
        mediaPlayer.setOnReady(() -> {
            mediaPlayer.volumeProperty().bind(volume.valueProperty());
            progress.setMin(0);
            progress.setValue(0);
            progress.setMax(mediaPlayer.getTotalDuration().toSeconds());
            mediaPlayer.currentTimeProperty().addListener((observable, oldValue, newValue) -> {
                if (!isPressed) {
                    progress.setValue(newValue.toSeconds());
                }
            });
        });
    }
}
```

##### ④获取文件信息

```java
Media media = new Media(audioFile);
// 添加标记, 可以用来添加歌词
media.getMarkers().put("a", Duration.seconds(3));
media.getMarkers().put("b", Duration.seconds(6));
media.getMarkers().put("c", Duration.seconds(9));
MediaPlayer mediaPlayer = new MediaPlayer(media);
mediaPlayer.setVolume(0.3);
mediaPlayer.setOnReady(() -> {
    // 检索此媒体源中包含的曲目。如果没有轨道，返回的ObservableList将为空。
    for (Track track : media.getTracks()) {
        System.out.println(track);
    }
    System.out.println("--------------------------------");
    // 获取描述信息
    // artist艺术家
    // album专辑
    // image专辑图片
    // title标题
    // year年份
    // genre体裁
    // raw metadata原始元数据
    ObservableMap<String, Object> metadata = media.getMetadata();
    for (Map.Entry<String, Object> entry : metadata.entrySet()) {
        System.out.println(entry.getKey() + " - " + entry.getValue());
    }
});
mediaPlayer.setOnMarker(event -> {
    System.out.println(event.getMarker());
});
```

##### ⑤获取音频频谱数据

```java
// 频谱更新之间的间隔（秒）。默认值为0.1秒。
mediaPlayer.setAudioSpectrumInterval(0.1);
// 灵敏度阈值，单位为分贝；必须是非正的。相对于给定谱带中的峰值频率低于该阈值的值将被设置为阈值的值。默认值为-60 dB。
mediaPlayer.setAudioSpectrumThreshold(-100);
// 音频频谱中的频带数。默认值为128, 最小值为2。
mediaPlayer.setAudioSpectrumNumBands(100);
// 对频谱的监听
// timestamp时间戳/秒, 当前时间, 不会自动清零
// duration是时间间隔
// magnitudes频谱数据/float[128], 0到-60, 可更改
// phases相位/float[100], -π到π
mediaPlayer.setAudioSpectrumListener((timestamp, duration, magnitudes, phases) -> {
    System.out.println(phases.length);
});
```

#### 3、播放视频`MediaView`

```java
Button button1 = new Button("播放");
Button button2 = new Button("停止");
Button button3 = new Button("暂停");
HBox hBox = new HBox(20, button1, button2, button3);
String mvFile = Objects.requireNonNull(App.class.getResource("/mv/test.mp4")).toExternalForm();
Media media = new Media(mvFile);
MediaPlayer mediaPlayer = new MediaPlayer(media);
MediaView mediaView = new MediaView(mediaPlayer);
// 保持宽高比
mediaView.setPreserveRatio(true);
// 设置平滑
mediaView.setSmooth(true);
// 在媒体帧中指定一个矩形视口。视口是在媒体帧的坐标中指定的矩形。
// 缩放之前得到的边界将是视口的大小。显示的图像将包括帧和视口的交点。
// 视口可以超过边框的大小，但只显示交叉点。将视口设置为null将清除视口。
//Rectangle2D rectangle2D = new Rectangle2D(100,200,400,600);
//mediaView.setViewport(rectangle2D);
// 定义MediaView原点的当前x坐标。
mediaView.setX(100);
// 定义MediaView原点的当前y坐标。
mediaView.setY(200);
mediaPlayer.setOnReady(() -> {
    // 获取视频宽和高
    System.out.println(media.getWidth());
    System.out.println(media.getHeight());
    mediaView.setFitWidth(media.getWidth());
});
button1.setOnAction(event -> {
    mediaPlayer.play();
});
button2.setOnAction(event -> {
    mediaPlayer.stop();
});
button3.setOnAction(event -> {
    mediaPlayer.pause();
});
VBox vBox = new VBox(20, hBox, mediaView);
anchorPane.getChildren().addAll(vBox);
```

### 八、简单案例

#### 1、登录窗口

```java
Label accountLabel = new Label("账号:");
TextField accountField = new TextField();
Label passwordLabel = new Label("密码:");
PasswordField passwordField = new PasswordField();
Button loginButton = new Button("登录");
Button cencelButton = new Button("取消");
HBox hBox = new HBox(loginButton, cencelButton);
hBox.setSpacing(100);
hBox.setPadding(new Insets(0, 20, 0, 20));

// 网格布局
GridPane gridPane = new GridPane();
gridPane.add(accountLabel, 0, 0);
gridPane.add(accountField, 1, 0);
gridPane.add(passwordLabel, 0, 1);
gridPane.add(passwordField, 1, 1);
gridPane.add(hBox, 0, 2);
// 设置hBox跨两列
GridPane.setColumnSpan(hBox, 2);
gridPane.setAlignment(Pos.CENTER);
gridPane.setVgap(10);
gridPane.setHgap(10);

primaryStage.setScene(new Scene(gridPane));
primaryStage.setTitle("登录");
primaryStage.setResizable(false);
primaryStage.setWidth(400);
primaryStage.setHeight(300);
primaryStage.getIcons().add(new Image(Utils.getFaviconExternalForm("favicon.png")));
primaryStage.show();

// 设置事件
loginButton.setOnAction(event -> {
    System.out.println("账号: " + accountField.getText());
    System.out.println("密码: " + passwordField.getText());
});
```

#### 2、将图片转换为字符画保存到文件中

```java
String path = Utils.getImgExternalForm("img4.jpg");
// 生成字符画
Image image = new Image(path, 1000,180,false, true);
int width = (int) image.getWidth(), height = (int) image.getHeight();
System.out.println(width + " " + height);
PixelReader pixelReader = image.getPixelReader();
StringBuilder builder = new StringBuilder();
// 是按行写, 因此先行后列
for (int i = 0; i < height; i++) {
	for (int j = 0; j < width; j++) {
        int argb = pixelReader.getArgb(j, i);
        // 红色值
        int red = argb >> 16 & 0xff;
        // 越亮的地方使用的字符占位应当越小
        builder.append(getChar(red));
	}
	builder.append("\r\n");
}
saveFile(builder, new File(Utils.getResourcesPath("img4字符画.txt")));

public static char getChar(int value) {
    if (value >= 0 && value < 10) {
    return '#';
    } else if (value < 20) {
    return '#';
    } else if (value < 30) {
    return '#';
    } else if (value < 40) {
    return '#';
    } else if (value < 50) {
    return 'n';
    } else if (value < 60) {
    return 'X';
    } else if (value < 70) {
    return 'W';
    } else if (value < 80) {
    return 'M';
    } else if (value < 90) {
    return 'g';
    } else if (value < 100) {
    return 'a';
    } else if (value < 110) {
    return 'c';
    } else if (value < 120) {
    return 's';
    } else if (value < 130) {
    return 'o';
    } else if (value < 140) {
    return '?';
    } else if (value < 150) {
    return ';';
    } else if (value < 160) {
    return '!';
    } else if (value < 170) {
    return '+';
    } else if (value < 180) {
    return '=';
    } else if (value < 190) {
    return '-';
    } else if (value < 200) {
    return ',';
    } else if (value < 210) {
    return '.';
    } else if (value < 220) {
    return '.';
    } else if (value < 230) {
    return ' ';
    } else if (value < 240) {
    return ' ';
    } else if (value < 250) {
    return ' ';
    } else {
    return ' ';
    }
}
```

#### 3、鼠标拖动组件和自定义窗体

```java
public class Main extends Application {
    private double cursorX1 = 0, cursorY1 = 0, cursorX2 = 0, cursorY2 = 0;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Button button = new Button("button");
        button.setLayoutX(200);
        button.setLayoutY(200);
        // 获取点击时的鼠标坐标
        button.setOnMousePressed(event -> {
            cursorX1 = event.getX();
            cursorY1 = event.getY();
        });
        // 移动组件
        button.setOnMouseDragged(event -> {
            button.setLayoutX(event.getSceneX() - cursorX1);
            button.setLayoutY(event.getSceneY() - cursorY1);
        });

        // 只剩窗体
        stage.initStyle(StageStyle.TRANSPARENT);
        AnchorPane anchorPane = new AnchorPane(button);
        anchorPane.setBorder(new Border(
            new BorderStroke(Color.valueOf("#912cee"),
                             BorderStrokeStyle.DASHED,
                             new CornerRadii(100),
                             new BorderWidths(2),
                             Insets.EMPTY)));
        // 给窗口设置圆角
        anchorPane.setBackground(new Background(
            new BackgroundFill(Paint.valueOf("#ffb5c560"),
                               new CornerRadii(100),
                               Insets.EMPTY)));
        Scene scene = new Scene(anchorPane);
        // 将scene的白色背景改为透明
        scene.setFill(Paint.valueOf("#ffffff00"));
        // 移动窗体
        scene.setOnMousePressed(event -> {
            cursorX2 = event.getScreenX() - stage.getX();
            cursorY2 = event.getScreenY() - stage.getY();
        });
        scene.setOnMouseDragged(event -> {
            stage.setX(event.getScreenX() - cursorX2);
            stage.setY(event.getScreenY() - cursorY2);
        });
        stage.setScene(scene);
        // 布局
        stage.setTitle("JavaFX");
        stage.setWidth(800);
        stage.setHeight(800);
        stage.show();
    }
}
```

#### 4、图表的动态数据显示

```java
/*
 * © 2023 huayunliufeng保留所有权利, 依据GPL许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package priv.zq;

import javafx.application.Application;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.Random;

/**
 * @author huayunliufeng
 * @date_time 2023/9/10 22:07
 * @desc
 */
public class App extends Application {
    private int time = 0;
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        MyScheduledService myScheduledService = new MyScheduledService();
        Button startButton = new Button("监控");
        startButton.setOnAction(event -> {
            if (!myScheduledService.isRunning()) {
                myScheduledService.start();
            }
        });


        Button stopButton = new Button("停止");
        stopButton.setOnAction(event -> {
            myScheduledService.cancel();
            myScheduledService.reset();
        });

        LineChart<Number, Number> lineChart = getView();
        myScheduledService.setDelay(Duration.ZERO);
        myScheduledService.setPeriod(Duration.seconds(1));
        myScheduledService.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {

                XYChart.Series<Number, Number> cpuUsed = lineChart.getData().get(0);
                XYChart.Series<Number, Number> memoryUsed = lineChart.getData().get(1);
                int size = cpuUsed.getData().size();
                if (size >= 10) {
                    cpuUsed.getData().remove(0);
                    memoryUsed.getData().remove(0);
                }
                XYChart.Data<Number, Number> data1 = new XYChart.Data<>(time, newValue[0]);
                XYChart.Data<Number, Number> data2 = new XYChart.Data<>(time, newValue[1]);

                if (size > 8) {
                    NumberAxis xAxis = (NumberAxis) lineChart.getXAxis();
                    xAxis.setLowerBound(xAxis.getLowerBound() + 1);
                    xAxis.setUpperBound(xAxis.getUpperBound() + 1);
                }
                cpuUsed.getData().add(data1);
                memoryUsed.getData().add(data2);
                time++;
            }
        });

        HBox hBox = new HBox(10, startButton, stopButton);
        VBox vBox = new VBox(20, hBox, lineChart);
        vBox.setPadding(new Insets(20));
        AnchorPane anchorPane = new AnchorPane(vBox);
        Scene scene = new Scene(anchorPane);
        primaryStage.setScene(scene);
        // 布局
        primaryStage.setTitle("JavaFX");
        primaryStage.setWidth(700);
        primaryStage.setHeight(500);
        primaryStage.show();

    }

    public LineChart<Number, Number> getView() {
        XYChart.Series<Number, Number> cpuUsed = new XYChart.Series<>();
        cpuUsed.setName("CPU使用率");
        XYChart.Series<Number, Number> memoryUsed = new XYChart.Series<>();
        memoryUsed.setName("内存使用率");
        NumberAxis x = new NumberAxis("时间 / 秒", 0, 9, 1);
        NumberAxis y = new NumberAxis("使用率 / %", 0, 100, 5);
        LineChart<Number, Number> lineChart = new LineChart<>(x, y);
        lineChart.setPrefWidth(600);
        lineChart.setPrefHeight(400);
        lineChart.getData().addAll(cpuUsed, memoryUsed);
        // 去除动画
        //lineChart.setAnimated(false);
        return lineChart;
    }
}

class MyScheduledService extends ScheduledService<Number[]> {

//    private final CPUInfo cpuInfo = WindowsResourcesMonitorInfo.CPU_INFO();
//    private final MemoryInfo memoryInfo = WindowsResourcesMonitorInfo.MEMORY_INFO();

    private final Random random = new Random();

    @Override
    protected Task<Number[]> createTask() {
        return new Task<>() {
            @Override
            protected Number[] call() throws Exception {
                // data[0] = new Number[]{time, cpuInfo.getCpuUsageRate()};
                // data[1] = new Number[]{time, memoryInfo.getUsage()};
                return new Number[]{random.nextInt(101), random.nextInt(101)};
            }
        };
    }
}
```

#### 5、时间线动画组合

##### ①位移、旋转、缩放、不透明度

```java
Rectangle rectangle = new Rectangle(50, 50);
rectangle.setFill(Utils.getRandomColorColor(false));
KeyValue kv1_1 = new KeyValue(rectangle.translateXProperty(), 0);
KeyValue kv1_2 = new KeyValue(rectangle.rotateProperty(), 0);
KeyValue kv1_3 = new KeyValue(rectangle.scaleXProperty(), 1);
KeyValue kv1_4 = new KeyValue(rectangle.scaleYProperty(), 1);
KeyValue kv1_5 = new KeyValue(rectangle.scaleZProperty(), 1);
KeyValue kv1_6 = new KeyValue(rectangle.opacityProperty(), 1);
KeyFrame kf1 = new KeyFrame(Duration.seconds(0), kv1_1, kv1_2, kv1_3, kv1_4, kv1_5, kv1_6);
KeyValue kv2_1 = new KeyValue(rectangle.translateXProperty(), 300);
KeyValue kv2_2 = new KeyValue(rectangle.rotateProperty(), 360);
KeyValue kv2_3 = new KeyValue(rectangle.scaleXProperty(), 2);
KeyValue kv2_4 = new KeyValue(rectangle.scaleYProperty(), 2);
KeyValue kv2_5 = new KeyValue(rectangle.scaleZProperty(), 2);
KeyValue kv2_6 = new KeyValue(rectangle.opacityProperty(), 0.2);
KeyFrame kf2 = new KeyFrame(Duration.seconds(2), kv2_1, kv2_2, kv2_3, kv2_4, kv2_5, kv2_6);
Timeline timeline = new Timeline(60);
timeline.getKeyFrames().addAll(kf1, kf2);
timeline.setAutoReverse(true);
timeline.setCycleCount(Timeline.INDEFINITE);
```

##### ②多个动画组合

```java
KeyValue kv1 = new KeyValue(rectangle.rotateProperty(), 0);
KeyFrame kf1 = new KeyFrame(Duration.seconds(0), kv1);
KeyValue kv2_1 = new KeyValue(rectangle.rotateProperty(), 360);
KeyValue kv2_2 = new KeyValue(rectangle.translateXProperty(), 0);
KeyFrame kf2 = new KeyFrame(Duration.seconds(1), kv2_1, kv2_2);
KeyValue kv4_1 = new KeyValue(rectangle.translateXProperty(), 300);
KeyValue kv4_2 = new KeyValue(rectangle.rotateProperty(), 360);
KeyFrame kf4 = new KeyFrame(Duration.seconds(2), kv4_1, kv4_2);
KeyValue kv6 = new KeyValue(rectangle.rotateProperty(), 0);
KeyFrame kf6 = new KeyFrame(Duration.seconds(3), kv6);
KeyValue kv7 = new KeyValue(rectangle.rotateProperty(), 0);
KeyFrame kf7 = new KeyFrame(Duration.seconds(4), kv7);
Timeline timeline = new Timeline(60);
timeline.getKeyFrames().addAll(kf1, kf2, kf4, kf6,kf7);
```

#### 6、轮播图效果

```java
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huayunliufeng
 * @date_time 2023/9/10 22:07
 * @desc
 */
public class App extends Application {

    private final List<ImageView> list = new ArrayList<>();
    double pos0_x, pos_y, pos1_x, pos2_x, pos_z = 60;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        double width = 800;
        double height = 600;
        // 布局
        AnchorPane anchorPane = new AnchorPane();
        Scene scene = new Scene(anchorPane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("JavaFX");
        primaryStage.setWidth(width);
        primaryStage.setHeight(height);
        primaryStage.show();

        Pane pane = getView(500, 200);

        anchorPane.getChildren().addAll(pane);
        AnchorPane.setLeftAnchor(pane, 100.0);
        AnchorPane.setTopAnchor(pane, 100.0);
    }

    public Pane getView(int w, int h) {
        VBox leftButton = getChangeNode("<", 50, h);
        VBox rightButton = getChangeNode(">", 50, h);

        leftButton.setOnMouseClicked(event -> {
            rTl();
        });

        rightButton.setOnMouseClicked(event -> {
            lTr();
        });

        ImageView iv1 = new ImageView(Utils.getImgExternalForm("img1.jpg"));
        iv1.setPreserveRatio(true);
        iv1.setFitWidth(w / 1.6);

        ImageView iv2 = new ImageView(Utils.getImgExternalForm("img2.jpg"));
        iv2.setPreserveRatio(true);
        iv2.setFitWidth(w / 1.6);

        ImageView iv3 = new ImageView(Utils.getImgExternalForm("img4.jpg"));
        iv3.setPreserveRatio(true);
        iv3.setFitWidth(w / 1.6);

        pos0_x = 0;
        pos_y = (h - iv1.prefHeight(-1)) / 2;
        pos1_x = (w - iv1.getFitWidth()) / 2;
        pos2_x = w - iv1.getFitWidth();

        iv1.setTranslateX(pos0_x);
        iv2.setTranslateX(pos1_x);
        iv3.setTranslateX(pos2_x);

        iv1.setTranslateY(pos_y);
        iv2.setTranslateY(pos_y);
        iv3.setTranslateY(pos_y);

        iv1.setTranslateZ(pos_z);
        iv2.setTranslateZ(0);
        iv3.setTranslateZ(pos_z);

        list.add(iv1); // 索引为0在左边
        list.add(iv2); // 索引为1在中间
        list.add(iv3); // 索引为2在右边

        HBox hBox = new HBox(w - (leftButton.prefWidth(-1) * 2), leftButton, rightButton);
        AnchorPane anchorPane = new AnchorPane(iv1, iv2, iv3);
        SubScene subScene = new SubScene(anchorPane, w, h, true, SceneAntialiasing.BALANCED);
        PerspectiveCamera camera = new PerspectiveCamera();
        subScene.setCamera(camera);
        return new StackPane(subScene, hBox);
    }

    public VBox getChangeNode(String text, int w, int h) {
        Text t = new Text(text);
        t.setFont(Font.font(50));
        VBox vBox = new VBox(t);
        vBox.setAlignment(Pos.CENTER);
        Background background = new Background(new BackgroundFill(Paint.valueOf("#ffffff40"), new CornerRadii(10), Insets.EMPTY));
        vBox.setOnMouseEntered(event -> {
            vBox.setBackground(background);
        });
        vBox.setOnMouseExited(event -> {
            vBox.setBackground(null);
        });
        vBox.setPrefWidth(w);
        vBox.setPrefHeight(h);
        return vBox;
    }

    private void rTl() {
        ImageView iv1 = list.get(0);
        ImageView iv2 = list.get(1);
        ImageView iv3 = list.get(2);
        ltrAnimate(iv1);
        mtlAnimate(iv2);
        rtmAnimate(iv3);
        deChangeIndex();
    }

    private void lTr() {
        ImageView iv1 = list.get(0);
        ImageView iv2 = list.get(1);
        ImageView iv3 = list.get(2);
        ltmAnimate(iv1);
        mtrAnimate(iv2);
        rtlAnimate(iv3);
        inChangeIndex();
    }

    private void ltmAnimate(ImageView imageView) {
        TranslateTransition tt = new TranslateTransition(Duration.seconds(0.5), imageView);
        tt.setFromX(pos0_x);
        tt.setFromZ(pos_z);
        tt.setToX(pos1_x);
        tt.setToZ(0);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5), imageView);
        ft.setFromValue(0.5);
        ft.setToValue(1);
        ParallelTransition pt = new ParallelTransition(imageView, tt, ft);
        pt.play();
    }

    private void mtrAnimate(ImageView imageView) {
        TranslateTransition tt = new TranslateTransition(Duration.seconds(0.5), imageView);
        tt.setFromX(pos1_x);
        tt.setFromZ(0);
        tt.setToX(pos2_x);
        tt.setToZ(pos_z);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5), imageView);
        ft.setFromValue(1);
        ft.setToValue(0.5);
        ParallelTransition pt = new ParallelTransition(imageView, tt, ft);
        pt.play();
    }

    private void rtlAnimate(ImageView imageView) {
        TranslateTransition tt = new TranslateTransition(Duration.seconds(0.5), imageView);
        tt.setFromX(pos2_x);
        tt.setFromZ(pos_z);
        tt.setToX(pos0_x);
        tt.setToZ(pos_z);
        tt.play();
    }

    private void ltrAnimate(ImageView imageView) {
        TranslateTransition tt = new TranslateTransition(Duration.seconds(0.5), imageView);
        tt.setFromX(pos0_x);
        tt.setFromZ(pos_z);
        tt.setToX(pos2_x);
        tt.setToZ(pos_z);
        tt.play();
    }

    private void mtlAnimate(ImageView imageView) {
        TranslateTransition tt = new TranslateTransition(Duration.seconds(0.5), imageView);
        tt.setFromX(pos1_x);
        tt.setFromZ(0);
        tt.setToX(pos0_x);
        tt.setToZ(pos_z);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5), imageView);
        ft.setFromValue(1);
        ft.setToValue(0.5);
        ParallelTransition pt = new ParallelTransition(imageView, tt, ft);
        pt.play();
    }

    private void rtmAnimate(ImageView imageView) {
        TranslateTransition tt = new TranslateTransition(Duration.seconds(0.5), imageView);
        tt.setFromX(pos2_x);
        tt.setFromZ(pos_z);
        tt.setToX(pos1_x);
        tt.setToZ(0);
        FadeTransition ft = new FadeTransition(Duration.seconds(0.5), imageView);
        ft.setFromValue(0.5);
        ft.setToValue(1);
        ParallelTransition pt = new ParallelTransition(imageView, tt, ft);
        pt.play();
    }

    private void inChangeIndex() {
        ImageView iv = list.get(0);
        ImageView tmp;
        for (int i = 1; i < list.size(); i++) {
            tmp = list.get(i);
            list.set(i, iv);
            iv = tmp;
        }
        list.set(0, iv);
    }

    private void deChangeIndex() {
        ImageView iv = list.get(list.size() - 1);
        ImageView tmp;
        for (int i = list.size() - 2; i >= 0; i--) {
            tmp = list.get(i);
            list.set(i, iv);
            iv = tmp;
        }
        list.set(list.size() - 1, iv);
    }
}
```

#### 7、落雪动画

```java
import javafx.animation.*;
import javafx.application.Application;
import javafx.scene.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author huayunliufeng
 * @date_time 2023/9/10 22:07
 * @desc
 */
public class App extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        double width = 800;
        double height = 600;
        // 布局
        AnchorPane anchorPane = new AnchorPane();
        Scene scene = new Scene(anchorPane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("JavaFX");
        primaryStage.setWidth(width);
        primaryStage.setHeight(height);
        primaryStage.setFullScreen(true);
        primaryStage.centerOnScreen();
        primaryStage.show();

        ImageView imageView = new ImageView(Utils.getImgExternalForm("img4.jpg"));
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(scene.getWidth());
        imageView.setOpacity(0.8);
        Node flowerView = getFlowerView(30, (int) scene.getWidth(), (int) scene.getHeight(), 2000);
        StackPane stackPane = new StackPane(imageView, flowerView);
        anchorPane.getChildren().addAll(stackPane);
    }

    private Node getFlowerView(int number, int w, int h, int z) {
        List<ImageView> list = new ArrayList<>(number);
        int pos_x, pos_y, pos_z;
        Random random = new Random();
        for (int i = 0; i < number; i++) {
            ImageView iv = new ImageView(Utils.getImgExternalForm("雪花.png"));
            iv.setPreserveRatio(true);
            iv.setFitWidth(100);
            pos_x = Utils.getIntRandom(0, w);
            pos_y = Utils.getIntRandom(0, 20);
            pos_z = Utils.getIntRandom(0, z);
            iv.setTranslateX(pos_x);
            iv.setTranslateY(pos_y);
            iv.setTranslateZ(pos_z);
            list.add(iv);
        }
        AnchorPane anchorPane = new AnchorPane();
        anchorPane.getChildren().addAll(list);
        SubScene subScene = new SubScene(anchorPane, w, h, true, SceneAntialiasing.BALANCED);
        PerspectiveCamera camera = new PerspectiveCamera();
        subScene.setCamera(camera);

        for (ImageView imageView : list) {
            double time = random.nextDouble() * 10 + 5;
            // 位移动画
            TranslateTransition tt = new TranslateTransition(Duration.seconds(time));
            tt.setFromX(imageView.getTranslateX());
            tt.setFromY(imageView.getTranslateY());
            tt.setByX(400);
            tt.setToY(h);
            // 旋转动画
            RotateTransition rt = new RotateTransition(Duration.seconds(time));
            rt.setFromAngle(0);
            rt.setToAngle(360);
            // 透明度动画
            FadeTransition ft1 = new FadeTransition(Duration.seconds(time / 2));
            ft1.setFromValue(0);
            ft1.setToValue(1);
            FadeTransition ft2 = new FadeTransition(Duration.seconds(3));
            ft2.setFromValue(1);
            ft2.setToValue(0);
            // 串行动画
            SequentialTransition st = new SequentialTransition(ft1, ft2);
		   // 并行动画
            ParallelTransition pt = new ParallelTransition();
            pt.setNode(imageView);
            pt.getChildren().addAll(tt, st, rt);
            pt.setCycleCount(Animation.INDEFINITE);
            pt.play();
        }
        return subScene;
    }
}

```

#### 8、区域截图

```java
/*
 * © 2023 huayunliufeng保留所有权利, 依据GPL许可证发布。
 * 请勿更改或删除版权声明或此文件头。
 * 此代码是免费软件, 您可以重新分发和/或修改它。
 * 开源是希望它有用, 但不对代码做任何保证。
 * 如有疑问请联系: huayunliufeng@163.com
 */

package priv.zq;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.robot.Robot;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * @author huayunliufeng
 * @date_time 2023/9/24 18:41
 * @desc
 */
public class SnapshotTest extends Application {
    private double x0, x1, y0, y1;
    private final Polygon polygon = new Polygon();
    private final Label label = new Label();
    private final Button complete = new Button("截图完成");
    private final ImageView imageView = new ImageView();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        int width = 800;
        int height = 600;
        AnchorPane root = new AnchorPane();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setWidth(width);
        primaryStage.setHeight(height);
        primaryStage.setTitle("JavaFX截图");
        primaryStage.show();
        Button button1 = new Button("点击截图");
        Button button2 = new Button("点击保存");
        imageView.setPreserveRatio(true);
        HBox hBox = new HBox(20, button1, button2);
        VBox vBox = new VBox(20, hBox, imageView);
        root.getChildren().addAll(vBox);
        button1.setOnAction(event -> {
            show(primaryStage);
        });
        button2.setOnAction(event -> {
            try {
                Image image = imageView.getImage();
                if (image == null) {
                    return;
                }
                FileChooser fileChooser = new FileChooser();
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("保存类型", "*.jpg", "*.png"));
                File file = fileChooser.showSaveDialog(primaryStage);
                if (file != null) {
                    String fileExtension = "";
                    String fileName = file.getName();
                    int index = fileName.lastIndexOf(".");
                    if (index != -1 && index != 0) {
                        fileExtension = fileName.substring(index + 1);
                    }
                    // 输出为jpg格式的方式
                    BufferedImage bufferedImage = null;
                    if ("jpg".equalsIgnoreCase(fileExtension)) {
                        bufferedImage = new BufferedImage((int) image.getWidth(), (int) image.getHeight(), BufferedImage.TYPE_INT_RGB);
                    }
                    ImageIO.write(SwingFXUtils.fromFXImage(image, bufferedImage), fileExtension, file);
                }
            } catch (Exception ignore) {
            }
        });
        // 设置快捷键
        Mnemonic mnemonic = new Mnemonic(button1, KeyCombination.valueOf("ctrl+alt+p"));
        scene.addMnemonic(mnemonic);
    }

    private void show(Stage primaryStage) {
        // 最小化
        primaryStage.setIconified(true);
        // 截屏用的窗口
        Stage stage = new Stage(StageStyle.TRANSPARENT);
        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setStyle("-fx-background-color: rgba(169,183,169,0.2);");
        Scene scene = new Scene(anchorPane);
        scene.setFill(Paint.valueOf("#ffffff00"));
        stage.setScene(scene);
        stage.setWidth(10);
        stage.setHeight(10);
        stage.setFullScreenExitHint("");
        stage.setFullScreen(true);
        stage.show();
        scene.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ESCAPE) {
                stage.close();
                primaryStage.setIconified(false);
            }
        });
        complete.setOnAction(e -> {
            getScreenImg(stage, primaryStage);
        });
        drag(anchorPane);
    }

    private void drag(Pane pane) {
        // 鼠标按下
        pane.setOnMousePressed(event -> {
            x0 = event.getScreenX();
            y0 = event.getScreenY();
            // 初始化截图区域
            polygon.getPoints().clear();
            polygon.setFill(null);
            polygon.setStroke(Paint.valueOf("#dd5412"));
            polygon.setStrokeType(StrokeType.OUTSIDE);
            polygon.setStrokeWidth(2);
            // 显示截图区域宽和高
            label.setText("");
            label.setFont(Font.font("微软雅黑", 15));
            label.setTextFill(Paint.valueOf("#ff0000"));
            label.setPrefHeight(30);
            pane.getChildren().addAll(polygon, label, complete);
            complete.setVisible(false);
        });
        // 拖拽检测
        pane.setOnDragDetected(event -> {
            pane.startFullDrag();
        });
        // 拖拽更改截图区域大小
        pane.setOnMouseDragOver(event -> {
            x1 = event.getScreenX();
            y1 = event.getScreenY();
            // 先把之前的点清空
            polygon.getPoints().clear();
            // 顺时针画矩形
            polygon.getPoints().addAll(x0, y0, x0, y1, x1, y1, x1, y0);
            if (x1 < x0) {
                label.setLayoutX(x0 - label.getWidth());
                label.setTextAlignment(TextAlignment.RIGHT);
            } else {
                label.setLayoutX(x0);
                label.setTextAlignment(TextAlignment.LEFT);
            }
            label.setLayoutY(y1 < y0 ? y0 : y0 - 30);
            label.setText("宽度: " + (int) Math.abs(x1 - x0) + ", 高度: " + (int) Math.abs(y1 - y0));
        });

        // 鼠标松开, 截图完成
        pane.setOnMouseDragExited(event -> {
            complete.setVisible(true);
            complete.setLayoutX(x1 < x0 ? x1 : x1 - complete.getWidth());
            complete.setLayoutY(y1 < y0 ? y1 - complete.getHeight() - 5 : y1 + 5);
        });
    }

    private void getScreenImg(Stage stage, Stage primaryStage) {
        stage.close();
        primaryStage.setIconified(false);
        double width = Math.abs(x1 - x0);
        double height = Math.abs(y1 - y0);
        WritableImage image = getWritableImage(width, height);
        imageView.setFitWidth(width);
        imageView.setImage(image);
        // 将图片保存到剪切板中
        Clipboard clipboard = Clipboard.getSystemClipboard();
        ClipboardContent content = new ClipboardContent();
        content.putImage(image);
        clipboard.setContent(content);
    }

    private WritableImage getWritableImage(double width, double height) {
        Robot robot = new Robot();
        WritableImage image = new WritableImage((int) width, (int) height);
        if (x1 >= x0) {
            if (y1 >= y0) {
                image = robot.getScreenCapture(image, x0, y0, width, height);
            } else {
                image = robot.getScreenCapture(image, x0, y1, width, height);
            }
        } else {
            if (y1 >= y0) {
                image = robot.getScreenCapture(image, x1, y0, width, height);
            } else {
                image = robot.getScreenCapture(image, x1, y1, width, height);
            }
        }
        return image;
    }
}
```

### 九、公共类

#### 1、`Student`

```java
package priv.zq.bean;

import javafx.beans.property.*;

import java.util.Objects;

/**
 * @author huayunliufeng
 * @date_time 2023/8/29 19:41
 * @desc
 */
public class Student {
    private static int ID_GENERATOR = 0;
    // 可监听的包装类型
    private ReadOnlyIntegerWrapper id;
    private ReadOnlyStringWrapper name;
    private ReadOnlyDoubleWrapper score;
    private ReadOnlyBooleanWrapper isChanged;

    public Student(String name, double score, boolean isChanged) {
        setId(++ID_GENERATOR);
        setName(name);
        setScore(score);
        setIsChanged(isChanged);
    }

    public int getId() {
        return idProperty().get();
    }

    public String getName() {
        return nameProperty().get();
    }

    public double getScore() {
        return scoreProperty().get();
    }

    public boolean isChanged() {
        return isChangedProperty().get();
    }

    public ReadOnlyIntegerProperty idProperty() {
        return getIdProperty().getReadOnlyProperty();
    }

    public ReadOnlyStringProperty nameProperty() {
        return getNameProperty().getReadOnlyProperty();
    }

    public ReadOnlyDoubleProperty scoreProperty() {
        return getScoreProperty().getReadOnlyProperty();
    }

    public ReadOnlyBooleanProperty isChangedProperty() {
        return getIsChangedProperty().getReadOnlyProperty();
    }

    private ReadOnlyIntegerWrapper getIdProperty() {
        if (id == null) {
            id = new ReadOnlyIntegerWrapper();
        }
        return id;
    }

    private ReadOnlyStringWrapper getNameProperty() {
        if (name == null) {
            name = new ReadOnlyStringWrapper();
        }
        return name;
    }

    private ReadOnlyDoubleWrapper getScoreProperty() {
        if (score == null) {
            score = new ReadOnlyDoubleWrapper();
        }
        return score;
    }

    private ReadOnlyBooleanWrapper getIsChangedProperty() {
        if (isChanged == null) {
            isChanged = new ReadOnlyBooleanWrapper();
        }
        return isChanged;
    }

    private void setId(int id) {
        getIdProperty().set(id);
    }

    public void setName(String name) {
        getNameProperty().set(name);
    }

    public void setScore(double score) {
        getScoreProperty().set(score);
    }

    public void setIsChanged(boolean isChanged) {
        getIsChangedProperty().set(isChanged);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Student)) {
            return false;
        }
        Student student = (Student) obj;
        return getId() == student.getId() && getName().equals(student.getName());
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + getId() +
                ", name=" + getName() +
                ", score=" + getScore() +
                ", isChanged=" + isChanged() +
                '}';
    }
}
```

#### 2、`Utils`

```java
package priv.zq;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.Screen;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.Random;

/**
 * @author huayunliufeng
 * @date_time 2023/8/25 12:41
 * @desc
 */
public class Utils {

    // 获取主屏幕
    private static final Screen screen = Screen.getPrimary();
    private static Random random;
    private static final char[] colorEle = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static RadioButton[] getRadioButtonList(int size) {
        RadioButton[] radioButtons = new RadioButton[size];
        for (int i = 0; i < size; i++) {
            radioButtons[i] = new RadioButton("radioButton" + i);
        }
        return radioButtons;
    }

    public static CheckBox[] getCheckBoxList(int size) {
        CheckBox[] checkBoxes = new CheckBox[size];
        for (int i = 0; i < size; i++) {
            checkBoxes[i] = new CheckBox("checkBox" + i);
        }
        return checkBoxes;
    }

    public static Button[] getButtonList(int size) {
        Button[] buttons = new Button[size];
        for (int i = 0; i < size; i++) {
            buttons[i] = new Button("button" + i);
        }
        return buttons;
    }

    public static MenuItem[] getMenuItems(int size) {
        MenuItem[] menuItems = new MenuItem[size];
        for (int i = 0; i < size; i++) {
            menuItems[i] = new MenuItem("菜单项" + i);

        }
        return menuItems;
    }

    public static ImageView getFaviconPNG(String filename) {
        URL url = Utils.class.getClassLoader().getResource(filename);
        if (url != null) {
            ImageView imageView = new ImageView(url.toExternalForm());
            imageView.setFitWidth(60);
            imageView.setFitHeight(50);
            return imageView;
        }
        return null;
    }

    public static String getImgExternalForm(String filename) {
        return Objects.requireNonNull(Utils.class.getClassLoader().getResource(filename)).toExternalForm();
    }

    public static URL getFileURL(String filename) {
        return Utils.class.getClassLoader().getResource(filename);
    }


    public static void listAllFiles(File file, String level) {
        if (file != null) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File file1 : files) {
                    System.out.println(level + file1.getName());
                    if (file1.isDirectory()) {
                        listAllFiles(file1, "--" + level);
                    }
                }
            }
        }
    }

    public static Text[] getTextList(int size) {
        Text[] texts = new Text[size];
        for (int i = 0; i < size; i++) {
            texts[i] = new Text("text----------》" + i);
        }
        return texts;
    }

    private static void printARGB(PixelReader pixelReader, int x, int y) {
        printARGB(pixelReader.getArgb(x, y));
    }


    private static void printARGB(int argbValue) {
        // 获取透明度
        int alpha = argbValue >> 24 & 0xff;
        // 红色值
        int red = argbValue >> 16 & 0xff;
        // 绿色值
        int green = argbValue >> 8 & 0xff;
        // 蓝色值
        int blue = argbValue & 0xff;
        System.out.println("alpha = " + alpha + ", red = " + red + ", green = " + green + ", blue = " + blue);
    }

    private static int getRandomARGB() {
        int alpha = getIntRandom(0, 256);
        int red = getIntRandom(0, 256);
        int green = getIntRandom(0, 256);
        int blue = getIntRandom(0, 256);
        return ((blue & 0xFF) << 24) | ((green & 0xFF) << 16) | ((red & 0xFF) << 8) | (alpha & 0xFF);
    }

    public static int getIntRandom(int min, int max) {
        if (random == null) {
            random = new Random();
        }
        return random.nextInt(max) + min;
    }

    public static String getResourcesPath(String filename) {
        return "D:\\dev\\ideaworkspace\\JavaFXTest\\src\\main\\resources\\" + filename;
    }

    public static String getThreadName() {
        return "当前线程名: " + Thread.currentThread().getName();
    }

    public static void fillFile(int filesize) {
        byte[] bytes = new byte[1024 * 1024];
        String filename = "fillFile_" + filesize + "MB";
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(Utils.getResourcesPath(filename)))) {
            for (int i = 0; i < filesize; i++) {
                bos.write(bytes);
            }
            bos.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getRandomColor(boolean isOpacity) {
        StringBuilder builder = new StringBuilder("#");
        Random random = new Random();
        int len = isOpacity ? 8 : 6;
        for (int i = 0; i < len; i++) {
            builder.append(colorEle[random.nextInt(colorEle.length)]);
        }
        return builder.toString();
    }

    public static Paint getRandomPaintColor(boolean isOpacity) {
        return Paint.valueOf(getRandomColor(isOpacity));
    }

    public static Color getRandomColorColor(boolean isOpacity) {
        return Color.valueOf(getRandomColor(isOpacity));
    }

    public static String getResourceFromAppClass(String filename){
        URL url = App.class.getResource(filename);
        assert url != null;
        return url.toExternalForm();
    }

    public static double getScreenMaxWidth(){
        // 获取屏幕的宽度和高度
        return screen.getBounds().getWidth();
    }

    public static double getScreenMaxHeight(){
        return screen.getBounds().getHeight();
    }
}
```





