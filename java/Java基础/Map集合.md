### Map集合

- 实现了 Map 接口的集合都称为 Map 集合。属于上层接口，与 Collection 接口平级。
- Map 集合的特征。接口的声明：Interface Map<K,V>，有两个泛型标志。K表示 key（关键字），V 表示 value（内容）。Map 集合中的元素都是通过 key 来表示 value，每存入一个元素，该元素必须要指定一个唯一的 key ，如果要取出元素可以通过 key 来取，以上是它的根本特征。
- List 和 Set 集合中的元素都是单个的，这两种又称为单列集合。而 Map 的每个元素都含有 key 和 value ，因此又称双列集合。
- Map 集合内部可以分成两部分，一部分是 key 的集合，一个是 value 的集合，而 key 是属于 Set 集合，value 属于 List，因此 Map 中的 key 不能重复，List 可以重复，key 是无序的（针对 HashSet 来讲），因此它的最常用的实现类 HashMap 就是无序的。

### Map的实现类

- HashMap 类

  - 是 Map 集合最常用的集合，它的 key 存放在 HashSet 集合中。对 Map 的基本操作，包括增删改查。

- Map 的迭代

  1. 通过 key 来得到 value ，因此需要先把存有 key 的集合提出来，再迭代 key 的集合，再通过 key 得到 value。
  2. Map集合中的每个 k v 对 在内部都表示为一个 Entry 对象，这种迭代就是先取出包含所有 entry 对象的集合，然后再对该集合进行迭代，最后从 entry 中获取 key 和 value。

  ```java
  package com.zhong.test_2;
  
  import java.util.HashMap;
  import java.util.Map;
  import java.util.Set;
  
  public class MapDemo {
      public static void main(String[] args) {
          Map<String,String> map = new HashMap<>();
          //添加
          map.put("k1","v1");
          map.put("k2","v2");
          map.put("k3","v3");
          //得到key的集合
          Set<String> keys = map.keySet();
          for (String key : keys) {
              System.out.println(key + "->" + map.get(key));
          }
  
          //得到Entry的集合
          Set<Map.Entry<String,String>> entries = map.entrySet();
          for (Map.Entry<String, String> entry : entries) {
              System.out.println(entry.getKey() + "-+>" + entry.getValue());
          }
      }
  }
  ```
  
  - 如果 Map 中的 key 是自定义类，则必须重写 hashCode 和 equals 方法。否则可能会内存泄露

