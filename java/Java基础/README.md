### 一、File 类

- IO体系的基石：五类一接口，File 类就是五类之一。
- File 用来表示文件或目录，一个 File 对象可以用来表示一个文件或一个目录。

1. 构造方法

   - File(String pathname) pathname 是文件或目录的路径

2. 四个路径分隔符

   - 如果不确定程序在哪种系统上执行，可以用 File.separatorChar 来表示分隔符

3. 得到的方法（getXXX）

   - ```java
     System.out.println("绝对路径：" + file.getAbsolutePath());
     ```

   - ```java
     System.out.println("文件名：" + file.getName());
     ```

   - ```java
     System.out.println("上级目录路径：" + file.getParent());
     ```

   - ```java
     System.out.println("磁盘总空间：" + file.getTotalSpace() / (1024 * 1024 * 1024 * 1.0) + " GB");
     ```

   - ```java
     System.out.println("磁盘可用空间（估计）：" + file.getFreeSpace() / (1024 * 1024 * 1024 * 1.0) + " GB");
     ```

   - ```java
     System.out.println("磁盘可用空间（准确）：" + file.getUsableSpace() / (1024 * 1024 * 1024 * 1.0) + "GB");
     ```

   - ```java
     System.out.println("文件大小：" + file.length() / 1024 * 1.0 + " KB");
     ```

4. 判断的方法（isXXX）

   - ```java
     System.out.println("是否是文件：" + file.isFile());
     ```

   - ```java
     System.out.println("是否是目录：" + file.isDirectory());
     ```

   - ```java
     System.out.println("文件是否存在：" + file.exists());
     ```

   - ```java
     System.out.println("是否是隐藏文件：" + file.isHidden());
     ```

   - ```java
     System.out.println("是否可读：" + file.canRead());
     ```

   - ```java
     System.out.println("是否可写：" + file.canWrite());
     ```

   - ```java
     System.out.println("是否绝对路径：" + file.isAbsolute());*/
     ```

5. 其他方法

   - ```java
     //列出下一级的名称
     String list[] = dirFile.list();
     ```

   - ```java
     //列出下一级各目录和文件对应的File对象
     File files[] = dirFile.listFiles();
     ```

   - ```java
     fileCreat.mkdir();//创建目录
     fileCreat.delete();//删除文件或目录
     newFile.createNewFile();//创建文件
     ```

### 二、递归算法

- 所有操作系统的目录都以树形结构来进行组织，因此目录之间，目录与文件之间存在着层次关系.

- 具有层次关系的数据有很多种，对于具有层次（树形）结构的数据，一般都需要用递归算法来展现它们的层次关系。

- 递归：在方法中调用本方法。每次方法调用都会进入到下一级，就是同一方法不断压栈的过程。递归的层次必须是有限，如果超限会产生栈溢出的错误。在方法中必须要有返回条件，该条件在限制层数范围内必须要成立。

- 递：逐层传递

- 归：逐层返回

- 用递归算法逐层列出指定目录下的所有的目录及文件，这也是一种遍历，原来写的遍历主要针对数组这类线性结构（一层for），还有集合（Iterator），都比较简单。

  ```java
  package com.zhong.test_5;
  
  import java.io.File;
  
  public class FileDiGUIDemo {
      public static void main(String[] args) {
          File dir = new File("E:\\IDEA project\\src\\com\\zhong\\test_5");
          if(dir.isFile()){
              System.out.println("文件：" + dir.getName());
          }
          //是目录调用方法
          printDir(dir,0);
      }
  
      static void printDir(File dir,int level){
          //得到目录下的所有的内容（File对象）
          File[] dirs = dir.listFiles();
          for (File file : dirs) {
              System.out.println((file.isDirectory()?"目录":"文件") + level + file.getName());
              if(file.isDirectory()) printDir(file,level + 1);
          }
      }
  }
  ```

### 三、文件的搜索

1. 使用 public File[ ] listFiles(FileFilter filter) 方法，传递 FileFilter 接口类型的对象。

2. 接口中的 accept 方法的调用时机，会针对 files 中的每个 file 都调用一次，如果返回 false 则当前的 file 被剔除；方法返回 true ，表示当前文件是目标文件（列出）。

  ```java
  package com.zhong.test_5;
  
  import java.io.File;
  
  public class SearchDemo {
      public static void main(String[] args) {
          File dir = new File("E:\\IDEA project\\src\\com\\zhong\\test_5");
          printDir(dir,0);
      }
  
      static void printDir(File dir,int level){
          //得到目录下的所有的内容（File对象）
          //方法返回true，表示当前文件是目标文件（列出），否则不列出
  //accept方法的调用时机，会针对files中的每个file都调用一次
          File[] dirs = dir.listFiles(pathname -> {
              if(pathname.isDirectory() || pathname.getName().endsWith(".java")) return true;
              else return false;
          });
          for (File file : dirs) {
              if(file.isFile()) System.out.println(("文件" + level + file.getName()));
              else printDir(file,level + 1);
          }
      }
  }
  ```

### 四、IO流的基本概念

1. I 表示 input，输入

2. O 表示 output，输出

3. 流，一组有序流动的字节

4. java 的输入输出模型

5. 字节，计算机以字节为单位来存储和传输数据，1Byte = 8b

6. 字符、占用两个字节，java 表示字符使用 unicode 编码集，国际统一编码集，可以表示任何国家的语言和文字

7. 汉字，可以用gb2312（gbk）和 utf-8 两种字符集来表示，gbk 表示一个汉字用两个字节，utf-8 用三个字节，在信息通讯中，utf-8 传递汉字是以字节为单位。char 类型占用两个字节，一个汉字是一个字符，而 utf-8超出了两字节的范围，因此 java 内部需要做一些特殊处理，才能正确的表示汉字。

8. java IO 的类层级结构

   - IO 中的五类一接口，其中 InputStream，OutputStream，Reader，Writer

9. 字节流

   - 计算机中的任何数据（文本文件，图形图像，声音视频）都是以字节为单位组成的，因此字节流可以传输任何数据。
   - 字节输入流就是把文件，控制台，网络或内存中的一份数据，读到 java 进程中。
   - 字节输入流的基础类是 InputStream，它是一个抽象类。
   - 流类中有 close()，用来关闭和释放资源。在java中，如果类有 close() 方法，该类就属于资源类。资源类在用完后需要使用 close() 方法进行释放，以保证资源的安全。
   - 输入流中提供重载的 read() 方法，来实现对数据的读取。抽象方法，是输入流读取数据的基础方法，调用一次就读一个字节。

   1. 通过文件字节输入流来了解字节输入流的用法

      - 文件字节输入流以字节为单位把磁盘上文件中的数据读取（输入）到java程序的内存中

      - 在实际编程中，用文件字节输入流读取数据 ，一般采用的方法是循环读取，每次读一部分，再处理这部分。用 byte[ ] 数组来接收本次读到的字节

        ```java
        package com.zhong.test_5;
        
        import java.io.FileInputStream;
        import java.io.IOException;
        
        public class InputStreamDemo {
            public static void main(String[] args) {
                FileInputStream fis = null;
                try {
                    //创建流对象
                    fis = new FileInputStream("E:\\IDEA project\\src\\com\\zhong\\test_5\\test.txt");
                    //定义一个int类型的变量，接收每次操作的返回值
                  //  int len = 1;
                    //定义一个字节数组，存放本次读取到的所有字节
                    byte[] b = new byte[1024];
                    //循环读取，可以通过多次read(byte[])方法的调用，来完成对整个文件的读操作
                    while (fis.read(b) != -1){
                        String str = new String(b,"utf-8");
                        System.out.println(str);
                    }
        
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        assert fis != null;
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        ```

   2. 通过文件字节输出流来了解字节输出流的用法

      - 通过构造方法的参数，我们要知道，name 参数是输出的文件路径，如果该文件不存在，会创建文件再进行输出。append 为 false， 本次执行的输出内容会覆盖老的内容，如果为 true，本次输出的内容会接在上次内容之后。默认是 false

      - 输出流都提供了 write() 来写数据，flush() 作用是刷新缓冲区的数据，可以把数据强制的写入到文件中

        ```java
        package com.zhong.test_5;
        
        import java.io.FileOutputStream;
        import java.io.IOException;
        
        public class FileOutputStreamDemo {
            public static void main(String[] args) {
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream("E:\\IDEA project\\src\\com\\zhong\\test_5\\test.txt",true);
                    fos.write("\n大家好".getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        assert fos != null;
                        fos.flush();
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        ```

   3. 使用文件的输入输出流实现文件的复制

      - ```java
        package com.zhong.test_5;
        
        import java.io.FileInputStream;
        import java.io.FileOutputStream;
        import java.io.IOException;
        
        public class FileCopeDemo {
            public static void main(String[] args) {
                FileInputStream fis = null;
                FileOutputStream fos = null;
                try {
                    //创建流对象
                    fis = new FileInputStream("E:\\IDEA project\\src\\com\\zhong\\test_5\\test.txt");
                    fos = new FileOutputStream("E:\\IDEA project\\src\\com\\zhong\\test_5\\testCope.txt",true);
        
                    //定义一个int类型的变量，接收每次操作的返回值
                      int len;
                    //定义一个字节数组，存放本次读取到的所有字节
                    byte[] b = new byte[1024];
                    //循环读取，可以通过多次read(byte[])方法的调用，来完成对整个文件的读操作
                    while ((len = fis.read(b)) != -1){
                        fos.write(b,0,len);
                        fos.flush();
                    }
        
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        assert fis != null;
                        assert fos != null;
                        fos.close();
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        ```


### 五、字符流

- 它可以字符为单位进行数据的读写。字符流只能读取文本数据。每次读一个字符比每次读一个字节更快，如果读写的数据是文本或字符就应当使用字符流。

  ```java
  package com.zhong.test_5;
  
  import java.io.FileReader;
  import java.io.FileWriter;
  import java.io.IOException;
  
  public class ReadWriterDemo {
      public static void main(String[] args) {
          FileReader fr = null;
          FileWriter fw = null;
          try {
              fr = new FileReader("E:\\IDEA project\\src\\com\\zhong\\test_5\\test.txt");
              fw = new FileWriter("E:\\IDEA project\\src\\com\\zhong\\test_5\\testCope1.txt",true);
              int len;
              char[] chars = new char[1024];
              while ((len = fr.read(chars)) != -1){
                  System.out.println(String.valueOf(chars));
                  fw.write(chars,0,len);
              }
          } catch (IOException e) {
              e.printStackTrace();
          }finally {
              try {
                  fw.flush();
                  fw.close();
                  fr.close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
  }
  ```

### 六、属性流

- Properties 类继承自 HashTable，因此它是线程安全的，它可以处理的数据也是 K V 的形式。本类提供了写属性和读属性值的方法，而且还提供了 load(Reader reader)，该方法可以读取磁盘上属性文件的内容。
- 属性文件是 java 程序使用具有规定格式的文本文件。它规定文件中的内容都是以键值对的形式出现。属性名 = 属性值，age = 21,23，如果一行写不完，行的末尾要用 / ，表示下一行的内容要紧接上一行。
- 属性文件一般被框架使用，有些程序中需要使用的值是固定的，但是最好不要把该值直接写到程序中，把它写到外部的属性文件中是一种好的方法。开发项目时，可能会把一些配置文件放在属性文件中。

### 七、java设计模式

1. 设计模式不是 java 语言中固有的内容，它应该属于 java 的高级知识。
2. 设计模式的出现，经历了一个很长的过程，很多工程师在长期的编程实践中会遇到各种各样的问题，就会用各种方法去解决，最后总结了一些固定的设计套路，把这些方法进行一些抽象并整理，就形成了设计模式。
3. 设计模式是为了解决问题而生的。
4. 当下 java 具有23种设计模式

### 八、装饰器模式

1. 作用：给一个对象添加额外的功能。
2. 继承可以让类的功能得到扩展，在有些情况下使用继承可能会让子类迅速膨胀，会让程序变得臃肿。
3. 继承是一种静态的设计方式，增强的功能在程序运行之前就必须要完成。
4. 使用装饰器模式可以很好的解决以上继承中的问题。它采用动态的方法也就是程序运行期进行功能的扩展。
5. 装饰器模式的实现，对于设计来讲是有明确的规范，设计者必须按照此模式的具体要求去编写程序。

### 九、用标准的设计工具类绘制类图

- 类图是项目设计阶段对所有项目中使用的接口和类采用一种图形的方式进行表达，其中各种类及接口之间的关系必须要表达出来
- sysbase公司，power designer工具软件，数据库的设计工具，设计实体关系图（数据库概念模型），设计数据库的物理模型。把物理模型转换为创建库和表等对象的 SQL 命令。也可以把 SQL 创建库和表的命令转换为物理模型（逆向工程）。
- 作为类图的设计工具。
- 可以绘制符合 UML 统一建模语言规范的各种图形，比如功能图，时序图，流程图，用例图等。
- 主要使用者：公司架构师，DBA。

### 十、缓冲流

- 此流内部提供了缓冲流，缓冲流也是内存中的一块区域，当数据从某处流向另一处的过程，可以在缓冲区中做暂时的停留。停留的作用在于：计算机在内存中对数据的操作速度很快，而外设相当于计算机来讲速度非常慢，为了在两者之间实现较好的同步，可以先把要读写的数据放入缓冲区，一直到缓冲区再一次进行读写。

- 缓冲流属于处理流，共有四个，分别是输入输出和字节字符，其设计采用的是装饰器模式。

  ```java
  package com.zhong.test_6;
  
  import java.io.*;
  
  public class BufferDemo {
      public static void main(String[] args) {
          FileInputStream fis = null;
          FileOutputStream fos = null;
          long begin = System.currentTimeMillis();
          try {
              fis = new FileInputStream("E:\\IDEA project\\src\\com\\zhong\\test_5\\背景.jpg");
              fos = new FileOutputStream("E:\\IDEA project\\src\\com\\zhong\\test_6\\cope.jpg");
              BufferedInputStream bis = new BufferedInputStream(fis);
              BufferedOutputStream bos = new BufferedOutputStream(fos);
              int len;
              byte[] b = new byte[1024];
              while((len = bis.read(b)) != -1){
                  bos.write(b,0,len);
              }
              bos.flush();
              bos.close();
              bis.close();
          } catch (IOException e) {
              e.printStackTrace();
          }finally {
              try {
  
                  assert fos != null;
                  fos.close();
                  fis.close();
  
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
          long end = System.currentTimeMillis();
          System.out.println("耗时" + (end - begin) + "ms");
  
      }
  }
  ```

### 十一、转换流

- 它是字节流到字符流之间的桥梁。文件字符输入输出流都是从转换流中继承而来的。计算机中的所有数据都是由字节组成的，字符流的底层依然是采用字节流来读写，所以转换流就起到了连接（字节转字符或字符转字节）的作用。

- 转换具有编码和解码的功能，因为字符可以对应不同的编码字符集，在字符与字节相互转换的过程中，就要利用编码集来实现编码与解码的功能。

- 转换流是文件字符输入输出流的基础，转换流才是实现字符流功能的主类。StreamDecoder，StreamEncoder。这种扩展不是基于继承来实现，此种方式称为组合。组合可以让类的功能变得丰富多彩。

  ```java
  package com.zhong.test_6;
  
  import java.io.*;
  
  public class BufferedReaderWriterDemo {
      public static void main(String[] args) {
          BufferedReader br = null;
          BufferedWriter bw = null;
          try {
              br = new BufferedReader(new InputStreamReader(new FileInputStream("E:\\IDEA project\\src\\com\\zhong\\test_6\\test.txt"),"utf-8"));
              bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("E:\\IDEA project\\src\\com\\zhong\\test_6\\testCope.txt"),"utf-8"));
              String lineStr;
              while((lineStr = br.readLine()) != null){
                  bw.write(lineStr);
                  bw.newLine();
              }
          } catch (IOException e) {
              e.printStackTrace();
          }finally {
              try {
                  bw.close();
                  br.close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
  }
  ```

### 十二、对象序列化及反序列化

- 序列化就是把内存中对象的数据以有序的字节保存到文件或者发送到网络。

- 实现对象序列化的前提，就是对象必须实现序列化接口（serializable），此接口没有方法，它是标识性接口，用来指定当前对象可以执行序列化操作。实现序列化的过程要使用 ObjectOutputStream（对象输出流）来完成。

- 反序列化，就是把文件或网络上的用有序字节表示的对象数据读入到内存中，并生成一个对象。实现反序列化过程要使用 ObjectInputStream。

- transient 关键字，称为瞬态关键字，实现序列化接口的类中的属性被它修饰，该属性就不会被序列化。如果对象被序列化后，类被修改了，反序列化时可能会抛出异常。这种情况下类可以指定版本号，private static final long serialVersionUID = XXXXXXXXXXXXL；以阻止这种异常的产生。

  ```java
  package com.zhong.test_6;
  
  import java.io.*;
  
  public class Person implements Serializable {
      private static final long serialVersionUID = -2328718976600459996L;
      private String name;
      private transient int age;//瞬态，加了transient不能被序列化，也就是不能被写入内存
      private char sex;
      public Person(String name, int age) {
          this.name = name;
          this.age = age;
      }
  
      public Person() { }
  
      public String getName() {
          return name;
      }
  
      public void setName(String name) {
          this.name = name;
      }
  
      public int getAge() {
          return age;
      }
  
      public void setAge(int age) {
          this.age = age;
      }
  
      public static void main(String[] args) {
          Person p1 = new Person("小明",25);
          ObjectOutputStream cos = null;
          ObjectInputStream ois = null;
          try {
              /*cos = new ObjectOutputStream(new FileOutputStream("E:\\IDEA project\\src\\com\\zhong\\test_6\\person.obj"));
              cos.writeObject(p1);*/
              ois = new ObjectInputStream(new FileInputStream("E:\\IDEA project\\src\\com\\zhong\\test_6\\person.obj"));
              Person o = (Person) ois.readObject();
              System.out.println(o.getName() + " " + o.getAge());
          } catch (IOException e) {
              e.printStackTrace();
          } catch (ClassNotFoundException e) {
              e.printStackTrace();
          } finally {
              try {
                  //cos.close();
                  ois.close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
  
      }
  }
  ```

### 十三、打印流

- PrintStream 字节打印流，原有的作用是把字节数据放在控制台（idea 和操作系统控制台）进行输出。

- 打印流有很多不同参数的 print() 方法，参数是不同的类型，因此具有格式化的输出功能，也就是可以把不同的类型数据转换为 String 再输出。

- PrintWriter 字符打印流，又称为字符格式化打印流。

- 打印流也是处理流，使用时需要传递更底层的流给他。

  ```java
  package com.zhong.test_6;
  
  import java.io.PrintWriter;
  
  public class PrintStream {
      public static void main(String[] args) {
          java.io.PrintStream ps = new java.io.PrintStream(System.out);
          ps.println(new Object());
          PrintWriter pw = new PrintWriter(System.out,true);
          pw.println("abc");
          for (int i = 0; i < 10; i++) {
              pw.print(i + " ");
              pw.flush();
          }
          ps.close();
          pw.close();
      }
  }
  ```

**字节流属于比较底层的流，处理流属于比较高层的流。底层的流与输入输出的目标直接有关，而处理流不知道数据的输入输出目标是谁，它只是在节点流的基础上添加了其他的增强的功能，比如缓冲流添加了缓冲区，打印流添加了格式化的转换（printXXX）。所以处理流又被称为包装流。**

