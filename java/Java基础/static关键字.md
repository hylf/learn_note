### static 关键字

- static 具有静态的含义，它可以用来修饰成员变量，修饰成员方法，也可以修饰内部类，而且也可以在类中修饰一段代码。以上四种 static 的运用会产生类变量（静态变量），类方法（静态方法），静态内部类，静态代码块。

#### 一、静态变量

- 在类中，可以具有成员变量和成员方法，他们是在类中声明的，但是在使用过程中，只能通过类的对象来使用，因此我们认为成员变量和方法属于对象。
- 从对象内存图来看，成员变量的值在堆中对象所在的位置，也就是对象的数据包含了所有成员变量的值。堆中的对象含有对方法区类中方法的引用，内部也有规定，如果是成员方法也只能通过对象去调用。因此，在成员方法中可以使用成员变量。
- 如果类中存在静态变量，static int v ;  它与成员变量最大的区别在于静态变量存放在方法区（其中有个静态区） ，而类的字节码在方法区只能有一份，因此静态变量的个数也只有一个，因此我们就认为静态变量属于类，不属于对象。静态变量并没有被对象直接引用。从这一点看，成员变量可以有多个，因为同一个类可以派生出多个对象，不同的对象都可以拥有同名成员变量，它们各自属于不同的对象。
- 静态变量被类的所有对象所共享，因此有的地方把静态变量称为全局变量。

```java
package test;

public class People {
    private String name;
    public static String count;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
```

```java
package test;

public class Static_Test {
    public static void main(String[] args) {
        People p1 = new People();
        p1.setName("上海人");
        People p2 = new People();
        p2.setName("北京人");
        System.out.println(p1.getName());
        System.out.println(p2.getName());
        //静态变量的使用方式，建议用类名来使用，不要用对象名来使用（虽然可以但不妥当）
        People.count = "中国";
        System.out.println(p1.count);
        System.out.println(p2.count);
        System.out.println(People.count);
    }
}
```

####  二、静态方法

- public static void 方法名() { } 。成员方法的调用一定是通过对象来调用的，因此成员方法与对象的具体属性有紧密的关系，这样可以推断出成员方法中基本上都会使用成员变量，因为成员变量属于对象所有。如果一个方法并不会使用到成员变量，建议把此方法声明为静态方法。
- 静态方法的代码与成员方法的代码都存放在方法区中，但是使用静态方法还是要通过类名来调用，不建议用对象名调用（它与对象没有直接关系）。
- 静态方法也是属于类所有，因此它与对象没有关系，那么在静态方法中就不能使用成员变量。可以使用静态变量。

#### 三、静态代码快

- 静态代码快也属于类的成员之一，static { 语句; }，它是被 static 修饰的一段代码，它具有特殊的含义和作用。静态代码快在类被加载时做初始化执行，因此静态代码的执行时机早于构造方法的执行时机。还有一点，静态变量属于类，保证该类被加载后静态变量应该具有默认值或初值，因此静态代码块的作用就是对静态变量执行初始化的。静态代码块执行时，不可能存在类的对象，
- 类的加载步骤：
  1. 加载：查找和导入Class文件
  2. 链接：其中解析步骤是可以选择的 
     - （a）检查：检查载入的class文件数据的正确性 
     - （b）准备：给类的静态变量分配存储空间 
     - （c）解析：将符号引用转成直接引用
  3. 初始化：对静态变量，静态代码块执行初始化工作

```java
package test;

public class People {
    private String name;
    public static String country;

    static {
        System.out.println("静态代码快执行了");
        country = "中国";
    }

    public People(){
        System.out.println("构造方法执行了");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void printCount(){
        System.out.println("我是"+country+"人。");
    }

}
```

```java
package test;

public class Static_Test {
    public static void main(String[] args) {
        People p1 = new People();
        p1.setName("上海人");
        People.printCount();

    }
}
```

### Arrays 类

- 该类是数组工具类，也在工具包中，它可以针对各种类型的数组实现一些常见的操作，比如排序，查找，转换，比较等。
- 在java中凡是称为工具类的类，它的方法一般都是静态方法，这些方法直接通过类名调用。