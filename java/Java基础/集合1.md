### 一、什么是集合

- java中的集合是一个容器，可以存放多个元素。（集合只能存放对象，集合存放的是对象的引用，称为软拷贝）

### 二、什么是集合框架

- 在java中，所有的集合都实现了各种接口，每个集合都对应具体的类，不同的集合因为实现的接口不同，而具有不同的特征及行为。因此，统一的把集合需要实现的接口的结构及层级，具体的集合类型的各种方法，称为集合框架。

### 三、Collection接口

- 它是集合框架中的根接口，它具有一些基本的操作集合的方法，这些方法是我们使用集合过程中要使用的基础方法。它有两个重要的子接口，一个是List，另一个是Set，这些子接口就会拥有Collection接口的所有方法。List和Set表示两类具有不同特征及行为的集合体系。List和Set接口的具体实现也就是常用的集合类型。

### 四、集合中的迭代器

- 在集合框架中，有一个名称为 Iterable 的接口，也就是可迭代的接口，它是 Collection 接口的父接口，就会具有迭代集合的功能。Iterable 接口提供的主要方法有 iterator() 可用来得到迭代器对象，foreach() 就表示可以使用增强for循环语句来迭代集合。
- 总的来讲，Iterable 提供了循环的手段来迭代元素。其中 iterator() 可以返回一个迭代器对象，实现迭代操作的对象就是它。
- hasnext()，返回boolean，该方法用来判断是否还有未迭代过得元素。next() 方法，该方法可以移动指针，让指针指向下一个未迭代的元素，同时返回当前元素。因此在具体的迭代操作中会把以上两个方法结合起来使用。
- 如果一个集合实现了 Iterable 接口就表示该集合提供了自我迭代功能，这样就可以用迭代器去迭代当前的集合对象。

### 五、增强for循环语句

- for i 语句主要操作的对象是具有索引的数组或 List 集合，在java集合框架中有部分集合的元素没有索引，增强for循环主要就适用于这种类型的循环。即使集合的元素有索引，只要该集合实现了Iterable接口，也可以适用增强for循环进行迭代操作。

- 语法：for(元素的类型 元素变量 : 集合变量){ 操作元素变量 }

- ```java
  package com.zhong.test_3;
  
  import java.util.ArrayList;
  import java.util.Collection;
  import java.util.Iterator;
  
  public class CollectionDemo {
      public static void main(String[] args) {
          Collection list = new ArrayList();
          System.out.println(list.isEmpty());
          //添加元素
          list.add("e1");
          list.add("e2");
          list.add("e3");
          list.add("e4");
          list.add("e5");
          //返回元素个数
          System.out.println(list.size());
          System.out.println(list.isEmpty());
          //删除元素
          list.remove("e3");
          //迭代集合，迭代是数组和集合共用的一种操作，
          // 类似于遍历，利用一个可移动的指针指向不同的集合元素，
          //指针指向哪个元素就可以把元素从该集合中取出来
          //遍历更强调采用某种顺序从集合中取元素，它是操作数组的主要方式。
          Iterator iterator = list.iterator();
          while (iterator.hasNext()){
              Object e = iterator.next();
             // System.out.println(e);
          }
          //增强for循环来实现迭代
          for (Object o : list) {
              System.out.println(o);
          }
  
          //无论什么类型的数据，放入集合后都会被转换成Object类型
          Object[] obj = list.toArray();
          for (Object o : obj) {
              System.out.println(o);
          }
          //删除所有元素
          list.clear();
          System.out.println(list.size());
          System.out.println(list.isEmpty());
      }
  }
  ```

### 六、泛型

- 不可知的类型

- 在使用集合时，如果集合中的元素的最终类型不是同一类型，那么在迭代过程中，如果打算把元素类型转换为最终类型，可能就会发生类型转换的异常。如果这种异常出现，这段代码就缺乏类型的安全性。这是java从1.0开始在集合类型中的一大缺点。到了jdk1.5就出现了泛型，它出现的目的就是为了彻底解决类型不安全的问题。

- 泛型的使用：

  1. 用在类上，class 类名< 泛型符号（T，E）>，在实例化类时，一定要指定具体的类型，在类中凡是用泛型所声明的类型都要替换成这个具体的类型。
  2. 在使用时才确定具体的类型，就保证了对象的类型是确定且明确的，就可以避免类型不安全问题的发生。
  3. 在具体应用中，特别是使用集合类型时，要求结合泛型进行使用。此时能够放入集合中的元素都是同一类型的元素，也就是在实践中，需要放在同一集合中的元素往往都是同类型的元素。
  4. 泛型也可以用在接口上。

  ```java
  package com.zhong.test_3;
  
  import java.util.ArrayList;
  import java.util.Date;
  
  public class GenericDemo {
      public static void main(String[] args) {
          ArrayList list = new ArrayList();
          //集合中的元素都是Object类型，这样集合中可以放置任何类型的元素
          list.add("hello");
          list.add(123);
          list.add(new Date());
          for (Object o : list) {
              //System.out.println(o);
              if(o instanceof String) {
                  String str = (String) o;
                  System.out.println(str.substring(2,4));
              }
              if(o instanceof Integer){
                  int a = (int) o;
                  System.out.println(++a);
              }
          }
          //使用泛型类
          Person<String> person1 = new Person<>("天空");
          Person<Date> person2 = new Person<>(new Date());
          System.out.println(person1);
          System.out.println(person2);
  
          ArrayList<Integer> list1 = new ArrayList<>();
          list1.add(123);
          list1.add(456);
          list1.add(789);
          for (Integer integer : list1) {
              System.out.println(++integer);
          }
      }
  }
  
  
  class Person<T>{
      private T t;
  
      public Person(T t){
          this.t = t;
      }
  
      @Override
      public String toString() {
          return t.toString();
      }
  }
  ```

1. 泛型可以使用在方法上面，使用的前提，如果不确定方法的参数或返回值的类型是什么，此时就可以在方法上使用泛型。

   - 修饰符 <泛型>  返回值类型 方法名（参数列表）{  }

   - ```java
     public <T> T getT(T t){ }
     ```

2. 泛型可以使用在接口上，在接口名称后面带上泛型标志。interface 接口名<泛型>，集合框架中的接口基本上都带有泛型。

3. 泛型通配符 ？，如果我们需要使用某个接口或者类，但是类和接口又带有泛型，使用者在使用时也不知道该用什么类型，此时应该使用泛型通配符来表示类型。

4. 受限制的泛型，泛型是不可知的类型，也就是任何类型都可以作为泛型的实现类型，如果设计者认为类的使用者在使用泛型时，具体的类型应该处于某个范围之内，此时可以使用受限制的泛型。可以使用的关键字是：extends（只能用指定类型的子类，含本类），super（只能用指定类型的父类，含本类）

   - <? extends 具体类型>，<? super 具体类型>

   - ```java
     public static void setColl(Collection<? extends Number> coll){}
     ```

5. 多泛型，在声明类和接口的时候，有时一个泛型不够，此时可以声明多个泛型。<标记1，标记2……>

