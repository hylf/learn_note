### 一、我们要建立面向对象的编程思想

- 相对于面向过程的思想而言的，它认为所有的事物都是对象，对象具有特征和行为，类表示一类（组）对象的共同的特征和行为。

### 二、类与对象

1. 类是对象的抽象与概括。类是抽象的，它的特征没有具体的描述。
2. 对象可以从类派生而来，对象是具体的，具体的特征与行为。

### 三、类与对象的关系

- 类与对象具有不可分的关系，对象的特征与行为被抽象化，就产生了类。类的特征和行为被具体化，就生成了对象。
- 在java中，把从类中派生出对象的做法，称为对象的实例化。

### 四、java中如何来定义类

- 类是程序的基本单位，一个java应用程序是由多个类组成的，类与类之间可以具有相互包括或引用的关系。定义类就是编写java程序最基础的行为。

- 修饰符    class    类名    [ extend    implements ] {

  ​		类的成员变量（用来表示类的特征）

  ​		类的成员方法（用来表示类的行为）

  }

- 注意：凡是能出现在类中的事物，都称为类的成员。除了成员变量和成员方法外，类中还可以有构造方法，静态代码块，静态方法，实例代码块，内部类等。

  ```java
  package test;
  
  public class Person {
      //定义成员变量，他应该出现在方法的外面
      public String name;
      public int age;
      public char gender;
  
      //定义成员方法
      public void eat(){
          System.out.println("好好吃饭！");
  
      }
  
      public String desc(){
          return "我叫"+this.name+"今年"+this.age+"岁，"+"是一位"+(this.gender == '女'?"美女":"帅哥" );
      }
  
      public void learn(int day){
          System.out.println("我一定要认真学习，一周学习" + day + "天");
      }
  }
  ```

### 五、如何使用类

1. 把类实例化为对象，通过对象来调用方法或改变成员变量。

2. 类不需要被实例化，直接通过类来使用方法或变量。


### 六、如何实例化对象

- 通过 new 操作符来实例化对象，类名    变量名  =  new  类名();

1. 对象中的变量成员如果没有赋值，它们各自取默认值。引用类型的变量为null，整型的变量为0，浮点型变量为0.0，字符型为 ‘\u0000’ 布尔型为false。
2. 给对象的成员变量赋值的方法，对象变量.成员变量名 = 值。
3. 调用类的方法，如果方法没有static关键字，该方法才能称为成员方法，成员方法只能通过对象来调用，不能通过类来调用。
4. 类中的成员变量和成员方法属于对象所有，所以使用它们都要通过对象来进行。

```java
package test;

public class Person_Test {
    public static void main(String[] args) {

        //实例化Person类的对象，创建类的对象
        Person person1 = new Person();
        System.out.println(person1.name+" "+person1.age+" "+person1.gender);
        //给成员变量赋值
        person1.name = "赵本山";
        person1.age = 18;
        person1.gender = '男';
        System.out.println(person1.name+" "+person1.age+" "+person1.gender);
        //调用对象的方法
        person1.eat();
        String desc = person1.desc();
        System.out.println(desc);
        person1.learn(6);
    }
}
```

### 七、封装

- 面向对象的语言都具有三大特征：封装，继承，多态。

1. 什么是封装
   - 把成员变量和方法都写在类中，然后给类进行命名，以后要使用成员变量和方法就可以通过类来使用。这是封装的一般性概念。
2. 封装的特定要求
   - 成员变量必须是私有的，访问这些成员变量的方法必须是公共的。这种情况下，在类的外部无法感知成员变量的存在及具体特征，这样对成员变量起到保护性的作用；针对成员变量提供get和set方法，利用这两个方法来操作成员变量，在类的外部只能通过调用set和get方法来操作成员变量，也可以在方法中设定某特定的执行逻辑。符合以上特征才是真正的封装。

```java
package test;

public class Cat {
    private String name;
    private int age;
    private String sound;

    //this也是一个关键字，用在类中，用来表示当前类的对象。
    //每个对象都可以

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }
}
```

```java
package test;

public class Cat_Test {

    public static void main(String[] args) {
        Cat cat1 = new Cat();
        cat1.setName("旺仔");
        cat1.setAge(1);
        cat1.setSound("喵喵");
        System.out.println(cat1.getName()+" "+cat1.getAge()+" "+cat1.getSound());
    }
}
```