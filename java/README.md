### 设计模式的六大原则

###### 1、开闭原则（Open Close Principle）

- 开闭原则就是说对扩展开放，对修改关闭。在程序需要进行拓展的时候，不能去修改原有的代码，实现一个热插拔的效果。所以一句话概括就是：为了使程序的扩展性好，易于维护和升级。想要达到这样的效果，我们需要使用接口和抽象类，后面的具体设计中我们会提到这点。


###### 2、里氏代换原则（Liskov Substitution Principle）

- 里氏代换原则（Liskov Substitution Principle LSP）面向对象设计的基本原则之一。 里氏代换原则中说，任何基类可以出现的地方，子类一定可以出现。 LSP是继承复用的基石，只有当衍生类可以替换掉基类，软件单位的功能不受到影响时，基类才能真正被复用，而衍生类也能够在基类的基础上增加新的行为。里氏代换原则是对“开-闭”原则的补充。实现“开-闭”原则的关键步骤就是抽象化。而基类与子类的继承关系就是抽象化的具体实现，所以里氏代换原则是对实现抽象化的具体步骤的规范。


###### 3、依赖倒转原则（Dependence Inversion Principle）

- 这个是开闭原则的基础，具体内容：真对接口编程，依赖于抽象而不依赖于具体。


###### 4、接口隔离原则（Interface Segregation Principle）

- 这个原则的意思是：使用多个隔离的接口，比使用单个接口要好。还是一个降低类之间的耦合度的意思，从这儿我们看出，其实设计模式就是一个软件的设计思想，从大型软件架构出发，为了升级和维护方便。所以上文中多次出现：降低依赖，降低耦合。


###### 5、迪米特法则（最少知道原则）（Demeter Principle）

- 为什么叫最少知道原则，就是说：一个实体应当尽量少的与其他实体之间发生相互作用，使得系统功能模块相对独立。


###### 6、合成复用原则（Composite Reuse Principle）

- 原则是尽量使用合成/聚合的方式，而不是使用继承。

### 设计模式的分类

1. 结构型：共七种：适配器模式、装饰器模式、代理模式、外观模式、桥接模式、组合模式、享元模式。
2. 创建型：共五种：工厂方法模式、抽象工厂模式、单例模式、建造者模式、原型模式。
3. 行为型：共十一种：策略模式、模板方法模式、观察者模式、迭代子模式、责任链模式、命令模式、备忘录模式、状态模式、访问者模式、中介者模式、解释器模式。

![](E:\JAVA培训\笔记\设计模式.jpg)

### 一、装饰模式（Decorator，结构型）

1. 作用：给一个对象动态的添加额外的功能，要求装饰对象和被装饰对象实现同一个接口，装饰对象持有被装饰对象的实例。
2. 继承可以让类的功能得到扩展，在有些情况下使用继承可能会让子类迅速膨胀，会让程序变得臃肿。
3. 继承是一种静态的设计方式，增强的功能在程序运行之前就必须要完成。
4. 使用装饰器模式可以很好的解决以上继承中的问题。它采用动态的方法也就是程序运行期进行功能的扩展。
5. 装饰器模式的实现，对于设计来讲是有明确的规范，设计者必须按照此模式的具体要求去编写程序。

- ```java
  public interface Sourceable {
  	public void method();
  }
  ```

- ```java
  public class Source implements Sourceable {
   
  	@Override
  	public void method() {
  		System.out.println("the original method!");
  	}
  }
  ```

- ```java
  public class Decorator implements Sourceable {
   
  	private Sourceable source;
  	
  	public Decorator(Sourceable source){
  		super();
  		this.source = source;
  	}
  	@Override
  	public void method() {
  		System.out.println("before decorator!");
  		source.method();
  		System.out.println("after decorator!");
  	}
  }
  ```

- ```java
  public class DecoratorTest {
   
  	public static void main(String[] args) {
  		Sourceable source = new Source();
  		Sourceable obj = new Decorator(source);
  		obj.method();
  	}
  }
  ```

### 二、单例模式（Singleton，创建型）

1. 单例模式要求某个类的实例在整个应用中只能有一个，实例只能由本类来创建，实例在多线程情况下必须是线程安全的。
2. 在应用中，如果需要某个类的实例只能有一个，比如整个应用都要对同一 个文件进行读写，多个线程要操作同一个变量，多个线程要使用同一个连接对象来操作数据库。
3. spring 中的 Bean 默认都是单例的。
4. 单例可以节约资源，一是对象的创建只有一 次，节省内存，节省创建和销毁的时间。
5. 单例模式的设计原则，必须在类中具有三个要素：
   - 需要一个静态的属于本类的成员变量。
   - 构造方法必须是私有的，保证只能在内部创建对象。
   - 需要一个静态的方法，创建对象并返回对象。保证在没有对象的前提下，可以创建唯一对象并返回对象。

- 设计形式：

  1. 懒汉型，包括线程安全与非线程安全（加锁，用 synchronized 或可重入锁）只建议单线程情况下使用，不加锁。（使用时才创建）

     ```java
     package com.zhong.test_6;
     
     public class Lazy {
         private static Lazy lazy;//懒汉型
         private Lazy() {}
         public static Lazy getInstance() {
             if (lazy == null)
                 lazy = new Lazy();
             return lazy;
         }
     }
     ```

  2. 饿汉型，是线程安全的。初始化时就创建对象。（可能存在浪费资源问题）

     ```java
     package com.zhong.test_6;
     
     public class Hungry {
         private static Lazy lazy = new Lazy();//饿汉型
         private Lazy() {}
         public static Lazy getInstance() {
             return lazy;
         }
     }
     ```

  3. 双检锁型，主要用在多线程并发的情况下，并且效率很高。也不会出现资源浪费情况。多线程下最建议使用的。

     ```java
     package com.zhong.test_6;
     
     public class DoubleCheckLock {
         private static DoubleCheckLock obj;
         private DoubleCheckLock() {}
         public static DoubleCheckLock getInstance() {
             if (obj == null) synchronized (DoubleCheckLock.class) {
                 if (obj == null) {
                     obj = new DoubleCheckLock();
                 }
             }
             return obj;
         }
     }
     ```

### 三、工厂模式（Factory，创建型）

1. java 中的工厂设计模式，是创建对象的最佳方式。它属于创建型模式。
2. 好处在于，可以隐藏创建对象的逻辑，对象的使用只需要告诉对方我要什么对象就可以了。
3. 工厂模式主要应用于对象的创建等比较繁琐的情形。
4. 使用工厂模式，必须返回接口类型。

- ```java
  public interface Sender {
  	public void Send();
  }
  ```

- ```java
  public class MailSender implements Sender {
  	@Override
  	public void Send() {
  		System.out.println("this is mailsender!");
  	}
  }
  ```

- ```java
  public class SmsSender implements Sender {
   
  	@Override
  	public void Send() {
  		System.out.println("this is sms sender!");
  	}
  }
  ```

- ```java
  public class SendMailFactory implements Provider {
  	
  	@Override
  	public Sender produce(){
  		return new MailSender();
  	}
  }
  ```

- ```java
  public class SendSmsFactory implements Provider{
   
  	@Override
  	public Sender produce() {
  		return new SmsSender();
  	}
  }
  ```

- ```java
  public interface Provider {
  	public Sender produce();
  }
  ```

- ```java
  public class Test {
   
  	public static void main(String[] args) {
  		Provider provider = new SendMailFactory();
  		Sender sender = provider.produce();
  		sender.Send();
  	}
  }
  ```

### 四、享元模式（Flyweight，结构型）

- 享元模式的主要目的是实现对象的共享，即共享池，当系统中对象多的时候可以减少内存的开销，通常与工厂模式一起使用。

- ```java
  package com.zhong.test_9;
  
  public interface Shape {
      void draw();
  }
  ```

- ```java
  package com.zhong.test_9;
  
  public class Circle implements Shape {
      private String color;
      private int x, y, radius;
  
      public Circle(String color) {
          this.color = color;
      }
  
      public String getColor() {
          return color;
      }
  
      public void setColor(String color) {
          this.color = color;
      }
  
      public int getX() {
          return x;
      }
  
      public void setX(int x) {
          this.x = x;
      }
  
      public int getY() {
          return y;
      }
  
      public void setY(int y) {
          this.y = y;
      }
  
      public int getRadius() {
          return radius;
      }
  
      public void setRadius(int radius) {
          this.radius = radius;
      }
  
      @Override
      public void draw() {
          System.out.println("颜色：" + color + "，位置：" + x + "," + y + "，半径：" + radius);
      }
  }
  ```

- ```java
  package com.zhong.test_9;
  
  import java.util.HashMap;
  import java.util.Map;
  import java.util.Random;
  
  public class CircleFlyWeight {
      private static final Map<String, Circle> map = new HashMap<>();
  
      public static Circle getCircle(String color) {
          Circle circle = map.get(color);
          if (circle == null) {
              circle = new Circle(color);
              map.put(color, circle);
          }
          return circle;
      }
  }
  ```

- ```java
  package com.zhong.test_9;
  
  import java.util.Random;
  
  public class CircleDemo {
      public static void main(String[] args) {
          String[] colors = {"red", "green", "blue", "yellow", "pink"};
          for (int i = 0; i < 100; i++) {
              Circle circle = CircleFlyWeight.getCircle(colors[getColor(colors.length)]);
              circle.setX(getX());
              circle.setY(getY());
              System.out.print(i + "：");
              circle.draw();
          }
      }
  
      public static int getColor(int len) {
          return new Random().nextInt(len);
      }
  
      public static int getX() {
          return new Random().nextInt(100);
      }
  
      public static int getY() {
          return new Random().nextInt(100);
      }
      
      public static int getRadius() {
          return new Random().nextInt(10 - 5 + 1) + 5; //控制半径是[5-10]，[a,b]->(b - a + 1) + 5，[a,b)->(b - a) + a
      }
     
  
  }
  ```

- ```java
  Circle circle1 = CircleFlyWeight.getCircle("red");
  circle1.setX(getX());
  circle1.setY(getY());
  circle1.setRadius(getRadius());
  circle1.draw();
  
  Circle circle2 = CircleFlyWeight.getCircle("red");
  circle2.setX(getX());
  circle2.setY(getY());
  circle2.setRadius(getRadius());
  circle2.draw();
  System.out.println(circle1 == circle2);//true
  ```

### 五、策略模式（Strategy，行为型模式）

1. 在程序的执行过程中，动态的决定使用什么操作。这种设计模式就是策略模式，它是23种模式中使用最多的模式之一。
2. 程序的设计者在设计时并不知道程序执行后会具体的采用哪种操作，因此，对于需要执行的操作在设计时是抽象的，只有在运行时才能把抽象的规定进行具体化。此种模式要求把每一个具体的操作用一个类来表示，程序执行中，需要哪个操作就去使用哪个类。它可以替换掉 if else 语句复杂的逻辑。
3. 策略模式的决定权在用户，系统本身提供不同算法的实现，新增或者删除算法，对各种算法做封装。因此，策略模式多用在算法决策系统中，外部用户只需要决定用哪个算法即可。

- ```java
  public interface ICalculator {
  	public int calculate(String exp);
  }
  ```

- ```java
  public abstract class AbstractCalculator {
  	
  	public int[] split(String exp,String opt){
  		String array[] = exp.split(opt);
  		int arrayInt[] = new int[2];
  		arrayInt[0] = Integer.parseInt(array[0]);
  		arrayInt[1] = Integer.parseInt(array[1]);
  		return arrayInt;
  	}
  }
  ```

  ```java
  public class Plus extends AbstractCalculator implements ICalculator {
   
  	@Override
  	public int calculate(String exp) {
  		int arrayInt[] = split(exp,"\\+");
  		return arrayInt[0]+arrayInt[1];
  	}
  }
  ```

- ```java
  public class Minus extends AbstractCalculator implements ICalculator {
   
  	@Override
  	public int calculate(String exp) {
  		int arrayInt[] = split(exp,"-");
  		return arrayInt[0]-arrayInt[1];
  	}
   
  }
  ```

- ```java
  public class StrategyTest {
   
  	public static void main(String[] args) {
  		String exp = "2+8";
  		ICalculator cal = new Plus();
  		int result = cal.calculate(exp);
  		System.out.println(result);
  	}
  }
  ```

### 六、建造者模式（Builder，创建型）

- 建造者模式主要用于在创建复杂对象时的一种模式。

- ```java
  package com.zhong.test_10;
  
  public interface Item {//产品的模型
      String name();//名字
  
      Packing packing();//采用的包装
  
      double price();//价格
  }
  ```

  ```java
  package com.zhong.test_10;
  
  public interface Packing {//包装
      String pack();
  }
  ```

  ```java
  package com.zhong.test_10;
  
  public class PaperBox implements Packing {
      @Override
      public String pack() {
          return "纸盒";
      }
  }
  ```

  ```java
  package com.zhong.test_10;
  
  public class BottleBox implements Packing {
      @Override
      public String pack() {
          return "瓶子";
      }
  }
  ```

  ```java
  package com.zhong.test_10;
  
  public abstract class Rice implements Item {//饭食，为抽象类
      @Override
      public Packing packing() {
          return new PaperBox();
      }
  }
  ```

  ```java
  package com.zhong.test_10;
  
  public class EggRice extends Rice {
      @Override
      public String name() {
          return "蛋炒饭";
      }
  
      @Override
      public double price() {
          return 12;
      }
  }
  ```

  ```java
  package com.zhong.test_10;
  
  public class FishRice extends Rice {
      @Override
      public String name() {
          return "鱼焖饭";
      }
  
      @Override
      public double price() {
          return 15;
      }
  }
  ```

  ```java
  package com.zhong.test_10;
  
  public abstract class Drink implements Item {//饮料同理
      @Override
      public Packing packing() {
          return new BottleBox();
      }
  }
  ```

  ```java
  package com.zhong.test_10;
  
  public class TeaDrink extends Drink {
      @Override
      public String name() {
          return "茶";
      }
  
      @Override
      public double price() {
          return 9;
      }
  }
  ```

  ```java
  package com.zhong.test_10;
  
  public class CoffeeDrink extends Drink {
      @Override
      public String name() {
          return "咖啡";
      }
  
      @Override
      public double price() {
          return 15;
      }
  }
  ```

  ```java
  package com.zhong.test_10;
  
  import java.util.ArrayList;
  
  public class Meal {//套餐的模型
      private ArrayList<Item> items = new ArrayList<>();
  
      public Meal addItem(Item item) {//明显体现出建造者模式特征，为链式方法
          items.add(item);
          return this;
      }
  
      public void showItems() {
          double totalCost = 0.0;
          for (Item item : items) {
              System.out.println("名称：" + item.name() + "，包装：" + item.packing().pack() + "，价格：" + item.price());
              totalCost += item.price();
          }
          System.out.println("总价：" + totalCost);
      }
  }
  ```

  ```java
  package com.zhong.test_10;
  
  public class MealDirector {//管理者
      public Meal buildA() {
          Meal meal = new Meal();
          meal.addItem(new EggRice()).addItem(new TeaDrink());
          return meal;
      }
  
      public Meal buildB() {
          Meal meal = new Meal();
          meal.addItem(new EggRice()).addItem(new CoffeeDrink());
          return meal;
      }
  
      public Meal buildC() {
          Meal meal = new Meal();
          meal.addItem(new FishRice()).addItem(new TeaDrink());
          return meal;
      }
  
      public Meal buildD() {
          Meal meal = new Meal();
          meal.addItem(new FishRice()).addItem(new CoffeeDrink());
          return meal;
      }
  
      public static void main(String[] args) {
          MealDirector mealDirector = new MealDirector();
          Meal mealA = mealDirector.buildA();
          Meal mealB = mealDirector.buildB();
          Meal mealC = mealDirector.buildC();
          Meal mealD = mealDirector.buildD();
          mealA.showItems();
          mealB.showItems();
          mealC.showIt        mealD.showItems();
  
      }
  }
  ```

### 七、桥接模式（Bridge，结构型）

1. 在抽象化和实现化之间实现脱耦。抽象化就是把对象的实现采用抽象类的方式，实现化把对象的实现采用接口。脱耦就是让抽象化体系与实现化体系不要耦合在一起。
2. 对象功能的扩展一般都使用继承，在有些情况下会造成子类数量增长的非常快，也就是为了扩展类的功能，可能会写很多类。
3. 把抽象类的体系剥离出来，让实现类去实现接口，让抽象类去组合实现类。

- ```java
  package com.zhong.test_11;
  
  public interface TypePC {
      void sale();
  }
  ```

  ```java
  package com.zhong.test_11;
  
  public class DeskTopTypePC implements TypePC {
      @Override
      public void sale() {
          System.out.println("桌面电脑");
      }
  }
  ```

  ```java
  package com.zhong.test_11;
  
  public class NotBookTypePC implements TypePC {
      @Override
      public void sale() {
          System.out.println("笔记本电脑");
      }
  }
  ```

  ```java
  package com.zhong.test_11;
  
  public class PadTypePC implements TypePC {
      @Override
      public void sale() {
          System.out.println("Pad电脑");
      }
  }
  ```

  ```java
  package com.zhong.test_11;
  
  public abstract class Brand {
      protected TypePC typePC;
  
      public Brand(TypePC typePC) {
          this.typePC = typePC;
      }
  
      public abstract void sale();
  }
  ```

  ```java
  package com.zhong.test_11;
  
  public class AcerBrand extends Brand {
  
      public AcerBrand(TypePC typePC) {
          super(typePC);
      }
  
      @Override
      public void sale() {
          System.out.print("宏基");
          typePC.sale();
      }
  }
  ```

  ```java
  package com.zhong.test_11;
  
  public class HuaWeiBrand extends Brand {
  
      public HuaWeiBrand(TypePC typePC) {
          super(typePC);
      }
  
      @Override
      public void sale() {
          System.out.print("华为");
          typePC.sale();
      }
  }
  ```

  ```java
  package com.zhong.test_11;
  
  public class LenoveBrand extends Brand {
  
      public LenoveBrand(TypePC typePC) {
          super(typePC);
      }
  
      @Override
      public void sale() {
          System.out.print("联想");
          typePC.sale();
      }
  }
  ```

  ```java
  package com.zhong.test_11;
  
  public class BridgeDemo {
      public static void main(String[] args) {
          TypePC typePC1 = new DeskTopTypePC();
          TypePC typePC2 = new PadTypePC();
          TypePC typePC3 = new NotBookTypePC();
  
          Brand brand1 = new LenoveBrand(typePC1);
          Brand brand2 = new AcerBrand(typePC2);
          Brand brand3 = new HuaWeiBrand(typePC3);
  
          brand1.sale();
          brand2.sale();
          brand3.sale();
  
      }
  }
  ```

### 八、模板模式（Template，行为型）

1. 某些算法有确定的步骤，还有可能其中的某些步骤是完全一样的，这样可用抽象类把算法的具体步骤固定，把具有相同代码的步骤具体化，从而形成算法的骨架，再把其中抽象的方法放到子类中实现，因此完整功能的实现由抽象类与子类共同完成，这样在子类中实现的方法是一种延迟的实现。
2. 好处在于明确了执行步骤，公共的代码实现了共享，符合五大设计原则（SOLID）中的第二大原则 “开放闭合原则” ，也就是对扩展开放，对修改关闭。 
3. 缺点在于子类的数量不受控制。

- ```java
  
  public abstract class AbstractCalculator {
  	
  	/*主方法，实现对本类其它方法的调用*/
  	public final int calculate(String exp,String opt){
  		int array[] = split(exp,opt);
  		return calculate(array[0],array[1]);
  	}
  	
  	/*被子类重写的方法*/
  	abstract public int calculate(int num1,int num2);
  	
  	public int[] split(String exp,String opt){
  		String array[] = exp.split(opt);
  		int arrayInt[] = new int[2];
  		arrayInt[0] = Integer.parseInt(array[0]);
  		arrayInt[1] = Integer.parseInt(array[1]);
  		return arrayInt;
  	}
  }
  ```

- ```java
  public class Plus extends AbstractCalculator {
   
  	@Override
  	public int calculate(int num1,int num2) {
  		return num1 + num2;
  	}
  }
  ```

- ```java
  public class StrategyTest {
   
  	public static void main(String[] args) {
  		String exp = "8+8";
  		AbstractCalculator cal = new Plus();
  		int result = cal.calculate(exp, "\\+");
  		System.out.println(result);
  	}
  }
  ```

### 九、观察者模式（Observer，行为型）

1. 它属于行为型模式。

2. 是一个非常常用的模式，在 jdk 中就提供了实现这种模式的 API。

   - public interface Observer；实现该接口的类就有观察某件事情是否发生的能力。只有一个 void update(Observer o，Object arg)，当观察的对象的状态发生变化时，就会执行该方法。
   - public class Observable；addObserver(Observer o) 添加观察者；notifyObservers() 方法当本对象的状态发生变化时需要调用的方法，该类就是被观察的对象。

   ```java
   package com.zhong.test_13;
   
   /**
    * @author 华韵流风
    * @ClassName Observer
    * @Description TODO
    * @Date 2021/4/28 19:24
    * @packageName com.zhong.test_13
    */
   public interface Observer {
       /**
        * 被观察对象发生变化时做的事
        *
        * @param course course
        */
       void update(String course);
   }
   ```

   ```java
   package com.zhong.test_13;
   
   /**
    * @author 华韵流风
    * @ClassName Teacher
    * @Description TODO
    * @Date 2021/4/28 19:25
    * @packageName com.zhong.test_13
    */
   public class Teacher implements Observer {
       @Override
       public void update(String course) {
           if ("java".equals(course)) {
               System.out.println("java课");
           }
           if ("mysql".equals(course)) {
               System.out.println("mysql课");
           }
       }
   }
   ```

   ```java
   package com.zhong.test_13;
   
   /**
    * @author 华韵流风
    * @ClassName Student
    * @Description TODO
    * @Date 2021/4/28 19:27
    * @packageName com.zhong.test_13
    */
   public class Student implements Observer {
       @Override
       public void update(String course) {
           if ("java".equals(course)) {
               System.out.println("java语法");
           }
           if ("mysql".equals(course)) {
               System.out.println("mysql语法");
           }
       }
   }
   ```

   ```java
   package com.zhong.test_13;
   
   import java.util.ArrayList;
   import java.util.List;
   
   /**
    * @author 华韵流风
    * @ClassName Course
    * @Description TODO
    * @Date 2021/4/28 19:28
    * @packageName com.zhong.test_13
    */
   public class Course {
       private String course;
       private final List<Observer> observers = new ArrayList<>();
   
       public void setCourse(String course) {
           this.course = course;//改变状态
           this.notifyObservers();//通知观察者状态已发生改变
       }
   
       public Course addObserver(Observer observer) {
           observers.add(observer);
           return this;
       }
   
       public void notifyObservers() {
           for (Observer observer : observers) {
               observer.update(course);
           }
       }
   }
   ```
```java
	package com.zhong.test_13;
   
   /**
    * @author 华韵流风
    * @ClassName ObserverDemo
    * @Description TODO
    * @Date 2021/4/28 19:31
    * @packageName com.zhong.test_13
    */
   public class ObserverDemo {
       public static void main(String[] args) {
           Course course = new Course();
           Teacher teacher = new Teacher();
           Student student = new Student();
           course.addObserver(teacher).addObserver(student);
   
   
           //工作过程，改变被观察者的状态。
           course.setCourse("java");
           course.setCourse("mysql");
   
       }
   
   }
```

### 十、命令模式（command，行为型）

1. 行为型模式

2. 在不同的类之间可能存在通过接口来传递命令和参数。如果接口方法的参数比较多，执行命令的流程比较复杂，此时就可以把要传递的内容用一个对象封装起来，把该对象交给命令的执行者，

3. 好处在于可以把命令的请求者与命令接收者通过命令对象实现完全的脱耦。

4. 命令模式需要包含的几个部分

   - client 客户，创建命令的请求者及命令对象；
   - invoke，命令请求者，它包括命令要完成的任务；
   - command，命令对象，它要包括命令请求者及需要完成的任务；
   - receive，命令的接收者，它负责接收并执行命令。

5. 命令模式的实现模型：

   - ```java
     package com.zhong.test_14;
     
     /**
      * //                       .::::.
      * //                     .::::::::.
      * //                    :::::::::::
      * //                 ..:::::::::::'
      * //              '::::::::::::'
      * //                .::::::::::
      * //           '::::::::::::::..
      * //                ..::::::::::::.
      * //              ``::::::::::::::::
      * //               ::::``:::::::::'        .:::.
      * //              ::::'   ':::::'       .::::::::.
      * //            .::::'      ::::     .:::::::'::::.
      * //           .:::'       :::::  .:::::::::' ':::::.
      * //          .::'        :::::.:::::::::'      ':::::.
      * //         .::'         ::::::::::::::'         ``::::.
      * //     ...:::           ::::::::::::'              ``::.
      * //    ```` ':.          ':::::::::'                  ::::..
      * //                       '.:::::'                    ':'````..
      *
      * @author 华韵流风
      * @ClassName Stock
      * @Description TODO
      * @Date 2021/4/28 20:15
      * @packageName com.zhong.test_14
      */
     public class Stock {
         private String name = "大米";
         private int quantity = 10;
     
         public void buy() {
             System.out.println("Stock[" + name + "，quantity：" + quantity + "]bought");
         }
         
         public void sell() {
             System.out.println("Stock[" + name + "，quantity：" + quantity + "]sell");
     
         }
     }
     ```

   - ```java
     package com.zhong.test_14;
     
     /**
      * @author 华韵流风
      * @ClassName Order
      * @Description TODO
      * @Date 2021/4/28 20:18
      * @packageName com.zhong.test_14
      */
     public interface Order {
         public void execute();
     }
     ```

   - ```java
     package com.zhong.test_14;
     
     /*
      * @author 华韵流风
      * @ClassName Buy
      * @Description TODO
      * @Date 2021/4/28 20:18
      * @packageName com.zhong.test_14
      */
     public class Buy implements Order {
         private Stock stock;
     
         public Buy(Stock stock) {
             this.stock = stock;
         }
     
         @Override
         public void execute() {
             stock.buy();
         }
     }
     ```

   - ```java
     package com.zhong.test_14;
     
     /**
      * @author 华韵流风
      * @ClassName Sell
      * @Description TODO
      * @Date 2021/4/28 20:19
      * @packageName com.zhong.test_14
      */
     public class Sell implements Order {
         private Stock stock;
     
         public Sell(Stock stock) {
             this.stock = stock;
         }
     
         @Override
         public void execute() {
             stock.sell();
         }
     }
     ```

   - ```java
     package com.zhong.test_14;
     
     import java.util.ArrayList;
     import java.util.List;
     
     /**
      * @author 华韵流风
      * @ClassName Broker
      * @Description TODO
      * @Date 2021/4/28 20:19
      * @packageName com.zhong.test_14
      */
     public class Broker {
         private final List<Order> orders = new ArrayList<>();
     
         public Broker takeOrder(Order order) {
             orders.add(order);
             return this;
         }
     
         public void placeOrder() {
             for (Order order : orders) {
                 order.execute();
             }
             orders.clear();
         }
     }
     ```

   - ```java
     package com.zhong.test_14;
     
     /**
      * @author 华韵流风
      * @ClassName Client
      * @Description TODO
      * @Date 2021/4/28 20:21
      * @packageName com.zhong.test_14
      */
     public class Client {
         public static void main(String[] args) {
             Stock stock = new Stock();
             Order buy = new Buy(stock);
             Order sell = new Sell(stock);
     
             Broker broker = new Broker();
     
             //工作过程
             broker.takeOrder(buy).takeOrder(sell);
             broker.placeOrder();
         }
     }
     ```

