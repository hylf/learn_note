### 一、什么是 AOP

- 面向切面编程，这也是一种编程思想，它是建立在面向对象编程（OOP）的思想之上。
- 面向对象编程采用的思路是自顶向下的思路，所有的类和接口都处于某种继承或实现的关系之上。

### 二、AOP 的实现原理

1. 理解代理设计模式（结构型）

2. 代理模式的基本实现：

   - 静态代理：创建代理类，程序执行时，执行代理类，从而相当于执行了目标类。

     - ```java
       package com.zhong.calculator;
       
       /**
        * @author 华韵流风
        * @ClassName Calculator
        * @Description TODO
        * @Date 2021/6/13 16:31
        * @packageName com.zhong.calculator
        */
       public interface Calculator {
           /**
            * 加法
            *
            * @param a a
            * @param b b
            * @return int
            */
           int add(int a,int b);
       
           /**
            * 减法
            *
            * @param a a
            * @param b b
            * @return int
            */
           int sub(int a,int b);
       
           /**
            * 除法法
            *
            * @param a a
            * @param b b
            * @return int
            */
           int mov(int a,int b);
       
           /**
            * 乘法
            *
            * @param a a
            * @param b b
            * @return int
            */
           int mul(int a,int b);
       }
       ```

     - ```java
       package com.zhong.calculator;
       
       /**
        * @author 华韵流风
        * @ClassName CalculatorImpl
        * @Description TODO
        * @Date 2021/6/13 16:33
        * @packageName com.zhong.calculator
        */
       public class CalculatorImpl implements Calculator {
           public int add(int a, int b) {
               return a + b;
           }
       
           public int sub(int a, int b) {
               return a - b;
           }
       
           public int mov(int a, int b) {
               return a / b;
           }
       
           public int mul(int a, int b) {
               return a * b;
           }
       }
       ```

     - ```java
       package com.zhong.calculator;
       
       /**
        * @author 华韵流风
        * @ClassName CalculatorProxy
        * @Description TODO
        * @Date 2021/6/13 16:42
        * @packageName com.zhong.calculator
        */
       public class CalculatorProxy implements Calculator{
       
           /**
            * 定义代理的类型
            */
           private final Calculator calculator;
       
           public CalculatorProxy(Calculator calculator) {
               this.calculator = calculator;
           }
       
           public int add(int a, int b) {
               int c = calculator.add(a,b);
               try {
                   System.out.println("方法 add 参数" + a + "，" + b);
                   return c;
               }finally {
                   System.out.println("方法 add 结果" + c);
               }
       
           }
       
           public int sub(int a, int b) {
               int c = calculator.sub(a,b);
               try {
                   System.out.println("方法 sub 参数" + a + "，" + b);
                   return c;
               }finally {
                   System.out.println("方法 sub 结果" + c);
               }
           }
       
           public int mov(int a, int b) {
               int c = calculator.mov(a,b);
               try {
                   System.out.println("方法 mov 参数" + a + "，" + b);
                   return c;
               }finally {
                   System.out.println("方法 mov 结果" + c);
               }
           }
       
           public int mul(int a, int b) {
               int c = calculator.mul(a,b);
               try {
                   System.out.println("方法 mul 参数" + a + "，" + b);
                   return c;
               }finally {
                   System.out.println("方法 mul 结果" + c);
               }
           }
       }
       ```

     - ```java
       package com.zhong.calculator;
       
       /**
        * @author 华韵流风
        * @ClassName CalculatorDemo
        * @Description TODO
        * @Date 2021/6/13 16:35
        * @packageName com.zhong.calculator
        */
       public class CalculatorDemo {
           public static void main(String[] args) {
               Calculator calculator = new CalculatorImpl();
               CalculatorProxy calculatorProxy = new CalculatorProxy(calculator);
               System.out.println(calculatorProxy.add(1,2));
           }
       }
       ```

   - 采用 jdk 的代理技术（java 建议）：

     - java.lang.reflect.Proxy 提供了创建动态（程序运行时）代理类和实例的静态方法，它也是由这些方法创建（运行时创建）的所有动态代理类的超类。

     - public interface InvocationHandler，是由代理对象的调用处理程序实现的接口，每个代理对象都有一个关联的调用处理程序。当在代理对象上调用方法时，会调用其调用处理程序的 invoke 方法。

       - Object invoke(Object proxy，Method method，Object[ ] args)

     - 实现：

       - 创建动态代理工厂类，提供一个工厂方法，返回代理类对象。

       - ```java
         package com.zhong.calculator;
         
         import java.lang.reflect.InvocationHandler;
         import java.lang.reflect.Method;
         import java.lang.reflect.Proxy;
         import java.util.Arrays;
         
         /**
          * 创建计算器的代理对象
          *
          * @author 华韵流风
          * @ClassName CalculatorProxyFactory
          * @Description TODO
          * @Date 2021/6/13 17:02
          * @packageName com.zhong.calculator
          */
         public class CalculatorProxyFactory {
         
             /**
              * 目标对象
              */
             private final Calculator calculator;
         
             public CalculatorProxyFactory(Calculator calculator) {
                 this.calculator = calculator;
             }
         
             public  Object buildProxy() {
                 //创建代理对象并返回
                 //声明代理对象的调用程序对象
                 InvocationHandler invocationHandler;
                 invocationHandler = new InvocationHandler() {
                     /**
                      * 本方法就是目标对象的调用处理程序
                      *
                      * @param proxy 当前的代理对象
                      * @param method 目标对象的方法，也就是在接口中定义的方法
                      * @param args 目标对象方法的参数
                      * @return Object
                      * @throws Throwable Throwable
                      */
                     public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                         //打印前日志
                         System.out.println("方法 " + method.getName() + " 参数=" + Arrays.asList(args));
                         //执行目标对象的方法
                         Object result = method.invoke(calculator, args);
                         //打印后日志
                         System.out.println("方法 " + method.getName() + " 结果=" + result);
                         //返回目标方法的执行结果
                         return result;
                     }
                 };
                 //使用Proxy创建目标对象的代理对象
         
                 return Proxy.newProxyInstance(calculator.getClass().getClassLoader(), new Class[]{Calculator.class}, invocationHandler);
             }
         }
         ```

       - 应用，创建目标对象；通过工厂得到代理对象（与目标对象有相同的接口）；通过代理对象调用目标对象的方法，也就是通过执行调用程序，把日志进行输出，调用目标对象的方法得到结果，再返回结果。com.sun.proxy.\$Proxy0 表示 jdk 的代理对象。

       - ```java
         package com.zhong.calculator;
         
         /**
          * @author 华韵流风
          * @ClassName CalculatorDemo
          * @Description TODO
          * @Date 2021/6/13 16:35
          * @packageName com.zhong.calculator
          */
         public class CalculatorDemo {
             public static void main(String[] args) {
                 //创建目标对象
                 Calculator calculator = new CalculatorImpl();
                 CalculatorProxyFactory factory = new CalculatorProxyFactory(calculator);
                 Calculator proxy = (Calculator) factory.buildProxy();
                 //通过代理对象来实现计算器的功能
                 System.out.println(proxy.add(1,2));
                 System.out.println(proxy.sub(1,2));
                 System.out.println(proxy.mul(1,2));
                 System.out.println(proxy.mov(10,2));
         
             }
         }
         ```

   - 采用 Cglib 的代理技术

     - 它随着 spring 框架一起提供，底层的实现是基于 ASM 字节码框架的技术来完成的，它比 jdk 的代理更好用，不要求目标对象实现接口，它的代理对象就是目标对象类型的子类。

     - ```java
       package com.zhong.calculator;
       
       import org.springframework.cglib.proxy.Enhancer;
       import org.springframework.cglib.proxy.MethodInterceptor;
       import org.springframework.cglib.proxy.MethodProxy;
       
       import java.lang.reflect.Method;
       import java.util.Arrays;
       
       /**
        * @author 华韵流风
        * @ClassName CalculatorCglibProxy
        * @Description TODO
        * @Date 2021/6/13 17:40
        * @packageName com.zhong.calculator
        */
       public class CalculatorCglibProxyFactory {
       
           public static CalculatorImpl getProxy(){
               //得到字节码增强器对象
               Enhancer enhancer = new Enhancer();
               //设置目标对象的类型
               enhancer.setSuperclass(CalculatorImpl.class);
               //设置代理对象的回调函数，传递方法拦截器对象，intercept是代理对象的调用程序
               enhancer.setCallback(new MethodInterceptor() {
                   public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                       System.out.println(methodProxy.getSignature().getName()+" "+ Arrays.asList(objects));
                       Object result = methodProxy.invokeSuper(o, objects);
                       System.out.println(methodProxy.getSignature().getName()+" 结果="+result);
                       return result;
                   }
               });
       
               return (CalculatorImpl) enhancer.create();
           }
       
       }
       ```
   
     - ```java
       package com.zhong.calculator;
       
       /**
        * @author 华韵流风
        * @ClassName CalculatorDemo
        * @Description TODO
        * @Date 2021/6/13 16:35
        * @packageName com.zhong.calculator
        */
       public class CalculatorDemo {
           public static void main(String[] args) {
               CalculatorImpl calculator = CalculatorCglibProxyFactory.getProxy();
               System.out.println(calculator.add(1,2));
           }
       }
       ```
   
   - 总结：重点是理解两种代理技术的实现步骤及原理，有利于理解各种框架是如何工作的。

### 三、基于 spring 的 AOP，利用 AspectJ 模板实现 AOP 的功能

1. spring 框架本身提供了 aop 的功能实现，但是它并不是最好的实现方式。
2. AOP 是一个设计思想，spring 框架本身就带有 aop 的实现规范及具体实现。
3. AspectJ 是一套社区软件，它是当前最优秀的 aop 实现框架。
4. 在使用 aop 时，需要新增的依赖包括两个部分，一个是 aop 联盟，另一个就是 spring 整合 AspectJ。
5. 在配置文件中添加 aop 的 scheme，再添加 \<aop:aspectj-autoproxy/> 配置项，表示项目需要使用 apsectj 的代理功能（aop 的实现）。
6. 利用 AspectJ 的 AOP 功能，实现计算器的日志功能的应用。
   - 切面：是一个类，该类中的方法可以织入到目标对象的方法中。
     - 创建切面：在类上添加 @Component 保证切面被容器管理，再添加 @Aspect 注解，表示该类作为切面使用。
   - 通知：切面中的方法，通过可以被织入到目标对象的方法中。
   - 目标对象：就是需要被代理的对象（需要增强功能）
   - 代理对象：它是动态生成的，其中包括了对象及通知。
   - 连接点：目标对象的方法中的某个位置，比如方法执行前，执行后，抛异常，返回结果等。这是一个抽象的概念，并没有一个具体的可明确表达的位置。它对应一个连接点的注解。
   - 切入点：抽象的概念，表示目标对象中方法所在的位置，它对应一个切入点表达式。
7. 创建前置通知
   - 前置通知就是在目标方法执行前的位置（连接点）所执行的方法。
   - 注解：@Before("execution(* zhong.calculator.CalculatorImpl.add(int,int))") 属性是切入点表达式，它指定哪个目标对象的什么方法会被织入前置通知。
   - 方法的参数必须是 JoinPoint 接口类型。
   - 在方法中书写需要执行的代码，与目标对象有关的一些参数可以通过 JoinPoint 来获取。
8. 创建后置通知
   - 在目标方法执行后及返回结果生成前的位置
   - 注解：@After("execution(* zhong.calculator.CalculatorImpl.add(int,int))")
   - 方法参数与前置通知相同。
9. 返回通知
   - 在目标方法生成返回值后的位置
   - 注解：@AfterReturning(value = "execution(* zhong.calculator.CalculatorImpl.add(int,int))",returning = "result")
10. 异常通知
    - 在目标方法抛出异常的位置
    - 注解：@AfterThrowing(value = "execution(* zhong.calculator.CalculatorImpl.add(int,int))",throwing = "exception")，exception 是抛出的异常对象
11. 环绕通知
    - 是最复杂功能最强的通知
    - 可以同时在方法的前、后、返回、抛异常的位置
    - 注解：@Around(value = "execution(* zhong.calculator.CalculatorImpl.add(int,int))")
    - 方法的参数为：ProceedingJoinPoint  joinPoint
    - 在方法中通过 joinPoint.proceed() 来执行目标对象的方法。
    - 方法必须有返回值，也就是目标对象的返回值。

- 以上通过五个注解分别指定了五种类型的连接点也就是方法的不同的位置，这些位置可以被通知织入，织入后的通知会执行，从而实现 AOP 的功能。也就是可以利用以上的五个注解，在目标对象的方法的任何位置织入需要执行的代码，这就是 AOP 的典型应用模式。
- 以上的这些原理依然利用了动态代理技术

- 切点表达式的基本写法

  - "execution(* zhong.calculator.CalculatorImpl.mov(int,int))"

  1. 必须具有 execution 前缀。
  2. 第一个 * 表示任意修饰符和任意返回值，也可以直接使用修饰符和返回值类型。比如 public int
  3. 包可以有不同的层次，也可以用 * 来表示不同层级的包，比如 \*.*.\*. 另外 \*.. 表示任意层级。
  4. 最后一点的后面一定是方法名，也可以用 * 表示任意方法。
  5. 方法的入参不要写参数名，只用参数类型就可以，可以用 .. 表示任意个数类型的参数组合。

- 采用 xml 配置的方式来实现

  - ```java
    package com.zhong.calculator;
    
    import org.aspectj.lang.JoinPoint;
    import org.aspectj.lang.ProceedingJoinPoint;
    import org.aspectj.lang.annotation.*;
    import org.springframework.stereotype.Component;
    
    import java.util.Arrays;
    
    /**
     * @author 华韵流风
     * @ClassName CalculatorAspect
     * @Description TODO
     * @Date 2021/6/16 19:48
     * @packageName zhong.calculator
     */
    
    @Component
    @Aspect
    public class CalculatorAspect {
        public void beforeAdvice(JoinPoint joinPoint) {
            System.out.println("前置 方法 " + joinPoint.getSignature().getName() + " 参数=" + Arrays.asList(joinPoint.getArgs()));
        }
    
        public void afterAdvice(JoinPoint joinPoint) {
            System.out.println("后置 方法 " + joinPoint.getSignature().getName() + " 参数=" + Arrays.asList(joinPoint.getArgs()));
        }
    
        public void returnAdvice(JoinPoint joinPoint, Object result) {
            System.out.println("返回 " + joinPoint.getSignature().getName() + " 结果=" + result);
        }
    
        public void throwAdvice(JoinPoint joinPoint, Throwable exception) {
            System.out.println("异常 " + joinPoint.getSignature().getName() + " 异常内容=" + exception.getMessage());
        }
    
        public Object aroundAdvice(ProceedingJoinPoint joinPoint) {
            //执行前置代码
            System.out.println("前置 方法 " + joinPoint.getSignature().getName() + " 参数=" + Arrays.asList(joinPoint.getArgs()));
            Object result = null;
            try {
                //执行目标方法
                result = joinPoint.proceed();
                System.out.println("后置 方法 " + joinPoint.getSignature().getName() + " 参数=" + Arrays.asList(joinPoint.getArgs()));
                return result;
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                System.out.println("异常 方法" + joinPoint.getSignature().getName() + " 异常=" + throwable.getMessage());
            } finally {
                System.out.println("返回 方法" + joinPoint.getSignature().getName() + " 结果=" + result);
            }
            return null;
        }
    
    }
    ```

  - ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <beans xmlns="http://www.springframework.org/schema/beans"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:aop="http://www.springframework.org/schema/aop"
           xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/aop https://www.springframework.org/schema/aop/spring-aop.xsd">
    
        <!--  纯配置的aop的实现  -->
    
        <!--  需要使用的bean  -->
        <bean id="calculator" class="com.zhong.calculator.CalculatorImpl"/>
        <bean id="calculatorAspect" class="com.zhong.calculator.CalculatorAspect"/>
    
        <!--  使用spring提供的 aop相关的配置对切面，切入点，连接点等进行设置  -->
        <aop:config>
            <aop:pointcut id="pct" expression="execution(* com.zhong.calculator.CalculatorImpl.*(..))"/>
            <aop:aspect id="aspect" ref="calculatorAspect">
                <aop:before method="beforeAdvice" pointcut-ref="pct"/>
                <aop:after method="afterAdvice" pointcut-ref="pct"/>
                <aop:after-returning method="returnAdvice" pointcut-ref="pct" returning="result"/>
                <aop:after-throwing method="throwAdvice" pointcut-ref="pct" throwing="exception"/>
                <aop:around method="aroundAdvice" pointcut-ref="pct"/>
            </aop:aspect>
        </aop:config>
    </beans>
    ```

### 四，在项目中使用 spring 提供的 jdbc 模板来操作数据库

- 它是 spring 框架提供的一个基础组件，它基于 jdbc 之上，对一些常用的数据库的操作进行了封装，提供了 JdbcTemplate 来调用这些功能。

1. 引入 mysql 的驱动、连接池、spring-jdbc 的依赖。

2. 在配置中配置模板

   - ```xml
     <?xml version="1.0" encoding="UTF-8"?>
     <beans xmlns="http://www.springframework.org/schema/beans"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:context="http://www.springframework.org/schema/context"
            xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd">
     <!--引入数据文件-->
         <context:property-placeholder location="db.properties"/>
     
         <!--添加数据源，就是连接池的配置-->
         <bean id="datasource" class="com.alibaba.druid.pool.DruidDataSource">
             <property name="username" value="${jdbc.user}"/>
             <property name="password" value="${jdbc.password}"/>
             <property name="url" value="${jdbc.url}"/>
             <property name="driverClassName" value="${jdbc.driverName}"/>
             <property name="maxActive" value="${jdbc.MaxActive}"/>
             <property name="minIdle" value="${jdbc.MinIdle}"/>
             <property name="initialSize" value="${jdbc.InitialSize}"/>
         </bean>
     
     <!--  配置jdbc模板  -->
         <bean id="jdbcTemplate" class="org.springframework.jdbc.core.JdbcTemplate">
             <property name="dataSource" ref="datasource"/>
         </bean>
     
     
     </beans>
     ```

3. 在测试类中注入模板对象并使用

   - ```java
     package com.zhong;
     
     import org.junit.Test;
     import org.junit.runner.RunWith;
     import org.springframework.beans.factory.annotation.Autowired;
     import org.springframework.jdbc.core.JdbcTemplate;
     import org.springframework.test.context.ContextConfiguration;
     import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
     
     import java.util.List;
     import java.util.Map;
     import java.util.Set;
     
     /**
      * @author 华韵流风
      * @ClassName JdbcDempo
      * @Description TODO
      * @Date 2021/6/18 19:54
      * @packageName com.zhong
      */
     
     @RunWith(SpringJUnit4ClassRunner.class)
     @ContextConfiguration("classpath:applicationContext.xml")
     public class JdbcDemo {
         /**
          * 注入模板
          */
         @Autowired
         private JdbcTemplate template;
     
         @Test
         public void test1(){
             String sql = "update t_user set loginname =\"111111\" where uid=\"x\"";
             template.update(sql);
         }
     
         @Test
         public void test2(){
             String sql = "select * from t_user";
     
             //每个map对象对应一条记录
             List<Map<String, Object>> maps = template.queryForList(sql);
             for (Map<String, Object> map : maps) {
                 Set<Map.Entry<String, Object>> entries = map.entrySet();
                 for (Map.Entry<String, Object> entry : entries) {
                     System.out.println(entry.getKey()+":"+entry.getValue()+"\n");
                 }
             }
         }
     
     
     }
     ```

### 五、spring 的事务处理

1. 通过 AOP 技术来实现事务处理的功能。
2. 使用的是 jdk 的代理，要求 service 一定要有接口，需要进行事务处理的方法必须是公共的方法。
3. spring 的事务处理是一种对事务具体实现的抽象，无论采用什么数据库，编程者写的代码和配置是不变的。好处在于不用再写一些重复的事务处理代码，屏蔽了不同数据库的差异。
4. 抽象功能的完成，主要依靠 spring 提供的事务管理器来完成，它是 spring-jdbc 依赖所提供的多个类型，不同的类型针对不同的事务处理模式，比如编程式的事务，使用 hibernate orm 框架，比如通过 tomcat 所实现的 Jpa，最常用的就是利用普通的 Bean 通过切面技术实现事务处理。
5. 利用 spring 来实现事务处理主要有两种方式，一种是编程式的事务处理，另一种是声明式的事务处理。

- 声明式的事务处理（是推荐的 spring 的主要的事务处理方式，）：

  - 前提：添加与事务处理相关的依赖 spring-tx 及 spring-aspects。在配置文件中添加事务处理器。

  - ```xml
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
    <property name="dataSource" ref="datasource"/>
    </bean>
    ```

  1. 采用注解来实现：

     - 添加以下配置，让注解生效。

     - ```xml
       <!--  事务的注解驱动，属性可以不写，前提是事务管理器的id必须是transactionManager  -->
       <tx:annotation-driven transaction-manager="transactionManager"/>
       ```

     - 事务处理的功能都要在 service 层中实现，因此针对 service 中需要进行事务处理的方法加事务注解 @Transactional。该注解可以放在方法上对当前方法有效，如果放在类上则类中所有的方法都有效。被添加了注解的方法就成了目标方法，spring 会针对 service 创建代理对象，针对目标方法织入事务的通知，增强目标对象的功能。

     - 关于事务的传播行为：

       - 事务传播是指在一个事务中又创建了一个新事务。默认是 REQUARED。

       - ```java
         @Transactional(propagation = Propagation.REQUIRES_NEW)
         ```

       - 事务具有隔离级别，不同的数据库隔离程度不同，oracle 的隔离级别是读已提交，mysql 是可重复读。级别越高效率越低。spring 会依据所使用的数据库采用默认的隔离级别。在项目中一般不做修改。

  2. 采用配置的方式：
  
     - 配置的方式实现事务，要在配置文件中明确的指定切点表达式，还需要把 aop 的通知与事务管理器进行关联，另外还需要对事务进行明确的限电。配置的内容比基于注解的方式要复杂。
  
     - 这种更易于与 AOP 的技术联系在一起，应该是更接近底层的方式。
  
     - 设计要点：
  
       1. 添加 tx:advice 配置
  
          - ```xml
            <tx:advice id="advice" transaction-manager="transactionManager">
                <tx:attributes>
                    <tx:method name="buyBook" propagation="REQUIRES_NEW" timeout="2000"/>
                </tx:attributes>
            </tx:advice>
            ```
  
          - method：指定产生事务的方法
  
          - isolation：隔离级别
  
          - propagation：传播行为
  
          - read-only：是否只读事务，一般用在查询事务上
  
          - rollback-for：事务回滚的条件
  
          - no-rollback-for：事务不回滚的条件
  
       2. 使用 spring aop 的配置项
  
          - ```xml
            <aop:config>
                <aop:pointcut id="pct1" expression="execution(* service.StockService.buyBook(..))"/>
                <aop:pointcut id="pct2" expression="execution(* service.AccountService.pay(..))"/>
                <aop:advisor advice-ref="advice" pointcut-ref="pct1"/>
                <aop:advisor advice-ref="advice" pointcut-ref="pct2"/>
            </aop:config>
            ```
  
          - advisor：增强器，用指定的通知去增强切入点表达式所指定的方法。通过该配置可以明确的感受到，spring 的事务处理是把 spring 与 aop 技术（抽象）结合在一起。