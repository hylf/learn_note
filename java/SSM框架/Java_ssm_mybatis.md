### 一、什么是 ORM

- 称为对象关系映射，O 是对象，R 是关系，M 是映射。
- 对象可以理解为 java 中的 Bean（PO），关系指存放于关系型数据库表中的数据或结果数据。
- java 程序与数据库程序是两个独立的进程，它们各自的任务不同，更大的问题在于 java 面向对象的数据结构与关系表这种二维结构的数据差异很大，因为数据结构不同，设计的思维就不同，把两者结合在一起会给设计者造成麻烦。
- 如果能够在对象与关系之前建立一个桥梁，在 java 中我们只与对象打交道而不与关系型的数据打交道，会让程序设计变得更加简单，数据结构更加单一。比如我们从表中取出的数据存放在一个结果集中，必须通过遍历把关系型数据转换为集合对象以及集合中的元素对象。设计类似的功能就相当于是设计者在对象与关系之间建立了一个桥梁。另外，还需要在代码中书写 sql 命令，这样把 sql 与 java 代码耦合在一起。
- 现在有了 orm 的框架，比如 hibernate，mybatis，springdata 等，可以避免在对象与关系之间做过多的处理工作，因为框架实现了对象与关系之间的映射。
- orm 的框架工作在数据访问层，也就是 dao 层。因此，在项目中使用 ORM 的框架，可以很好地替换掉原来在 dao 层所有的一些繁琐的内容。

### 二、什么是 mybatis 框架

- 原名为 ibatis，是 apache 的项目，转手后名称变为 mybatis。它属于半 ORM 的框架，主要原因在于使用该框架用户需要自己来写 sql。虽然要写，但是不用写在代码中，而是写在映射配置文件中。

### 三、如何使用 mybatis 框架

- mybatis 工作在 dao 层，只要项目需要操作数据库，都可以使用它。

1. 创建一个 java 项目，添加依赖

   - ```xml
     <dependencies>
         <dependency>
             <groupId>mysql</groupId>
             <artifactId>mysql-connector-java</artifactId>
             <version>8.0.24</version>
         </dependency>
     
         <dependency>
             <groupId>org.mybatis</groupId>
             <artifactId>mybatis</artifactId>
             <version>3.4.6</version>
         </dependency>
         <dependency>
             <groupId>log4j</groupId>
             <artifactId>log4j</artifactId>
             <version>1.2.12</version>
         </dependency>
         <dependency>
             <groupId>org.slf4j</groupId>
             <artifactId>slf4j-api</artifactId>
             <version>1.5.6</version>
         </dependency>
     
         <dependency>
             <groupId>junit</groupId>
             <artifactId>junit</artifactId>
             <version>4.13.2</version>
             <scope>test</scope>
         </dependency>
     </dependencies>
     ```

2. 添加模组

3. 向项目中添加配置文件 mybatis.xml

   - ```xml
     <?xml version="1.0" encoding="UTF-8" ?>
     <!DOCTYPE configuration PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
     "http://mybatis.org/dtd/mybatis-3-config.dtd">
     <configuration>
     	<!--引用外部的属性文件-->
        <properties resource="db.properties"></properties>
     	<!--环境组配置，其中可以配置多个使用环境，每个环境对应于一个数据库-->
        	<environments default="development">	
     		<environment id="development">
     			<!--指定使用jdbc的事务处理-->
     		    <transactionManager type="JDBC"/>
     			<!--数据库连接池，它是mybatis自带的连接池-->
     			<dataSource type="POOLED">
     				<property name="driver" value="${driver}"/>
     				<property name="url" value="${url}"/>
     				<property name="username" value="${user}"/>
     				<property name="password" value="${password}"/>
     			</dataSource>
     		</environment>
     	</environments>
     	<!--映射配置，指定映射文件的路径-->
     	<mappers>
     	   <mapper resource="mapper.xml"/>
     	</mappers>
     </configuration>
     ```

4. 添加映射文件 mapper.xml

5. 在 mapper 中添加 satement

   - ```xml
     <?xml version="1.0" encoding="UTF-8" ?>
     <!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
     "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
     
     <!--namespace是命名空间，本文件中所命名的语句只在本空间中有效，可以解决映射文件中同名语句的冲突，可以与接口的全限定名做关联，从而实现mapper代理的功能-->
     
     <mapper namespace="test">  
        <insert id="insert" parameterType="com.zhong.po.Book">
           insert into book values (#{isbn},#{bookName},#{price})
        </insert>
     
        <update id="update" parameterType="com.zhong.po.Book">
           update book set price=#{price} where isbn=#{isbn}
        </update>
     
        <delete id="delete" parameterType="String">
           delete from book where isbn=#{isbn}
        </delete>
     
        <select id="findBookById" parameterType="String" resultType="com.zhong.po.Book">
           select * from book where isbn=#{isbn}
        </select>
     
        <select id="findBookAll" resultType="com.zhong.po.Book">
           select * from book
        </select>
     </mapper>
     ```

6. 测试代码

   - ```java
     package com.zhong.po;
     
     /**
      * @author 华韵流风
      * @ClassName Book
      * @Description TODO
      * @Date 2021/6/24 20:19
      * @packageName com.zhong.po
      */
     public class Book {
         private String isbn;
         private String bookName;
         private int price;
     
         public Book(String isbn, String bookName, int price) {
             this.isbn = isbn;
             this.bookName = bookName;
             this.price = price;
         }
     
         public Book() {
         }
     
         @Override
         public String toString() {
             return "Book{" +
                     "isbn='" + isbn + '\'' +
                     ", bookName='" + bookName + '\'' +
                     ", price=" + price +
                     '}';
         }
     
         public String getIsbn() {
             return isbn;
         }
     
         public void setIsbn(String isbn) {
             this.isbn = isbn;
         }
     
         public String getBookName() {
             return bookName;
         }
     
         public void setBookName(String bookName) {
             this.bookName = bookName;
         }
     
         public int getPrice() {
             return price;
         }
     
         public void setPrice(int price) {
             this.price = price;
         }
     }
     ```

   - ```java
     import com.zhong.po.Book;
     import org.apache.ibatis.io.Resources;
     import org.apache.ibatis.session.SqlSession;
     import org.apache.ibatis.session.SqlSessionFactory;
     import org.apache.ibatis.session.SqlSessionFactoryBuilder;
     import org.junit.Before;
     import org.junit.Test;
     
     import java.io.IOException;
     import java.io.InputStream;
     import java.util.List;
     
     /**
      * @author 华韵流风
      * @ClassName MybatisDemo
      * @Description TODO
      * @Date 2021/6/24 20:23
      * @packageName PACKAGE_NAME
      */
     public class MybatisDemo {
         SqlSessionFactory factory = null;
     
         @Before
         public void init(){
             //得到主配置文件的流对象
             try{
                 InputStream ris = Resources.getResourceAsStream("mybatis.xml");
                 factory = new SqlSessionFactoryBuilder().build(ris);
             }catch (IOException e){
                 e.printStackTrace();
             }
         }
     
         @Test
         public void testInsert(){
             SqlSession session = factory.openSession();
             Book book = new Book("b3","c++",20);
             session.insert("insert",book);
             session.commit();
             session.close();
         }
     
         @Test
         public void testUpdate(){
             SqlSession session = factory.openSession();
             Book book = new Book("b3","",200);
             session.update("update",book);
             session.commit();
             session.close();
         }
     
         @Test
         public void testDelete(){
             SqlSession session = factory.openSession();
             session.delete("delete","b3");
             session.commit();
             session.close();
         }
     
         @Test
         public void testSelect(){
             SqlSession session = factory.openSession();
             Book book = session.selectOne("findBookById", "b1");
             System.out.println(book);
             session.commit();
             session.close();
         }
     
         @Test
         public void testSelectAll(){
             SqlSession session = factory.openSession();
             List<Book> books = session.selectList("findBookAll");
             for (Book book : books) {
                 System.out.println(book);
             }
             session.commit();
             session.close();
         }
     }
     ```
   
7. 在 insert 语句中添加获取新生成的主键的配置。

   - ```xml
     <selectKey keyProperty="isbn" keyColumn="isbn" order="AFTER" resultType="String">
        SELECT LAST_INSERT_ID()
     </selectKey>
     ```

8. 在 select 语句中进行模糊查询

   - ```mysql
     select * from book where bookname like '%${value}%'
     ```

   - 该语句使用了 \${value}。
   - \${value} 表示一个字符串参数，它可以与其他的串进行拼接，例如‘%\${value}%’
   - #{ } 主要用来生成 preparestatement 的 ？，另外决定了参数的传递位置及内容。

### 四、按照 dao 层的设计规范来使用 mybatis。

1. 在 dao 接口的实现类的各方法的代码中，有什么不好的地方？
   - 大量冗余代码，代码顺序都相同，区别在于执行的语句参数和返回值。

### 五、在 mybatis 中使用 mapper 代理

- 为了解决在 dao 中出现重复的代码以及固定的代码书写顺序的问题，可以通过代理技术把一些重复的按固定套路书写的代码织入到基础代码中，设计者在写代码时，只用关注不同的命令中不同的内容，也就是知道要干什么就可以，而不用自己去干，从而简便开发的过程。
- 此处使用的代理仍然是 jdk 的代理，它需要接口也就是 dao 层的接口，而实现类不用再写，它由代理对象来承担。此处的代理对象由 mybatis 来创建。如果要使用 mapper 代理，在设计时还需要注意几个问题，
  1. 一个是必须要有接口。
  2. 第二接口所在的包必须被扫描（保证接口能够被精确的获取）。
  3. 第三映射文件的命名空间与语句的 id 分别必须取值为接口的全限定名及接口的方法名。
  4. 语句的参数必须与接口方法的参数保持一致。
  5. 第五接口与对应的映射文件可以放在同一包下且映射文件的名称与接口的名称一致。
  6. 在 maven 项目中，映射文件可以放在 resources 下但是目录层必须与对应的接口所在的包的层级保持一致。（在 resources 下一次建多级目录应当这样操作：d1/d2/d3……，而不是 d1.d2.d3 ）。

### 六、关于映射文件中的语句的参数问题

- 关于 mapper 接口方法中有多个参数的问题。

1. 用 Map 作为参数

   - ```xml
     <insert id="add2" parameterType="HashMap">
        insert into book values (#{isbn},#{bookName},#{price})
     </insert>
     ```

   - ```java
     @Test
     public void testInsert2(){
         SqlSession session = factory.openSession();
         BookMapper mapper = session.getMapper(BookMapper.class);
         Map<String,String> map = new HashMap<String, String>();
         map.put("isbn", "b5");
         map.put("bookName", "jq");
         map.put("price", "30");
         mapper.add2(map);
         session.commit();
         session.close();
     }
     ```

2. 用参数注解来标识各个参数

   - ```java
     /**
      * 添加
      *
      * @param isbn isbn
      * @param bookName bookName
      * @param price price
      */
     void add3(@Param("isbn")String isbn,@Param("bookName")String bookName,@Param("price")int price);
     ```

   - ```xml
     <insert id="add3">
     		insert into book values (#{isbn},#{bookName},#{price})
     </insert>
     
     ```

   - ```java
     @Test
     public void testInsert4(){
         SqlSession session = factory.openSession();
         BookMapper mapper = session.getMapper(BookMapper.class);
         mapper.add3("b6","python",13);
         session.commit();
         session.close();
     }
     ```

3. 用参数的顺序来指定

   - ```java
     /**
      * 添加（参数顺序）
      *
      * @param isbn isbn
      * @param bookName bookName
      * @param price price
      */
     void add4(String isbn,String bookName,int price);
     ```

   - ```xml
     <insert id="add4">
        insert into book values (#{param1},#{param2},#{param3})
     </insert>
     ```

   - ```java
     @Test
     public void testInsert5(){
         SqlSession session = factory.openSession();
         BookMapper mapper = session.getMapper(BookMapper.class);
         mapper.add4("b7","go",15);
         session.commit();
         session.close();
     }
     ```

### 七、在 mapper 配置中的动态 sql 的配置

1. 需要执行的 sql 命令会随着条件的不同而生成不同的命令，这就是动态 sql。

2. mapper 中提供的与动态 sql 有关的配置。

   - \<where> 标签，成对出现，用来表示 sql 命令的条件部分。

     - ```xml
       <where>
          <include refid="booksql"/>
       </where>
       ```

   - \<if> 标签，在配置中可以用来对条件或逻辑表达式的结果进行判断，并执行相应的功能。

   - \<sql> 标签，它表示 sql 片段，也就是一个完整的 sql 命令中的一部分，可以是任何部分。

     - ```xml
       <sql id="booksql">
          <if test="isbn!=null and isbn!=''">
             isbn=#{isbn}
          </if>
       
          <if test="bookName!=null and bookName!=''">
             bookname=#{bookName}
          </if>
       </sql>
       ```

   - \<foreach> 标签，遍历标签，可以对集合中的元素进行遍历。

     - ```xml
       <select id="findBookByIds" parameterType="book" resultType="book">
       		select * from book where isbn in
       		<foreach collection="list" item="item" separator="," open="(" close=")">
       			#{item}
       		</foreach>
       </select>
       ```
     
     - ```java
       @Test
       public void testSelect2(){
           SqlSession session = factory.openSession();
           BookMapper mapper = session.getMapper(BookMapper.class);
           List<String> list = new ArrayList<String>();
           Collections.addAll(list, "b1","b2","b3");
           List<Book> books = mapper.findBookByIds(list);
           System.out.println(books);
           session.close();
       }
       ```

### 八、mybtis 实体间的映射关系

- 数据库表间存在四种关系，分别是一对一，多对多，多对一，一对多。多对多可以添加中间表生成多对一和一对多的关系，一对一就是多对一的特例。

1. resultMap

   - 作用：实现结果映射，当查询结果与现有的数据类型不匹配时，比如表的列名与 po 的属性名不同就无法得到想要的结果。

   - ```xml
     <resultMap id="bookMap" type="book">
        <id column="isbn" property="isbn"/>
        <result column="bookname" property="bookName"/>
        <result column="price" property="price"/>
     </resultMap>
     ```

   - ```xml
     <select id="findBook" parameterType="book" resultMap="bookMap">
        select * from book
        <where>
           <include refid="booksql"/>
        </where>
     </select>
     ```

2. 实现关联查询，也就是基于多表查询。查询时一般使用连接查询，包括内连接，外连接，全外连接等。

   - ORM 框架都提供了基于多表查询的映射方法，从而以一种更面向对象的方式生成结果。

   - 对于有连接关系的 po 之间，要把对方作为一个属性归入进来，如果对方是一方，只用声明 po 类型。如果是多方，就要声明为集合。

     - ```xml
       <resultMap id="bookMap" type="book">
          <id column="isbn" property="isbn"/>
          <result column="bookname" property="bookName"/>
          <result column="price" property="price"/>
          <association property="" javaType="">
             <!--配置-->
              <!--property表示属性名，javaType表示属性类型，column表示表的列名-->
          </association>
       </resultMap>
       ```
       
     - ```xml
       <resultMap id="bookMap" type="book">
                 <id column="isbn" property="isbn"/>
                 <result column="bookname" property="bookName"/>
                 <result column="price" property="price"/>
                 <collection property="" javaType="">
                    <!--配置-->
                 </collection>
              </resultMap>
       ```

### 九、mybatis 逆向工程

- 由官方提供的一个工程模板，可以根据指定的数据库自动生成针对单表的所有的常规的增删改查的配置及 mapper 接口。

1. 在 pom 中引用插件依赖

   - ```xml
     <build>
             <finalName>zsxt</finalName>
             <!--执行逆向工程代码的插件-->
             <plugins>
                 <plugin>
                     <groupId>org.mybatis.generator</groupId>
                     <artifactId>mybatis-generator-maven-plugin</artifactId>
                     <version>1.3.5</version>
                     <configuration>
                         <verbose>true</verbose>
                         <overwrite>true</overwrite>
                     </configuration>
     
                     <dependencies>
                         <dependency>
                             <groupId>mysql</groupId>
                             <artifactId>mysql-connector-java</artifactId>
                             <version>8.0.24</version>
                         </dependency>
                     </dependencies>
     
                 </plugin>
             </plugins>
         </build>
         <dependencies>
             <dependency>
                 <groupId>org.mybatis</groupId>
                 <artifactId>mybatis</artifactId>
                 <version>3.4.6</version>
             </dependency>
         </dependencies>
     ```

2. 修改配置文件

   - ```properties
     jdbc.driverClass=com.mysql.cj.jdbc.Driver
     jdbc.connectionURL=jdbc:mysql://localhost:3306/bookshop?serverTimezone=UTC&characterEncoding=utf-8&useSSL=false
     jdbc.userId=
     jdbc.password=
     ```
     
- ```xml
     <?xml version="1.0" encoding="UTF-8"?>
     <!DOCTYPE generatorConfiguration
             PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
             "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">
     
     <generatorConfiguration>
         <!--导入属性配置-->
         <properties resource="generator.properties"/>
      
         <context id="default" targetRuntime="MyBatis3">
     
             <!-- optional，旨在创建class时，对注释进行控制 -->
             <commentGenerator>
                 <property name="suppressDate" value="true"/>
                 <property name="suppressAllComments" value="true"/>
             </commentGenerator>
     
             <!--jdbc的数据库连接 -->
             <jdbcConnection
                     driverClass="${jdbc.driverClass}"
                     connectionURL="${jdbc.connectionURL}"
                     userId="${jdbc.userId}"
                     password="${jdbc.password}">
             </jdbcConnection>
     
     
             <!-- 非必需，类型处理器，在数据库类型和java类型之间的转换控制-->
             <javaTypeResolver>
                 <property name="forceBigDecimals" value="false"/>
             </javaTypeResolver>
     
     
             <!-- Model模型生成器,用来生成含有主键key的类，记录类 以及查询Example类
                 targetPackage     指定生成的model生成所在的包名
                 targetProject     指定在该项目下所在的路径
             -->
             <javaModelGenerator targetPackage="com.zhong.pojo"
                                 targetProject="src/main/java">
     
                 <!-- 是否允许子包，即targetPackage.schemaName.tableName -->
                 <property name="enableSubPackages" value="false"/>
                 <!-- 是否对model添加 构造函数 -->
                 <property name="constructorBased" value="true"/>
                 <!-- 是否对类CHAR类型的列的数据进行trim操作 -->
                 <property name="trimStrings" value="true"/>
                 <!-- 建立的Model对象是否 不可改变  即生成的Model对象不会有 setter方法，只有构造方法 -->
                 <property name="immutable" value="false"/>
             </javaModelGenerator>
     
             <!--Mapper映射文件生成所在的目录 为每一个数据库的表生成对应的SqlMap文件 -->
             <sqlMapGenerator targetPackage="com.zhong.mapper"
                              targetProject="src/main/java">
                 <property name="enableSubPackages" value="false"/>
             </sqlMapGenerator>
     
             <!-- 客户端代码，生成易于使用的针对Model对象和XML配置文件 的代码
                     type="ANNOTATEDMAPPER",生成Java Model 和基于注解的Mapper对象
                     type="MIXEDMAPPER",生成基于注解的Java Model 和相应的Mapper对象
                     type="XMLMAPPER",生成SQLMap XML文件和独立的Mapper接口
             -->
             <javaClientGenerator targetPackage="com.zhong.mapper"
                                  targetProject="src/main/java" type="XMLMAPPER">
                 <property name="enableSubPackages" value="true"/>
             </javaClientGenerator>
     
             <!--要执行逆向工程所用到的表-->
             <table tableName="book" />
             <table tableName="stock" />
         </context>
     </generatorConfiguration>
     ```
   
3. 执行 maven 命令：mybatis-generator:generate。（如果需要再次执行，需要先把前面执行生成的xml文件全部删除）

4. 在指定的包下查看结果。

   - 对应于表的 po。
   - 对应于 po 的 mapper 接口及映射文件。
   - 对应于 po 映射文件的可以生成执行条件的 Example 类型。

5. 把逆向工程生成的内容复制到使用的项目中。测试：

   - ```java
     import com.zhong.mapper.BookMapper;
     import com.zhong.pojo.Book;
     import com.zhong.pojo.BookExample;
     import org.apache.ibatis.io.Resources;
     import org.apache.ibatis.session.SqlSession;
     import org.apache.ibatis.session.SqlSessionFactory;
     import org.apache.ibatis.session.SqlSessionFactoryBuilder;
     import org.junit.Test;
     
     import java.io.IOException;
     import java.io.InputStream;
     import java.util.List;
     
     /**
      * @author 华韵流风
      * @ClassName GeneratorDemo
      * @Description TODO
      * @Date 2021/6/28 12:47
      * @packageName com.zhong
      */
     public class GeneratorDemo {
         private SqlSessionFactory factory;
     
         public GeneratorDemo() {
             try{
                 InputStream ris = Resources.getResourceAsStream("mybatis.xml");
                 factory = new SqlSessionFactoryBuilder().build(ris);
             }catch (IOException e){
                 e.printStackTrace();
             }
         }
     
         @Test
         public void t1(){
             SqlSession session = factory.openSession();
             BookMapper mapper = session.getMapper(BookMapper.class);
             //创建复杂条件查询所需要的example。
             BookExample example = new BookExample();
             //通过example创建条件对象。
             BookExample.Criteria criteria = example.createCriteria();
             //使用criteria来指定查询条件。
             criteria.andIsbnEqualTo("b1");
             List<Book> books = mapper.selectByExample(example);
             System.out.println(books);
         }
     
     }
     ```

### 十、用 spring 去整合 mybatis

1. 创建整合工程。

2. 引入 spring 及 mybatis 的相关依赖及 mybatis-spring 的整合包。

   - ```xml
     <?xml version="1.0" encoding="UTF-8"?>
     <project xmlns="http://maven.apache.org/POM/4.0.0"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
         <modelVersion>4.0.0</modelVersion>
     
         <groupId>org.example</groupId>
         <artifactId>spring-mybatis-Demo</artifactId>
         <version>1.0-SNAPSHOT</version>
     
         <dependencies>
             <dependency>
                 <groupId>log4j</groupId>
                 <artifactId>log4j</artifactId>
                 <version>1.2.17</version>
             </dependency>
     
             <dependency>
                 <groupId>org.slf4j</groupId>
                 <artifactId>slf4j-api</artifactId>
                 <version>2.0.0-alpha1</version>
             </dependency>
     
             <dependency>
                 <groupId>org.slf4j</groupId>
                 <artifactId>slf4j-log4j12</artifactId>
                 <version>2.0.0-alpha1</version>
             </dependency>
     
             <dependency>
                 <groupId>junit</groupId>
                 <artifactId>junit</artifactId>
                 <version>4.13.2</version>
                 <scope>test</scope>
             </dependency>
     
             <dependency>
                 <groupId>org.springframework</groupId>
                 <artifactId>spring-test</artifactId>
                 <version>5.3.7</version>
             </dependency>
     
             <dependency>
                 <groupId>mysql</groupId>
                 <artifactId>mysql-connector-java</artifactId>
                 <version>8.0.24</version>
             </dependency>
     
             <dependency>
                 <groupId>com.alibaba</groupId>
                 <artifactId>druid</artifactId>
                 <version>1.2.6</version>
             </dependency>
     
             <dependency>
                 <groupId>org.springframework</groupId>
                 <artifactId>spring-context</artifactId>
                 <version>5.3.7</version>
             </dependency>
     
             <dependency>
                 <groupId>org.springframework</groupId>
                 <artifactId>spring-jdbc</artifactId>
                 <version>5.3.7</version>
             </dependency>
     
             <dependency>
                 <groupId>org.springframework</groupId>
                 <artifactId>spring-tx</artifactId>
                 <version>5.3.7</version>
             </dependency>
     
             <dependency>
                 <groupId>org.springframework</groupId>
                 <artifactId>spring-aspects</artifactId>
                 <version>5.3.7</version>
             </dependency>
     
             <dependency>
                 <groupId>org.mybatis</groupId>
                 <artifactId>mybatis</artifactId>
                 <version>3.4.6</version>
             </dependency>
     
             <dependency>
                 <groupId>org.mybatis</groupId>
                 <artifactId>mybatis-spring</artifactId>
                 <version>2.0.6</version>
             </dependency>
     
             <!--    引入分页依赖    -->
             <dependency>
                 <groupId>com.github.pagehelper</groupId>
                 <artifactId>pagehelper</artifactId>
                 <version>5.2.1</version>
             </dependency>
         </dependencies>
     
     </project>
     ```

3. 把逆向工程生成的 pojo 接口及映射文件复制到项目中。

4. 把映射文件放到 resources 的子目录（对应 mapper 接口所在的包）

5. 创建 service 子包，包下创建接口，子包下创建接口的实现类。

   - ```java
     package com.zhong.service;
     
     import com.zhong.pojo.Book;
     
     /**
      * @author 华韵流风
      * @ClassName BookService
      * @Description TODO
      * @Date 2021/6/28 16:38
      * @packageName com.zhong.service
      */
     public interface BookService {
     
         /**
          * 按isbn查书
          *
          * @param isbn isbn
          * @return Book
          */
         Book findBookByIsbn(String isbn);
     }
     ```

6. 给 service 实现类加 DI 的注解，@Service，@Autowired。

   - ```java
     package com.zhong.service;
     
     import com.zhong.mapper.BookMapper;
     import com.zhong.pojo.Book;
     import org.springframework.beans.factory.annotation.Autowired;
     import org.springframework.stereotype.Service;
     
     
     /**
      * @author 华韵流风
      * @ClassName BookServiceImpl
      * @Description TODO
      * @Date 2021/6/28 16:40
      * @packageName com.zhong.service
      */
     @Service
     public class BookServiceImpl implements BookService{
     
         @Autowired
         private BookMapper bookMapper;
     
         public Book findBookByIsbn(String isbn) {
             return bookMapper.selectByPrimaryKey(isbn);
         }
     }
     ```

7. 写 spring 的配置文件（最重点）。

   - ```xml
     <?xml version="1.0" encoding="UTF-8"?>
     <beans xmlns="http://www.springframework.org/schema/beans"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:context="http://www.springframework.org/schema/context" xmlns:tx="http://www.springframework.org/schema/tx"
            xmlns:aop="http://www.springframework.org/schema/aop"
            xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd http://www.springframework.org/schema/aop https://www.springframework.org/schema/aop/spring-aop.xsd">
     
         <!--  引入属性文件  -->
         <context:property-placeholder location="db.properties"/>
     
         <!--  使用DI注解，需要包扫描  -->
         <context:component-scan base-package="com.zhong.service.impl"/>
     
         <!--  配置数据源  -->
         <bean id="datasource" class="com.alibaba.druid.pool.DruidDataSource">
             <property name="driverClassName" value="${jdbc.driverName}"/>
             <property name="url" value="${jdbc.url}"/>
             <property name="username" value="${jdbc.user}"/>
             <property name="password" value="${jdbc.password}"/>
         </bean>
     
         <!--  配置事务管理器  -->
         <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
             <property name="dataSource" ref="datasource"/>
         </bean>
     
         <!--  事务通知  -->
         <tx:advice id="avc" transaction-manager="transactionManager">
             <tx:attributes>
                 <!--  查询的方法-->
                 <tx:method name="find*" read-only="true"/>
                 <!-- 添加的方法      -->
                 <tx:method name="add*"/>
             </tx:attributes>
         </tx:advice>
     
         <!--  事务AOP配置  -->
         <aop:config>
             <aop:pointcut id="pct" expression="execution(* com.zhong.service.*.*(..))"/>
             <aop:advisor advice-ref="avc" pointcut-ref="pct"/>
         </aop:config>
     
         <!--  整合mybatis的相关配置  -->
         <!--  把mybatis会话工厂添加到容器  -->
         <bean id="sqlSessionFactoryBean" class="org.mybatis.spring.SqlSessionFactoryBean">
             <property name="dataSource" ref="datasource"/>
             <property name="configLocation" value="mybatis.xml"/>
         </bean>
     
         <!--  添加实现mybatis mapper代理功能的bean  -->
         <bean id="mapperScanner" class="org.mybatis.spring.mapper.MapperScannerConfigurer">
             <property name="basePackage" value="com.zhong.mapper"/>
             <property name="sqlSessionFactoryBeanName" value="sqlSessionFactoryBean"/>
         </bean>
     
     </beans>
     ```

### 十一、利用 mybatis 的分页插件实现分页功能

- 方案一：利用 mybatis 所提供的 RowBounds 来设置分布参数，以该方法作为 dao 方法中的参数。执行原理属于逻辑分页，需要先查出所有的记录，一般不用。

- 方案二：采用物理分页，会在 sql 中使用 limit 关键字，基于逆向工程的情况下，会修改映射文件中的语句，会造成侵入式的设计。

- 方案三：采用第三方的分页插件，pageHelper，也是属于物理分页。

- pageHelper的使用方法：

  1. 引入依赖。

     - ```xml
       <dependency>
           <groupId>com.github.pagehelper</groupId>
           <artifactId>pagehelper</artifactId>
           <version>5.2.1</version>
       </dependency>
       ```

  2. 在 mybatis 的主配置文件中添加 pageHelper 的分页插件配置（可以在 spring 配置中写，也可以在 mybatis 配置文件中写）。

     - ```xml
       <?xml version="1.0" encoding="UTF-8" ?>
       <!DOCTYPE configuration PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
               "http://mybatis.org/dtd/mybatis-3-config.dtd">
       
       <configuration>
           <!--  原有的配置已被spring取代  -->
           <!--  分页配置  -->
           <plugins>
               <plugin interceptor="com.github.pagehelper.PageInterceptor">
               </plugin>
           </plugins>
       
       </configuration>
       ```
     
     - ```java
       import com.zhong.pojo.Book;
       import com.zhong.service.BookService;
       import org.junit.Test;
       import org.junit.runner.RunWith;
       import org.springframework.beans.factory.annotation.Autowired;
       import org.springframework.test.context.ContextConfiguration;
       import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
       
       import java.util.List;
       
       /**
        * @author 华韵流风
        * @ClassName ServiceDemo
        * @Description TODO
        * @Date 2021/6/28 17:06
        * @packageName PACKAGE_NAME
        */
       
       @RunWith(SpringJUnit4ClassRunner.class)
       @ContextConfiguration("classpath:applicationContext.xml")
       public class ServiceDemo {
       
           @Autowired
           BookService bookService;
       
           @Test
           public void t1(){
               Book b1 = bookService.findBookByIsbn("b7");
               System.out.println(b1);
       
           }
       
           @Test
           public void t2(){
               List<Book> books = bookService.findBookByExample();
               System.out.println(books);
           }
       
       
       }
       ```

