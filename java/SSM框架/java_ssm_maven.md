### 一、为什么需要使用 maven？

- 设计项目时，需要用到多种外部的 jar 包，当包的数量和包之间的关系越来越多，越来越复杂的时候，会造成引入的包并不是我们需要的包，也可能不同的包与另外的包都发生关系，会造成包的混乱，就会造成编译出问题，也可能使用的功能不正确。如果使用 maven，则可以最大限度的避免问题的发生。

### 二、maven

1. 它是 apache 公司的一个项目，作用是管理项目（比如编译项目，测试项目，打包项目，发布项目等），另外，更重要的一点是可以实现依赖管理。
2. 依赖管理就是可以按照坐标把项目所需要使用的 jar 包引入到项目中来。从而不需要去复制这些 jar 包。
3. maven 所提供的资源都在 maven 的中央仓库中。有些公司内部还搭建了私服（公司内部可用的 maven 仓库）；很多开发者的电脑中也有本地仓库。

### 三、使用 maven

1. 得到 maven 的压缩包
2. 解压到指定的目录
3. 修改 settings.xml 文件中的内容。
4. 配置 path。
5. 测试 mvn，mvn -version，输出版本号，表示可用。

### 四、配置 maven

1. 在 maven 的安装目录下，打开 settings.xml 文件。
2. 创建本地仓库对应的目录，把该目录写到 \<localRepository>E:\jar lib\mvnrepository\</localRepository>   ，以后从中央仓库下载的 jar 包都会放在该目录下，
3. mirror 表示镜像，指定为\<url>http://maven.aliyun.com/nexus/content/repositories/central\</url>，表示使用国内的阿里云的镜像仓库作为中央仓库，以避免访问国外的仓库不正常的问题。

五、使用 maven 来管理项目

1. 了解 maven 项目的基本结构，只有符合所要求结构的项目才能被 maven 正确的管理。
   - 项目的根目录：
     - src（源程序）：
       - main：
         - java：
           - 包
         - resources：
           - 配置文件
       - test：
         - 包：
           - 测试类
     - pom.xml 文件（maven 项目的主配置文件，包括的内容很多，主要包含项目所有的依赖，插件，构建工具，以及项目间的层次依赖，项目的类型，项目自身的坐标。）
2. 编译项目：在根目录下打开命令行，执行 mvn compile
3. 打包项目，生成 jar 包：mvn package
4. 把项目安装到本地：mvn install

- 编译：mvn compile　　--src/main/java目录java源码编译生成class （target目录下）

  测试：mvn test　　　　--src/test/java 目录编译

  清理：mvn clean　　　 --删除target目录，也就是将class文件等删除

  打包：mvn package　　--生成压缩文件：java项目#jar包；web项目#war包，也是放在target目录下

  安装：mvn install　　　--将压缩文件(jar或者war)上传到本地仓库

### 六、在 idea 中使用 maven

1. 在设置中把 maven 的本地仓库指向自己创建的仓库，把 settings 的路径设置为文件所在的路径。
2. 创建 maven 项目，所有的项目都可以创建为 maven 项目，比如 jar（java 项目），war（java web 项目），pom（聚合项目）。
3. 创建项目时可以选择项目的模板，也可以不选择。
4. 创建 jar 项目，不需要使用模板。首先添加依赖，如果本地仓库中没有就到中央仓库去下载并存在本地仓库。
5. 创建 web 项目，可以使用模板，在 main 下面创建 java 和 resources 子目录，并分别指定为 Sources 和 Resources 目录类型，目录的颜色
   会发生变化，否则编译会有问题。

### 七、现在流行的项目管理工具

1. gradle 工具，速度和执行效率比 maven 快，添加依赖的语法比 maven 要复杂。
2. maven 工具，速度一般。