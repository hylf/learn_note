### MongoDB

### 一、什么是 mongodb

- 它属于 no sql 数据库，介于关系型非关系型数据库之间，主要用来保存大数据量的数据。

### 二、mongodb 的逻辑结构

- mongodb->database->collection->document

- BSON，类似于JSON，但是数据类型不同，还包括特殊符号。

### 三、安装 mongodb（<https://www.mongodb.com/try/download/community>）

1. 安装 window 平台下的数据库，它包括服务器和客户端两部分。
2. 安装 linux 平台下的数据库，同样包括客户端和服务器。

### 四、连接服务器 mongo ip

### 五、使用

- 主键必须是 \_id。
- mongodb 默认数据库是 test。
- use 作用：存在该数据库时连接到该数据库；不存在时创建。
- 执行  db.spit.insert({"a":"abc"})，第一次执行时就创建了 spit 集合，并添加一份文档数据。
- db.spit.find() 查询所有的数据，如果添加数据时未指定 id，会自动生成一个唯一的 id。在生产环境下要避免无条件的查询命令，另外在添加数据时需要指定自己的 id。另外，查询条件可以是 js 的正则表达式。
- db.spit.count() 查询记录条数。
- db.spit.insert({_id:"4",content:"坚持就是胜利",userid:"1014",nickname:"诺诺",visits:NumberInt(1223)});
- 关于数字类型：
  - NumberInt，NumberLong 分别表示四字节和八字节的整数，如果数字不带上面的函数则数据类型是小数。不同的类型在 java 客户端取值时方法是不同的。
- 查询数据：db.集合.find(bson条件)。
- 修改数据：db.集合.update({条件},{修改的值})，新值必须是完整的，否则会丢失，也就是不修改的也要写。
- 删除数据：db.集合.remove({条件})。不加条件删除所有。

### 六、java 操作 mongodb

1. 引入依赖：

   - ```xml
     <dependency>
         <groupId>org.mongodb</groupId>
         <artifactId>mongodb-driver</artifactId>
     </dependency>
     ```

2. 实现查询：

   - ```java
     package com.zhong;
     
     import com.mongodb.BasicDBObject;
     import com.mongodb.MongoClient;
     import com.mongodb.client.FindIterable;
     import com.mongodb.client.MongoCollection;
     import com.mongodb.client.MongoDatabase;
     import org.bson.Document;
     
     import java.util.HashMap;
     import java.util.Map;
     
     /**
      * @author 华韵流风
      * @ClassName MongoDemo
      * @Date 2021/9/25 11:00
      * @packageName com.zhong
      * @Description TODO
      */
     public class MongoDemo {
         public static void main(String[] args) {
             //创建数据库客户端对象，它的作用类似于连接对象。
             MongoClient mongoClient = new MongoClient("192.168.46.130");
             //得到指定的数据库
             MongoDatabase database = mongoClient.getDatabase("tensquare");
     //        System.out.println(database);
             //得到集合
             MongoCollection<Document> spit = database.getCollection("spit");
             //bson数据可以嵌套，value也可以是一个bson
             /*Map<String,Object> map = new HashMap<>();
             map.put("_id","2");*/
             BasicDBObject bson = new BasicDBObject("visits",new BasicDBObject("$gt",1000));
             //查询
             FindIterable<Document> documents = spit.find(bson);
             for (Document document : documents) {
                 System.out.println(document);
             }
         }
     }
     ```

### 七、使用 spring data mongodb，这是项目中的使用方式。

1. 启动器

   - ```xml
     <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-data-mongodb</artifactId>
     </dependency>
     ```

2. 配置文件：

   - ```yml
     server:
       port: 9006
     spring:
       application:
         name: tensquare-spit
       data:
         mongodb:
           host: 192.168.46.130
           database: tensquare
     ```

3. 示例 pojo：

   - ```java
     package com.tensquare.spit.pojo;
     
     import lombok.Data;
     import org.springframework.data.annotation.Id;
     import org.springframework.data.mongodb.core.mapping.Document;
     
     import java.io.Serializable;
     import java.util.Date;
     
     /**
      * @author 华韵流风
      * @ClassName Spit
      * @Date 2021/9/22 20:13
      * @packageName com.tensquare.spit.pojo
      * @Description TODO
      */
     @Data
     @Document(collation = "spit")
     public class Spit implements Serializable {
         @Id
         private String _id;
         private String content;
         private Date publishtime;
         private String userid;
         private String nickname;
         private Integer visits;
         private Integer thumbup;
         private Integer share;
         private Integer comment;
         private String state;
         private String parentid;
     
     }
     ```

4. 创建 dao：

   - ```java
     package com.tensquare.spit.dao;
     
     import com.tensquare.spit.pojo.Spit;
     import org.springframework.data.mongodb.repository.MongoRepository;
     
     /**
      * @author 华韵流风
      * @ClassName SpitDao
      * @Date 2021/9/22 20:21
      * @packageName com.tensquare.spit.dao
      * @Description TODO
      */
     public interface SpitDao extends MongoRepository<Spit,String> {
     }
     ```

5. 创建 service。

   - ```java
     package com.tensquare.spit.service;
     
     import com.tensquare.spit.dao.SpitDao;
     import com.tensquare.spit.pojo.Spit;
     import com.tensquare.utils.IdWorker;
     import org.springframework.beans.factory.annotation.Autowired;
     import org.springframework.data.domain.Page;
     import org.springframework.data.domain.PageRequest;
     import org.springframework.data.mongodb.core.MongoTemplate;
     import org.springframework.data.mongodb.core.query.Criteria;
     import org.springframework.data.mongodb.core.query.Query;
     import org.springframework.data.mongodb.core.query.Update;
     import org.springframework.stereotype.Service;
     import org.springframework.transaction.annotation.Transactional;
     
     import java.util.List;
     
     /**
      * @author 华韵流风
      * @ClassName SpitService
      * @Date 2021/9/22 20:24
      * @packageName com.tensquare.spit.service
      * @Description TODO
      */
     @Service
     @Transactional(rollbackFor = Exception.class)
     public class SpitService {
     
         @Autowired
         private SpitDao spitDao;
     
         @Autowired
         private IdWorker idWorker;
     
         @Autowired
         private MongoTemplate mongoTemplate;
     
         /**
          * 添加spit
          *
          * @param spit spit
          */
         public void add(Spit spit){
             spit.setVisits(0);
             spit.setShare(0);
             spit.setThumbup(0);
             spit.setComment(0);
             spit.set_id(String.valueOf(idWorker.nextId()));
             spitDao.save(spit);
         }
     
         /**
          * 修改spit
          *
          * @param id id
          * @param spit spit
          */
         public void update(String id,Spit spit){
             spit.set_id(id);
             spitDao.save(spit);
         }
     
         /**
          * 删除spit
          *
          * @param id id
          */
         public void deleteById(String id){
             spitDao.deleteById(id);
         }
     
         /**
          * 查询所有spit
          *
          * @return List<Spit>
          */
         public List<Spit> findAll(){
             return spitDao.findAll();
         }
     
         /**
          * 根据id查询
          *
          * @param id id
          * @return Spit
          */
         public Spit findById(String id){
             return spitDao.findById(id).get();
         }
     
         /**
          * 根据上级ID查询吐槽数据（分页）
          *
          * @param parentId pagentId
          * @param page page
          * @param size size
          * @return Page<Spit>
          */
         public Page<Spit> findByParentId(String parentId, int page, int size){
             return spitDao.findByParentid(parentId, PageRequest.of(page-1, size));
         }
     
         /**
          * 吐槽点赞
          *
          * @param spitId spitId
          */
         public void thumbUp(String spitId){
             //查询本id的记录，然后点赞数+1
             /*Spit spit = spitDao.findById(spitId).get();
             spit.setThumbup(spit.getThumbup()+1);
             spitDao.save(spit);*/
             Query query = new Query();
             query.addCriteria(Criteria.where("_id").is(spitId));
             Update update = new Update();
             update.inc("thumbup");
             mongoTemplate.updateFirst(query, update, Spit.class);
         }
     
     }
     ```

6. 创建 controller。

   - ```java
     package com.tensquare.spit.controller;
     
     import com.tensquare.entity.PageResult;
     import com.tensquare.entity.Result;
     import com.tensquare.entity.StatusCode;
     import com.tensquare.spit.pojo.Spit;
     import com.tensquare.spit.service.SpitService;
     import org.springframework.beans.factory.annotation.Autowired;
     import org.springframework.data.domain.Page;
     import org.springframework.data.redis.core.RedisTemplate;
     import org.springframework.web.bind.annotation.*;
     
     import javax.annotation.Resource;
     
     /**
      * @author 华韵流风
      * @ClassName SpitController
      * @Date 2021/9/22 20:20
      * @packageName com.tensquare.spit.controller
      * @Description TODO
      */
     @RestController
     @RequestMapping("/spit/spit")
     @CrossOrigin
     public class SpitController {
     
         @Autowired
         private SpitService spitService;
     
         @Resource
         private RedisTemplate<String, Boolean> redisTemplate;
     
         /**
          * 添加spit
          *
          * @param spit spit
          * @return Result
          */
         @PostMapping
         public Result add(@RequestBody Spit spit) {
             try {
                 spitService.add(spit);
                 return new Result(StatusCode.OK, true, "添加成功");
             } catch (Exception e) {
                 e.printStackTrace();
             }
             return new Result(StatusCode.FAILER, false, "添加失败");
         }
     
         /**
          * 修改spit
          *
          * @param spit   spit
          * @param spitId spitId
          * @return Result
          */
         @PutMapping("/{spitId}")
         public Result update(@PathVariable String spitId, @RequestBody Spit spit) {
             try {
                 spitService.update(spitId, spit);
                 return new Result(StatusCode.OK, true, "修改成功");
             } catch (Exception e) {
                 e.printStackTrace();
             }
             return new Result(StatusCode.FAILER, false, "修改失败");
         }
     
         /**
          * 查询所有spit
          *
          * @return Result
          */
         @GetMapping
         public Result findAll() {
             try {
                 return new Result(StatusCode.OK, true, "查询成功", spitService.findAll());
             } catch (Exception e) {
                 e.printStackTrace();
             }
             return new Result(StatusCode.FAILER, false, "查询失败");
         }
     
         /**
          * 根据id查询spit
          *
          * @param spitId spitId
          * @return Result
          */
         @GetMapping("/{spitId}")
         public Result findById(@PathVariable String spitId) {
             try {
                 return new Result(StatusCode.OK, true, "查询成功", spitService.findById(spitId));
             } catch (Exception e) {
                 e.printStackTrace();
             }
             return new Result(StatusCode.FAILER, false, "查询失败");
         }
     
         /**
          * 删除spit
          *
          * @param spitId spitId
          * @return Result
          */
         @DeleteMapping("/{spitId}")
         public Result remove(@PathVariable String spitId) {
             try {
                 spitService.deleteById(spitId);
                 return new Result(StatusCode.OK, true, "删除成功");
             } catch (Exception e) {
                 e.printStackTrace();
             }
             return new Result(StatusCode.FAILER, false, "删除失败");
         }
     
         /**
          * 根据上级ID查询吐槽数据（分页）
          *
          * @param parentid parentid
          * @param page     page
          * @param size     size
          * @return Result
          */
         @GetMapping("/comment/{parentid}/{page}/{size}")
         public Result findByParentId(@PathVariable String parentid, @PathVariable int page, @PathVariable int size) {
             try {
                 Page<Spit> pageList = spitService.findByParentId(parentid, page, size);
                 return new Result(StatusCode.OK, true, "查询成功", new PageResult<>(pageList.getTotalElements(), pageList.getContent()));
             } catch (Exception e) {
                 return new Result(StatusCode.FAILER, false, "查询失败");
             }
         }
     
         /**
          * 吐槽点赞
          *
          * @param spitId spitId
          * @return Result
          */
         @PutMapping("/thumbup/{spitId}")
         public Result thumbUp(@PathVariable String spitId) {
             try {
                 //不允许重复点赞，用户点赞后需要在redis中记录下来。
                 String username = "user";
                 if (redisTemplate.opsForValue().get("spit_thumbUp_" + username) != null) {
                     return new Result(StatusCode.FAILER, false, "重复点赞");
                 }
                 spitService.thumbUp(spitId);
                 redisTemplate.opsForValue().set("spit_thumbUp_" + username, true);
                 return new Result(StatusCode.OK, true, "点赞成功");
             } catch (Exception e) {
                 e.printStackTrace();
             }
             return new Result(StatusCode.FAILER, false, "点赞失败");
         }
     
     
     }
     ```