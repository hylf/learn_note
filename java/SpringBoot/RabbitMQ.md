### RabbitMQ

### 一、direct

1. pom 里添加依赖：

   - ```xml
     <?xml version="1.0" encoding="UTF-8"?>
     <project xmlns="http://maven.apache.org/POM/4.0.0"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
         <modelVersion>4.0.0</modelVersion>
     
         <groupId>org.example</groupId>
         <artifactId>rabbirmq_test</artifactId>
         <version>1.0-SNAPSHOT</version>
     
         <parent>
             <artifactId>spring-boot-starter-parent</artifactId>
             <groupId>org.springframework.boot</groupId>
             <version>2.4.1</version>
         </parent>
         <dependencies>
             <dependency>
                 <groupId>org.springframework.boot</groupId>
                 <artifactId>spring-boot-starter-amqp</artifactId>
             </dependency>
             <dependency>
                 <groupId>org.springframework.boot</groupId>
                 <artifactId>spring-boot-starter-test</artifactId>
             </dependency>
             <dependency>
                 <groupId>junit</groupId>
                 <artifactId>junit</artifactId>
                 <scope>test</scope>
             </dependency>
         </dependencies>
     
     </project>
     ```

2. 配置：

   - ```yml
     server:
       port: 8092
     spring:
       application:
         name: rabbitmq-test
       rabbitmq:
         host: 192.168.46.130
     ```

3. 生产者（一条消息只能使用一次）：

   - ```java
     package com.rabbitmq;
     
     import com.rabbitmq.test.RabbitMQApplication;
     import org.junit.Test;
     import org.junit.runner.RunWith;
     import org.springframework.amqp.rabbit.core.RabbitTemplate;
     import org.springframework.beans.factory.annotation.Autowired;
     import org.springframework.boot.test.context.SpringBootTest;
     import org.springframework.test.context.junit4.SpringRunner;
     
     /**
      * @author 华韵流风
      * @ClassName SendMessage
      * @Date 2021/9/29 16:15
      * @packageName com.rabbitmq
      * @Description TODO
      */
     @RunWith(SpringRunner.class)
     @SpringBootTest(classes = RabbitMQApplication.class)
     public class SendMessage {
     
         @Autowired
         private RabbitTemplate rabbitTemplate;
     
         @Test
         public void sendMsg() {
             rabbitTemplate.convertAndSend("que_dic", "direct的消息");
         }
     }
     ```

4. 消费者（可同时启动多个来观察负载均衡）：

   - ```java
     package com.rabbitmq.test.consumer;
     
     import org.springframework.amqp.rabbit.annotation.RabbitHandler;
     import org.springframework.amqp.rabbit.annotation.RabbitListener;
     import org.springframework.stereotype.Component;
     
     /**
      * @author 华韵流风
      * @ClassName Consumer
      * @Date 2021/9/29 16:18
      * @packageName com.rabbitmq.test.consumer
      * @Description TODO
      */
     @Component
     @RabbitListener(queues = "que_dic")
     public class Consumer {
     
         /**
          * 用来接收消息的方法
          *
          * @param msg msg
          */
         @RabbitHandler
         public void receiveMsg(String msg) {
             System.out.println("33333" + msg);
         }
     }
     ```

### 二、fanout

- ```java
  @Test
  public void sendFanoutMsg() {
      rabbitTemplate.convertAndSend("exchange_fanout","", "fanout的消息");
  }
  ```

- ```java
  package com.rabbitmq.test.consumer;
  
  import org.springframework.amqp.rabbit.annotation.RabbitHandler;
  import org.springframework.amqp.rabbit.annotation.RabbitListener;
  import org.springframework.stereotype.Component;
  
  /**
   * @author 华韵流风
   * @ClassName Consumer
   * @Date 2021/9/29 16:18
   * @packageName com.rabbitmq.test.consumer
   * @Description TODO
   */
  @Component
  @RabbitListener(queues = "que1")
  public class Consumer1 {
  
      /**
       * 用来接收消息的方法
       *
       * @param msg msg
       */
      @RabbitHandler
      public void receiveMsg(String msg) {
          System.out.println("que1" + msg);
      }
  }
  ```

- 适合发送通知类消息。

### 三、topic

- ```java
  package com.rabbitmq.test.consumer;
  
  import org.springframework.amqp.rabbit.annotation.RabbitHandler;
  import org.springframework.amqp.rabbit.annotation.RabbitListener;
  import org.springframework.stereotype.Component;
  
  /**
   * @author 华韵流风
   * @ClassName Consumer
   * @Date 2021/9/29 16:18
   * @packageName com.rabbitmq.test.consumer
   * @Description TODO
   */
  @Component
  @RabbitListener(queues = "que2")
  public class Consumer2 {
  
      /**
       * 用来接收消息的方法
       *
       * @param msg msg
       */
      @RabbitHandler
      public void receiveMsg(String msg) {
          System.out.println("que2" + msg);
      }
  }
  ```

- ```java
  package com.rabbitmq.test.consumer;
  
  import org.springframework.amqp.rabbit.annotation.RabbitHandler;
  import org.springframework.amqp.rabbit.annotation.RabbitListener;
  import org.springframework.stereotype.Component;
  
  /**
   * @author 华韵流风
   * @ClassName Consumer
   * @Date 2021/9/29 16:18
   * @packageName com.rabbitmq.test.consumer
   * @Description TODO
   */
  @Component
  @RabbitListener(queues = "que3")
  public class Consumer3 {
  
      /**
       * 用来接收消息的方法
       *
       * @param msg msg
       */
      @RabbitHandler
      public void receiveMsg(String msg) {
          System.out.println("que3" + msg);
      }
  }
  ```

- ```java
  @Test
  public void sendTopic1Msg() {
      rabbitTemplate.convertAndSend("exchange_topic","good.abc", "good.#的消息");
  }
  
  @Test
  public void sendTopic2Msg() {
      rabbitTemplate.convertAndSend("exchange_topic","abc.log", "abc.log.#的消息");
  }
  
  @Test
  public void sendTopic3Msg() {
      rabbitTemplate.convertAndSend("exchange_topic","good.log", "good.log的消息");
  }
  ```

