### 一、了解 docker

- 在一个微服务项目中，所有的微服务分为两大类，一类是业务微服务比如登录，文章管理等，另一类是系统微服务，比如 mysql，mongodb 等。多个微服务它们各自需要的运行环境可能不同，这给服务的部署和运行造成麻烦。另一方面， 所有的业务微服务都要经过开发，测试及上线运行三个步骤，这三步的运行环境也可能是不一样的。以上问题会给测试及运维方面造成困扰。

1. 什么是 docker

   - Docker 是一个开源的应用容器引擎，诞生于2013 年初，基于 Go 语言实现，dotCloud 公司出品（后改名为 Docker Inc）。
   - Docker 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上。
   - 容器是完全使用沙箱机制，相互隔离。
   - 容器性能开销极低。
   - Docker 17. 03 版本之后分为 CE（Community Edition: 社区版）和 EE (Enterprise Edition: 企业版)。

2. 安装 docker

   - docker 本身是一个制作镜像和容器的执行环境。

   - 自动安装（二选一）：

     - ```shell
       curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
       ```

     - ```shell
       curl -sSL https://get.daocloud.io/docker | sh
       ```

   - 手动安装（建议）：

     - ```shell
       yum install -y yum-utils
       yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
       yum install docker-ce
       ```

3. 启动 docker（其他略）：

   - ```shell
     systemctl start docker
     ```

4. 下载镜像：docker pull xxx

5. 查看镜像：docker images

6. 创建容器：

   - ```shell
     #创建容器
     docker run -di --name=tensquare_mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 centos/mysql-57-centos7
     #查询所有的容器，包括未执行的
     docker ps -a
     #停止容器(id或名称，下同)
     docker stop xxx
     #启动容器
     docker start xxx
     #删除容器
     docker rm xxx
     ```

