### 一、node.js

- 基于 chrom v8 的一个 js 的执行引擎（运行环境）。可以让 js 的程序在后台执行，也就是 js 具有类似于 java 的功能。命令 node 类似于 java 的 java 命令。node 命令用来执行后台的 js 程序。

### 二、了解 js 的后台程序的编写与运行

1. 控制台输出：

   - ```javascript
     let a = 1;
     let b = 2;
     let c = add(a, b);
     console.log("c=" + c);
     
     function add(x, y) {
         return x + y;
     }
     ```

   - 使用 node 文件名 来运行。

2. 模块化编程，需要达到类似于 java 按照类的方式来编程。

   - 主要遇到的关键字：exports 用来公开指定的内容；require 用来引用指定的 文件。

   - ```javascript
     //example2.js
     exports.add = function (x, y) {
         return x + y;
     }
     ```

   - ```javascript
     //example3.js
     let example2 = require("./example2");
     console.log(example2.add(1,2));
     ```

3. 创建 web 服务器

   - 也就是创建一个 web 应用程序，可以通过 http 协议进行访问，类似于发布到 tomcat 上的一份应用。

   - ```javascript
     //加载http模块功能
     let http = require("http");
     http.createServer(function (request, response) {
         //处理请求的代码
         response.writeHead(200,{'Content-Type':'text/plain'});
         response.end("Hello Word!");
     }).listen(8089);
     
     console.log('server running at http://127.0.0.1:8089');
     ```

4. 服务器渲染

   - 我们原来的设计都属于前端渲染，也就是利用浏览器的内核执行页面上的 js 的代码，把执行的结果在页面上呈现出来。

   - 现在使用 node 环境，执行 js 代码的位置发生了变化，当 js 代码在服务器上执行后其结果会通过网络直接输出到客户的页面上。

   - ```javascript
     //加载http模块功能
     let http = require("http");
     http.createServer(function (request, response) {
         //处理请求的代码
         response.writeHead(200, {'Content-Type': 'text/plain'});
         for (let i = 0; i < 10; i++) {
             response.write("i=" + i + "\n");
         }
         response.end("end!");
     }).listen(8089);
     
     console.log('server running at http://127.0.0.1:8089');
     ```

5. 接收请求参数

   - ```javascript
     //加载http模块功能
     let http = require("http");
     let url = require("url");
     http.createServer(function (request, response) {
         //处理请求的代码
         response.writeHead(200, {'Content-Type': 'text/plain'});
         //query就是?后的参数键值对
         let params = url.parse(request.url,true).query;
         response.write("name="+params.name);
         response.end();
     }).listen(8089);
     
     console.log('server running at http://127.0.0.1:8089');
     ```

### 三、如何搭建 Vue 脚手架项目

- 使用 vue 搭建有两种情况：多数前台是多页面的；多数后台是单页面的。

- 使用命令 npm install -g vue-cli 安装 vue-cli 工具（使用 vue -V 出现版本号表示安装完成）。
- 选择一个空的目录作为项目的根目录，在该目录下执行 vue init webpack。
- 上一步完成后执行 npm run dev 即可查看结果。

### 四、熟悉项目的前端系统

- '~'（波浪符号）：会更新到当前 minor version（也就是中间的那位数字，小版本）中最新的版本。例如："exif-js": "~2.3.0"，这个库会去匹配更新到2.3.x的最新版本，如果出了一个新的版本为2.4.0，则不会自动升级。波浪符号是曾经npm安装时候的默认符号，现在已经变为了插入符号。
- '^'（插入符号）：会把当前库的版本更新到当前 major version（也就是第一位数字，主要版本）中最新的版本。例如："vue": "^2.2.2", 这个库会去匹配2.x.x中最新的版本，但是他不会自动更新到3.0.0。
- 不写 ‘~’ 或者 ‘^’ 就是固定版本号。

- 后台项目的前端系统

  - package.json 文件相当于 maven 的 pom.xml 文件，主要用来设置依赖（node 平台的各种包）

  - ```json
  //程序运行时的依赖
    "dependencies": {
      "axios": "0.17.1",
      "element-ui": "2.0.8",
      "js-cookie": "2.2.0",
      "normalize.css": "7.0.0",
      "nprogress": "0.2.0",
      "vue": "2.5.10",
      "vue-router": "3.0.1",
      "vuex": "3.0.1"
    }
    //"devDependencies"是开发时的依赖
    ```
  
  - 执行 npm install

  - 执行 npm run dev

- 前台项目的前端系统

  - ```json
    "dependencies": {
      "axios": "^0.18.0",
      "element-ui": "^2.3.9",
      "js-cookie": "^2.2.0",
      "nuxt": "^1.0.0",
      "vue-infinite-scroll": "^2.0.2",
      "vue-quill-editor": "^3.0.6",
      "vue-quill-editor-upload": "^1.1.0"
    }
    ```

  - 执行 npm install

  - 执行 npm run dev

