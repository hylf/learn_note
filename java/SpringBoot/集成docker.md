### 一、消息总线

- 基于集中配置的系统中，当配置的内容发生改变时，相应的服务会得到改变的消息。并主动的去读取最新的配置内容。spring cloud 提供了消息总线。

- 针对配置服务器添加依赖

  - ```xml
    <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-bus</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-stream-binder-rabbit</artifactId>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-actuator</artifactId>
            </dependency>
    ```

  - 配置：

    - ```yml
      management: #暴露触发消息总线的地址
        endpoints:
          web:
            exposure:
              include: bus-refresh
      ```

- 针对配置客户端添加相同依赖。

- 修改码云上的配置，添加 rabbitMQ 的地址。

- 添加自定义配置需要在控制器上添加注解 @RefreshScope，此注解用于刷新配置。

- post 请求 http://localhost:12000/actuator/bus-refresh 可以刷新配置而不用重启服务器。

  - ```java
    package com.zhong.provider.controller;
    
    import com.zhong.provider.pojo.User;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.beans.factory.annotation.Value;
    import org.springframework.cloud.context.config.annotation.RefreshScope;
    import org.springframework.web.bind.annotation.GetMapping;
    import org.springframework.web.bind.annotation.RequestMapping;
    import org.springframework.web.bind.annotation.RequestParam;
    import org.springframework.web.bind.annotation.RestController;
    
    import javax.servlet.http.HttpServletRequest;
    
    /**
     * @author 华韵流风
     * @ClassName ProviderController
     * @Date 2021/10/15 15:54
     * @packageName com.zhong.provider.controller
     * @Description TODO
     */
    @RestController
    @RequestMapping("/provider")
    @RefreshScope//此注解用于刷新配置
    public class ProviderController {
    
        @Value("${info}")
        private String info;
    
        @GetMapping("/user")
        public User user(@RequestParam String id) {
            if ("1".equals(id)) {
                throw new RuntimeException();
            }
            User user = new User();
            user.setId(id);
            user.setName(info);
            return user;
        }
    
    }
    ```

### 二、使用 dockerfile 的命令创建 docker 镜像。

1. 在 linux 下创建一个生成镜像的目录：

   - mkdir -p /usr/local/dockerjdk8。

2. 在目录下创建 Dockerfile 文件，添加配置：

   - ```shell
     #依赖镜像名称和ID
     FROM centos:7
     #指定镜像创建者信息
     MAINTAINER huayunliufeng
     #切换工作目录
     WORKDIR /usr
     RUN mkdir /usr/local/java
     #ADD 是相对路径jar,把java添加到容器中
     ADD jdk-8u171-linux-x64.tar.gz /usr/local/java/
     #配置java环境变量
     ENV JAVA_HOME /usr/local/java/jdk1.8.0_171
     ENV JRE_HOME $JAVA_HOME/jre
     ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib:$CLASSPATH
     ENV PATH $JAVA_HOME/bin:$PATH
     ```

   - 执行命令构建镜像（注意空格和点，jdk1.8是名称，可以修改）：

     - docker build ‐t='jdk1.8' .

   - 创建容器：

     - docker run ‐it ‐‐name=myjdk8 jdk1.8 /bin/bash

3. 私有仓库

   - 拉取私有仓库：docker pull registry
   - 启动私有仓库：
     - docker run ‐di ‐‐name=registry ‐p 5000:5000 registry
     - docker update registry --restart=always
   - 查看私有仓库：
     - <http://192.168.46.130:5000/v2/_catalog>
   - 修改 daemon.json：vi /etc/docker/daemon.json 并添加以下内容，注意是在原有的那个大括号里面添加。
     - {"insecure‐registries":["192.168.46.130:5000"]}
   - 重启 docker：systemctl restart docker

4. 镜像上传至私有仓库：

   - 标记此镜像为私有仓库的镜像：
     - docker tag jdk1.8 192.168.46.130:5000/jdk1.8
   - 再次重启私服容器：docker restart registry
   - 上传标记的镜像：
     - docker push 192.168.46.130:5000/jdk1.8

5. DockerMaven 插件

   - 修改宿主机的 docker 配置，让其可以远程访问。

     - vi /lib/systemd/system/docker.service
     - 其中ExecStart=后添加配置 ‐H tcp://0.0.0.0:2375 ‐H unix:///var/run/docker.sock

   - 刷新配置，重启服务：

     - ```shell
       systemctl daemon‐reload
       systemctl restart docker
       docker start registry
       ```

   - 在 eureka 微服务中的 pom.xml 文件添加配置：

     - ```xml
       <build>
               <finalName>app</finalName>
               <plugins>
                   <plugin>
                       <groupId>org.springframework.boot</groupId>
                       <artifactId>spring-boot-maven-plugin</artifactId>
                       <version>2.1.7.RELEASE</version>
                   </plugin>
                   <!--   docker的maven插件，官网https://github.com/spotify/docker‐maven‐plugin -->
                   <plugin>
                       <groupId>com.spotify</groupId>
                       <artifactId>docker-maven-plugin</artifactId>
                       <version>0.4.13</version>
                       <configuration>
                           <imageName>192.168.46.130:5000/${project.artifactId}:${project.version}</imageName>
                           <baseImage>jdk1.8</baseImage>
                           <entryPoint>["java","‐jar","/${project.build.finalName}.jar"]</entryPoint>
                           <resources>
                               <resource>
                                   <targetPath>/</targetPath>
                                   <directory>${project.build.directory}</directory>
                                   <include>${project.build.finalName}.jar</include>
                               </resource>
                           </resources>
                           <dockerHost>http://192.168.46.130:2375</dockerHost>
                       </configuration>
                   </plugin>
               </plugins>
           </build>
       ```
       
     
   - 在需要打包的工程目录下执行命令：

     - mvn clean package docker:build -DpushImage

### 三、持续集成

- 采用自动化的方式，把已完成的项目的代码生成 docker 镜像的过程。
- goes：代码仓库，可以使用 git 来上传代码；
- jinkins：可完成持续集成的工具；

### 四、docker 环境的管理和监控

- kubernetes，简称 k8s。



