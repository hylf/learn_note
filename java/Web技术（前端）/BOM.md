### 一、什么是 BOM ?

- BOM（browser object model，浏览器对象模型）。js 提供了一些对象来表示浏览器的窗口以及浏览器窗口中的其它的对象，这些对象都有相应的方法，属性以及事件。

### 二、BOM 的结构

- window 对象，表示浏览器窗口。
- document 文档，DOM 与它相关。
- history 表示浏览器的浏览历史。
-  location 表示当前文档的 URL。
- navigator 表示与浏览器相关的一些内容。
- screen 表示呈现浏览器窗口的屏幕。
- status 状态条

### 三、document 对象

- 它表示一份完整的文档（网页）内容，它的方法，属性及事件很多，在 js 中，把 document 当做一块比较独立的内容来进行设计。这一块归纳为 DOM（文档对象模型）。学完 BOM 后的重点就是 DOM。

### 四、history 对象

- 表示浏览器展示网页的历史过程，浏览器会按照浏览网页的先后顺序，把这些构成一条按时间排列的链条。history 对象就是用来表示这个链条的。它提供的方法有两个，一个是 back()后退，另一个就是 forward() 前进方法。
- 我们在设计 web 项目时，如果当前的页面提交数据失败，我们可以利用 history 对象让浏览器回退到问题页面。

#### 五、location 对象

1. 使用 reload() 方法可以刷新当前网页。<a href="javascript:location.reload()">刷新网页</a>
2. location.href 属性，给该属性赋值也就是给它一个 URL，就可以让浏览器展示 URL 的内容。这种改变当前网页的方式在 js 编程中用的比较多，在 web 程序中，经常会使用该属性来动态的实现网页的跳转。<a href="javascript:location.href = 'next.html'">页面跳转</a>

### 六、window 对象

- window 对象是以上对象的父对象，以上的对象都包含在 window 中，在实际应用中，一般前面不带 window.

1. 信息弹框：alert()

2. 确认框：confirm()   //确认返回 true，取消返回 false

3. 输入框：prompt()   //按确认返回填写的字符串，按取消返回 null

4. 计时器方法：setTimeout(函数名，毫秒值（时长）)，该方法在指定的时刻（本方法执行的时间+时长）会调用该函数一次。（clearTimeout()清除计时器）

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <script src="../js/jquery-3.6.0.js"></script>
       <script>
           //计时器
           let time;
           beginTime = () => {
               let date = new Date();//返回当前的日期时间值，精确到毫秒。
               let dateStr = date.getFullYear() + '年' + (date.getMonth() + 1) + '月' + date.getDate() + '日  ' + date.getHours() + '时' + date.getMinutes() + '分' + date.getSeconds() + '秒' + date.getMilliseconds() + '毫秒';
               $('#time').val(dateStr);
               //document.getElementById('time').value = dateStr;
               time = setTimeout('beginTime()', 1);
           };
   
           endTime = () => {
               clearTimeout(time);
           };
   
       </script>
   </head>
   <body>
   <input type="text" id="time" size="40"/>
   <button onclick="beginTime()">开始计时</button>
   <button onclick="endTime()">停止计时</button>
   </body>
   </html>
   ```

   - 另一个计时器方法：setInterval(函数名，毫秒值（时长）)经过指定时长后，会反复调用函数。除非强制终止。clearInterval()

     ```javascript
     beginTime = () => {
                 //setInterval
                 time = setInterval(() => {
                     let date = new Date();//返回当前的日期时间值，精确到毫秒。
                     let dateStr = date.getFullYear() + '年' + (date.getMonth() + 1) + '月' + date.getDate() + '日  ' + date.getHours() + '时' + date.getMinutes() + '分' + date.getSeconds() + '秒' + date.getMilliseconds() + '毫秒';
                     $('#time').val(dateStr);
                 }, 1);
     
             };
     
     endTime = () => {
                 //clearTimeout(time);
                 clearInterval(time)
             };
     ```

