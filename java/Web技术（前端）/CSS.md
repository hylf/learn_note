### 一、CSS 级联样式表，层叠样式表

- 作用是美化网页，实现排版。

### 二、样式表的学习内容

1. 规则选择器
2. 关于文字的样式
3. 关于背景的样式
4. 关于对齐的样式
5. 边框的样式
6. 定位样式
7. 浮动样式

### 三、规则选择器

1. 样式的基本语法：规则选择器 { 属性名：属性值;…… }

2. 规则选择器的作用

   - 在样式设计时，可以使用它来选择需要被样式表修饰的 html 元素。

3. 了解常用的选择器

   - id 选择器，所有元素都有 id 属性，id 具有唯一性，通过它可以选择唯一的一个元素。#id

   - class 选择器，又称为类选择器，所有的元素都有 class 属性，如果不同的元素具有相同的样式，这些元素就可以指定 class 属性，针对该属性值指定样式。.class

   - 标签选择器，用 html 的标签名称作为选择器。页面中的指定的标签都适用该样式。p h2 img

   - 通配符选择器，用来选择所有元素。*

   - 并列规则选择器，把多种选择器用逗号分隔，相关的所有元素都适用指定的样式。

   - 父子规则选择器，对于具有嵌套关系的元素，这些元素具有父子关系，可以通过父元素来指定子元素的样式

     1. 父 子{ }，所有的子元素都适用。
     2. 父 > 子{ }下一级子元素适用。

   - 伪类选择器，给某些元素指定一些特别的样式。

     - 比如：<a>标签有四种状态，1、未被点击：link，2、被点击过：visited，3、鼠标悬停：hover，4、鼠标按下未放开：active
     - 可以针对以上四种状态分别指定样式。书写顺序是：l v h a
     - ::selection{} 被选中元素的样式
     - ::first-line 第一行的样式
     - ::first-child 第一个子元素的样式

   - 属性选择器

     - 利用元素的属性名或属性值来选择元素。

       ```html
       <input name="username" /> [name]{color: red;}
       ```

### 四、样式表的三种语法

1. 页内样式表，在head中 用style标签，把样式写在标签内部。所写的样式适用于本页面。
2. 元素样式，在元素中使用style属性，给当前的元素指定样式。<input style="color: yellow;" name="username" />
3. 外部文件样式表，创建一份样式文件， 把样式写在文件中，需要使用的页面通过link来引用外部文件。优先使用，可以让多份页面使用文件中的公共样式。

### 五、选择器的优先级

- 当多种选择器都选中了同一个元素，并且样式属性也相同值不同。
- 规则一、离元素越近的越优先。
- 规则二、越具体优先级越高。

### 六、关于文本的样式

- ```css
  font-size: 13px;/*13或者14是最常用的字体大小*/
  ```

- ```css
  font-family: "Arial Black";/*指定字体*/
  ```

- ```css
  font-style: italic;/*字体样式，加粗、斜体等*/
  ```

- ```css
  color: rgba(0, 0, 0, 0.5);/*字体颜色，rgb(a)或者#或者lsv()*/
  ```

- ```css
  line-height: 13px;/*指定一行文字的高度，也可以用它来调节容器中文字的居中对齐*/
  ```

### 七、背景样式

- ```css
  background-color: #b4bdff;/*元素的背景色*/
  ```

- ```css
  background-image: url("../img/1.jpg");/*背景图片，默认情况下图片会向x和y方向重复铺陈，*/
  ```

- ```css
  background-repeat-y: no-repeat;/*控制图片的铺陈方向*/
  ```

- ```css
  background-position: left center;/*控制图片位置*/
  ```

- ```css
  background-attachment: scroll;/*控制图片是否随滚动条滚动*/
  ```

### 八、对齐样式

- ```css
  text-align: center;/*水平对齐样式*/
  ```

- ```css
  vertical-align: bottom;/*垂直对齐样式*/
  ```

### 九、大小样式

- 高度 height
- 宽度 width
- 最大（小）高度 max-height min-height（响应式布局）
- 最大（小）宽度 max-width min-width

### 十、框模型

- 又称盒子模型。在网页中的所有元素都是框。
- 框模型提供了对元素的边框，内外边距的设置。绘制框线，可以针对四条边框分别进行绘制，有三个参数分别是粗细，颜色，线型。

### 十一、定位

- 采用样式设置放在它应该出现的位置

1. 网页中的元素的自然排列方式（普通流的方式）
   - 网页中的元素按照书写的顺序先后出现，元素越是写在前面，就越会出现在网页的上面或左边。
   - 块级元素，会独占一行。<ul><table><div><p>等。
   - 行内元素，不会独占一行，行内元素可以与其他的元素在同一行出现。<img><a><td><span>等。
2. 样式表中所提供的定位方式
   - 绝对定位（absolute），会让出它在普通流中的位置。相对于父元素进行定位。如果所有祖先元素都没有定位就以当前屏幕进行定位。
   - 相对定位（relative），相对元素本身的位置进行定位，相对定位不脱离自己原来的文档流，移动的位置是以自己左上角为基点来移动的
   - 固定定位（fixed），相对于浏览器窗口进行定位，脱离原来的文档流。
   - z-index 定位，需要设置定位属性，设置元素的堆叠顺序。拥有更高堆叠顺序的元素总是会处于堆叠顺序较低的元素的前面。
   - 浮动（float），浮动的元素会让出普通流的位置，块级元素被浮动后，它们可以在同一行出现。元素在向两个方向靠近时，如果遇到阻碍就会停止。
   - 元素的显示和隐藏
     - display：none 元素会隐藏，原有位置不保留。
     - display：block 元素会显示。（也可以把行内元素转关为块级元素）
     - display：inline 把块级元素转换为行内元素。