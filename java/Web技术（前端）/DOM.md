### 一、什么是 DOM？

- DOM 是 window 下的一个对象，名称是 document object model（文档对象模型）；DOM 是 w3c 的一个标准，基于此标准有三种不同的具体体现，分别是核心 DOM，XML 的 DOM，HTML 的 DOM。

### 二、HTML DOM 的作用

- 通过 DOM 可以访问网页中的所有元素，也可以操作网页中的所有元素。在实际的项目设计中，大多数情况下都是利用网页作为系统的前端，甚至手机的应用。作为一个前端界面来看，它应该能够结合用户的操作或者数据的变化让界面的结构或内容能够发生改变，类似这样的效果称为动态效果，它们都是由基于 DOM 的操作来实现。

### 三、DOM 的结构

- 在一份网页中，DOM 是一种树形的结构，元素可以有子元素，也可以有父元素，也可以有兄弟元素，元素还可以包含属性，元素还可以包含文字，网页中还可以有注释。上面的这些都可以用 DOM 树的形式来进行表达。

### 四、document 对象

- 它是 DOM 最核心的对象，它包含了网页中的所有内容，我们可以通过该对象来访问网页中的所有节点（元素对象）。
- document.all 该属性可以返回网页中所有的元素节点（有<>标签）。
- 网页中的节点分为几种：
  1. 元素节点，比如 <a><div>等，它们的类型取值为1。
  2. 文字节点，比如<a>跳转</a>中的文字。类型取值为3.
  3. 文档节点，文档特指 document。
  4. 属性节点，元素节点的属性。
  5. 注释节点，页面上的注释。

### 五、在页面中导航

- 我们可以在页面中任意选取一个元素节点，利用节点之间的关系属性，可以到达其它任意节点的位置。

- 节点的关系属性有这样几种：

  1. parentNode 表示当前节点的父节点，任何元素最多只有一个父节点。
  2. childNodes，表示一个节点的所有子节点，可以有多个。
  3. firstChild，表示第一个子节点。
  4. lastChild，表示最后一个子节点。
  5. previousSibling（上一个兄弟节点），nextSibling（下一个兄弟节点）。

  例程：给页面中任意一个元素，输出它所有的子元素，采用递归算法遍历元素：

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <script>
          test = (node) => {
              //通过document对象得到页面中所有元素
              /*let elements = document.all;
              for (let i = 0; i < elements.length; i++) {
                  console.log(elements[i].nodeName + "  " + elements[i].nodeType)
              elements[i].nextSibling
               }*/
              console.log(node.nodeName + "  " + node.nodeType);
              let childs = node.childNodes;
              if (childs.length > 0)//遍历节点
                  childs.forEach((ele) => {
                      if (ele.childNodes.length !== 0) test(ele);
                  });
          };
      </script>
  </head>
  <body>
  <h2>这是标题</h2>
  <ul id="ul">
      <li>清单1</li>
      <li>清单2</li>
      <li>清单3</li>
  </ul>
  <button onclick="test(document.body)">测试</button>
  </body>
  </html>
  ```

### 六、如何访问网页中的元素

1. 通过页面导航，可以得到与当前元素有关系的其他的元素节点。
2. 通过 document 的多种属性也可以得到相关的元素节点，比如 document.body，document.forms，document.head 等。
3. 通过 document 的多种方法来得到元素节点。
   - document.getElementById('')，得到指定 id 的元素节点，返回一个唯一的节点。
   - document.getElementsByName('')，得到指定 name 属性值的一组元素，返回类似于数组的 HTML Collcetions 集合（只读）。
   - document.getElementsByTagName('')，得到指定标签名的所有元素。
   - document.getElementsByClassName('')，得到指定 class 属性值的一组元素。
4. 通过 innerText 属性得到元素包含的文字内容。
5. 通过 innerHTML 属性得到元素包含的 HTML 的内容。
   - 注意：以上两个属性是有区别的，一个表示纯文字，一个表示标签。

### 七、如何操作网页中的元素

1. 设置元素的属性：node.属性名 = ‘属性值’

2. 设置元素的样式：node.style.样式名 = ‘样式值’，如果样式名由多个单词组成比如 background-color，去掉减号，后面单词大写。

   - 利用以上两种方式，可以改变任意元素的任何属性和任何样式，要学会举一反三。

3. 创建元素：document.createElement(‘标签名称’)；

4. 创建文字节点：document.createTextNode('内容')

5. 向网页中添加元素：添加操作需要知道父节点是谁，把需要添加的元素作为子节点交给父节点。

   ```js
   let liobj = document.createElement('li');
   let textNode = document.createTextNode('清单4');
   liobj.appendChild(textNode);
   document.getElementById('ul').appendChild(liobj);
   ```

6. 删除网页中的元素：删除操作也需要知道被删除元素的父节点，把需要删除的元素传给父节点。

   ```js
   let ele = document.getElementById('ul');
   let lastLi = ele.lastChild;
   ele.removeChild(lastLi);
   ```

