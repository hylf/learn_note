### 一、什么是 javascript ?

- 97 年正式投入使用，它的官方名称是 ECMASCRIPT。它是基于对象的一种程序开发语言，具有安全性的特点；基于对象是指该语言既有面向过程的特点，又有面向对象的特点。面向过程的设计方式会把每个功能定义为一个函数，函数之间可以相互调用，事件也可以调用函数。面向对象这方面， js 本身就提供了多种内置对象供程序使用，在程序中也可以创建自定义的对象，也可以基于对象来设置属性及调用方法。

### 二、javascript 语言的应用领域

1. 它的原始目标就是能够基于浏览器运行，让网页能够与用户多一些交互的功能。它是作为一门脚本语言而出世，在现在这种方式依然很流行。
2. 前五年左右，js 为了能够抢占更多的市场份额，就是能够侵入后台程序的开发，到现在为止，成功了。主要在于 node.js 平台的出现。

### 三、如何使用 js 来实现前端的程序

1. js 程序与网页中的 html 标签，css 的样式结合在一起，它们如何工作？方式有三种。
   - 第一种，在网页中用<script>标签，程序就写在标签内部，这种只能在本页面使用。
   - 第二种，创建外部文件，文件扩展名.js，表示该文件中是 js 的程序，可以在任何网页中使用。
   - 第三种，在元素的事件属性中写 js 代码（包含函数的调用）。

### 四、js 的基本语法及语句，运算符，关键字等

1. 变量的声明，js 是弱类型，因此在声明变量时不需要指定数据类型，命名规则参考 java。要求变量前使用 var 关键字。ECMA6 的版本之前变量声明只有 var 关键字，没有其他关键字。当前的版本新增两个声明变量关键字，分别是：
   - let 把变量声明为代码块中的变量，var 声明的是全局变量。
   - const 关键字，视同 java 的 final，带有 const 声明的变量就是常量。
2. 关于 js 中的数据类型
   - 简单类型共三种：string，number，boolean。
   - 对象类型：数组（var ary = new Array();），日期，自定义对象，null。
   - undefined：变量未指定明确值。
3. 运算符：与 java 几乎一样，逻辑运算符中没有非短路。
4. 语句：
   - 条件语句，与 java 一样，也包含 switch。
   - 循环语句，与 java 几乎一样，js for(变量 in 对象或数组)
   - 注释，与 java 一样。
5. 关键字，java 有53种，js 的关键字 java 都有。

### 五、全局变量和局部变量

- 在面向过程的设计方式中，变量的声明可以在函数的内部，也可以在函数的外部。
- 全局变量是声明在函数外面的变量，作用范围是从声明的位置开始整个程序中，em6 中认为用 var 声明的变量就是全局变量。
- 在函数内声明的变量是局部变量，作用范围是本函数内部。

### 六、js 的自定义函数

- y = f(x) 是函数的一般表示，js 函数可以包含三个部分，一个是参数，一个是函数体，还有函数名。返回值不是必须的，如果有返回值直接用 return 返回结果。
- 函数必须先定义才能调用，函数不被调用就不会执行。
- 函数定义的语法：function 函数名（参数列表）{函数体}，参数可有可无，返回值不需要声明。
- 按照常规的三种用法，有参有返回值；有参无返回值；无参无返回值；无参有返回值。

### 七、js 常用的内置函数

- 内置函数是由 js 的设计者在本语言中所定义的可以直接使用的函数。

### 八、js 中的事件

1. 事件就是用户或网页自身所做的一些操作，比如用户鼠标的点击和滑动，悬停，按键等。网页的加载也有相应的事件。
2. 事件与 js 的关系，js 是事件驱动的语言，事件是 js 的基本特征之一。
3. js 具有事件机制，它体现在有事件源，有事件监听者，还有事件执行程序。
4. 与用户的具体操作有直接关系的就是网页上的各种元素，因此大多数元素都有自己的事件。
5. 事件穿透
   - 比如一个 div，里面有一个按钮，这两个元素都注册了点击事件程序，当用户点击了按钮，这两个元素上的事件程序都会执行。js 认为这是叫事件传递，这是合理的。jquery 出来后，针对此问题提出了合理和简便的解决方式。
   - 在写事件函数时，为了不让事件冒泡的现象发生，需要终止事件对象的传递。在函数的最后调用 event.stopPropagation();另外，函数上需要传入 event 对象。

### 九、常用事件及编程

1. 网页的加载事件

   - 当网页被加载或刷新时，会触发加载事件，事件属性为 onload；一般用来做一些初始化的工作，比如给一些变量做初始化的赋值。事件的执行时机，在网页元素被初始化之前。

2. 元素的点击事件

   - 这是最常用的事件，大多数元素都有该事件，点击也是使用网页过程中最多的一种操作。
   - 事件属性是：onclick。
   - 用程序随机的生成双色球的号码。
   - 红色球6个，蓝色球1个，红色号码从1-33，蓝色是1-16，每个球的号码不能重复。

   （1）界面，有7个文本框来显示号码，1个按钮来生成一组号码。

   （2）程序，当点击按钮生成随机7个数字，分别写到各文本框中。

   （3）其业务逻辑主要使用随机数函数生成不重复的数字

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <script src="../js/jquery-3.6.0.js"></script>
       <style>
           .c1 {
               color: #f00;
           }
   
           .c1 input {
               border: 1px #f00 solid;
           }
   
           .c2 {
               color: #00f;
           }
   
           .c2 input {
               border: 1px #00f solid;
           }
   
           input {
               text-align: center;
           }
   
       </style>
       <script>
           //生成7个号码并写到文本框中
           const tmp = new Array(34);
   
           const f = () => {
               for (let i = 1; i < tmp.length; i++) {
                   tmp[i] = false;
               }
   
               $('#p1').val(getRandom(1, 33));
               $('#p2').val(getRandom(1, 33));
               $('#p3').val(getRandom(1, 33));
               $('#p4').val(getRandom(1, 33));
               $('#p5').val(getRandom(1, 33));
               $('#p6').val(getRandom(1, 33));
               $('#p7').val(getRandom(1, 16));
   
           }
   
           const getRandom = (m, n) => {
               let num = m + Math.ceil(Math.random() * (n - m));
   
               while (tmp[num]) {
                   num = m + Math.ceil(Math.random() * (n - m));
               }
               tmp[num] = true;
               return num;
           };
       </script>
   </head>
   <body>
   <p class="c1"><label for="p1">1、</label><input type="text" id="p1" disabled='disabled'/></p>
   <p class="c1"><label for="p2">2、</label><input type="text" id="p2" disabled='disabled'/></p>
   <p class="c1"><label for="p3">3、</label><input type="text" id="p3" disabled='disabled'/></p>
   <p class="c1"><label for="p4">4、</label><input type="text" id="p4" disabled='disabled'/></p>
   <p class="c1"><label for="p5">5、</label><input type="text" id="p5" disabled='disabled'/></p>
   <p class="c1"><label for="p6">6、</label><input type="text" id="p6" disabled='disabled'/></p>
   <p class="c2"><label for="p7">7、</label><input type="text" id="p7" disabled='disabled'/></p>
   <button onclick="f()">生成号码</button>
   
   </body>
   </html>
   ```

3. 获得和丢失焦点事件

   - 一些表单控件可以输入或选择数据，当控件当前处于可输入数据的状态时，那么控件具有焦点，反之控件失去焦点。

   - 得到焦点的属性是：onfocus 失去焦点的属性是：onblur

     ```html
     <!DOCTYPE html>
     <html lang="en">
     <head>
         <meta charset="UTF-8">
         <title>Title</title>
         <script src="../js/jquery-3.6.0.js"></script>
         <script>
             get = () => {
                 let t = $('#input');
                 if (t.val() === '请输入用户名：') t.val('');
             };
     
             lose = () => {
                 let input = $('#input');
                 if (input.val() === '') input.val('请输入用户名：');
     
             };
         </script>
     </head>
     <body>
     <label for="input"></label><input type="text" onfocus="get()" onblur="lose()" id="input" value="请输入用户名："/>
     </body>
     </html>
     ```

4. 下拉框的选择事件

   - 用户在下拉框中选中了一个选项，就会触发选择事件。

   - 事件属性名：onchange

   - 下拉框的每个 option 都应该要有 value 属性，不能重复，该值表示当前选中的选项。

     ```html
     <select id="sel" onchange="select()">
         <option value="1">冠军</option>
         <option value="2">亚军</option>
         <option value="3">季军</option>
     </select>
     ```

     ```javascript
     select = () => {
         let sel = $('#sel');
         alert(sel.val());
     };
     ```

5. 鼠标的移入和移出事件

   - 当鼠标进入元素时触发进入事件 onmouseenter，当鼠标移除元素时触发事件 onmouseout，该类事件一般用来作内容的切换。

     ```html
     <div id="div" style="width: 300px;height: 300px;border: 1px #000 solid;" onmouseenter="alert('in');"
          onmouseleave="alert('out')"></div>
     ```

6. 表单的提交事件

   - 当点击表单中的 submit 按钮，图片按钮，button按钮时，以及调用表单对象的 submit 方法时，都会触发表单的提交事件。

   - 事件的属性名：onsubmit

   - 在该事件中如果返回 true，该表单就会被提交。如果返回 false，则表单不提交。依照以上的特点，该事件主要用来通过对数据进行验证的结果，来决定是否提交表单，如果数据验证通过则提交，否则不提交。

   - 例子：注册表单的数据验证及提交控制。

     ```html
     <!DOCTYPE html>
     <html lang="en">
     <head>
         <meta charset="UTF-8">
         <title>Title</title>
         <script src="../js/jquery-3.6.0.js"></script>
         <script>
             checkUsername = () => {
                 let username = $('#username');
                 if (username.val() === "") {
                     alert("用户名不能为空！");
                     return false;
                 }
     
                 if ((/\W/).test(username.val())) {
                     alert('用户名不符合要求！(只能由数字或字母组成)');
                     return false;
                 }
                 // alert('正确');
                 return true;
             };
             checkPass = () => {
                 let pass1 = $('#pass1');
                 let pass2 = $('#pass2');
                 if (pass1.val().length < 5) {
                     alert('密码长度太短');
                     return false;
                 }
                 if (pass1.val() !== pass2.val()) {
                     alert('两次密码不一致！');
                     return false;
                 }
                 // alert('正确');
                 return true;
             };
     
             checkSex = () => {
                 let men = $("#men:checked");
                 let women = $("#women:checked");
                 if (men.val() === "men" || women.val() === 'women') {
                     // alert('正确');
                     return true;
                 }
                 alert('请选择性别！');
                 return false;
             };
             checkCity = () => {
                 let index = $('#city');
                 if (index.val() === '0') {
                     alert('请选择城市');
                     return false;
                 }
                 // alert('正确');
                 return true;
             };
     
             check = () => {
                 return checkCity() && checkSex() && checkPass() && checkUsername();
             };
         </script>
     </head>
     <body>
     <form action="#" method="get" id="form" onsubmit="return check()">
         <p>用户名：<input type="text" id="username" size="30"/>不能为空，由数字和字母组成</p>
         <p>密码：<input type="password" id="pass1" size="5"/>不能为空，长度>=5</p>
         <p>确认密码：<input type="password" id="pass2" size="5"/>再次确认</p>
         <p>性别：<input type="radio" id="men" name="men" value="men"/>男。<input type="radio" id="woman" name="women" value="women"/>女。必须选择</p>
         <p>选择城市：<select id="city">
             <option value="0">请选择</option>
             <option value="1">武汉</option>
             <option value="2">黄石</option>
             <option value="3">北京</option>
         </select>必须选择
         </p>
         <input type="submit" value="提交"/>
     </form>
     
     </body>
     </html>
     ```

### 十、js的对象

- js 是基于对象的语言，所以可以利用这种机制去创建和使用对象。面向对象的设计在 js 中是适用的。

1. js 中的对象分类

   - 本地对象，js 内部提供了一些类，设计者可以基于这些类可以创建不同的对象来使用。
     - Object 类，它是所有对象的超类，它本身没有任何属性和方法，可以用它来创建自定义对象。
     - 所有的基础类型（number，string，boolean，null，undefined）也提供了相应的类，利用它们也可以创建对象。但是 js 不建议用它们来创建对象，如果这样会降低程序的执行效率。所有基础类型的首字母大写，就表示类。
     - RegExp 对象，是正则表达式对象。
     - Array 对象，就是数组对象。
     - Date 对象，日期时间对象。
     - 一组表示异常或错误的对象，比如 Error，TypeError 等。
   - 内置对象：当 js 的程序运行时，会自动创建的对象。
     - Globe 全局对象，typeof，parseInt，parseFloat 等。
     - Math 对象，提供一个算数运算的方法。
   - 宿主对象：js 程序所运行的环境提供的对象，前端页面所执行的 js 程序的宿主就是浏览器。
     - BOM 对象
     - DOM 对象

2. 如何创建自定义对象

   1. 工厂方法方式
   2. 构造方法方式
   3. 原型方式，利用对象的原型来创建属性和方法，原型是一个关键字，protoType。

   - 以上三种都是 DCMA Script - 262 版本提供的方式，就是 javascript 的 5.1 版本。现在最新的版本是 6 版本，ECMA 6.

   - 它提供了创建对象更好用的方式，用一对 { } 来表示对象。现在的编程中用的很普遍。

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <script>
           f = (event) => {
               event.stopPropagation();
           };
   
           objectTest = () => {
               // alert(obj.name);
               // obj.learn(obj);
               return {
                   name: "张三",
                   age: 23,
                   learn: (obj) => {
                       alert("我是" + obj.name + "，今年" + obj.age + "岁");
   
                   }
               };
           };
   
           baseTest = () => {
               let str = String();
               alert(typeof str);
           };
   
           regexpTest = () => {
               let reg = new RegExp(/\d$/);
               alert(reg.test("abc12a"))
           };
   
           arrayTest = () => {
               let ary = [1, 4, 7];
               alert(ary.length);
           };
   
           dateTest = () => {
               let date = new Date();
               alert(date);
           };
   
           errorTest = () => {
               try {
                   alert(9 / RegExp);
                   throw new Error('124');
               } catch (e) {
                   alert('异常产生了');
               }
   
           };
   
           testFactory = () => {
               let obj = objectTest();
               obj.learn(obj);
           };
   
       </script>
   </head>
   <body>
   <button onclick="testFactory()">测试</button>
   </body>
   </html>
   ```

3. 闭包：在函数内操作函数外的变量，当程序被解释时，并不会对函数外部变量进行赋值或计算，要等到运行时才确定变量的初始值。还有一种就是在一个函数内部定义了另一个函数，内部的函数也可以使用外部函数的变量。

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <script>
           test = () => {
               let a = 1;
               let inner = () => {
                   a = a + 1;
                   return a;
               };
               inner();
               inner();
               inner();
               return a;
           };
       </script>
   </head>
   <body>
   <button onclick="console.log(test())">测试</button>
   </body>
   </html>
   ```

### 十一、js 的数组

- 数组是一个对象，通过 Array 类派生而来的。

1. 创建数组的方式。在某些情况下，创建或者得到的数组对象其实是集合对象，要注意区分。
2. 得到数组的长度。
3. 遍历数组，两种方式，一种是 for 循环，另一种是 foreach（迭代方式）。
4. 数组的常用方法，向数组中添加元素和删除元素的方法。

### 十二、日期对象（Date）

1. Date 对象用来表示日期和时间，从 1970.1.1 0 时至今的所有时刻都可以表示。

2. 创建 Date 对象。let date = new Date();

3. 得到用毫秒值表示的日期时间。console.log(date.getTime());

4. 可以获取构造日期时间的各个要素值，包含年，月，日，时，分，秒，毫秒。

   ```javascript
   let dateStr = date.getFullYear() + '年' + (date.getMonth() + 1) + '月' + date.getDate() + '日  ' + date.getHours() + '时' + date.getMinutes() + '分' + date.getSeconds() + '秒' + date.getMilliseconds() + '毫秒';
   ```

5. 创建表示任意日期时间的对象。let date1 = new Date(2005,6,17);

6. 可以在 Date 对象上修改它的所有的日期时间要素，修改的方法参照获取的方法。

