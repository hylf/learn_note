package com.zhong.test_12;

import com.alibaba.druid.pool.DruidDataSource;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 数据库工具类，提供创建连接池，产生连接对象，关闭资源等功能
 */

public class DBUtils {
    //建立数据库连接需要四要素：驱动程序名；连接的url；用户名；密码
    //5.XXX版本可直接写jdbc:mysql://127.0.0.1:3306/数据库名?characterEncoding=utf-8
    private DruidDataSource dds;
    private final String propPath;

    public DBUtils(String propPath) {
        //初始化
        this.propPath = propPath;
        try {
            Class.forName(getProperties().getProperty("driverName"));//加上这个会提高效率
            //Class.forName(Objects.requireNonNull(getProperties()).getProperty("driverName"));//加上这个会提高效率
            dds = new DruidDataSource();
            //加载驱动程序，向程序中注册加载的驱动程序
            dds.setDriverClassName(getProperties().getProperty("driverName"));
            //dds.setDriverClassName(Objects.requireNonNull(getProperties()).getProperty("driverName"));
            dds.setUrl(getProperties().getProperty("url"));
            dds.setUsername(getProperties().getProperty("userName"));
            dds.setPassword(getProperties().getProperty("password"));
            dds.setInitialSize(Integer.parseInt(getProperties().getProperty("initialSize")));//初始数量
            dds.setMinIdle(Integer.parseInt(getProperties().getProperty("minIdle")));//最小的空闲连接数量
            dds.setMaxActive(Integer.parseInt(getProperties().getProperty("maxActive")));//最大允许的连接数量
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //读取属性文件
    private Properties getProperties() {
        Properties properties;
        try {
            properties = new Properties();
            properties.load(new FileReader(this.propPath));
            return properties;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //得到连接
    public Connection getConnection() {
        try {
            // return DriverManager.getConnection(getProperties().getProperty("url"), getProperties().getProperty("userName"), getProperties().getProperty("password"));
            return this.dds.getConnection();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    //关闭连接
    public static void release(Connection con, PreparedStatement pst, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (pst != null) {
            try {
                pst.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

}
