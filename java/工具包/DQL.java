package com.zhong.test_12;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 实现查询操作，不需要考虑事务的问题
 * 使用模板设计模式
 * 需要先调用setConnection方法
 *
 * @param *sql查询命令
 * @param *sql命令中的参数
 */

public abstract class DQL<T> {
    private static Connection con;
    private static PreparedStatement pst;
    private ResultSet rs;

    public static void setConnection(Connection con) {
        //得到连接
        DQL.con = con;
    }

    public List<T> query(String sql, Object... params) {

        try {
            pst = con.prepareStatement(sql);//创建预编译声明对象

            //给 sql 命令注入参数
            for (int i = 0; i < (params == null ? -1 : params.length); i++) {
                pst.setObject(i + 1, params[i]);
            }
            //发送并执行命令,得到结果集
            rs = pst.executeQuery();
            //处理结果的具体代码，本方法的设计者不知道最终用户要如何处理结果
            return process(rs);

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } finally {
            DBUtils.release(con, pst, rs);
        }
        return null;
    }

    protected abstract List<T> process(ResultSet rs);

}
