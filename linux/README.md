### 一、安装CentOS7-2009

+ 官网：<https://www.centos.org/>
+ <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>F2-F6</kbd>，可以切换到命令行界面`(shell)`。<kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>F1</kbd>回到图形界面。

### 二、目录结构

+ `/`
  + `/root`
    + `/root/Desktop`
    + `/root/Maildir`
    + ……
  + `/bin`，`Binary`的缩写，存放最经常使用的命令
  + `/boot`，引导文件
  + `/sbin`，`s`是`system`的意思，存放系统管理员使用的系统管理程序
  + `/dev`，存放设备
  + `/etc`，存放配置文件
  + `/home`，存放普通用户的目录
  + `/opt`，存放第三方软件包的位置
  + `/proc`，进程目录，是一个系统目录
  + `/run`，运行目录，存放系统运行信息，是一个临时目录
  + `/srv`，存放系统服务相关内容
  + `/sys`，存放系统硬件相关信息
  + `/tmp`，临时目录
  + `/var`，一般存放日志
  + `/lib`和`/lib64`，类似`System32`和`System`
  + `/usr`，存放很多应用程序和用户相关数据
    + `/usr/bin`
    + `/usr/lib`
    + ……
  + `/media`，存放媒体设备，例如光盘，光驱
  + `/mnt`，类似于`/media`，是一个挂载点
  + ……

### 三、vim

#### 1、常用语法（普通模式下）

+ 一般用来做删除、复制和粘贴操作

|    语法    |                        功能                         |
| :--------: | :-------------------------------------------------: |
|    `yy`    |                  复制光标当前一行                   |
| `y 数字 y` |          复制一段（从光标当前行到后`n`行）          |
|    `p`     |                箭头移动到目的行粘贴                 |
|    `u`     |                     撤销上一步                      |
|    `dd`    |                   删除光标当前行                    |
| `d 数字 d` |               删除光标（含）后多少行                |
|    `x`     |        剪切一个字符（当前光标），相当于`del`        |
|    `X`     | 剪切一个字符（当前光标的前一个），相当于`Backspace` |
|    `yw`    |                     复制一个词                      |
|    `dw`    |                     删除一个词                      |
|    `^`     |                     移动到行头                      |
|    `$`     |                     移动到行尾                      |
|    `w`     |             移动到下一个词（词头位置）              |
|    `e`     |                   移动到当前词尾                    |
|    `gg`    |                     移动到页头                      |
|    `G`     |                     移动到页尾                      |
|  `数字+G`  |                    移动到目标行                     |
|    `r`     |                      替换字符                       |
|    `R`     |                      替换模式                       |

#### 2、进入编辑模式方法

| 按键 |              功能              |
| :--: | :----------------------------: |
| `i`  |           当前光标前           |
| `a`  |           当前光标后           |
| `o`  | 当前光标行的下一行（插入新行） |
| `I`  |         光标所在行最前         |
| `A`  |         光标所在行最后         |
| `O`  | 当前光标行的上一行（插入新行） |

#### 3、命令模式

|      命令       |                      功能                      |
| :-------------: | :--------------------------------------------: |
|      `:w`       |                      保存                      |
|      `:q`       |                      退出                      |
|      `:wq`      |                   保存并退出                   |
|      `:q!`      |                 不保存强制退出                 |
|  `/要查询的词`  | `n`查找下一个，`N`往上查找，加上`\c`忽略大小写 |
|     `:noh`      |                  取消高亮显示                  |
|    `:set nu`    |                    显示行号                    |
|   `:set nonu`   |                    关闭行号                    |
|  `:s/old/new`   |      替换当前行匹配到的第一个`old`为`new`      |
| `:s/old/new/g`  |       替换当前行匹配到的所有`old`为`new`       |
|  `:%s/old/new`  |   替换文档中每一行匹配到的第一个`old`为`new`   |
| `:%s/old/new/g` |       替换文档中匹配到的所有`old`为`new`       |

### 四、网络配置和系统管理操作

#### 1、网络配置

+ `vim /etc/sysconfig/network-scripts/ifcfg-ens33`

```ini
TYPE="Ethernet"
PROXY_METHOD="none"
BROWSER_ONLY="no"
BOOTPROTO="none"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="yes"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_FAILURE_FATAL="no"
IPV6_ADDR_GEN_MODE="stable-privacy"
NAME="ens33"
UUID="aca80fae-dce9-42db-ac1f-b537c5c09aba"
DEVICE="ens33"
ONBOOT="yes"
IPADDR="192.168.46.10"
PREFIX="24"
GATEWAY="192.168.46.1"
IPV6_PRIVACY="no"
MTU="1"
DNS1="8.8.8.8"
DNS2="114.114.114.114"
```

+ 重启网络服务：
  + `service network restart`
  + `systemctl restart network`
  + `systemctl restart NetworkManager`

#### 2、修改主机名

+ 查询主机名：`hostname`
+ 修改主机名：`vim /etc/hostname`（重启生效）
+ 立即生效：`hostnamectl set-hostname xxx`

##### 3、修改`hosts`文件

+ `vim /etc/hosts`，`#`是注释

  ```
  127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
  ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
  192.168.46.10   learn1
  ```

+ `windows`的`hosts`文件存放路径：`C:\Windows\System32\drivers\etc\hosts`

#### 4、`ssh`登录

+ `windos`命令行进行登录：`ssh root@192.168.46.10`，如果配置了`hosts`，可以使用`ssh root@learn1`登录，其中`root`表示登录的用户名。
+ 退出：`exit`

#### 5、文件传输

+ 上传：`scp src_path user@host:dst_path`
  + `eg`：`scp readme.txt root@192.168.46.10:/host/tmp`
+ 下载：`scp user@host:src_path dst_path`
  + `eg`：`scp root@192.168.46.10:/host/tmp/readme.txt D:\tmp\`

#### 6、`service`服务管理

+ 带`d`后缀的一般代表守护服务

+ `centos 6`版本
  + `service 服务名 start|stop|restart|status`
  + 查看服务：`ls /etc/init.d/`
+ `centos 7`版本（重点）
  + `systemctl start|stop|restart|status 服务名`
  + 查看服务：`ls /usr/lib/systemd/system`
+ 进入服务管理界面：`setup`
  + 方向键切换，空格键选中，<kbd>Tab</kbd>切换，<kbd>Enter</kbd>确认。

#### 7、`Linux`运行级别

+ 开机 –> BIOS –> /boot –> init进程 –> 运行级别 –> 运行级别对应的服务

+ `linux`运行级别（常用的是`3`和`5`）：
  + 0：系统停机状态，系统默认运行级别不能设为0，否则不能正常启动；
  + 1：单用户工作状态，`root`权限，用于系统维护，禁止远程登录；
  + 2：多用户状态（没有`NFS网络文件系统`），不支持网络；
  + 3：完全的多用户状态（有`NFS`），登录后进入控制台命令行模式；
  + 4：系统未使用，保留；
  + 5：`X11`控制台，登录后进入图形`GUI`模式；
  + 6：系统正常关闭并重启，默认运行级别不能设为6，否则不能正常启动。

+ `centos 7`的运行级别简化为：

  + `multi-user.target`，等价于原运行级别3（多用户有网，无图形界面）
  + `graphical.target`，等价于原运行级别5（多用户有网，有图形界面）

+ 查询默认级别：`systemctl get-default`

+ 设置默认级别：`systemctl set-default xxx`

+ `centos 6`查看运行级别：`cat /etc/inittab`

  ```python
  # inittab is no longer used when using systemd.
  #
  # ADDING CONFIGURATION HERE WILL HAVE NO EFFECT ON YOUR SYSTEM.
  #
  # Ctrl-Alt-Delete is handled by /usr/lib/systemd/system/ctrl-alt-del.target
  #
  # systemd uses 'targets' instead of runlevels. By default, there are two main targets:
  #
  # multi-user.target: analogous to runlevel 3
  # graphical.target: analogous to runlevel 5
  #
  # To view current default target, run:
  # systemctl get-default
  #
  # To set a default target, run:
  # systemctl set-default TARGET.target
  #
  ```

+ 进入每个运行级别：`init x`，例如`init 3`

+ `centos 6`查看不同服务在不同运行级别下的开机自启情况：`chkconfig –list`

  ```
  [root@learn1 ~]# chkconfig --list
  
  注：该输出结果只显示 SysV 服务，并不包含
  原生 systemd 服务。SysV 配置数据
  可能被原生 systemd 配置覆盖。 
  
        要列出 systemd 服务，请执行 'systemctl list-unit-files'。
        查看在具体 target 启用的服务请执行
        'systemctl list-dependencies [target]'。
  
  netconsole     	0:关	1:关	2:关	3:关	4:关	5:关	6:关
  network        	0:关	1:关	2:关	3:关	4:关	5:关	6:关
  
  ```

  + 打开某个服务开机自启：`chkconfig [--level n] xxx on`
  + 关闭每个服务开机自启：`chkconfig [--level n] xxx off`
  + 其中`--level n`是可选项，可以指定在某个级别下的开启和关闭，默认全部
  + `systemctl disable|enable xxx`可以在当前运行级别下修改

+ `centos 7`：`systemctl list-unit-files`

  + `unit`含义：所有服务都是`systemctl`管理的单元
  + `static`状态代表未知，因为部分服务依赖其它服务

+ `centos 6`的防火墙叫：`iptables`

+ `centos 7`的防火墙叫：`firewalld`

#### 8、关机操作

+ `shutdown`，默认1分钟之后关机
+ `shutdown now`，立即关机
+ `shutdown n`，`n`分钟后关机
+ `shutdown -c`，取消关机操作
+ `shutdown xx:xx`，指定时间关机，例如`shutdown 23:00`

##### 1）基本语法

+ `sync`，将数据由内存同步到硬盘中
+ `halt`，停机，关闭系统但不断电
+ `poweroff`，关机，断电
+ `reboot`，重启，等同于`shutdown -r now`
+ `shutdown [选项] 时间`

### 五、`Shell`命令整体介绍

#### 1、帮助命令（`manual`）

+ `man [命令或配置文件]`，获得帮助信息，通常显示以下信息：

  |     信息      |           功能           |
  | :-----------: | :----------------------: |
  |    `NAME`     |   命令的名称和单行描述   |
  |  `SYNOPSIS`   |       怎样使用命令       |
  | `DESCRIPTION` |    命令功能的深入探讨    |
  |  `EXAMPLES`   |    怎样使用命令的例子    |
  |  `SEE ALSO`   | 相关主题（通常是手册页） |

+ 内置（`built in`）命令：内嵌在`shell`中，随`shell`一起加载，常驻系统内存中；相应的其它命令被称为“外部命令”

+ 使用`type xxx`获取命令类型

+ 使用`help xxx`获取`shell`内置命令的帮助信息

+ 使用`man -f xxx`可以查看内置命令，例如`man -f cd`

  ```
  cd (1)               - GNU Bourne-Again SHell (GNU 命令解释程序 “Bourne二世”)
  cd (3tcl)            - 改变工作目录
  cd (1p)              - change the working directory
  ```

  + 以上列举的是`cd`命令在不同页的解释，使用`man 1p cd`可以查看`cd`命令在`1p`页下的解释。

+ 使用`xxx –help`也可以查看帮助

#### 2、文件目录类

+ `pwd`，显示当前工作目录的绝对路径（`print working directory`）

  + `pwd -P`，抛开软链接等其它影响，显示真实的物理路径

+ `cd -`，返回上一个目录

  + `cd`或`cd ~`，进入该用户的主目录
  + `cd -P`，进入真实的物理路径

+ `ls -a`，列出所有文件，包括隐藏文件

  + 以`.`开头的文件和目录是隐藏的
  + `ls -i xxx`，查看`xxx`的索引号

+ `ll`是`ls -l`的缩写

  + `-`表示文件，`l`表示链接（相当于快捷方式），`d`表示目录，`c`表示字符类型的设备文件，例如鼠标；`b`表示块设备文件，例如硬盘

+ `mkdir [-p] xxx`，创建一个目录，`-p`参数表示递归创建

+ `rmdir [-p] xxx`，移除一个目录

+ `touch`，创建一个空文件

+ `cp [选项] source dest`，复制`source`到`dest`，如果都是一个文件，会提示是否覆盖`dest`的内容

  + `-r`选项，递归复制整个目录

  + 在`cp`命令前加`\`可以默认确认覆盖，例如`\cp ~/initial-setup-ks.cfg ./`

  + 其中，在命令前加上`\`代表使用原生命令，不加默认的参数，例如使用`\ls`发现展示的内容不再有颜色区分

  + 使用`alias`命令可以查看带有默认参数的命令：

    ```
    alias cp='cp -i'
    alias egrep='egrep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias grep='grep --color=auto'
    alias l.='ls -d .* --color=auto'
    alias ll='ls -l --color=auto'
    alias ls='ls --color=auto'
    alias mv='mv -i'
    alias rm='rm -i'
    alias which='alias | /usr/bin/which --tty-only --read-alias --show-dot --show-tilde'
    
    ```

+ `rm [选项] xxx`，删除目录

  + `-r`，递归删除目录中所有内容
  + `-f`，强制执行删除操作，而不提示用于进行确认
  + `-v`，显示指令的详细执行过程
  + `rm -f ./*`，删除当前目录下所有内容，但是保留此目录

+ `mv old new`，重命名

+ `mv src dst`，移动文件（移动的同时也可以重命名），例如：`mv hello1 tmp/hello`

+ `cat [选项] xxx`，查看文件内容，从第一行开始显示

  + `-n`，显示行号

+ `more xxx`，分屏显示文件内容

  |              操作              |              功能说明              |
  | :----------------------------: | :--------------------------------: |
  |        <kbd>Space</kbd>        |              向下翻页              |
  |        <kbd>Enter</kbd>        |             向下翻一行             |
  |          <kbd>Q</kbd>          | 立刻离开`more`，不再显示该文件内容 |
  | <kbd>Ctrl</kbd>`+`<kbd>F</kbd> |            向下滚动一屏            |
  | <kbd>Ctrl</kbd>`+`<kbd>B</kbd> |             返回上一屏             |
  |          <kbd>=</kbd>          |          输出当前行的行号          |
  |              `:f`              |      输出文件名和当前行的行号      |

+ `less xxx`，分屏显示文件内容，并不是一次全部加载，而是根据需要加载内容，兼容`more`的快捷键，除此之外：

  |        操作         |                           功能说明                           |
  | :-----------------: | :----------------------------------------------------------: |
  | <kbd>pagedown</kbd> |                           向下翻页                           |
  |  <kbd>pageup</kbd>  |                           向上翻页                           |
  |       `/子串`       | 向下搜寻<mark>子串</mark>的功能；按`n`，向下查询，`N`，向上查找 |
  |       `?子串`       | 向上搜寻<mark>子串</mark>的功能；按`n`，向上查找，`N`，向下查找 |
  |         `q`         |                        离开`less`程序                        |

+ `echo [选项] [输出内容]`，输出内容到控制台

  + `-e`，支持反斜线控制的字符转换
    + `\\`，输出`\`本身
    + `\n`，换行
    + `\t`，制表符，也就是<kbd>Tab</kbd>键
  + `echo $`，查看环境变量

+ `>`，输出重定向，`>>`，追加，文件没有会自动创建

  + `ls -l > xxx`，命令执行结果写到文件`xxx`中（覆盖写）
  + `ls -al >> xxx`，命令执行结果追加到`xxx`文件末尾
  + `cat 文件1 > 文件2`，将文件1的内容覆盖到文件2
  + `echo “内容” >> 文件`，将一段文本追加到文件末尾

+ `head xxx`，查看文件前10行内容

+ `head -n 5 xxx`，查看`xxx`前5行内容

+ `tail xxx`，查看文件尾部10行内容

+ `tail -n 5 xxx`，查看文件尾部5行内容

+ `tail -f xxx`，实时追踪该文档的所有更新，`vim`下的修改不能被监控到，因为文件的`inode`已经改变

  + <kbd>Ctrl</kbd>+<kbd>S</kbd>，暂停追踪
  + <kbd>Ctrl</kbd>+<kbd>Q</kbd>，继续追踪
  + <kbd>Ctrl</kbd>+<kbd>C</kbd>，退出该程序

#### 3、软链接

+ 类似于快捷方式，有自己的数据块，主要存放了链接其他文件的路径，也称为符号链接
+ 基本语法：`ln -s [原文件或目录] [软链接名]`，给原文件创建一个软链接
  + 删除软链接：`rm -rf 软链接名`，如果使用`rm -rf 软链接名/`会把软链接对应的文件删除，如果链接到的是目录，会删除目录下的所有文件，目录本身保留，软链接本身不会被删除

#### 4、硬链接

+ `ln [原文件或目录] [软链接名]`，只能创建针对文件的硬链接（指向同一个`inode`），相当于一个文件有多个不同的文件名，修改其中任意一个，所有与其有硬链接的文件都一起修改了。

#### 5、其它

+ `history`，显示历史命令
  + `history 10`，显示最近10条命令
  + `history -c`，清空之前的历史记录

#### 6、日期时间型

+ `date`，显示当前时间
  + `date +%Y`，显示当前年份
  + `date +%m`，显示当前月份
  + `date +%d`，显示当前是哪一天
  + `date "+%Y-%m-%d %H:%M:%S"`，显示年月日时分秒
  + `date +%s`，从`1970年1月1日`到现在的秒数
  + `date -d '1 days ago'`，显示昨天的时间
  + `date -s "2022-12-12 12:12:12"`，设置系统当前时间
  + `ntpdate xxx`，同步网络上的时间，`xxx`表示具体的连接，可查询<https://blog.csdn.net/weixin_42588262/article/details/82501488>获取详细信息。
    + 示例：`ntpdate ntp.tuna.tsinghua.edu.cn`
+ `cal`，查看日历
  + `cal [选项] [[[日] 月] 年]`
    + `cal 1 8 2022`，查看`2022年8月1日`
  + `-1`，只显示当前月份，默认
  + `-3`，显示上个月、当月和下个月
  + `-s`，周日作为一周第一天（`sunday`）
  + `-m`，周一作为一周第一天（`monday`）
  + `-j`，输出该月每一天是该年的第几天
  + `-y`，输出整年
  + `-V`，显示版本信息并退出
  + `-h`，显示帮助并退出

#### 7、用户管理命令

+ 添加用户：`useradd 用户名`，添加新用户
  + 默认情况下会在`/home`下创建和用户名相同的一个目录，使用`useradd -d /home/dave david`可以给创建的新用户指定目录名和所在位置
  + `useradd -g 组名 用户名`，添加某个用户到某个组
  
+ `passwd 用户名`，给某个用户设定密码

+ `usermod -g 组名 用户名`，将某个用户添加到某个组中

+ `id 用户名`，查看每个用户的`uid`、`gid`和所在组。可以用来验证某个用户是否存在

+ `cat /etc/passwd`，查看所有的用户

+ `su`，即`switch user`，切换用户
  + `exit`，退出当前用户
  + `who`，显示当前的用户名
  
+ `sudo xxx`，以`root`权限执行`xxx`命令

  + `vim /etc/sudoers`，添加一行

    ```
    %xxx	ALL=(ALL)	NOPASSWD:	ALL
    ```

  + 加`%`代表组，加`NOPASSWD:`代表不需要输入密码

+ 删除用户：`userdel [选项] 用户名`，删除后对应的目录会保留。加上`-r`选项对应的目录也会删除

#### 7、用户组管理

+ `groupadd 组名`，添加一个组
+ `cat /etc/group`，查看所有组
+ `groupmod -n 新组名 现组名`，修改组名
+ `groupdel xxx`，删除`xxx`组

#### 8、文件权限

+ `chmod [{ugoa}{+-=}{rwx}] 文件或目录`
  + 例子：`chmod g=xw tmp`，`chmod g+w tmp`
  + 或者`chmod [mode=42] 文件或目录`
  + `U`，所有者；`g`，所有组；`o`，其他人；`a`所有人
  + `+`，增加权限；`-`，移除权限
  + `r=4; w=2; x=1`
  + `chmod -R 权限 目录`，指定目录下所有文件具有某种权限
+ `chown [选项] [用户名] [文件或目录]`，更改文件或目录的所有者
  + `-R`，递归操作
+ `chgrp [组名] [文件或目录]`，改变文件或目录所属组

#### 9、搜索查找类

+ `find [搜索范围] [选项]`

  | 选项              | 功能                                                         |
  | :---------------- | :----------------------------------------------------------- |
  | `-name<查询方式>` | 按照指定的文件名和查找模式查找文件                           |
  | `-user<用户名>`   | 查找属于指定用户名的所有文件                                 |
  | `-size<文件大小>` | 按照指定的文件大小查找文件：<br />b——块（512字节）<br />c——字节<br />w——字（2字节）<br />k——千字节<br />M——兆字节<br />G——吉字节 |

  + 示例：`find /home/ -user tony`，`find -name "*.cfg"`，`find -name hello`
    + `find ./ -size 2k`，查询当前目录下所有文件或目录大小等于`2k`的文件或目录；`+2k`表示大于`2k`；`-2k`表示小于`2k`

+ `locate 搜索文件`，快速定义文件路径

  + 该指令利用事先建立好的系统中所有文件名称及路径的`locate`数据库实现快速定位给定的文件。`locate`指令不需要遍历整个文件系统，查询速度较快，为了保证查询结果的准确性，管理员必须定期更新`locate`时刻
  + 第一次运行前必须使用`updatedb`指令创建`locate`数据库

+ `which locate`，`whereis locate`，查询命令存放位置
+ `grep [选项] 查找内容 源文件`，管道符`|`表示将前一个命令的处理结果输出传递给后面的命令处理，如果包含空格，需要放在单引号或双引号里
  
  + `-n`，显示匹配行及行号
  + `-w`，只返回包含单词的行
  + `-v`，排除匹配
  + `-i`，忽略大小写
  + `-e`，有两个及以上的排除条件时可多次使用`-e`选项
  + `-E`，使用扩展正则表达式
  + `--exclude-dir`，排除目录，有多个目录时，使用`–exclude-dir={dir1, dir2, ……}`
  + 例如：`ll | grep hello`
  + 单独使用：`grep -n 1 hello1`，查询`hello1`文件中包含字符串`1`的行，并高亮显示，显示行号
+ `wc 文件名`，（word count）单词统计
  
  + `grep -n 31 2022 | wc`，显示结果依次表示行数、单词数、大小（若单独使用`wc`还会显示文件名）
+ `yum -y install tree`安装`tree`指令
  
  + `tree 目录`，以树状结构展示指定目录结构

#### 10、压缩和解压缩

+ `gzip 文件`，压缩文件，只能将文件压缩为`*.gz`文件

+ `gunzip 文件`，解压缩文件

+ 经验技巧

  + 只能压缩文件不能压缩目录
  + 不保留原来的文件
  + 同时多个文件会产生多个压缩包

+ `zip [选项] dst src`，压缩文件和目录

+ `unzip [选项] xxx`，解压缩

  + `-r`，压缩目录
  + `-d<目录>`，指定解压后存放的目录
  + `zip`命令在`windows`和`linux`都通用，可以压缩目录并且保留源文件
  + 示例：`zip -d tmp/ 2022.zip 2022`

+ `tar [选项] dst src...`，打包目录

  | 选项 |          功能          |
  | :--: | :--------------------: |
  | `-c` |   产生`.tar`打包文件   |
  | `-v` |      显示详细信息      |
  | `-f` |   指定压缩后的文件名   |
  | `-z` | 打包同时压缩（`gzip`） |
  | `-x` |     解包`.tar`文件     |
  | `-C` |     解压到指定目录     |

  + 打包并压缩：`tar -zcvf temp.tar.gz 2022 hello1`
  + 解压缩到指定目录：`tar -zxvf temp.tar.gz -C tmp/`

#### 11、磁盘管理类

+ `du [选项] 目录/文件`，显示目录下每个子目录的磁盘使用情况（disk usage，磁盘占用情况）

  |      选项       |                  功能                  |
  | :-------------: | :------------------------------------: |
  |      `-h`       |          以较易阅读的格式显示          |
  |      `-a`       |     不仅查看子目录大小，还包括文件     |
  |      `-c`       | 显示所有的文件和子目录大小后，显示总和 |
  |      `-s`       |               只显示总和               |
  | `--max-depth=n` |      指定统计子目录的深度为`n`层       |

  + 示例：`du -ach ./`，`du -hs /home`

+ `df [选项]`，列出文件系统的整体磁盘使用量，检查文件系统的磁盘空间占用情况（disk free空余磁盘）

  + `-h`，以较易阅读的方式显示

+ `free -h`，查看内存和`swap`使用情况

+ `lsblk`，查看设备挂载情况

  + `-f`，查看详细的设备挂载情况，显示文件系统信息

  ```
  NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
  sda      8:0    0   40G  0 disk 
  ├─sda1   8:1    0    1G  0 part /boot
  ├─sda2   8:2    0    4G  0 part [SWAP]
  └─sda3   8:3    0   35G  0 part /
  sr0     11:0    1  4.4G  0 rom 
  ```

  ```
  [root@learn1 test]# lsblk -f
  NAME   FSTYPE  LABEL           UUID                                 MOUNTPOINT
  sda                                                                 
  ├─sda1 xfs                     6f5f8f84-1a71-4041-b067-3f8fe5727c50 /boot
  ├─sda2 swap                    55e5795c-0852-4994-885e-7eb14a11fa20 [SWAP]
  └─sda3 xfs                     c1362898-70ee-4074-b2d4-73c8bc32f4dd /
  sr0    iso9660 CentOS 7 x86_64 2020-11-04-11-36-43-00 
  ```

+ `mount/umount`，挂载/卸载

  + `mount [-t vfstype] [-o options] device dir`，挂载设备

  + `umount 设备名称或挂载点`，卸载设备

    | 参数         | 功能                                                         |
    | :----------- | :----------------------------------------------------------- |
    | `-t vfstype` | 指定文件系统的类型，通常不必指定，`mount`会自动选择正确的类型，常用类型有：<br />光盘或光盘镜像：`iso9660`<br />`DOS fat16`文件系统：`msdos`<br />`Windows 9x fat32`文件系统：`vfat`<br />`Windows NT ntfs`文件系统：`ntfs`<br />`Mount Windows`文件网络共享：`smbfs`<br />`UNIX(LINUX)`文件网络共享：`nfs` |
    | `-o options` | 主要用来描述设备或档案的挂接方式。常用参数有：<br />`loop:`用来把一个文件当成硬盘分区挂载上的系统<br />`ro:`采用只读方式挂载设备<br />`rw:`采用读写方式挂接设备<br />`iocharset:`指定访问文件系统所用字符集 |

    + 挂载示例：`mount /dev/sr0 /mnt/cdrom/`
    + 卸载示例：`umount /dev/sr0`或者`umount /mnt/cdrom`

  + 设置开机自动挂载

    + `vim /etc/fstab`，添加一行：

      ```
      /dev/sr0      /mnt/cdrom      iso9660 defaults        0 0
      ```

+ `fdisk`分区

  + `fdisk -l`，查看磁盘分区详情
  + `fdisk 硬盘设备名`，对新增硬盘进行分区操作
  + 最多四个主分区（1-4），逻辑分区（5-16）

+ `mkfs -t xxx 设备名称`，指定磁盘的文件系统（例如`xfs`）

#### 12、进程管理类

+ `ps `命令（`process status`进程状态）

  + `ps aux | grep xxx`，查看系统中所有进程
    + `USER`，该进程是由哪个用户产生的
    + `PID`，进程的`ID`号
    + `%CPU`，该进程占用`CPU`资源的百分比
    + `%MEM`，该进程占用物理内存的百分比
    + `VSZ`，该进程占用虚拟内存的大小，单位`KB`
    + `RSS`，该进程占用实际物理内存的大小，单位`KB`
    + `TTY`，该进程是在哪个终端中运行的，对于`CentOS`来说，`tty1`是图形化终端，`tty2-tty6`是本地的字符界面终端。`pts/0-255`代表虚拟终端，远程登录的都是虚拟终端。`?`代表没有使用终端，是一个后台进程
    + `STAT`，进程状态，常见的状态有：
      + `R`，运行状态
      + `S`，睡眠状态
      + `T`，暂停状态
      + `Z`，僵尸状态
      + `s`，包含子进程
      + `l`，多线程
      + `+`，前台显示
      + `<`，表示高优先级
      + `N`，表示低优先级
    + `START`，该进程的启动时间
    + `TIME`，该进程占用`CPU`的运算时间
    + `COMMAND`，调用该进程的命令
  + `ps -ef | grep xxx`，查看子父进程之间的关系（`PPID`）
    + `STIME`，启动时间
    + `PPID`，父进程`ID`
    + `CMD`，调用该进程的命令
    + `0`号进程：`idle`

  | 选项 |                    功能                    |
  | :--: | :----------------------------------------: |
  | `a`  |        列出带有终端的所有用户的进程        |
  | `x`  | 列出当前用户的所有进程，包括没有终端的进程 |
  | `u`  |           面向用户友好的显示风格           |
  | `-e` |                列出所有进程                |
  | `-u` |         列出某个用户关联的所有进程         |
  | `-f` |           显示完整格式的进程列表           |

  + 不加`-`是`BSD`风格，加`-`是`UNIX`风格
  + 例子：使用普通用户通过`ssh`登录时：

  ```
  [root@learn1 test]# ps -ef | grep -n sshd
  123:root        816      1  0 09:00 ?        00:00:00 /usr/sbin/sshd -D
  131:root       1203    816  0 09:00 ?        00:00:00 sshd: root@pts/0
  180:root       2575    816  0 09:49 ?        00:00:00 sshd: zq [priv]
  181:zq         2580   2575  0 09:49 ?        00:00:00 sshd: zq@pts/1
  186:root       2640   1297  0 09:49 pts/0    00:00:00 grep --color=auto -n sshd
  
  ```

  + 执行普通命令时使用`181`行对应的进程，执行`root`命令时使用`180`行对应的命令，这是为了权限分离，保证安全

+ `kill [选项] 进程号`，通过进程号杀死进程

  + `killall 进程名称`，通过进程名称杀死进程，支持通配符

  + `-9`，强迫进程立即停止

    + `kill -l`，查看所有信号值

    ```
    [root@learn1 test]# kill -l
     1) SIGHUP	 2) SIGINT	 3) SIGQUIT	 4) SIGILL	 5) SIGTRAP
     6) SIGABRT	 7) SIGBUS	 8) SIGFPE	 9) SIGKILL	10) SIGUSR1
    11) SIGSEGV	12) SIGUSR2	13) SIGPIPE	14) SIGALRM	15) SIGTERM
    16) SIGSTKFLT	17) SIGCHLD	18) SIGCONT	19) SIGSTOP	20) SIGTSTP
    21) SIGTTIN	22) SIGTTOU	23) SIGURG	24) SIGXCPU	25) SIGXFSZ
    26) SIGVTALRM	27) SIGPROF	28) SIGWINCH	29) SIGIO	30) SIGPWR
    31) SIGSYS	34) SIGRTMIN	35) SIGRTMIN+1	36) SIGRTMIN+2	37) SIGRTMIN+3
    38) SIGRTMIN+4	39) SIGRTMIN+5	40) SIGRTMIN+6	41) SIGRTMIN+7	42) SIGRTMIN+8
    43) SIGRTMIN+9	44) SIGRTMIN+10	45) SIGRTMIN+11	46) SIGRTMIN+12	47) SIGRTMIN+13
    48) SIGRTMIN+14	49) SIGRTMIN+15	50) SIGRTMAX-14	51) SIGRTMAX-13	52) SIGRTMAX-12
    53) SIGRTMAX-11	54) SIGRTMAX-10	55) SIGRTMAX-9	56) SIGRTMAX-8	57) SIGRTMAX-7
    58) SIGRTMAX-6	59) SIGRTMAX-5	60) SIGRTMAX-4	61) SIGRTMAX-3	62) SIGRTMAX-2
    63) SIGRTMAX-1	64) SIGRTMAX	
    ```

  + 例子：如果`kill`掉`sshd`的守护进程，已经登录的用户不受影响，并且父进程变为`1`，但新的用户无法通过`ssh`登录

+ `yum -y install pstree`，`pstree`命令可以查看进程树

  + `-p`，显示进程`PID`
  + `-u`，显示进程所属用户

+ `top [选项]`，实时监控系统进程状态

  |   选项    |                     功能                     |
  | :-------: | :------------------------------------------: |
  | `-d 秒数` |     指定`top`命令每隔几秒更新，默认`3`秒     |
  |   `-i`    |      使`top`不显示任何闲置或僵死的进程       |
  |   `-p`    | 通过指定监控进程`ID`来仅仅监控某个进程的状态 |

  + 操作说明

  | 操作 |            功能             |
  | :--: | :-------------------------: |
  | `P`  | 以`CPU`使用率排序，默认选择 |
  | `M`  |     以内存的使用率排序      |
  | `N`  |         以`PID`排序         |
  | `k`  |        杀死某个进程         |
  | `q`  |          退出`top`          |

#### 13、`netstat`显示网络状态和端口占用信息

+ `netstat -anp | grep 进程号`，查看该进程网络信息

+ `netstat -nlp | grep 端口号`，查看网络端口号占用情况

  | 选项 |                         功能                         |
  | :--: | :--------------------------------------------------: |
  | `-a` | 显示所有正在监听(`listen`)和未监听的套接字(`socket`) |
  | `-n` |       拒绝显示别名，能显示数字的全部转化为数字       |
  | `-l` |                仅列出在监听的服务状态                |
  | `-p` |                表示显示哪个进程在调用                |

#### 14、`crontab`系统定时任务

+ 重启`crond服务`：`systemctl restart crond`

+ `crontab [选项]`

  | 选项 |              功能               |
  | :--: | :-----------------------------: |
  | `-e` |      编辑`crontab`定时任务      |
  | `-l` |        查询`crontab`任务        |
  | `-r` | 删除当前用户所有的`crontab`任务 |

+ 参数说明（`crontab -e`）

  + `* * * * * 执行的任务`

  |   项目    |         含义         |          范围           |
  | :-------: | :------------------: | :---------------------: |
  | 第一个`*` | 一小时当中的第几分钟 |         `0-59`          |
  | 第二个`*` |  一天当中的第几小时  |         `0-23`          |
  | 第三个`*` |  一个月当中的第几天  |         `1-31`          |
  | 第四个`*` |   一年当中的第几月   |         `1-12`          |
  | 第五个`*` |   一周当中的星期几   | `0-7`（0和7都代表周日） |

+ 特殊符号

  | 符号  | 含义                                                         |
  | :---- | :----------------------------------------------------------- |
  | `**`  | 代表任何时间，例如第一个`*`代表一小时中每一分钟都执行一次    |
  | `,`   | 代表不连续的时间。比如`0 8,12,16 * * * 命令`，代表在每天的`8点0分， 12点0分， 16点0分`都执行一次命令 |
  | `-`   | 代表连续的时间范围。比如`0 5 * * 1-6 命令`，代表在周一到周六的凌晨`5点0分`执行命令 |
  | `*/n` | 代表每隔多久执行一次，例如`*/10 * * * * 命令`，代表每隔`10分钟`就执行一遍命令 |

+ 特定时间执行命令

  | 时间                | 含义                                             |
  | :------------------ | :----------------------------------------------- |
  | `45 22 * * * 命令`  | 每天`22点45分`执行命令                           |
  | `0 17 * * 1 命令`   | 每周`1`的`17点0分`执行命令                       |
  | `0 5 1,15 * * 命令` | 每月`1号、15号`凌晨`5点0分`执行命令              |
  | `40 4 * * 1-5 命令` | 每`周一`到`周五`的凌晨`4点40分`执行命令          |
  | `*/10 4 * * * 命令` | 每天的凌晨`4点`，每隔`10分钟`执行一次命令        |
  | `0 0 1,15 * 1 命令` | 每月`1号。15号`，每`周1`的`0点0分`都会执行命令。 |

+ 示例：

  ```
  */1 * * * * echo "hello, world" >> /home/zq/test/cron_test
  ```

### 六、软件包管理

#### 1、`RPM`（RedHat Package Manager）

+ `rpm -qa | grep 软件包名称`，查询某个软件包是否存在
+ `rpm -qi 软件包名称`，查看软件包详细信息
  + `-e`，卸载软件包
  + `--nodeps`，卸载软件时，不检查依赖（使用该软件包的软件可能无法使用）
+ `rpm -ivh 包全名`，安装软件包
  + `-i`，安装
  + `-v`，显示详细信息
  + `-h`，显示进度条
  + `–nodeps`，安装前不检查依赖

#### 2、`yum`（Yellow dog Updater, Modified）

+ `yum [选项] [参数]`

  + `-y`，对所有提问都回答`yes`

+ 参数说明

  |      参数      |              功能               |
  | :------------: | :-----------------------------: |
  |   `install`    |         安装`rpm`软件包         |
  |    `update`    |         更新`rpm`软件包         |
  | `check-update` | 检查是否有可用的更新`rpm`软件包 |
  |    `remove`    |      删除指定的`rpm`软件包      |
  |     `list`     |         显示软件包信息          |
  |    `clear`     |       清理`yum`过期的缓存       |
  |   `deplist`    |  显示`yum`软件包的所有依赖关系  |

+ 修改`yum`镜像源

  + 安装`wget`依赖从指定`URL`下载文件
    + `yum install -y wget`
  + 下载网易163或者aliyun的`repos`文件，选择其一
    + `wget http://mirrors.aliyun.com/repo/Centos-7.repo`
    + `wget http://mirrors.163.com/.help/CentOS7-Base-163.repo`
  + `less /etc/yum.repos.d/CentOS-Base.repo`

+ 备份`CentOS-Base.repo`，替换掉其中的内容

### 七、`Shell`编程

+ 查看支持的`shell`：`cat /etc/shells`

  ```
  [root@learn1 ~]# cat /etc/shells 
  /bin/sh
  /bin/bash
  /usr/bin/sh
  /usr/bin/bash
  /bin/tcsh
  /bin/csh
  ```

  + 查看默认`shell`：

  ```
  [root@learn1 ~]# echo $SHELL
  /bin/bash
  ```

#### 1、脚本入门

+ 脚本格式：以`#!/bin/bash`开头，指定解析器

+ 创建第一个脚本：

  ```sh
  [root@learn1 scripts]# cat hello.sh 
  #!/bin/bash
  
  echo "hello world!"
  
  ```

+ 执行方式：

  + 第一种：采用`bash`或`sh+脚本名`的路径（不需要脚本具有`x`权限）
  + 第二种：直接输入脚本的路径（需要有`x`权限）
    + 前两种都会打开子`shell`
  + 第三种：`source+脚本路径`（不需要`x`权限）
  + 第四种：`. 脚本路径`（不需要`x`权限）
    + 这两种方式相当于在当前`shell`下执行，不会启动新的`shell`

  ```
  [root@learn1 scripts]# ./hello.sh 
  hello world!
  [root@learn1 scripts]# bash hello.sh 
  hello world!
  [root@learn1 scripts]# sh hello.sh 
  hello world!
  [root@learn1 scripts]# source hello.sh 
  hello world!
  [root@learn1 scripts]# . hello.sh 
  hello world!
  ```

#### 2、变量

+ 全局变量，对所有`shell`有效；局部变量只对所在`shell`有效，但子`shell`无效

+ 常用系统变量

  + `$HOME`，`$PWD`，`$SHELL`，`$USER`等

+ 查看所有系统全局变量：`env | less`或`printenv | less`

+ 查看系统所有变量：`set | less`，还包括局部变量和用户自定义变量

+ 自定义变量语法：`变量名=变量值`，`=`前后不能有空格

  ```
  [root@learn1 scripts]# a=2
  [root@learn1 scripts]# echo $a
  2
  [root@learn1 scripts]# my_var=hello
  [root@learn1 scripts]# echo $my_var 
  hello
  [root@learn1 scripts]# my_var=hello world
  bash: world: 未找到命令...
  [root@learn1 scripts]# my_var="hello world"
  [root@learn1 scripts]# echo $my_var 
  hello world
  [root@learn1 scripts]# set | grep my_var
  my_var='hello world'
  ```

+ 定义全局变量：`export 变量名`，不用加`$`

  + 如果在子`shell`里对全局变量进行了修改，不会影响到父`shell`

    ```
    [root@learn1 scripts]# echo $my_var 
    hello world
    [root@learn1 scripts]# bash
    [root@learn1 scripts]# echo $my_var 
    hello world
    [root@learn1 scripts]# my_var=123
    [root@learn1 scripts]# echo $my_var 
    123
    [root@learn1 scripts]# exit
    exit
    [root@learn1 scripts]# echo $my_var 
    hello world
    [root@learn1 scripts]# my_var=456
    [root@learn1 scripts]# echo $my_var 
    456
    [root@learn1 scripts]# bash
    [root@learn1 scripts]# echo $my_var 
    456
    [root@learn1 scripts]# my_var=HELLO
    [root@learn1 scripts]# export my_var
    [root@learn1 scripts]# exit
    exit
    [root@learn1 scripts]# echo $my_var 
    456
    ```

+ 定义只读变量：`readonly 变量名=值`，例如`readonly b=5`

  ```
  [root@learn1 scripts]# readonly a=5
  [root@learn1 scripts]# a=6
  -bash: a: 只读变量
  ```

+ `set | grep -n a=5`

  ```
  [root@learn1 scripts]# set | grep -n a=5
  84:a=5
  ```

+ 撤销变量：`unset 变量名`

  + 只读变量不能`unset`

+ 将脚本放到`$PATH`中可以直接把脚本作为命令执行

  ```
  [root@learn1 scripts]# echo $PATH
  /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
  ```

+ 特殊变量

  + `$n`，`n`为数字，`$0`代表该脚本名称，`$1-$9`代表第一到第九个参数，十以上的参数需要使用花括号包含，例如`${10}`

  ```shell
  [root@learn1 scripts]# cat hello.sh 
  #!/bin/bash
  
  echo -e "hello, $1\nworld"
  [root@learn1 scripts]# ./hello.sh xiaoming
  hello, xiaoming
  world
  ```

  ```sh
  [root@learn1 scripts]# cat hello2.sh 
  #!/bin/bash
  
  echo -e '==========$n=========='
  echo script name: $0
  echo 1st paramter: $1
  echo 2st paramter: $2
  [root@learn1 scripts]# ./hello2.sh abc def
  ==========$n==========
  script name: ./hello2.sh
  1st paramter: abc
  2st paramter: def
  [root@learn1 scripts]# ../scripts/hello2.sh abc def
  ==========$n==========
  script name: ../scripts/hello2.sh
  1st paramter: abc
  2st paramter: def
  ```

  + `$#`，获取所有输入参数个数，常用于循环，判断参数个数是否正确以及加强脚本的健壮性
  + `$*`，获取命令行中的所有参数，即把所有参数看做一个整体
  + `$@`，获取命令行中的所有参数，但把每个参数区分对待
  + `$_`，上一个命令的最后一个参数
  + `$!`，后台运行的最后一个进程的 ID 号

  ```
  ==========$n==========
  script name: ../scripts/hello2.sh
  1st paramter: abc
  2st paramter: def
  ==========$#==========
  paramter numbers: 2
  ==========$*==========
  paramter: abc def
  ==========$@==========
  paramter: abc def
  ```

+ `$?`，最后一次执行命令的返回状态，如果值为0，表示是上一个命令正确执行，否则不正确

  ```
  [root@learn1 scripts]# ls
  hello2.sh  hello.sh
  [root@learn1 scripts]# echo $?
  0
  [root@learn1 scripts]# unset a
  -bash: unset: a: 无法反设定: 只读 variable
  [root@learn1 scripts]# echo $?
  1
  ```

+ `shift` 命令用于对参数的移动(左移)，通常用于在不知道传入参数个数的情况下依次遍历每个参数然后进行相应处理,常见于 Linux 中各种程序的启动脚本。
  + 默认每次移动一个参数
  + 

#### 3、运算符

+ 使用`$(())`或`$[]`

  ```
  [root@learn1 scripts]# expr 1 + 2
  3
  [root@learn1 scripts]# expr 1 - 2
  -1
  [root@learn1 scripts]# expr 1 / 2
  0
  [root@learn1 scripts]# expr 2 / 2
  1
  [root@learn1 scripts]# expr 2 % 2
  0
  [root@learn1 scripts]# expr 2 % 3
  2
  [root@learn1 scripts]# expr 2 \* 3
  6
  [root@learn1 scripts]# echo $[5*2]
  10
  [root@learn1 scripts]# echo $((5*2))
  10
  [root@learn1 scripts]# b = `expr 1 + 2`
  bash: b: 未找到命令...
  [root@learn1 scripts]# b=`expr 1 + 2`
  [root@learn1 scripts]# echo $b
  3
  [root@learn1 scripts]# b=$(expr 1 + 3)
  [root@learn1 scripts]# echo $b
  4
  [root@learn1 scripts]# b=$[(2+3)*4]
  [root@learn1 scripts]# echo $b
  20
  ```

  + 19行叫命令替换

  ```
  [root@learn1 scripts]# cat add.sh 
  #!/bin/bash
  
  echo $[$1+$2]
  
  [root@learn1 scripts]# bash add.sh 1 2
  3
  ```

#### 4、条件判断

+ `test condition`

  + `[ condition ]`，`condition`前后要有空格

  ```
  [root@learn1 scripts]# test $a = 5
  [root@learn1 scripts]# echo $?
  0
  [root@learn1 scripts]# test $a = 6
  [root@learn1 scripts]# echo $?
  1
  [root@learn1 scripts]# [ $a = 5 ]
  [root@learn1 scripts]# echo $?
  0
  [root@learn1 scripts]# [ $a = 6 ]
  [root@learn1 scripts]# echo $?
  1
  ```

  + `=`前后必须要有空格

+ 判断条件

  + 整数比较：

    + `-eq`，等于
    + `-ne`，不等于
    + `-lt`，小于
    + `-le`，小于等于
    + `-gt`，大于
    + `-ge`，大于等于

    ```
    [root@learn1 ~]# [ 1 -eq 2 ]
    [root@learn1 ~]# echo $?
    1
    [root@learn1 ~]# [ 1 -le 2 ]
    [root@learn1 ~]# echo $?
    0
    [root@learn1 ~]# [ 1 -ge 2 ]
    [root@learn1 ~]# echo $?
    1
    [root@learn1 ~]# [ 1 -ne 2 ]
    [root@learn1 ~]# echo $?
    0
    [root@learn1 ~]# [ 1 -lt 2 ]
    [root@learn1 ~]# echo $?
    0
    [root@learn1 ~]# [ 1 -gt 2 ]
    [root@learn1 ~]# echo $?
    1
    ```

    

  + 字符串比较：`=`和`!=`

    ```
    [root@learn1 ~]# [ abc = abc ]
    [root@learn1 ~]# echo $?
    0
    [root@learn1 ~]# [ abc != abc ]
    [root@learn1 ~]# echo $?
    1
    ```

  + 按照文件权限进行判断：

    + `-r`，有读权限
    + `-w`，有写权限
    + `-x`，有可执行权限

    ```
    [root@learn1 scripts]# [ -x hello2.sh ]
    [root@learn1 scripts]# echo $?
    0
    [root@learn1 scripts]# [ -x add.sh ]
    [root@learn1 scripts]# echo $?
    1
    ```

    + `-e`，文件是否存在
    + `-f`，文件存在并且是一个常规文件
    + `-d`，文件存在并且是一个目录

    ```
    [root@learn1 scripts]# [ -e xxx ]
    [root@learn1 scripts]# echo $?
    1
    [root@learn1 scripts]# [ -e add.sh ]
    [root@learn1 scripts]# echo $?
    0
    [root@learn1 scripts]# [ -f add.sh ]
    [root@learn1 scripts]# echo $?
    0
    [root@learn1 scripts]# [ -d add.sh ]
    [root@learn1 scripts]# echo $?
    1
    ```

+ 多条件判断：`&&`，`||`

  ```
  [root@learn1 scripts]# a=15
  [root@learn1 scripts]# [ $a -lt 20 ] && echo "$a < 20" || echo "$a >= 20"
  15 < 20
  [root@learn1 scripts]# a=20
  [root@learn1 scripts]# [ $a -lt 20 ] && echo "$a < 20" || echo "$a >= 20"
  20 >= 20
  ```
  + 如果需要在同一个`[]`里表示多条件判断，需要使用`-a`和`-o`（and, or）

  ```shell
  if [ $a -gt 18 -a $a -lt 35 ]; then echo OK; else echo NO OK; fi
  ```

#### 5、流程控制

+ `if`判断

  + ```shell
    if [ 条件判断式 ];then
    	程序
    fi
    ```

  + ```shell
    if [ 条件判断式 ]
    then
    	程序
    fi
    ```

+ 多分支

  + ```shell
    if [ 条件判断式 ]
    then
    	程序
    elif [ 条件判断式 ]
    then
    	程序
    else
    	程序
    fi
    ```

  ```shell
  [root@learn1 scripts]# cat if.sh 
  #!/bin/bash
  
  if [ "$1"x = "man"x ]
  then
  	echo "welcome, man"
  else
  	echo "welcome, woman"
  fi
  [root@learn1 scripts]# . if.sh man
  welcome, man
  [root@learn1 scripts]# . if.sh
  welcome, woman
  ```

+ `case`语句：

  ```shell
  case $变量名 in
  "值1"）
  	如果变量的值等于值1， 则执行程序1
  ;;
  "值2"）
  	如果变量的值等于值2， 则执行程序2
  ;;
  	...省略其他分支...
  *)
  	如果变量的值都不是以上值， 则执行此程序
  esac
  ```

  + `case`行尾必须是单词`in`，每一个模式匹配必须以右括号`)`结束
  + 双分号`;;`表示命令序列结束，相当于`break`
  + 最后的`*)`表示默认模式，相当于`default`

  ```shell
  [root@learn1 scripts]# cat case_test.sh 
  #!/bin/bash
  
  case $1 in
  1)
  	echo "one"
  ;;
  2)
  	echo "two"
  ;;
  3)
  	echo "three"
  ;;
  *)
  	echo "others"
  esac
  
  [root@learn1 scripts]# . case_test.sh 1
  one
  [root@learn1 scripts]# . case_test.sh 4
  others
  ```

+ `for`循环

  ```shell
  for (( 初始值;循环控制条件;变量变化 ))
  do
  	程序
  done
  ```

  ```shell
  [root@learn1 scripts]# cat for_test.sh 
  #!/bin/bash
  
  sum=0
  for (( i=1; i<=$1; i++  ))
  do
  	sum=$[ $sum + $i ]
  done
  echo $sum
  [root@learn1 scripts]# . for_test.sh 100
  5050
  ```

  或者

  ```shell
  for 变量 in 值1 值2 值3 ...
  do
  	程序
  done
  ```

  ```
  [root@learn1 scripts]# for os in linux windows macos; do echo $os; done
  linux
  windows
  macos
  [root@learn1 scripts]# sum=0; for i in {1..100}; do sum=$[ $sum + $i  ]; done; echo $sum
  5050
  ```

+ `$*`和`$@`区别：

  ```shell
  [root@learn1 scripts]# cat for_test2.sh 
  #!/bin/bash
  
  echo '==========$*=========='
  for param in "$*"
  do
  	echo $param
  done
  
  echo '==========$@=========='
  for param in "$@"
  do
  	echo $param
  done
  [root@learn1 scripts]# . for_test2.sh a b c d e f g
  ==========$*==========
  a b c d e f g
  ==========$@==========
  a
  b
  c
  d
  e
  f
  g
  ```

+ `while`循环

  ```shell
  while [ 条件判断式 ]
  do
  	程序
  done
  ```

  ```shell
  [root@learn1 scripts]# cat for_test.sh 
  #!/bin/bash
  
  sum=0
  for (( i=1; i<=$1; i++  ))
  do
  	sum=$[ $sum + $i ]
  done
  echo "for的实现："$sum
  
  a=1
  sum2=0
  while [ $a -le $1  ]
  do
  	sum2=$[ $sum2 + $a  ]
  	a=$[ $a + 1  ]
  done
  echo "while的实现："$sum2
  
  [root@learn1 scripts]# . for_test.sh 100
  for的实现：5050
  while的实现：5050
  ```

+ 一种新的写法：

  ```shell
  while [ $a -le $1  ]
  do
  #       sum2=$[ $sum2 + $a  ]
  #       a=$[ $a + 1  ]
          let sum2+=a
          let a++
  done
  echo "while的实现："$sum2
  ```

  + 第五行`=`前后不能有空格

#### 6、`read`读取控制台输入

+ `read (选项) (参数)`

  + `-p`，指定读取值时的提示符
  + `-t`，指定读取值时等待的时间（秒），如果`-t`不指定则一直等
  + 参数：变量，指定读取值的变量名

  ```shell
  [root@learn1 scripts]# cat read_test.sh 
  #!/bin/bash
  
  read -t 10 -p "请输入您的昵称：" name
  echo "welcome, $name"
  [root@learn1 scripts]# . read_test.sh 
  请输入您的昵称：华韵流风
  welcome, 华韵流风
  ```

#### 7、系统函数

+ `basename [string/pathname] [suffix]`，删掉所有的前缀包括最后一个`('/')`字符，然后将字符串显示出来

  + `basename`可以理解为取路径里的文件名称
  + `suffix`为后缀，如果`suffix`被指定了，`basename`会将`pathname`或`string`中的`suffix`去掉

  ```shell
  [root@learn1 scripts]# cat fun_test.sh 
  #!/bin/bash
  
  filename="$1"_log_$(date +%s)
  echo $filename
  [root@learn1 scripts]# . fun_test.sh hylf
  hylf_log_1659274743
  ```

  ```shell
  [root@learn1 scripts]# basename /home/zq/test/scripts/hello2.sh .sh
  hello2
  [root@learn1 scripts]# basename /home/zq/test/scripts/hello2.sh
  hello2.sh
  ```

+ `dirname 文件绝对路径`，从给定的包含绝对路径的文件名中去除文件名（非目录的部分），然后返回目录的部分。可以理解为取文件路径的绝对路径名称

  ```shell
  [root@learn1 scripts]# dirname /home/xx/test/scripts/hello.sh 
  /home/xx/test/scripts
  [root@learn1 scripts]# dirname ./hello.sh 
  .
  ```

  ```shell
  echo script dir: $(cd $(dirname $0); pwd)
  ```

#### 8，自定义函数

+ 基本语法

  ```shell
  [ function ] funname(){
  	Action;
  	[return int;]
  }
  ```

  + 在调用函数的地方之前，先声明函数
  + 函数返回值，只能通过`$?`来获得，可以显示加：`return`返回，如果不加，将以最后一条命令运行结果作为返回值。`return`后跟数值`n(0-255)`
  + 参数的传递和使用和脚本相同（`$1，$2，……`）

  ```sh
  [root@learn1 scripts]# cat fun.sh
  #!/bin/bash
  
  function add(){
  	s=$[ $1 + $2  ]
  	echo "和："$s
  }
  
  read -p "请输入第一个整数：" a
  read -p "请输入第二个整数：" b
  
  add $a $b
  [root@learn1 scripts]# . fun.sh
  请输入第一个整数：1
  请输入第二个整数：10
  和：11
  ```

+ 获取返回值的一种思路：把自定义函数作为系统函数看待，`$()`

  ```shell
  [root@learn1 scripts]# cat fun.sh
  #!/bin/bash
  
  function add(){
  	s=$[ $1 + $2  ]
  	echo $s
  }
  
  read -p "请输入第一个整数：" a
  read -p "请输入第二个整数：" b
  
  sum=$(add $a $b)
  echo "和："$sum
  echo "和的平方："$[ $sum * $sum ]
  [root@learn1 scripts]# . fun.sh
  请输入第一个整数：10
  请输入第二个整数：10
  和：20
  和的平方：400
  ```

### 八、实际应用

#### 1、归档文件

```shell
#!/bin/bash

# 判断输入参数个数是否为1
if [ $# -ne 1 ]
then
	echo "参数个数错误，应该是1"
	exit
fi

# 从参数中获取目录名称
if [ -d $1 ]
then
	echo
else
	echo
	echo "目录不存在!"
	echo
	exit
fi

DIR_NAME=$(basename $1)
DIR_PATH=$(cd $(dirname $1); pwd)
# 获取当前日期
DATE=$(date +%Y%m%d)

# 定义生成的归档文件名称
FILE=archive_${DIR_NAME}_$DATE.tar.gz
DEST=/root/archive/$FILE

# 开始归档

echo "开始归档……"
echo

tar -zcf $DEST $DIR_PATH/$DIR_NAME

if [ $? -eq 0 ]
then
	echo
	echo "归档成功!"
	echo "归档文件为：$DEST"
	echo
else
	echo "归档失败!"
	echo
fi

exit
```

+ 设置一个定时任务，每天凌晨两点执行

  ```shell
  
  0 2 * * * /root/scripts/daily_archive.sh /home/xxx/test/scripts
  ```


#### 2、发送消息

+ 实现一个向某个用户快速发送消息的脚本，输入用户名作为第一个参数，后面直接跟要发送的消息。脚本需要检测用户是否登录在系统中，是否打开消息功能，以及当前发送消息是否为空。

  ```shell
  [root@learn1 test]# mesg
  is y
  [root@learn1 test]# who -T
  root     + pts/0        2022-08-02 21:23 (192.168.46.254)
  zq       + pts/1        2022-08-02 22:30 (192.168.46.254)
  [root@learn1 test]# mesg n
  [root@learn1 test]# who -T
  root     - pts/0        2022-08-02 21:23 (192.168.46.254)
  zq       + pts/1        2022-08-02 22:30 (192.168.46.254)
  [root@learn1 test]# mesg y
  [root@learn1 test]# who -T
  root     + pts/0        2022-08-02 21:23 (192.168.46.254)
  zq       + pts/1        2022-08-02 22:30 (192.168.46.254)
  [root@learn1 test]# write zq pts/1
  hi, zq
  hello
  ^[[A
  ^C[root@learn1 test]# 
  ```

  ```shell
  #!/bin/bash
  
  # 查看用户是否登录
  login_user=$(who | grep -i -m 1 $1 | awk '{print $1}')
  if [ -z $login_user ]
  then
  	echo "$1不在线!"
  	echo "脚本退出.."
  	exit
  fi
  
  # 查看用户是否开启消息功能
  is_allowed=$(who -T | grep -i -m 1 $1 | awk '{print $2}')
  if [ $is_allowed != "+" ]
  then
  	echo "$1没有开启消息功能!"
  	echo "脚本退出.."
  	exit
  fi
  
  # 确认是否有消息发送
  if [ -z $2 ]
  then
  	echo "$1没有消息发送!"
  	echo "脚本退出.."
  	exit
  fi
  
  # 从参数中获取要发送的信息
  whole_msg=$(echo $* | cut -d " " -f 2-)
  
  # 获取用户登录的终端
  user_terminal=$(who | grep -i -m 1 $1 | awk '{print $2}')
  
  # 写入要发送的消息
  echo $whole_msg | write $login_user $user_terminal
  
  if [ $? != 0 ]
  then
  	echo "发送失败!"
  else
  	echo "发送成功!"
  fi
  
  exit
  ```

  ```
  [root@learn1 scripts]# ./sendmessage.sh zq boss is coming
  发送成功!
  [zq@learn1 ~]$ 
  Message from root@learn1 on pts/0 at 22:50 ...
  boss is coming
  EOF
  
  [zq@learn1 ~]$ 
  ```

### 九、文本处理工具

#### 1、`cut`

+ 剪切数据，从文件的每一行剪切字节、字符和字段并将其输出

+ `cut [选项] filename`

  | 选项 |                       功能                        |
  | :--: | :-----------------------------------------------: |
  | `-f` |                 列号，提取第几列                  |
  | `-d` |     分隔符，按照指定分隔符分割列，默认是`\t`      |
  | `-c` | 按字符进行切割，后加`n`表示提取第几列，例如`-c 1` |

  ```shell
  [root@learn1 test]# cat cut.txt 
  dong shen
  guan zhen
  wo wo
  lai lai
  le le
  [root@learn1 test]# cut -d " " -f 1 cut.txt 
  dong
  guan
  wo
  lai
  le
  [root@learn1 test]# cut -d " " -f 1,2 cut.txt 
  dong shen
  guan zhen
  wo wo
  lai lai
  le le
  ```

  ```shell
  [root@learn1 test]# cat /etc/passwd | grep bash$
  root:x:0:0:root:/root:/bin/bash
  zq:x:1000:1000:zq:/home/zq:/bin/bash
  xiaoming:x:1001:1001::/home/xiaoming:/bin/bash
  xiaoliang:x:1002:1001::/home/xiaoliang:/bin/bash
  xiaohong:x:1003:1002::/home/xiaohong:/bin/bash
  xiaolan:x:1004:1002::/home/xiaolan:/bin/bash
  [root@learn1 test]# cat /etc/passwd | grep bash$ | cut -d ":" -f 1,6,7
  root:/root:/bin/bash
  zq:/home/zq:/bin/bash
  xiaoming:/home/xiaoming:/bin/bash
  xiaoliang:/home/xiaoliang:/bin/bash
  xiaohong:/home/xiaohong:/bin/bash
  xiaolan:/home/xiaolan:/bin/bash
  [root@learn1 test]# cat /etc/passwd | grep bash$ | cut -d ":" -f 1,6-
  root:/root:/bin/bash
  zq:/home/zq:/bin/bash
  xiaoming:/home/xiaoming:/bin/bash
  xiaoliang:/home/xiaoliang:/bin/bash
  xiaohong:/home/xiaohong:/bin/bash
  xiaolan:/home/xiaolan:/bin/bash
  [root@learn1 test]# cat /etc/passwd | grep bash$ | cut -d ":" -f 1,4-7
  root:0:root:/root:/bin/bash
  zq:1000:zq:/home/zq:/bin/bash
  xiaoming:1001::/home/xiaoming:/bin/bash
  xiaoliang:1001::/home/xiaoliang:/bin/bash
  xiaohong:1002::/home/xiaohong:/bin/bash
  xiaolan:1002::/home/xiaolan:/bin/bash
  [root@learn1 test]# cat /etc/passwd | grep bash$ | cut -d ":" -f -4
  root:x:0:0
  zq:x:1000:1000
  xiaoming:x:1001:1001
  xiaoliang:x:1002:1001
  xiaohong:x:1003:1002
  xiaolan:x:1004:1002
  ```

+ 获取`ip`地址

  ```shell
  [root@learn1 test]# ifconfig ens33
  ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1280
          inet 192.168.46.10  netmask 255.255.255.0  broadcast 192.168.46.255
          inet6 fe80::bd0e:10b6:bc41:f176  prefixlen 64  scopeid 0x20<link>
          ether 00:0c:29:78:6b:9d  txqueuelen 1000  (Ethernet)
          RX packets 2460  bytes 170682 (166.6 KiB)
          RX errors 0  dropped 0  overruns 0  frame 0
          TX packets 857  bytes 102648 (100.2 KiB)
          TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
  
  [root@learn1 test]# ifconfig ens33 | grep netmask
          inet 192.168.46.10  netmask 255.255.255.0  broadcast 192.168.46.255
  [root@learn1 test]# ifconfig ens33 | grep netmask | cut -d " " -f 10
  192.168.46.10
  [root@learn1 test]# ifconfig| grep netmask | cut -d " " -f 10
  192.168.46.10
  127.0.0.1
  192.168.122.1
  ```

#### 2、`awk`

+ 把文件逐行的读入，以空格为默认分隔符将每行切片，切开的部分再进行分析处理

  ```shell
  [root@learn1 test]# which awk 
  /usr/bin/awk
  [root@learn1 test]# ll /usr/bin/ | grep awk
  lrwxrwxrwx. 1 root root          4 7月  24 11:49 awk -> gawk
  -rwxr-xr-x. 1 root root     514168 6月  29 2017 dgawk
  -rwxr-xr-x. 1 root root     428584 6月  29 2017 gawk
  -rwxr-xr-x. 1 root root       3188 6月  29 2017 igawk
  -rwxr-xr-x. 1 root root     428672 6月  29 2017 pgawk
  ```

+ `awk [选项参数] '/pattern1/{action1} /pattern2/{action2}...' filename`

  + `pattern`，表示`awk`在数据中查找的内容，就是匹配模式
  + `action`，在找到匹配内容时所执行的一系列命令
  + `-F`，指定输入文件分隔符
  + `-v`，赋值一个用户定义变量

  ```shell
  [root@learn1 test]# cat /etc/passwd | grep ^root | cut -d ":" -f 7
  /bin/bash
  [root@learn1 test]# cat /etc/passwd | awk -F ":" '/^root/{print $7}'
  /bin/bash
  [root@learn1 test]# cat /etc/passwd | awk -F ":" '/^root/{print $1","$7}'
  root,/bin/bash
  [root@learn1 test]# cat /etc/passwd | awk -F ":" 'BEGIN{print "user,shell"}/^root/{print $1","$7}END{print "end of file"}'
  user,shell
  root,/bin/bash
  end of file
  [root@learn1 test]# cat /etc/passwd | awk -F ":" '{print $3+1}'
  ```

+ `awk`的内置变量

  + `FILENAME`，文件名
  + `NR`，已读的记录数（行号）
  + `NF`，浏览记录的域的个数（切割后，列的个数）

  ```shell
  [root@learn1 test]# cat /etc/passwd | awk -v i=100 -F ":" '{print $3+i}'
  [root@learn1 test]# awk -F ":" '/^root/{print "文件名："FILENAME" 行号："NR" 列数："NF}' /etc/passwd
  文件名：/etc/passwd 行号：1 列数：7
  [root@learn1 test]# ifconfig | grep -n ^$
  9:
  18:
  26:
  [root@learn1 test]# ifconfig | awk '/^$/{print NR}'
  9
  18
  26
  [root@learn1 test]# ifconfig | awk '/netmask/{print $2}'
  192.168.46.10
  127.0.0.1
  192.168.122.1
  ```


### 十、常用功能

#### 1、配置免密登录

- 普通用户的`.ssh`目录在`/home/用户名/.ssh`下，`root`用户在`/root/.ssh`下。

- 在任意地方使用`ssh-keygen -t rsa`生成密钥对，第一个选项指定生成的位置和文件名（建议都放在默认的`.ssh`目录下），如果已经存在默认为`id_rsa`和`id_rsa.pub`的文件，建议更改文件名；紧跟着的是密钥对的密码和确认密码，可为空。

- 将公钥上传到服务器中，重命名为`authorized_keys`，

  - 说明：在`/etc/ssh/sshd_config`中可以更改此名称，默认是：`AuthorizedKeysFile      .ssh/authorized_keys`

  - 如果是`root`用户需要免密登录，需要更改配置：

    ```
    PermitRootLogin yes
    PubkeyAuthentication yes
    ```

- 如果默认的`.ssh`目录下已经有了`id_rsa`文件，在使用`ssh`命令登录时需要指定一个密钥：

  ```
  ssh -i 密钥路径 用户名@ip
  ```

  - 否则可以不加`-i`参数。

- 如果需要服务器自己对自己免密登录，可以将私钥也传到服务器的`.ssh`目录下，登录时同样的操作。

  - 注意：该私钥的权限也应该是`600`

