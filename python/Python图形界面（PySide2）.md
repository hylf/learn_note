### 一、`pyside2`

+ `pip install pyside2`
+ <https://www.byhy.net/tut/py/gui/qt_05_1/>

```python
from PySide2.QtWidgets import QApplication, QMainWindow, QPushButton, QPlainTextEdit

app = QApplication([])

window = QMainWindow()
window.resize(500, 400)
window.move(300, 310)
window.setWindowTitle("薪资统计")

text_edit = QPlainTextEdit(window)
text_edit.setPlaceholderText("请输入薪资表：")
text_edit.move(10, 25)
text_edit.resize(300, 350)

button = QPushButton("统计", window)
button.move(380, 80)

window.show()

app.exec_()

```

#### 1、事件

```python
from PySide2.QtWidgets import QApplication, QMainWindow, QPushButton, QPlainTextEdit, QMessageBox
app = QApplication([])


class Stats(object):
    def __init__(self):
        self.window = QMainWindow()
        self.window.resize(500, 400)
        self.window.move(300, 310)
        self.window.setWindowTitle("薪资统计")

        self.text_edit = QPlainTextEdit(self.window)
        self.text_edit.setPlaceholderText("请输入薪资表：")
        self.text_edit.move(10, 25)
        self.text_edit.resize(300, 350)

        self.button = QPushButton("统计", self.window)
        self.button.move(380, 80)
        self.button.clicked.connect(self.handle_calc)

    def __enter__(self):
        self.window.show()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.window.close()

    def handle_calc(self):
        info = self.text_edit.toPlainText()

        salary_above_20k = ''
        salary_below_20k = ''
        for line in info.splitlines():
            if not line.strip():
                continue
            parts = line.split(" ")
            parts = [p for p in parts if p]
            name, salary, age = parts
            if int(salary) >= 20000:
                salary_above_20k += name + "\n"
            else:
                salary_below_20k += name + "\n"
        QMessageBox.about(self.window, "统计结果", f"薪资20k以上的有:\n{salary_above_20k}\n薪资20k以下的有: \n{salary_below_20k}")


with Stats() as stats:
    app.exec_()

```

#### 2、界面生成器

+ `Qt Designer`，路径为：`venv\Lib\site-packages\PySide2\designer.exe`
+ 逻辑和界面分离

```python
from PySide2.QtWidgets import QApplication, QMessageBox
from PySide2.QtUiTools import QUiLoader
from PySide2.QtCore import QFile
app = QApplication([])


class Stats(object):

    def __init__(self):
        # 从文件中加载UI定义
        qfile_stats = QFile("../ui/stats.ui")
        qfile_stats.open(QFile.ReadOnly)
        qfile_stats.close()

        # 从UI定义中动态创建一个相应的窗体对象
        # 里面的控件对象也成为窗口对象的属性
        # 比如 self.ui.button, self.ui.text_edit
        self.ui = QUiLoader().load(qfile_stats)
        self.ui.button.clicked.connect(self.handle_calc)

    def __enter__(self):
        self.ui.show()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.ui.close()

    def handle_calc(self):
        info = self.ui.text_edit.toPlainText()
        salary_above_20k = ''
        salary_below_20k = ''
        for line in info.splitlines():
            if not line.strip():
                continue
            parts = line.split(" ")
            parts = [p for p in parts if p]
            name, salary, age = parts
            if int(salary) >= 20000:
                salary_above_20k += name + "\n"
            else:
                salary_below_20k += name + "\n"
        QMessageBox.about(self.ui, "统计结果", f"薪资20k以上的有:\n{salary_above_20k}\n薪资20k以下的有: \n{salary_below_20k}")


with Stats() as stats:
    app.exec_()

```

#### 3、界面布局

+ `QHBoxLayout`，水平布局
+ `QVBoxLayout`，垂直布局
+ `QGraidLayout`，表格布局
+ `QFormLayout`，表单布局

#### 4、发布程序

+ `pyinstaller xxx --noconsole --hidden-import PySide2.QtXml`

#### 5、技巧

+ 多线程任务
  + 进行比较耗时的操作时创建子线程，否则界面会僵死
+ 只通过主线程操作界面，子线程不要直接操作界面

