### 一、字符串学习

1. ```
   ''' 多行注释，或者表示字符串'''
   """"""
   ""
   ''
   以上都表示字符串
   ```

2. <字符串>[M:N:K]，M缺失表示至开头，N缺失表示至结尾，K表示步长（-1表示逆序）。

3. Python 可以使用 ** 操作来进行幂运算。

4. `str()`函数，将任意类型转换为对应的字符串形式。

5. `hex(x)`，`oct(x)`，`bin(x)`，返回整数**x**的**16**进制或**8**进制或**2**进制小写形式字符串。

6. `chr(u)`，**u**为`Unicode`编码，返回其对应的字符。

7. `ord(x)`，**x**为字符，返回其对应的`Unicode`编码

   ```python
   # 8个常用的字符串方法
   strSta = "Hello Python3!"
   # 全大写
   print(strSta.upper())
   # 全小写
   print(strSta.lower())
   # 字符串分割
   strList = strSta.split(" ")
   print(strList[0], strList[1])
   print(str(strList))
   # 统计字串出现次数
   print(strSta.count("o"))
   # 字符串替换
   print(strSta.replace("!", "."))
   # 字符串根据宽度居中，默认是" "填充
   print(strSta.center(30, ">"))
   # 从字符串中去掉左侧和右侧chars中列出的字符
   print(strSta.strip(" 3l!He "))
   # 在给定字符串iter除最后元素外每个元素后增加一个str，用法str.join(iter)
   print("->".join("12345"))
   ```

8. ```python
   """
   槽内部对格式化的配置方式：
   : 为引导符号
   <填充>，用于填充的单个字符
   <对齐>，<左对齐，>右对齐，^居中对齐
   <宽度>，槽设定的输出宽度
   <,>，数字的千位分隔符
   <.精度>，浮点数小数精度或字符串最大输出长度
   <类型>，整数类型b,c,d,o,x,X
   """
   # 0表示填充哪一个，=表示填充的字符，默认填充空格，^表示居中对齐，20表示宽度
   print("{1:=^30}".format("123", "234"))
   # ,加上千位分隔符，.2f指定小数位数
   print("{0:,.2f}".format(123456.789))
   # b表示二进制，c表示显示对应Unicode的字符，d表示十进制整数，o表示八进制，x表示小写16进制，X表示大写16进制
   print("{0:b},{0:c},{0:d},{0:o},{0:x},{0:X}".format(425))
   # e为科学计数法小写表示浮点数，E为科学计数法大写表示浮点数，f为非科学计数法表示浮点数，%为百分小数表示
   print("{0:e},{0:E},{0:f},{0:.2%}".format(0.01))
   ```

9. `python`可以对字符串进行`*`操作，表示字符串被复制的次数。

   ```python
   print('+=' * 10)
   # +=+=+=+=+=+=+=+=+=+=
   ```

### 二、Time库的使用

- 时间获取：`time()`，`ctime()`，`gmtime()`。

- 时间格式化：`strftime()`，`strptime()`。

- 程序计时：`sleep()`，`perf_counter`。

- `time()`，获取当前时间戳，返回浮点数。

- `ctime()`，获取当前时间并以易读的方式表示，返回字符串。

  ```python
  import time
  
  print(time.time())
  # 可以指定时间，flot格式
  print(time.ctime())
  # 可以指定时间，flot格式
  print(time.gmtime())
  
  # 1651108926.049349
  # Thu Apr 28 09:22:06 2022   
  # time.struct_time(tm_year=2022, tm_mon=4, tm_mday=28, tm_hour=1, tm_min=24, tm_sec=51, tm_wday=3, tm_yday=118, tm_isdst=0)
  ```

- `strftime(format,t)`，**format**是格式化模板字符串，用来定义输出效果，**t**是计算机内部时间类型变量。

  ```python
  print(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
  # 2022-04-28 01:39:45
  ```

##### 1、常用的时间格式化控制符：

- `%Y`，年份，0000~9999；

- `%m`，月份，01~12；

- `%B`，月份名称，January~December；

- `%b`，月份名称缩写，Jan~Dec；

- `%d`，日期，01~31；

- `%A`，星期，Monday~Sunday；

- `%a`，星期缩写，Mon~Sun；

- `%H`，小时（24H），00~23；

- `%I`，小时（12H），01~12；

- `%p`，上/下午，AM/PM；

- `%M`，分钟，00~59；

- `%S`，秒，00~59。

- `strptime(str,format)`，将字符串转换为时间。

  ```python
  import time
  from datetime import datetime, timedelta, timezone
  
  print(time.time())
  print(time.ctime())
  timeNow = time.gmtime()
  print(timeNow)
  print(time.strftime("%Y-%m-%d %H:%M:%S %b %h %I", time.gmtime()))
  
  SHA_TZ = timezone(
      timedelta(hours=8),
      name='Asia/Shanghai',
  )
  # 协调世界时
  utc_now = datetime.utcnow().replace(tzinfo=timezone.utc)
  print("UTC:")
  print(utc_now, utc_now.time())
  print(utc_now.date(), utc_now.tzname())
  # 北京时间
  beijing_now = utc_now.astimezone(SHA_TZ)
  print("Beijing:")
  print(beijing_now)
  print(beijing_now.strftime('%Y-%m-%d %H:%M:%S'))
  
  # 系统默认时区
  local_now = utc_now.astimezone()
  print("Default:")
  print(local_now, local_now.time())
  print(local_now.date(), local_now.tzname())
  print(time.strptime("2022-4-28 10:6:20", "%Y-%m-%d %H:%M:%S"))
  """
  1651112196.4294934
  Thu Apr 28 10:16:36 2022
  time.struct_time(tm_year=2022, tm_mon=4, tm_mday=28, tm_hour=2, tm_min=16, tm_sec=36, tm_wday=3, tm_yday=118, tm_isdst=0)
  2022-04-28 02:16:36 Apr Apr 02
  UTC:
  2022-04-28 02:16:36.429493+00:00 02:16:36.429493
  2022-04-28 UTC
  Beijing:
  2022-04-28 10:16:36.429493+08:00
  2022-04-28 10:16:36
  Default:
  2022-04-28 10:16:36.429493+08:00 10:16:36.429493
  2022-04-28 中国标准时间
  time.struct_time(tm_year=2022, tm_mon=4, tm_mday=28, tm_hour=10, tm_min=6, tm_sec=20, tm_wday=3, tm_yday=118, tm_isdst=-1)
  """
  ```

- `perf_counter()`，返回一个CPU级别的精确时间计数值，单位为秒。连续调用才有意义。

- `sleep(s)`，程序休眠**s**秒时间，可以是浮点数。

##### 2、线程休眠

```python
import time

start = time.perf_counter()
print(start)

time.sleep(10)

end = time.perf_counter()
print(end)

print(end - start)
"""
523835.1563316
523845.1638386
10.007507000002079
"""
```

##### 3、文本进度条：

```python
import time

scale = 10
print("------执行开始------")
for i in range(scale + 1):
    a = '*' * i
    b = '.' * (10 - i)
    c = i / scale
    print("{:^4.0%}[{}->{}]".format(c, a, b))
    time.sleep(1)
print("------执行结束------")
"""
------执行开始------
 0% [->..........]
10% [*->.........]
20% [**->........]
30% [***->.......]
40% [****->......]
50% [*****->.....]
60% [******->....]
70% [*******->...]
80% [********->..]
90% [*********->.]
100%[**********->]
------执行结束------
"""
```

- `\r`可以将光标退回到之前的位置，在`print()`中使用`end=""`可以让其不换行。

  ```python
  import time
  
  for i in range(101):
      print("\r{:3}%".format(i), end="")
      time.sleep(0.5)
  ```

##### 4、文本进度条进阶：

```python
import time

width = 50

print("开始".center(width // 2, "-") + "\n")
start = time.perf_counter()
for i in range(width + 1):
    a = '=' * i
    b = '_' * (width - i)
    c = i / width
    dur = time.perf_counter() - start
    print("\r{0:^4.0%}[{1}{2}]{3:.2f}s".format(c, a, b, dur), end="")
    time.sleep(0.2)
print("\n\n" + "结束".center(width // 2, "-"))
"""
------------开始-----------

100%[==================================================]10.37s

------------结束-----------
"""
```

##### 5、不同文本进度条的设计函数：

- `Linear`，趋势：**Constant**，函数：$f(x)=x$；
- `Early Pause`，趋势：**Speeds up**，函数：$f(x)=x-\frac{1-sin(2\pi x+\frac{\pi}{2}}{8}$；
- `Late Pause`，趋势：**Slows down**，函数：$f(x)=x+\frac{1-sin(2\pi x+\frac{\pi}{2}}{8}$；
- `Slow Wavy`，趋势：**Constant**，函数：$f(x)=x+\frac{sin(5\pi x)}{20}$；
- `Fast Wavy`，趋势：**Constant**，函数：$f(x)=x+\frac{sin(20\pi x)}{80}$；
- `Power`，趋势：**Speeds up**，函数：$f(x)=(x+0.03(1-x))^2$；
- `Inverse Power`，趋势：**Slows down**，函数：$f(x)=1-(1-x)^{1.5}$；
- `Fast Power`，趋势：**Speeds up**，函数：$f(x)=(x+\frac{1-x}{2})^8$；
- `Inverse Fast Power`，趋势：**Slows down**，函数：$f(x)=1-(1-x)^3$。

##### 6、文本进度条终极版：

```python
import time
import math

width = 50
speed = 0.01


# Constant
def linear(x):
    return x


# speeds up
def earlyPause(x):
    return x - (1 - math.sin(x * math.pi * 2 + math.pi / 2)) / 8


# slows down
def latePause(x):
    return x + (1 - math.sin(x * math.pi * 2 + math.pi / 2)) / 8


# constant
def slowWavy(x):
    return x + math.sin(x * math.pi * 5) / 20


# constant
def fastWavy(x):
    return x + math.sin(x * math.pi * 20) / 80


# speeds up
def power(x):
    return (x + (1 - x) * 0.03) ** 2


# slows down
def inversePower(x):
    return 1 + (1 - x) ** 2


# speeds up
def fastPower(x):
    return (x + (1 + x) / 2) ** 8


# slows down
def inverseFastPower(x):
    return 1 - (1 - x) ** 3


def progressType(fun, string):
    start = time.perf_counter()
    for i in range(width + 1):
        a = '=' * i
        c = i / width
        dur = time.perf_counter() - start
        b = '_' * (width - int(fun(i)))
        print("\r{}{:^4.0%}[{}{}]{:.2f}s".format(string.center(20), c, a, b, dur), end="")
        time.sleep(speed)
    print()


progressType(linear, "Linear:")
progressType(earlyPause, "Early Pause:")
progressType(latePause, "Late Pause:")
progressType(slowWavy, "Slow Wavy")
progressType(fastWavy, "Fast Wavy")
progressType(power, "Power")
progressType(inversePower, "Inverse Power")
progressType(fastPower, "Fast Power")
progressType(inverseFastPower, "Inverse Fast Power")
"""
      Linear:       100%[==================================================]0.87s
    Early Pause:    100%[==================================================]0.84s
    Late Pause:     100%[==================================================]0.84s
     Slow Wavy      100%[==================================================]0.85s
     Fast Wavy      100%[==================================================]0.83s
       Power        100%[==================================================]0.90s
   Inverse Power    100%[==================================================]0.84s
     Fast Power     100%[==================================================]0.85s
 Inverse Fast Power 100%[==================================================]0.84s
"""
```

### 三、基础语法

##### 1、分支结构

- 紧凑型式：

  ```py
  guess = int(input())
  # 此写法类似于其它语言的三元表达式：条件?返回true的结果:返回false的结果
  print("猜{}了".format("对" if guess == 99 else "错"))
  ```

  ```python
  import random
  
  randNum = int(random.random() * 100)
  num = int(input())
  while num != randNum:
      print("{}了".format("大" if num > randNum else "小"))
      num = int(input())
  print("猜对了")
  ```

##### 2、异常处理：

```python
# 异常处理

try:
    # 可能会发生异常的操作
    num = int(input())
except ValueError:
    # 发生异常时的操作
    print("发生异常时的操作。")
else:
    # 不发生异常时执行
    print("不发生异常时执行。")
finally:
    # 无论是否发生异常都会执行
    print("无论是否发生异常都会执行。")
```

##### 3、身体质量指数BMI：

```python
inputStr = input("输入身高和体重：").split(" ")
height = eval(inputStr[0])
weight = eval(inputStr[1])

bmi = weight / (height * height)
print("BMI数值为：{:.2f}".format(bmi))
resInter = resChina = ""
if bmi >= 30:
    resInter = "肥胖"
elif bmi >= 25:
    resInter = "偏胖"
elif bmi >= 18.5:
    resInter = resChina = "正常"
else:
    resInter = resChina = "偏瘦"

if bmi >= 28:
    resChina = "肥胖"
elif bmi >= 24:
    resChina = "偏胖"
print("BMI指标为：国际'{0}'，国内'{1}'".format(resInter, resChina))
```

##### 4、循环结构

- ```python
  string = "123456"
  print(",".join(string))
  for i in string:
      print(i, end=",")
  print()    
  for i in ["a", "b", "c"]:
      print(i, end=",")
  ```

- ```python
  string = "123456"
  print(",".join(string))
  for i in string:
      print(i, end=",")
  else:
      # 这一行会被执行
      print("for没有执行break")
  
  while 1 == 1:
      break
  else:
      # 这一行不会被执行
      print("while没有执行break")
  ```

### 四、random库

- 随机数种子，梅森旋转算法。只要种子相同，产生的随机数序列及其之间的关系是相同的。

- 函数`seed(a=None)`，初始化给定种子，默认为系统当前时间。

- `random`，生成一个`[0.0，1.0]`之间的随机数。

- `randint(a，b)`，生成一个`[a，b]`之间的整数。

- `randrange(m，n[，k])`，生成一个`[m，n]`之间以`k`为步长的随机整数。

- `getrandbits(k)`，生成一个`k`比特长的随机整数。

- `uniform(a，b)`，生成一个`[a，b]`之间的随机小数。

- `choice(seq)`，从序列`seq`中随机选取一个元素。

- `shuffle(seq)`，将序列`seq`中元素随机排列，返回打乱后的序列。

  ```python
  import random
  
  # 生成一个`[0.0，1.0]`之间的随机数
  print(random.random())
  # 生成一个`[a，b]`之间的整数。
  print(random.randint(1, 10))
  # 生成一个`[m，n]`之间以`k`为步长的随机整数。
  print(random.randrange(10, 100, 20))
  # 生成一个`k`比特长的随机整数。
  print(random.getrandbits(8))
  # 生成一个`[a，b]`之间的随机小数。
  print(random.uniform(8, 10))
  # 从序列`seq`中随机选取一个元素。
  print(random.choice([1, 2, 3, 4, 5]))
  # 将序列`seq`中元素随机排列，返回打乱后的序列。
  s = [1, 2, 3, 4, 5]
  random.shuffle(s)
  print(s)
  
  """
  0.6520827650222399
  5
  10
  47
  9.0952362813991
  4
  [1, 4, 3, 2, 5]
  
  """
  ```

##### 1、圆周率的计算方法公式：

$$ \pi=\sum_{k=0}^{\infty}[\frac{1}{16^k}(\frac{4}{8k+1}-\frac{2}{8k+4}-\frac{1}{8k+5}-\frac{1}{8k+6})] $$

```python
pi = 0
N = 100
for k in range(N):
    pi += 1 / 16 ** k * (4 / (8 * k + 1) - 2 / (8 * k + 4) - 1 / (8 * k + 5) - 1 / (8 * k + 6))
print(pi)
"""
3.141592653589793
"""
```

##### 2、蒙特卡洛方法：

```python
import time
import random

DARTS = 1000 * 1000
hits = 0.0
start = time.perf_counter()
for i in range(1, DARTS + 1):
    x, y = random.random(), random.random()
    dist = pow(x * x + y * y, 0.5)
    if dist <= 1.0:
        hits += 1
pi = 4 * (hits / DARTS)
print("圆周率是：{}".format(pi))
print("运行时间是：{:.5f}s".format(time.perf_counter() - start))
"""
圆周率是：3.141212
运行时间是：0.36826s
"""
```

### 五、函数

##### 1、求最大公约数和最小公倍数：

```python
import time


# 辗转相除法求最大公约数
def gcd(a, b):
    return gcd(b, a % b) if b > 0 else a


# 更相减损法求最大公约数
def gcd_msm(a, b):
    if a == b:
        return a
    reduction = 1
    while True:
        if a % 2 == 0 and b % 2 == 0:
            a //= 2
            b //= 2
            reduction *= 2
        else:
            break
    while True:
        minNum = min(a, b)
        maxNum = max(a, b)
        if minNum * 2 == maxNum:
            return minNum * reduction
        else:
            a = minNum
            b = maxNum - minNum


# 求最小公倍数
def lcm(a, b):
    return a * b / gcd(a, b)


x = 501
y = 50000

start = time.perf_counter()
print(gcd(x, y))
end = time.perf_counter()
print(end - start)
start = time.perf_counter()
print(gcd_msm(x, y))
end = time.perf_counter()
print(end - start)
print(lcm(x, y))
"""
1
1.900014467537403e-05
1
4.349998198449612e-05
25050000.0
"""
```

- 定义函数时可以指定形参的默认值。可选参数必须放在必选参数之后。

- 需要使用的函数必须写在前面，后面的函数可以调用前面的，前面的不能调用后面的。

- `*b`来表示可变参数，`b`可以是任意符合命名规范的形参名。

- `**b`表示字典。

- 可以不按照形参顺序传值：`print(gcd(b=x, a=y))`，指定传递是哪一个形参的值即可。

- 函数可以返回多个结果（元组），使用英文逗号分隔，例如：`return a,b,c`。

- 在函数中可以使用`global`来声明使用全局变量，该全局变量。

- 局部变量为组合数据类型且未创建，等同于全局变量。

- `lambda`函数返回函数名作为结果。是一种匿名函数。

  ```python
  fun = lambda x, y: x + y
  fun2 = lambda: "Hello World!"
  print(fun(10, 15))
  print(fun2())
  """
  25
  Hello World!
  """
  ```

##### 2、七段数码管绘制：

```python
import time
import turtle


# 画线，true表示画实线，false表示移动
def drawLine(draw):
    turtle.pendown() if draw else turtle.penup()
    # 向前40px
    turtle.fd(40)
    # 右转90°
    turtle.right(90)


# 根据数字绘制七段数码管
def drawDigit(digit):
    drawLine(True) if digit in [2, 3, 4, 5, 6, 8, 9] else drawLine(False)
    drawLine(True) if digit in [0, 1, 3, 4, 5, 6, 7, 8, 9] else drawLine(False)
    drawLine(True) if digit in [0, 2, 3, 5, 6, 8, 9] else drawLine(False)
    drawLine(True) if digit in [0, 2, 6, 8] else drawLine(False)
    turtle.left(90)
    drawLine(True) if digit in [0, 4, 5, 6, 8, 9] else drawLine(False)
    drawLine(True) if digit in [0, 2, 3, 5, 6, 7, 8, 9] else drawLine(False)
    drawLine(True) if digit in [0, 1, 2, 3, 4, 7, 8, 9] else drawLine(False)
    turtle.left(180)
    turtle.penup()
    turtle.fd(20)


# 获取要输出的日期，入参格式为yyyyMMdd，例如20220506
def drawDate(date):
    for i in date:
        drawDigit(eval(i))


# 完整的绘制，默认绘制当前日期
def run(datetime=time.strftime("%Y%m%d")):
    # 设置窗口大小和位置
    turtle.setup(800, 350, 200, 200)
    # 画笔抬起
    turtle.penup()
    # 向负方向移动
    turtle.fd(-300)
    # 画笔粗细
    turtle.pensize(5)
    drawDate(datetime)
    # 隐藏海龟，即箭头
    turtle.hideturtle()
    # 停止画笔程序，但窗口不关闭
    turtle.done()


run()
```

- ```
  turtle.write(arg，move，align，font(name，size，family))
  ```

  用于在当前位置书写文本。

  - `arg`表示要写入的信息；
  - `move`默认是`False`，即不移动，如果为`True`，则表示画笔随文字移动；
  - `align`表示文字位置（默认是`left`），取值为`left`，`right`或`center`；
  - `font`，设置字体，包含字体名称（默认是`Arial`），字体大小（默认是`8`），字体类型（默认是`normal`）等等。

##### 3、进阶版：

```python
import time
import turtle


# 绘制数码管间隔
def drawGap():
    turtle.penup()
    turtle.fd(5)


# 画线，true表示画实线，false表示移动
def drawLine(draw):
    # 在绘制开始和结束加一个间隔
    drawGap()
    turtle.pendown() if draw else turtle.penup()
    # 向前40px
    turtle.fd(40)
    # 加一个间隔
    drawGap()
    # 右转90°
    turtle.right(90)


# 根据数字绘制七段数码管
def drawDigit(digit):
    drawLine(True) if digit in [2, 3, 4, 5, 6, 8, 9] else drawLine(False)
    drawLine(True) if digit in [0, 1, 3, 4, 5, 6, 7, 8, 9] else drawLine(False)
    drawLine(True) if digit in [0, 2, 3, 5, 6, 8, 9] else drawLine(False)
    drawLine(True) if digit in [0, 2, 6, 8] else drawLine(False)
    turtle.left(90)
    drawLine(True) if digit in [0, 4, 5, 6, 8, 9] else drawLine(False)
    drawLine(True) if digit in [0, 2, 3, 5, 6, 7, 8, 9] else drawLine(False)
    drawLine(True) if digit in [0, 1, 2, 3, 4, 7, 8, 9] else drawLine(False)
    turtle.left(180)
    turtle.penup()
    turtle.fd(20)


# 获取要输出的日期，入参格式为yyyy年MM月dd日，例如2022年05月06日
def drawDate(date):
    name = "Arial"
    size = 20
    family = "normal"
    for i in date:
        if i == "年":
            turtle.write("年", font=(name, size, family))
            turtle.pencolor("#0f0")
            turtle.fd(40)
        elif i == "月":
            turtle.write("月", font=(name, size, family))
            turtle.pencolor("#00f")
            turtle.fd(40)
        elif i == "日":
            turtle.write("日", font=(name, size, family))
        else:
            drawDigit(eval(i))


# 完整的绘制，默认绘制当前日期
def run(datetime=time.strftime("%Y年%m月%d日")):
    turtle.title("七段数码管绘制：" + datetime)
    # 设置窗口大小和位置
    turtle.setup(800, 350, 200, 200)
    # 画笔抬起
    turtle.penup()
    # 向负方向移动
    turtle.fd(-350)
    # 画笔粗细
    turtle.pensize(5)
    drawDate(datetime)
    # 隐藏海龟，即箭头
    turtle.hideturtle()
    # 停止画笔程序，但窗口不关闭
    turtle.done()


turtle.speed(0)
run()
```

##### 4、终极版：

```python
import random
import time
import turtle

# 设置标题
turtle.title("七段数码管绘制")
# 设置画布背景色
turtle.bgcolor("#000")
# 设置窗口大小和位置
turtle.setup(1000, 200, 200, 200)
# 设置跳过动画
turtle.tracer(2)
yearPen = monthPen = dayPen = hourPen = minutePen = secondPen = turtle.Pen()


# 获取颜色
def getColor():
    R = random.random()
    G = random.random()
    B = random.random()
    return R, G, B


# 画笔初始化
def turtleInit():
    newPen = turtle.Pen()
    newPen.hideturtle()
    newPen.pensize(3)
    newPen.speed(0)
    return newPen


# 画笔移动
def penMove(dist, pen):
    pen.penup()
    pen.fd(dist)
    pen.pendown()


# 画线，true表示画实线，false表示移动
def drawLine(draw, pen):
    # 在绘制开始和结束加一个间隔
    penMove(3, pen)
    pen.pendown() if draw else pen.penup()
    # 向前15px
    pen.fd(15)
    # 加一个间隔
    penMove(3, pen)
    # 右转90°
    pen.right(90)


# 根据数字绘制七段数码管
def drawDigit(digit, pen):
    drawLine(True, pen) if digit in [2, 3, 4, 5, 6, 8, 9] else drawLine(False, pen)
    drawLine(True, pen) if digit in [0, 1, 3, 4, 5, 6, 7, 8, 9] else drawLine(False, pen)
    drawLine(True, pen) if digit in [0, 2, 3, 5, 6, 8, 9] else drawLine(False, pen)
    drawLine(True, pen) if digit in [0, 2, 6, 8] else drawLine(False, pen)
    pen.left(90)
    drawLine(True, pen) if digit in [0, 4, 5, 6, 8, 9] else drawLine(False, pen)
    drawLine(True, pen) if digit in [0, 2, 3, 5, 6, 7, 8, 9] else drawLine(False, pen)
    drawLine(True, pen) if digit in [0, 1, 2, 3, 4, 7, 8, 9] else drawLine(False, pen)
    pen.left(180)
    penMove(20, pen)


# 绘制年
def drawYear():
    global yearPen
    yearPen.hideturtle()
    # 获取年
    year = time.strftime("%Y")
    # 清空画布
    yearPen.clear()
    # 绘制年的turtle对象
    yearPen = turtleInit()
    penMove(-80, yearPen)
    # 画笔抬起
    yearPen.penup()
    # 向负方向移动
    yearPen.fd(-400)
    # 画笔颜色
    yearPen.pencolor("#f00")
    for i in year:
        drawDigit(eval(i), yearPen)


# 绘制月
def drawMonth():
    global monthPen
    monthPen.hideturtle()
    # 获取月
    month = time.strftime("%m")
    # 清空画布
    monthPen.clear()
    # 绘制月的turtle对象
    monthPen = turtleInit()
    penMove(-250, monthPen)
    monthPen.pencolor("#0f0")
    for i in month:
        drawDigit(eval(i), monthPen)
    drawYear()


# 绘制日
def drawDayOfMonth():
    global dayPen
    dayPen.hideturtle()
    # 获取日
    dayOfMonth = time.strftime("%d")
    # 清空画布
    dayPen.clear()
    # 绘制日的turtle对象
    dayPen = turtleInit()
    penMove(-100, dayPen)
    dayPen.pencolor("#00f")
    for i in dayOfMonth:
        drawDigit(eval(i), dayPen)
    drawMonth()


# 绘制时
def drawHour():
    global hourPen
    hourPen.hideturtle()
    # 获取时
    hour = time.strftime("%H")
    if hour == "00":
        drawDayOfMonth()
    # 清空画布
    hourPen.clear()
    # 绘制时的turtle对象
    hourPen = turtleInit()
    penMove(50, hourPen)
    hourPen.pencolor(getColor())
    for i in hour:
        drawDigit(eval(i), hourPen)


# 绘制分
def drawMinute():
    global minutePen
    minutePen.hideturtle()
    # 获取分
    minute = time.strftime("%M")
    if minute == "00":
        drawHour()
    # 清空画布
    minutePen.clear()
    # 绘制分的turtle对象
    minutePen = turtleInit()
    penMove(200, minutePen)
    minutePen.pencolor(getColor())
    for i in minute:
        drawDigit(eval(i), minutePen)


# 绘制秒
def drawSecond():
    global secondPen
    secondPen.hideturtle()
    # 获取秒
    second = time.strftime("%S")
    if second == "00":
        drawMinute()
    # 清空画布
    secondPen.clear()
    # 绘制秒的turtle对象
    secondPen = turtleInit()
    penMove(350, secondPen)
    secondPen.pencolor(getColor())
    for i in second:
        drawDigit(eval(i), secondPen)


# 绘制文字
def drawWord():
    word = ["年", "月", "日", "时", "分", "秒"]
    # 绘制文字的turtle对象
    wordPen = turtleInit()
    # 设置初始位置
    penMove(-300, wordPen)
    wordPen.right(90)
    penMove(10, wordPen)
    wordPen.left(90)
    name = "Arial"
    size = 15
    family = "normal"
    dist = 150
    for i in range(len(word)):
        wordPen.pencolor("#fff")
        wordPen.write(word[i], font=(name, size, family))
        penMove(dist, wordPen)


# 主函数
def main():
    # 绘制文字
    drawWord()
    drawSecond()
    drawMinute()
    drawHour()
    drawDayOfMonth()
    while True:
        drawSecond()
        time.sleep(1)


try:
    main()
except KeyboardInterrupt:
    exit(0)
finally:
    exit(0)
```

##### 5、函数的递归

```python
# 字符串反转
def reverseStr(s):
    if s == "":
        return s
    else:
        return reverseStr(s[1:]) + s[0]


# 斐波那契第n个数
def fibonacci(n):
    if n == 1 or n == 2:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


# 汉诺塔问题，当n等于1时直接将圆盘从A(起点)搬运到C(终点)，当n>1时先将n-1个圆盘搬到B，再从B搬到C，我们不需要关心具体的过程。
def hanoi(n, src, mid, dst):
    global count
    # 一个圆盘时直接搬到终点
    if n == 1:
        count += 1
        print("第{}步：{}->{}".format(count, src, dst))
    else:
        # 先将n-1个圆盘从A搬到B
        hanoi(n - 1, src, dst, mid)
        count += 1
        print("第{}步：{}->{}".format(count, src, dst))
        # 接着将这n-1个圆盘从B搬到C
        hanoi(n - 1, mid, src, dst)


count = 0
print(fibonacci(10))
print(reverseStr("12345"))
hanoi(3, "A", "B", "C")
"""
55
54321
第1步：A->C
第2步：A->B
第3步：C->B
第4步：A->C
第5步：B->A
第6步：B->C
第7步：A->C

"""
```

- 安装

   

  ```
  PyInstaller
  ```

  库：

  - 执行：`pip install pyinstaller`。
  - （`cmd`命令行）执行：`pyinstaller -F <文件名.py>`
  - 常用参数：
    - `-F`，打包单个文件；
    - `-D`，打包成一个目录，默认值；
    - `-c`，只对`windows`有效，默认选项，使用控制台；
    - `-w`，只对`windows`有效，不使用控制台；
    - `-p`，设置导入路径；
    - `-i`，设置自定义图标，`icon`或者`ico`格式；
    - `-h`，查看帮助；
    - `--clean`，生成可执行文件前清理打包过程中的缓存和临时文件。
  - 详细内容查看：<https://blog.csdn.net/BearStarX/article/details/81054134>

##### 6、科赫曲线，也叫雪花曲线

```python
# 科赫曲线绘制
import turtle


def koch(size, n):
    if n == 0:
        turtle.fd(size)
    else:
        for angle in [0, 60, -120, 60]:
            turtle.left(angle)
            koch(size / 3, n - 1)


def main():
    turtle.speed(0)
    turtle.title("科赫曲线绘制")
    turtle.setup(800, 400)
    turtle.penup()
    turtle.goto(-300, -50)
    turtle.pendown()
    turtle.pensize(2)
    koch(600, 3)
    turtle.hideturtle()
    turtle.done()


main()
```

##### 7、科赫雪花

```python
# 科赫曲线绘制
import turtle


def koch(size, n):
    if n == 0:
        turtle.fd(size)
    else:
        for angle in [0, 60, -120, 60]:
            turtle.left(angle)
            # 这里除3是为了防止更改n时图形大小变化过大
            koch(size / 3, n - 1)


def draw(size, level, turn):
    for i in range(turn):
        turtle.right(360 / turn)
        koch(size, level)


def main():
    turtle.speed(0)
    turtle.hideturtle()
    turtle.title("科赫曲线绘制")
    turtle.hideturtle()
    turtle.setup(800, 800)
    turtle.penup()
    turtle.goto(-200, 200)
    turtle.pendown()
    turtle.pensize(2)
    # 雪花阶数
    level = 3
    size = 300
    # 转向次数
    turn = 3
    koch(size, level)
    draw(size, level, turn)
    turtle.done()


main()
```

##### 8、举一反三

```python
# 科赫曲线绘制
import turtle


def koch(size, n):
    if n == 0:
        turtle.fd(size)
    else:
        for angle in [0, 90, -90, -90, 90]:
            turtle.left(angle)
            # 这里除3是为了防止更改n时图形大小变化过大
            koch(size / 3, n - 1)


def draw(angle, size, level):
    turtle.right(angle)
    koch(size, level)


def main():
    turtle.speed(0)
    turtle.hideturtle()
    turtle.title("科赫曲线绘制")
    turtle.setup(800, 600)
    turtle.penup()
    turtle.goto(-200, 100)
    turtle.pendown()
    turtle.pensize(2)
    # 阶数
    level = 2
    size = 200
    koch(size, level)
    for i in range(3):
        draw(90, size, level)
    turtle.done()


main()
```

- 其它分形几何：

  - 康托尔集：

    ```python
    # 康托尔集，size表示线段长度，n表示n分集，l表示行数，x0和y0是初始位置
    import turtle
    
    
    def cantor(x0, y0, size, n, l):
        if l > 0:
            turtle.penup()
            turtle.goto(x0, y0)
            turtle.pendown()
            turtle.goto(x0 + size, y0)
            for i in range(n // 2 + 1):
                x = x0 + (size / n * i * 2)
                y = y0 - 20
                cantor(x, y, size / n, n, l - 1)
    
    
    def main():
        turtle.title("康托尔集")
        turtle.setup(900, 600, 200, 100)
        turtle.pensize(2)
        turtle.tracer(2)
        turtle.hideturtle()
        turtle.speed(0)
        x0 = -400
        y0 = 200
        size = 600
        n = 9
        l = 3
        cantor(x0, y0, size, n, l)
        turtle.done()
    
    
    main()
    ```

  - 谢尔宾斯基三角形：

    ```python
    # 谢尔宾斯基三角形
    import math
    import turtle
    
    
    # 画三角形
    def drawTriangle(*pos):
        turtle.penup()
        turtle.goto(pos[0])
        turtle.pendown()
        for i in [1, 2, 0]:
            turtle.goto(pos[i])
    
    
    # 获取两点中间坐标
    def getMidPos(p1, p2):
        return (p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2
    
    
    # 入参为三角形三个点的初始坐标
    def sierpinski(*pos):
        p1 = pos[0]
        p2 = pos[1]
        p3 = pos[2]
        drawTriangle(p1, p2, p3)
        border = math.fabs((p3[0] - p1[0]) / 2)
        if border > 10:
            # p1和p2的中点
            p1_p2_center = (getMidPos(p1, p2))
            # p2和p3的中点
            p2_p3_center = (getMidPos(p2, p3))
            # p3和p1的中点
            p3_p1_center = (getMidPos(p3, p1))
            # 绘制左下角三角形
            sierpinski(p1, p1_p2_center, p3_p1_center)
            # 绘制上方三角形
            sierpinski(p1_p2_center, p2, p2_p3_center)
            # 绘制右下角三角形
            sierpinski(p3_p1_center, p2_p3_center, p3)
    
    
    def main():
        turtle.title("谢尔宾斯基三角形")
        turtle.setup(900, 600, 200, 100)
        turtle.pensize(2)
        turtle.tracer(2)
        turtle.hideturtle()
        turtle.speed(0)
        sideLength = 400
        p1 = (-sideLength / 2, -100)
        p2 = (0, sideLength / 2 * math.sqrt(3) - 100)
        p3 = (sideLength / 2, -100)
        sierpinski(p1, p2, p3, 10)
        turtle.done()
    
    
    main()
    ```

    ```python
    # 谢尔宾斯基三角形
    import math
    import random
    import turtle
    
    
    # 画三角形
    def draw_triangle(pos):
        turtle.penup()
        turtle.goto(pos[0])
        turtle.pendown()
        for i in [1, 2, 0]:
            turtle.goto(pos[i])
    
    
    # 获取一个随机点
    def getRandomPos(x, y):
        return random.randint(-x / 2, x / 2), random.randint(-y / 2, y / 2)
    
    
    # 获取两点中间坐标
    def getMidPos(p1, p2):
        return (p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2
    
    
    # 入参为三角形三个点的初始坐标
    def sierpinski(*pos):
        # 画三角形
        draw_triangle(pos)
        p1, p2, p3 = pos
        # 在三角形内随机取一个点
        randX = (p1[0] + p2[0]) / random.randint(2,10)
        randY = (p2[1] + p3[1]) / random.randint(2,10)
        for i in range(10000):
            # 画点
            turtle.penup()
            turtle.goto(randX, randY)
            turtle.pendown()
            turtle.dot(3)
            # 随机选择一个顶点
            rand_i = random.randint(0, 2)
            randVer = pos[rand_i]
            # 计算该点到顶点的中心点
            randX = (randX + randVer[0]) / 2
            randY = (randY + randVer[1]) / 2
    
    
    def main():
        turtle.title("谢尔宾斯基三角形")
        width = 900
        height = 600
        startX = 200
        startY = 100
        turtle.setup(width, height, startX, startY)
        turtle.pensize(2)
        turtle.tracer(2)
        turtle.hideturtle()
        turtle.bgcolor("#000")
        turtle.pencolor("#fff")
        # 任取三个点
        p1 = getRandomPos(width - 20, height - 20)
        p2 = getRandomPos(width - 20, height - 20)
        p3 = getRandomPos(width - 20, height - 20)
        sierpinski(p1, p2, p3)
        turtle.done()
    
    
    main()
    ```

  - 分形树：

    ```python
    # 分形树
    import turtle
    
    
    def fractalTree(size, angle, var, border):
        if size >= border:
            turtle.fd(size)
            # 画右边树
            turtle.right(angle)
            fractalTree(size - var, angle, var, border)
            # 画左边树
            turtle.left(angle * 2)
            fractalTree(size - var, angle, var, border)
            # 后退
            turtle.right(angle)
            turtle.backward(size)
    
    
    def main():
        turtle.title("分形树")
        width = 900
        height = 600
        startX = 200
        startY = 100
        turtle.setup(width, height, startX, startY)
        turtle.pensize(1)
        turtle.tracer(2)
        turtle.hideturtle()
        turtle.bgcolor("#000")
        turtle.pencolor("#fff")
        turtle.penup()
        turtle.goto(-50, -250)
        turtle.pendown()
        turtle.left(90)
        fractalTree(100, 20, 10, 5)
        turtle.done()
    
    
    main()
    ```

  - 龙形曲线：

    ```python
    import turtle
    
    
    def dragon(curve, n, size):
        if n == 0:
            return
    
        for i in curve:  # 起始位置
            if i == 'H':  # 水平
                dragon('HLVF', n - 1, size)
            elif i == 'V':  # 垂直
                dragon('FHRV', n - 1, size)
            else:
                if i == 'F':
                    turtle.fd(size)
                elif i == 'L':
                    turtle.lt(90)
                elif i == 'R':
                    turtle.rt(90)
    
    
    def main():
        turtle.title("龙形曲线")
        width = 900
        height = 800
        startX = 200
        startY = 0
        turtle.setup(width, height, startX, startY)
        turtle.pensize(1)
        turtle.tracer(2)
        turtle.hideturtle()
        turtle.left(90)
        turtle.bgcolor("#000")
        turtle.pencolor("#fff")
        dragon("V", 10, 6)
        turtle.done()
    
    
    main()
    ```

  - 空间填充曲线：

    ```python
    import turtle
    
    
    class Peano:
        direction = 1
        count = 1
    
        def swerve(self, length):
            turn(self.direction)
            turtle.forward(length)
            turn(self.direction)
            if self.count % 2 == 1:
                self.direction *= -1
            self.count += 1
    
    
    # 转向
    def turn(s):
        if s == 1:
            turtle.right(90)
        elif s == -1:
            turtle.left(90)
    
    
    # 第三次时会转向，以2为除数不好计算
    def join(tier, length):
        if tier > 0:
            for j in range(1, 10):
                join(tier - 1, length)
                if j == 9:
                    break
                elif j % 3 == 0:
                    peanoList[tier - 1].swerve(length)
                else:
                    turtle.forward(length)
    
    
    def main():
        turtle.title("皮亚诺曲线")
        width = 900
        height = 800
        startX = 200
        startY = 0
        turtle.setup(width, height, startX, startY)
        turtle.pensize(1)
        turtle.tracer(2)
        turtle.penup()
        turtle.goto(-200, 200)
        turtle.pendown()
        turtle.hideturtle()
        length = 10
        # 阶数
        n = 3
        # 为每个阶层定义类
        for i in range(n):
            peanoList.append(Peano())
        join(n, length)
        turtle.done()
    
    
    peanoList = []
    main()
    ```

### 六、组合数据类型

#### 1、集合的定义

- 集合使用`{}`表示，元素间使用逗号`,`分隔；
- 集合中每个元素唯一，不存在相同元素；
- 集合元素之间无序，且可以存放不同类型的数据。

#### 2、集合操作符

- `S | T`，返回一个新集合，包括在集合`S`和`T`中的所有元素；

- `S - T`，返回一个新集合，包括在集合`S`但不在`T`中的元素；

- `S & T`，返回一个新集合，包括同时在集合`S`和`T`中的元素；

- `S ^ T`，返回一个新集合，包括集合`S`和`T`中的非相同元素；

- `S <= T`或 `S < T`，返回`Ture/False`，判断`S`和`T`的子集关系；

- `S >= T`或 `S > T`，返回`Ture/False`，判断`S`和`T`的包含关系；

  ```python
  # 集合
  a = {"a", "b", "c", "1"}
  b = {"a", "c", "d", "2", "3", "1"}
  c = {"a", "b"}
  # S | T
  print(a | b)
  # S - T
  print(a - b)
  # S & T
  print(a & b)
  # S ^ T
  print(a ^ b)
  # S <= T 或 S < T
  print(a <= b)
  print(c <= a)
  print(c < a)
  # S >= T 或 S > T
  print(a >= c)
  print(a > b)
  """
  {'d', '1', '2', 'c', 'a', '3', 'b'}
  {'b'}
  {'1', 'c', 'a'}
  {'d', 'b', '3', '2'}
  False
  True
  True
  True
  False
  
  """
  ```

- 增强操作符：

  - `S |= T`，更新集合`S`，包括在集合`S`和`T`中的所有元素；
  - `S -= T`，更新集合`S`，包括在集合`S`但不在`T`中的元素；
  - `S &= T`，更新集合`S`，包括同时在集合`S`和`T`中的元素；
  - `S ^= T`，更新集合`S`，包括集合`S`和`T`中的非相同元素。

#### 3、集合处理方法

- `S.add(x)`，如果`x`不在集合`S`中，将`x`添加到`S`；
- `S.discard(x)`，移除`S`中元素`x`，如果`x`不在集合`S`中，不报错；
- `S.remove(x)`，移除`S`中元素`x`，如果`x`不在集合`S`中，产生`KeyError`异常；
- `S.clear()`，移除`S`中所有元素；
- `S.pop()`，随机返回`S`的一个元素，同时在集合`S`中删除该元素，若`S`为空产生`KeyError`异常；
- `S.copy()`，返回`S`集合的一个副本；
- `len(S)`，返回`S`集合的元素个数；
- `x in S`，判断`S`中的元素`x`，`x`在集合`S`中，返回`Ture`，否则返回`False`；
- `x not in S`，判断`S`中元素`x`，`x`不在集合`S`中返回`Ture`，否则返回`False`；
- `set(x)`，将其它类型变量`x`转换为集合类型。

#### 4、序列类型及操作

- 序列是具有先后关系的一组元素，是一个基本数据类型。它也是双向索引。
- `x in s`，如果`x`是`s`的元素，返回`Ture`，否则返回`False`；
- `x not in s`，如果`x`不是`s`的元素返回`False`，否则返回`Ture`；
- `s + t`，连接两个序列`s`和`t`；
- `s*n`或`n*s`，将序列`s`复制`n`次；
- `s[i]`，索引，返回`s`中的第`i`个元素；
- `s[i:j]`或`s[i:j:k]`，切片，返回序列`s`中第`i`到`j`以`k`为步长的元素子序列，`k`默认为1。例如`[::-1]`表示取反。

#### 5、序列类型通用函数和方法

- `len(s)`，返回序列`s`的长度；
- `min(s)`，返回序列`s`的最小元素，`s`中元素需要可比较；
- `max(s)`，返回序列`s`的最大元素，`s`中元素需要可比较；
- `s.index(x)`或`s.index(x,i,j)`，返回序列`s`从`i`开始到`j`位置第一次出现元素`x`的位置；
- `s.count(x)`，返回`x`在`s`中出现的总次数。

#### 6、元组类型及操作

- 元组是序列类型的一种扩展，一旦创建就不能被修改。
- 使用小括号`()`或`tuple()`创建，元素间用逗号`,`分隔。
- 可以使用括号，也可以不使用。

#### 7、列表类型及操作

- 列表是序列类型的一种扩展，创建后可以随意被修改；

- 使用`[]`或`list()`创建，元素间使用逗号`,`分隔；

- 列表中个元素类型可以不同，无长度限制。

- `ls[i] = x`，替换列表`ls`第`i`元素为`x`；

- `ls[i:j:k] = lt`，用列表`lt`替换`ls`切片后所对应子元素列表；

- `del ls[i]`，删除列表`ls`中第`i`个元素；

- `del ls[i:j:k]`，删除列表`ls`中第`i`到第`j`以`k`为步长的元素；

- `ls += lt`，更新列表`ls`，将列表`lt`元素增加到列表`ls`中；

- `ls *= n`，更新列表`ls`，其元素重复`n`次；

- `ls.append(x)`，在列表`ls`最后增加一个元素`x`；

- `ls.clear()`，删除列表`ls`中所有元素；

- `ls.copy()`，生成一个新列表，赋值`ls`中所有的元素；

- `ls.insert(i,x)`，在列表`ls`的第`i`位置增加元素`x`；

- `ls.pop(i)`，将列表`ls`中第`i`位置元素取出并删除该元素，默认最后一个；

- `ls.remove(x)`，将列表`ls`中出现的第一个元素`x`删除；

- `ls.reverse()`，将列表`ls`中的元素反转。

  ```python
  lt = []
  lt += [1, 2, 3, 4, 5]
  lt[2] = 22
  print(lt)
  lt.insert(2, 222)
  print(lt)
  del lt[1]
  print(lt)
  del lt[1:4]
  print(lt)
  print(0 in lt)
  lt.append(0)
  print(lt.index(0))
  print(len(lt))
  print(max(lt))
  lt.clear()
  print(lt)
  """
  [1, 2, 22, 4, 5]
  [1, 2, 222, 22, 4, 5]
  [1, 222, 22, 4, 5]
  [1, 5]
  False
  2
  3
  5
  []
  
  """
  ```

#### 8、字典类型

- 映射是一种键（索引）和值（数据）的对应。

- 字典是键值对的集合，键值对之间无序。

- 采用`{}`和`dict()`创建，键值对用冒号`:`表示。

  ```python
  d = {"中国": "北京", "美国": "华盛顿"}
  print(d["美国"])
  d["美国"] = "123"
  print(d["中国"])
  ls = list()
  st = set()
  st.add(1)
  st.add(1)
  ls.append(1)
  ls.append(1)
  de = {}
  print(type(de))
  print(type(ls))
  print(type(st))
  print(ls)
  print(st)
  print(d["美国"])
  """
  华盛顿
  北京
  <class 'dict'>
  <class 'list'>
  <class 'set'>
  [1, 1]
  {1}
  123
  
  """
  ```

- 字典类型操作函数和方法：

  - ##### `del d[k]`，删除字典`d`中键`k`对应的数据值；

  - `k in d`，判断键`k`是否在字典`d`中，如果在返回`True`，否则`False`；

  - `d.keys()`，返回字典`d`中所有的键信息；

  - `d.values()`，返回字典`d`中所有的值的信息；

  - `d.items()`，返回字典`d`中所有的键值对信息。

  - `d.get(k,<default>)`，键`k`存在，则返回相应的值，不存在返回`<default>`的值；

  - `d.pop(k,<default>)`，键`k`存在，则取出相应的值，否则返回`<default>`的值；

  - `d.popitem()`，随机从字典中取出一个键值对，以元组形式返回；

  - `d.clear()`，删除所有的键值对；

  - `len(d)`，返回字典`d`中元素的个数。

### 七、jieba库介绍

- `jieba`是优秀的中文分词第三方库，需要额外安装。

  ```powershell
  pip install jieba
  ```

- `jieba`分词依靠中文词库。

- 精确模式：把文本精确的切分开，不存在冗余单词；

- 全模式：把文本中所有可能的词语都扫描出来，有冗余；

- 搜索引擎模式：在精确模式基础上，对长词再次切分。

- `jieba.lcut(s,cut_all=True)`，精确模式，返回一个列表类型的分词结果；加上`cut_all=True`后变成全模式，返回一个列表类型的分词结果，存在冗余。

- `jieba.lcut_for_search(s)`，搜索引擎模式，返回一个列表类型的分词结果，存在冗余。

- `jieba.add_word(w)`，向分词词典中增加新词`w`。

  ```python
  import jieba
  
  word = "中国是一个伟大的国家"
  print(jieba.lcut(word))
  print(jieba.lcut(word, cut_all=True))
  word = "中华人民共和国是伟大的"
  print(jieba.lcut_for_search(word))
  """
  ['中国', '是', '一个', '伟大', '的', '国家']
  ['中国', '国是', '一个', '伟大', '的', '国家']
  ['中华', '华人', '人民', '共和', '共和国', '中华人民共和国', '是', '伟大', '的']
  """
  ```

- 词频统计：

  ```python
  def getText():
      txt = open("hamlet.txt", "r").read()
      txt = txt.lower()
      for ch in '!"#$%^&*()_+-~`{}|/[\\],.=<>@:;?\'':
          txt = txt.replace(ch, " ")
      return txt
  
  
  hamletTxt = getText()
  words = hamletTxt.split(" ")
  counts = {}
  for word in words:
      counts[word] = counts.get(word, 0) + 1
  items = list(counts.items())
  items.sort(key=lambda x: x[1], reverse=True)
  for item in items:
      print(item)
  ```

  ```python
  import jieba
  
  txt = open("threekingdoms.txt", "r", encoding="utf-8").read()
  words = jieba.lcut(txt)
  counts = {}
  for word in words:
      if word in ["，", "。", '"', "：", "、", "；", "？", "！", "\n", " ", '”', '”']:
          continue
      else:
          counts[word] = counts.get(word, 0) + 1
  items = list(counts.items())
  items.sort(key=lambda x: x[1], reverse=True)
  for i in range(len(items)):
      print(i, items[i])
  ```

### 八、文件和数据格式化

- 文件是数据的抽象和集合。
- 文本文件：由单一特定编码组成的文件，如`UTF-8`编码；
- 二进制文件：直接由比特0和1组成，没有统一字符编码。
- 所有文件本质上都是二进制文件。

#### 1、读文件

```python
# 文本形式打开文件，t表示文本形式
tf = open("f.txt", "rt", encoding="utf-8")
print(tf.readline())
tf.close()
"""
中国是一个伟大的国家
"""
# 二进制形式打开文件，b表示二进制形式
tf = open("f.txt", "rb")
print(tf.readline())
tf.close()
"""
b'\xe4\xb8\xad\xe5\x9b\xbd\xe6\x98\xaf\xe4\xb8\x80\xe4\xb8\xaa\xe4\xbc\x9f\xe5\xa4\xa7\xe7\x9a\x84\xe5\x9b\xbd\xe5\xae\xb6'
"""
```

- ```
  <变量名> = open(<文件路径>,<打开方式>)
  ```

  ，可以是绝对路径或者相对路径。

  - `r`，只读模式，默认值，如果文件不存在返回`FileNotFoundError`。
  - `w`，覆盖写模式，文件不存在则创建，存在则完全覆盖。
  - `x`，创建写模式，文件不存在则创建，存在则返回`FileExistsError`。
  - `a`，追加写模式，文件不存在则创建，文件存在则在文件最后追加内容。
  - `b`，二进制文件形式打开。
  - `t`，文本文件形式打开，默认值。
  - `+`，与`r/w/x/a`一起使用，在原基础功能上增加同时读写功能。

- `<f>.read(size=-1)`，读入全部内容，如果给出参数，读入前`size`长度。

- `<f>.readline(size=-1)`，读入一行内容，如果给出参数，读入该行前`size`长度。

- `<f>.readlines(hint=-1)`，读入文件所有行，以每行为元素形成列表，如果给出参数，读入前`hint`行。

#### 2、写文件

- `<f>.write(s)`，向文件写一个字符串或字节流。

- `<f>.writelines(lines)`，将一个元素全为字符串的列表写入文件。

- `<f>.seek(offset)`，改变当前文件操作指针的位置：

  - `0`，文件开头；
  - `1`，当前位置；
  - `2`，文件末尾。

  ```python
  # 写文件
  tf = open("./f.txt", "at", encoding="utf-8")
  tf.write("\nHPE")
  tf.writelines(["\n中国", "法国", "美国"])
  tf.close()
  """
  f.txt内容
  中国是一个伟大的国家
  HPE
  中国法国美国
  """
  ```

#### 3、自动轨迹绘制

- `map(func, *iterables)`，*map*是内嵌函数，将第一个参数的功能作用于第二个参数。

  ```python
  import turtle as t
  
  t.title('自动轨迹绘制')
  t.setup(800, 600, 0, 0)
  t.pencolor('red')
  t.pensize(5)
  # 数据读取
  datals = []
  # 数据格式：前进距离,0左转1右转,角度,颜色RGB值
  with open('template.txt', 'rt') as file:
      for line in file:
          line = line.replace("\n", "")
          # map是内嵌函数，将第一个参数的功能作用于第二个参数
          datals.append(list(map(eval, line.split(","))))
      file.close()
  
  print(datals)
  
  # 自动绘制
  for i in range(len(datals)):
      t.pencolor(datals[i][3], datals[i][4], datals[i][5])
      t.fd(datals[i][0])
      if datals[i][1]:
          t.right(datals[i][2])
      else:
          t.left(datals[i][2])
  
  t.done()
  ```

- 自动化思维：数据和功能分离，数据驱动的自动运行。

- 接口化设计：格式化设计接口，清晰明了。

- 二维数据应用：应用维度组织数据，二维数据最常用。

#### 4、一维数据的表示

- 列表类型可以表达一维有序数据。

#### 5、二维数据的表示

- 可以使用二维列表来表示。

- `CSV`，全称是**Comma-Separated Values**，国际通用的一二维数据存储格式，扩展名为`.csv`。每行一个一维数据，采用逗号分隔，无空行。

- 如果某个元素缺失，逗号仍要保留。

- 一般索引习惯：`ls[row][column]`，即先行后列。

  ```python
  list = [["姓名", "年龄", "籍贯"], ["张三", "18", "湖北"], ["李四", "19", "上海"], ["王五", "20", "北京"]]
  
  with open("test.csv", "wt", encoding="utf-8") as file:
      for row in list:
          file.write(",".join(row))
          file.write("\n")
      file.close()
  
  with open("test.csv", "rt", encoding="utf-8") as file:
      for row in file:
          print(row, end="")
      file.close()
  """
  姓名,年龄,籍贯
  张三,18,湖北
  李四,19,上海
  王五,20,北京
  """
  ```

#### 6、WordCloud库

- `wordcloud`库把词云当作一个`WordCloud`对象。

- `wordcloud.WordCloud()`代表一个文本对应的词云。

- 可以根据文本中词语出现的频率等参数绘制词云。

- 绘制词云的形状、尺寸和颜色等都可以设定。

  ```python
  w = wordcloud.WordCloud()
  ```

  - `w.generate(txt)`，向`WordCloud`对象`w`中加载文本`txt`。
  - `w.to_file(filename)`，将词云输出为图像文件，`.png`或`.jpg`格式。

- 配置对象参数

  - `width`，指定词云对象生成图片的宽度，默认`400px`。

  - `height`，指定词云对象生成图片的高度，默认`200px`。

  - `min_font_size`，指定词云中字体的最小字号，默认`4`号。

  - `max_font_size`，指定词云中字体的最大字号，根据高度自动调节。

  - `font_step`，指定词云中字体字号的步进间隔，默认是`1`。

  - `font_path`，指定字体文件的路径，默认`None`。

  - `max_words`，指定词云显示的最大单词数量，默认`20`。

  - `stopwords`，指定词云的排除词列表，即不显示的单词列表。

  - `mask`，指定词云的形状，默认为长方形，需要引用`imread()`函数。例如：

    ```python
    from scipy.misc import imread
    mk = imread("pic.png")
    w = wordcloud.WordCloud(mask=mk)
    ```

  - `background_color`，指定词云图片的背景颜色，默认为黑色。

- 对中文文本生成词云需要使用到`jieba`库。

  ```python
  import jieba
  import wordcloud
  txt = "目标"
  w = wordcloud.WordCloud(width=1000, font_path="msyh.ttc", height=700)
  w.generate(" ".join(jieba.lcut(txt)))
  ```

- 政府工作报告：

  ```python
  import os.path
  import jieba
  import wordcloud
  
  def get_word_cloud_pic(filename: str, color="#000"):
      with open(filename, "rt", encoding="utf-8") as file:
          t = file.read()
          file.close()
      ls = jieba.lcut(t)
      res = []
      for word in ls:
          if len(word) > 1:
              res.append(word)
      txt = " ".join(res)
      w = wordcloud.WordCloud(width=1000, font_path="msyh.ttc", height=700, background_color=color)
      w.generate(txt)
      w.to_file(os.path.splitext(filename)[0] + ".png")
  
  
  get_word_cloud_pic("新时代中国特色社会主义.txt")
  get_word_cloud_pic("关于实施乡村振兴战略的意见.txt")
  ```

### 九、计算生态

- 竞争发展
- 相互依存
- 迅速更迭
- 解决`pip`安装慢方法：
  - `pip install -i <source url> <第三方库名称>`
  - 阿里云： `https://mirrors.aliyun.com/pypi/simple/`
  - 豆瓣：`https://pypi.douban.com/simple/`
  - 清华大学：`https://pypi.tuna.tsinghua.edu.cn/simple/`
  - 中国科学技术大学：`https://pypi.mirrors.ustc.edu.cn/simple/`
  - 华中科技大学：`https://pypi.hustunique.com/`
  - 华中理工大学：`https://pypi.hustunique.com/`
  - 山东理工大学：`https://pypi.sdutlinux.org/`
- 永久解决：
  - `pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple`
  - `pip config set install.trusted-host mirrors.aliyun.com`

### 十、Python社区

- **Python**基础语法（**MOOC**）：
  - **Python**网络爬虫与信息提取；
  - **Python**游戏开发入门；
  - **Python**数据分析与展示；
  - **Python**云端系统开发入门；
  - **Python**机器学习应用；
  - **Python**科学计算三维可视化
- **Python**进阶语法（网易云课堂嵩老师，微专业课程）
- <https://pypi.org/>
- `pip install -U xxx`，下载并更新；
- `pip download xxx`，下载但不安装；
- `pip show xxx`，列出某个库的详细信息；
- `pip search xxx`，搜索第三方库；
- [文件安装方法](http://www.lfd.uci.edu/~gohike/pythonlibs/)
- 一些优秀的第三方库：
  - 数据处理
    - `numpy`，**N**维数据表示和运算；
    - `pandas`，高效数据分析和计算；
    - `scipy`，数学、科学和工程计算功能库；
  - 数据可视化
    - `matplotlib`，二维数据可视化；
    - `seaborn`，统计类数据可视化功能库；
    - `mayavi`，三维科学数据可视化功能库 ；
  - 文本处理
    - `pypdf2`，**PDF**文件内容提取和处理；
    - `nltk`，自然语言文本处理第三方库；
    - `python-docx`，创建或更新**Microsoft Word**文件的第三方库；
  - 机器学习
    - `sklearn`，机器学习和数据挖掘；
    - `tensorflow`，**AlphaGo**背后的机器学习计算框架；
    - `mxnet`，基于神经网络的深度学习计算框架；
    - `aip`，百度**AI**开放平台接口；
  - 爬虫相关
    - `pyspider`，强大的**Web**页面爬取系统；
    - `Scrapy`，优秀的网络爬虫框架；
    - `python-goose`，提取文章类型**Web**页面的功能库
    - `requests`，**HTTP**协议访问及网络爬虫；
    - `beautifulsoup4`，**HTML**和**XML**解析器；
  - **Web**相关
    - `django`，**Python**最流行的**Web**开发框架（大型）；
    - `flask`，轻量级**Web**开发框架（小型）；
    - `pyramid`，规模适中的**Web**应用框架（中等）；
  - 工具类
    - `jieba`，中文分词；
    - `wheel`，**python**第三方库文件打包工具；
    - `pyinstaller`，打包**Python**，源文件为可执行文件；
    - `sympy`，数学符号计算框架；
    - `docopt`，**Python**命令行解析；
    - `MyQR`，二维码生成第三方库；
    - `re`，正则表达式解析和处理功能库（不需要安装，python标准库）；
  - **GUI**开发
    - `pyqt5`，基于**Qt**的专业级**GUI**开发框架；
    - `wxPython`，跨平台**GUI**开发框架；
    - `PyGobject`，使用**GTK+开发GUI**的功能库；
  - 游戏开发相关
    - `pygame`，简单小游戏开发框架；
    - `Panda3D`，开源、跨平台的**3D**渲染和游戏开发库；
    - `cocos2d`，构建**2D**游戏和图形界面交互式应用的框架；
  - 虚拟现实
    - `VR Zero`，在树莓派上开发**VR**应用的**Python**库；
    - `pyovr`，**Oculus Rift**的**Python**开发接口；
    - `vizard`，基于**Python**的通用**VR**开发引擎；
  - 图形库
    - `quads`，迭代的艺术，对图片进行四分迭代，形成像素风；
    - `ascii_art`，**ASCII**艺术库；
    - `Pillow`，图像处理；
  - 其它
    - `werobot`，微信机器人开发框架；
    - `networkx`，复杂网络和图结构的建模和分析；
    - `pyopengl`，多平台**OpenGL**开发接口；

### 十一、os库

#### 1、`os.path`子库

- `os.path.abspath(path)`，返回`path`在当前系统中的绝对路径；
- `os.path.normpath(path)`，归一化`path`的表示形式，统一使用`\\`分隔路径；
- `os.path.relpath(path)`，返回当前程序与文件之间的相对路径；
- `os.path.dirname(path)`，返回`path`中的目录名称；
- `os.path.basename(path)`，返回`path`中最后的文件名称；
- `os.path.join(path, *paths)`，组合`path`与`paths`，返回一个路径字符串；
- `os.path.exists(path)`，判断`path`对应文件或目录是否存在，返回`True`或`False`；
- `os.path.isfile(path)`，判断`path`所对应的是否为已存在的文件，返回`True`或`False`；
- `os.path.isdir(path)`，判断`path`所对应的是否为已存在的目录，返回`True`或`False`；
- `os.path.getatime(path)`，返回`path`对应文件或目录上一次的访问时间；
- `os.path.getmtime(path)`，返回`path`对应文件或目录最近一次的修改时间；
- `os.path.getctime(path)`，返回`path`对应文件或目录的创建时间。

#### 2、`os.system`库

- 调用其它的外部程序，执行其它的程序或命令，例如：

  ```python
  os.system("calc.exe")
  ```

- 带参数的，例如：

  ```python
  os.system("mspaint.exe 关于实施乡村振兴战略的意见.png")
  ```

#### 3、获取或改变系统环境信息

- `os.chdir(path)`，修改当前程序操作的路径；
- `os.getcwd()`，返回程序的当前路径；
- `os.getlogin()`，获取当前系统登录用户名称；
- `os.cpu_count()`，获取当前系统的CPU数量。