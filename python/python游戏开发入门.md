### 一、***`pygame`***的安装

+ `pip install pygame`
+ 运行`python -m pygame.examples.aliens`打开系统提供的小游戏

### 二、***`pygame`***最小开发框架

1. 引入`pygame`和`sys`
2. 初始化`init()`及设置
3. 获取事件并逐类响应
4. 刷新屏幕

```python
import sys
import pygame

pygame.init()
screen = pygame.display.set_mode((600, 400))
pygame.display.set_caption("pygame游戏之旅")
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        pygame.display.update()

```

### 三、常用方法及属性

1. `pygame.image.load(filename)`，将`filename`路径下的图像载入游戏，支持`JPG`、`PNG`、`GIF`（非动画）等`13`种常用图片格式；
2. `Surface`对象，`ball.get_rect()`，`Pygame`使用内部定义的`Surface`对象表示所有载入的图像，其中`.get_rect()`方法返回一个覆盖图像的矩形`Rect`对象；
   - `Rect`对象有一些重要属性：
     - `top`，`bottom`，`left`，`right`表示上下左右；
     - `width`，`height`表示宽高
3. `ball_rect.move(x, y)`，矩形移动一个偏移量`(x, y)`，即在横轴方向移动`x`像素，纵轴方向移动`y`像素，`xy`为整数；
4. `screen.fill(color)`，显示窗口背景填充为`color`颜色，采用`RGB`色彩体系。由于壁球不断运动，运动后原有位置将默认填充白色，因此需要不断刷新背景色；
5. `screen.blit(src, dest)`，将一个图像绘制在另一个图像上，即将`src`绘制到`dest`位置上，通过`Rect`对象引导对壁球的绘制；
6. `pygame.time.Clock()`，创建一个`Clock`对象，用于操作时间；
7. `clock.tick(framerate)`，控制帧率速度，即窗口刷新速度，例如：`clock.tick(100)`表示每秒刷新100次。

```python
import sys

import pygame

pygame.init()
size = width, height = 600, 400
speed = [1, 1]
BLACK = 0, 0, 0
screen = pygame.display.set_mode(size)
pygame.display.set_caption("pygame壁球")
ball = pygame.image.load("../resource/image/PYG02-ball.gif")
ball_rect = ball.get_rect()
# Frames per Second 每秒帧率参数
fps = 300
clock = pygame.time.Clock()
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
    ball_rect = ball_rect.move(speed[0], speed[1])
    if ball_rect.left <= 0 or ball_rect.right >= width:
        speed[0] *= -1
    if ball_rect.top <= 0 or ball_rect.bottom >= height:
        speed[1] *= -1
    screen.fill(BLACK)
    screen.blit(ball, ball_rect)
    pygame.display.update()
    clock.tick(fps)

```

1. `pygame.KEYDOWN`，对键盘敲击事件的定义。

```python
import sys

import pygame

pygame.init()
size = width, height = 600, 400
speed = [1, 1]
BLACK = 0, 0, 0
screen = pygame.display.set_mode(size)
pygame.display.set_caption("pygame壁球")
ball = pygame.image.load("../resource/image/PYG02-ball.gif")
ball_rect = ball.get_rect()
# Frames per Second 每秒帧率参数
fps = 300
clock = pygame.time.Clock()
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key in [pygame.K_a, pygame.K_LEFT]:
                speed[0] = speed[0] if speed[0] == 0 else (abs(speed[0]) - 1) * int(speed[0] / abs(speed[0]))
            elif event.key in [pygame.K_d, pygame.K_RIGHT]:
                speed[0] = speed[0] + 1 if speed[0] > 0 else speed[0] - 1
            elif event.key in [pygame.K_w, pygame.K_UP]:
                speed[1] = speed[1] + 1 if speed[1] > 0 else speed[1] - 1
            elif event.key in [pygame.K_s, pygame.K_DOWN]:
                speed[1] = speed[1] if speed[1] == 0 else (abs(speed[1]) - 1) * int(speed[1] / abs(speed[1]))
    ball_rect = ball_rect.move(speed[0], speed[1])
    if ball_rect.left <= 0 or ball_rect.right >= width:
        speed[0] *= -1
    if ball_rect.top <= 0 or ball_rect.bottom >= height:
        speed[1] *= -1
    screen.fill(BLACK)
    screen.blit(ball, ball_rect)
    pygame.display.update()
    clock.tick(fps)

```

### 四、屏幕与事件

#### 1、事件类型及属性

|     事件类型      |        属性         |
| :---------------: | :-----------------: |
|      `QUIT`       |       `none`        |
|   `ACTIVEEVENT`   |    `gain, state`    |
|     `KEYDOWN`     | `unicode, key, mod` |
|      `KEYUP`      |     `key, mod`      |
|   `MOUSEMOTION`   | `pos, rel, buttons` |
|  `MOUSEBUTTONUP`  |    `pos, button`    |
| `MOUSEBUTTONDOWN` |    `pos, button`    |
|  `JOYAXISMOTION`  | `joy, axis, vaule`  |
|  `JOYBALLMOTION`  |  `joy, ball, rel`   |
|  `JOYHATMOTION`   |  `joy, hat, value`  |
|   `JOYBUTTONUP`   |    `joy, button`    |
|  `JOYBUTTONDOWN`  |    `joy, button`    |
|   `VIDEORESIZE`   |    `size, w, h`     |
|   `VIDEOEXPOSE`   |       `none`        |
|    `USEREVENT`    |       `code`        |

#### 2、处理事件

+ `pygame.event.get()`，从事件队列中获得事件列表，即获取所有队列的事件;
  + 增加参数获取某一类或某些类事件：`pygame.event.get(type)`，`pygame.event.get(typelist)`
+ `pygame.event.poll()`，从事件队列中获取一个事件，且获取后从队列中删除该事件；
  + 如果队列为空，返回`event.NOEVENT`
+ `pygame.event.clear()`，默认删除所有事件；
  + 增加参数获取某一类或某些类事件：`pygame.event.clear(type)`，`pygame.event.clear(typelist)`

#### 3、操作事件队列

+ 同时仅能存储`128`个事件，当队列满时，更多事件将被丢弃。

+ `pygame.event.set_blocked(type or typelist)`，控制哪些事件类型不允许保存到事件队列中；
+ `pygame.event.get_blocked()`，测试某个事件类型是否被事件队列所禁止，如果事件类型被禁止，返回`True`，否则返回`False`；
+ `pygame.event.set_allowed(type or typelist)`，控制哪些事件允许被保存到事件队列中。

#### 4、生成事件

+ `pygame.event.post(Event)`，产生一个事件，并将其放入事件队列；
  + 一般用于放置用户自定义事件`(pygame.USEREVENT)`，也可以用于放置系统定义事件（如鼠标或键盘等）
+ `pygame.event.Event(type, dict)`，创建一个给定类型的事件。
  + 其中，事件的属性和值采用字典类型复制，属性名采用字符串形式，如果创建已有事件，属性需要一致

```python
import sys
import pygame

pygame.init()

screen = pygame.display.set_mode((600, 400))
pygame.display.set_caption("pygame事件产生")
fps = 100
clock = pygame.time.Clock()
while True:
    user_event = pygame.event.Event(pygame.KEYDOWN,
                                    {"unicode": 123, "key": pygame.K_SPACE,
                                     "mod": pygame.KMOD_ALT & pygame.KMOD_CTRL})
    pygame.event.post(user_event)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.unicode == "":
                print("[KEYDOWN]: ", "#", event.key, event.mod)
            else:
                print("[KEYDOWN]: ", event.unicode, event.key, event.mod)

        pygame.display.update()
        clock.tick(fps)

```

```python
import sys
import pygame

pygame.init()

screen = pygame.display.set_mode((600, 400))
pygame.display.set_caption("pygame事件处理")
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.unicode == "":
                print("[KEYDOWN]: ", "#", event.key, event.mod)
            else:
                print("[KEYDOWN]: ", event.unicode, event.key, event.mod)
        pygame.display.update()

```

#### 5、鼠标事件及属性

+ `pygame.event.MOUSEMOTION`，鼠标移动事件
  + `event.pos`，鼠标当前坐标值`(x, y)`，相对于窗口左上角；
  + `event.rel`，鼠标相对运动距离`(x, y)`，相对于上次事件；
  + `event.buttons`，鼠标按钮状态`(a, b, c)`，对应鼠标的三个键，鼠标移动时，若这三个键处于按下状态，对应位置值为`1`，反之为`0`。
+ `pygame.event.MOUSEBUTTONUP`，鼠标释放事件
  + `event.pos`，鼠标当前坐标值`(x, y)`，相对于窗口左上角；
  + `event.button`，鼠标按下键编号`n`，取值`0/1/2`，分别对应三个键。
+ `pygame.event.MOUSEBUTTONDOWN`，鼠标按下事件
  + `event.pos`，鼠标当前坐标`(x, y)`，相对于窗口左上角；
  + `event.button`，鼠标按下键编号`n`，取值为整数，左键为`1`，右键为`3`，设备相关

```python
import sys
import pygame

pygame.init()

screen = pygame.display.set_mode((600, 400))
pygame.display.set_caption("pygame事件处理")
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.unicode == "":
                print("[KEYDOWN]: ", "#", event.key, event.mod)
            else:
                print("[KEYDOWN]: ", event.unicode, event.key, event.mod)
        elif event.type == pygame.MOUSEMOTION:
            print("[MOUSEMOTION]: ", event.pos, event.rel, event.buttons)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            print("[MOUSEBUTTONDOWN]: ", event.pos, event.button)
        elif event.type == pygame.MOUSEBUTTONUP:
            print("[MOUSEBUTTONUP]: ", event.pos, event.button)
        pygame.display.update()


```

#### 6、壁球小游戏（鼠标版）

```python
import sys

import pygame

pygame.init()
size = width, height = 600, 400
speed = [1, 1]
BLACK = 0, 0, 0
screen = pygame.display.set_mode(size)
pygame.display.set_caption("pygame壁球")
ball = pygame.image.load("../resource/image/PYG02-ball.gif")
ball_rect = ball.get_rect()
# Frames per Second 每秒帧率参数
fps = 300
clock = pygame.time.Clock()
still = False
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key in [pygame.K_a, pygame.K_LEFT]:
                speed[0] = speed[0] if speed[0] == 0 else (abs(speed[0]) - 1) * int(speed[0] / abs(speed[0]))
            elif event.key in [pygame.K_d, pygame.K_RIGHT]:
                speed[0] = speed[0] + 1 if speed[0] > 0 else speed[0] - 1
            elif event.key in [pygame.K_w, pygame.K_UP]:
                speed[1] = speed[1] + 1 if speed[1] > 0 else speed[1] - 1
            elif event.key in [pygame.K_s, pygame.K_DOWN]:
                speed[1] = speed[1] if speed[1] == 0 else (abs(speed[1]) - 1) * int(speed[1] / abs(speed[1]))
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                still = True
        elif event.type == pygame.MOUSEBUTTONUP:
            still = False
            if event.button == 1:
                ball_rect = ball_rect.move(event.pos[0] - ball_rect.left, event.pos[1] - ball_rect.top)
        elif event.type == pygame.MOUSEMOTION:
            if event.buttons[0] == 1:
                x = event.pos[0] - ball_rect.left - ball_rect.width / 2
                y = event.pos[1] - ball_rect.top - ball_rect.height / 2
                ball_rect = ball_rect.move(x, y)
                if ball_rect.right > width:
                    ball_rect = ball_rect.move(width - ball_rect.right, 0)
                if ball_rect.bottom > height:
                    ball_rect = ball_rect.move(0, height - ball_rect.bottom)
                if ball_rect.left < 0:
                    ball_rect = ball_rect.move(0 - ball_rect.left, 0)
                if ball_rect.top < 0:
                    ball_rect = ball_rect.move(0, 0 - ball_rect.top)

    if pygame.display.get_active() and not still:
        ball_rect = ball_rect.move(speed[0], speed[1])
    if ball_rect.left < 0 or ball_rect.right > width:
        speed[0] *= -1
        if width < ball_rect.right < ball_rect.right + speed[0]:
            speed[0] *= -1
    if ball_rect.top < 0 or ball_rect.bottom > height:
        speed[1] *= -1
        if height < ball_rect.bottom < ball_rect.bottom + speed[1]:
            speed[1] *= -1
    screen.fill(BLACK)
    screen.blit(ball, ball_rect)
    pygame.display.update()
    clock.tick(fps)

```

### 五，色彩与绘图机制

#### 1、`pygame.Color`

+ `Color`类用于表达色彩，使用`RGB`或`RGBA`色彩模式，`A`可选；

+ `Color`类可以用色彩名字、`RGBA`值、`HTML`色彩格式等方式定义。

+ `pygame.Color.r`，获取`Color`类的红色`r`值；

+ `pygame.Color.g`，获取`Color`类的红色`g`值；

+ `pygame.Color.b`，获取`Color`类的红色`b`值；

+ `pygame.Color.a`，获取`Color`类的红色`a`值；

+ `pygame.Color.normalize`，将`RGBA`各通道值归一到`0-1`之间

  ```python
  import sys
  
  import pygame
  
  
  def RGBChannel(param: float):
      return 0 if param < 0 else int(param)
  
  
  pygame.init()
  size = width, height = 600, 400
  speed = [1, 1]
  BLACK = 0, 0, 0
  screen = pygame.display.set_mode(size)
  pygame.display.set_caption("pygame壁球")
  ball = pygame.image.load("../resource/image/PYG02-ball.gif")
  ball_rect = ball.get_rect()
  # Frames per Second 每秒帧率参数
  fps = 300
  clock = pygame.time.Clock()
  still = False
  bg_color = pygame.Color(255, 255, 255)
  while True:
      for event in pygame.event.get():
          if event.type == pygame.QUIT:
              sys.exit()
          elif event.type == pygame.KEYDOWN:
              if event.key in [pygame.K_a, pygame.K_LEFT]:
                  speed[0] = speed[0] if speed[0] == 0 else (abs(speed[0]) - 1) * int(speed[0] / abs(speed[0]))
              elif event.key in [pygame.K_d, pygame.K_RIGHT]:
                  speed[0] = speed[0] + 1 if speed[0] > 0 else speed[0] - 1
              elif event.key in [pygame.K_w, pygame.K_UP]:
                  speed[1] = speed[1] + 1 if speed[1] > 0 else speed[1] - 1
              elif event.key in [pygame.K_s, pygame.K_DOWN]:
                  speed[1] = speed[1] if speed[1] == 0 else (abs(speed[1]) - 1) * int(speed[1] / abs(speed[1]))
          elif event.type == pygame.MOUSEBUTTONDOWN:
              if event.button == 1:
                  still = True
          elif event.type == pygame.MOUSEBUTTONUP:
              still = False
              if event.button == 1:
                  ball_rect = ball_rect.move(event.pos[0] - ball_rect.left, event.pos[1] - ball_rect.top)
          elif event.type == pygame.MOUSEMOTION:
              if event.buttons[0] == 1:
                  x = event.pos[0] - ball_rect.left - ball_rect.width / 2
                  y = event.pos[1] - ball_rect.top - ball_rect.height / 2
                  ball_rect = ball_rect.move(x, y)
                  if ball_rect.right > width:
                      ball_rect = ball_rect.move(width - ball_rect.right, 0)
                  if ball_rect.bottom > height:
                      ball_rect = ball_rect.move(0, height - ball_rect.bottom)
                  if ball_rect.left < 0:
                      ball_rect = ball_rect.move(0 - ball_rect.left, 0)
                  if ball_rect.top < 0:
                      ball_rect = ball_rect.move(0, 0 - ball_rect.top)
  
      if pygame.display.get_active() and not still:
          ball_rect = ball_rect.move(speed[0], speed[1])
      if ball_rect.left < 0 or ball_rect.right > width:
          speed[0] *= -1
          if width < ball_rect.right < ball_rect.right + speed[0]:
              speed[0] *= -1
      if ball_rect.top < 0 or ball_rect.bottom > height:
          speed[1] *= -1
          if height < ball_rect.bottom < ball_rect.bottom + speed[1]:
              speed[1] *= -1
      bg_color.r = RGBChannel(ball_rect.left * 255 / width)
      bg_color.g = RGBChannel(ball_rect.top * 255 / height)
      bg_color.b = RGBChannel(min(speed[0], speed[1]) * 255 / max(speed[0], speed[1], 1))
      screen.fill(bg_color)
      screen.blit(ball, ball_rect)
      pygame.display.update()
      clock.tick(fps)
  
  ```

#### 2、[`pygame.RECT`](<https://www.pygame.org/docs/ref/rect.html>)

+ 表达一个矩形区域的类，用于存储坐标和长度等信息，`Pygame`利用`Rect`类来操作图形***/***图像等元素

+ 属性和方法：

  |    属性名     |       方法名        |
  | :-----------: | :-----------------: |
  |      `x`      |      `.copy()`      |
  |      `y`      |      `.move()`      |
  |      `w`      |    `.inflate()`     |
  |      `h`      |     `.clamp()`      |
  |    `size`     |      `.clip()`      |
  |    `width`    |     `.union()`      |
  |   `height`    |    `.unionall()`    |
  |     `top`     |      `.fit()`       |
  |    `left`     |   `.normalize()`    |
  |   `bottom`    |    `.contains()`    |
  |    `right`    |  `.collidepoint()`  |
  |   `topleft`   |  `.colliderect()`   |
  | `bottomleft`  |  `.collidelist()`   |
  |  `topright`   | `.collidelistall()` |
  | `bottomright` |  `.collidedict()`   |
  |   `midtop`    | `.collidedictall()` |
  |   `midleft`   |                     |
  |  `midbottom`  |                     |
  |  `midright`   |                     |
  |   `center`    |                     |
  |   `centerx`   |                     |
  |   `centery`   |                     |

#### 3、`pygame.draw`

+ 绘制图形

  |    方法名    |     图形     |
  | :----------: | :----------: |
  |  `.rect()`   |     矩形     |
  | `.polygon()` |    多边形    |
  | `.circle()`  |     圆形     |
  | `.ellipse()` |    椭圆形    |
  |   `.arc()`   |   椭圆弧形   |
  |  `.line()`   |     直线     |
  |  `.lines()`  |   连续多线   |
  | `.aaline()`  |   无锯齿线   |
  | `.aalines()` | 连续无锯齿线 |

+ `pygame.draw.rect(Surface, color, Rect, width=0)`

  + `Surface`，矩形的绘制屏幕

  + `Color`，矩形的绘制颜色

  + `Rect`，矩形的绘制区域，`x, y, width, height`

  + `width`，绘制边缘的宽度，默认为`0`，即填充图形

    ```python
    import sys
    import pygame
    
    pygame.init()
    
    screen = pygame.display.set_mode((600, 400))
    pygame.display.set_caption("pygame图形绘制")
    gold = 255, 251, 0
    red = 255, 0, 0
    
    r1_rect = pygame.draw.rect(screen, gold, (100, 100, 200, 100), 5)
    r2_rect = pygame.draw.rect(screen, red, (210, 210, 200, 100), 0)
    
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            pygame.display.update()
    
    ```

+ `pygame.draw.polygon(Surface, color, pointlist, width=0)`

  + `Surface`，多边形的绘制屏幕
  + `Color`，多边形的绘制颜色
  + `pointlist`，多边形顶点坐标列表
  + `width=0`，绘制边缘的宽度，默认为`0`，即填充图形

+ `pygame.draw.circle(Surface, color, pos, radius, width=0)`

  + `pos`，圆心的坐标
  + `radius`，半径

+ `pygame.draw.ellipse(Surface, color, Rect, width=0)`

  + `Rect`，椭圆形的绘制区域

+ `pygame.draw.arc(Surface, color, Rect, start_angle, stop_angle, width=0)`

  + `start_angle`，弧度绘制起始弧度值，横向右侧为`0`度，范围`0-360`
  + `stop_angle`，弧度绘制结束弧度值

+ `pygame.draw.line(Surface, color, start_pos, end_pos, width=1)`

  + `start_pos, end_pos`，起始和结束坐标
  + `width`，宽度，默认是`1`

+ `pygame.draw.lines(Surface, color, closed, pointlist, width=1)`

  + `closed`，如果为`True`，起止节点间自动增加封闭直线
  + `pointlist`，连续多线的顶点坐标列表

+ `pygame.draw.aaline(Surface, color, start_pos, end_pos, blend=1)`

  + `blend`，不为`0`时，与线条所在背景颜色进行混合

+ `pygame.draw.aalines(Surface, color, closed, pointlist, blend=1)`

  ```python
  import sys
  import pygame
  from math import pi
  
  pygame.init()
  screen = pygame.display.set_mode((600, 400))
  pygame.display.set_caption("Pygame图形绘制")
  GOLD = 255, 251, 0
  RED = pygame.Color('red')
  WHITE = 255, 255, 255
  GREEN = pygame.Color('green')
  
  e1rect = pygame.draw.ellipse(screen, GREEN, (50, 50, 500, 300), 3)
  c1rect = pygame.draw.circle(screen, GOLD, (200, 180), 30, 5)
  c2rect = pygame.draw.circle(screen, GOLD, (400, 180), 30)
  r1rect = pygame.draw.rect(screen, RED, (170, 130, 60, 10), 3)
  r2rect = pygame.draw.rect(screen, RED, (370, 130, 60, 10))
  plist = [(295, 170), (285, 250), (260, 280), (340, 280), (315, 250), (305, 170)]
  
  al1rect = pygame.draw.aalines(screen, GOLD, True, plist, 2)
  a1rect = pygame.draw.arc(screen, RED, (200, 220, 200, 100), 1.4 * pi, 1.9 * pi, 3)
  while True:
      for event in pygame.event.get():
          if event.type == pygame.QUIT:
              sys.exit()
      pygame.display.update()
  
  ```

### 六、文字绘制

#### 1、`pygame.freetype`

+ 向屏幕上绘制特定字体的文字
  + 文字不能直接绘制，而是用像素根据字体点阵图绘制

+ `pygame.freetype.Font(file, size=0)`

  + `file`，字体类型名称或路径
  + `size`，字体大小

+ `Font.render_to(surf, dest, text, fgcolor=None, bgcolor=None, rotation=0, size=0) -> Rect`

  + `dest`，绘制位置，`(x, y)`

  + `text`，绘制的文字

  + `fgcolor`，文字颜色

  + `bgcolor`，背景颜色

  + `rotation`，逆时针旋转的角度，取值`0-359`，部分字体可旋转

  + `size`，文字大小

    ```python
    import sys
    import pygame
    import pygame.freetype
    
    pygame.init()
    
    screen = pygame.display.set_mode((600, 400))
    pygame.display.set_caption("pygame文字绘制")
    gold = 255, 251, 0
    f1 = pygame.freetype.Font("C://Windows//Fonts//msyh.ttc", 36)
    f1_rect = f1.render_to(screen, (200, 160), "世界和平", fgcolor=gold, size=50)
    
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
        pygame.display.update()
    
    ```

+ `Font.render(text, fgcolor=None, bgcolor=None, rotation=0, size=0) -> (Surface, Rect)`

  ```python
  import sys
  import pygame
  import pygame.freetype
  
  pygame.init()
  
  screen = pygame.display.set_mode((600, 400))
  pygame.display.set_caption("pygame文字绘制")
  gold = 255, 251, 0
  f1 = pygame.freetype.Font("C://Windows//Fonts//msyh.ttc", 36)
  f1_surface, f1_rect = f1.render("世界和平", fgcolor=gold, size=50)
  
  
  while True:
      for event in pygame.event.get():
          if event.type == pygame.QUIT:
              sys.exit()
      screen.blit(f1_surface, (200, 160))
      pygame.display.update()
  
  ```

#### 2、壁球小游戏文字版

```python
import sys
import pygame.freetype
import pygame

pygame.init()
size = width, height = 600, 400
speed = [1, 1]
BLACK = 0, 0, 0
gold = 255, 251, 0
pos = [230, 160]
screen = pygame.display.set_mode(size)
pygame.display.set_caption("pygame壁球（文字型）")
f1 = pygame.freetype.Font("C://Windows//Fonts//msyh.ttc", 36)
f1_rect = f1.render_to(screen, pos, "世界和平", fgcolor=gold, size=50)

# Frames per Second 每秒帧率参数
fps = 300
clock = pygame.time.Clock()
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
    if pos[0] < 0 or pos[0] + f1_rect.width > width:
        speed[0] *= -1
    if pos[1] < 0 or pos[1] + f1_rect.height > height:
        speed[1] *= -1
    pos[0] += speed[0]
    pos[1] += speed[1]
    screen.fill(BLACK)
    f1_rect = f1.render_to(screen, pos, "世界和平", fgcolor=gold, size=50)
    pygame.display.update()
    clock.tick(fps)

```

```python
import sys
import pygame.freetype
import pygame

pygame.init()
size = width, height = 600, 400
speed = [1, 1]
BLACK = 0, 0, 0
gold = 255, 251, 0
pos = [230, 160]
screen = pygame.display.set_mode(size)
pygame.display.set_caption("pygame壁球（文字型）")
f1 = pygame.freetype.Font("C://Windows//Fonts//msyh.ttc", 36)
f1_surface, f1_rect = f1.render("PYTHON", fgcolor=gold, size=50)

# Frames per Second 每秒帧率参数
fps = 300
clock = pygame.time.Clock()
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
    if pos[0] < 0 or pos[0] + f1_rect.width > width:
        speed[0] *= -1
    if pos[1] < 0 or pos[1] + f1_rect.height > height:
        speed[1] *= -1
    pos[0] += speed[0]
    pos[1] += speed[1]
    screen.fill(BLACK)
    screen.blit(f1_surface, tuple(pos))
    pygame.display.update()
    clock.tick(fps)
```

