### 一、requests库

#### 1、Request对象

+ `request.request(method, url, **kwargs)`，构造一个请求，支撑以下各方法的基础方法；
  + `kwargs`：
    - `params`，字典或字节序列，作为参数增加到`url`中；
    - `data`，字典，字节序列或文件对象，作为`Request`的内容；
    - `json`，`JSON`格式的数据，作为`Request`的内容；
    - `headers`，字典，定制`HTTP`头；
    - `cookies`，字典或`CookieJar`，`Request`中的`cookie`；
    - `auth`，元组，支持`HTTP`认证功能；
    - `files`，字典类型，传输文件；
    - `timeout`，设定超时时间，单位是秒；
    - `proxies`，字典类型，设定访问代理服务器，可以增加登录认证；
    - `allow_redirects`：`True/False`，默认是`False`，重定向开关；
    - `stream`：`True/False`，默认是`True`，获取内容立即开始下载开关；
    - `verify`：`True/False`，默认是`True`，认证`SSL`，证书开关；
    - `cert`：本地`SSL`证书路径。

+ `request.get(url, params=None, **kwargs)`
+ `request.header()`，获取`HTTP`网页头信息的方法；
+ `request.post()`；
+ `request.put()`；
+ `request.patch()`，向网页提交局部修改请求；
+ `request.delete()`；

#### 2、Response对象

+ 属性

  + `status_code`，`http`，请求的返回状态，`200`表示连接成功；

  + `text`，`http`响应内容的字符串形式，即`url`对应的页面内容；

  + `encoding`，从`HTTP header`中猜测的响应内容编码方式；

  + `apparent_encoding`，从内容中分析出的响应内容编码方式（备选编码方式）；

  + `content`，`HTTP`响应内容的二进制形式；

    ```python
    import requests
    
    baidu = requests.get("http://www.baidu.com")
    print(baidu.status_code)
    baidu.encoding = baidu.apparent_encoding
    print(baidu.text)
    print(baidu.content)
    
    ```

+ 方法

  + `raise_for_status()`，如果返回值不是`200`，产生异常`HTTPError`；

#### 3、异常

+ `ConnectionError`，网络连接错误；
+ `HTTPError`，`HTTP`错误异常；
+ `URLRequired`，`URL`缺失异常；
+ `TooManyRedirects`，超过最大重定向次数，产生重定向异常；
+ `ConnectTimeout`，连接到远程服务器超时异常；
+ `Timeout`，请求`URL`超时，产生超时异常；

### 二、网络爬虫的限制

+ 来源审查，判断`User-Agent`进行限制
  + 检查来访`HTTP`协议头的`User-Agent`域，只响应浏览器或友好爬虫的访问。
+ 发布公告：`Robots`协议
  + 告知所有爬虫网站的爬取策略，要求爬虫遵守。（非强制）

### 三、Robots协议

+ `Robots Exclusion Standard`，网络爬虫排除标准；

+ 作用，网站告知网络爬虫哪些页面可以抓取，哪些不可以。

+ 形式，在网站根目录下的`robots.txt`文件。

+ 语法：

  ```ini
  # 注释，* 代表所有，/ 代表根目录
  User-agent: * # 表名哪些爬虫，此处只所有爬虫
  Disallow: / # 不允许访问资源的目录，可有多个
  ```

### 四、Beautiful Soup库

+ ```shell
  pip install beaufulsoup4
  ```

#### 1、`beaufulsoup4`的解析器：

+ `bs4`的`HTML`解析器，安装`bs4`库；
+ `lxml`的`HTML`解析器，安装`lxml`库；
+ `lxml`的`XML`解析器，安装`lxml`库；
+ `html5lib`的解析器，安装`html5lib`库

#### 2、`BeautifulSoup`类的基本元素

+ `Tag`，标签，最基本的信息组织单元，分别使用`<>`和`</>`标明开头和结尾；

+ `Name`，标签的名字，`<p>……</p>`的名字是`p`，格式`<tag>.name`；

+ `Attributes`，标签的属性，字典形式组织，格式`<tag>.attrs`；

+ `NavigableString`，标签内非属性字符串，`<>……</>`中字符形式，格式`<tag>.string`；

+ `Comment`，标签内字符串的注释部分，一种特殊的`Comment`类型

+ ```python
  import requests
  from bs4 import BeautifulSoup
  
  url = "http://python123.io/ws/demo.html"
  res = requests.get(url)
  res.encoding = res.apparent_encoding
  soup = BeautifulSoup(res.text, "html.parser")
  
  print(soup.title.string)
  print(type(soup.title.string))
  print(soup.a.parent.name)
  print(soup.a.parent.parent.name)
  print(soup.a.attrs)
  print(soup.a.attrs['href'])
  """
  This is a python demo page
  <class 'bs4.element.NavigableString'>
  p
  body
  {'href': 'http://www.icourse163.org/course/BIT-268001', 'class': ['py1'], 'id': 'link1'}
  http://www.icourse163.org/course/BIT-268001
  """
  ```

#### 3、标签树的下行遍历

+ `.contents`，子节点的列表，将`<tag>`所有子节点存入列表；

+ `.children`，子节点的迭代类型，与`.contents`类似，用于循环遍历子节点；

+ `descendants`，子孙节点的迭代类型，包含所有子孙节点，用于循环遍历。

+ ```python
  import requests
  from bs4 import BeautifulSoup
  
  url = "http://python123.io/ws/demo.html"
  res = requests.get(url)
  res.encoding = res.apparent_encoding
  soup = BeautifulSoup(res.text, "html.parser")
  
  print(soup.head.contents)
  print(soup.body.contents, len(soup.body.contents))
  
  for child in soup.body.children:
      print("子节点文本：" + child.text)
  
  for des in soup.body.descendants:
      print(des)
  
  ```

#### 4、标签树的上行遍历

+ `.parent`，节点的父亲节点；

+ `.parents`，节点先辈标签的迭代类型，用于循环遍历先辈节点。

+ ```python
  import requests
  from bs4 import BeautifulSoup, element
  
  url = "http://python123.io/ws/demo.html"
  res = requests.get(url)
  res.encoding = res.apparent_encoding
  soup = BeautifulSoup(res.text, "html.parser")
  
  print(soup.title.parent)
  for parent in soup.a.parents:
      if parent is not None:
          print(parent.name)
  """
  <head><title>This is a python demo page</title></head>
  p
  body
  html
  [document]
  """
  ```

#### 5、标签树的平行遍历

+ `.next_sibling`，返回按照`HTML`，文本顺序的下一个平行节点标签；

+ `.previous_sibling`，返回按照`HTML`文本顺序的上一个平行节点标签；

+ `.next_siblings`，迭代类型，返回按照`HTML`文本顺序的后续所有平行节点标签；

+ `.previous_siblings`，迭代类型，返回按照`HTML`文本顺序的前续所有平行节点标签。

+ ```python
  import requests
  from bs4 import BeautifulSoup, element
  
  url = "http://python123.io/ws/demo.html"
  res = requests.get(url)
  res.encoding = res.apparent_encoding
  soup = BeautifulSoup(res.text, "html.parser")
  
  print(soup.a.next_sibling)
  print(soup.a.next_sibling.next_sibling)
  print(soup.a.previous_sibling)
  print("后面的兄弟：")
  for sibling in soup.a.next_siblings:
      print("兄弟：", end="")
      print(sibling)
  
  print("前面的兄弟：")
  for sibling in soup.a.previous_siblings:
      print("兄弟：", end="")
      print(sibling)
  
  ```

#### 6、信息提取

+ 三种信息标记形式：

  + `XML`
  + `JSON`
  + `YAML`

+ `.find_all(name, attrs, recursive, string, **kwargs)`，返回一个列表类型，存储查找的结果：

  + `name`，对标签名称的检索字符串，这是一个列表类型；

  + `attrs`，对标签属性值的检索字符串，可标注属性检索；

  + `recursive`，是否对子孙全部检索，默认`True`；

  + `string`，`<>...</>`中字符串区域的检索字符串；

  + ```python
    import requests
    from bs4 import BeautifulSoup
    import re
    
    url = "http://python123.io/ws/demo.html"
    res = requests.get(url)
    res.encoding = res.apparent_encoding
    soup = BeautifulSoup(res.text, "html.parser")
    
    print("筛选目标标签：")
    for link in soup.find_all(name=['a', 'b']):
        print(link)
    
    print("使用正则表达式：")
    for tag in soup.find_all(name=re.compile('b')):
        print(tag.name)
    
    print("添加属性筛选：")
    for tag in soup.find_all(name='p', attrs='course'):
        print(tag)
    
    print("指定属性：")
    for tag in soup.find_all(id='link1'):
        print(tag)
    
    print("使用正则表达式指定属性：")
    for tag in soup.find_all(id=re.compile('link')):
        print(tag)
    
    print("不检索子孙节点：")
    for link in soup.find_all(name='a', recursive=False):
        print(link)
    
    print("检索字符串：")
    print(soup.find_all(string="Basic Python"))
    print(soup(string="Basic Python"))
    
    print("检索字符串（正则表达式）：")
    print(soup.find_all(string=re.compile("Python")))
    
    ```

  + `<tag>(..)`等价于`<tag>.find_all()`；

  + `soup(..)`等价于`soup.find_all`；

+ `find()`，搜索且只返回一个结果，字符串类型，同`.find_all`参数；

+ `.find_parents()`，在先辈节点中搜索，返回列表类型，同`.find_all`参数；

+ `.find_parent()`，在先辈节点中返回一个结果，字符串类型，同`.find()`参数；

+ `.find_next_siblings()`，在后续平行节点中搜索，返回列表类型，同`.find_all`参数；

+ `.find_next_sibling()`，在后续平行节点中返回一个结果，字符串类型，同`.find()`参数；

+ `.find_previous_siblings()`，在前序平行节点中搜索，返回列表类型，同`.find_all()`参数；

+ `.find_previous_sibling()`，在前序平行节点中返回一个结果，字符串类型，同`.find()`参数；

### 五、实例

#### 1、中国大学排名获取

+ ```python
  import bs4
  import requests
  
  
  def get_html_text(url: str) -> str:
      """
      获取指定连接的html页面文本
      :param url: 链接
      :return: 指定链接的文本, str
      """
      try:
          res = requests.get(url)
          res.encoding = "utf-8" if res.apparent_encoding is None else res.apparent_encoding
      except requests.RequestException:
          return "请求失败！"
      else:
          return res.text
  
  
  def fill_univ_list(url_list: list, html: str) -> None:
      """
      从网页文本中提取内容
      :param url_list: 结果列表
      :param html: 网页文本
      :return: None
      """
      soup = bs4.BeautifulSoup(html, "html.parser")
      for tr in soup.find('tbody').children:
          if isinstance(tr, bs4.element.Tag):
              tds = tr(name='td')
              url_list.append((tds[0].text.replace("\n", "").strip(),
                               tds[1].text.replace("\n", "").strip(),
                               tds[2].text.replace("\n", "").strip(),
                               tds[3].text.replace("\n", "").strip(),
                               tds[4].text.replace("\n", "").strip(),
                               tds[5].text.replace("\n", "").strip()))
  
  
  def print_univ_list(url_list: list, num: int) -> None:
      """
      打印结果
      :param url_list: 结果列表
      :param num: 打印结果数
      :return: None
      """
      template = "{0:{6}^5}\t{1:{6}^100}\t{2:{6}^10}\t{3:{6}^10}\t{4:{6}^10}\t{5:{6}^10}"
      print(template.format("排名", "学校名称", "省市", "类型", "总分", "办学层次", " "))
      for i in range(num):
          u = url_list[i]
          print(template.format(u[0], u[1], u[2], u[3], u[4], u[5], " "))
  
  
  def main():
      uinfo = []
      url = "https://www.shanghairanking.cn/rankings/bcur/2022"
      html = get_html_text(url)
      fill_univ_list(uinfo, html)
      print_univ_list(uinfo, 20)
  
  
  if __name__ == '__main__':
      main()
  
  ```


### 六、正则表达式

| 操作符 |                说明                |                  实例                  |
| :----: | :--------------------------------: | :------------------------------------: |
|   .    |          表示任何单个字符          |                                        |
|  [ ]   |   字符集，对单个字符给出取值范围   | [abc]表示a、b、c，[a-z]表示a-z单个字符 |
| [ ^ ]  |  非字符集，对单个字符给出排除范围  |    [ ^abc ]表示非a、b、c的单个字符     |
|   \*   |     前一个字符0次或无限次扩展      |     abc*表示ab，abc，abcc，abccc等     |
|   +    |     前一个字符1次或无限次扩展      |       abc+表示abc，abcc，abccc等       |
|   ?    |       前一个字符0次或1次扩展       |            abc?表示ab、abc             |
|   \|   |         左右表达式任意一个         |         abc \| def表示abc、def         |
|  {m}   |         扩展前一个字符m次          |             ab{2}c表示abbc             |
| {m, n} |   扩展前一个字符m至n次（包含n）    |         ab{1, 2}c表示abc、abbc         |
|   \^   |           匹配字符串开头           |    ^abc表示abc且在一个字符串的开头     |
|   \$   |           匹配字符串结尾           |    abc$表示abc且在一个字符串的结尾     |
|  ( )   | 分组标记，内部只能使用 *\|* 操作符 |  (abc)表示abc，(abc\|def)表示abc、def  |
|  \\d   |         数字，等价于[0-9]          |                                        |
|  \\w   |    单词字符，等价于[0-9a-zA-Z_]    |                                        |

+ 最小匹配操作符

| 操作符  |                 说明                  |
| :-----: | :-----------------------------------: |
|   \*?   |  前一个字符0次或无限次扩展，最小匹配  |
|   +?    |  前一个字符1次或无限次扩展，最小匹配  |
|   ??    |   前一个字符0次或1次扩展，最小匹配    |
| {m, n}? | 扩展前一个字符m至n次（含n），最小匹配 |



#### 1、Re库

+ `re.search(pattern, string, flags=0)`，在一个字符串中搜索匹配正则表达式的第一个位置，返回`match`对象；
  + `re.I re.IGNORECASE`，忽略正则表达式的大小写，[A-Z]能匹配小写字符；
  + `re.M re.MULTILINE`，正则表达式中的`^`操作符能够将给定字符串的每行当做匹配开始；
  + `re.S re.DOTALL`，正则表达式中的`.`操作符能够匹配所有字符串，默认匹配除换行外的所有字符。
+ `re.match(pattern, string, flags=0)`，从一个字符串的开始位置起匹配正则表达式，返回`match`对象；
+ `re.findall(pattern, string, flags=0)`，搜索字符串，以列表类型返回全部能匹配的子串；
+ `re.split(pattern, string, maxsplit=0, flags=0)`，将一个字符串按照正则表达式匹配结果进行分割，返回列表类型；
  + `maxsplit`，最大分割数，剩余部分作为最后一个元素输出；
+ `re.finditer(pattern, string, flags=0)`，搜索字符串，返回一个匹配结果的迭代类型，每个迭代元素是`match`对象；
+ `re.sub(pattern, repl, string, count=0, flags=0)`，在一个字符串中替换所有匹配正则表达式的子串，返回替换后的字符串；
  + `count`，匹配的最大替换参数
+ `re.compile(pattern, flags=0)`，将正则表达式的字符串形式编译成正则表达式对象。

#### 2、`Match`对象的属性

+ `.string`，待匹配的文本；
+ `.re`，匹配时使用的`pattern`对象（正则表达式）；
+ `.pos`，正则表达式搜索文本的开始位置；
+ `.endpos`，正则表达式搜索文本的结束位置。

#### 3、`Match`对象的方法

+ `.group(0)`，获得匹配后的字符串；
+ `.start()`，匹配字符串在原始字符串的开始位置；
+ `.end()`，匹配字符串在原始字符串的结束位置；
+ `.span()`，返回`(.start(), .end())`

```python
import re

reg = r'[A-Z]\d'
string = "abcd123a2n3nxjdw4n5kn3k5n34kj5nb3k4j34rwdnkjnwajk092i13091312"

ls = re.findall(reg, string, flags=re.I)
match = re.search(reg, string, flags=re.I)
ls_split = re.split(r'[a-z]\d', string, maxsplit=0, flags=re.IGNORECASE)
res = re.sub(reg, chr(10084), string, flags=re.I)
print(ls)
print(match.start())
print(ls_split, len(ls_split))
print(res)
"""
['d1', 'a2', 'n3', 'w4', 'n5', 'n3', 'k5', 'n3', 'j5', 'b3', 'k4', 'j3', 'k0', 'i1']
3
['abc', '23', '', 'nxjd', '', 'k', '', '', '4k', 'n', '', '', '4rwdnkjnwaj', '92', '3091312'] 15
abc❤23❤❤nxjd❤❤k❤❤❤4k❤n❤❤❤4rwdnkjnwaj❤92❤3091312

"""
```

### 七、Scrapy爬虫框架

+ `pip install -i https://mirrors.aliyun.com/pypi/simple/ scrapy`
+ 命令格式：`scrapy <command> [options] [args]`
  - `startproject`，创建一个新工程，`scrapy startproject <name> [dir]`；
  - `genspider`，创建一个爬虫，`scrapy genspider [options] <name> <domain>`；
  - `settings`，获取爬虫配置信息，`scrapy settings [options]`；
  - `crawl`，运行一个爬虫，`scrapy crawl <spider>`；
  - `list`，列出工程中所有爬虫，`scrapy list`；
  - `shell`，启动`URL`调试命令行，`scrapy shell [url]`。
+ 框架：
  + `Engine`，控制模块之间的数据流；根据条件触发事件。
  + `Downloader`，根据请求下载网页。
  + `Scheduler`，对所有爬取请求进行调度管理。
  + `Downloader Middleware`，实施`Engine、Scheduler和Downloader`之间进行用户可配置的控制，修改、丢弃、新增请求或响应。
  + `Spider`，解析`Downloader`返回的响应；产生爬取项；产生额外的爬取请求。
  + `Item Pipelines`，以流水线方式处理`Spider`产生的爬取项；由一组操作顺序组成，类似流水线，每个操作是一个`Item Pipelines`类型；可能操作包括：清理、检验和查重爬取项中的`HTML`数据、将数据存储到数据库。
  + `Spider Middleware`，对请求和爬取项的再处理，修改、丢弃、新增请求或爬取项。

#### 1、步骤一：建立一个`Scrapy`爬虫工程

+ `scrapy startproject python123demo`
+ 目录结构：
  + `python123demo`，外层目录
    + `scrapy.cfg`，部署`Scrapy`爬虫的配置文件
    + `python123demo`，`Scrapy`框架的用户自定义`Python`代码
      + `__init__.py`，初始化脚本
      + `items.py`，`Items`代码模板（继承类）
      + `middlewares.py`，`Middlewares`代码模板（继承类）
      + `pipelines.py`，`Pipelines`代码模板（继承类）
      + `settings.py`，`Scrapy`爬虫的配置文件
      + `spiders`，`Spider`代码模板目录（继承类）
        + `__init.py__`，初始文件，无需修改
        + `__pycache__`，缓存目录，无需修改

#### 2、步骤二：在工程中产生一个`Scrapy`爬虫

+ `scrapy genspider demo python123.io`，`spiders`目录下的`demo.py`文件内容：

  ```python
  import scrapy
  
  class DemoSpider(scrapy.Spider):
      name = 'demo'
      allowed_domains = ['python123.io']
      start_urls = ['http://python123.io/']
  
      def parse(self, response):
          pass
  ```

+ 修改过后的：

  ```python
  import scrapy
  
  
  class DemoSpider(scrapy.Spider):
      name = 'demo'
      # allowed_domains = ['python123.io']
      start_urls = ['http://python123.io/ws/demo.html']
  
      def parse(self, response, **kwargs):
          filename = response.url.split('/')[-1]
          with open(filename, 'wb') as file:
              file.write(response.body)
          self.log(f'Save file {filename}.')
  
  ```

+ 运行：`scrapy crawl demo`

+ `yield`关键字（生成器）：

  + 生成器是一个不断产生值的函数；
  + 包含`yield`语句的函数是一个生成器；
  + 生成器每次产生一个值（`yield`语句），函数被冻结，被唤醒后再产生一个值。

  ```python
  def gen(n):
      for k in range(n):
          yield k ** 2
  
  
  for i in gen(5):
      print(i, " ", end="")
  """
  0  1  4  9  16  
  """
  ```

  + 优点：
    + 更节省存储空间；
    + 响应更迅速；
    + 使用更灵活

#### 3、步骤三：编写`Item Pipeline`

#### 4、步骤四：优化配置策略

#### 5、Request类

+ `.url`，`Request`对应的请求`url`地址；
+ `.method`，对应的请求方法，`GET`，`POST`等；
+ `.headers`，字典类型风格的请求头；
+ `.body`，请求内容主体，字符串类型；
+ `.meta`，用户添加的扩展信息，在`Scrapy`内部模块间传递信息使用；
+ `.copy()`，复制该请求

#### 6、Response类

+ `.url`，`Response`对应的`url`地址；
+ `.status`，`HTTP`状态码，默认是`200`；
+ `.headers`，`Response`对应的头部信息；
+ `.body`，`Response`对应的内容信息，字符串类型；
+ `.flags`，一组标记；
+ `.request`，产生`Response`类型对应的`Request`对象；
+ `.copy()`，复制该响应

#### 7、Item类

+ 表示一个从`HTML`页面中提取的信息内容
+ 由`Spider`生成，由`Item Pipeline`处理
+ `Item`类似字典类型，可以按照字典类型操作

#### 8、`Scrapy`爬取信息的方法

+ `Beautiful Soup`
+ `lxml`
+ `re`
+ `XPath Selector`
+ `CSS Selector`
  + `<HTML>.css('a::attr(href)').extract()`

#### 9、股票数据爬取实例

+ `scrapy startproject BaiduStocks`

+ `cd BaiduStocks`

+ `scrapy genspider stocks baidu.com`

+ 进一步修改`spiders/stocks.py`文件

+ 配置`stocks.py`文件

+ 修改对返回页面的处理

+ 修改对新增`url`爬取请求的处理

+ `stocks.py`

  ```python
  import scrapy
  import re
  
  
  class StocksSpider(scrapy.Spider):
      name = 'stocks'
      start_urls = ['http://quote.eastmoney.com/stocklist.html']
  
      def parse(self, response, **kwargs):
          for href in response.css('a:"attr(href)').extract():
              try:
                  stock = re.findall(r"s[hz]\d{6}", href)[0]
                  url = f'https://gupiao.baidu.com/stock/{stock}.html'
                  yield scrapy.Request(url, callback=self.parse_stock)
              except RuntimeError:
                  continue
  
      @staticmethod
      def parse_stock(response):
          info_dict = {}
          stock_info = response.css(".stock-bets")
          name = stock_info.css('.bets-name').extract[0]
          key_list = stock_info.css('dt').extract()
          value_list = stock_info.css('dd').extract()
          for i in range(len(key_list)):
              key = re.findall(r'>.*</dt>', key_list[i][0][1:-5])
              try:
                  val = re.findall(r'\d+\.?.*</dd>', value_list[i][0][0:-5])
              except RuntimeError:
                  val = '--'
              info_dict[key] = val
          info_dict.update({'股票名称': re.findall(r'\s.*\(', name)[0].split()[0] + re.findall(r'>.*<', name)[0][1:-1]})
  
  ```

+ `pipelines.py`

  ```python
  # Define your item pipelines here
  #
  # Don't forget to add your pipeline to the ITEM_PIPELINES setting
  # See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
  
  
  # useful for handling different item types with a single interface
  from itemadapter import ItemAdapter
  
  
  class BaidustocksPipeline:
      def process_item(self, item, spider):
          return item
  
  
  class BaiduStocksInfoPipeline(object):
      def __init__(self):
          self.f = None
  
      def open_spider(self, spider):
          self.f = open('BaiduStockInfo.txt', 'w')
  
      def close_spider(self, spider):
          self.f.close()
  
      def process_item(self, item, spider):
          try:
              line = str(dict(item)) + '\n'
              self.f.write(line)
          except RuntimeError:
              pass
          return item
  
  ```

+ `settings.py`：65行

  ```
  # Configure item pipelines
  # See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
  ITEM_PIPELINES = {
     'BaiduStocks.pipelines.BaiduStocksInfoPipeline': 300,
  }
  ```

+ 执行：`scrapy crawl stocks`

#### 10、配置并发连接选项

+ `CONCURRENT_REQUESTS`，`Downloader`最大并发请求下载数量，默认`32`；
+ `CONCURRENT_ITEMS`，`Item Pipeline`最大并发`Item`处理数量，默认`100`；
+ `CONCURRENT_REQUESTS_PER_DOMAIN`，每个目标域名最大的并发请求数量，默认`8`；
+ `CONCURRENT_REQUESTS_PER_IP`，每个目标`IP`最大的并发请求数量，默认`0`，非`0`有效

