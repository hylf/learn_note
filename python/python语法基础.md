- python 包含 33 个关键字，诞生于 1990 年，属于解释型语言。
- python 中缩进也是语法的一部分，同级的代码缩进必须相同。
- python 代码结尾可以没有分号。

### 一、数据类型

#### 1、字符串

- 在 python 中字符串和列表包含正向和反向，一个长度为 10 的字符，第一个字符序号是 0 或 -10，第 10 个字符的编号为 9 或 -1。
- 可以用单引号或者双引号表示字符。
- [开始:结束] 表示截取字符串，例如 [1:3] 表示从第二个字符开始到第四个字符，不包含第四个。

#### 2、列表

- 类似数组：[‘1’,‘a’]。

### 二、语句与函数

#### 1、分支语句

- 使用 if，elif，else 构成条件判断，英文冒号结尾。

- 温度转换实例：

  ```python
  # 温度转换，C/c表示华氏温度值，F/f表示里是摄氏温度值
  TempStr = input("请输入带有符号的温度值：")
  if TempStr[-1] in ['F', 'f']:
      C = (eval(TempStr[0:-1]) - 32) / 1.8
      print("转换后的华氏温度是{:.3f}C".format(C))
  elif TempStr[-1] in ['C', 'c']:
      F = (eval(TempStr[0:-1]) + 32) * 1.8
      print("转换后的摄氏温度值是{:.2f}F".format(F))
  else:
      print("输入的温度格式错误！")
  
  '''
  这是多行注释
  '''
  
  """
  这是多行注释
  """
  ```

- input 是输入函数，用法

  ```python
  变量名 = input(<提示信息>)
  ```

- print 是输出函数，包含格式化的方式

  - {} 表示槽，后续变量填充到槽中
  - {:.2f} 表示将 format 中的变量按要求格式化

- eval，表示评估函数。去掉参数最外侧引号并执行余下语句的函数。例如：

  ```python
  eval('print("Hello")')
  ```

- 换行的特殊写法：

  ```python
  print('''ddqdqd'
  'dqdqd''')
  ```

### 三、基本库的学习

#### 1、turtle（海龟绘图）

- 绘制蟒蛇

  ```python
  # python蟒蛇绘制
  # import turtle as t，可以指定别名
  import turtle
  turtle.setup(650, 350, 200, 200)
  turtle.penup()
  turtle.fd(-250)
  turtle.pendown()
  turtle.pensize(25)
  turtle.pencolor("red")
  turtle.seth(-40)
  for i in range(4):
      turtle.circle(40, 80)
      turtle.circle(-40, 80)
  turtle.circle(40, 80 / 2)
  turtle.fd(40)
  turtle.circle(16, 180)
  turtle.fd(40 * 2 / 3)
  turtle.done()
  ```

- setup(窗口宽，窗口高，窗口横坐标，窗口纵坐标)，设置生成窗口的属性。不指定后两个参数默认在中间。

- goto(横坐标，纵坐标)，移动海龟的位置。

- fd(像素值)，向海龟正前方移动指定像素，海龟默认向右。

- circle(半径，角度)，向海龟左侧移动指定角度和半径的圆。

- seth(角度)，改变海龟的方向（相对初始方向，绝对方向）。

- left/right，向左或向右（相对当前方向，相对方向）。

  ```python
  import turtle
  # 或者turtle.left(45)
  turtle.seth(45)
  turtle.fd(150)
  # 或者turtle.right(135)
  turtle.seth(-90)
  turtle.fd(300)
  # 或者turtle.left(135)
  turtle.seth(45)
  turtle.fd(150)
  turtle.done()
  ```

- turtle 默认使用 rgb 的小数值表示颜色，可以使用 colormode(mode) 切换。

  - colormode(1.0)，小数模式；
  - colormode(255)，整数模式；

- 简写：

  ```python
  # python蟒蛇绘制
  from turtle import *
  setup(650, 350)
  penup()
  fd(-250)
  pendown()
  pensize(25)
  pencolor("red")
  seth(-40)
  for i in range(4):
      circle(40, 80)
      circle(-40, 80)
  circle(40, 80 / 2)
  fd(40)
  circle(16, 180)
  fd(40 * 2 / 3)
  done()
  ```
  - 可能会出现重名问题。

- penup()，画笔抬起，此时不在画布上留下轨迹。

- pendown()，画笔落下，此时会留下轨迹。

- pensize()，画笔宽度。别名：width()。

- pencolor()，设置轨迹颜色。

  - 颜色小写字符串，例如 purple。
  - RGB 的小数值，例如 0.63，0.13，0.94。或者元组类型。
  - RGB 的整数值，例如 255，255，255。

- title()，窗口标题。

- setheading(angle)，改变行进分享，海龟的角度。别名：seth。

- hideturtle()，隐藏箭头。
- showturtle()，显示箭头。
- fillcolor(colorstring)，设置填充颜色。
- begin_fill()，开始填充。
- end_fill()，结束填充。

### 第四章、基本语法

#### 1、数据类型

- 整数，二进制以 0b 或 0B 开头，八进制以 0o 或 0O 开头，十六进制用 0x 或 0X 开头。
- round(x,y)，x表示需要进行四舍五入的数，y表示保留的位数。
- complex(x)，将 x 变为复数，增加虚数部分。

