### 一、下载类

#### 1、window系统下载

- `windows10`下载地址：https://www.microsoft.com/zh-cn/software-download/windows10
- `windows11`下载地址：https://www.microsoft.com/zh-cn/software-download/windows11
- `office`: https://otp.landian.vip/zh-cn/

#### 2、linux系统下载

- `centos7`：http://isoredirect.centos.org/centos/7/isos/x86_64/
- `kali`：https://www.kali.org/get-kali/#kali-virtual-machines
- `Rocky`: https://rockylinux.org/

#### 3、开发工具

- `idea`：https://www.jetbrains.com/zh-cn/idea/download/#section=windows
- `pycharm`：https://www.jetbrains.com/zh-cn/pycharm/download/#section=windows
- `goland`：https://www.jetbrains.com/zh-cn/go/download/download-thanks.html
- `vscode`：https://code.visualstudio.com/
- `visualstudio`：https://visualstudio.microsoft.com/zh-hans/downloads/
- `微信开发者工具`：https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html

#### 4、开发语言

- `java`：https://www.oracle.com/cn/java/technologies/downloads/#java11
- `python`：https://www.python.org/downloads/
- `go`：https://golang.google.cn/dl/
- `nodejs`：https://nodejs.org/en/download
- `c`：https://sourceforge.net/projects/mingw/

#### 5、构建工具

- `maven`：https://maven.apache.org/download.cgi
- `gradle`：https://gradle.org/releases/

#### 6、开发必备软件包

- `git`：https://git-scm.com/downloads
- `tomcat`：https://tomcat.apache.org/download-10.cgi
- `cmake`：https://cmake.org/download/
- `SPL`: http://www.scudata.com/, http://doc.scudata.com/esproc/tutorial/azysy.html
- `logisim`：http://www.cburch.com/logisim/，https://blog.csdn.net/m0_59667483/article/details/125941447，https://blog.csdn.net/u013648063/article/details/115383173
- `Visual VM`: https://github.com/graalvm/graalvm-ce-builds/releases/

#### 7、数据库

- `mysql`：https://dev.mysql.com/downloads/
- `mongodb`：https://www.mongodb.com/try/download/community
- `redis`：https://redis.io/download/

#### 8、浏览器

- `chrome`：https://www.google.cn/chrome/
- `firefox`：http://www.firefox.com.cn/
- `edge`：https://www.microsoft.com/zh-cn/edge/download?form=MA13FJ

#### 9、其它

- `vmware`：https://www.vmware.com/cn/products/workstation-pro.html
- `typora`：https://typoraio.cn/
- `wireshark`：https://www.wireshark.org/download.html
- `apifox`：https://apifox.com/#pricing
- `office`：https://www.microsoft.com/zh-cn/microsoft-365
- `wps`：https://www.wps.cn/
- `photoshop`：https://www.adobe.com/products/photoshop.html?promoid=RBS7NL7F&mv=other
- `navicat`：https://www.navicat.com.cn/products/navicat-premium
- `selenium`：https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/
- `小丸工具箱`：https://maruko.appinn.me/
- `视频压缩软件`：https://zhs.moo0.com/?top=https://zhs.moo0.com/software/VoiceRecorder/
- `图吧工具箱`：http://www.tbtool.cn/
- `chrome驱动下载`：https://registry.npmmirror.com/binary.html?path=chromedriver/
- `其它驱动下载`：https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/
- `mumu模拟器`：https://mumu.163.com/
- `网易UU加速器`：https://uu.163.com/
- `腾讯手游助手`：https://mumu.163.com/
- `teamviewer`：https://www.teamviewer.cn/cn/
- `xshell/xftp`：https://www.xshell.com/zh/free-for-home-school/
- `PDF编辑器`：https://www.tracker-software.com/product/downloads
- `gif录制`：https://github.com/NickeManarin/ScreenToGif
- `比特彗星`：https://www.qbittorrent.org/，https://github.com/c0re100/qBittorrent-Enhanced-Edition/releases/tag/release-4.5.2.10

### 二、常用网站地址

#### 1、工具类

##### ①在线工具

- `在线正则表达式`：https://regex101.com/
- `综合工具`：https://www.bejson.com/
- `在线算法演示`：https://www.cs.usfca.edu/~galles/visualization/Algorithms.html
- `ip查询`：https://ip138.com/
- `在线文档转换器`：https://www.aconvert.com/cn/
- `在线md5加密解密`：http://www.ttmd5.com/index.php?lang=cn，https://www.cmd5.com/
- `免费的LOGO在线设计制作工具`：https://www.uugai.com/
- `ico图标在线生成`：http://www.ico51.cn/，https://www.bitbug.net/
- `在线题目搜索`：https://cx.icodef.com/query.html
- `在线颜色选择器`：http://www.86y.org/code/colorpicker/color.html
- `在线资源搜索`：https://www.upyunso.com/
- `在线标签生成`：https://shields.io/category/test-results

##### ②其他

- `chatgpt`：https://chat.openai.com/chat
- `stackoverflow`：https://stackoverflow.com/
- `maven仓库`：https://mvnrepository.com/
- `编程语言排行榜`：https://www.tiobe.com/tiobe-index/
- `rfc官网`：https://www.rfc-editor.org/，https://www.cnblogs.com/lihw-study/p/15962982.html，https://www.rfc-editor.org/rfc/rfc6455
- `SpringBoot官网`：https://spring.io/projects/spring-boot/
- `echarts官网`：https://echarts.apache.org/examples/zh/index.html#chart-type-line
- `洛谷`：https://www.luogu.com.cn/
- `leetcode`：https://leetcode.cn/problemset/all/
- `牛客`：https://www.nowcoder.com/
- `植物大战僵尸`：http://pvz.booen.vip/infoPage.php?infoPageId=21441&infoPageAction=display
- `鼠标指针美化`：https://zhutix.com/tag/cursors/
- `阿里云盘`：https://www.aliyundrive.com/sign/in
- `游戏列表`：https://www.cmdw.vip/?type=product&id=74
- `switch520`：https://xxxxx520.com/
- `阿里巴巴矢量图形库`：https://www.iconfont.cn/collections/index?spm=a313x.7781069.1998910419.3
- `超能网`：https://www.expreview.com/
- `fastmock`：https://www.fastmock.site/#/login
- `ICP/IP地址/域名信息备案管理系统`：https://beian.miit.gov.cn/#/Integrated/index
- `互联网站安全管理服务平台`：http://www.beian.gov.cn/portal/index.do
- `elementui`：https://element.eleme.cn/，https://element-plus.gitee.io/zh-CN/

#### 2、python类问答

- `解决pip下载慢`：https://blog.csdn.net/change_you_life/article/details/124466875
- `pytest手册`：https://learning-pytest.readthedocs.io/zh/latest/index.html
- `私有变量的访问`：https://blog.csdn.net/m0_47146037/article/details/120696649
- `文本朗读库`：https://blog.csdn.net/qq_35164554/article/details/105846824
- `pyside2鼠标绘图板`：https://zhuanlan.zhihu.com/p/266955848
- `pyside2常用组件`：https://www.byhy.net/tut/py/gui/qt_01/
- `flask框架`：https://www.w3cschool.cn/flask/flask_url_building.html
- `pyinstaller`：https://blog.csdn.net/u014157782/article/details/107588709
- `pyinstaller打包多个文件`：https://blog.csdn.net/BXD1314/article/details/125226289
- `官方文档`：https://docs.python.org/zh-cn/3.10/
##### Scrapy相关
- `中文教程`: https://www.osgeo.cn/scrapy/intro/tutorial.html
- `调试运行`: https://www.cnblogs.com/Chary/articles/14363782.html
- `查询节点的方法`: https://blog.csdn.net/qq_39504519/article/details/107021737
- `xpath基本语法`: https://blog.csdn.net/qq_50854790/article/details/123610184

#### 3、java类问答

- `@PostConstruct注解`：https://blog.csdn.net/sunayn/article/details/92840439
- `@Async注解`：https://blog.csdn.net/qq_44750696/article/details/123960134
- `@ControllerAdvice注解`：https://blog.csdn.net/qq_36829919/article/details/101210250
- `junit`：https://www.w3cschool.cn/junit/
- `jmeter插件管理`：https://blog.csdn.net/u012106306/article/details/109067686
- `MAT的使用`：https://blog.csdn.net/kuankuan199308153614/article/details/123230090
- `@SpyBean和@MockBean区别`：https://blog.csdn.net/T3210692788/article/details/111572526
- `tomcat的localhost_access.log配置`：https://blog.csdn.net/lixiangchibang/article/details/84024253
- `java内存模型`：https://www.jianshu.com/p/8420ade6ff76
- `springboot配置https证书`：https://www.jianshu.com/p/244f4eb71eb7
- `equals和hashCode`：https://www.cnblogs.com/lulipro/p/5628750.html
- `设计模式`：https://blog.csdn.net/zhangerqing/article/details/8194653?spm=1001.2014.3001.5501，https://blog.csdn.net/zhangerqing/article/details/8239539?spm=1001.2014.3001.5501，https://blog.csdn.net/zhangerqing/article/details/8243942?spm=1001.2014.3001.5501，https://blog.csdn.net/zhangerqing/article/details/8245537?spm=1001.2014.3001.5501

#### 4、Go语言问答

- `go语言菜鸟教程`：https://www.runoob.com/go/go-tutorial.html
- `中文文档`：https://studygolang.com/pkgdoc
- `goframe框架`：https://github.com/gogf/gf，https://goframe.org/pages/viewrecentblogposts.action?key=gf
- `go mod的使用`：https://zhuanlan.zhihu.com/p/482014524
- `go mod机制`：https://blog.csdn.net/jkwanga/article/details/109672748
- `彻底搞懂golang的GOROOT和GOPATH`：https://blog.csdn.net/qq_38151401/article/details/105729884?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522165597350216781818768204%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=165597350216781818768204&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~first_rank_ecpm_v1~rank_v31_ecpm-1-105729884-null-null.nonecase&utm_term=go&spm=1018.2226.3001.4450
- `使用GO111MODULE解决go导包难,下包难问题-go如何导包下载包？go包报错如何解决?`：https://blog.csdn.net/qq_39611230/article/details/121232707

#### 5、shell问答

- `在线shell编程`：https://www.runoob.com/try/runcode.php?filename=helloworld&type=bash
- `linux命令大全`：https://www.runoob.com/linux/linux-command-manual.html
- `设置变量默认值`：https://blog.csdn.net/happytree001/article/details/120980066
- `shell测试框架`：https://github.com/kward/shunit2

#### 6、linux问答

- `iptables端口开启及端口关闭`：https://blog.csdn.net/solly793755670/article/details/112387411
- `rocky linux`：https://blog.csdn.net/qq_45392321/article/details/121585755
- `SSH服务启动错误`：https://blog.csdn.net/m0_46317658/article/details/121498061
- `OpenSSL library not found`：https://blog.csdn.net/qq837468220/article/details/123543696
- `linux安装从官网上下载的git源码包详细攻略`：https://blog.csdn.net/weixin_42169168/article/details/111283797
- `centos7升级OpenSSH 至最新 OpenSSH_8.6版本`：https://blog.csdn.net/qq_41355314/article/details/116495837
- `CentOS7安装MySQL8`：https://blog.csdn.net/weixin_42326851/article/details/123984601
- `centos7安装Python3`：https://blog.csdn.net/qq_42017469/article/details/118576335
- `CentOS 源使用帮助`：https://mirrors.ustc.edu.cn/help/centos.html
- `Centos7安装redis7`：https://blog.csdn.net/y13460108531/article/details/124564118
- `linux添加或修改hosts的方法`：https://blog.csdn.net/sqlquan/article/details/99974553
- `在docker中 java进程的内存设置`：https://blog.csdn.net/bigtree_3721/article/details/108111187

#### 7、markdown问答

- `Markdown 高级技巧`：https://www.runoob.com/markdown/md-advance.html
- `Markdown基本语法`：https://www.jianshu.com/p/191d1e21f7ed
- `LaTeX公式-Katex解析`：https://blog.csdn.net/u013210620/article/details/81938733
- `常用html标签`：https://www.cnblogs.com/catmelo/p/4162154.html
- `Markdown数学公式语法`：https://www.jianshu.com/p/e74eb43960a1

#### 8、jenkins问答

- `安装Jenkins并配置插件`：https://blog.csdn.net/qq_30273575/article/details/127147785
- `jenkins官网`：https://www.jenkins.io/zh/
- `设置中文`：https://blog.csdn.net/qq_37489565/article/details/104337073
- `教程`：https://blog.csdn.net/zhishidi/article/details/118082509
- `配置国内插件源`：https://www.cnblogs.com/zbzSH/p/16294521.html

#### 9、gradle问答

- `Gradle 教程`：https://www.w3cschool.cn/gradle/
- `快速入门`：www.360doc.com/content/18/0818/21/9200790_779311354.shtml
- `Gradle 快速入门`：https://www.jianshu.com/p/61e16306243a
- `Building Java Applications Sample`：https://docs.gradle.org/current/samples/sample_building_java_applications.html
- `Gradle的使用教程`：https://blog.csdn.net/qq_22172133/article/details/81513955

#### 10、selenium问答

- `官网`：http://www.selenium.org.cn/1598.html
- `python库的使用`：https://blog.csdn.net/weixin_36279318/article/details/79475388
- `加载策略`：https://zhuanlan.zhihu.com/p/453590557
- `selenium4新版本使用指南`：https://blog.csdn.net/weixin_47754149/article/details/125551434
- `Selenium的Options 参数`：https://www.jianshu.com/p/ab4e2b115ab6
- `通过xpath定位元素`：https://www.cnblogs.com/nini0806/p/13411179.html
- `爬取异步js数据`：https://www.cnblogs.com/sam-uncle/p/15415526.html
- `Actions类`：https://zhuanlan.zhihu.com/p/396773865

#### 11、websocket问答

- `w3c教程`：https://www.w3cschool.cn/websocket_protocol/r3lakozt.html

#### 12、git问答

- `Git中tag标签的使用`：https://blog.csdn.net/jdsjlzx/article/details/98654951
- `无法读取远程仓库`：https://blog.csdn.net/Dartao/article/details/128358052
- `git使用教程`：https://mp.weixin.qq.com/s/7Kx8yIbQst_mOQVN147btw

#### 13、redis问答

- `Redis 命令参考`：http://redisdoc.com/

#### 14、视频音乐网站

- `厂长资源`：https://czzy01.com/
- `天空影视`：https://tkznp.com/
- `热播库`：https://reboku.cc/
- `洛雪音乐`：https://github.com/lyswhut/lx-music-desktop#readme

#### 15、网络安全相关

- `nmap`：https://nmap.org/man/zh/index.html#man-description
- `钟馗之眼`：https://www.zoomeye.org/
- `互联网地图`：http://internet-map.net/

#### 13、其它

- `线程和协程的区别`：https://zhuanlan.zhihu.com/p/169426477?ivk_sa=1024320u
- `data: image/png； base64 用法详解`：https://blog.csdn.net/weixin_50339217/article/details/113387229
- `graphql`：https://graphql.org/
- `WebAssembly`：https://www.jianshu.com/p/e4d002780cf8，https://www.wasm.com.cn/getting-started/developers-guide/
- `反编译apk`：https://blog.csdn.net/SonnyJack/article/details/79273023
- `idea整合docker ca加密认证`：https://blog.csdn.net/wenkezhuangyuan/article/details/119393557
- `docker-maven-plugin使用SSL证书自动打包部署镜像到Docker服务器`：https://blog.csdn.net/qq_32201423/article/details/104503468
- `IDEA集成Docker插件实现一键自动打包部署微服务项目`：https://blog.csdn.net/u013490585/article/details/113545869
- `github添加开源许可证`：https://blog.csdn.net/weixin_43233914/article/details/121420751